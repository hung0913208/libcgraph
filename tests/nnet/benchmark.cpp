#ifndef BOOST_TEST_MAIN
#include "libbase/macros.hpp"

#if APPLE || USE_BASE_UNITTEST
#include "libbase/unittest.hpp"
#else
#include <gtest/gtest.h>
#endif

#include "all.hpp"

#ifndef BENCHMARK_WITH_MODEL
#define BENCHMARK_WITH_MODEL "tests/models/nghia.pbtxt"
#endif
#ifndef LABEL_FOR_BENCHMARK
#define LABEL_FOR_BENCHMARK "tests/samples/nghia.txt"
#endif
#ifndef SOUND_FOR_BENCHMARK
#define SOUND_FOR_BENCHMARK "tests/samples/nghia_call_wife_0.wav"
#endif

#ifndef NUM_LOOP_OF_PLAYGROUND
#define NUM_LOOP_OF_PLAYGROUND 1000
#endif

#ifndef MAX_STEP_OF_IM2COL
#define MAX_STEP_OF_IM2COL 20
#endif

#ifndef NUM_LOOP_OF_MODEL
#define NUM_LOOP_OF_MODEL 10
#endif

#if CHECK_CASE_BENCHMARK_TENSOR || (NDEBUG && !CHECK_CASE)
TEST(Benchmark, Tensor_){
  std::vector<int> begin{1, 2};
  std::vector<int> end{2, 2};

  nn::InterpreterW interpreter{""};

  interpreter([&](){
    auto tensor    = nNet_CreateTensorS();
    auto niltensor = nNet_CreateTensorS();
    auto ord       = Ordinate{nullptr};

    /* @NOTE: mac dinh khi khoi tao cac doi tuong deu duoc cap phat va co the 
     * dung ngay lap tuc */

    EXPECT_FALSE(nn::utils::outdate(tensor));
    EXPECT_TRUE(nNet_SetTensorNumT(tensor, Real32_t));
    EXPECT_TRUE(nNet_SetTensorShape(tensor, nNet_CreateTensorShapeS(4, 3, 3, 3, 2)));

    EXPECT_TRUE(nn::utils::assign<Real32>(tensor, {
      1,  2,  3,   4,  5,  6,   7,  8,  9,  // matrix 1 (.., 0, 0)
      10, 11, 12,  13, 14, 15,  16, 17, 18, // matrix 2 (.., 1, 0)
      19, 20, 21,  22, 23, 24,  25, 26, 27, // matrix 3 (.., 2, 0)
      28, 29, 30,  31, 32, 33,  34, 35, 36, // matrix 4 (.., 0, 1)
      37, 38, 39,  40, 41, 42,  43, 44, 45, // matrix 5 (.., 1, 1)
      46, 47, 48,  49, 50, 51,  52, 53, 54  // matrix 6 (.., 2, 1)
    }).code == base::error::ENoError);

    for (auto i = 0; i < nNet_GetArraySizeT(tensor); ++i){
      auto value = nNet_GetTensorItemS(tensor, i);

      EXPECT_TRUE(value != nullptr);
      if (value)
        EXPECT_EQ(*((Real32*)value), i + 1.0);
      nNet_TryDropingItem(tensor, value, i);
    }

    ord = nNet_CreateOrdinateW2(tensor, (int*)begin.data(), 2, 
                                        (int*)end.data(), 2);
    EXPECT_TRUE(ord != nullptr);
    EXPECT_TRUE(nNet_GetSubTensor(tensor, ord, niltensor) == true);

   BEGIN_PTRACKING()
    for (auto j = 0; j < NUM_LOOP_OF_PLAYGROUND; ++j)
      for (auto i0 = 0; i0 < 2; ++i0){
        for (auto i1 = 0; i1 < 3; ++i1){
          for (auto i2 = 0; i2 < 3; ++i2){
            for (auto i3 = 0; i3 < 3; ++i3){
              nNet_OrdinateToIndex(tensor,
                nNet_RewriteOrdinateS(ord, tensor, 4, i3, i2, i1, i0));
            }
          }
        }
      }
   END_PTRACKING;
    /* EXPECT_TIME_LIMITED(1000); */
    nNet_CalcIndex(niltensor, 1);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_BENCHMARK_MODEL_CONV2D || (NDEBUG && !CHECK_CASE)
#include <libnn/devices/generic/wavio.h>
#include <memory>

#include "all.hpp"

using FloattPtr = std::shared_ptr<float>;
using Labels = std::vector<std::string>;

FloattPtr   wav_data{nullptr};
Labels      wav_labels{};
std::string wav_path{};
std::size_t wav_size;

bool prepare(std::string path){
  using namespace nn::generic::audio;

  if (wav_data == nullptr || path != wav_path){
    WavIO<> wavio{};

    if (wavio.open(rvalue(path)))
      return false;
    try {
      wav_path = path;
      wav_size = wavio.size();
      wav_data = FloattPtr(new float[wav_size]);
    
      for (size_t i = 0; i < wav_size; ++i){
        wavio >> wav_data.get()[i];
      }
    } catch(base::Error& error){
      return false;
    }
  }
  return true;
}

base::Error ReadLabelsFile(const std::string& file_name) {
  if (!wav_labels.size()){
    std::ifstream file(file_name);
    std::string line;

    if (!file.is_open()){
      return NotFound;
    } else {
      while (std::getline(file, line)) {
        wav_labels.push_back(line);
      }
    }
    file.close();
  }

  return NoError;
}

void GetTopLabels(Tensor output, Int32 how_many_labels,
                  std::vector<Int32> *out_indices, std::vector<Real32> *out_scores) {
    const Tensor& unsorted_scores_tensor = output;
    auto unsorted_scores_flat = nn::utils::values<Real32>(unsorted_scores_tensor);
    std::vector<std::pair<Int32, Real32>> scores;
    scores.reserve(unsorted_scores_flat.size());

    for (size_t i = 0; i < unsorted_scores_flat.size(); ++i) {
        scores.push_back(std::pair<Int32, Real32>({i, unsorted_scores_flat[i]}));
    }

    std::sort(scores.begin(), scores.end(),
              [](const std::pair<Int32, Real32>& left,
                 const std::pair<Int32, Real32>& right) {
                  return left.second > right.second;
              });

    scores.resize(how_many_labels);
    std::vector<Int32> sorted_indices;
    std::vector<Real32> sorted_scores;
    for (size_t i = 0; i < scores.size(); ++i) {
        sorted_indices.push_back(scores[i].first);
        sorted_scores.push_back(scores[i].second);
    }
    *out_indices = sorted_indices;
    *out_scores = sorted_scores;
}

TEST(Benchmark, SpeechConv2D_With_Eval_On_SingleThread){
  using namespace nn::tensor;
  using namespace std;

  auto interpreter   = nNet_CreateInterpreterFromFile(BENCHMARK_WITH_MODEL);
  auto sample_rate   = tensor<Int32>({16000}, interpreter);
  auto tuple_inputs  = nNet_CreateCGates(interpreter, 2);
  auto stride_sample = 16000*0.030;

  ASSERT_TRUE(prepare(SOUND_FOR_BENCHMARK));
  ASSERT_FALSE(ReadLabelsFile(LABEL_FOR_BENCHMARK));

  nNet_AssignTensorToCGates(&tuple_inputs, "sample_rate", sample_rate, 1);

  for (size_t i = 0; i < wav_size; i += stride_sample){
    size_t        realsize{i + 16000 < wav_size? 16000: wav_size - i};
    vector<float> window(&wav_data.get()[i], &wav_data.get()[i] + realsize);

    auto signal = tensor<Real32>(rvalue(window), {16000, 1}, interpreter);

    // base::trace::console << "step " << i << " ";
    nNet_AssignTensorToCGates(&tuple_inputs, "signal", signal, 0);
    nNet_EvalGraph(interpreter, &tuple_inputs, [&](CGates outputs){
      std::vector<Int32> out_indices;
      std::vector<Real32> out_scores;

      GetTopLabels(outputs.tensors[0], wav_labels.size(), &out_indices, &out_scores);
      base::trace::console << "top label: " << wav_labels[out_indices[0]] 
                           << " top score: " << std::to_string(out_scores[0])
                           << base::trace::eol;
    });
  }

  EXPECT_TRUE(nNet_RemoveCGates(&tuple_inputs));
  EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
}

TEST(Benchmark, SpeechConv2D_With_Run_On_SingleThread){
  using namespace nn::tensor;
  using namespace std;

  auto interpreter   = nNet_CreateInterpreterFromFile(BENCHMARK_WITH_MODEL);
  auto tuple_inputs  = nNet_CreateCGates(interpreter, 2);
  auto stride_sample = 16000*0.030;

  EXPECT_TRUE(interpreter != NULL);
  ASSERT_TRUE(prepare(SOUND_FOR_BENCHMARK));
  ASSERT_FALSE(ReadLabelsFile(LABEL_FOR_BENCHMARK));

  for (size_t i = 0; i < wav_size; i += stride_sample){
    size_t        realsize{i + 16000 < wav_size? 16000: wav_size - i};
    vector<float> window(&wav_data.get()[i], &wav_data.get()[i] + realsize);

    auto signal = tensor<Real32>(rvalue(window), {16000, 1}, interpreter);
    auto sample_rate   = tensor<Int32>({16000}, interpreter);

    nNet_AssignTensorToCGates(&tuple_inputs, "signal", signal, 0);
    nNet_AssignTensorToCGates(&tuple_inputs, "sample_rate", sample_rate, 1);

    // base::trace::console << "call RunGraph at step " << i 
    //                      << base::trace::eol;
    nNet_RunGraph(interpreter, &tuple_inputs, [&](CGates outputs){
      std::vector<Int32> out_indices;
      std::vector<Real32> out_scores;

      GetTopLabels(outputs.tensors[0], wav_labels.size(), &out_indices, &out_scores);
      base::trace::console << "top label: " << wav_labels[out_indices[0]] 
                           << " top score: " << std::to_string(out_scores[0])
                           << base::trace::eol;
    });
  }

  EXPECT_TRUE(nNet_RemoveCGates(&tuple_inputs));
  EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
}

TEST(Benchmark, SpeechConv2D_With_Invoke_On_SingleThread){
  using namespace nn::tensor;
  using namespace std;

  auto interpreter   = nNet_CreateInterpreterFromFile(BENCHMARK_WITH_MODEL);
  auto tuple_inputs  = nNet_CreateCGates(interpreter, 2);
  auto stride_sample = 16000*0.030;

  EXPECT_TRUE(interpreter != NULL);
  ASSERT_TRUE(prepare(SOUND_FOR_BENCHMARK));
  ASSERT_FALSE(ReadLabelsFile(LABEL_FOR_BENCHMARK));

  for (size_t i = 0; i < wav_size; i += stride_sample){
    size_t        realsize{i + 16000 < wav_size? 16000: wav_size - i};
    vector<float> window(&wav_data.get()[i], &wav_data.get()[i] + realsize);

    auto signal = tensor<Real32>(rvalue(window), {16000, 1}, interpreter);
    auto sample_rate   = tensor<Int32>({16000}, interpreter);

    nNet_AssignTensorToCGates(&tuple_inputs, "signal", signal, 0);
    nNet_AssignTensorToCGates(&tuple_inputs, "sample_rate", sample_rate, 1);

    nNet_InvokeGraph(interpreter, &tuple_inputs);
  }

  nNet_FlushGraph(interpreter, [&](CGates outputs){
    std::vector<Int32> out_indices;
    std::vector<Real32> out_scores;

    GetTopLabels(outputs.tensors[0], wav_labels.size(), &out_indices, &out_scores);
    base::trace::console << "top label: " << wav_labels[out_indices[0]] 
                         << " top score: " << std::to_string(out_scores[0])
                         << base::trace::eol;
  });
  EXPECT_TRUE(nNet_RemoveCGates(&tuple_inputs));
  EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
}

TEST(Benchmark, SpeechConv2D_With_Eval_On_MultiThread){
  using namespace nn::tensor;
  using namespace std;

  auto interpreter   = nNet_CreateInterpreterFromFile(BENCHMARK_WITH_MODEL);
  auto sample_rate   = tensor<Int32>({16000}, interpreter);
  auto tuple_inputs  = nNet_CreateCGates(interpreter, 2);
  auto stride_sample = 16000*0.030;

  ASSERT_TRUE(prepare(SOUND_FOR_BENCHMARK));
  ASSERT_FALSE(ReadLabelsFile(LABEL_FOR_BENCHMARK));
  nNet_AssignTensorToCGates(&tuple_inputs, "sample_rate", sample_rate, 1);

  base::Thread ([&](base::Thread&, base::Tuple) -> base::Tuple{
    for (size_t i = 0; i < wav_size; i += stride_sample){
      size_t        realsize{i + 16000 < wav_size? 16000: wav_size - i};
      vector<float> window(&wav_data.get()[i], &wav_data.get()[i] + realsize);

      auto signal = tensor<Real32>(rvalue(window), {16000, 1}, interpreter);

      nNet_AssignTensorToCGates(&tuple_inputs, "signal", signal, 0);
      nNet_EvalGraph(interpreter, &tuple_inputs, [&](CGates outputs){
        std::vector<Int32> out_indices;
        std::vector<Real32> out_scores;

        GetTopLabels(outputs.tensors[0], wav_labels.size(), &out_indices, &out_scores);
        base::trace::console << "top label: " << wav_labels[out_indices[0]] 
                             << " top score: " << std::to_string(out_scores[0])
                             << base::trace::eol;
      });
    }

    return base::Tuple::make();
  }, true);


  base::Boundary bound{[](){}, [&](){
    EXPECT_TRUE(nNet_RemoveCGates(&tuple_inputs));
    EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
  }};
}

TEST(Benchmark, SpeechConv2D_With_Run_On_MultiThread){
  using namespace nn::tensor;
  using namespace std;

  auto interpreter   = nNet_CreateInterpreterFromFile(BENCHMARK_WITH_MODEL);
  
  auto tuple_inputs  = nNet_CreateCGates(interpreter, 2);
  auto stride_sample = 16000*0.030;

  EXPECT_TRUE(interpreter != NULL);
  ASSERT_TRUE(prepare(SOUND_FOR_BENCHMARK));
  ASSERT_FALSE(ReadLabelsFile(LABEL_FOR_BENCHMARK));

  base::Thread ([&](base::Thread&, base::Tuple) -> base::Tuple{
    for (size_t i = 0; i < wav_size; i += stride_sample){
      size_t        realsize{i + 16000 < wav_size? 16000: wav_size - i};
      vector<float> window(&wav_data.get()[i], &wav_data.get()[i] + realsize);

      auto signal = tensor<Real32>(rvalue(window), {16000, 1}, interpreter);
      auto sample_rate   = tensor<Int32>({16000}, interpreter);

      nNet_AssignTensorToCGates(&tuple_inputs, "sample_rate", sample_rate, 1);
      nNet_AssignTensorToCGates(&tuple_inputs, "signal", signal, 0);
      nNet_RunGraph(interpreter, &tuple_inputs, [&](CGates outputs){
        std::vector<Int32> out_indices;
        std::vector<Real32> out_scores;

        GetTopLabels(outputs.tensors[0], wav_labels.size(), &out_indices, &out_scores);
        base::trace::console << "top label: " << wav_labels[out_indices[0]] 
                             << " top score: " << std::to_string(out_scores[0])
                             << base::trace::eol;
      });
    }

    nNet_FlushGraph(interpreter, nullptr);
    return base::Tuple::make();
  }, true);

  EXPECT_TRUE(nNet_RemoveCGates(&tuple_inputs));
  EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
}

TEST(Benchmark, SpeechConv2D_With_Invoke_On_MultiThread){
  using namespace nn::tensor;
  using namespace std;

  auto interpreter   = nNet_CreateInterpreterFromFile(BENCHMARK_WITH_MODEL);
  auto tuple_inputs  = nNet_CreateCGates(interpreter, 2);
  auto stride_sample = 16000*0.030;

  EXPECT_TRUE(interpreter != NULL);
  ASSERT_TRUE(prepare(SOUND_FOR_BENCHMARK));
  ASSERT_FALSE(ReadLabelsFile(LABEL_FOR_BENCHMARK));

  base::Thread ([&](base::Thread&, base::Tuple) -> base::Tuple{
    for (size_t i = 0; i < wav_size; i += stride_sample){
      size_t        realsize{i + 16000 < wav_size? 16000: wav_size - i};
      vector<float> window(&wav_data.get()[i], &wav_data.get()[i] + realsize);

      auto signal = tensor<Real32>(rvalue(window), {16000, 1}, interpreter);
      auto sample_rate   = tensor<Int32>({16000}, interpreter);

      nNet_AssignTensorToCGates(&tuple_inputs, "sample_rate", sample_rate, 1);
      nNet_AssignTensorToCGates(&tuple_inputs, "signal", signal, 0);
      nNet_InvokeGraph(interpreter, &tuple_inputs);
    }

    nNet_FlushGraph(interpreter, [&](CGates outputs){
      std::vector<Int32> out_indices;
      std::vector<Real32> out_scores;

      GetTopLabels(outputs.tensors[0], wav_labels.size(), &out_indices, &out_scores);
      base::trace::console << "top label: " << wav_labels[out_indices[0]] 
                           << " top score: " << std::to_string(out_scores[0])
                           << base::trace::eol;
    });
    return base::Tuple::make();
  }, true);

  base::Boundary bound{[](){}, [&](){
    EXPECT_TRUE(nNet_RemoveCGates(&tuple_inputs));
    EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
  }};
}
#endif

#if CHECK_CASE_BENCHMARK_COL2IM || (NDEBUG && !CHECK_CASE)
#include <libnn/devices/generic/math.h>

using namespace nn::generic::math::conv2d;
using namespace nn::utils;

#if CHECK_CASE_COL2IM_OF_NCHW
TEST(Benchmark, Col2Im_Of_NCHW){
}
#endif

#if CHECK_CASE_COL2IM_OF_NHWC
TEST(Benchmark, Col2Im_Of_NHWC){
}
#endif
#endif

#if CHECK_CASE_BENCHMARK_IM2COL || (NDEBUG && !CHECK_CASE)
#include <libnn/devices/generic/math.h>
#include <libnn/tensor.h>

using namespace nn::generic::math::conv2d;
using namespace nn::utils;

#if CHECK_CASE_IM2COL_OF_NCHW || !CHECK_CASE
TEST(Benchmark, Im2Col_Of_NCHW){
  int h = 512, w = 512;

  for (auto i = 0; i < MAX_STEP_OF_IM2COL; ++i){
    base::trace::console << "step " << i << ": "
                         << "(h, w) = " << "(" << h << "," << w << ")" 
                         << base::trace::eol;

    Interpreter interpreter = nNet_CreateInterpreter(nullptr);
    auto input  = nn::tensor::zeros({1, 64, h, w}, Real32_t, interpreter);
    auto filter = nn::tensor::zeros({10, 4, 64, 64}, Real32_t, interpreter);

    Tensor* result_same  = nullptr;
    Tensor* result_valid = nullptr;

    EXPECT_TRUE(nchw::im2col<Real32>(interpreter, input, filter, result_valid, 
                        {1, 1}, {1, 1}, "VALID", 0));

    EXPECT_TRUE(nchw::im2col<Real32>(interpreter, input, filter, result_same, 
                        {1, 1}, {1, 1}, "SAME", 0));

    if (result_same)
      free(result_same);
    if (result_valid)
      free(result_valid);
    nNet_RemoveInterpreter(interpreter);

    h *= 2;
    w *= 2;
  }
}
#endif

#if CHECK_CASE_IM2COL_OF_NHWC || !CHECK_CASE
TEST(Benchmark, Im2Col_Of_NHWC){
  int h = 16, w = 16;

  for (auto i = 0; i < MAX_STEP_OF_IM2COL; ++i){
    base::trace::console << "step " << i << ": "
                         << "(h, w) = " << "(" << h << "," << w << ")" 
                         << base::trace::eol;

    Interpreter interpreter = nNet_CreateInterpreter(nullptr);
    auto input  = nn::tensor::zeros({1, h, w, 64}, Real32_t, interpreter);
    auto filter = nn::tensor::zeros({10, 4, 64, 64}, Real32_t, interpreter);

    Tensor* result_same  = nullptr;
    Tensor* result_valid = nullptr;

    EXPECT_TRUE(nhwc::im2col<Real32>(interpreter, input, filter, result_valid, 
                        {1, 1}, {1, 1}, "VALID", 0));

    EXPECT_TRUE(nhwc::im2col<Real32>(interpreter, input, filter, result_same, 
                        {1, 1}, {1, 1}, "SAME", 0));

    if (result_same)
      free(result_same);
    if (result_valid)
      free(result_valid);
    nNet_RemoveInterpreter(interpreter);

    h *= 2;
    w *= 2;
  }
}
#endif
#endif

#ifndef TEST_ALLTESTS_CPP_
#include <libbase/all.hpp>
#include <signal.h>

static void handler(int sig, siginfo_t *dont_care, void* dont_care_either){
  base::trace::backtrace(30);
  exit(-1);
}

int main(int argc, char** argv){
  using namespace base;

 #if APPLE || USE_BASE_UNITTEST
  base::testing::init(argc, (const char**)argv);
 #else
  ::testing::InitGoogleTest(&argc, argv);
 #endif

 #if NDEBUG
  struct sigaction sa;

  memset(&sa, 0, sizeof(struct sigaction));
  sigemptyset(&sa.sa_mask);

  sa.sa_flags     = SA_NODEFER;
  sa.sa_sigaction = handler;

  sigaction(SIGSEGV, &sa, NULL); /* ignore whether it works or not */
 #endif

  base::config::start(argv, argc);
  base::config::set("console", base::Auto{}.set(std::string{"console.txt"}));
  base::config::set("monitor", base::Auto{}.set(true));

  base::config::set("metal", base::Auto{}.set(std::string{"bundle"}));
  base::config::set("metal.file", base::Auto{}.set(std::string{"metal.metalib"}));

  base::config::set("opencl", base::Auto{}.set(std::string{"auto"}));
  base::config::set("opencl.file", base::Auto{}.set(std::string{"opencl.clbin"}));

  base::config::set("cuda", base::Auto{}.set(std::string{"file"}));
  base::config::set("cuda.file", base::Auto{}.set(std::string{"opencl.cubin"}));

  if (config::get("metal").type().get() != typeid(std::string))
    return -1;
  nNet_LoadEverything();
  return base::config::finish(RUN_ALL_TESTS(), [](){ nNet_UnloadEverything(); });
}
#endif // TEST_ALLTESTS_CPP_
#endif // BOOST_TEST_MAIN
