
#ifndef BOOST_TEST_MAIN
#include <unistd.h>
#include "all.hpp"

#if APPLE || USE_BASE_UNITTEST
#include <libbase/unittest.hpp>
#else
#include <gtest/gtest.h>
#endif

#define LOOP 2
#define SLOWDOWN 2

#if CHECK_CASE_STREMING_SPEECHAI_CONV2D || CHECK_CASE_STREMING ||              \
    (NDEBUG && !CHECK_CASE) || CHECK_CASE_FAIL_STREAMING
TEST(Streaming, SpeechConv2D_With_CPP0) {
  auto interpreter = nNet_CreateInterpreterFromFile("tests/models/speechai.pb");
  auto gate = nNet_CreateCGates(interpreter, 1);
  EXPECT_TRUE(interpreter != NULL);

  for (auto i = 0; i < LOOP; ++i){
    auto yes =
      nn::tensor::tensor<char*>({(char*)"tests/samples/yes.wav"}, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "wav_data", yes, 0));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [&](CGates result) {
   #if NDEBUG
    auto expected = nn::tensor::tensor<Real32>(
        {3.751737587e-09, 0.001292244764, 0.9849011898, 0.006376835983,
         2.366850538e-08, 0.0002707100939, 0.007051775698, 3.125795774e-05,
         7.187461648e-08, 9.557074918e-06, 1.295448101e-06, 6.507908256e-05},
        {1, 12}, result.interpreter);
   #else
    auto expected = nn::tensor::tensor<Real32>(
        {3.751854383e-09, 0.001292274334, 0.9849008322, 0.006377107464,
         2.36694202e-08, 0.0002707167296, 0.007051826455, 3.125862713e-05,
         7.187691153e-08, 9.55728683e-06, 1.295488005e-06, 6.508054503e-05},
        {1, 12}, result.interpreter);
   #endif
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i){
    auto yes =
      nn::tensor::tensor<char*>({(char*)"tests/samples/yes.wav"}, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "wav_data", yes, 0));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  nNet_FlushGraph(interpreter, 0, [&](std::vector<Tensor> outputs) {
   #if NDEBUG
    auto expected = nn::tensor::tensor<Real32>(
        {3.751737587e-09, 0.001292244764, 0.9849011898, 0.006376835983,
         2.366850538e-08, 0.0002707100939, 0.007051775698, 3.125795774e-05,
         7.187461648e-08, 9.557074918e-06, 1.295448101e-06, 6.507908256e-05},
        {1, 12}, interpreter);
   #else
    auto expected = nn::tensor::tensor<Real32>(
        {3.751854383e-09, 0.001292274334, 0.9849008322, 0.006377107464,
         2.36694202e-08, 0.0002707167296, 0.007051826455, 3.125862713e-05,
         7.187691153e-08, 9.55728683e-06, 1.295488005e-06, 6.508054503e-05},
        {1, 12}, interpreter);
   #endif

    for (auto i = 0; i < cast_(i, outputs.size()); ++i)
      EXPECT_EQ(nn::utils::diff(outputs[i], expected), 0);

    EXPECT_TRUE(nNet_RemoveCGates(&gate));
    EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
  });
}

TEST(Streaming, SpeechConv2D_With_CPP1) {
  auto interpreter = nNet_CreateInterpreterFromFile("tests/models/speechai.pb");
  auto gate = nNet_CreateCGates(interpreter, 1);

  EXPECT_TRUE(interpreter != NULL);
  for (auto i = 0; i < LOOP; ++i) {
    auto yes =
      nn::tensor::tensor<char*>({(char*)"tests/samples/no.wav"}, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "wav_data", yes, 0));
    nNet_InvokeGraph(interpreter, &gate);
    sleep(SLOWDOWN);  // <-- slowdown 2s to make a system hange
  }

  EXPECT_FALSE(!nNet_FlushGraph(interpreter, 0, [&](std::vector<Tensor> outputs) {
   #if NDEBUG
    auto expected = nn::tensor::tensor<Real32>(
        {3.751737587e-09, 0.001292244764, 0.9849011898, 0.006376835983,
         2.366850538e-08, 0.0002707100939, 0.007051775698, 3.125795774e-05,
         7.187461648e-08, 9.557074918e-06, 1.295448101e-06, 6.507908256e-05},
        {1, 12}, interpreter);
   #else
    auto expected = nn::tensor::tensor<Real32>(
        {3.751854383e-09, 0.001292274334, 0.9849008322, 0.006377107464,
         2.36694202e-08, 0.0002707167296, 0.007051826455, 3.125862713e-05,
         7.187691153e-08, 9.55728683e-06, 1.295488005e-06, 6.508054503e-05},
        {1, 12}, interpreter);
   #endif
    for (auto& result: outputs)
      EXPECT_EQ(nn::utils::diff(result, expected), 0);

    EXPECT_TRUE(nNet_RemoveCGates(&gate));
    EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
  }));

  EXPECT_TRUE(nNet_RemoveCGates(&gate));
  EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
}

TEST(Streaming, SpeechConv2D_With_CPP2) {
  for (auto j = 0; j < LOOP; ++j){
    auto interpreter = nNet_CreateInterpreterFromFile("tests/models/speechai.pb");
    auto gate = nNet_CreateCGates(interpreter, 1);
    EXPECT_TRUE(interpreter != NULL);

    for (auto i = 0; i < LOOP; ++i) {
      auto yes = nn::tensor::tensor<char*>({(char*)"tests/samples/yes.wav"},
                                         interpreter);

      EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "wav_data", yes, 0));
      EXPECT_TRUE(nNet_InvokeGraph(interpreter, &gate));
      sleep(SLOWDOWN);  // <-- slowdown 2s to make a system hange
    }

    EXPECT_TRUE(!nNet_FlushGraph(interpreter, 0, [&](std::vector<Tensor> outputs) {
     #if NDEBUG
      auto expected = nn::tensor::tensor<Real32>(
          {3.751737587e-09, 0.001292244764, 0.9849011898, 0.006376835983,
           2.366850538e-08, 0.0002707100939, 0.007051775698, 3.125795774e-05,
           7.187461648e-08, 9.557074918e-06, 1.295448101e-06, 6.507908256e-05},
          {1, 12}, interpreter);
     #else
      auto expected = nn::tensor::tensor<Real32>(
        {3.751854383e-09, 0.001292274334, 0.9849008322, 0.006377107464,
         2.36694202e-08, 0.0002707167296, 0.007051826455, 3.125862713e-05,
         7.187691153e-08, 9.55728683e-06, 1.295488005e-06, 6.508054503e-05},
        {1, 12}, interpreter);
     #endif

      for (auto& result : outputs)
        EXPECT_EQ(nn::utils::diff(result, expected), 0);

      EXPECT_TRUE(nNet_RemoveCGates(&gate));
      EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
    }));
  }
}

TEST(Streaming, SpeechConv2D_With_CPP4) {
  for (auto j = 0; j < LOOP; ++j){
    auto interpreter =
        nNet_CreateInterpreterFromFile("tests/models/speechai.pb");
    auto gate = nNet_CreateCGates(interpreter, 1);

    EXPECT_TRUE(interpreter != NULL);
      for (auto i = 0; i < LOOP; ++i) {
        auto yes = nn::tensor::tensor<char*>({(char*)"tests/samples/yes.wav"},
                                         interpreter);

        EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "wav_data", yes, 0));
        EXPECT_TRUE(!nNet_RunGraph(interpreter, &gate, [&](CGates outputs){

     #if NDEBUG
      auto expected = nn::tensor::tensor<Real32>(
          {3.751737587e-09, 0.001292244764, 0.9849011898, 0.006376835983,
           2.366850538e-08, 0.0002707100939, 0.007051775698, 3.125795774e-05,
           7.187461648e-08, 9.557074918e-06, 1.295448101e-06, 6.507908256e-05},
          {1, 12}, interpreter);
     #else
      auto expected = nn::tensor::tensor<Real32>(
        {3.751854383e-09, 0.001292274334, 0.9849008322, 0.006377107464,
         2.36694202e-08, 0.0002707167296, 0.007051826455, 3.125862713e-05,
         7.187691153e-08, 9.55728683e-06, 1.295488005e-06, 6.508054503e-05},
        {1, 12}, interpreter);
     #endif

      EXPECT_EQ(nn::utils::diff(outputs.tensors[0], expected), 0);
      }));
      sleep(SLOWDOWN);  // <-- slowdown 2s to make a system hange
    }

    EXPECT_TRUE(!nNet_FlushGraph(interpreter, CYield{nullptr}));
    EXPECT_TRUE(nNet_RemoveCGates(&gate));
    EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
  }
}
#endif

#ifndef TEST_ALLTESTS_CPP_
#include <libbase/all.hpp>
#include <signal.h>

static void handler(int sig, siginfo_t *dont_care, void* dont_care_either){
  base::trace::backtrace(30);
  exit(-1);
}

int main(int argc, char** argv){
  using namespace base;

 #if APPLE || USE_BASE_UNITTEST
  base::testing::init(argc, (const char**)argv);
 #else
  ::testing::InitGoogleTest(&argc, argv);
 #endif

  struct sigaction sa;

  memset(&sa, 0, sizeof(struct sigaction));
  sigemptyset(&sa.sa_mask);

  sa.sa_flags     = SA_NODEFER;
  sa.sa_sigaction = handler;

  sigaction(SIGSEGV, &sa, NULL); /* ignore whether it works or not */

  base::config::start(argv, argc);
  base::config::set("console", base::Auto{}.set(std::string{"console.txt"}));
  base::config::set("monitor", base::Auto{}.set(true));

  base::config::set("metal", base::Auto{}.set(std::string{"bundle"}));
  base::config::set("metal.file", base::Auto{}.set(std::string{"metal.metalib"}));

  base::config::set("opencl", base::Auto{}.set(std::string{"auto"}));
  base::config::set("opencl.file", base::Auto{}.set(std::string{"opencl.clbin"}));

  base::config::set("cuda", base::Auto{}.set(std::string{"file"}));
  base::config::set("cuda.file", base::Auto{}.set(std::string{"opencl.cubin"}));

  if (config::get("metal").type().get() != typeid(std::string))
    return -1;
  nNet_LoadEverything();
  return base::config::finish(RUN_ALL_TESTS(), [](){ nNet_UnloadEverything(); });
}
#endif  // TEST_ALLTESTS_CPP_
#endif  // BOOST_TEST_MAIN