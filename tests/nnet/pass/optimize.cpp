#ifndef BOOST_TEST_MAIN
#include <libbase/all.hpp>

#if APPLE || USE_BASE_UNITTEST
#include <libbase/unittest.hpp>
#else
#include <gtest/gtest.h>
#endif

#include "all.hpp"

#define EPOCH 100

/* @NOTE: f(x) = x*x -4*x + 4 */
double f(double x){
  return x*x -4*x + 4;
}

double dx(double x){
  return 2*x - 4;
}

/* @NOTE: f(x1, x2) = x1^2 + x2^2 + x1*x2 */
double f(double x1, double x2){
  return pow(x1, 2.0) + pow(x2, 2.0) + x2*x1;
};

double dx1(double x1, double x2){
  return (2.0*x1 + x2);
};

double dx2(double x1, double x2){
  return 2.0*x2 + x1;
};

#if CHECK_CASE_GRADIENT_DESCENT || !CHECK_CASE || CHECK_OPTIMIZE
TEST(Math, GradientDescent){
  auto x1 = 10.0, x2 = 5.0;

  for (auto i = 0; i < EPOCH; i++){
    auto tmp_x1 = x1 - 0.01*dx(x1);
    x1 = tmp_x1;
  }

  base::trace::console << "min(f(x)) = " << f(x1) << " at " << x1 
                       << base::trace::eol;

  for (auto i = 0; i < EPOCH; i++){
    auto tmp_x1 = x1 - 0.01*dx1(x1, x2);
    auto tmp_x2 = x2 - 0.01*dx2(x1, x2);
    x1 = tmp_x1;
    x2 = tmp_x2;
  }

  base::trace::console << "min(f(x1, x2)) = " << f(x1, x2) 
                       << " at (" << x1 << ", " << x2 << ")"
                       << base::trace::eol;
}

TEST(Math, Momentum){
}

TEST(Optimize, GradientDescend){
}
#endif

#if CHECK_CASE_STOSTAIC_GRADIENT_DESCENT || !CHECK_CASE || CHECK_OPTIMIZE
TEST(Math, StostaicGradienDescent){
}

TEST(Optimize, SGD){
}
#endif

#if CHECK_CASE_ADAGRAD || !CHECK_CASE
TEST(Math, AdaGrad){  
}

TEST(Optimize, AdaGrad){
}
#endif

#if CHECK_CASE_ADAM || !CHECK_CASE || CHECK_OPTIMIZE
TEST(Math, Adam){
  auto alpha = 0.01;
  auto beta_1 = 0.9;
  auto beta_2 = 0.999;
  auto epsilon = 1e-8;

  auto theta_0 = 0.f;
  auto theta_1 = 0.f;
  auto m_t = 0.f;
  auto v_t = 0.f;

  for (auto t = 1; t <= EPOCH; t++){
    auto g_t = dx(theta_0);

    m_t = beta_1 * m_t + (1.f - beta_1) * g_t;
    v_t = beta_2 * v_t + (1.f - beta_2) * g_t * g_t;

    auto m_cap = m_t / (1.f - pow(beta_1, t));
    auto v_cap = v_t / (1.f - pow(beta_2, t));
    auto theta_0_prev = theta_0;

    theta_0 = theta_0 - alpha * m_cap / (sqrt(v_cap) + epsilon);
    if (theta_0 != theta_0_prev)
      continue;

    base::trace::console << "finish at " << t << base::trace::eol;
    break;
  }

  base::trace::console << "min(f(x)) = " << f(theta_0) << " at " << theta_0 
                       << base::trace::eol;

  auto m_t_0 = 0.f;
  auto v_t_0 = 0.f;
  auto m_t_1 = 0.f;
  auto v_t_1 = 0.f;

  for (auto t = 1; t <= EPOCH; t++){
    auto g_t_0 = dx1(theta_0, theta_1);
    auto g_t_1 = dx2(theta_0, theta_1);

    m_t_0 = beta_1 * m_t_0 + (1.f - beta_1) * g_t_0;
    v_t_0 = beta_2 * v_t_0 + (1.f - beta_2) * g_t_0 * g_t_0;
    m_t_1 = beta_1 * m_t_1 + (1.f - beta_1) * g_t_0;
    v_t_1 = beta_2 * v_t_1 + (1.f - beta_2) * g_t_0 * g_t_0;

    auto m_cap_0 = m_t_0 / (1.f - pow(beta_1, t));
    auto v_cap_0 = v_t_0 / (1.f - pow(beta_2, t));
    auto m_cap_1 = m_t_1 / (1.f - pow(beta_1, t));
    auto v_cap_1 = v_t_1 / (1.f - pow(beta_2, t));
    auto theta_0_prev = theta_0;
    auto theta_1_prev = theta_1;

    theta_0 = theta_0 - alpha * m_cap_0 / (sqrt(v_cap_0) + epsilon);
    theta_1 = theta_1 - alpha * m_cap_1 / (sqrt(v_cap_1) + epsilon);

    if ((theta_0 != theta_0_prev) || (theta_1 != theta_1_prev))
      continue;

    base::trace::console << "finish at " << t << base::trace::eol;
    break;
  }
}

TEST(Optimize, Adam){
}
#endif

#if CHECK_CASE_ESTIMATE_MAXIMIZE || !CHECK_CASE || CHECK_OPTIMIZE
TEST(Math, EstimateMaximize){
}

TEST(Optimize, EstimateMaximize){
}
#endif

#ifndef TEST_ALLTESTS_CPP_
#include <libbase/all.hpp>
#include <signal.h>

static void handler(int sig, siginfo_t *dont_care, void* dont_care_either){
  base::trace::backtrace(30);
  exit(-1);
}

int main(int argc, char** argv){
  using namespace base;

 #if APPLE || USE_BASE_UNITTEST
  base::testing::init(argc, (const char**)argv);
 #else
  ::testing::InitGoogleTest(&argc, argv);
 #endif

  struct sigaction sa;

  memset(&sa, 0, sizeof(struct sigaction));
  sigemptyset(&sa.sa_mask);

  sa.sa_flags     = SA_NODEFER;
  sa.sa_sigaction = handler;

  sigaction(SIGSEGV, &sa, NULL); /* ignore whether it works or not */

  base::config::start(argv, argc);
  base::config::set("console", base::Auto{}.set(std::string{"console.txt"}));
  base::config::set("monitor", base::Auto{}.set(true));

  base::config::set("metal", base::Auto{}.set(std::string{"bundle"}));
  base::config::set("metal.file", base::Auto{}.set(std::string{"metal.metalib"}));

  base::config::set("opencl", base::Auto{}.set(std::string{"auto"}));
  base::config::set("opencl.file", base::Auto{}.set(std::string{"opencl.clbin"}));

  base::config::set("cuda", base::Auto{}.set(std::string{"file"}));
  base::config::set("cuda.file", base::Auto{}.set(std::string{"opencl.cubin"}));

  if (config::get("metal").type().get() != typeid(std::string))
    return -1;
  nNet_LoadEverything();
  return base::config::finish(RUN_ALL_TESTS(), [](){ nNet_UnloadEverything(); });
}
#endif // TEST_ALLTESTS_CPP_
#endif // BOOST_TEST_MAIN
