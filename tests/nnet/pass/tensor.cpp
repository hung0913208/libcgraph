#ifndef BOOST_TEST_MAIN
#include "all.hpp"

#if APPLE || USE_BASE_UNITTEST
#include <libbase/unittest.hpp>

/* @NOTE: we needs implement these function to support printing everything to
 * */
namespace std {
std::string to_string(Tensor &tensor) { return nn::utils::print(tensor); }

std::string to_string(Shape &shape) { return nn::utils::print(shape); }

std::string to_string(Ordinate &ordinate) { return nn::utils::print(ordinate); }
} // namespace std
#else
#include <gtest/gtest.h>
#endif
#include <libbase/all.hpp>

#define IF(condition, tests)                                                   \
  if (condition)                                                               \
    tests();                                                                   \
  else                                                                         \
    DoNothing.reason("can't pass condition")
#define TRUE(condition) condition == true

namespace nn {
namespace tensor {
std::size_t size(int *dims, int ndims) {
  auto result = 1;

  for (auto i = 0; i < ndims; ++i)
    result *= dims[i];
  return result;
}
} // namespace tensor
} // namespace nn

#if CHECK_CASE_TENSOR_PLAYGROUND || !CHECK_CASE 
TEST(NetTensor, Playground) {
  std::vector<int> dims{1, 2, 3};
  std::vector<int> pos{0, 0, 0};
  std::vector<int> begin{1, 2};
  std::vector<int> end{1, 2};

  nn::InterpreterW interpreter{""};
  for (auto &device : interpreter.devices()) {
    if (!interpreter.use(device))
      continue;
    info(DoNothing).reason("interpreter will use device " + std::get<0>(device));

    interpreter([&]() {
      auto nilshape = nNet_CreateTensorShapeS(0);
      auto shape0 = nNet_CreateTensorShapeS(3, 1, 2, 3);
      auto shape1 = nNet_CreateTensorShapeW(3, const_cast<int *>(dims.data()));
      auto shape2 = nNet_CreateTensorShapeS(1, 3);

      auto niltensor0 = nNet_CreateTensorS();
      auto niltensor1 = nNet_CreateTensorS();
      auto tensor0 = nNet_CreateTensorS();
      auto tensor1 = nNet_CreateTensorW(shape1);

      auto ordinate0 = nNet_CreateOrdinateS(nullptr, 3, 0, 0, 0);
      auto ordinate1 = nNet_CreateOrdinateW1(tensor1, (int *)pos.data(), 3);
      auto ordinate2 = nNet_CreateOrdinateW2(tensor1, (int *)begin.data(), 2,
                                             (int *)end.data(), 2);
      /* @NOTE: mac dinh khi khoi tao cac doi tuong deu duoc cap phat va
       * co the dung ngay lap tuc */

      EXPECT_FALSE(nn::utils::outdate(nilshape));
      EXPECT_FALSE(nn::utils::outdate(shape0));
      EXPECT_FALSE(nn::utils::outdate(shape1));
      EXPECT_FALSE(nn::utils::outdate(niltensor0));
      EXPECT_FALSE(nn::utils::outdate(niltensor1));
      EXPECT_FALSE(nn::utils::outdate(tensor0));
      EXPECT_FALSE(nn::utils::outdate(tensor1));
      EXPECT_FALSE(nn::utils::outdate(ordinate0));
      EXPECT_FALSE(nn::utils::outdate(ordinate1));
      EXPECT_FALSE(nn::utils::outdate(ordinate2));

      /* @NOTE: khoi tao cac thong tin co ban cho 1 tensor bang 2 phuong phap
       * - Dung CreateTensorW de tao khoi tao lai 1 tensor va cau hinh theo
       * shape
       * - Dung cac ham SetTensorShape va SetTensorNumT de gan cac thuoc tinh
       * cho 1 tensor rong
       */

      EXPECT_TRUE(nNet_SetTensorNumT(tensor0, Real32_t) == true);
      EXPECT_TRUE(nNet_SetTensorShape(tensor0, shape0) == true);
      EXPECT_TRUE(nNet_SetTensorNumT(tensor1, Real64_t) == true);

      /* @NOTE: lay cac thong tin tu tensor hien tai: shape, number type,
       * real value */
      EXPECT_EQ(nNet_GetTensorNumT(tensor0), Real32_t);
      EXPECT_EQ(nNet_GetTensorNumT(tensor1), Real64_t);

      EXPECT_TRUE(nNet_GetTensorShape(tensor1, nilshape) == true);
      EXPECT_TRUE(nNet_GetShapeSize(nilshape) ==
                  (int)nn::tensor::size(dims.data(), dims.size()));

      IF(TRUE(nNet_GetTensorShape(tensor0, nilshape)), [&]() {
        auto realshape = nn::utils::values(nilshape);

        EXPECT_EQ(realshape.size(), dims.size());
        EXPECT_EQ(realshape, dims);
      });

      IF(TRUE(nNet_GetTensorShape(tensor0, nilshape)), [&]() {
        auto realshape = nn::utils::values(nilshape);

        EXPECT_EQ(realshape.size(), dims.size());
        EXPECT_EQ(realshape, dims);
      });

      EXPECT_TRUE(nNet_SetSubTensor(tensor0,
                                    nNet_CreateOrdinateW1(tensor0, nullptr, 0),
                                    nn::tensor::ones(dims, Real32_t)) == true);

      EXPECT_TRUE(nNet_GetSubTensor(tensor0, ordinate0, niltensor0) == true);
      EXPECT_TRUE(nNet_GetSubTensor(tensor0, ordinate1, niltensor0) == true);
      EXPECT_TRUE(nNet_GetSubTensor(tensor0, ordinate2, niltensor0) == true);

      IF(TRUE(nNet_GetSubTensor(
             tensor0, nNet_CreateOrdinateW1(tensor0, nullptr, 0), niltensor0)),
         [&]() {
           auto realtensor = nn::utils::values<Real32>(niltensor0);

           EXPECT_EQ(realtensor.size(),
                     std::size_t(nn::tensor::size(dims.data(), dims.size())));
         });

      EXPECT_TRUE(nNet_GetSubTensor(niltensor0, ordinate2, niltensor1) == true);
      EXPECT_TRUE(nNet_SetSubTensor(tensor1,
                                    nNet_CreateOrdinateW1(tensor1, nullptr, 0),
                                    nn::tensor::zeros(dims, Real64_t)) == true);
      EXPECT_TRUE((niltensor0 = nNet_ClearTensor(niltensor0)) != nullptr);
      IF(TRUE(nNet_GetSubTensor(
             tensor1, nNet_CreateOrdinateW1(tensor1, nullptr, 0), niltensor0)),
         [&]() {
           auto realtensor = nn::utils::values<Real64>(niltensor0);

           EXPECT_EQ(realtensor.size(),
                     nn::tensor::size(dims.data(), dims.size()));
         });

      EXPECT_TRUE(nNet_SetTensorShape(niltensor1, shape2) == true);
      EXPECT_TRUE(nNet_SetTensorNumT(niltensor0, Real64_t) == true);
      EXPECT_TRUE(nNet_SetTensorNumT(niltensor0, Real32_t) == true);
      EXPECT_TRUE(nNet_SetTensorNumT(niltensor0, Real64_t) == true);
      EXPECT_TRUE(nNet_SetTensorNumT(niltensor1, Real64_t) == true);
      EXPECT_TRUE(nNet_SetTensorNumT(niltensor1, Real32_t) == true);
      EXPECT_TRUE(nNet_SetTensorNumT(niltensor1, Real64_t) == true);
      EXPECT_TRUE(nNet_SetTensorNumT(tensor0, Real64_t) == true);
      EXPECT_TRUE(nNet_SetTensorNumT(tensor1, Real64_t) == true);
      EXPECT_TRUE(nNet_SetTensorNumT(tensor1, Real32_t) == true);
      EXPECT_TRUE(nNet_SetTensorNumT(tensor0, Real32_t) == true);
      EXPECT_TRUE(nNet_SetTensorNumT(tensor1, Real64_t) == true);

      /*  @NOTE: sau khi xoa object thi object do khong dung duoc nua */
      EXPECT_EQ((ordinate0 = nNet_RemoveOrdinate(ordinate0)), nullptr);
      EXPECT_EQ((ordinate1 = nNet_RemoveOrdinate(ordinate1)), nullptr);
      EXPECT_EQ((ordinate2 = nNet_RemoveOrdinate(ordinate1)), nullptr);

      IF(TRUE(nn::utils::outdate(ordinate0)), [&]() {
        EXPECT_FALSE(nNet_GetSubTensor(tensor0, ordinate0, niltensor0) == true);
      });

      IF(TRUE(nn::utils::outdate(ordinate1)), [&]() {
        EXPECT_FALSE(nNet_GetSubTensor(tensor0, ordinate1, niltensor0) == true);
      });

      IF(TRUE(nn::utils::outdate(ordinate2)), [&]() {
        EXPECT_FALSE(nNet_GetSubTensor(tensor0, ordinate2, niltensor0) == true);
      });

      EXPECT_EQ((shape0 = nNet_RemoveTensorShape(shape0)), nullptr);
      EXPECT_EQ((shape1 = nNet_RemoveTensorShape(shape1)), nullptr);

      IF(TRUE(nn::utils::outdate(shape0)), [&]() {
        EXPECT_FALSE(nNet_SetTensorShape(tensor0, shape0) == true);
        EXPECT_FALSE(nNet_SetTensorShape(tensor1, shape0) == true);
      });

      IF(TRUE(nn::utils::outdate(shape1)), [&]() {
        EXPECT_FALSE(nNet_SetTensorShape(tensor0, shape1) == true);
        EXPECT_FALSE(nNet_SetTensorShape(tensor1, shape1) == true);
      });

      EXPECT_EQ((tensor0 = nNet_RemoveTensor(tensor0)), nullptr);
      EXPECT_EQ((tensor1 = nNet_RemoveTensor(tensor1)), nullptr);

      IF(TRUE(nn::utils::outdate(tensor0)), [&]() {
        EXPECT_FALSE(nNet_SetTensorNumT(tensor0, Real32_t) == true);
        EXPECT_FALSE(nNet_SetTensorShape(tensor0, shape0) == true);
        EXPECT_FALSE(nNet_GetTensorShape(tensor0, nilshape) == true);

        EXPECT_EQ(nNet_GetTensorNumT(tensor0), Unknown);
      });

      IF(TRUE(nn::utils::outdate(tensor1)), [&]() {
        EXPECT_FALSE(nNet_SetTensorNumT(tensor1, Real64_t) == true);
        EXPECT_FALSE(nNet_SetTensorShape(tensor1, shape1) == true);
        EXPECT_FALSE(nNet_GetTensorShape(tensor1, nilshape) == true);

        EXPECT_EQ(nNet_GetTensorNumT(tensor1), Unknown);
      });
    });

    EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
  }
}
#endif

#if CHECK_CASE_TENSOR_SET_TENSOR_NUMT || !CHECK_CASE
TEST(NetTensor, SetTensorNumT) {
  std::vector<int> dims{1, 2, 3};
  // std::vector<int> pos{0, 0, 0};
  std::vector<int> begin{1, 2};
  std::vector<int> end{1, 2};

  nn::InterpreterW interpreter{""};

  for (auto &device : interpreter.devices()) {
    if (!interpreter.use(device))
      continue;
    info(NoError.reason("interpreter will use device " + std::get<0>(device)));

    interpreter([&]() {
      auto shape0 = nNet_CreateTensorShapeS(3, 1, 2, 3);
      auto niltensor0 = nNet_CreateTensorS();
      auto tensor0 = nNet_CreateTensorS();
      auto ord = Ordinate{nullptr};

      /* @NOTE: mac dinh khi khoi tao cac doi tuong deu duoc cap phat va
       * co the dung ngay lap tuc */

      EXPECT_FALSE(nn::utils::outdate(shape0));
      EXPECT_FALSE(nn::utils::outdate(tensor0));

      /* @NOTE: khoi tao cac thong tin co ban cho 1 tensor bang 2 phuong phap
       * - Dung CreateTensorW de tao khoi tao lai 1 tensor va cau hinh theo
       * shape
       * - Dung cac ham SetTensorShape va SetTensorNumT de gan cac thuoc tinh
       * cho 1 tensor rong
       */

      EXPECT_TRUE(nNet_SetTensorNumT(tensor0, Real32_t) == true);
      EXPECT_TRUE(nNet_SetTensorShape(tensor0, shape0) == true);

      /* @NOTE: lay cac thong tin tu tensor hien tai: shape, number type,
       * real value */
      EXPECT_EQ(nNet_GetTensorNumT(tensor0), Real32_t);
      EXPECT_TRUE(nNet_SetSubTensor(tensor0,
                                    nNet_CreateOrdinateW1(tensor0, nullptr, 0),
                                    nn::tensor::ones(dims, Real32_t)) == true);

      ord = nNet_CreateOrdinateW2(tensor0, (int *)begin.data(), 2,
                                  (int *)end.data(), 2);
      EXPECT_TRUE(nNet_GetSubTensor(tensor0, ord, niltensor0) == true);
      EXPECT_TRUE(nNet_SetTensorNumT(niltensor0, Real64_t) == true);
      EXPECT_TRUE(nNet_SetTensorNumT(niltensor0, Real32_t) == true);
    });
    EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
  }
}
#endif

#if CHECK_CASE_TENSOR_COMMUNICATE || !CHECK_CASE
TEST(NetTensor, Communicate) {
  std::vector<int> begin{1, 2};
  std::vector<int> end{2, 2};

  nn::InterpreterW interpreter{""};

  for (auto &device : interpreter.devices()) {
    if (!interpreter.use(device))
      continue;
    info(NoError.reason("interpreter will use device " + std::get<0>(device)));

    interpreter([&]() {
      auto tensor = nNet_CreateTensorS();
      auto niltensor = nNet_CreateTensorS();
      auto ord = Ordinate{nullptr};
      auto index = 0;

      /* @NOTE: mac dinh khi khoi tao cac doi tuong deu duoc cap phat va co the
       * dung ngay lap tuc */

      EXPECT_FALSE(nn::utils::outdate(tensor));
      EXPECT_TRUE(nNet_SetTensorNumT(tensor, Real32_t));
      EXPECT_TRUE(
          nNet_SetTensorShape(tensor, nNet_CreateTensorShapeS(4, 3, 3, 3, 2)));

      EXPECT_TRUE(
          nn::utils::assign<Real32>(
              tensor,
              {
                  1,  2,  3,  4,  5,  6,  7,  8,  9,  // matrix 1 (.., 0, 0)
                  10, 11, 12, 13, 14, 15, 16, 17, 18, // matrix 2 (.., 1, 0)
                  19, 20, 21, 22, 23, 24, 25, 26, 27, // matrix 3 (.., 2, 0)
                  28, 29, 30, 31, 32, 33, 34, 35, 36, // matrix 4 (.., 0, 1)
                  37, 38, 39, 40, 41, 42, 43, 44, 45, // matrix 5 (.., 1, 1)
                  46, 47, 48, 49, 50, 51, 52, 53, 54  // matrix 6 (.., 2, 1)
              })
              .code == base::error::ENoError);

      for (auto i = 0; i < nNet_GetArraySizeT(tensor); ++i){
        auto value = nNet_GetTensorItemS(tensor, i);

        EXPECT_TRUE(value != nullptr);
        if (value)
          EXPECT_EQ(*((Real32*)value), i + 1.0);
        nNet_TryDropingItem(tensor, value, i);
      }

      ord = nNet_CreateOrdinateW2(tensor, (int *)begin.data(), 2,
                                  (int *)end.data(), 2);
      EXPECT_TRUE(ord != nullptr);
      EXPECT_TRUE(nNet_GetSubTensor(tensor, ord, niltensor) == true);

      for (auto i0 = 0; i0 < 2; ++i0) {
        for (auto i1 = 0; i1 < 3; ++i1) {
          for (auto i2 = 0; i2 < 3; ++i2) {
            for (auto i3 = 0; i3 < 3; ++i3) {
              EXPECT_TRUE((ord = nNet_RewriteOrdinateS(ord, tensor, 4, i3, i2,
                                                       i1, i0)) != nullptr);
              EXPECT_TRUE(nNet_OrdinateToIndex(tensor, ord) == index++);
            }
          }
        }
      }
      nNet_CalcIndex(niltensor, 1);
    });
    EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
  }
}
#endif

#if CHECK_CASE_CUSTOM_TENSOR
#include "dummy.hpp"

// @TODO: dang co bug invalid write tren dummy.hpp
TEST(NetTensor, Custom) {
  std::vector<int> dims{1, 2, 3};
  std::vector<int> pos{0, 0, 0};
  std::vector<int> begin{1, 2};
  std::vector<int> end{1, 2};

  nn::InterpreterW interpreter{""};

  interpreter.assignDevice("dummy", (new nn::device::custom::CPDummy{})->interface());
  interpreter.use("dummy");
  interpreter([&]() {
    auto nilshape = nNet_CreateTensorShapeS(0);
    auto shape0 = nNet_CreateTensorShapeS(3, 1, 2, 3);
    auto shape1 = nNet_CreateTensorShapeW(3, const_cast<int *>(dims.data()));
    auto shape2 = nNet_CreateTensorShapeS(1, 3);

    auto niltensor0 = nNet_CreateTensorS();
    auto niltensor1 = nNet_CreateTensorS();
    auto tensor0 = nNet_CreateTensorS();
    auto tensor1 = nNet_CreateTensorW(shape1);

    auto ordinate0 = nNet_CreateOrdinateS(nullptr, 3, 0, 0, 0);
    auto ordinate1 = nNet_CreateOrdinateW1(tensor1, (int *)pos.data(), 3);
    auto ordinate2 = nNet_CreateOrdinateW2(tensor1, (int *)begin.data(), 2,
                                           (int *)end.data(), 2);
    /* @NOTE: mac dinh khi khoi tao cac doi tuong deu duoc cap phat va
     * co the dung ngay lap tuc */

    EXPECT_FALSE(nn::utils::outdate(nilshape));
    EXPECT_FALSE(nn::utils::outdate(shape0));
    EXPECT_FALSE(nn::utils::outdate(shape1));
    EXPECT_FALSE(nn::utils::outdate(niltensor0));
    EXPECT_FALSE(nn::utils::outdate(niltensor1));
    EXPECT_FALSE(nn::utils::outdate(tensor0));
    EXPECT_FALSE(nn::utils::outdate(tensor1));
    EXPECT_FALSE(nn::utils::outdate(ordinate0));
    EXPECT_FALSE(nn::utils::outdate(ordinate1));
    EXPECT_FALSE(nn::utils::outdate(ordinate2));

    /* @NOTE: khoi tao cac thong tin co ban cho 1 tensor bang 2 phuong phap
     * - Dung CreateTensorW de tao khoi tao lai 1 tensor va cau hinh theo shape
     * - Dung cac ham SetTensorShape va SetTensorNumT de gan cac thuoc tinh cho
     * 1 tensor rong
     */

    EXPECT_TRUE(nNet_SetTensorNumT(tensor0, Real32_t) == true);
    EXPECT_TRUE(nNet_SetTensorShape(tensor0, shape0) == true);
    EXPECT_TRUE(nNet_SetTensorNumT(tensor1, Real64_t) == true);

    /* @NOTE: lay cac thong tin tu tensor hien tai: shape, number type,
     * real value */
    EXPECT_EQ(nNet_GetTensorNumT(tensor0), Real32_t);
    EXPECT_EQ(nNet_GetTensorNumT(tensor1), Real64_t);

    EXPECT_TRUE(nNet_GetTensorShape(tensor1, nilshape) == true);
    EXPECT_TRUE(nNet_GetShapeSize(nilshape) ==
                (int)nn::tensor::size(dims.data(), dims.size()));

    IF(TRUE(nNet_GetTensorShape(tensor0, nilshape)), [&]() {
      auto realshape = nn::utils::values(nilshape);

      EXPECT_EQ(realshape.size(), dims.size());
      EXPECT_EQ(realshape, dims);
    });

    IF(TRUE(nNet_GetTensorShape(tensor0, nilshape)), [&]() {
      auto realshape = nn::utils::values(nilshape);

      EXPECT_EQ(realshape.size(), dims.size());
      EXPECT_EQ(realshape, dims);
    });

    EXPECT_TRUE(nNet_SetSubTensor(tensor0,
                                  nNet_CreateOrdinateW1(tensor0, nullptr, 0),
                                  nn::tensor::ones(dims, Real32_t)) == true);

    EXPECT_TRUE(nNet_GetSubTensor(tensor0, ordinate0, niltensor0) == true);
    EXPECT_TRUE(nNet_GetSubTensor(tensor0, ordinate1, niltensor0) == true);
    EXPECT_TRUE(nNet_GetSubTensor(tensor0, ordinate2, niltensor0) == true);

    IF(TRUE(nNet_GetSubTensor(
           tensor0, nNet_CreateOrdinateW1(tensor0, nullptr, 0), niltensor0)),
       [&]() {
         auto realtensor = nn::utils::values<Real32>(niltensor0);

         EXPECT_EQ(realtensor.size(),
                   std::size_t(nn::tensor::size(dims.data(), dims.size())));
       });

    EXPECT_TRUE(nNet_GetSubTensor(niltensor0, ordinate2, niltensor1) == true);
    EXPECT_TRUE(nNet_SetSubTensor(tensor1,
                                  nNet_CreateOrdinateW1(tensor1, nullptr, 0),
                                  nn::tensor::zeros(dims, Real64_t)) == true);
    EXPECT_TRUE((niltensor0 = nNet_ClearTensor(niltensor0)) != nullptr);
    IF(TRUE(nNet_GetSubTensor(
           tensor1, nNet_CreateOrdinateW1(tensor1, nullptr, 0), niltensor0)),
       [&]() {
         auto realtensor = nn::utils::values<Real64>(niltensor0);

         EXPECT_EQ(realtensor.size(),
                   nn::tensor::size(dims.data(), dims.size()));
       });

    EXPECT_TRUE(nNet_SetTensorShape(niltensor1, shape2) == true);
    EXPECT_TRUE(nNet_SetTensorNumT(niltensor0, Real64_t) == true);
    EXPECT_TRUE(nNet_SetTensorNumT(niltensor0, Real32_t) == true);
    EXPECT_TRUE(nNet_SetTensorNumT(niltensor0, Real64_t) == true);
    EXPECT_TRUE(nNet_SetTensorNumT(niltensor1, Real64_t) == true);
    EXPECT_TRUE(nNet_SetTensorNumT(niltensor1, Real32_t) == true);
    EXPECT_TRUE(nNet_SetTensorNumT(niltensor1, Real64_t) == true);
    EXPECT_TRUE(nNet_SetTensorNumT(tensor0, Real64_t) == true);
    EXPECT_TRUE(nNet_SetTensorNumT(tensor1, Real64_t) == true);
    EXPECT_TRUE(nNet_SetTensorNumT(tensor1, Real32_t) == true);
    EXPECT_TRUE(nNet_SetTensorNumT(tensor0, Real32_t) == true);
    EXPECT_TRUE(nNet_SetTensorNumT(tensor1, Real64_t) == true);

    /*  @NOTE: sau khi xoa object thi object do khong dung duoc nua */
    EXPECT_EQ((ordinate0 = nNet_RemoveOrdinate(ordinate0)), nullptr);
    EXPECT_EQ((ordinate1 = nNet_RemoveOrdinate(ordinate1)), nullptr);
    EXPECT_EQ((ordinate2 = nNet_RemoveOrdinate(ordinate1)), nullptr);

    IF(TRUE(nn::utils::outdate(ordinate0)), [&]() {
      EXPECT_FALSE(nNet_GetSubTensor(tensor0, ordinate0, niltensor0) == true);
    });

    IF(TRUE(nn::utils::outdate(ordinate1)), [&]() {
      EXPECT_FALSE(nNet_GetSubTensor(tensor0, ordinate1, niltensor0) == true);
    });

    IF(TRUE(nn::utils::outdate(ordinate2)), [&]() {
      EXPECT_FALSE(nNet_GetSubTensor(tensor0, ordinate2, niltensor0) == true);
    });

    EXPECT_EQ((shape0 = nNet_RemoveTensorShape(shape0)), nullptr);
    EXPECT_EQ((shape1 = nNet_RemoveTensorShape(shape1)), nullptr);

    IF(TRUE(nn::utils::outdate(shape0)), [&]() {
      EXPECT_FALSE(nNet_SetTensorShape(tensor0, shape0) == true);
      EXPECT_FALSE(nNet_SetTensorShape(tensor1, shape0) == true);
    });

    IF(TRUE(nn::utils::outdate(shape1)), [&]() {
      EXPECT_FALSE(nNet_SetTensorShape(tensor0, shape1) == true);
      EXPECT_FALSE(nNet_SetTensorShape(tensor1, shape1) == true);
    });

    EXPECT_EQ((tensor0 = nNet_RemoveTensor(tensor0)), nullptr);
    EXPECT_EQ((tensor1 = nNet_RemoveTensor(tensor1)), nullptr);

    IF(TRUE(nn::utils::outdate(tensor0)), [&]() {
      EXPECT_FALSE(nNet_SetTensorNumT(tensor0, Real32_t) == true);
      EXPECT_FALSE(nNet_SetTensorShape(tensor0, shape0) == true);
      EXPECT_FALSE(nNet_GetTensorShape(tensor0, nilshape) == true);

      EXPECT_EQ(nNet_GetTensorNumT(tensor0), Unknown);
    });

    IF(TRUE(nn::utils::outdate(tensor1)), [&]() {
      EXPECT_FALSE(nNet_SetTensorNumT(tensor1, Real64_t) == true);
      EXPECT_FALSE(nNet_SetTensorShape(tensor1, shape1) == true);
      EXPECT_FALSE(nNet_GetTensorShape(tensor1, nilshape) == true);

      EXPECT_EQ(nNet_GetTensorNumT(tensor1), Unknown);
    });
  });
}
#endif

#if CHECK_CASE_PRINT_TENSOR || !CHECK_CASE
TEST(NetTensor, Print){
  nn::InterpreterW interpreter{""};

  interpreter([&](){
    for (auto &device : interpreter.devices()) {
      if (!interpreter.use(device))
        continue;
      auto expect1 = "[[1, 1],\n [1, 1],\n [1, 1]]\n";
      auto expect2 = "[[0, 0],\n [0, 0],\n [0, 0]]\n";
      auto expect3 = "[[1, 0],\n [0, 1],\n [0, 0]]\n";
      auto expect4 =
          "[[[[1, 1],\n   [1, 1]],\n\n   [1, 1],\n   [1, 1]],\n\n   [1, 1],\n  "
          " [1, 1]]],\n\n  [[1, 1],\n   [1, 1]],\n\n   [1, 1],\n   [1, "
          "1]],\n\n   [1, 1],\n   [1, 1]]],\n\n  [[1, 1],\n   [1, 1]],\n\n   "
          "[1, 1],\n   [1, 1]],\n\n   [1, 1],\n   [1, 1]]]]\n";

      EXPECT_EQ(nn::utils::print(nn::tensor::ones(Real32_t, 3, 2)), expect1);
      EXPECT_EQ(nn::utils::print(nn::tensor::zeros(Real32_t, 3, 2)), expect2);
      EXPECT_EQ(nn::utils::print(nn::tensor::eye(Real32_t, 3, 2)), expect3);
      EXPECT_EQ(nn::utils::print(nn::tensor::ones({3, 3, 2, 2}, Real32_t)),
                expect4);
    }
  });
}
#endif

#ifndef TEST_ALLTESTS_CPP_
#include <libbase/all.hpp>
#include <signal.h>

static void handler(int sig, siginfo_t *dont_care, void* dont_care_either){
  base::trace::backtrace(30);
  exit(-1);
}

int main(int argc, char** argv){
  using namespace base;

 #if APPLE || USE_BASE_UNITTEST
  base::testing::init(argc, (const char**)argv);
 #else
  ::testing::InitGoogleTest(&argc, argv);
 #endif

  struct sigaction sa;

  memset(&sa, 0, sizeof(struct sigaction));
  sigemptyset(&sa.sa_mask);

  sa.sa_flags     = SA_NODEFER;
  sa.sa_sigaction = handler;

  sigaction(SIGSEGV, &sa, NULL); /* ignore whether it works or not */

  base::config::start(argv, argc);
  base::config::set("console", base::Auto{}.set(std::string{"console.txt"}));
  base::config::set("monitor", base::Auto{}.set(true));

  base::config::set("metal", base::Auto{}.set(std::string{"bundle"}));
  base::config::set("metal.file", base::Auto{}.set(std::string{"metal.metalib"}));

  base::config::set("opencl", base::Auto{}.set(std::string{"auto"}));
  base::config::set("opencl.file", base::Auto{}.set(std::string{"opencl.clbin"}));

  base::config::set("cuda", base::Auto{}.set(std::string{"file"}));
  base::config::set("cuda.file", base::Auto{}.set(std::string{"opencl.cubin"}));

  if (config::get("metal").type().get() != typeid(std::string))
    return -1;
  nNet_LoadEverything();
  return base::config::finish(RUN_ALL_TESTS(), [](){ nNet_UnloadEverything(); });
}
#endif // TEST_ALLTESTS_CPP_
#endif // BOOST_TEST_MAIN
