#ifndef LIBNN_TEST_DUMMY_HPP_
#define LIBNN_TEST_DUMMY_HPP_

#include "libnn/devices/dev_common.h"
// #include "all.hpp"

DEFINE_OP(generic, Mfcc);
DEFINE_OP(generic, DecodeWav);
DEFINE_OP(generic, AudioSpectrogram);

DEFINE_OP(generic, Add);
DEFINE_OP(generic, MatMul);
DEFINE_OP(generic, Reshape);

DEFINE_OP(generic, Conv2D);
DEFINE_OP(generic, Relu);
DEFINE_OP(generic, MaxPool);

DEFINE_OP(generic, Softmax);

namespace nn{
namespace device{
namespace custom{
Device create(Device pattern, Interpreter interpreter);
Device release(Device device);
Operator find(Device device, const char* name);
Tensor   tensor(Interpreter interpreter, Tensor tensor);

bool inspect(Device device);
bool install(Device device, const char* name, Operator op);
bool scan(Device device, Operator op);

struct CPDummy: nn::interpreter::DeviceW{
 public:
  CPDummy(): DeviceW("cpu_custom"){
    /* @NOTE: install global configure */
    interface()->level = COMMON_GPU_PLATFORM;

    interface()->_template._priv   = this;
    interface()->_template.find    = custom::find;
    interface()->_template.scan    = custom::scan;
    interface()->_template.inspect = custom::inspect;
    interface()->_template.create  = custom::create;
    interface()->_template.release = custom::release;
    interface()->_template.install = custom::install;

    interface()->tensor = custom::tensor;

    /* @NOTE: install generic's operators */
    FUNCTION(generic, interface(), Mfcc);
    FUNCTION(generic, interface(), Add);
    FUNCTION(generic, interface(), MatMul);
    FUNCTION(generic, interface(), Reshape);
    FUNCTION(generic, interface(), Conv2D);
    FUNCTION(generic, interface(), Relu);
    FUNCTION(generic, interface(), MaxPool);
    FUNCTION(generic, interface(), Softmax);
  }

  ~CPDummy(){
    if (interface()->_enable)
      free(interface()->_enable);
    interface()->_enable = nullptr; 
  }
};

struct TensorW: tensor::GPContent{
 public:
  explicit TensorW(Interpreter interpreter, Tensor tensor, int type, 
                   bool renew = false):
      GPContent(interpreter, tensor, true), _cache{nullptr}, _ref{false}
  {
    if (nNet_GetTensorType(tensor) != NilT)
      init(interpreter, tensor, type, renew);
  }

 ~TensorW(){ clear(); }

 public:
  void init(Interpreter interpreter, Tensor tensor, int dtype, bool renew){
    auto ord   = nNet_CreateOrdinateC1(interpreter, tensor, 0);
    auto shape = nNet_CreateTensorShapeC1(interpreter, 0);

    auto data  = nNet_GetArrayData(tensor);
    auto stype = nNet_GetTensorNumT(tensor);

    auto dims  = tensor->ndims > 0? new int[tensor->ndims]: nullptr;
    auto size  = 1;
    auto success = false;

    base::Boundary bound{[](){}, [&](){
      if (dims)
        delete [] dims;
    }};


    switch(nNet_GetTensorType(tensor)){
    case NilT:
    default:
      throw NoSupport;

    case ScalarT:
      if (!(_cache = malloc(nNet_SizeType(dtype))))
        throw DrainMem;
      if (!nNet_ConvertNumT(data, stype, _cache, dtype))
        throw BadAccess << "nNet_ConvertNumT()";
      return;

    case VectorT:
    case MatrixT:
    case TensorT:
      break;
    }

    if (!ord){
      throw DrainMem.reason("nNet_CreateOrdinateC1()");
    }

    if (!shape){
      throw DrainMem.reason("nNet_CreateTensorShapeC1()");
    }

    /* @NOTE: get Tensor's Shape which indicate the real size of this tensor */
    if (!nNet_GetTensorShape(tensor, shape)){
      throw BadAccess.reason("nNet_GetTensorShape()");
    } else {
      for (auto i = 0; i < shape->ndims; i++){
        size *= shape->dims[i];
      }
    }

    /* @NOTE: allocate _cache */
    if (!(_cache = malloc(size * nNet_SizeType(stype)))){
      throw DrainMem;
    }

    /* @NOTE: everything is done, now copy data from tensor to _cache */
    if (!renew){
      auto dst_idx = 0, src_idx = 0;

      for (auto i = 0; i < tensor->ndims; ++i){ dims[i] = 0; }
      while (dims[tensor->ndims - 1] < tensor->dims[tensor->ndims - 1]){
        auto editing = 0;
        auto lasted  = true;

        for (; dims[editing] < tensor->dims[editing]; ++dims[editing]){
          ord     = nNet_RewriteOrdinateW1(ord, tensor, tensor->ndims, dims);
          src_idx = nNet_OrdinateToIndex(tensor, ord);

          if (src_idx >= 0){
            auto src = nNet_GetArrayItem(data, src_idx, stype);
            auto dst = nNet_GetArrayItem(_cache, dst_idx, dtype);

            if (!(success = nNet_ConvertNumT(src, stype, dst, dtype))){
              free(_cache);
              _cache = nullptr;

              throw BadAccess.reason("nNet_ConvertNumT()");
            }
            dst_idx++;
          }
        }

        if (tensor->ndims == 1)
          break;
        for (auto i = editing + 1; i < tensor->ndims; ++i){
          dims[i - 1] = 0;

          if (dims[i] + 1 < tensor->dims[i]){
            lasted = false;
            dims[i]++;
            break;
          }
        } 

        if (lasted)
          dims[tensor->ndims - 1]++;
        dims[0] = 0;
      }
    }
  }

  Array cache(Array array, bool reference = true){
    _cache = array;
    _ref   = reference;

    return _cache;
  }

  Array cache(){ return _cache; }

  inline void clear() final{
    if (_cache){
      if (!_ref) free(_cache);
      _cache = nullptr; 
    }
  }

 private:
  void init() final{}

 private:
  Array _cache;
  bool  _ref;
};

Device create(Device pattern, Interpreter UNUSED(interpreter)){
  return pattern;
}

Device release(Device device){
  if (nNet_CheckStatus() == nn::Releasing || !nNet_IsTemplateDevice(device)){
    auto manager = dynamic_cast<CPDummy*>(nNet_GetDeviceW(device));

    if (manager){
      delete manager;
      return nullptr;
    }
  }
  return nNet_RemoveDevice(device);
}

bool inspect(Device){ return true; }

bool install(Device device, const char* name, Operator op){
  auto  delegate  = reinterpret_cast<CPDummy*>(device->_template._priv);
  auto& operators = delegate->operators();

  if (operators.find(std::string{name}) != operators.end())
    return BadLogic.reason("duplicate installing op " + std::string{name});

  operators[std::string(name)] = op;
  return true;
}

bool scan(Device device, Operator sample){
  auto  delegate  = reinterpret_cast<CPDummy*>(device->_template._priv);

  for (auto& item: delegate->operators()){
    if (std::get<1>(item) == sample)
      return true;
  }
  return false;
}

Operator find(Device device, const char* name){
  auto  delegate  = reinterpret_cast<CPDummy*>(device->_template._priv);
  auto& operators = delegate->operators();

  if (!name) return nullptr;
  else if (operators.find(std::string(name)) != operators.end())
    return operators[std::string(name)];
  return nullptr;
}

Tensor tensor(Interpreter interpreter, Tensor tensor){
  using namespace nn::interpreter;
  using namespace nn::tensor;

  auto device = reinterpret_cast<Device>(tensor->_device);
  auto result = tensor;

  if (nn::utils::outdate(interpreter, tensor))
    return nNet_RemoveTensor(tensor);

  if (!utils::response<DeviceW, CPDummy>(device->_priv))
    return nNet_RemoveTensor(tensor);

  /* @NOTE: because we need to convert tensor from device to another device, 
   * so we must convert data first 
   */
  if (nNet_GetTensorType(tensor) != NilT){
    try{
      auto content = new TensorW(interpreter, tensor, nNet_GetTensorNumT(tensor));
      auto shape   = nNet_CreateTensorShapeC1(interpreter, 0);

      if (nNet_GetTensorType(tensor) >= VectorT){

        /* @NOTE: only call tensor->release because we don't want to remove tensor
         * completely, we only care about remove its content and rebuild a new 
         * content. In somecase, we must preseve its shape too.
         */

        if (!nNet_GetTensorShape(tensor, shape)){
          return utils::clearFaulty(interpreter, tensor,
                                    BadAccess.reason("nNet_GetTensorShape()"));
        }
      }

      if (tensor->release)
        tensor->release(tensor, interpreter);
      else {
        if (tensor->_content)
          free (tensor->_content);
        tensor->_content = nullptr;
      }

      tensor->_content = content;
      if (nNet_GetTensorType(tensor) >= VectorT){
        if (!nNet_SetTensorShapeW(interpreter, tensor, shape)){
          result = utils::clearFaulty(interpreter, tensor,
                                      BadAccess <<"nNet_SetTensorShapeW()");
        }
      }

      if (nNet_RemoveTensorShapeW(interpreter, shape)){
        nNet_GetInterpreterW(interpreter)->error(
                                      BadAccess << "nNet_RemoveTensorShapeW()");
      }
    } catch(base::Error& error){
      return utils::clearFaulty(interpreter, tensor, error);
    }
  }

  /* @NOTE: install static functions which are used to manipulate with this 
   * tensor */
  result->diff = [](Tensor left, Tensor right) -> bool{
    auto length = 0;

    if (nNet_GetArraySizeT(left) == nNet_GetArraySizeT(right))
      length = nNet_GetArraySizeT(left);
    else return false;

    return !memcpy(nNet_GetArrayData(left), nNet_GetArrayData(right), length);
  };

  /* @NOTE: constructor and destructor */
  result->init =
    [](Tensor self, Interpreter interpreter, int size, int type, bool force) -> bool{
      auto device = reinterpret_cast<Device>(self->_device);

      if (!self->_content || force || self->type != type || type == Unknown){
        /* @NOTE: verify this tensor must be created by CPDummy */

        if (utils::response<DeviceW, CPDummy>(device->_priv) == nullptr)
          return false;

        /* @NOTE: init will be call in 3 cases
         * - Realloc
         * - Change numtype 
         * - Reshape with shape is nil
         */

        if ((self->_content && force) || type == Unknown){
          if (self->_content){
            delete utils::response<GPContent, TensorW>(self->_content);
            self->_content = nullptr;
          }
        }

        if (!self->_content && type != Unknown){
          self->_content = new TensorW(interpreter, self, type, true);
          self->release  = [](Tensor self, Interpreter UNUSED(interpreter)){
            if (self->_content){
              if (utils::response<GPContent, TensorW>(self->_content))
                delete utils::response<TensorW>(self->_content);
              self->_content = nullptr;
            }
          };
        }

        if (type != Unknown){
          auto content = utils::response<GPContent, TensorW>(self->_content);

          /* @NOTE: config cache, return false doesn't mean init is fail, it 
           * just means it's still not done completly*/
          if (content){
            content->clear();

            if (force){
              content->cache(calloc(size, nNet_SizeType(type)), false);
            } else {
              content->cache(realloc(content->cache(), 
                                     size*nNet_SizeType(type)), false);
            }
          }
          return true;
        }
      }
      return true;
    };

  result->_array.get = [](Tensor self) -> Array{
    auto control = utils::response<GPContent, TensorW>(self->_content);

    if (control){
      auto result = calloc(self->_size, nNet_SizeType(self->type));

      if (result){
        memcpy(result, control->cache(), self->_size*nNet_SizeType(self->type));
        return result;
      } else
        return nullptr;
    } else
      return nullptr;
  };

  result->_array.set = [](Tensor self, Array array) -> bool{
    auto control = utils::response<GPContent, TensorW>(self->_content);

    if (control){
      if (array != control->cache()){
        control->clear();
        control->cache(array);
      }
      return true;
    } else return false;
  };

  result->release = [](Tensor self, Interpreter UNUSED(interpreter)){
    if (self->_content){
      if (utils::response<GPContent, TensorW>(self->_content))
        delete utils::response<TensorW>(self->_content);
      self->_content = nullptr;
    }
  };

  return tensor;
}
} // namespace custom
} // namespace device
} // namespace nn
#endif  // LIBNN_TEST_DUMMY_HPP_
