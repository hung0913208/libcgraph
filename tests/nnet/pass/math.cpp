#ifndef BOOST_TEST_MAIN
#include <libnn/devices/generic/math.h>
#include <libnn/interpreter.h>
#include <libbase/all.hpp>

#if APPLE || USE_BASE_UNITTEST
#include <libbase/unittest.hpp>
#else
#include <gtest/gtest.h>
#endif

using namespace nn::generic::math::conv2d;
using namespace nn::utils;

#if CHECK_CASE_IM2COL_OF_NHWC || !CHECK_CASE
TEST(Math, Im2Col_Of_NHWC){
  Interpreter interpreter = nNet_CreateInterpreter(nullptr);
  Tensor  input  = nn::tensor::tensor(range(0, 1*4*4*1), {1, 4, 4, 1});
  Tensor  filter = nn::tensor::tensor(range(0, 3*3*1*1), {3, 3, 1, 1});

  Tensor* result_same  = nullptr;
  Tensor* result_valid = nullptr;

  Tensor expected_same[2] = {
      nn::tensor::tensor(
          std::vector<Int32>(
              {0,  0,  0,  0,  0,  1,  0,  4,  5, 
               0,  0,  0,  0,  1,  2,  4,  5,  6,
               0,  0,  0,  1,  2,  3,  5,  6,  7,
               0,  0,  0,  2,  3,  0,  6,  7,  0,
               0,  0,  1,  0,  4,  5,  0,  8,  9,
               0,  1,  2,  4,  5,  6,  8,  9, 10,
               1,  2,  3,  5,  6,  7,  9, 10, 11,
               2,  3,  0,  6,  7,  0, 10, 11,  0,
               0,  4,  5,  0,  8,  9,  0, 12, 13,
               4,  5,  6,  8,  9, 10, 12, 13, 14,
               5,  6,  7,  9, 10, 11, 13, 14, 15,
               6,  7,  0, 10, 11,  0, 14, 15,  0,
               0,  8,  9,  0, 12, 13,  0,  0,  0,
               8,  9, 10, 12, 13, 14,  0,  0,  0,
               9, 10, 11, 13, 14, 15,  0,  0,  0,
              10, 11,  0, 14, 15,  0,  0,  0,  0}),
          {4 * 4 * 1 * 1, 3 * 3}),
      nn::tensor::tensor(range(0, 1 * 3 * 3 * 1), {1, 3*3})};

  Tensor expected_valid[2] = {
      nn::tensor::tensor(
          std::vector<Int32>({0,  1,  2,  4,  5,  6,  8,  9, 10,
                              1,  2,  3,  5,  6,  7,  9, 10, 11,
                              4,  5,  6,  8,  9, 10, 12, 13, 14,
                              5,  6,  7,  9, 10, 11, 13, 14, 15}),
          {4 * 1 * 1, 3 * 3}),
      nn::tensor::tensor(range(0, 1 * 3 * 3 * 1), {1, 3*3})};

  EXPECT_TRUE(nhwc::im2col<Int32>(interpreter, input, filter, result_same, 
                                  {1, 1}, {1, 1}, "SAME"));
  EXPECT_TRUE(nhwc::im2col<Int32>(interpreter, input, filter, result_valid,
                                  {1, 1}, {1, 1}, "VALID"));
  EXPECT_TRUE(result_same != NULL);
  EXPECT_TRUE(result_valid != NULL);

  if (result_same){
    for (auto i = 0; i < 2*nn::tensor::dimemsion(filter, 2); ++i)
      EXPECT_EQ(nn::utils::diff(result_same[i], expected_same[i]), 0);
    free(result_same);
  }

  if (result_valid){
    for (auto i = 0; i < 2*nn::tensor::dimemsion(filter, 2); ++i)
      EXPECT_EQ(nn::utils::diff(result_valid[i], expected_valid[i]), 0);
    free(result_valid);
  }

  nNet_RemoveInterpreter(interpreter);
}
#endif

#if CHECK_CASE_IM2COL_OF_NCHW || !CHECK_CASE
TEST(Math, Im2Col_Of_NCHW){
  Interpreter interpreter = nNet_CreateInterpreter(nullptr);
  Tensor  input  = nn::tensor::tensor(range(0, 1*4*4*1), {1, 1, 4, 4});
  Tensor  filter = nn::tensor::tensor(range(0, 3*3*1*1), {3, 3, 1, 1});

  Tensor* result_same  = nullptr;
  Tensor* result_valid = nullptr;
  Tensor expected_same[2] = {
      nn::tensor::tensor(
          std::vector<Int32>(
              {0,  0,  0,  0,  0,  1,  0,  4,  5, 
               0,  0,  0,  0,  1,  2,  4,  5,  6,
               0,  0,  0,  1,  2,  3,  5,  6,  7,
               0,  0,  0,  2,  3,  0,  6,  7,  0,
               0,  0,  1,  0,  4,  5,  0,  8,  9,
               0,  1,  2,  4,  5,  6,  8,  9, 10,
               1,  2,  3,  5,  6,  7,  9, 10, 11,
               2,  3,  0,  6,  7,  0, 10, 11,  0,
               0,  4,  5,  0,  8,  9,  0, 12, 13,
               4,  5,  6,  8,  9, 10, 12, 13, 14,
               5,  6,  7,  9, 10, 11, 13, 14, 15,
               6,  7,  0, 10, 11,  0, 14, 15,  0,
               0,  8,  9,  0, 12, 13,  0,  0,  0,
               8,  9, 10, 12, 13, 14,  0,  0,  0,
               9, 10, 11, 13, 14, 15,  0,  0,  0,
              10, 11,  0, 14, 15,  0,  0,  0,  0}),
          {4 * 4 * 1 * 1, 3 * 3}),
      nn::tensor::tensor(range(0, 1 * 3 * 3 * 1), {1, 3 * 3})};

  Tensor expected_valid[2] = {
      nn::tensor::tensor(
          std::vector<Int32>({0,  1,  2,  4,  5,  6,  8,  9, 10,
                              1,  2,  3,  5,  6,  7,  9, 10, 11,
                              4,  5,  6,  8,  9, 10, 12, 13, 14,
                              5,  6,  7,  9, 10, 11, 13, 14, 15}),
          {4 * 1 * 1, 3 * 3}),
      nn::tensor::tensor(range(0, 1 * 3 * 3 * 1), {1, 3 * 3})};

  EXPECT_TRUE(nchw::im2col<Int32>(interpreter, input, filter, result_same, 
                                  {1, 1}, {1, 1}, "SAME"));
  EXPECT_TRUE(nchw::im2col<Int32>(interpreter, input, filter, result_valid,
                                  {1, 1}, {1, 1}, "VALID"));
  EXPECT_TRUE(result_same != NULL);
  EXPECT_TRUE(result_valid != NULL);

  if (result_same){
    for (auto i = 0; i < 2*nn::tensor::dimemsion(filter, 2); ++i)
      EXPECT_EQ(nn::utils::diff(result_same[i], expected_same[i]), 0);
    free(result_same);
  }

  if (result_valid){
    for (auto i = 0; i < 2*nn::tensor::dimemsion(filter, 2); ++i)
      EXPECT_EQ(nn::utils::diff(result_valid[i], expected_valid[i]), 0);
    free(result_valid);
  }

  nNet_RemoveInterpreter(interpreter);
}
#endif

#if CHECK_CASE_COL2IM_OF_NHWC || !CHECK_CASE
TEST(Math, Col2Im_Of_NHWC){
  Interpreter interpreter = nNet_CreateInterpreter(nullptr);
  Tensor columns[] = {
    nn::tensor::tensor(range(0, 1*4*4), {1, 4*4}),
    nn::tensor::tensor(range(0, 1*4*4), {1, 4*4}),
    nn::tensor::tensor(range(0, 1*4*4), {1, 4*4}),
  };
  Tensor output = nn::tensor::zeros({1, 2, 2, 3}, Int32_t);
  Tensor expected = nn::tensor::tensor<Int32>({0, 0, 0, 
                                               2, 2, 2, 
                                               1, 1, 1, 
                                               3, 3, 3}, 
                                              {1, 2, 2, 3});

  EXPECT_TRUE(nhwc::col2im<Int32>(interpreter, columns, output));
  EXPECT_EQ(nn::utils::diff(output, expected), 0);
  nNet_RemoveInterpreter(interpreter);
}
#endif

#if CHECK_CASE_COL2IM_OF_NCHW || !CHECK_CASE
TEST(Math, Col2Im_Of_NCHW){
  Interpreter interpreter = nNet_CreateInterpreter(nullptr);
  Tensor columns[] = {
    nn::tensor::tensor(range(0, 1*2*2), {1, 2*2}),
    nn::tensor::tensor(range(0, 1*2*2), {1, 2*2}),
    nn::tensor::tensor(range(0, 1*2*2), {1, 2*2}),
  };
  Tensor output = nn::tensor::zeros({1, 2, 2, 3}, Int32_t);
  Tensor expected = nn::tensor::tensor<Int32>({0, 1, 2, 3, 
                                               0, 1, 2, 3,
                                               0, 1, 2, 3}, 
                                              {1, 2, 2, 3});

  EXPECT_TRUE(nchw::col2im<Int32>(interpreter, columns, output));
  EXPECT_EQ(nn::utils::diff(output, expected), 0);
  nNet_RemoveInterpreter(interpreter);
}
#endif

#ifndef TEST_ALLTESTS_CPP_
#include <libbase/all.hpp>
#include <signal.h>

static void handler(int sig, siginfo_t *dont_care, void* dont_care_either){
  base::trace::backtrace(30);
  exit(-1);
}

int main(int argc, char** argv){
  using namespace base;

 #if APPLE || USE_BASE_UNITTEST
  base::testing::init(argc, (const char**)argv);
 #else
  ::testing::InitGoogleTest(&argc, argv);
 #endif
 #if NDEBUG
  struct sigaction sa;

  memset(&sa, 0, sizeof(struct sigaction));
  sigemptyset(&sa.sa_mask);

  sa.sa_flags     = SA_NODEFER;
  sa.sa_sigaction = handler;

  sigaction(SIGSEGV, &sa, NULL); /* ignore whether it works or not */
 #endif

  base::config::start(argv, argc);
  base::config::set("console", base::Auto{}.set(std::string{"console.txt"}));
  base::config::set("monitor", base::Auto{}.set(true));

  base::config::set("metal", base::Auto{}.set(std::string{"bundle"}));
  base::config::set("metal.file", base::Auto{}.set(std::string{"metal.metalib"}));

  base::config::set("opencl", base::Auto{}.set(std::string{"auto"}));
  base::config::set("opencl.file", base::Auto{}.set(std::string{"opencl.clbin"}));

  base::config::set("cuda", base::Auto{}.set(std::string{"file"}));
  base::config::set("cuda.file", base::Auto{}.set(std::string{"opencl.cubin"}));

  if (config::get("metal").type().get() != typeid(std::string))
    return -1;
  nNet_LoadEverything();
  return base::config::finish(RUN_ALL_TESTS(), [](){ nNet_UnloadEverything(); });
}
#endif // TEST_ALLTESTS_CPP_
#endif // BOOST_TEST_MAIN
