#ifndef BOOST_TEST_MAIN
#include <libnn/devices/generic/wavio.h>
#include <libbase/all.hpp>

#if APPLE || USE_BASE_UNITTEST
#include <libbase/unittest.hpp>
#else
#include <gtest/gtest.h>
#endif

#if ENABLE_FORWARD_BACKWARD_OPERATORS
#if CHECK_CASE_WAVIO_READ_FROM_FILE || !CHECK_CASE
TEST(WavIO, ReadFromFile){      
  namespace audio = nn::generic::audio;
  float   fbuffer;
  int32_t ibuffer;

  audio::WavIO<float>   float_victim("tests/samples/victim.wav");
  audio::WavIO<int32_t> int32_victim("tests/samples/victim.wav");

  EXPECT_TRUE(float_victim.size() > 0);
  EXPECT_TRUE(int32_victim.size() > 0);
  
  int32_victim >> ibuffer;
  float_victim >> fbuffer;

  std::cout << float_victim.countSamples();
}
#endif

#if CHECK_CASE_WAVIO_WRITE_TO_FILE
TEST(WavIO, WriteToFile){ 
  namespace audio = nn::generic::audio;
  audio::WavIO<float>   float_victim("float_victim.wav", std::ios::out);
  audio::WavIO<int32_t> int32_victim("int32_victim.wav", std::ios::out);

  float_victim << 0.2;
  float_victim << 0.3;
  float_victim << 0.4;
  float_victim << 0.5;
  float_victim << 0.6;
  float_victim << 0.4;
  float_victim << 0.2;
  float_victim << 0.3;
  float_victim << 0.4;
  float_victim << 0.5;
  float_victim << 0.6;

  int32_victim << 1;
  int32_victim << 2;
  int32_victim << 3;
  int32_victim << 4;
  int32_victim << 5;
}
#endif

#if CHECK_CASE_WAVIO_READ_FROM_BUFFER
TEST(WavIO, ReadFromBuffer){   
  namespace audio = nn::generic::audio;
  audio::WavIO<float>   float_victim{};
  audio::WavIO<int32_t> int32_victim{};

  EXPECT_TRUE(float_victim.size() == 0);
  EXPECT_TRUE(int32_victim.size() == 0);
}
#endif

#if CHECK_CASE_WAVIO_WRITE_TO_BUFFER
TEST(WavIO, WriteToBuffer){
  namespace audio = nn::generic::audio;
  audio::WavIO<float>   float_victim{};
  audio::WavIO<int32_t> int32_victim{};

  EXPECT_TRUE(float_victim.size() == 0);
  EXPECT_TRUE(int32_victim.size() == 0);
}
#endif
#endif  // ENABLE_FORWARD_BACKWARD_OPERATORS

#ifndef TEST_ALLTESTS_CPP_
#include <libbase/all.hpp>
#include <signal.h>

static void handler(int sig, siginfo_t *dont_care, void* dont_care_either){
  base::trace::backtrace(30);
  exit(-1);
}

int main(int argc, char** argv){
  using namespace base;

 #if APPLE || USE_BASE_UNITTEST
  base::testing::init(argc, (const char**)argv);
 #else
  ::testing::InitGoogleTest(&argc, argv);
 #endif

  struct sigaction sa;

  memset(&sa, 0, sizeof(struct sigaction));
  sigemptyset(&sa.sa_mask);

  sa.sa_flags     = SA_NODEFER;
  sa.sa_sigaction = handler;

  sigaction(SIGSEGV, &sa, NULL); /* ignore whether it works or not */

  base::config::start(argv, argc);
  return base::config::finish(RUN_ALL_TESTS());
}
#endif // TEST_ALLTESTS_CPP_
#endif // BOOST_TEST_MAIN
