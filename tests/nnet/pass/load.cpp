#ifndef BOOST_TEST_MAIN
#include "all.hpp"

#include <dirent.h>
#include <libbase/all.hpp>

#if APPLE || USE_BASE_UNITTEST
#include <libbase/unittest.hpp>
#else
#include <gtest/gtest.h>
#endif

#include <fstream>

using namespace base;

#if CHECK_CASE_LOAD_TFGRAPH_SIMPLE
TEST(TFGraph, Simple){
  auto dp   = (struct dirent*){nullptr};
  auto dirp = opendir("tests/models/");

  while((dp = readdir(dirp)) != nullptr){
    auto pass = true;
    std::cout << std::string{dp->d_name} << std::endl;

    if (strcmp(dp->d_name, ".") && strcmp(dp->d_name, "..")){
      std::fstream stream{"tests/models/" + std::string{dp->d_name}, 
                          std::ios::in};
      if (stream.is_open()){
        nn::InterpreterW interpreter{stream, true};

        interpreter([&](nn::InterpreterW& self){
          EXPECT_TRUE((pass = (self.operators().size() > 0)));
          EXPECT_TRUE((pass = (self.flow().size() > 0)));
        });
        EXPECT_TRUE(interpreter.error().code == base::error::ENoError);

        if (interpreter.error().code || !pass){
          base::trace::console << (base::Color{base::Color::Red} << "[  FAILED  ]")
                               << " at model tests/models/" << std::string{dp->d_name}
                               << base::trace::eol;
        }
      }
    }
  }
  closedir(dirp);
}
#endif

#if CHECK_CASE_LOAD_TFGRAPH_LAZYLOAD_SIMPLE
TEST(TFGraph, LazyloadSimple){
  auto dp   = (struct dirent*){nullptr};
  auto dirp = opendir("tests/models/");

  while((dp = readdir(dirp)) != nullptr){
    auto pass = true;

    if (strcmp(dp->d_name, ".") && strcmp(dp->d_name, "..")){
      std::fstream stream{"tests/models/" + std::string{dp->d_name}, 
                          std::ios::in};
      if (stream.is_open()){
        nn::InterpreterW interpreter{stream, true, true};

        interpreter([&](nn::InterpreterW& self){
          EXPECT_TRUE((pass = (self.operators().size() > 0)));
          EXPECT_TRUE((pass = (self.flow().size() > 0)));
        });
        EXPECT_TRUE(interpreter.error().code == base::error::ENoError);

        if (interpreter.error().code || !pass){
          base::trace::console << (base::Color{base::Color::Red} << "[  FAILED  ]")
                               << " at model tests/models/" << std::string{dp->d_name}
                               << base::trace::eol;
        }
      }
    }
  }
  closedir(dirp);
}
#endif

#ifndef TEST_ALLTESTS_CPP_
#include <libbase/all.hpp>
#include <signal.h>

static void handler(int sig, siginfo_t *dont_care, void* dont_care_either){
  base::trace::backtrace(30);
  exit(-1);
}

int main(int argc, char** argv){
  using namespace base;

 #if APPLE || USE_BASE_UNITTEST
  base::testing::init(argc, (const char**)argv);
 #else
  ::testing::InitGoogleTest(&argc, argv);
 #endif

  struct sigaction sa;

  memset(&sa, 0, sizeof(struct sigaction));
  sigemptyset(&sa.sa_mask);

  sa.sa_flags     = SA_NODEFER;
  sa.sa_sigaction = handler;

  sigaction(SIGSEGV, &sa, NULL); /* ignore whether it works or not */

  base::config::start(argv, argc);
  base::config::set("console", base::Auto{}.set(std::string{"console.txt"}));
  base::config::set("monitor", base::Auto{}.set(true));

  base::config::set("metal", base::Auto{}.set(std::string{"bundle"}));
  base::config::set("metal.file", base::Auto{}.set(std::string{"metal.metalib"}));

  base::config::set("opencl", base::Auto{}.set(std::string{"auto"}));
  base::config::set("opencl.file", base::Auto{}.set(std::string{"opencl.clbin"}));

  base::config::set("cuda", base::Auto{}.set(std::string{"file"}));
  base::config::set("cuda.file", base::Auto{}.set(std::string{"opencl.cubin"}));

  if (config::get("metal").type().get() != typeid(std::string))
    return -1;
  nNet_LoadEverything();
  return base::config::finish(RUN_ALL_TESTS(), [](){ nNet_UnloadEverything(); });
}
#endif // TEST_ALLTESTS_CPP_
#endif // BOOST_TEST_MAIN
