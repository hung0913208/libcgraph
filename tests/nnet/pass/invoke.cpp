#ifndef BOOST_TEST_MAIN
#include "all.hpp"
#include "dummy.hpp"

#if APPLE || USE_BASE_UNITTEST
#include <libbase/unittest.hpp>
#else
#include <gtest/gtest.h>
#endif
#include <dirent.h>
#include <libbase/all.hpp>

#include <fstream>

using namespace base;
#if CHECK_CASE_INVOKE_TFGRAPH_CONST_ADD || !CHECK_CASE
TEST(Invoke, TFGraph_ConstAdd_NoLazyload) {
  std::fstream stream{"tests/models/add.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto expected = nn::tensor::tensor<Int32>(
        {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24}, {3, 4});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_FALSE(self.error());
    EXPECT_EQ(result.size(), 1);

    if (result.size() == 1)
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_ConstAdd_WithLazyload) {
  std::fstream stream{"tests/models/add.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto expected = nn::tensor::tensor<Int32>(
        {2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22, 24}, {3, 4});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_FALSE(self.error());
    EXPECT_EQ(result.size(), 1);

    if (result.size() == 1)
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_CONST_MATMUL || !CHECK_CASE
TEST(Invoke, TFGraph_ConstMatMul_NoLazyload) {
  std::fstream stream{"tests/models/matmult.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto expected = nn::tensor::tensor<Int32>(
        {60, 50, 40, 180, 154, 128, 300, 258, 216}, {3, 3});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_FALSE(self.error());
    EXPECT_EQ(result.size(), 1);

    if (result.size() == 1)
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_ConstMatMul_WithLazyload) {
  std::fstream stream{"tests/models/matmult.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto expected = nn::tensor::tensor<Int32>(
        {60, 50, 40, 180, 154, 128, 300, 258, 216}, {3, 3});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_FALSE(self.error());
    EXPECT_EQ(result.size(), 1);

    if (result.size() == 1)
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_CONST_RELU || !CHECK_CASE
TEST(Invoke, TFGraph_ConstRelu_NoLazyload) {
  std::fstream stream{"tests/models/relu.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto expected = nn::tensor::tensor<Int32>(
        {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}, {4, 3});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_FALSE(self.error());
    EXPECT_EQ(result.size(), 1);

    if (result.size() == 1)
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_ConstRelu_WithLazyload) {
  std::fstream stream{"tests/models/relu.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto expected = nn::tensor::tensor<Int32>(
        {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12}, {4, 3});

    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_FALSE(self.error());
    EXPECT_EQ(result.size(), 1);

    if (result.size() == 1)
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_CONST_RESHAPE || !CHECK_CASE
TEST(Invoke, TFGraph_ConstReshape_NoLazyload) {
  std::fstream stream{"tests/models/reshape.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto expected =
        nn::tensor::tensor<Real32>({1.0, 1.1, 1.2, 1.3, 1.4, 1.5}, {2, 3});    
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_FALSE(self.error());
    EXPECT_EQ(result.size(), 1);

    if (result.size() == 1)
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_ConstReshape_WithLazyload) {
  std::fstream stream{"tests/models/reshape.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto expected =
        nn::tensor::tensor<Real32>({1.0, 1.1, 1.2, 1.3, 1.4, 1.5}, {2, 3});

    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_FALSE(self.error());
    EXPECT_EQ(result.size(), 1);

    if (result.size() == 1)
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_CONST_MAXPOOL_VALID || !CHECK_CASE
TEST(Invoke, TFGraph_ConstMaxPool_Valid_NoLazyload) {
  std::fstream stream{"tests/models/maxpool_valid.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto nhwc_expected =
        nn::tensor::tensor<Real32>({19.0, 22.0, 43.0, 46.0, 83.0, 86.0, 107.0,
                                    110.0, 147.0, 150.0, 171.0, 174.0},
                                   {1, 3, 2, 2});
    auto nchw_expected =
        nn::tensor::tensor<Real32>({55.0, 56.0, 57.0, 64.0, 65.0, 66.0, 127.0,
                                    128.0, 129.0, 136.0, 137.0, 138.0},
                                   {1, 2, 2, 3});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 2);

    if (result.size() == 2) {
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), nhwc_expected), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[1]), nchw_expected), 0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_ConstMaxPool_Valid_WithLazyload) {
  std::fstream stream{"tests/models/maxpool_valid.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto nhwc_expected =
        nn::tensor::tensor<Real32>({19.0, 22.0, 43.0, 46.0, 83.0, 86.0, 107.0,
                                    110.0, 147.0, 150.0, 171.0, 174.0},
                                   {1, 3, 2, 2});
    auto nchw_expected =
        nn::tensor::tensor<Real32>({55.0, 56.0, 57.0, 64.0, 65.0, 66.0, 127.0,
                                    128.0, 129.0, 136.0, 137.0, 138.0},
                                   {1, 2, 2, 3});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 2);

    if (result.size() == 2) {
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), nhwc_expected), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[1]), nchw_expected), 0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_CONST_MAXPOOL_SAME || !CHECK_CASE
/*

*/
TEST(Invoke, TFGraph_ConstMaxPool_Same_NoLazyload) {
  std::fstream stream{"tests/models/maxpool_same.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto maxpool_0 = nn::tensor::tensor<Real32>(
        {19,  22,  24,  43,  46,  48,  59,  62,  64,  83,  86,  88,  107, 110,
         112, 123, 126, 128, 147, 150, 152, 171, 174, 176, 187, 190, 192},
        {1, 3, 3, 3});
    auto maxpool_1 = nn::tensor::tensor<Real32>(
        {55,  56,  57,  64,  65,  66,  70,  71,  72,  127, 128, 129, 136, 137,
         138, 142, 143, 144, 175, 176, 177, 184, 185, 186, 190, 191, 192},
        {1, 3, 3, 3});
    auto maxpool_2 =
        nn::tensor::tensor<Real32>({19, 24, 43, 48, 59, 64, 83, 88, 107, 112,
                                    123, 128, 147, 152, 171, 176, 187, 192},
                                   {1, 3, 3, 2});
    auto maxpool_3 =
        nn::tensor::tensor<Real32>({55, 56, 57, 70, 71, 72, 127, 128, 129, 142,
                                    143, 144, 175, 176, 177, 190, 191, 192},
                                   {1, 3, 2, 3});
    auto maxpool_4 = nn::tensor::tensor<Real32>(
        {19, 24, 59, 64, 83, 88, 123, 128, 147, 152, 187, 192}, {1, 3, 2, 2});
    auto maxpool_5 = nn::tensor::tensor<Real32>(
        {19, 24, 59, 64, 83, 88, 123, 128, 147, 152, 187, 192}, {1, 3, 2, 2});
    auto maxpool_6 = nn::tensor::tensor<Real32>(
        {55, 56, 57, 70, 71, 72, 175, 176, 177, 190, 191, 192}, {1, 2, 2, 3});
    auto maxpool_7 = nn::tensor::tensor<Real32>(
        {10, 16, 58, 64, 74, 80, 122, 128, 138, 144, 186, 192}, {1, 3, 2, 2});
    auto maxpool_8 = nn::tensor::tensor<Real32>(
        {28, 29, 30, 46, 47, 48, 172, 173, 174, 190, 191, 192}, {1, 2, 2, 3});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 9);

    if (result.size() == 9) {
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), maxpool_0), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[1]), maxpool_1), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[2]), maxpool_2), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[3]), maxpool_3), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[4]), maxpool_4), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[5]), maxpool_5), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[6]), maxpool_6), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[7]), maxpool_7), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[8]), maxpool_8), 0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_ConstMaxPool_Same_WithLazyload) {
  std::fstream stream{"tests/models/maxpool_same.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto maxpool_0 = nn::tensor::tensor<Real32>(
        {19,  22,  24,  43,  46,  48,  59,  62,  64,  83,  86,  88,  107, 110,
         112, 123, 126, 128, 147, 150, 152, 171, 174, 176, 187, 190, 192},
        {1, 3, 3, 3});
    auto maxpool_1 = nn::tensor::tensor<Real32>(
        {55,  56,  57,  64,  65,  66,  70,  71,  72,  127, 128, 129, 136, 137,
         138, 142, 143, 144, 175, 176, 177, 184, 185, 186, 190, 191, 192},
        {1, 3, 3, 3});
    auto maxpool_2 =
        nn::tensor::tensor<Real32>({19, 24, 43, 48, 59, 64, 83, 88, 107, 112,
                                    123, 128, 147, 152, 171, 176, 187, 192},
                                   {1, 3, 3, 2});
    auto maxpool_3 =
        nn::tensor::tensor<Real32>({55, 56, 57, 70, 71, 72, 127, 128, 129, 142,
                                    143, 144, 175, 176, 177, 190, 191, 192},
                                   {1, 3, 2, 3});
    auto maxpool_4 = nn::tensor::tensor<Real32>(
        {19, 24, 59, 64, 83, 88, 123, 128, 147, 152, 187, 192}, {1, 3, 2, 2});
    auto maxpool_5 = nn::tensor::tensor<Real32>(
        {19, 24, 59, 64, 83, 88, 123, 128, 147, 152, 187, 192}, {1, 3, 2, 2});
    auto maxpool_6 = nn::tensor::tensor<Real32>(
        {55, 56, 57, 70, 71, 72, 175, 176, 177, 190, 191, 192}, {1, 2, 2, 3});
    auto maxpool_7 = nn::tensor::tensor<Real32>(
        {10, 16, 58, 64, 74, 80, 122, 128, 138, 144, 186, 192}, {1, 3, 2, 2});
    auto maxpool_8 = nn::tensor::tensor<Real32>(
        {28, 29, 30, 46, 47, 48, 172, 173, 174, 190, 191, 192}, {1, 2, 2, 3});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 9);

    if (result.size() == 9) {
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), maxpool_0), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[1]), maxpool_1), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[2]), maxpool_2), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[3]), maxpool_3), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[4]), maxpool_4), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[5]), maxpool_5), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[6]), maxpool_6), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[7]), maxpool_7), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[8]), maxpool_8), 0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_CONST_MAXPOOL_NCHW || !CHECK_CASE
TEST(Invoke, TFGraph_ConstMaxPoolNCHW_NoLazyload) {
  std::fstream stream{"tests/models/maxpool_NCHW.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto expected = nn::tensor::tensor<Real32>({5, 14, 23}, {1, 3, 1, 1});

    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_FALSE(self.error());
    EXPECT_EQ(result.size(), 1);

    if (result.size() == 1)
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_ConstMaxPoolNCHW_WithLazyload) {
  std::fstream stream{"tests/models/maxpool_NCHW.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto expected = nn::tensor::tensor<Real32>({5, 14, 23}, {1, 3, 1, 1});

    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_FALSE(self.error());
    EXPECT_EQ(result.size(), 1);

    if (result.size() == 1)
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_CONST_MAXPOOL_NHWC || !CHECK_CASE
TEST(Invoke, TFGraph_ConstMaxPoolNHWC_NoLazyload) {
  std::fstream stream{"tests/models/maxpool_NHWC.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto expected = nn::tensor::tensor<Real32>({13, 14, 15}, {1, 1, 1, 3});

    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_FALSE(self.error());
    EXPECT_EQ(result.size(), 1);

    if (result.size() == 1)
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_ConstMaxPoolNHWC_WithLazyload) {
  std::fstream stream{"tests/models/maxpool_NHWC.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto expected = nn::tensor::tensor<Real32>({13, 14, 15}, {1, 1, 1, 3});

    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_FALSE(self.error());
    EXPECT_EQ(result.size(), 1);

    if (result.size() == 1)
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_CONST_CONV2D || !CHECK_CASE
TEST(Invoke, TFGraph_ConstConv2D_NoLazyload) {
  std::fstream stream{"tests/models/conv2d.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto conv2d_0 = nn::tensor::tensor<Real32>(
        {8,  12, 20, 24, 32, 36, 8,  12, 20, 24, 32, 36,
         56, 60, 68, 72, 80, 84, 56, 60, 68, 72, 80, 84},
        {2, 2, 3, 2});
    auto conv2d_1 = nn::tensor::tensor<Real32>(
        {64, 72, 88, 96, 112, 120, 64, 72, 88, 96, 112, 120}, {1, 2, 3, 2});
    auto conv2d_2 = nn::tensor::tensor<Real32>(
        {8,  12, 20, 24, 32, 36, 8,  12, 20, 24, 32, 36,
         56, 60, 68, 72, 80, 84, 56, 60, 68, 72, 80, 84},
        {2, 2, 3, 2});
    auto conv2d_3 = nn::tensor::tensor<Real32>(
        {64, 72, 88, 96, 112, 120, 64, 72, 88, 96, 112, 120}, {1, 2, 3, 2});
    auto conv2d_4 = nn::tensor::tensor<Real32>(
        {8, 8, 12, 12, 20, 20, 24, 24, 32, 32, 36, 36}, {1, 3, 2, 2});
    auto conv2d_5 = nn::tensor::tensor<Real32>(
        {36, 36, 52, 52, 84, 84, 100, 100, 132, 132, 148, 148}, {1, 3, 2, 2});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 6);

    if (result.size() == 6) {
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), conv2d_0), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[1]), conv2d_1), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[2]), conv2d_2), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[3]), conv2d_3), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[4]), conv2d_4), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[5]), conv2d_5), 0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_ConstConv2D_WithLazyload) {
  std::fstream stream{"tests/models/conv2d.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto conv2d_0 = nn::tensor::tensor<Real32>(
        {8,  12, 20, 24, 32, 36, 8,  12, 20, 24, 32, 36,
         56, 60, 68, 72, 80, 84, 56, 60, 68, 72, 80, 84},
        {2, 2, 3, 2});
    auto conv2d_1 = nn::tensor::tensor<Real32>(
        {64, 72, 88, 96, 112, 120, 64, 72, 88, 96, 112, 120}, {1, 2, 3, 2});
    auto conv2d_2 = nn::tensor::tensor<Real32>(
        {8,  12, 20, 24, 32, 36, 8,  12, 20, 24, 32, 36,
         56, 60, 68, 72, 80, 84, 56, 60, 68, 72, 80, 84},
        {2, 2, 3, 2});
    auto conv2d_3 = nn::tensor::tensor<Real32>(
        {64, 72, 88, 96, 112, 120, 64, 72, 88, 96, 112, 120}, {1, 2, 3, 2});
    auto conv2d_4 = nn::tensor::tensor<Real32>(
        {8, 8, 12, 12, 20, 20, 24, 24, 32, 32, 36, 36}, {1, 3, 2, 2});
    auto conv2d_5 = nn::tensor::tensor<Real32>(
        {36, 36, 52, 52, 84, 84, 100, 100, 132, 132, 148, 148}, {1, 3, 2, 2});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 6);

    if (result.size() == 6) {
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), conv2d_0), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[1]), conv2d_1), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[2]), conv2d_2), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[3]), conv2d_3), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[4]), conv2d_4), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[5]), conv2d_5), 0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_CONST_CONV2D_NCHW_VALID || !CHECK_CASE
TEST(Invoke, TFGraph_ConstConv2D_NCHW_VALID_NoLazyload) {
  std::fstream stream{"tests/models/conv2d_nchw_valid.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto conv2d_0 = nn::tensor::tensor<Real32>(
        {72, 80, 96, 104, 120, 128, 72, 80, 96, 104, 120, 128}, {1, 2, 3, 2});
    auto conv2d_1 = nn::tensor::tensor<Real32>(
        {72,  80,  96,  104, 120, 128, 72,  80,  96,  104, 120, 128,
         264, 272, 288, 296, 312, 320, 264, 272, 288, 296, 312, 320},
        {2, 2, 3, 2});
    auto conv2d_2 = nn::tensor::tensor<Real32>(
        {12, 16, 24, 28, 36, 40, 12, 16, 24, 28, 36, 40,
         60, 64, 72, 76, 84, 88, 60, 64, 72, 76, 84, 88},
        {2, 2, 3, 2});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 3);

    if (result.size() == 3) {
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), conv2d_0), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[1]), conv2d_1), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[2]), conv2d_2), 0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_ConstConv2D_NCHW_VALID_WithLazyload) {
  std::fstream stream{"tests/models/conv2d_nchw_valid.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto conv2d_0 = nn::tensor::tensor<Real32>(
        {72, 80, 96, 104, 120, 128, 72, 80, 96, 104, 120, 128}, {1, 2, 3, 2});
    auto conv2d_1 = nn::tensor::tensor<Real32>(
        {72,  80,  96,  104, 120, 128, 72,  80,  96,  104, 120, 128,
         264, 272, 288, 296, 312, 320, 264, 272, 288, 296, 312, 320},
        {2, 2, 3, 2});
    auto conv2d_2 = nn::tensor::tensor<Real32>(
        {12, 16, 24, 28, 36, 40, 12, 16, 24, 28, 36, 40,
         60, 64, 72, 76, 84, 88, 60, 64, 72, 76, 84, 88},
        {2, 2, 3, 2});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 3);

    if (result.size() == 3) {
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), conv2d_0), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[1]), conv2d_1), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[2]), conv2d_2), 0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_CONST_CONV2D_NCHW_SAME || !CHECK_CASE
TEST(Invoke, TFGraph_ConstConv2D_NCHW_SAME_NoLazyload) {
  std::fstream stream{"tests/models/conv2d_nchw_same.pbtxt", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto conv2d_0 = nn::tensor::tensor<Real32>(
        {1161, 1251, 1236, 1296, 1161, 1251, 1236, 1296}, {1, 2, 2, 2});
    auto conv2d_1 = nn::tensor::tensor<Real32>(
        {1161, 1251, 1236, 1296, 1161, 1251, 1236, 1296, 4185, 4275, 3252, 3312,
         4185, 4275, 3252, 3312},
        {2, 2, 2, 2});
    auto conv2d_2 =
        nn::tensor::tensor<Real32>({51, 81, 188, 208, 51, 81, 188, 208, 387,
                                    417, 412, 432, 387, 417, 412, 432},
                                   {2, 2, 2, 2});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 3);

    if (result.size() == 3) {
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), conv2d_0), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[1]), conv2d_1), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[2]), conv2d_2), 0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_ConstConv2D_NCHW_SAME_WithLazyload) {
  std::fstream stream{"tests/models/conv2d_nchw_same.pbtxt", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto conv2d_0 = nn::tensor::tensor<Real32>(
        {1161, 1251, 1236, 1296, 1161, 1251, 1236, 1296}, {1, 2, 2, 2});
    auto conv2d_1 = nn::tensor::tensor<Real32>(
        {1161, 1251, 1236, 1296, 1161, 1251, 1236, 1296, 4185, 4275, 3252, 3312,
         4185, 4275, 3252, 3312},
        {2, 2, 2, 2});
    auto conv2d_2 =
        nn::tensor::tensor<Real32>({51, 81, 188, 208, 51, 81, 188, 208, 387,
                                    417, 412, 432, 387, 417, 412, 432},
                                   {2, 2, 2, 2});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 3);

    if (result.size() == 3) {
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), conv2d_0), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[1]), conv2d_1), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[2]), conv2d_2), 0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_CONST_CONV2D_NHWC_VALID || !CHECK_CASE
TEST(Invoke, TFGraph_ConstConv2D_NHWC_VALID_NoLazyload) {
  std::fstream stream{"tests/models/conv2d_nhwc_valid.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto conv2d_0 = nn::tensor::tensor<Real32>(
        {44, 44, 60, 60, 92, 92, 108, 108, 140, 140, 156, 156}, {1, 3, 2, 2});
    auto conv2d_1 = nn::tensor::tensor<Real32>(
        {44,  44,  60,  60,  92,  92,  108, 108, 140, 140, 156, 156,
         236, 236, 252, 252, 284, 284, 300, 300, 332, 332, 348, 348},
        {2, 3, 2, 2});
    auto conv2d_2 = nn::tensor::tensor<Real32>(
        {12, 12, 16, 16, 24, 24, 28, 28, 36, 36, 40, 40,
         60, 60, 64, 64, 72, 72, 76, 76, 84, 84, 88, 88},
        {2, 3, 2, 2});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 3);

    if (result.size() == 3) {
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), conv2d_0), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[1]), conv2d_1), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[2]), conv2d_2), 0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_ConstConv2D_NHWC_VALID_WithLazyload) {
  std::fstream stream{"tests/models/conv2d_nhwc_valid.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto conv2d_0 = nn::tensor::tensor<Real32>(
        {44, 44, 60, 60, 92, 92, 108, 108, 140, 140, 156, 156}, {1, 3, 2, 2});
    auto conv2d_1 = nn::tensor::tensor<Real32>(
        {44,  44,  60,  60,  92,  92,  108, 108, 140, 140, 156, 156,
         236, 236, 252, 252, 284, 284, 300, 300, 332, 332, 348, 348},
        {2, 3, 2, 2});
    auto conv2d_2 = nn::tensor::tensor<Real32>(
        {12, 12, 16, 16, 24, 24, 28, 28, 36, 36, 40, 40,
         60, 60, 64, 64, 72, 72, 76, 76, 84, 84, 88, 88},
        {2, 3, 2, 2});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 3);

    if (result.size() == 3) {
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), conv2d_0), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[1]), conv2d_1), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[2]), conv2d_2), 0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_CONST_CONV2D_NHWC_SAME || !CHECK_CASE
TEST(Invoke, TFGraph_ConstConv2D_NHWC_SAME_NoLazyload) {
  std::fstream stream{"tests/models/conv2d_nhwc_same.pbtxt", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto conv2d_0 = nn::tensor::tensor<Real32>(
        {441, 441, 711, 711, 1680, 1680, 1860, 1860}, {1, 2, 2, 2});
    auto conv2d_1 = nn::tensor::tensor<Real32>(
        {441, 441, 711, 711, 1680, 1680, 1860, 1860, 3465, 3465, 3735, 3735,
         3696, 3696, 3876, 3876},
        {2, 2, 2, 2});
    auto conv2d_2 =
        nn::tensor::tensor<Real32>({51, 51, 81, 81, 188, 188, 208, 208, 387,
                                    387, 417, 417, 412, 412, 432, 432},
                                   {2, 2, 2, 2});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 3);

    if (result.size() == 3) {
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), conv2d_0), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[1]), conv2d_1), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[2]), conv2d_2), 0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_ConstConv2D_NHWC_SAME_WithLazyload) {
  std::fstream stream{"tests/models/conv2d_nhwc_same.pbtxt", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto conv2d_0 = nn::tensor::tensor<Real32>(
        {441, 441, 711, 711, 1680, 1680, 1860, 1860}, {1, 2, 2, 2});
    auto conv2d_1 = nn::tensor::tensor<Real32>(
        {441, 441, 711, 711, 1680, 1680, 1860, 1860, 3465, 3465, 3735, 3735,
         3696, 3696, 3876, 3876},
        {2, 2, 2, 2});
    auto conv2d_2 =
        nn::tensor::tensor<Real32>({51, 51, 81, 81, 188, 188, 208, 208, 387,
                                    387, 417, 417, 412, 412, 432, 432},
                                   {2, 2, 2, 2});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 3);

    if (result.size() == 3) {
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), conv2d_0), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[1]), conv2d_1), 0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[2]), conv2d_2), 0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_CONST_AUDIO_SPECTROGRAM || !CHECK_CASE
TEST(Invoke, TFGraph_ConstAudioSpectrogram_NoLazyload) {
  std::fstream stream{"tests/models/audio_spectrogram.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto audio_spectrogram_0 = nn::tensor::tensor<Real32>(
        {1, 1, 1, 1.6653345e-16, 1, 1.6653345e-16, 1, 1, 1, 1.6653345e-16, 1,
         1.6653345e-16, 1, 1, 1},
        {1, 5, 3});
    auto audio_spectrogram_1 =
        nn::tensor::tensor<Real32>({0, 1, 2, 1, 0}, {1, 1, 5});
    auto audio_spectrogram_2 = nn::tensor::tensor<Real32>(
        {0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0}, {1, 7, 2});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 3);

    if (result.size() == 3) {
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), audio_spectrogram_0),
                0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[1]), audio_spectrogram_1),
                0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[2]), audio_spectrogram_2),
                0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_ConstAudioSpectrogram_WithLazyload) {
  std::fstream stream{"tests/models/audio_spectrogram.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto audio_spectrogram_0 = nn::tensor::tensor<Real32>(
        {1, 1, 1, 1.6653345e-16, 1, 1.6653345e-16, 1, 1, 1, 1.6653345e-16, 1,
         1.6653345e-16, 1, 1, 1},
        {1, 5, 3});
    auto audio_spectrogram_1 =
        nn::tensor::tensor<Real32>({0, 1, 2, 1, 0}, {1, 1, 5});
    auto audio_spectrogram_2 = nn::tensor::tensor<Real32>(
        {0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0}, {1, 7, 2});
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 3);

    if (result.size() == 3) {
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), audio_spectrogram_0),
                0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[1]), audio_spectrogram_1),
                0);
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[2]), audio_spectrogram_2),
                0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_CONST_MFCC || !CHECK_CASE
TEST(Invoke, TFGraph_ConstMfcc_NoLazyload) {
  std::fstream stream{"tests/models/mfcc.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW &self) {
    auto expected = nn::tensor::tensor<Real32>(
        {2.9139698029e+01, -6.4156856537e+00, -6.1903023720e-01,
         -9.6778661013e-01, -2.6819854975e-01, -4.0906998515e-01,
         -1.5614774823e-01, -2.3203134537e-01, -1.0481504351e-01,
         -1.5430282056e-01, -7.6979078352e-02, -1.0806099325e-01,
         -6.0475967824e-02},
        {1, 1, 13});

    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_FALSE(self.error());
    EXPECT_EQ(result.size(), 1);

    if (result.size() == 1)
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_ConstMfcc_WithLazyload) {
  std::fstream stream{"tests/models/mfcc.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW &self) {
    auto expected = nn::tensor::tensor<Real32>(
        {2.9139698029e+01, -6.4156856537e+00, -6.1903023720e-01,
         -9.6778661013e-01, -2.6819854975e-01, -4.0906998515e-01,
         -1.5614774823e-01, -2.3203134537e-01, -1.0481504351e-01,
         -1.5430282056e-01, -7.6979078352e-02, -1.0806099325e-01,
         -6.0475967824e-02},
        {1, 1, 13});

    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_FALSE(self.error());
    EXPECT_EQ(result.size(), 1);

    if (result.size() == 1)
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_CONST_SOFTMAX || !CHECK_CASE
TEST(Invoke, TFGraph_ConstSoftmax_NoLazyload) {
  std::fstream stream{"tests/models/softmax.pbtxt", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
   #if NDEBUG
    auto expected =
        nn::tensor::tensor<Real32>({0.30060959, 0.33222499, 0.36716536});
   #else
    auto expected =
        nn::tensor::tensor<Real32>({0.3006096184, 0.3322249949, 0.3671653867});
   #endif
    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_FALSE(self.error());
    EXPECT_EQ(result.size(), 1);

    if (result.size() == 1)
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_ConstSoftmax_WithLazyload) {
  std::fstream stream{"tests/models/softmax.pbtxt", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
   #if NDEBUG
    auto expected =
        nn::tensor::tensor<Real32>({0.30060959, 0.33222499, 0.36716536});
   #else
    auto expected =
        nn::tensor::tensor<Real32>({0.3006096184, 0.3322249949, 0.3671653867});
   #endif

    auto result = self.invoke();

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_FALSE(self.error());
    EXPECT_EQ(result.size(), 1);

    if (result.size() == 1)
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_FUNCT_ADD || !CHECK_CASE
TEST(Invoke, TFGraph_FunctAdd_NoLazyload) {
  std::fstream stream{"tests/models/fadd.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto X = nn::tensor::ones({3, 4}, Real32_t, &self.interface());
    auto Y = nn::tensor::ones({3, 4}, Real32_t, &self.interface());
    auto expected = nn::tensor::tensor<Real32>(
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2}, {3, 4});

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(
        nn::utils::diff(std::get<1>(self.invoke({{"x", X}, {"x_1", Y}})[0]),
                        expected),
        0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_FunctAdd_WithLazyload) {
  std::fstream stream{"tests/models/fadd.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto X = nn::tensor::ones({3, 4}, Real32_t, &self.interface());
    auto Y = nn::tensor::ones({3, 4}, Real32_t, &self.interface());
    auto expected = nn::tensor::tensor<Real32>(
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2}, {3, 4});

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(
        nn::utils::diff(std::get<1>(self.invoke({{"x", X}, {"x_1", Y}})[0]),
                        expected),
        0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_FUNCT_MATMUL || !CHECK_CASE
TEST(Invoke, TFGraph_FunctMatMul_NoLazyload) {
  std::fstream stream{"tests/models/fmatmult.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto X = nn::tensor::ones({4, 3}, Real32_t, &self.interface());
    auto Y = nn::tensor::ones({3, 2}, Real32_t, &self.interface());
    auto expected =
        nn::tensor::tensor<Real32>({3, 3, 3, 3, 3, 3, 3, 3}, {4, 2});

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(nn::utils::diff(std::get<1>(self.invoke({{"x", X}, {"y", Y}})[0]),
                              expected),
              0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_FunctMatMul_WithLazyload) {
  std::fstream stream{"tests/models/fmatmult.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto X = nn::tensor::ones({4, 3}, Real32_t, &self.interface());
    auto Y = nn::tensor::ones({3, 2}, Real32_t, &self.interface());
    auto expected =
        nn::tensor::tensor<Real32>({3, 3, 3, 3, 3, 3, 3, 3}, {4, 2});

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(nn::utils::diff(std::get<1>(self.invoke({{"x", X}, {"y", Y}})[0]),
                              expected),
              0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_FUNCT_RELU || !CHECK_CASE
TEST(Invoke, TFGraph_FunctRelu_NoLazyload) {
  std::fstream stream{"tests/models/frelu.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto X = nn::tensor::ones({4, 3}, Real32_t, &self.interface());

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(nn::utils::diff(std::get<1>(self.invoke({{"x", X}})[0]), X), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_FunctRelu_WithLazyload) {
  std::fstream stream{"tests/models/frelu.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto X = nn::tensor::ones({4, 3}, Real32_t, &self.interface());

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(nn::utils::diff(std::get<1>(self.invoke({{"x", X}})[0]), X), 0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_FUNCT_RESHAPE || !CHECK_CASE
TEST(Invoke, TFGraph_FunctReshape_NoLazyload) {
  std::fstream stream{"tests/models/freshape.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto X = nn::tensor::ones({4, 3}, Real32_t, &self.interface());
    auto shape = nn::tensor::tensor<Int32>({3, 4});
    auto expected = nn::tensor::ones({3, 4}, Real32_t, &self.interface());

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(nn::utils::diff(
                  std::get<1>(self.invoke({{"input", X}, {"shape", shape}})[0]),
                  expected),
              0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_FunctReshape_WithLazyload) {
  std::fstream stream{"tests/models/freshape.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto X = nn::tensor::ones({4, 3}, Real32_t, &self.interface());
    auto shape = nn::tensor::tensor<Int32>({3, 4});
    auto expected = nn::tensor::ones({3, 4}, Real32_t, &self.interface());

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(nn::utils::diff(
                  std::get<1>(self.invoke({{"input", X}, {"shape", shape}})[0]),
                  expected),
              0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_FUNCT_MAXPOOL_FAIL || !CHECK_CASE
TEST(Invoke, TFGraph_FunctMaxPool_NoLazyload) {
  std::fstream stream{"tests/models/fmaxpool_fail.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto X = nn::tensor::ones({1, 4, 3, 1}, Real32_t, &self.interface());

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(self.invoke({{"input", X}}).size(), 0);
  });
  EXPECT_TRUE(interpreter.error().code != base::error::ENoError);
}

TEST(Invoke, TFGraph_FunctMaxPool_WithLazyload) {
  std::fstream stream{"tests/models/fmaxpool_fail.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto X = nn::tensor::ones({1, 4, 3, 1}, Real32_t, &self.interface());

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(self.invoke({{"input", X}}).size(), 0);
  });
  EXPECT_TRUE(interpreter.error().code != base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_FUNCT_DECODED_WAV || !CHECK_CASE
TEST(Invoke, TFGraph_FunctDecodedWav_NoLazyload) {
  std::fstream stream{"tests/models/fdecoded_wav.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto wav_data = nn::tensor::tensor<char*>(
        {(char*)"tests/samples/victim.wav"}, &self.interface());
    auto result = self.invoke({{"wav_data", wav_data}});

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 2);

    if (result.size() == 2) {
      EXPECT_EQ(nn::tensor::scalar<Real32>(std::get<1>(result[1])), 16000);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_FunctDecodedWav_WithLazyload) {
  std::fstream stream{"tests/models/fdecoded_wav.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto wav_data = nn::tensor::tensor<char*>(
        {(char*)"tests/samples/victim.wav"}, &self.interface());
    auto result = self.invoke({{"wav_data", wav_data}});

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(result.size(), 2);

    if (result.size() == 2) {
      EXPECT_EQ(nn::tensor::scalar<Real32>(std::get<1>(result[1])), 16000);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_FUNCT_SOFTMAX || !CHECK_CASE
TEST(Invoke, TFGraph_FuntSoftmax_NoLazyload) {
  std::fstream stream{"tests/models/fsoftmax.pbtxt", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto input = nn::tensor::tensor<Real32>({1.0, 1.1, 1.2}, &self.interface());
   #if NDEBUG
    auto expected =
        nn::tensor::tensor<Real32>({0.30060959, 0.33222502, 0.36716542});
   #else
    auto expected =
        nn::tensor::tensor<Real32>({0.3006096184, 0.3322250247, 0.3671654165});
   #endif

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);

    auto result = self.invoke({{"input", input}});

    if (result.size()){
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_FuntSoftmax_WithLazyload) {
  std::fstream stream{"tests/models/fsoftmax.pbtxt", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto input = nn::tensor::tensor<Real32>({1.0, 1.1, 1.2}, &self.interface());
   #if NDEBUG
    auto expected =
        nn::tensor::tensor<Real32>({0.30060959, 0.33222502, 0.36716542});
   #else
    auto expected =
        nn::tensor::tensor<Real32>({0.3006096184, 0.3322250247, 0.3671654165});
   #endif

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);

    auto result = self.invoke({{"input", input}});

    if (result.size()){
      EXPECT_EQ(nn::utils::diff(std::get<1>(result[0]), expected), 0);
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_FUNCT_AUDIO_SPECTROGRAM || !CHECK_CASE
TEST(Invoke, TFGraph_FunctAudioSpectrogram_NoLazyload) {
  std::fstream stream{"tests/models/faudio_spectrogram.pbtxt", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto input = nn::tensor::tensor<Real32>(
        nn::utils::range(
            1.0f, 100.0f, std::function<float(float)>([](float index) -> float {
              return -1 * index * ((int(index) % 2) + (int(index) % 2 - 1));
            })),
        {100, 1}, &self.interface());
    auto expected = nn::tensor::tensor<Real32>(
        {0, 0.0010743747, 0.0059795827, 0.024246406, 0.10861559, 0.70897084,
         11.499127, 1529.5068, 5184, 0, 0.0010743747, 0.0059795827, 0.024246406,
         0.10861559, 0.70897084, 11.499127, 52217.508, 207936},
        {1, 2, 9});

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(nn::utils::diff(
                  std::get<1>(self.invoke({{"wav_data", input}})[0]), expected),
              0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_FunctAudioSpectrogram_WithLazyload) {
  std::fstream stream{"tests/models/faudio_spectrogram.pbtxt", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto input = nn::tensor::tensor<Real32>(
        nn::utils::range(
            1.0f, 100.0f, std::function<float(float)>([](float index) -> float {
              return index * (-(int(index) % 2) + -1 * (int(index) % 2 - 1));
            })),
        {100, 1}, &self.interface());
    auto expected = nn::tensor::tensor<Real32>(
        {0, 0.0010743747, 0.0059795827, 0.024246406, 0.10861559, 0.70897084,
         11.499127, 1529.5068, 5184, 0, 0.0010743747, 0.0059795827, 0.024246406,
         0.10861559, 0.70897084, 11.499127, 52217.508, 207936},
        {1, 2, 9});

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(nn::utils::diff(
                  std::get<1>(self.invoke({{"wav_data", input}})[0]), expected),
              0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_INVOKE_TFGRAPH_FUNCT_MFCC || !CHECK_CASE
TEST(Invoke, TFGraph_FunctMfcc_NoLazyload) {
  std::fstream stream{"tests/models/fmfcc.pbtxt", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
#else
  nn::InterpreterW interpreter{stream, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto freq = nn::tensor::tensor<Int32>({22050});
    auto input = nn::tensor::tensor<Real32>(nn::utils::range(1.0f, 2 * 512.0f),
                                            {1, 2, 512}, &self.interface());
    auto expected = nn::tensor::tensor<Real32>(
        {2.9113586426e+01,  -6.4153504372e+00, -6.1919867992e-01,
         -9.6764630079e-01, -2.6856425405e-01, -4.0897506475e-01,
         -1.5616844594e-01, -2.3198813200e-01, -1.0542469472e-01,
         -1.5351770818e-01, -7.7686473727e-02, -1.0898180306e-01,
         -6.2494549900e-02, -9.1406807303e-02, -6.0569364578e-02,
         -7.6810963452e-02, -4.8301458359e-02, -6.0485724360e-02,
         -3.7342905998e-02, -4.8158973455e-02, -3.3160515130e-02,
         -4.6676896513e-02, -4.2822800577e-02, -5.5559080094e-02,
         -3.8741260767e-02, -2.7859924361e-02, -1.7849138007e-02,
         -3.0075028539e-02, -2.0239477977e-02, -1.6290662810e-02,
         -5.1081269048e-03, -1.0007441044e-02, -1.3250294141e-02,
         -2.0485172048e-02, -2.5216287002e-02, -1.4114616439e-02,
         -2.9798627365e-03, 1.0049007833e-02,  1.7178364098e-02,
         8.6459759623e-03,  4.0587924957e+01,  -3.5944519043e+00,
         3.2837167382e-02,  -4.1776859760e-01, -3.1046478543e-03,
         -1.5890207887e-01, -8.8625056669e-03, -8.6810790002e-02,
         -1.1250497773e-02, -5.8437198400e-02, -1.2485572137e-02,
         -4.1964959353e-02, -1.4549666084e-02, -4.1848413646e-02,
         -2.4803595617e-02, -4.0037736297e-02, -2.1519232541e-02,
         -3.2560471445e-02, -1.6729570925e-02, -2.6331406087e-02,
         -1.6910847276e-02, -2.9480798170e-02, -3.0315773562e-02,
         -4.2797122151e-02, -2.9978893697e-02, -1.8472308293e-02,
         -1.1022432707e-02, -2.2754848003e-02, -1.5314219519e-02,
         -1.0982658714e-02, -1.4712141128e-03, -5.8929375373e-03,
         -1.0414796881e-02, -1.7388217151e-02, -2.3257987574e-02,
         -1.2215257622e-02, -1.9742713775e-03, 1.0952386074e-02,
         1.7606623471e-02,  8.9865028858e-03},
         {1, 2, 40});

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(
        nn::utils::diff(std::get<1>(self.invoke(
                            {{"spectrogram", input}, {"frequence", freq}})[0]),
                        expected),
        0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Invoke, TFGraph_FunctMfcc_WithLazyload) {
  std::fstream stream{"tests/models/fmfcc.pbtxt", std::ios::in};
  ASSERT_TRUE(stream.is_open());

#if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy",
                           (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
#else
  nn::InterpreterW interpreter{stream, true, true};
#endif

  interpreter([&](nn::InterpreterW& self) {
    auto freq = nn::tensor::tensor<Int32>({22050});
    auto input = nn::tensor::tensor<Real32>(nn::utils::range(1.0f, 2 * 512.0f),
                                            {1, 2, 512}, &self.interface());
    auto expected = nn::tensor::tensor<Real32>(
         {2.9113586426e+01,  -6.4153504372e+00, -6.1919867992e-01,
         -9.6764630079e-01, -2.6856425405e-01, -4.0897506475e-01,
         -1.5616844594e-01, -2.3198813200e-01, -1.0542469472e-01,
         -1.5351770818e-01, -7.7686473727e-02, -1.0898180306e-01,
         -6.2494549900e-02, -9.1406807303e-02, -6.0569364578e-02,
         -7.6810963452e-02, -4.8301458359e-02, -6.0485724360e-02,
         -3.7342905998e-02, -4.8158973455e-02, -3.3160515130e-02,
         -4.6676896513e-02, -4.2822800577e-02, -5.5559080094e-02,
         -3.8741260767e-02, -2.7859924361e-02, -1.7849138007e-02,
         -3.0075028539e-02, -2.0239477977e-02, -1.6290662810e-02,
         -5.1081269048e-03, -1.0007441044e-02, -1.3250294141e-02,
         -2.0485172048e-02, -2.5216287002e-02, -1.4114616439e-02,
         -2.9798627365e-03, 1.0049007833e-02,  1.7178364098e-02,
         8.6459759623e-03,  4.0587924957e+01,  -3.5944519043e+00,
         3.2837167382e-02,  -4.1776859760e-01, -3.1046478543e-03,
         -1.5890207887e-01, -8.8625056669e-03, -8.6810790002e-02,
         -1.1250497773e-02, -5.8437198400e-02, -1.2485572137e-02,
         -4.1964959353e-02, -1.4549666084e-02, -4.1848413646e-02,
         -2.4803595617e-02, -4.0037736297e-02, -2.1519232541e-02,
         -3.2560471445e-02, -1.6729570925e-02, -2.6331406087e-02,
         -1.6910847276e-02, -2.9480798170e-02, -3.0315773562e-02,
         -4.2797122151e-02, -2.9978893697e-02, -1.8472308293e-02,
         -1.1022432707e-02, -2.2754848003e-02, -1.5314219519e-02,
         -1.0982658714e-02, -1.4712141128e-03, -5.8929375373e-03,
         -1.0414796881e-02, -1.7388217151e-02, -2.3257987574e-02,
         -1.2215257622e-02, -1.9742713775e-03, 1.0952386074e-02,
         1.7606623471e-02,  8.9865028858e-03},
        {1, 2, 40});

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(
        nn::utils::diff(std::get<1>(self.invoke(
                            {{"spectrogram", input}, {"frequence", freq}})[0]),
                        expected),
        0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#ifndef TEST_ALLTESTS_CPP_
#include <libbase/all.hpp>
#include <signal.h>

static void handler(int sig, siginfo_t *dont_care, void* dont_care_either){
  base::trace::backtrace(30);
  exit(-1);
}

int main(int argc, char** argv){
  using namespace base;

 #if APPLE || USE_BASE_UNITTEST
  base::testing::init(argc, (const char**)argv);
 #else
  ::testing::InitGoogleTest(&argc, argv);
 #endif

  struct sigaction sa;

  memset(&sa, 0, sizeof(struct sigaction));
  sigemptyset(&sa.sa_mask);

  sa.sa_flags     = SA_NODEFER;
  sa.sa_sigaction = handler;

  sigaction(SIGSEGV, &sa, NULL); /* ignore whether it works or not */

  base::config::start(argv, argc);
  base::config::set("console", base::Auto{}.set(std::string{"console.txt"}));
  base::config::set("monitor", base::Auto{}.set(true));

  base::config::set("metal", base::Auto{}.set(std::string{"bundle"}));
  base::config::set("metal.file", base::Auto{}.set(std::string{"metal.metalib"}));

  base::config::set("opencl", base::Auto{}.set(std::string{"auto"}));
  base::config::set("opencl.file", base::Auto{}.set(std::string{"opencl.clbin"}));

  base::config::set("cuda", base::Auto{}.set(std::string{"file"}));
  base::config::set("cuda.file", base::Auto{}.set(std::string{"opencl.cubin"}));

  if (config::get("metal").type().get() != typeid(std::string))
    return -1;
  nNet_LoadEverything();
  return base::config::finish(RUN_ALL_TESTS(), [](){ nNet_UnloadEverything(); });
}
#endif  // TEST_ALLTESTS_CPP_
#endif  // BOOST_TEST_MAIN
