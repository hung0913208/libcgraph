#ifndef BOOST_TEST_MAIN
#include "dummy.hpp"
#include "all.hpp"

#if APPLE || USE_BASE_UNITTEST
#include <libbase/unittest.hpp>
#else
#include <gtest/gtest.h>
#endif
#include <dirent.h>
#include <libbase/all.hpp>

#include <fstream>

using namespace base;

#if CHECK_CASE_MODEL_TFGRAPH_PERCEPTRON 
TEST(Model, TFGraph_Perceptron_NoLazyload){
  std::fstream stream{"tests/models/perceptron.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

 #if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy", (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream);
 #else
  nn::InterpreterW interpreter{stream, true};
 #endif

  interpreter([&](nn::InterpreterW& self) {
    auto input = nn::tensor::tensor<Real32>({1.0, 1.1, 1.2}, &self.interface());

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);

    for (auto& result: self.invoke({{"input", input}})){
      base::trace::console << std::get<0>(result) << " =\n"
                           << nn::utils::print(std::get<1>(result))
                           << base::trace::eol;
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Model, TFGraph_Perceptron_WithLazyload){
  std::fstream stream{"tests/models/perceptron.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

 #if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy", (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
 #else
  nn::InterpreterW interpreter{stream, true, true};
 #endif

  interpreter([&](nn::InterpreterW& self) {
    auto input = nn::tensor::tensor<Real32>({1.0, 1.1, 1.2}, &self.interface());

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);

    for (auto& result: self.invoke({{"input", input}})){
      base::trace::console << std::get<0>(result) << " =\n"
                           << nn::utils::print(std::get<1>(result))
                           << base::trace::eol;
    }
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#if CHECK_CASE_MODEL_TFGRAPH_SPEECH_RECOGNITION_CONV2D || (NDEBUG && !CHECK_CASE)
TEST(Model, TFGraph_Speech_Recognition_Conv2D_NoLazyload){
  std::fstream stream{"tests/models/speechai.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

 #if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true};

  interpreter.assignDevice("dummy", (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
 #else
  nn::InterpreterW interpreter{stream, true};
 #endif

  interpreter([&](nn::InterpreterW& self) {
    auto input = nn::tensor::tensor<char*>({ (char*)"tests/samples/yes.wav"}, 
                                             &self.interface());
   #if NDEBUG
    auto expected = nn::tensor::tensor<Real32>(
        {3.751737587e-09, 0.001292244764, 0.9849011898, 0.006376835983,
         2.366850538e-08, 0.0002707100939, 0.007051775698, 3.125795774e-05,
         7.187461648e-08, 9.557074918e-06, 1.295448101e-06, 6.507908256e-05},
        {1, 12}, &self.interface());
   #else
    auto expected = nn::tensor::tensor<Real32>(
        {3.751854383e-09, 0.001292274334, 0.9849008322, 0.006377107464,
         2.36694202e-08, 0.0002707167296, 0.007051826455, 3.125862713e-05,
         7.187691153e-08, 9.55728683e-06, 1.295488005e-06, 6.508054503e-05},
        {1, 12}, &self.interface());
   #endif

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(nn::utils::diff(
                  std::get<1>(self.invoke({{"wav_data", input}})[0]), expected),
              0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}

TEST(Model, TFGraph_Speech_Recognition_Conv2D_WithLazyload){
  std::fstream stream{"tests/models/speechai.pb", std::ios::in};
  ASSERT_TRUE(stream.is_open());

 #if CHECK_CASE_INVOKE_WITH_DUMMY
  nn::InterpreterW interpreter{"", true, true};

  interpreter.assignDevice("dummy", (new nn::device::custom::CPDummy{})->interface());
  interpreter.load(stream, true);
 #else
  nn::InterpreterW interpreter{stream, true, true};
 #endif

  interpreter([&](nn::InterpreterW& self) {
    auto input = nn::tensor::tensor<char*>({ (char*)"tests/samples/yes.wav"}, 
                                             &self.interface());
   #if NDEBUG
    auto expected = nn::tensor::tensor<Real32>(
        {3.751737587e-09, 0.001292244764, 0.9849011898, 0.006376835983,
         2.366850538e-08, 0.0002707100939, 0.007051775698, 3.125795774e-05,
         7.187461648e-08, 9.557074918e-06, 1.295448101e-06, 6.507908256e-05},
        {1, 12}, &self.interface());
   #else
    auto expected = nn::tensor::tensor<Real32>(
        {3.751854383e-09, 0.001292274334, 0.9849008322, 0.006377107464,
         2.36694202e-08, 0.0002707167296, 0.007051826455, 3.125862713e-05,
         7.187691153e-08, 9.55728683e-06, 1.295488005e-06, 6.508054503e-05},
        {1, 12}, &self.interface());
   #endif

    EXPECT_TRUE(self.operators().size() > 0);
    EXPECT_TRUE(self.flow().size() > 0);
    EXPECT_EQ(nn::utils::diff(
                  std::get<1>(self.invoke({{"wav_data", input}})[0]), expected),
              0);
  });
  EXPECT_TRUE(interpreter.error().code == base::error::ENoError);
}
#endif

#ifndef TEST_ALLTESTS_CPP_
#include <libbase/all.hpp>
#include <signal.h>

static void handler(int sig, siginfo_t *dont_care, void* dont_care_either){
  base::trace::backtrace(30);
  exit(-1);
}

int main(int argc, char** argv){
  using namespace base;

 #if APPLE || USE_BASE_UNITTEST
  base::testing::init(argc, (const char**)argv);
 #else
  ::testing::InitGoogleTest(&argc, argv);
 #endif

  // struct sigaction sa;

  // memset(&sa, 0, sizeof(struct sigaction));
  // sigemptyset(&sa.sa_mask);

  // sa.sa_flags     = SA_NODEFER;
  // sa.sa_sigaction = handler;

  // sigaction(SIGSEGV, &sa, NULL); /* ignore whether it works or not */

  base::config::start(argv, argc);
  base::config::set("console", base::Auto{}.set(std::string{"console.txt"}));
  base::config::set("monitor", base::Auto{}.set(true));

  base::config::set("metal", base::Auto{}.set(std::string{"bundle"}));
  base::config::set("metal.file", base::Auto{}.set(std::string{"metal.metalib"}));

  base::config::set("opencl", base::Auto{}.set(std::string{"auto"}));
  base::config::set("opencl.file", base::Auto{}.set(std::string{"opencl.clbin"}));

  base::config::set("cuda", base::Auto{}.set(std::string{"file"}));
  base::config::set("cuda.file", base::Auto{}.set(std::string{"opencl.cubin"}));

  if (config::get("metal").type().get() != typeid(std::string))
    return -1;
  nNet_LoadEverything();
  return base::config::finish(RUN_ALL_TESTS(), [](){ nNet_UnloadEverything(); });
}
#endif // TEST_ALLTESTS_CPP_
#endif // BOOST_TEST_MAIN
