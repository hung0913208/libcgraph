#ifndef BOOST_TEST_MAIN
#include <unistd.h>
#include "all.hpp"

#if APPLE || USE_BASE_UNITTEST
#include <libbase/unittest.hpp>
#else
#include <gtest/gtest.h>
#endif

#define LOOP 2
#define SLOWDOWN 2

#if CHECK_CASE_STREMING_SPEECHAI_CONV2D || CHECK_CASE_STREMING || (NDEBUG && !CHECK_CASE)
TEST(Streaming, SpeechConv2D) {
  auto interpreter = nNet_CreateInterpreterFromFile("tests/models/speechai.pb");
  auto gate = nNet_CreateCGates(interpreter, 1);

  EXPECT_TRUE(interpreter != NULL);
  for (auto i = 0; i < LOOP; ++i){
    auto yes =
      nn::tensor::tensor<char*>({(char*)"tests/samples/yes.wav"}, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "wav_data", yes, 0));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
   #if NDEBUG
    auto expected = nn::tensor::tensor<Real32>(
        {3.751737587e-09, 0.001292244764, 0.9849011898, 0.006376835983,
         2.366850538e-08, 0.0002707100939, 0.007051775698, 3.125795774e-05,
         7.187461648e-08, 9.557074918e-06, 1.295448101e-06, 6.507908256e-05},
        {1, 12}, result.interpreter);
   #else
    auto expected = nn::tensor::tensor<Real32>(
        {3.751854383e-09, 0.001292274334, 0.9849008322, 0.006377107464,
         2.36694202e-08, 0.0002707167296, 0.007051826455, 3.125862713e-05,
         7.187691153e-08, 9.55728683e-06, 1.295488005e-06, 6.508054503e-05},
        {1, 12}, result.interpreter);
   #endif
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i){
    auto yes =
      nn::tensor::tensor<char*>({(char*)"tests/samples/yes.wav"}, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "wav_data", yes, 0));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
   #if NDEBUG
    auto expected = nn::tensor::tensor<Real32>(
        {3.751737587e-09, 0.001292244764, 0.9849011898, 0.006376835983,
         2.366850538e-08, 0.0002707100939, 0.007051775698, 3.125795774e-05,
         7.187461648e-08, 9.557074918e-06, 1.295448101e-06, 6.507908256e-05},
        {1, 12}, result.interpreter);
   #else
    auto expected = nn::tensor::tensor<Real32>(
        {3.751854383e-09, 0.001292274334, 0.9849008322, 0.006377107464,
         2.36694202e-08, 0.0002707167296, 0.007051826455, 3.125862713e-05,
         7.187691153e-08, 9.55728683e-06, 1.295488005e-06, 6.508054503e-05},
        {1, 12}, result.interpreter);
   #endif
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i) {
    auto yes =
      nn::tensor::tensor<char*>({(char*)"tests/samples/yes.wav"}, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "wav_data", yes, 0));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
    sleep(SLOWDOWN);  // <-- slowdown 2s to make a system hange
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
   #if NDEBUG
    auto expected = nn::tensor::tensor<Real32>(
        {3.751737587e-09, 0.001292244764, 0.9849011898, 0.006376835983,
         2.366850538e-08, 0.0002707100939, 0.007051775698, 3.125795774e-05,
         7.187461648e-08, 9.557074918e-06, 1.295448101e-06, 6.507908256e-05},
        {1, 12}, result.interpreter);
   #else
    auto expected = nn::tensor::tensor<Real32>(
        {3.751854383e-09, 0.001292274334, 0.9849008322, 0.006377107464,
         2.36694202e-08, 0.0002707167296, 0.007051826455, 3.125862713e-05,
         7.187691153e-08, 9.55728683e-06, 1.295488005e-06, 6.508054503e-05},
        {1, 12}, result.interpreter);
   #endif
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));
  EXPECT_TRUE(nNet_RemoveCGates(&gate));
  EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
}
#endif

#if CHECK_CASE_STREMING_ADD || CHECK_CASE_STREMING || (NDEBUG && !CHECK_CASE)
TEST(Streaming, Add) {
  auto interpreter = nNet_CreateInterpreterFromFile("tests/models/fadd.pb");
  auto gate = nNet_CreateCGates(interpreter, 2);
  EXPECT_TRUE(interpreter != NULL);

  for (auto i = 0; i < LOOP; ++i){
    auto X = nn::tensor::ones({3, 4}, Real32_t, interpreter);
    auto Y = nn::tensor::ones({3, 4}, Real32_t, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "x", X, 0));
    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "x_1", Y, 1));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>(
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2}, {3, 4}, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i){
    auto X = nn::tensor::ones({3, 4}, Real32_t, interpreter);
    auto Y = nn::tensor::ones({3, 4}, Real32_t, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "x", X, 0));
    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "x_1", Y, 1));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>(
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2}, {3, 4}, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i) {
    auto X = nn::tensor::ones({3, 4}, Real32_t, interpreter);
    auto Y = nn::tensor::ones({3, 4}, Real32_t, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "x", X, 0));
    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "x_1", Y, 1));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
    sleep(SLOWDOWN);  // <-- slowdown 2s to make a system hange
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>(
        {2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2}, {3, 4}, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));
  EXPECT_TRUE(nNet_RemoveCGates(&gate));
  EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
}
#endif

#if CHECK_CASE_STREMING_MATMUL || CHECK_CASE_STREMING || (NDEBUG && !CHECK_CASE)
TEST(Streaming, MatMul) {
  auto interpreter = nNet_CreateInterpreterFromFile("tests/models/fmatmult.pb");
  auto gate = nNet_CreateCGates(interpreter, 2);
  EXPECT_TRUE(interpreter != NULL);


  for (auto i = 0; i < LOOP; ++i){
    auto X = nn::tensor::ones({4, 3}, Real32_t, interpreter);
    auto Y = nn::tensor::ones({3, 2}, Real32_t, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "x", X, 0));
    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "y", Y, 1));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>({3, 3, 3, 3, 3, 3, 3, 3}, {4, 2},
                                               result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i){
    auto X = nn::tensor::ones({4, 3}, Real32_t, interpreter);
    auto Y = nn::tensor::ones({3, 2}, Real32_t, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "x", X, 0));
    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "y", Y, 1));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>({3, 3, 3, 3, 3, 3, 3, 3}, {4, 2},
                                               result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i) {
    auto X = nn::tensor::ones({4, 3}, Real32_t, interpreter);
    auto Y = nn::tensor::ones({3, 2}, Real32_t, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "x", X, 0));
    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "y", Y, 1));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
    sleep(SLOWDOWN);  // <-- slowdown 2s to make a system hange
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>({3, 3, 3, 3, 3, 3, 3, 3}, {4, 2},
                                               result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));
  EXPECT_TRUE(nNet_RemoveCGates(&gate));
  EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
}
#endif

#if CHECK_CASE_STREMING_RELU || CHECK_CASE_STREMING || (NDEBUG && !CHECK_CASE)
TEST(Streaming, Relu) {
  auto interpreter = nNet_CreateInterpreterFromFile("tests/models/frelu.pb");
  auto gate = nNet_CreateCGates(interpreter, 1);

  EXPECT_TRUE(interpreter != NULL);

  for (auto i = 0; i < LOOP; ++i){
    auto X = nn::tensor::ones({4, 3}, Real32_t, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "x", X, 0));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::ones({4, 3}, Real32_t, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i){
    auto X = nn::tensor::ones({4, 3}, Real32_t, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "x", X, 0));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::ones({4, 3}, Real32_t, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i) {    
    auto X = nn::tensor::ones({4, 3}, Real32_t, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "x", X, 0));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
    sleep(SLOWDOWN);  // <-- slowdown 2s to make a system hange
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::ones({4, 3}, Real32_t, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));
  EXPECT_TRUE(nNet_RemoveCGates(&gate));
  EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
}
#endif

#if CHECK_CASE_STREMING_RESHAPE || CHECK_CASE_STREMING || (NDEBUG && !CHECK_CASE)
TEST(Streaming, Reshape) {
  auto interpreter = nNet_CreateInterpreterFromFile("tests/models/freshape.pb");
  auto gate = nNet_CreateCGates(interpreter, 2);
  EXPECT_TRUE(interpreter != NULL);

  for (auto i = 0; i < LOOP; ++i){
    auto X = nn::tensor::ones({4, 3}, Real32_t, interpreter);
    auto shape = nn::tensor::tensor<Int32>({3, 4});

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "input", X, 0));
    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "shape", shape, 1));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::ones({3, 4}, Real32_t, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i){
    auto X = nn::tensor::ones({4, 3}, Real32_t, interpreter);
    auto shape = nn::tensor::tensor<Int32>({3, 4});

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "input", X, 0));
    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "shape", shape, 1));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::ones({3, 4}, Real32_t, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i) {    
    auto X = nn::tensor::ones({4, 3}, Real32_t, interpreter);
    auto shape = nn::tensor::tensor<Int32>({3, 4});

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "input", X, 0));
    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "shape", shape, 1));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
    sleep(SLOWDOWN);  // <-- slowdown 2s to make a system hange
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::ones({3, 4}, Real32_t, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));
  EXPECT_TRUE(nNet_RemoveCGates(&gate));
  EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
}
#endif

#if CHECK_CASE_STREMING_MAXPOOL
TEST(Streaming, MaxPool) {
  auto interpreter = nNet_CreateInterpreterFromFile("tests/models/fmatmult.pb");
  auto gate = nNet_CreateCGates(interpreter, 2);
  EXPECT_TRUE(interpreter != NULL);


  for (auto i = 0; i < LOOP; ++i){
    auto X = nn::tensor::ones({4, 3}, Real32_t, interpreter);
    auto Y = nn::tensor::ones({3, 2}, Real32_t, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "x", X, 0));
    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "y", Y, 1));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>({3, 3, 3, 3, 3, 3, 3, 3}, {4, 2},
                                               result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i){
    auto X = nn::tensor::ones({4, 3}, Real32_t, interpreter);
    auto Y = nn::tensor::ones({3, 2}, Real32_t, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "x", X, 0));
    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "y", Y, 1));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>({3, 3, 3, 3, 3, 3, 3, 3}, {4, 2},
                                               result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i) {
    auto X = nn::tensor::ones({4, 3}, Real32_t, interpreter);
    auto Y = nn::tensor::ones({3, 2}, Real32_t, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "x", X, 0));
    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "y", Y, 1));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
    sleep(SLOWDOWN);  // <-- slowdown 2s to make a system hange
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>({3, 3, 3, 3, 3, 3, 3, 3}, {4, 2},
                                               result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));
  EXPECT_TRUE(nNet_RemoveCGates(&gate));
  EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
}
#endif

#if CHECK_CASE_STREMING_SOFTMAX || CHECK_CASE_STREMING || (NDEBUG && !CHECK_CASE)
TEST(Streaming, Softmax) {
  auto interpreter =
      nNet_CreateInterpreterFromFile("tests/models/fsoftmax.pbtxt");
  auto gate = nNet_CreateCGates(interpreter, 1);
  EXPECT_TRUE(interpreter != NULL);

  for (auto i = 0; i < LOOP; ++i){
    auto input = nn::tensor::tensor<Real32>({1.0, 1.1, 1.2}, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "input", input, 0));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>(
        {0.30060959, 0.33222502, 0.36716542}, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i){
    auto input = nn::tensor::tensor<Real32>({1.0, 1.1, 1.2}, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "input", input, 0));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>(
        {0.30060959, 0.33222502, 0.36716542}, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i) {
    auto input = nn::tensor::tensor<Real32>({1.0, 1.1, 1.2}, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "input", input, 0));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
    sleep(SLOWDOWN);  // <-- slowdown 2s to make a system hange
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>(
        {0.30060959, 0.33222502, 0.36716542}, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));
  EXPECT_TRUE(nNet_RemoveCGates(&gate));
  EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
}
#endif

#if CHECK_CASE_STREMING_AUDIO_SPECTROGRAM || CHECK_CASE_STREMING || (NDEBUG && !CHECK_CASE)
TEST(Streaming, AudioSpectrogram) {
  auto interpreter =
      nNet_CreateInterpreterFromFile("tests/models/faudio_spectrogram.pbtxt");
  auto gate = nNet_CreateCGates(interpreter, 1);
  EXPECT_TRUE(interpreter != NULL);

  for (auto i = 0; i < LOOP; ++i){
    auto input = nn::tensor::tensor<Real32>(
      nn::utils::range(
          1.0f, 100.0f, std::function<float(float)>([](float index) -> float {
            return -1 * index * ((int(index) % 2) + (int(index) % 2 - 1));
          })),
      {100, 1}, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "wav_data", input, 0));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>(
        {0, 0.0010743747, 0.0059795827, 0.024246406, 0.10861559, 0.70897084,
         11.499127, 1529.5068, 5184, 0, 0.0010743747, 0.0059795827, 0.024246406,
         0.10861559, 0.70897084, 11.499127, 52217.508, 207936},
        {1, 2, 9}, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i){
    auto input = nn::tensor::tensor<Real32>(
      nn::utils::range(
          1.0f, 100.0f, std::function<float(float)>([](float index) -> float {
            return -1 * index * ((int(index) % 2) + (int(index) % 2 - 1));
          })),
      {100, 1}, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "wav_data", input, 0));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>(
        {0, 0.0010743747, 0.0059795827, 0.024246406, 0.10861559, 0.70897084,
         11.499127, 1529.5068, 5184, 0, 0.0010743747, 0.0059795827, 0.024246406,
         0.10861559, 0.70897084, 11.499127, 52217.508, 207936},
        {1, 2, 9}, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i) {
    auto input = nn::tensor::tensor<Real32>(
      nn::utils::range(
          1.0f, 100.0f, std::function<float(float)>([](float index) -> float {
            return -1 * index * ((int(index) % 2) + (int(index) % 2 - 1));
          })),
      {100, 1}, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "wav_data", input, 0));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
    sleep(SLOWDOWN);  // <-- slowdown 2s to make a system hange
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>(
        {0, 0.0010743747, 0.0059795827, 0.024246406, 0.10861559, 0.70897084,
         11.499127, 1529.5068, 5184, 0, 0.0010743747, 0.0059795827, 0.024246406,
         0.10861559, 0.70897084, 11.499127, 52217.508, 207936},
        {1, 2, 9}, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));
  EXPECT_TRUE(nNet_RemoveCGates(&gate));
  EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
}
#endif

#if CHECK_CASE_STREMING_MFCC || CHECK_CASE_STREMING || (NDEBUG && !CHECK_CASE)
TEST(Streaming, Mfcc) {
  auto interpreter = nNet_CreateInterpreterFromFile("tests/models/fmfcc.pbtxt");
  auto gate = nNet_CreateCGates(interpreter, 2);
  EXPECT_TRUE(interpreter != NULL);

  for (auto i = 0; i < LOOP; ++i){
    auto freq = nn::tensor::tensor<Int32>({22050}, interpreter);
    auto input = nn::tensor::tensor<Real32>(nn::utils::range(1.0f, 2 * 512.0f),
                                          {1, 2, 512}, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "spectrogram", input, 0));
    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "frequence", freq, 1));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>(
         {2.9113586426e+01,  -6.4153504372e+00, -6.1919867992e-01,
         -9.6764630079e-01, -2.6856425405e-01, -4.0897506475e-01,
         -1.5616844594e-01, -2.3198813200e-01, -1.0542469472e-01,
         -1.5351770818e-01, -7.7686473727e-02, -1.0898180306e-01,
         -6.2494549900e-02, -9.1406807303e-02, -6.0569364578e-02,
         -7.6810963452e-02, -4.8301458359e-02, -6.0485724360e-02,
         -3.7342905998e-02, -4.8158973455e-02, -3.3160515130e-02,
         -4.6676896513e-02, -4.2822800577e-02, -5.5559080094e-02,
         -3.8741260767e-02, -2.7859924361e-02, -1.7849138007e-02,
         -3.0075028539e-02, -2.0239477977e-02, -1.6290662810e-02,
         -5.1081269048e-03, -1.0007441044e-02, -1.3250294141e-02,
         -2.0485172048e-02, -2.5216287002e-02, -1.4114616439e-02,
         -2.9798627365e-03, 1.0049007833e-02,  1.7178364098e-02,
         8.6459759623e-03,  4.0587924957e+01,  -3.5944519043e+00,
         3.2837167382e-02,  -4.1776859760e-01, -3.1046478543e-03,
         -1.5890207887e-01, -8.8625056669e-03, -8.6810790002e-02,
         -1.1250497773e-02, -5.8437198400e-02, -1.2485572137e-02,
         -4.1964959353e-02, -1.4549666084e-02, -4.1848413646e-02,
         -2.4803595617e-02, -4.0037736297e-02, -2.1519232541e-02,
         -3.2560471445e-02, -1.6729570925e-02, -2.6331406087e-02,
         -1.6910847276e-02, -2.9480798170e-02, -3.0315773562e-02,
         -4.2797122151e-02, -2.9978893697e-02, -1.8472308293e-02,
         -1.1022432707e-02, -2.2754848003e-02, -1.5314219519e-02,
         -1.0982658714e-02, -1.4712141128e-03, -5.8929375373e-03,
         -1.0414796881e-02, -1.7388217151e-02, -2.3257987574e-02,
         -1.2215257622e-02, -1.9742713775e-03, 1.0952386074e-02,
         1.7606623471e-02,  8.9865028858e-03},
        {1, 2, 40}, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i){
    auto freq = nn::tensor::tensor<Int32>({22050}, interpreter);
    auto input = nn::tensor::tensor<Real32>(nn::utils::range(1.0f, 2 * 512.0f),
                                          {1, 2, 512}, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "spectrogram", input, 0));
    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "frequence", freq, 1));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>(
         {2.9113586426e+01,  -6.4153504372e+00, -6.1919867992e-01,
         -9.6764630079e-01, -2.6856425405e-01, -4.0897506475e-01,
         -1.5616844594e-01, -2.3198813200e-01, -1.0542469472e-01,
         -1.5351770818e-01, -7.7686473727e-02, -1.0898180306e-01,
         -6.2494549900e-02, -9.1406807303e-02, -6.0569364578e-02,
         -7.6810963452e-02, -4.8301458359e-02, -6.0485724360e-02,
         -3.7342905998e-02, -4.8158973455e-02, -3.3160515130e-02,
         -4.6676896513e-02, -4.2822800577e-02, -5.5559080094e-02,
         -3.8741260767e-02, -2.7859924361e-02, -1.7849138007e-02,
         -3.0075028539e-02, -2.0239477977e-02, -1.6290662810e-02,
         -5.1081269048e-03, -1.0007441044e-02, -1.3250294141e-02,
         -2.0485172048e-02, -2.5216287002e-02, -1.4114616439e-02,
         -2.9798627365e-03, 1.0049007833e-02,  1.7178364098e-02,
         8.6459759623e-03,  4.0587924957e+01,  -3.5944519043e+00,
         3.2837167382e-02,  -4.1776859760e-01, -3.1046478543e-03,
         -1.5890207887e-01, -8.8625056669e-03, -8.6810790002e-02,
         -1.1250497773e-02, -5.8437198400e-02, -1.2485572137e-02,
         -4.1964959353e-02, -1.4549666084e-02, -4.1848413646e-02,
         -2.4803595617e-02, -4.0037736297e-02, -2.1519232541e-02,
         -3.2560471445e-02, -1.6729570925e-02, -2.6331406087e-02,
         -1.6910847276e-02, -2.9480798170e-02, -3.0315773562e-02,
         -4.2797122151e-02, -2.9978893697e-02, -1.8472308293e-02,
         -1.1022432707e-02, -2.2754848003e-02, -1.5314219519e-02,
         -1.0982658714e-02, -1.4712141128e-03, -5.8929375373e-03,
         -1.0414796881e-02, -1.7388217151e-02, -2.3257987574e-02,
         -1.2215257622e-02, -1.9742713775e-03, 1.0952386074e-02,
         1.7606623471e-02,  8.9865028858e-03},
        {1, 2, 40}, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));

  for (auto i = 0; i < LOOP; ++i) {
    auto freq = nn::tensor::tensor<Int32>({22050}, interpreter);
    auto input = nn::tensor::tensor<Real32>(nn::utils::range(1.0f, 2 * 512.0f),
                                          {1, 2, 512}, interpreter);

    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "spectrogram", input, 0));
    EXPECT_TRUE(nNet_AssignTensorToCGates(&gate, "frequence", freq, 1));
    EXPECT_TRUE(!nNet_InvokeGraph(interpreter, &gate));
    sleep(SLOWDOWN);  // <-- slowdown 2s to make a system hange
  }

  EXPECT_TRUE(!nNet_FlushGraph(interpreter, [](CGates result) {
    auto expected = nn::tensor::tensor<Real32>(
        {2.9113586426e+01,  -6.4153504372e+00, -6.1919867992e-01,
         -9.6764630079e-01, -2.6856425405e-01, -4.0897506475e-01,
         -1.5616844594e-01, -2.3198813200e-01, -1.0542469472e-01,
         -1.5351770818e-01, -7.7686473727e-02, -1.0898180306e-01,
         -6.2494549900e-02, -9.1406807303e-02, -6.0569364578e-02,
         -7.6810963452e-02, -4.8301458359e-02, -6.0485724360e-02,
         -3.7342905998e-02, -4.8158973455e-02, -3.3160515130e-02,
         -4.6676896513e-02, -4.2822800577e-02, -5.5559080094e-02,
         -3.8741260767e-02, -2.7859924361e-02, -1.7849138007e-02,
         -3.0075028539e-02, -2.0239477977e-02, -1.6290662810e-02,
         -5.1081269048e-03, -1.0007441044e-02, -1.3250294141e-02,
         -2.0485172048e-02, -2.5216287002e-02, -1.4114616439e-02,
         -2.9798627365e-03, 1.0049007833e-02,  1.7178364098e-02,
         8.6459759623e-03,  4.0587924957e+01,  -3.5944519043e+00,
         3.2837167382e-02,  -4.1776859760e-01, -3.1046478543e-03,
         -1.5890207887e-01, -8.8625056669e-03, -8.6810790002e-02,
         -1.1250497773e-02, -5.8437198400e-02, -1.2485572137e-02,
         -4.1964959353e-02, -1.4549666084e-02, -4.1848413646e-02,
         -2.4803595617e-02, -4.0037736297e-02, -2.1519232541e-02,
         -3.2560471445e-02, -1.6729570925e-02, -2.6331406087e-02,
         -1.6910847276e-02, -2.9480798170e-02, -3.0315773562e-02,
         -4.2797122151e-02, -2.9978893697e-02, -1.8472308293e-02,
         -1.1022432707e-02, -2.2754848003e-02, -1.5314219519e-02,
         -1.0982658714e-02, -1.4712141128e-03, -5.8929375373e-03,
         -1.0414796881e-02, -1.7388217151e-02, -2.3257987574e-02,
         -1.2215257622e-02, -1.9742713775e-03, 1.0952386074e-02,
         1.7606623471e-02,  8.9865028858e-03},
        {1, 2, 40}, result.interpreter);
    EXPECT_EQ(nn::utils::diff(result.tensors[0], expected), 0);
  }));
  EXPECT_TRUE(nNet_RemoveCGates(&gate));
  EXPECT_TRUE(nNet_RemoveInterpreter(interpreter) == NULL);
}
#endif

#ifndef TEST_ALLTESTS_CPP_
#include <libbase/all.hpp>
#include <signal.h>

static void handler(int sig, siginfo_t *dont_care, void* dont_care_either){
  base::trace::backtrace(30);
  exit(-1);
}

int main(int argc, char** argv){
  using namespace base;

 #if APPLE || USE_BASE_UNITTEST
  base::testing::init(argc, (const char**)argv);
 #else
  ::testing::InitGoogleTest(&argc, argv);
 #endif

  struct sigaction sa;

  memset(&sa, 0, sizeof(struct sigaction));
  sigemptyset(&sa.sa_mask);

  sa.sa_flags     = SA_NODEFER;
  sa.sa_sigaction = handler;

  sigaction(SIGSEGV, &sa, NULL); /* ignore whether it works or not */

  base::config::start(argv, argc);
  base::config::set("console", base::Auto{}.set(std::string{"console.txt"}));
  base::config::set("monitor", base::Auto{}.set(true));

  base::config::set("metal", base::Auto{}.set(std::string{"bundle"}));
  base::config::set("metal.file", base::Auto{}.set(std::string{"metal.metalib"}));

  base::config::set("opencl", base::Auto{}.set(std::string{"auto"}));
  base::config::set("opencl.file", base::Auto{}.set(std::string{"opencl.clbin"}));

  base::config::set("cuda", base::Auto{}.set(std::string{"file"}));
  base::config::set("cuda.file", base::Auto{}.set(std::string{"opencl.cubin"}));

  if (config::get("metal").type().get() != typeid(std::string))
    return -1;
  nNet_LoadEverything();
  return base::config::finish(RUN_ALL_TESTS(), [](){ nNet_UnloadEverything(); });
}
#endif  // TEST_ALLTESTS_CPP_
#endif  // BOOST_TEST_MAIN