unameOut="$(uname -s)"
case "${unameOut}" in
	Linux*)     machine=Linux;;
	Darwin*)    machine=Mac;;
	CYGWIN*)    machine=Cygwin;;
	MINGW*)     machine=MinGw;;
	*)          machine="UNKNOWN:${unameOut}"
esac

if [[ $# -ge 0 ]] || [[ "$1" != "build" ]]; then
    if [[ "$machine" == "Linux" ]]; then
        which ssh-agent || ( apt-get update -y && apt-get install sudo openssh-client git -y )
    fi

    if [[ "$LINUX_ENV" != "travis" ]]; then
        mkdir -p ~/.ssh
        echo "$SSH_PRIVATE_KEY" | tr -d '\r' > ~/.ssh/id_rsa
    else
        echo "$SSH_PRIVATE_KEY" > ~/.ssh/id_rsa_base64
        base64 --decode --ignore-garbage ~/.ssh/id_rsa_base64 > ~/.ssh/id_rsa
    fi

    chmod 700 ~/.ssh/id_rsa
    if [[ "$machine" == "Linux" ]]; then
        eval "$(ssh-agent -s)"
        ssh-add ~/.ssh/id_rsa
    fi
    { ssh-keyscan -H 'github.com'; ssh-keyscan -H 'gitlab.com'; ssh-keyscan -H 'bitbucket.org'; } >> ~/.ssh/known_hosts
    chmod 644 ~/.ssh/known_hosts
    git submodule update --init --recursive

    if [ "$machine" == "Linux" ]; then
        if [ ! "$LINUX_ENV" == "travis" ]; then
            apt-get update
            apt-get install -y curl git sudo 
        fi
    fi
    if [[ "$machine" == "Linux" ]]; then
        cat /proc/cpuinfo
    else
        sysctl -a | grep machdep.cpu
    fi
fi

if [[ $# -ge 1 ]] && [[ "$1" == "build" ]]; then
    if [ "$machine" == "Linux" ]; then
        if [ ! "$LINUX_ENV" == "travis" ]; then
            apt-get update
            apt-get install -qq -o=Dpkg::Use-Pty=0 -y curl git gdb sudo 
            apt-get install -qq -o=Dpkg::Use-Pty=0 -y cmake lcov swig valgrind protobuf-compiler ccache llvm wget
            apt-get install -qq -o=Dpkg::Use-Pty=0 -y libgtest-dev libprotobuf-dev libpcap-dev libssl-dev libboost-all-dev libopenblas-dev
            apt-get install -qq -o=Dpkg::Use-Pty=0 -y tree python-pip
        else
            sudo add-apt-repository -y ppa:maarten-fonville/protobuf
            sudo apt-get update
            sudo apt-get install -qq -o=Dpkg::Use-Pty=0 -y curl git gdb lcov valgrind protobuf-compiler  ccache llvm wget
            sudo apt-get install -qq -o=Dpkg::Use-Pty=0 -y libgtest-dev libprotobuf-dev libpcap-dev libssl-dev libboost-all-dev libopenblas-dev
            sudo apt-get install -qq -o=Dpkg::Use-Pty=0 -y tree python-pip
        fi

        sudo /usr/sbin/update-ccache-symlinks
        echo 'export PATH="/usr/lib/ccache:$PATH"' | tee -a ~/.bashrc
        source ~/.bashrc && echo "$PATH"

        if [ ! -f /usr/bin/swig ]; then
            sudo ln -s /usr/bin/swig2.0 /usr/bin/swig
        fi

        # install Android NDK
        wget -q https://dl.google.com/android/repository/android-ndk-r16b-linux-x86_64.zip
        unzip -qq android-ndk-r16b-linux-x86_64.zip
        export ANDROID_NDK_HOME=`pwd`/android-ndk-r16b
        export LOCAL_ANDROID_NDK_HOME="$ANDROID_NDK_HOME"
        export LOCAL_ANDROID_NDK_HOST_PLATFORM="linux-x86_64"
        export PATH=$PATH:${ANDROID_NDK_HOME}
        env
    elif [ "$machine" == "Mac" ]; then
        xcodebuild -version -sdk
        brew install valgrind
        brew install protobuf-c
        brew install lcov
        brew install gdb
        brew install tree
        brew install ccache
        brew install boost
        brew install brew-pip

        export CCACHE_CPP2=yes
        export CCACHE_SLOPPINESS=time_macros
        export PATH=`pwd`/third_party/llvm-build/Release+Asserts/bin:$PATH
    fi
    if [[ "$machine" == "Linux" ]]; then
        BRANCH="$(git rev-parse --abbrev-ref HEAD)"
        mkdir -p "$CI_PROJECT_DIR/dependencies" && (cd "$CI_PROJECT_DIR/dependencies" || exit -1)

        sudo bash "$CI_PROJECT_DIR/libbase/tools/gtest.sh" "$BRANCH" "$CI_PROJECT_DIR/dependencies"
        # bash "$CI_PROJECT_DIR/tools/dependencies/amd_sdk.sh"
        cd "$CI_PROJECT_DIR" || exit -1
    fi
    pip install requests
fi