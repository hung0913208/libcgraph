unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac

if [ ! -d $CI_PROJECT_DIR/tests/dataset ]; then
    mkdir -p $CI_PROJECT_DIR/tests/dataset
fi

if [ $machine == 'Linux' ]; then
    # bash $CI_PROJECT_DIR/libbase/tools/build.sh Coverage
    bash $CI_PROJECT_DIR/libbase/tools/build.sh Release
    bash $CI_PROJECT_DIR/libbase/tools/build.sh Debug -DCHECK_CASE_IM2COL_OF_NHWC=1 -DCHECK_CASE=1 -DCHECK_CASE_BENCHMARK_IM2COL=1
elif [ $machine == 'Mac' ]; then
    # bash ../tools/dependencies/protobuf.sh v3.6.0

    # CONFIGURE="-DPLATFORM_NAME=macosx$MACOS_VERSION" bash $CI_PROJECT_DIR/libbase/tools/build.sh Coverage -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
    CONFIGURE="-DPLATFORM_NAME=macosx$MACOS_VERSION" bash $CI_PROJECT_DIR/libbase/tools/build.sh Release -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
    CONFIGURE="-DPLATFORM_NAME=macosx$MACOS_VERSION" bash $CI_PROJECT_DIR/libbase/tools/build.sh Debug -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION -DCHECK_CASE_STREMING=1
fi
