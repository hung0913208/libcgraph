unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac

cd $CI_PROJECT_DIR/build

if [ $machine == 'Linux' ]; then
	pass=0

	if [[ "$CC" =~ "clang" ]] || [[ "$CXX" =~ "clang++" ]] || [[ "$machine" == "Mac" ]]; then
		export LD_LIBRARY_PATH=/usr/local/clang/lib:$LD_LIBRARY_PATH
	fi
	# python $CI_PROJECT_DIR/tools/fetch.py --source google --name speech_commands --output $CI_PROJECT_DIR/tests/dataset
	# bash $CI_PROJECT_DIR/libbase/tools/run.sh Optimize tests/alltests

	bash $CI_PROJECT_DIR/libbase/tools/run.sh Debug tests/alltests
	bash $CI_PROJECT_DIR/libbase/tools/run.sh Release
	if [ $? != 0 ]; then
		pass=-1
	fi
	# bash $CI_PROJECT_DIR/libbase/tools/valgrind.sh Debug tests/alltests
	# if [ $? != 0 ]; then
	# 	pass=-1
	# fi

	bash $CI_PROJECT_DIR/libbase/tools/coverage.sh Libcgraph_coverage
	if [ $? != 0 ]; then
		exit -1
	fi
fi

if [ $machine == 'Mac' ]; then
	bash $CI_PROJECT_DIR/libbase/tools/run.sh Debug tests/alltests
	bash $CI_PROJECT_DIR/libbase/tools/run.sh Release
	if [ $? != 0 ]; then
		exit -1
	fi
	bash $CI_PROJECT_DIR/libbase/tools/coverage.sh Libcgraph_coverage
	if [ $? != 0 ]; then
		exit -1
	fi
fi
