unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac
BRANCH=$(git rev-parse --abbrev-ref HEAD)

if [ "$BRANCH" != "release" ]; then
    exit 0
elif [ "$machine" == 'Linux' ]; then
    mkdir -p linux 
    cd linux/ || exit -1

    bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Coverage
    bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Release
    bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Debug
    cd ../ || exit -1

    mkdir -p android
    cd android/ || exit -1
    export CONFIGURE="-DCMAKE_TOOLCHAIN_FILE=$CI_PROJECT_DIR/CMakePlatforms/Android.cmake -DLLVM_ANDROID_TOOLCHAIN_DIR=$CI_PROJECT_DIR/android-ndk-r16b"

    bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Coverage
    bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Release
    bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Debug
    cd ../ || exit -1
elif [ "$machine" == 'Mac' ]; then
    bash "$CI_PROJECT_DIR/tools/dependencies/protobuf.sh"

    if [ -x "${MACOS_VERSION}" ]; then
        mkdir -p macosx
        cd macosx/ || exit -1
        export CONFIGURE="-DPLATFORM_NAME=macosx$MACOS_VERSION"

        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Coverage -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Release -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Debug -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        cd ../ || exit -1
    fi

    if [ -x "${IPHONEOS_VERSION}" ]; then
        mkdir -p iphoneos
        cd iphoneos || exit -1
        export CONFIGURE="-DCMAKE_TOOLCHAIN_FILE=$CI_PROJECT_DIR/CMakePlatforms/iOS.cmake -DPLATFORM_NAME=iphoneos$IPHONEOS_VERSION"

        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Coverage -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Release -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Debug -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        cd ../ || exit -1

        mkdir -p iphonesimulator
        cd iphonesimulator || exit -1
        export CONFIGURE="-DCMAKE_TOOLCHAIN_FILE=$CI_PROJECT_DIR/CMakePlatforms/iOS.cmake -DPLATFORM_NAME=iphonesimulator$IPHONEOS_VERSION"

        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Coverage -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Release -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Debug -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        cd ../ || exit -1
    fi

    if [ -x "${APPLE_TVOS_VERSION}" ]; then
        mkdir -p appletvos
        cd appletvos || exit -1
        export CONFIGURE="-DCMAKE_TOOLCHAIN_FILE=$CI_PROJECT_DIR/CMakePlatforms/iOS.cmake -DPLATFORM_NAME=appletvos$APPLE_TVOS_VERSION" 

        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Coverage -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Release -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Debug -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        cd ../ || exit -1

        mkdir -p appletvsimulator
        cd appletvsimulator || exit -1
        export CONFIGURE="-DCMAKE_TOOLCHAIN_FILE=$CI_PROJECT_DIR/CMakePlatforms/iOS.cmake -DPLATFORM_NAME=appletvsimulator$APPLE_TVOS_VERSION"

        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Coverage -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Release -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Debug -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        cd ../ || exit -1
    fi

    if [ -x "${WATCHOS_VERSION}" ]; then
        mkdir -p watchos
        cd watchos || exit -1
        export CONFIGURE="-DCMAKE_TOOLCHAIN_FILE=$CI_PROJECT_DIR/CMakePlatforms/iOS.cmake -DPLATFORM_NAME=watchos$WATCHOS_VERSION"

        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Coverage -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Release -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Debug -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        cd ../ || exit -1

        mkdir -p watchsimulator
        cd watchsimulator || exit -1
        export CONFIGURE="-DCMAKE_TOOLCHAIN_FILE=$CI_PROJECT_DIR/CMakePlatforms/iOS.cmake -DPLATFORM_NAME=watchsimulator$WATCHOS_VERSION"

        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Coverage -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Release -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        bash "$CI_PROJECT_DIR/libbase/tools/build.sh" Debug -DOSX_PRODUCT_VERSION=$OSX_PRODUCT_VERSION
        cd ../ || exit -1
    fi
fi
