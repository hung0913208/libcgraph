import shutils
import zipfile
import glob 
import os

import tensorflow as tf
import tensorflow.keras as keras

from tensorflow.contrib.framework.python.ops import audio_ops as contrib_audio

def extract_compressed_dataset(output, file, tool):
  if file is not None:
    for folder in file.split('/')[:-1]:
      os.mkdir(output + '/' + folder)
      output = output + '/' + folder

    if tool == 'tar':
      tar_fd = tarfile.open(file, 'r:' + file.split('.')[-1])
      tar_fd.extractall(path=output)
      tar_fd.close()
    elif tool == 'zip':
      zip_fd = zipfile.ZipFile(file, 'r')
      zip_fd.extractall(output)
      zip_fd.close()
    else:
      dataset = output + '/' + file.split('/')[-1]
      shutil.copy(file, dataset)
      print("cannot extract dataset %s, please do it manualy" % dataset)

def ctc_loss(args):
  y_pred, labels, input_length, label_length = args
  return keras.backend.ctc_batch_cost(labels, y_pred, input_length, label_length)

def ctc(y_true, y_pred):
  return y_pred

def ds1(input_data, is_training = True, output_dim=29, fc_count=3, fc_size=2048, rnn_size=512, dropouts=[0.1, 0.1, 0.1]):
  # build a simple model of deepspeech 1
  forward_model = None

  if isinstance(input_data, int):
    input_data = keras.Input(name='input', shape=(None, input_data))
    x = input_data
  else:  
    x = keras.Input(name='input', tensor=input_data)

  for i in range(fc_count):
    x = keras.layers.TimeDistributed(keras.layers.Dense(fc_size, name=('fc%d' % i), activation='relu'))(x)
    if is_training:
      x = keras.layers.Dropout(dropouts[0])(x)
  else:
    if is_training:
      x = keras.layers.Bidirectional(keras.layers.LSTM(rnn_size, return_sequences=True, activation='relu', name='birnn', dropout=dropouts[1]), merge_mode='sum')(x)
    else:
      x = keras.layers.Bidirectional(keras.layers.LSTM(rnn_size, return_sequences=True, activation='relu', name='birnn'), merge_mode='sum')(x)
    if is_training:
      x = keras.layers.Dropout(dropouts[0])(x)  
    forward_model = keras.layers.TimeDistributed(keras.layers.Dense(output_dim, name='y_pred', activation='softmax'), name='output')(x)

  if is_training:
    if not forward_model is None:
      # build loss function, we will use ctc loss function of tensorflow
      labels = keras.Input(name='labels', shape=[None,], dtype='int32')
      input_length = keras.Input(name='input_length', shape=[1], dtype='int32')
      label_length = keras.Input(name='label_length', shape=[1], dtype='int32')

      loss_funct = keras.layers.Lambda(ctc_loss, output_shape=(1,), name='ctc')([forward_model, labels, input_length, label_length])
      return keras.Model(inputs=[input_data, labels, input_length, label_length], outputs=loss_funct)
    else:
      return None
  else:
    return keras.Model(inputs=[input_data], outputs=forward_model)

def ds2():
  pass

def feature(sample_rate, window_size_ms, window_stride_ms, fingerprint_width, is_training=True):
  window_size_samples = int(sample_rate * window_size_ms / 1000)
  window_stride_samples = int(sample_rate * window_stride_ms / 1000)
  
  if is_training is True:
    placeholder = tf.placeholder(tf.string, [], name='wav_data')
    decoded_sample_data = contrib_audio.decode_wav(placeholder,
          desired_channels=1,
          name='decoded_sample_data')
    spectrogram = contrib_audio.audio_spectrogram(decoded_smple_data.audio, 
          name='spectrogram',
          window_size=window_size_samples,
          stride=window_stride_samples,ull
          magnitude_squared=True)
    sample_rate = decoded_sample_data.sample_rate
  else:
    placeholder = tf.placeholder(tf.float32, name='signal')
    spectrogram = contrib_audio.audio_spectrogram(signal, name='spectrogram',
          window_size=window_size_samples,
          stride=window_stride_samples,
          magnitude_squared=True)

  return contrib_audio.mfcc(spectrogram, sample_rate, name='mfcc',
          dct_coefficient_count=fingerprint_width), placeholder

def build_dataset(input_files, sample_rate, window_size_ms, window_stride_ms, fingerprint_width):
  mfcc, wav_data = feature(sample_rate, window_size_ms, window_stride_ms, fingerprint_width)

  def generator():
    for file in glob.glob(input_files)
      with open(file, 'rb') as fd:
        yield sess.run(mfcc, feed_dict={wav_data: fd.read()})

  return tf.data.Dataset().batch(1).from_generator(generator)

# build optimizer
DATASET = 'trinh_sentence.zip'
TRANSCRIPT = 'trinh_transcript.txt'

SAMPLE_RATE = 16000
WINDOW_SIZE_MS = 30
WINDOW_STRIDE_MS = 10
DCT_COEFFICIENT = 40

LEARNING_RATE = 0.001
STEP_PER_EPOCH = 50
EPOCH = 10

# fetch data tu server ve va dua vao dataset
if os.path.isfile('dataset'):
  os.remove('dataset')

if not os.path.exists('dataset'):
  os.mkdir('dataset')

extract_compressed_dataset('dataset', DATASET, 'zip')

dataset   = build_dataset(TRANSCRIPT, SAMPLE_RATE, WINDOW_SIZE_MS, WINDOW_STRIDE_MS, DCT_COEFFICIENT)
model     = ds1(DCT_COEFFICIENT, is_training=True)
optimizer = keras.optimizers.Adam(LEARNING_RATE)

model.compile(optimizer=optimizer, loss=ctc)
print(model.summary())

model.fit(dataset.make_one_shot_iterator(), step_per_epoch=STEP_PER_EPOCH, epoch=EPOCH, validation_split=0.1)
