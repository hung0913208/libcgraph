#ifndef BOOST_TEST_MAIN
#define TEST_ALLTESTS_CPP_
#include <libbase/all.hpp>
#include <signal.h>

#if APPLE || USE_BASE_UNITTEST
#include "libbase/unittest.hpp"
#else
#include <gtest/gtest.h>
#endif

#include "nnet/fail/streaming.cpp"

#include "nnet/pass/invoke.cpp"
#include "nnet/pass/load.cpp"
#include "nnet/pass/math.cpp"
#include "nnet/pass/models.cpp"
#include "nnet/pass/optimize.cpp"
#include "nnet/pass/streaming.cpp"
#include "nnet/pass/tbcalc.cpp"
#include "nnet/pass/tensor.cpp"
#include "nnet/pass/wavio.cpp"

#include "nnet/benchmark.cpp"
#include "fmath/benchmark.cpp"

static void handler(int sig, siginfo_t *dont_care, void* dont_care_either){
  base::trace::backtrace(30);
  exit(-1);
}

int main(int argc, char** argv){
  using namespace base;

 #if APPLE || USE_BASE_UNITTEST
  base::testing::init(argc, (const char**)argv);
 #else
  ::testing::InitGoogleTest(&argc, argv);
 #endif

 #if NDEBUG
  struct sigaction sa;

  memset(&sa, 0, sizeof(struct sigaction));
  sigemptyset(&sa.sa_mask);

  sa.sa_flags     = SA_NODEFER;
  sa.sa_sigaction = handler;

  sigaction(SIGSEGV, &sa, NULL); /* ignore whether it works or not */
 #endif

  base::config::start(argv, argc);
  base::config::set("console", base::Auto{}.set(std::string{"console.txt"}));
  base::config::set("monitor", base::Auto{}.set(true));

  base::config::set("metal", base::Auto{}.set(std::string{"bundle"}));
  base::config::set("metal.file", base::Auto{}.set(std::string{"metal.metalib"}));

  base::config::set("opencl", base::Auto{}.set(std::string{"auto"}));
  base::config::set("opencl.file", base::Auto{}.set(std::string{"opencl.clbin"}));

  base::config::set("cuda", base::Auto{}.set(std::string{"file"}));
  base::config::set("cuda.file", base::Auto{}.set(std::string{"opencl.cubin"}));

  if (config::get("metal").type().get() != typeid(std::string))
    return -1;
  nNet_LoadEverything();
  return base::config::finish(RUN_ALL_TESTS(), [](){ nNet_UnloadEverything(); });
}
#endif  // BOOST_TEST_MAIN