#ifndef BOOST_TEST_MAIN
#include "libfmath/fmath.h"
#include "libnn/interpreter.h"

#include "libbase/all.hpp"

#if APPLE || USE_BASE_UNITTEST
#include "libbase/unittest.hpp"
#else
#include <gtest/gtest.h>
#endif
const auto thread_count = 2*std::thread::hardware_concurrency();
const auto max_loops    = 100000;

#if CHECK_CASE_BENCHMARK_GENERIC || !NDEBUG
TEST(Benchmark, Generic_WithoutThread){
  float UNUSED(result) = 1.f;

  for (auto i = max_loops; i > 0; --i){
    result = 2*2;
  }
}

TEST(Benchmark, Generic_WithThread){
  base::Thread *threads[thread_count];

  for (auto i = 0; i < cast_(i, thread_count); ++i){
    threads[i] = new base::Thread(
      [&](base::Thread& UNUSED(thread), base::Tuple& UNUSED(args)) -> base::Tuple{
        float UNUSED(result) = 1.f;

        for (auto i = max_loops/thread_count; i > 0; --i){
          result = 2*2;
        }

        return base::Tuple::make();
      });
    threads[i]->start();
  }

  for (auto i = 0; i < cast_(i, thread_count); ++i){
    delete threads[i];
  }
}
#endif

#if CHECK_CASE_BENCHMARK_SSE2 && !NDEBUG && SSE2
TEST(Benchmark, SSE2_WithoutThread){
  ALIGNED(16) float inputs[4];
  ALIGNED(16) float results[4];

  inputs[0] = 0;
  inputs[1] = 1;
  inputs[2] = 2;
  inputs[3] = 3;

  auto p_src = (__m128*) inputs;
  auto p_dst = (__m128*) results;

  for (auto i = max_loops; i > 0; i -= 4){

    *p_dst = _mm_mul_ps(*p_src, *p_src);
  }
}

TEST(Benchmark, SSE2_WithThread){
  base::Thread* threads[thread_count];

  for (auto i = 0; i < cast_(i, thread_count); ++i){
    threads[i] = new base::Thread(
      [](base::Thread& UNUSED(thread), base::Tuple& UNUSED(args)) -> base::Tuple{
        ALIGNED(16) float inputs[4];
        ALIGNED(16) float results[4];

        inputs[0] = 0;
        inputs[1] = 1;
        inputs[2] = 2;
        inputs[3] = 3;

        auto p_src = (__m128*) inputs;
        auto p_dst = (__m128*) results;

        for (auto i = max_loops/thread_count; i > 0; i -= 4){
          *p_dst = _mm_mul_ps(*p_src, *p_src);
        }

        return base::Tuple::make();
      });
    threads[i]->start();
  }

  for (auto i = 0; i < cast_(i, thread_count); ++i){
    delete threads[i];
  }
}
#endif

#if CHECK_CASE_BENCHMARK_NEON32 && !NDEBUG && NEON32
TEST(Benchmark, NEON32_WithThread){
  base::Thread* threads[thread_count];

  for (auto i = 0; i < cast_(i, thread_count); ++i){
    threads[i] = new base::Thread(
      [](base::Thread& thread, base::Tuple& args) -> base::Tuple{
        for (auto i = max_loops/thread_count; i > 0; --i){
          
        }
      });
    threads[i]->start();
  }

  for (auto i = 0; i < cast_(i, thread_count); ++i){
    delete threads[i];
  }
}
#endif

#if CHECK_CASE_BENCHMARK_NAIVE
#include <string>
#include <vector>
#include <list>
#include <cmath>
#include <algorithm>
#include <map>
#include <set>
#include <iostream>
#include <cstdio>
#include <cstdlib>
#include <sys/time.h>
#include <xmmintrin.h>
#include <emmintrin.h>
using namespace std;

#define VIT(i, v) for (i = 0; i < v.size(); i++) 
#define IT(it, ds) for (it = ds.begin(); it != ds.end(); it++)
#define FUP(i, n) for (i = 0; i < n; i++)

typedef vector <int> IVec;
typedef vector <double> DVec;
typedef vector <string> SVec;
 
class MM {
  public:
    vector <double> M1;
    vector <double> M2;
    vector <double> P;
    int r1, c1, c2;
    int Print;
    int Blocksize;
    void MultiplyBlock(int row1, int col1, int col2);
    void Multiply();
    void PrintAll();
};

void MM::MultiplyBlock(int row1, int col1, int col2)
{
  double *m1top, *m1;
  double *m2top, *m2base, *m2;
  double *p, *ptmp, *ptmp2, *ptmp3, *ptmp4;
  double prod[2];
  int tmp, tmptop;
  __m128d *p1, *p2, *p3, *p4, *p5, v1, v2, v3, v4, v5, v6, v7, v8;

  m1 = &(M1[row1 * c1 + col1]);
  m1top = m1 + (c1 * Blocksize);
  if (m1top > &(M1[r1 * c1])) m1top = &(M1[r1 * c1]);

  m2base = &(M2[col2 * c1 + col1]);
  m2top = m2base + (c1 * Blocksize);
  if (m2top > &(M2[c2 * c1])) m2top = &(M2[c2 * c1]);

  p = &(P[row1 * c2 + col2]);

  tmptop = col1 + Blocksize;
  if (tmptop > c1) tmptop = c1;

  for ( ; m1 < m1top; m1 += (c1*4)) {
    ptmp = p;
    ptmp2 = p+c2;
    ptmp3 = ptmp2+c2;
    ptmp4 = ptmp3+c2;
    for (m2 = m2base; m2 < m2top; m2 += c1) {
      p1 = (__m128d *) m1;
      p3 = (__m128d *) (m1+c1);
      p4 = (__m128d *) (m1+(c1*2));
      p5 = (__m128d *) (m1+(c1*3));
      p2 = (__m128d *) m2;
      v1 = _mm_sub_pd(v1, v1);   /* Zero v1 */
      v3 = _mm_sub_pd(v3, v3);   /* Zero v3 */
      v5 = _mm_sub_pd(v5, v5);   /* Zero v5 */
      v7 = _mm_sub_pd(v7, v7);   /* Zero v7 */
      
      for (tmp = col1; tmp < tmptop; tmp += 2) {
        v2 = _mm_mul_pd(*p1, *p2);
        v4 = _mm_mul_pd(*p3, *p2);
        v6 = _mm_mul_pd(*p4, *p2);
        v8 = _mm_mul_pd(*p5, *p2);
        v1 = _mm_add_pd(v1, v2);
        v3 = _mm_add_pd(v3, v4);
        v5 = _mm_add_pd(v5, v6);
        v7 = _mm_add_pd(v7, v8);
        p1++;
        p2++;
        p3++;
        p4++;
        p5++;
      }
      _mm_store_pd(prod, v1);
      *ptmp += prod[0];
      *ptmp += prod[1];
      _mm_store_pd(prod, v3);
      *ptmp2 += prod[0];
      *ptmp2 += prod[1];
      _mm_store_pd(prod, v5);
      *ptmp3 += prod[0];
      *ptmp3 += prod[1];
      _mm_store_pd(prod, v7);
      *ptmp4 += prod[0];
      *ptmp4 += prod[1];
      ptmp++;
      ptmp2++;
      ptmp3++;
      ptmp4++;
    }
    p += (c2*4);
  }
}

void MM::Multiply()
{
  int row1, col1, col2;

  for (row1 = 0; row1 < r1; row1 += Blocksize) {
    for (col2 = 0; col2 < c2; col2 += Blocksize) {
      for (col1 = 0; col1 < c1; col1 += Blocksize) {
        MultiplyBlock(row1, col1, col2);
      }
    }
  }
}

void MM::PrintAll()
{
  int i, j;

  printf("M1: %d x %d\n\n", r1, c1);
  for (i = 0; i < r1; i++) {
    for (j = 0; j < c1; j++) printf(" %6.4lf", M1[i*c1+j]);
    printf("\n");
  }
    printf("\n");

  printf("M2: %d x %d\n\n", c1, c2);
  for (i = 0; i < c1; i++) {
    for (j = 0; j < c2; j++) printf(" %6.4lf", M2[j*c1+i]);
    printf("\n");
  }
    printf("\n");

  printf("P: %d x %d\n\n", r1, c2);
  for (i = 0; i < r1; i++) {
    for (j = 0; j < c2; j++) printf(" %6.4lf", P[i*c2+j]);
    printf("\n");
  }
}

TEST(Benchmark, Naive){
  MM *M;
  int r1, c1, c2, i, j, bs;
  string s;
  long seed;
  double t0, t1;
  struct timeval tv;
  
   M = new MM;

  r1 = 1000;
  c1 = 1000;
  c2 = 1000;
  bs = 380;

  if (r1 % 2 == 1) { fprintf(stderr, "This only works on even matrix dimensions.\n"); exit(0); }
  if (c1 % 2 == 1) { fprintf(stderr, "This only works on even matrix dimensions.\n"); exit(0); }
  if (c2 % 2 == 1) { fprintf(stderr, "This only works on even matrix dimensions.\n"); exit(0); }
  if (bs % 2 == 1) { fprintf(stderr, "This only works on even block sizes.\n"); exit(0); }

  M->r1 = r1;
  M->c1 = c1;
  M->c2 = c2;
  M->Blocksize = bs;

  seed = 0;
  srand48(seed);

  M->M1.resize(r1*c1);
  M->M2.resize(c1*c2);
  M->P.resize(r1*c2, 0);

  for(i = 0; i < r1*c1; i++) M->M1[i] = drand48()*2.0;
  for(i = 0; i < c1; i++) {
    for(j = 0; j < c2; j++) M->M2[j*c1+i] = drand48()*2.0;
  }
  
 BEGIN_PTRACKING()
  M->Multiply();
 END_PTRACKING;
}
#endif

#if (CHECK_CASE_BENCHMARK_OPENBLAS || !CHECK_CASE) && USE_ACCELERATE==2
#include <cblas.h>

TEST(Benchmark, OpenBLAS){
  auto left   = (float*)nNet_AllocateArray(1000*1000, Real32_t);
  auto right  = (float*)nNet_AllocateArray(1000*1000, Real32_t);
  auto output = (float*)nNet_AllocateArray(1000*1000, Real32_t);

  cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, 1000, 1000, 1000, 1.0f, 
              left, 1000, right, 1000, 0.0f, output, 1000);
  free(left);
  free(right);
  free(output);
}
#endif

#if (!CHECK_CASE || CHECK_CASE_BENCHMARK_ACCELERATE) && USE_ACCELERATE==1
#include <Accelerate/Accelerate.h>

TEST(Benchmark, Accelerate){
  auto left   = (float*)nNet_AllocateArray(1000*1000, Real32_t);
  auto right  = (float*)nNet_AllocateArray(1000*1000, Real32_t);
  auto output = (float*)nNet_AllocateArray(1000*1000, Real32_t);

 BEGIN_PTRACKING()
  vDSP_mmul(left, 1, right, 1, output, 1, 1000, 1000, 1000);
 END_PTRACKING;
  free(left);
  free(right);
  free(output);
}
#endif

#if (!CHECK_CASE || CHECK_CASE_BENCHMARK_INTEL_MKL) && USE_ACCELERATE==3
#include <Accelerate/Accelerate.h>

TEST(Benchmark, Accelerate){
  auto left   = (float*)nNet_AllocateArray(1000*1000, Real32_t);
  auto right  = (float*)nNet_AllocateArray(1000*1000, Real32_t);
  auto output = (float*)nNet_AllocateArray(1000*1000, Real32_t);

 BEGIN_PTRACKING()
  vDSP_mmul(left, 1, right, 1, output, 1, 1000, 1000, 1000);
 END_PTRACKING;
  free(left);
  free(right);
  free(output);
}
#endif

#ifndef TEST_ALLTESTS_CPP_
#include <libbase/all.hpp>
#include <signal.h>

static void handler(int sig, siginfo_t *dont_care, void* dont_care_either){
  base::trace::backtrace(30);
  exit(-1);
}

int main(int argc, char** argv){
  using namespace base;

 #if APPLE || USE_BASE_UNITTEST
  base::testing::init(argc, (const char**)argv);
 #else
  ::testing::InitGoogleTest(&argc, argv);
 #endif

  struct sigaction sa;

  memset(&sa, 0, sizeof(struct sigaction));
  sigemptyset(&sa.sa_mask);

  sa.sa_flags     = SA_NODEFER;
  sa.sa_sigaction = handler;

  sigaction(SIGSEGV, &sa, NULL); /* ignore whether it works or not */

  base::config::start(argv, argc);
  return base::config::finish(RUN_ALL_TESTS());
}
#endif // TEST_ALLTESTS_CPP_
#endif // BOOST_TEST_MAIN
