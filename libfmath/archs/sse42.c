#include "libnn/tensor.h"
#include "sse42.h"

#define SSE2_REAL32 16
#define SSE2_REAL64 16
#define SSE2_INT32  16
#define SSE2_INT64  16

#ifdef __SSE2__
int fMat_SizeType(int type){
  switch(type){
  case Real32_t:
    return SSE2_REAL32;

  case Real64_t:
    return SSE2_REAL64;

  case Int32_t:
    return SSE2_INT32;

  case Int64_t:
    return SSE2_INT64;

  default:
    return 1;
  }
}
#endif