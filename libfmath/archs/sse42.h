#ifndef LIBFMAT_SSE2_H_
#define LIBFMAT_SSE2_H_

#ifdef __SSE2__
#include <xmmintrin.h>
#define SSE2 1

#ifndef ALLOW_TO_USE_FMATH
#define ALLOW_TO_USE_FMATH 1
#endif // ALLOW_TO_USE_FMATH
#endif
#endif  // LIBFMAT_SSE2_H_