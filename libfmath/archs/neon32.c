#include "libnn/tensor.h"
#include "neon32.h"

#define NEON32_REAL32 16
#define NEON32_REAL64 16
#define NEON32_INT32  16
#define NEON32_INT64  16

#if defined(__ARM_NEON) && !defined(__aarch64__)
int fMat_SizeType(int type){
  switch(type){
  case Real32_t:
    return NEON32_REAL32;

  case Real64_t:
    return NEON32_REAL64;

  case Int32_t:
    return NEON32_INT32;

  case Int64_t:
    return NEON32_INT64;

  default:
    return 1;
  }
}
#endif