#include "sse42.h"
#include "fmath.h"

namespace fmath{
namespace sse42{
namespace mat4x4{
struct Matrix32: fmath::Matrix<float>{
 public:
  explicit Matrix(NumT* array, int xsize, int ysize);

 public:
  int   xsize();
  int   ysize();
  NumT* matrix();

 public:
  Matrix<NumT>& dot(Vector<NumT>& src);
  Matrix<NumT>& dot(Matrix<NumT>& src);

 public:
  Matrix<NumT>& operator *=(NumT src);
  Matrix<NumT>& operator *=(Vector<NumT>& src);
  Matrix<NumT>& operator *=(Matrix<NumT>& src);
  Matrix<NumT>& operator +=(NumT src);
  Matrix<NumT>& operator +=(Vector<NumT>& src);
  Matrix<NumT>& operator +=(Matrix<NumT>& src);
};

struct Matrix64: fmath::Matrix<double>{
};
} // namespace mat4x4
} // namespace sse42
} // namespace fmath