#include "sse42.h"

namespace fmath{
namespace sse42{
namespace vec4{
struct Vector64: fmath::Vector<double>{
  union{
    struct {
      __m256 _L1, _L2, _L3, _L4;
    };
    struct {
      double _11, _12, _13, _14;
    };
  } vector;

 public:
  explicit Vector(NumT* array, int size): fmath::Vector<NumT>(array, size)

 public:
  int   size();
  NumT* vector();

 public:
  NumT dot(Vector<NumT>& src);
  NumT transpose(Vector<NumT>& src);

 public:
  Vector<NumT>& operator *=(NumT src);
  Vector<NumT>& operator *=(Vector<NumT>& src);
  Vector<NumT>& operator +=(NumT src);
  Vector<NumT>& operator +=(Vector<NumT>& src);
};

struct Vector32: fmath::Vector<float>{
  union{
    struct {
      __m128 _L1, _L2, _L3, _L4;
    };
    struct {
      float _11, _12, _13, _14;
    };
  } vector;

  ~Vector(){ }

  explicit Vector(NumT* array, int size): fmath::Vector<NumT>(array, size){
  }

  int size(){
  }

  NumT* vector(){
  }

  NumT dot(Vector<NumT>& src){
  }

  Vector<NumT>& operator *=(NumT src){
    return *this;
  }

  Vector<NumT>& operator *=(Vector<NumT>& src){
    return *this;
  }

  Vector<NumT>& operator +=(NumT src){
    return *this;
  }

  Vector<NumT>& operator +=(Vector<NumT>& src){
    return *this;
  }
};
} // namespace vec4
} // namespace sse42
} // namespace fmath