#ifndef LIBFMAT_NEON32_H_
#define LIBFMAT_NEON32_H_

#if defined(__ARM_NEON) && !defined(__aarch64__)
#include <arm_neon.h>

#define NEON32 1
#ifndef ALLOW_TO_USE_FMATH
#define ALLOW_TO_USE_FMATH 1
#endif // ALLOW_TO_USE_FMATH
#endif
#endif  // LIBFMAT_NEON32_H_
