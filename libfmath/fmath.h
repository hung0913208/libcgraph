#ifndef LIBFMAT_FMAT_H_
#define LIBFMAT_FMAT_H_

#if defined(_MSC_VER)
#define ALIGNED(x) __declspec(align(x))
#else
#if defined(__GNUC__)
#define ALIGNED(x) __attribute__ ((aligned(x)))
#endif
#endif
#include <libbase/all.hpp>

#include "archs/sse42.h"
#include "archs/neon32.h"

#if ALLOW_TO_USE_FMATH
/* @NOTE: By default, we only support 3 level: vector with vector, matrix with 
 * vector and matrix with matrix. 
 *   Because fmath is a fast matrix mathematical libary, i would assume we will 
 * perform only with small matrix and vector.
 *   However, i will implement some C-APIs to support with large scale matrix 
 * and vector.
 */

#if __cplusplus
template<typename NumT>
struct Vector{
 public:
  virtual ~Vector(){ }
  explicit Vector(NumT* UNUSED(array), int UNUSED(size)){ }

 public:
  virtual int   size() = 0;
  virtual NumT* vector() = 0;

  virtual NumT  dot(Vector<NumT>& src) = 0;

  virtual Vector<NumT>& operator *=(NumT src) = 0;
  virtual Vector<NumT>& operator *=(Vector<NumT>& src) = 0;
  virtual Vector<NumT>& operator +=(NumT src) = 0;
  virtual Vector<NumT>& operator +=(Vector<NumT>& src) = 0;
};

template<typename NumT>
struct Matrix{
 public:
  virtual ~Matrix(){ }
  explicit Matrix(NumT* UNUSED(array), int UNUSED(xsize), int UNUSED(ysize)){}

 public:
  virtual int   xsize() = 0;
  virtual int   ysize() = 0;
  virtual NumT* matrix() = 0;

  virtual Matrix<NumT>& dot(Vector<NumT>& src) = 0;
  virtual Matrix<NumT>& dot(Matrix<NumT>& src) = 0;

  virtual Matrix<NumT>& operator *=(NumT src) = 0;
  virtual Matrix<NumT>& operator *=(Vector<NumT>& src) = 0;
  virtual Matrix<NumT>& operator *=(Matrix<NumT>& src) = 0;
  virtual Matrix<NumT>& operator +=(NumT src) = 0;
  virtual Matrix<NumT>& operator +=(Vector<NumT>& src) = 0;
  virtual Matrix<NumT>& operator +=(Matrix<NumT>& src) = 0;
};
#endif

#if __cplusplus
extern "C"{
#endif
int fMat_SizeType(int type);
#if __cplusplus
}
#endif
#else
#ifndef USE_FMATH
#undef USE_FMATH
#endif
#endif // ALLOW_TO_USE_FMATH
#endif  // LIBFMAT_FMATH_H_