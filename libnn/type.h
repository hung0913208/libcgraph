#ifndef LIBNN_TYPE_H_
#define LIBNN_TYPE_H_

#if __cplusplus
#include <cstdint>
#else
#include <stdint.h>
#endif

#if !__cplusplus
#define bool int
#define true 1
#define false 0
#endif

#if __cplusplus
extern "C"{
#endif
enum CType{
  NumT_t = 8,
  TensorT_t = 9
};

#if __APPLE__
#include <MacTypes.h>
/* @NOTE: use default num-type of APPLE */
#else
typedef uint8_t  UInt8;
typedef uint16_t UInt16;
typedef uint32_t UInt32;
#endif
typedef uint64_t UInt64;

typedef int8_t  Int8;
typedef int16_t Int16;
typedef int32_t Int32;
typedef int64_t Int64;

typedef float Real32;
typedef double Real64;

typedef const char* CString;

int     nNet_SizeType(int type);
bool    nNet_IsArrayPointer(int type);
long    nNet_MaxPhysicalMemory();
CString nNet_NameType(int type);


#if __cplusplus
}
#endif
#endif // LIBNN_TYPE_H_
