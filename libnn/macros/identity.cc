#include "interpreter.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
namespace kernels{
namespace macro{
namespace identity{
using namespace nn::interpreter;

bool Link(Operator op, Operator* ops, int ops_size){
  using namespace utils::wstring;
  using namespace utils;

  auto opw     = nNet_GetOperatorW(op);
  auto outputs = (Tensor*){nullptr};
  auto name    = std::string{};
  auto index   = 0;
  
  if (opw->outputs().size() > 0) /* @NOTE: prevent run multi-time */
    return false;
  else if (opw->inputs().size() != 1) /* @NOTE: make sure the input existed */
    return opw << (BadLogic << "op \'Identity\' require 1 input, but you have" 
                            << std::to_string(opw->inputs().size()));

  wstring::tie(name, index) = split(std::get<0>(opw->inputs()[0]), ':');
  for (auto i = 0; i < ops_size; ++i){
    auto output  = Tensor{nullptr};
    auto ts_size = 0;

    /* @NOTE: find the previous op and get its output to a temporary variable 
     * names 'value' and wait to the 'commit' step */

    if (name != ops[i]->name)
      continue;
    else if (ops[i]->outputs){
      if (ops[i]->outputs(ops[i], &outputs, &ts_size))
        return true;
      output = outputs[index];
    } else /* @NOTE: only happen with some macro */
      output = nNet_GetOperatorW(ops[i])->get<Tensor>("value");

    if (!output) return opw << (BadAccess);
    opw->outputs().push_back(output);
    return false;
  }

  return opw << (NotFound << "gate \'" << std::string{std::get<0>(opw->inputs()[0])}
                          << "\' didn\'t exist");
}

Operator Prepare(Operator op, Interpreter interpreter, CString model){
  try{
    auto attr = OperatorW::config(model, "Identity");

    if (attr)
      return (new interpreter::OperatorW(op, 2, interpreter, nullptr, attr))
                ->interface();
    else {
      nNet_GetInterpreterW(interpreter)->error(NotFound);
      return nullptr;
    }
  } catch(base::Error& error){
    nNet_GetInterpreterW(interpreter)->error(rvalue(error));
    return nullptr;
  }
}

bool Eval(Operator op, Interpreter UNUSED(interpreter)){
  op->outputs = [](Operator op, Tensor** outputs, int* size) -> bool{
    auto opw   = nNet_GetOperatorW(op);

    if (!outputs) 
      return opw << (BadAccess.reason("\'tensor\' must not be null"));
    else if (opw->outputs().size() != 1){
      return opw << (BadLogic << "op \'Identity\' require 1 output, but you have" 
                              << std::to_string(opw->outputs().size()));
    } else {
      *outputs = opw->outputs().data();
      *size    = 1;

      return nNet_GetInterpreterW(opw->interpreter())->error();
    }
  };
  return false;
}
} // namespace identity

Registration doRegisterIdentity(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = identity::Prepare;
  result.link   = identity::Link;
  result.eval   = identity::Eval;
  return result;
}
} // namespace macro
} // namespace kernels
} // namespace nn
