#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
namespace kernels{
namespace macro{
namespace constance{
using namespace nn::interpreter;

Operator Prepare(Operator op, Interpreter interpreter, CString model){
  try{
    auto attr = OperatorW::config(model, "Const");

    if (attr)
      return (new interpreter::OperatorW(op, 2, interpreter, attr))->interface();
    else {
      nNet_GetInterpreterW(interpreter)->error(NotFound);
      return nullptr;
    }
  } catch(base::Error& error){
    nNet_GetInterpreterW(interpreter)->error(rvalue(error));
    return nullptr;
  }
}

bool Eval(Operator op, Interpreter interpreter){
  auto interpreterw = nNet_GetInterpreterW(interpreter);

  try{
    auto opw = nNet_GetOperatorW(op);

    if (!opw){
      return (interpreterw->error(DoNothing.reason("op must be created first")));
    } else if (!opw->outputs().size() || !opw->outputs()[0]){
      return (interpreterw->error(
                      BadLogic.reason("value of op \'const\' must not be null")));
    }

    if (interpreterw->assignPlaceholder(op->name, opw->outputs()[0]))
      opw->outputs().push_back(opw->outputs()[0]);
    else return true;
  } catch(base::Error &error){
    return (nNet_GetInterpreterW(interpreter)->error(error));
  }
  return false;
}
} // namespace constance

Registration doRegisterConst(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = constance::Prepare;
  result.eval = constance::Eval;
  return result;
}
} // namespace macro
} // namespace kernels
} // namespace nn
