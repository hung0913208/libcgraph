#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
namespace kernels{
namespace macro{
namespace placeholder{
using namespace nn::interpreter;

Operator Prepare(Operator op, Interpreter interpreter, CString model){
  try{
    auto attr = OperatorW::config(model, "Placeholder");

    if (attr){
      auto opw = new interpreter::OperatorW(op, 2, interpreter, attr);

      if (opw)
        return opw->interface();
    } else
      nNet_GetInterpreterW(interpreter)->error(NotFound);
  } catch(base::Error& error){
    nNet_GetInterpreterW(interpreter)->error(rvalue(error));
  }
  return nullptr;
}

bool Eval(Operator op, Interpreter interpreter){
  auto  tensor  = nNet_CreateTensorC(interpreter, nullptr);

  using namespace nn::utils;

 #define FLOWCODE_WRONG "Operator must be create first"
 #define NOT_FOUND_DTYPE "attr \'dtype\'"
 #define BAD_SHAPE "nNet_SetTensorShape can\'t approve attr \'shape\'"

  if (!tensor) 
    return nn::utils::null<>(interpreter, DrainMem) != nullptr;
  else try{
    auto opw   = nNet_GetOperatorW(op);
    auto shape = opw->get<Shape>("shape");
  
    if (opw->outputs()[0])
      return false;

    if (!opw){
      return nullptr != clearFaulty(interpreter, tensor,
                                    BadLogic.reason(FLOWCODE_WRONG));
    } else if (!nNet_SetTensorNumTW(interpreter, tensor, opw->get<NumT>("dtype"))){
      return nullptr != clearFaulty(interpreter, tensor, 
                                    NotFound.reason(NOT_FOUND_DTYPE));
    }

    if (nNet_GetShapeSize(shape) > 0 && nNet_GetShapeNDims(shape) > 0){
      if (!nNet_SetTensorShapeW(interpreter, tensor, shape)){
        return nullptr != clearFaulty(interpreter, tensor, 
                                      BadLogic.reason(BAD_SHAPE));
      }
    }

    if (nNet_GetInterpreterW(interpreter)->assignPlaceholder(op->name, tensor))
      opw->outputs()[0] = tensor;
    else return true;
  } catch(base::Error &error){
    return nNet_GetInterpreterW(interpreter)->error(rvalue(error));
  }
  return false;
}
} // namespace placeholder

Registration doRegisterPlaceholder(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = placeholder::Prepare;
  result.eval = placeholder::Eval;
  return result;
}
} // namespace macro
} // namespace kernels
} // namespace nn
