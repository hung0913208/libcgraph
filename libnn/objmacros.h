#if !defined(LIBNN_MACROS_H_) && __cplusplus
#define LIBNN_MACROS_H_
#include "internal.h"

/* @NOTE: convenient macros */
#if __cplusplus
#define DEFINE_OP(space, op)                                                   \
  namespace nn{                                                                \
  namespace kernels{                                                           \
  namespace space{                                                             \
    Registration doRegister##op();                                             \
  }                                                                            \
  }                                                                            \
  }
#define PREPARE_OP(space, op)                                                  \
  namespace nn{                                                                \
  namespace kernels{                                                           \
  namespace space{                                                             \
  namespace op{                                                                \
    Operator Prepare(Operator op, Interpreter interpreter, CString model);     \
  }                                                                            \
  }                                                                            \
  }                                                                            \
  }
#define CHECK_OP(space, op)                                                    \
  namespace nn{                                                                \
  namespace kernels{                                                           \
  namespace space{                                                             \
  namespace op{                                                                \
    bool Check(Operator op, Interpreter interpreter);                          \
  }                                                                            \
  }                                                                            \
  }                                                                            \
  }
#define MACRO(op)                                                              \
  {                                                                            \
    Operator context{nullptr};                                                 \
    if ((context = (Operator) calloc(1, sizeof(OperatorDef)))){                \
      if (!nNet_PackOperator(context,                                          \
                             nn::kernels::macro::doRegister##op(), true))      \
        if (!nNet_InstallOperatorToInterpreter(#op, context, nullptr))         \
          warn(BadAccess).reason("can\'t not load macro "#op);                 \
    }                                                                          \
  }
#define FUNCTION(space, device, op)                                            \
  {                                                                            \
    Operator context{nullptr};                                                 \
    if ((context = (Operator) calloc(1, sizeof(OperatorDef)))){                \
      if (!nNet_PackOperator(context,                                          \
                             nn::kernels::space::doRegister##op(), false))     \
        if (!nNet_InstallOperatorToDevice(#op, context, device))               \
          warn(BadAccess).reason("can\'t not load function "#op);              \
    }                                                                          \
  }
#define TEMPLATE(op)                                                           \
  {                                                                            \
    Operator context{nullptr};                                                 \
    if ((context = (Operator) calloc(1, sizeof(OperatorDef)))){                \
      if (!nNet_PackOperator(context,                                          \
                             nn::kernels::templt::doRegister##op(), false))    \
        if (!nNet_InstallOperatorToInterpreter(#op, context, nullptr))         \
          warn(BadAccess).reason("can\'t not load template "#op);              \
    }                                                                          \
  }

#define SPEC_MACRO(model, op)                                               \
  {                                                                         \
    Operator context{nullptr};                                              \
    if ((context = (Operator)calloc(1, sizeof(OperatorDef)))) {             \
      if (!nNet_PackOperator(context, nn::kernels::macro::doRegister##op(), \
                             true))                                         \
        if (!nNet_InstallOperatorToInterpreter(#model "::" #op, context,    \
                                               nullptr))                    \
          warn(BadAccess).reason("can\'t not load macro " #op);             \
    }                                                                       \
  }
#define SPEC_FUNCTION(space, device, model, op)                              \
  {                                                                          \
    Operator context{nullptr};                                               \
    if ((context = (Operator)calloc(1, sizeof(OperatorDef)))) {              \
      if (!nNet_PackOperator(context, nn::kernels::space::doRegister##op(),  \
                             false))                                         \
        if (!nNet_InstallOperatorToDevice(#model "::" #op, context, device)) \
          warn(BadAccess).reason("can\'t not load function " #op);           \
    }                                                                        \
  }
#define SPEC_TEMPLATE(model, op)                                             \
  {                                                                          \
    Operator context{nullptr};                                               \
    if ((context = (Operator)calloc(1, sizeof(OperatorDef)))) {              \
      if (!nNet_PackOperator(context, nn::kernels::templt::doRegister##op(), \
                             false))                                         \
        if (!nNet_InstallOperatorToInterpreter(#model "::" #op, context,     \
                                               nullptr))                     \
          warn(BadAccess).reason("can\'t not load template " #op);           \
    }                                                                        \
  }

#define nNet_ConvertToInterpreterW(x)                                          \
          reinterpret_cast<nn::InterpreterW*>(x->priv)
#define nNet_GetInterpreterW(interpreter)                                      \
          ((interpreter) ? nNet_ConvertToInterpreterW(interpreter)             \
                         : (&nn::InterpreterW::instance()))
#define nNet_GetOperatorW(op)                                                  \
          reinterpret_cast<nn::interpreter::OperatorW*>(op->priv)
#define nNet_GetDeviceW(device)                                                \
          reinterpret_cast<nn::interpreter::DeviceW*>(device->_priv)
#endif
#endif  // LIBNN_MACROS_H_