#ifdef LIBCGRAPH_GIT_REFSPEC
#include "libbase/all.hpp"
#include "libfmath/fmath.h"
#else
#include "logcat.hpp"
#include "fmath.h"
#endif

#include "internal.h"
#include "tensor.h"
#include "type.h"
 
#include <string.h>
#include <assert.h>
#include <stdlib.h>

#if WINDOWS
#include <windows.h>

long nNet_MaxPhysicalMemory(){
  MEMORYSTATUSEX status;

  status.dwLength = sizeof(status);
  GlobalMemoryStatusEx(&status);
  return status.ullTotalPhys;
}
#else
#include <unistd.h>

long nNet_MaxPhysicalMemory(){
  long pages = sysconf(_SC_PHYS_PAGES);
  long page_size = sysconf(_SC_PAGE_SIZE);
  return pages*page_size;
}
#endif

int nNet_AlignedSizeOfType(int type){
 #if USE_FMATH
  switch(type){
  case Real32_t:
  case Real64_t:
    return fMat_SizeType(type);

  default:
    return nNet_SizeType(type);
  }
 #else
  return nNet_SizeType(type);
 #endif
}

Item nNet_DupplicatItem(Item item, int type){
  Array result = calloc(1, nNet_SizeType(type));

  if (result){
    memcpy(result, item, nNet_SizeType(type));
  }
  return result;
}

Array nNet_DuplicateArray(Array array, long size, int type){
  Array result = NULL;

  switch (type){
  case String_t:
    result = calloc(size, nNet_SizeType(type));

    if (result){
      char** output = (char**)result;
      char** input  = (char**)array;

      for (int i = 0; i < size; ++i){
        size_t len = strlen(input[i]);

        if ((output[i] = calloc(len, sizeof(char)))){
          memcpy(output[i], input[i], len);
        } else {
          for (int j = 0; j < i; ++j){
            free(output[i]);
          }

          free(output);
          return NULL;
        }
      }
    }
    break;

  default:
    result = calloc(size, nNet_SizeType(type));

    if (result){
      memcpy(result, array, size*nNet_SizeType(type));
    }
  }
  return result;
}

void nNet_RemoveIfNeeded(Array array, int size, int type){
  switch (type){
  case String_t:
    for (int i = 0; i < size; ++i){
      char* item = *((char**)nNet_GetArrayItem(array, i, type));
      if (item) free(item);
    }
    break;

  default:
    break;
  }
}

Item nNet_GetArrayItem(Array array, long index, int type){
 #if USE_VECTORIZE && USE_FMATH
  switch (type) {
  default:
    break;

  case Real32_t:
    return (Item)(&((Real32 *)__builtin_assume_aligned(array, 16))[index]);

  case Real64_t:
    return (Item)(&((Real64 *)__builtin_assume_aligned(array, 16))[index]);
  }
 #endif

  switch (type){
  case Bool_t:
    return (Item)(&((bool*)array)[index]);

  case String_t:
    return (Item)(&((char**)array)[index]);

  case Int32_t:
    return (Item)(&((Int32 *)array)[index]);

  case Int64_t:
    return (Item)(&((Int64 *)array)[index]);

  case Real32_t:
    return (Item)(&((Real32 *)array)[index]);

  case Real64_t:
    return (Item)(&((Real64 *)array)[index]);

  default:
    return NULL;
  }
}

Array nNet_SetArrayItem(Array array, Item item, long index, int type){
  switch (type) {
  case Bool_t:
    memcpy(&((bool *)array)[index], item, nNet_SizeType(type));
    break;

  case String_t:
    memcpy(&((char **)array)[index], item, nNet_SizeType(type));
    break;

  case Int32_t:
    memcpy(&((Int32 *)array)[index], item, nNet_SizeType(type));
    break;

  case Int64_t:
    memcpy(&((Int64 *)array)[index], item, nNet_SizeType(type));
    break;

  case Real32_t:
    memcpy(&((Real32 *)array)[index], item, nNet_SizeType(type));
    break;

  case Real64_t:
    memcpy(&((Real64 *)array)[index], item, nNet_SizeType(type));
    break;

  default:
    return NULL;
  }
  return array;
}

int nNet_SizeType(int type){
  switch (type){
  case Bool_t:
    return 1;

  case String_t:
    return sizeof(const char*);

  case Int32_t:
  case Real32_t:
    return 4;

  case Int64_t:
  case Real64_t:
    return 8;
  }
  return 0;
}

const char* nNet_NameType(int type){
  switch (type){
  case Bool_t:
    return "Bool";

  case String_t:
    return "String";

  case Int32_t:
    return "Int32";

  case Real32_t:
    return "Real32";

  case Int64_t:
    return "Int64";

  case Real64_t:
    return "Real64";

  default:
    return "Unknown";
  }
  return 0;
}

bool nNet_ConvertNumT(Item src, int stype, Item dst, int dtype) {
  Real32 real32;
  Real64 real64;
  Int32  int32;
  Int64  int64;

  if (!src || !dst) return false;
  switch (stype) {
  case String_t:
    if (dtype != String_t)
      return false;
    memcpy(dst, src, sizeof(const char*));
    break;

  case Bool_t:
    if (dtype != Bool_t)
      return false;
    memcpy(dst, src, sizeof(const char*));
    break;

  case Int32_t:
    switch (dtype) {
    case Int32_t:
      memcpy(dst, src, sizeof(Int32));
      break;

    case Real64_t:
      real64 = (Real64)(*((Int32*)(src)));
      memcpy(dst, &real64, sizeof(Real64));
      break;

    case Real32_t:
      real32 = (Real32)(*((Int32*)(src)));
      memcpy(dst, &real32, sizeof(Real32));
      break;

    case Int64_t:
      int64 = (Int64)(*((Int32*)(src)));
      memcpy(dst, &int64, sizeof(Int64));
      break;
    }
    break;

  case Int64_t:
    switch (dtype) {
    case Int64_t :
      memcpy(dst, src, sizeof(Int64));
      break;

    case Real32_t:
      real32 = (Real32)(*((Int64*)(src)));
      memcpy(dst, &real32, sizeof(Real32));
      break;

    case Real64_t:
      real64 = (Real64)(*((Int64*)(src)));
      memcpy(dst, &real64, sizeof(Real64));
      break;

    case Int32_t:
      int32 = (Int32)(*((Int64*)(src)));
      memcpy(dst, &int32, sizeof(Int32));
      break;
    }
    break;

  case Real32_t:
    switch (dtype) {
    case Real32_t:{
      memcpy(dst, src, sizeof(Real32));
      break;
    }

    case Real64_t:
      real64 = (Real64)(*((Real32*)(src)));
      memcpy(dst, &real64, sizeof(Real64));
      break;

    case Int32_t:
      int32 = (Int32)(*((Real32*)(src)));
      memcpy(dst, &int32, sizeof(Int32));
      break;

    case Int64_t:
      int64 = (Int64)(*((Real32*)(src)));
      memcpy(dst, &int64, sizeof(Int64));
      break;
    }
    break;

  case Real64_t:
    switch (dtype) {
    case Real32_t:
      real32 = (Real32)(*((Real64*)(src)));
      memcpy(dst, &real32, sizeof(Real32));
      break;

    case Real64_t:
      memcpy(dst, src, sizeof(Real64));
      break;

    case Int32_t:
      int32 = (Int32)(*((Real64*)(src)));
      memcpy(dst, &int32, sizeof(Int32));
      break;

    case Int64_t:
      int64 = (Int64)(*((Real64*)(src)));
      memcpy(dst, &int64, sizeof(Int64));
      break;
    }
    break;

  default:
    return false;
  }
  return true;
}
