#ifdef LIBCGRAPH_GIT_REFSPEC
#include <libbase/logcat.hpp>
#else
#include "logcat.hpp"
#endif

#include "type.h"
#include "interpreter.h"

/* @NOTE: global variables of libnn */
static bool loaded = false;

/* @NOTE: internal functions to control loading and unloading framework */
int  nNet_InitModels();
void nNet_ReleaseModels();

int nNet_LoadImporters();
int nNet_LoadExporters();

int nNet_UnloadImporters();
int nNet_UnloadExporters();

int nNet_LoadDevices();
int nNet_UnloadDevices();

int nNet_LoadMacros();
int nNet_UnloadOperators();

/* @NOTE: C-API of interpreters */
int nNet_LoadEverything(){
  if (loaded) return ENoError;  
  loaded = true;

  if (!nNet_InitModels())
    return error(EBadLogic, "system can't load any models");
  if (!nNet_LoadImporters())
    return error(EBadLogic, "it must need at least one importer to be loaded");
  if (!nNet_LoadExporters())
    return error(EBadLogic, "it must need at least one exporter to be loaded");
  if (!nNet_LoadDevices())
    return error(EBadLogic, "it must need at least one device to be loaded");
  nNet_LoadMacros();
  return ENoError;
}

int nNet_UnloadEverything(){
  if (!loaded) return ENoError;
  if (nNet_UnloadImporters())
    return error(EBadLogic, "there are still some importers to be loaded");
  if (nNet_UnloadExporters())
    return error(EBadLogic, "there are still some exporters to be unloaded");
  if (nNet_UnloadDevices())
    return error(EBadLogic, "there are still some devices to be unloaded");
  nNet_UnloadOperators();
  nNet_ReleaseModels();
  loaded = false;
  return ENoError;
}
