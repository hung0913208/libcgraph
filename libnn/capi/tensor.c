#include <stdlib.h>

#ifdef LIBCGRAPH_GIT_REFSPEC
#include <libbase/all.hpp>
#else
#include "macros.hpp"
#include "logcat.hpp"
#endif

#if HAS_C_ALIGNED_ALLOC
#if !defined(_ISOC11_SOURCE)
#define _ISOC11_SOURCE
#endif
#elif HAS_C_POSIX_MEMALIGN
#if LINUX
#if defined(_POSIX_C_SOURCE) && _POSIX_C_SOURCE < 200112L
#undef _POSIX_C_SOURCE
#endif

#define _POSIX_C_SOURCE 200112L
#elif APPLE
#if defined(_DARWIN_C_SOURCE) && _DARWIN_C_SOURCE < 200112L
#undef _DARWIN_C_SOURCE
#endif

#define _DARWIN_C_SOURCE 200112L
#endif
#endif

#if USE_VECTORIZE
#define ASSUME_ALIGNED(result, type)                            \
  switch (type) {                                               \
    default:                                                    \
      return NULL;                                              \
                                                                \
    case String_t:                                              \
      return __builtin_assume_aligned(result, sizeof(CString)); \
                                                                \
    case Bool_t:                                                \
      return __builtin_assume_aligned(result, sizeof(bool));    \
                                                                \
    case UInt_t:                                                \
      return __builtin_assume_aligned(result, sizeof(UInt32));  \
                                                                \
    case Int32_t:                                               \
      return __builtin_assume_aligned(result, sizeof(Int32));   \
                                                                \
    case Int64_t:                                               \
      return __builtin_assume_aligned(result, sizeof(Int64));   \
                                                                \
    case Real32_t:                                              \
      return __builtin_assume_aligned(result, sizeof(Real32));  \
                                                                \
    case Real64_t:                                              \
      return __builtin_assume_aligned(result, sizeof(Real64));  \
  }

int nNet_AlignedSizeOfType(int type);
#endif

#include <stdlib.h>
#include <errno.h>
#include <stdarg.h>
#include <string.h>
#include <stdio.h>

#if !defined(__MACH__) && !defined(__FreeBSD__)
#include <malloc.h>
#endif

#include "tensor.h"
#include "internal.h"

typedef void (*Release)(Tensor tensor, Interpreter interpreter);

/* @NOTE: brigate between C-API and CPP-API includes memory management array's
 * methods and some tensor's methods */
Shape    nNet_ReallocShapeW(Interpreter interpreter, Shape shape, int ndims);
Ordinate nNet_ReallocOrdinateW(Interpreter interpreter, Ordinate shape, int ndims);
Tensor   nNet_ReallocTensorW(Interpreter interpreter, Tensor tensor,
                             int type, bool force);
Tensor   nNet_ReallocRefTensorW(Interpreter interpreter, Tensor tensor,
                                Tensor src);

Shape    nNet_ReallocShape(Shape shape, int ndims);
Ordinate nNet_ReallocOrdinate(Ordinate shape, int ndims);
Tensor   nNet_ReallocTensor(Tensor tensor, int type, bool force);
Tensor   nNet_ReallocRefTensor(Tensor tensor, Tensor src);

bool nNet_CheckTensorOutdate(Interpreter interpreter, Tensor tensor);
bool nNet_CheckTensorShapeOutdate(Interpreter interpreter, Shape shape);
bool nNet_CheckOrdinateOutdate(Interpreter interpreter, Ordinate ordinate);

Array nNet_CloneIfNeeded(Array array, int size, int type);
bool  nNet_ConvertNumT(Item src, int stype, Item dst, int dtype);
long  nNet_GetTensorSize(Tensor tensor);

/* @NOTE: default callback which use to control Tensor */
void DontRemoveTensorData(Tensor tensor, Interpreter interpreter){
  tensor->_content = NULL;
  tensor->dims  = NULL;
  tensor->ndims = 0;
  tensor->_max  = 0;
  tensor->_size = 0;
  tensor->_begins = NULL;
  tensor->_ends   = NULL;
}

void JustRemoveTensorScales(Tensor tensor, Interpreter interpreter){
  tensor->_content = NULL;
  tensor->dims  = NULL;
  tensor->ndims = 0;
  tensor->_max  = 0;
  tensor->_size = 0;
  if (tensor->_begins) free(tensor->_begins);
  if (tensor->_ends) free(tensor->_ends);

  tensor->_begins = NULL;
  tensor->_ends = NULL;
}

void JustRemoveTensorDims(Tensor tensor, Interpreter interpreter){
  tensor->_content = NULL;
  tensor->ndims = 0;
  tensor->_max  = 0;
  tensor->_size = 0;

  if (tensor->dims) free(tensor->dims);
  if (tensor->_begins) free(tensor->_begins);
  if (tensor->_ends) free(tensor->_ends);

  tensor->dims = NULL;
  tensor->_begins = NULL;
  tensor->_ends = NULL;
}

/* @NOTE: implement Tensor C-API */
Shape nNet_CreateTensorShapeS(int ndims, ...){
  Shape result = nNet_ReallocShape(NULL, ndims);
  va_list vlist;

  if (!result) return NULL;

  va_start(vlist, ndims);
  for (int i = 0; i < ndims; ++i){
    result->dims[i] = va_arg(vlist, int);
  }
  va_end(vlist);

  return result;
}

Shape nNet_CreateTensorShapeW(int ndims, int *dims){
  return nNet_CreateTensorShapeC2(NULL, ndims, dims);
}

int nNet_GetShapeSize(Shape shape){
  int result = 1;

  for (int i = 0; i < shape->ndims; ++i){
    result *= shape->dims[i];
  }

  return result;
}

int nNet_GetShapeNDims(Shape shape){
  return shape->ndims;
}

Ordinate nNet_CreateOrdinateS(Tensor tensor, int size, ...){
  if (tensor == NULL || tensor->ndims > 0){
    Ordinate result = nNet_ReallocOrdinate(NULL, tensor? tensor->ndims: size);

    va_list vlist;
    int i;

    if (!result) return NULL;

    va_start(vlist, size);
    for (i = 0; i < size && i < result->ndims; ++i){
      int hook = va_arg(vlist, int);

      if (hook < 0) hook = -1;

      if (tensor && tensor->dims[i] <= hook){
        result->ends[i]   = tensor->dims[i];
        result->begins[i] = tensor->dims[i];
      } else if (tensor && tensor->_ends && hook > tensor->_ends[i]){
        result->ends[i]   = tensor->_ends[i] + 1;
        result->begins[i] = tensor->_begins[i];
      } else if (tensor && tensor->_begins && hook < tensor->_begins[i]){
        result->ends[i]   = tensor->_ends[i] + 1;
        result->begins[i] = tensor->_begins[i];
      } else {
        result->begins[i] = hook;
        result->ends[i]   = hook + 1;
      }
    }

    if (tensor){
      for (; i < tensor->ndims; ++i){
        result->begins[i] = tensor->_ends? tensor->_ends[i] + 1: tensor->dims[i];
        result->ends[i]   = tensor->_ends? tensor->_ends[i] + 1: tensor->dims[i];
      }
    }
    va_end(vlist);
    return result;
  } else
    return NULL;
}

Ordinate nNet_CreateOrdinateW1(Tensor tensor, int *ordinate, int size){
  return nNet_CreateOrdinateC2(NULL, tensor, ordinate, size);
}

Ordinate nNet_CreateOrdinateW2(Tensor tensor, int *begins, int bsize, int *ends,
                               int esize){
  return nNet_CreateOrdinateC3(NULL, tensor, begins, bsize, ends, esize);
}

Ordinate nNet_RewriteOrdinateS(Ordinate ordinate, Tensor tensor, int size, ...){
  va_list vlist;
  int i;

  if (!ordinate) return NULL;
  if (tensor && tensor->ndims != ordinate->ndims){
    if (tensor->ndims > ordinate->max){
      ordinate->begins = realloc(ordinate->begins, sizeof(int)*tensor->ndims);
      ordinate->ends   = realloc(ordinate->ends, sizeof(int)*tensor->ndims);
      ordinate->max    = tensor->ndims;
    }
    ordinate->ndims = tensor->ndims;
  }

  va_start(vlist, size);
  for (i = 0; i < size && i < ordinate->ndims; ++i){
    int hook = va_arg(vlist, int);

    if (hook < 0) hook = -1;
    if (tensor && tensor->dims[i] <= hook){
      ordinate->ends[i]   = tensor->dims[i];
      ordinate->begins[i] = tensor->dims[i];
    } else if (tensor && tensor->_ends && hook > tensor->_ends[i]){
      ordinate->ends[i]   = tensor->_ends[i] + 1;
      ordinate->begins[i] = tensor->_begins[i];
    } else if (tensor && tensor->_begins && hook < tensor->_begins[i]){
      ordinate->ends[i]   = tensor->_ends[i] + 1;
      ordinate->begins[i] = tensor->_begins[i];
    } else {
      ordinate->begins[i] = hook;
      ordinate->ends[i]   = hook + 1;
    }
  }

  if (tensor){
    for (; i < tensor->ndims; ++i){
      ordinate->begins[i] = tensor->_ends? tensor->_ends[i] + 1: tensor->dims[i];
      ordinate->ends[i]   = tensor->_ends? tensor->_ends[i] + 1: tensor->dims[i];
    }
  }
  va_end(vlist);
  return ordinate;
}

Ordinate nNet_RewriteOrdinateW1(Ordinate ordinate, Tensor tensor, int size,
                                int *dims){
  int i;

  if (!ordinate) return NULL;
  if (tensor && tensor->ndims != ordinate->ndims){
    if (tensor->ndims > ordinate->max){
      ordinate->begins = realloc(ordinate->begins, sizeof(int)*tensor->ndims);
      ordinate->ends   = realloc(ordinate->ends, sizeof(int)*tensor->ndims);
      ordinate->max    = tensor->ndims;
    }
    ordinate->ndims = tensor->ndims;
  }

  for (i = 0; i < size && i < ordinate->ndims; ++i){
    if (tensor && tensor->dims[i] <= dims[i]){
      ordinate->ends[i]   = tensor->dims[i];
      ordinate->begins[i] = tensor->dims[i];
    } else if (tensor && tensor->_ends && dims[i] > tensor->_ends[i]){
      ordinate->ends[i]   = tensor->_ends[i] + 1;
      ordinate->begins[i] = tensor->_begins[i];
    } else if (tensor && tensor->_begins && dims[i] < tensor->_begins[i]){
      ordinate->ends[i]   = tensor->_ends[i] + 1;
      ordinate->begins[i] = tensor->_begins[i];
    } else {
      ordinate->begins[i] = dims[i];
      ordinate->ends[i]   = dims[i] + 1;
    }
  }

  if (tensor){
    for (; i < tensor->ndims; ++i){
      ordinate->begins[i] = tensor->_ends? tensor->_ends[i] + 1: tensor->dims[i];
      ordinate->ends[i]   = tensor->_ends? tensor->_ends[i] + 1: tensor->dims[i];
    }
  }
  return ordinate;
}

Ordinate nNet_RewriteOrdinateW2(Ordinate ordinate, Tensor tensor,
                                int *begins, int bsize, int *ends, int esize){
  int i;

  if (!ordinate || (tensor->dims && tensor->ndims > 0))
    return NULL;
  if (tensor && tensor->ndims != ordinate->ndims){
    if (tensor->ndims > ordinate->max){
      ordinate->begins = realloc(ordinate->begins, sizeof(int)*tensor->ndims);
      ordinate->ends   = realloc(ordinate->ends, sizeof(int)*tensor->ndims);
      ordinate->max    = tensor->ndims;
    }
    ordinate->ndims = tensor->ndims;
  }

  for (i = 0; i < esize && i < ordinate->ndims; ++i){
    if (tensor && tensor->dims[i] <= ends[i]){
      ordinate->ends[i]   = tensor->dims[i];
    } else if (tensor && tensor->_ends && ends[i] > tensor->_ends[i]){
      ordinate->ends[i]   = tensor->_ends[i] + 1;
    } else {
      ordinate->ends[i]   = ends[i] + 1;
    }
  }

  if (tensor){
    for (; i < tensor->ndims; ++i){
      ordinate->ends[i]   = tensor->_ends? tensor->_ends[i] + 1: tensor->dims[i];
    }
  }

  for (i = 0; i < bsize && i < ordinate->ndims; ++i){
    if (tensor && tensor->_begins && begins[i] < tensor->_begins[i]){
      ordinate->begins[i] = tensor->_begins[i];
    } else {
      ordinate->begins[i] = begins[i];
    }
  }

  if (tensor && tensor->_begins && tensor->_ends && tensor->dims){
    for (; i < tensor->ndims; ++i){
      ordinate->begins[i] = tensor->_ends? tensor->_ends[i] + 1: tensor->dims[i];
    }
  }
  return ordinate;
}


Index nNet_OrdinateToIndex(Tensor tensor, Ordinate ordinate){
  Index result = 0, next = 0;
  int  *begins = NULL;
  int  *ends   = NULL;

  if (!tensor || !ordinate || tensor->ndims != ordinate->ndims)
    return -1;
  else {
    begins = tensor->_begins;
    ends   = tensor->_ends;
    next   = nNet_GetArraySizeT(tensor);
  }

  if (next < 0) return -1;
  for (int i = tensor->ndims; i > 0; --i){
    int begin = (begins && begins[i - 1] >= 0)? begins[i - 1]: 0;
    int end   = (ends   && ends[i - 1]   >= 0)? ends[i - 1 ] : tensor->dims[i - 1];
    int index = 0, from = result, to = next;

    if (ordinate->begins[i - 1] + 1 != ordinate->ends[i - 1])
      return -1;
    else if ((index = begin + ordinate->begins[i - 1]) >= end)
      return -1;

    result = from + index*(to - from)/tensor->dims[i - 1];
    next   = from + (index + 1)*(to - from)/tensor->dims[i - 1];
  }
  return result;
}

Index nNet_OrdinateToIndexW(Interpreter interpreter, Tensor tensor, Ordinate ordinate){
    Index result = 0, next = 0;
    int  *begins = NULL;
    int  *ends   = NULL;

    if (!tensor || !ordinate || tensor->ndims != ordinate->ndims)
        return -1;
    else {
        begins = tensor->_begins;
        ends   = tensor->_ends;
        next   = nNet_GetArraySizeTC(interpreter, tensor);
    }

    if (next < 0) return -1;
    for (int i = tensor->ndims; i > 0; --i){
        int begin = (begins && begins[i - 1] >= 0)? begins[i - 1]: 0;
        int end   = (ends   && ends[i - 1]   >= 0)? ends[i - 1 ] : tensor->dims[i - 1];
        int index = 0, from = result, to = next;

        if (ordinate->begins[i - 1] + 1 != ordinate->ends[i - 1])
            return -1;
        else if ((index = begin + ordinate->begins[i - 1]) >= end)
            return -1;

        result = from + index*(to - from)/tensor->dims[i - 1];
        next   = from + (index + 1)*(to - from)/tensor->dims[i - 1];
    }
    return result;
}

Tensor nNet_CreateTensorS(){ return nNet_ReallocTensor(NULL, Unknown, false); }

Tensor nNet_CreateTensorW(Shape shape){
  if (!shape) return nNet_CreateTensorS();
  if (shape->ndims != 0 && shape->dims != NULL){
    Tensor result = nNet_CreateTensorS();

    if (!result) return false;

    result->ndims = shape->ndims;
    result->dims  = shape->dims;

    shape->dims = NULL;
    shape->ndims = 0;

    return result;
  }
  return NULL;
}

Tensor nNet_ClearTensor(Tensor tensor){
  return nNet_ClearTensorW(NULL, tensor);
}

Tensor nNet_ClearTensorW(Interpreter interpreter, Tensor tensor){
  struct ShapeDef  fakeshape;
  Tensor result = NULL;

  memset(&fakeshape, 0, sizeof(struct ShapeDef));
  if (!(result = nNet_ReshapeTensorC1(interpreter, tensor, &fakeshape)))
    return result;
  
  result->type = Unknown;
  return result;
}

long nNet_GetTensorSize(Tensor tensor){
  if (tensor->_begins && tensor->_ends){
    int result = 1;

    for (int i = 0; i < tensor->ndims; ++i)
      result *= (tensor->_ends[i] - tensor->_begins[i]);
    return result;
  } else return tensor->_size;
}

CString nNet_GetTensorTypeName(Tensor tensor){
  if (!tensor || tensor->type == Unknown)
    return "NilT";
  else if (tensor->ndims == 0)
    return "ScalarT";
  else if (tensor->ndims == 1)
    return "VectorT";
  else if (tensor->ndims == 2)
    return "MatrixT";
  else return "TensorT";
}

int nNet_GetTensorType(Tensor tensor){
  if (!tensor || tensor->type == Unknown)
    return NilT;
  else if (tensor->ndims == 0 || 
           (tensor->ndims == 1 && tensor->dims[0] == 1))
    return ScalarT;
  else if (tensor->ndims == 1)
    return VectorT;
  else if (tensor->ndims == 2)
    return MatrixT;
  else return TensorT;
}

int nNet_GetTensorNumT(Tensor tensor){
  return tensor ? tensor->type : Unknown;
}

bool nNet_GetTensorShape(Tensor tensor, Shape shape){
  if (!shape || !tensor) return false;
  shape = nNet_ReallocShape(shape, tensor->ndims);

  if (shape->dims){
    for (int i = 0; i < tensor->ndims; ++i){
      shape->dims[i] = tensor->dims[i];

      if (tensor->_begins && tensor->_ends){
        shape->dims[i] = tensor->_ends[i] - tensor->_begins[i];

        if (!shape->dims[i])
          shape->dims[i] = 1;
      }
    }

    return true;
  }
  return false;
}

bool nNet_GetTensorShapeW(Interpreter interpreter, Tensor tensor, Shape shape) {
  if (!shape || !tensor) return false;
  shape = nNet_ReallocShapeW(interpreter, shape, tensor->ndims);

  if (shape->dims) {
    for (int i = 0; i < tensor->ndims; ++i) {
      shape->dims[i] = tensor->dims[i];

      if (tensor->_begins && tensor->_ends) {
        shape->dims[i] = tensor->_ends[i] - tensor->_begins[i];

        if (!shape->dims[i]) shape->dims[i] = 1;
      }
    }

    return true;
  }
  return false;
}

bool nNet_GetSubTensor(Tensor tensor, Ordinate ordinate, Tensor result){
  return nNet_GetSubTensorW(NULL, tensor, ordinate, result);
}

bool nNet_GetSubTensorW(Interpreter interpreter, Tensor tensor,
                        Ordinate ordinate, Tensor result){
  int pos = 0, scale = 1, block = 0;

  /* @NOTE: check parameters and remove tensor 'result' if it needs */
  if (!tensor || !result || 0 > tensor->type || tensor->type > Real64_t)
    return false;
  if (!ordinate || ordinate->ndims != tensor->ndims)
    return false;
  if (tensor->get)
    return tensor->get(tensor, ordinate, result);
  result = nNet_ClearTensorW(interpreter, result);

  if (!result) return false;
  else {
    /* @NOTE: copy lowlevel callbacks of tensor since result is a ref-tensor */

    result->device  = tensor->device;
    result->reshape = tensor->reshape;
    result->init    = tensor->init;
    result->diff    = tensor->diff;
    result->get     = tensor->get;
    result->set     = tensor->set;
  }

  for (int i = 0; i < tensor->ndims; ++i){
    /* @NOTE: ordinate might have use to select a area of tensor, it can easily
     * determine by compare the properties 'ends' and 'begins' with value -1
     */

    if (!block){
      if (ordinate->ends && ordinate->begins)
        block = ordinate->ends[i] != ordinate->begins[i];
      if (!block && ordinate->ends && ordinate->ends[i] < 0) block = true;
      if (!block && ordinate->begins && ordinate->begins[i] < 0) block = true;

      /* @NOTE: if this is getting a block, we must define result->release to
       * prevent it to remove reference memory area */
      if (block){
        if (ordinate->begins && ordinate->begins[i] >= 0){
          if (tensor->_begins){
            if ((tensor->dims[i] - tensor->_begins[i]) > ordinate->begins[i]){
              return false;
            }
          }
        }

        if (ordinate->ends && ordinate->ends[i] >= 0){
          if (tensor->_ends){
            if (tensor->_ends[i] - tensor->_begins[i] < ordinate->ends[i]){
              return false;
            }
          }
        }

        result->release = JustRemoveTensorScales;
        continue;
      }
    }

    if (!block){
      /* @NOTE: select a element of this tensor, in this case, ordinate must 
       * point to an exact posisiton, not a range */
      if (tensor->dims[i] < ordinate->ends[i]) return false;
      if (ordinate->begins[i] > ordinate->ends[i]) return false;
      if (tensor->_begins)
        pos += scale * (ordinate->ends[i] + tensor->_begins[i]);
      else
        pos += scale * ordinate->ends[i];
      scale *= tensor->dims[i];
    }
  }

  if (result->release == JustRemoveTensorScales){
    /* @NOTE: with the case selecting close-range block, we must define _begins
     * and _ends to indicate where is the begin and end of exact dimention.
     * These indicate the way to select value more easily and quiet simple */

    if ((ordinate->begins && ordinate->begins[0] < 0) ||
        (ordinate->ends   && ordinate->ends[0]   < 0)){
      /* @NOTE: Special case when we just want to make a reference to whole data 
       * of the tensor */

      memcpy(result, tensor, sizeof(struct TensorDef));
      result->dims = (int*) calloc(tensor->ndims, sizeof(int));

      if (!result->dims)
        return nNet_RemoveTensorW(interpreter, result) != NULL;
      else
        memcpy(result->dims, tensor->dims, sizeof(int)*tensor->ndims);
      result->release = JustRemoveTensorDims;

      return true;
    } else
      result = nNet_ReallocRefTensorW(interpreter, result, tensor);

    if (!result) return false;
    else
      result->_size = 1;

    for (int i = 0; i < tensor->ndims; ++i){
      result->_begins[i] = ordinate->begins[i] >= 0 ? ordinate->begins[i] : 0;

      if (ordinate->ends[i] < 0)
        result->_ends[i] = (tensor->_ends ? tensor->_ends[i] : tensor->dims[i]);
      else
        result->_ends[i] = ordinate->ends[i];

      if (result->_ends[i] != result->_begins[i])
        result->_size *= result->_ends[i] - result->_begins[i];
    }

    result->dims = (int*) calloc(tensor->ndims, sizeof(int));
    if (!result->dims)
      return nNet_RemoveTensorW(interpreter, result) != NULL;
    else
      memcpy(result->dims, tensor->dims, sizeof(int)*tensor->ndims);

    result->release  = JustRemoveTensorDims;
    result->ndims    = tensor->ndims;
    result->_content = tensor->_content;
    result->_max     = tensor->_max;
  } else {
    /* @NOTE: select a point inside this tensor, we just declare a dummy tensor
     * to store this data */

    Array array = nNet_GetArrayData(tensor);

    result->type  = tensor->type;
    result->ndims = 0;
    result->dims  = NULL;
    result->_max  = tensor->_max - pos;
    result->_size = 1;

    nNet_SetArrayData(result, nNet_GetArrayItem(array, pos, tensor->type));
    if (tensor->_content == array)
      result->release = DontRemoveTensorData;
  }

  return true;
}

bool nNet_SetTensorNumT(Tensor tensor, int type){
  return nNet_SetTensorNumTW(NULL, tensor, type);
}

bool nNet_SetTensorNumTW(Interpreter interpreter, Tensor tensor, int type){
  Array src_array, dst_array;
  bool  success = false;

  if (!tensor || !tensor->_content){
    if (tensor && tensor->type == Unknown){
      return nNet_ReallocTensorW(interpreter, tensor, type, true) != NULL;
    }
    return false;
  } else if (tensor->type == type){
    return true;
  } 
  // else if (tensor->init){
  //   success = tensor->init(tensor, interpreter, type, false);
  // }

  /* @NOTE: there are 2 different cases:
   * - Tensor has been create from scratch, then _begin and _end always be NULL
   * - Tensor has been create by making a reference with another tensor, we must
   * keep these data safe, so we have to create a clone of these data
   */
  if (tensor->type == type) return true;
  if (!tensor->_begins && !tensor->_ends &&
      tensor->release != JustRemoveTensorScales &&
      tensor->release != DontRemoveTensorData &&
      tensor->release != JustRemoveTensorDims){
    /* @NOTE: When tensor is created by nNet_CreateTensorS or nNet_CreatTensorW
     * it is quiet simple, just scale out or scale in the rang of tensor data
     * and reformat each element
     */
    int oldtype = tensor->type;

    tensor = nNet_ReallocTensor(tensor, type, false);
    if (!tensor)
      return false;
    else if (oldtype == Unknown)
      return true;

    dst_array = (src_array = nNet_GetArrayData(tensor));
    success   = true;

    if (nNet_SizeType(oldtype) > nNet_SizeType(type)){
      for (int i = 0; success && i < tensor->_size; ++i){
        Item src = nNet_GetArrayItem(src_array, i, oldtype);
        Item dst = nNet_GetArrayItem(dst_array, i, type);

        success = nNet_ConvertNumT(src, oldtype, dst, type);
      }
    } else {
      for (int i = tensor->_size; success && i > 0; --i){
        Item src = nNet_GetArrayItem(src_array, i - 1, oldtype);
        Item dst = nNet_GetArrayItem(dst_array, i - 1, type);

        success = nNet_ConvertNumT(src, oldtype, dst, type);
      }
    }

    success = success && (nNet_SetArrayData(tensor, dst_array) != NULL);
  } else {
    /* @NOTE: when tensor is created by make a reference with another tensor
     * throught out using nNet_GetSubTensor, we must create a cloning data
     * to keep the old data safe.
     */

    Release save = tensor->release;
    int  oldtype = tensor->type;

    struct TensorDef faketensor;

    memcpy(&faketensor, tensor, sizeof(struct TensorDef));

    tensor->release = DontRemoveTensorData;
    src_array = nNet_GetArrayData(tensor);
    tensor    = nNet_ReallocTensor(tensor, type, true);
    dst_array = nNet_GetArrayData(tensor);

    if (tensor){
      int* ordinate = (int*) calloc(faketensor.ndims, sizeof(int));
      int* begins   = faketensor._begins;
      int* ends     = faketensor._ends;
      int  maxsize  = 1;
      int  curr     = 0;

      for (int i = 0; i < faketensor.ndims; ++i)
        maxsize *= faketensor.dims[i];

      success   = true;
      for (int i = 0, j = 0; success && i < maxsize; ++i){
        int begin = !begins || begins[j] < 0? 0: begins[j];
        int end   = !ends   || ends[j]   < 0? faketensor.dims[j]: ends[j];

        if (begin <= ordinate[j] && ordinate[j] < end){
          Item src = nNet_GetArrayItem(src_array, i, oldtype);
          Item dst = nNet_GetArrayItem(dst_array, curr, type);

          success = nNet_ConvertNumT(src, oldtype, dst, type);
          curr++;
        }

        if (ordinate[j] + 1 == faketensor.dims[j])
          j++;
        else ordinate[j]++;
      }

      free(ordinate);
      if (tensor->ndims == 0 && tensor->_size == 1){
        success = nNet_ConvertNumT(src_array, oldtype, dst_array, type);
      }
    }

    success = success && (nNet_SetArrayData(tensor, dst_array) != NULL);
    if (save) save(&faketensor, interpreter);
    if (faketensor.dims) free(faketensor.dims);
    if (faketensor._content) free(faketensor._content);
    if (faketensor._begins) free(faketensor._begins);
    if (faketensor._ends) free(faketensor._ends);

    if (!tensor) return false;
    if (save != DontRemoveTensorData && save != JustRemoveTensorScales && 
        save != JustRemoveTensorDims){
      tensor->release = save;
    } else if (!tensor->init) tensor->release = NULL;
    
    if (!success)
      return nNet_RemoveTensorW(interpreter, tensor) != NULL;
  }

  if (success) tensor->type = type;
  return success;
}

bool nNet_SetTensorShape(Tensor tensor, Shape shape){
  return nNet_SetTensorShapeW(NULL, tensor, shape);
}

bool nNet_SetTensorShapeW(Interpreter interpreter, Tensor tensor, Shape shape){
  if (!tensor || !shape) return false;
  if (tensor->reshape) return tensor->reshape(tensor, shape);

  /* @NOTE: there are 2 different cases:
   * - Tensor has been create from scratch, then _begin and _end always be NULL
   * - Tensor has been create by making a reference with another tensor, so
   * _begins and _ends indicate where is actually the boundary of this tensor
   */

  if (tensor->_begins == NULL && tensor->_ends == NULL){
    /* @NOTE: we just check whether or not tensor.size == shape.size */
    int size = 1;

    for (int i = 0; i < shape->ndims; ++i){
      size *= shape->dims[i];
    }

    if (size != tensor->_size && tensor->_size != 0){
      int oldtype  = tensor->type;

      tensor = nNet_ClearTensorW(interpreter, tensor);
      if (tensor)
        tensor->type = oldtype;
      else
        return false;
    }

    /* @NOTE: after checking or configure, we must clear tensor->dims and reshape
     * tensor throught change this property
     */
    tensor = nNet_ReshapeTensorC1(interpreter, tensor, shape);
    if (!tensor || !tensor->dims) return false;
  } else {
    /* @NOTE: because this shape might change the structure of tensor entirely
     * so, to be safe, we must clone a new tensor and manipulate on this
     */
    struct TensorDef faketensor;
    Array src_array, dst_array;
    bool  success = true;

    memcpy(&faketensor, tensor, sizeof(struct TensorDef));
    tensor->release = DontRemoveTensorData;
    tensor = nNet_ReallocTensorW(interpreter, tensor, tensor->type, true);

    if (!tensor) return false;
    else if (!nNet_UseTemporaryTensor(interpreter, &faketensor))
      return false;
    else {
      src_array = nNet_GetArrayData(&faketensor);
      dst_array = nNet_GetArrayData(tensor);
    }

    if (!nNet_KillTemporaryTensor(interpreter, &faketensor))
      return false;
    else if (!src_array || !dst_array)
      return false;

    for (int i = 0, k = 0, l = 0; i < faketensor.ndims; ++i){
      for (int j = 0; success && j < faketensor.dims[i]; ++j, ++k){
        if (faketensor._begins[i] <= j && j < faketensor._ends[i]){
          Item src = nNet_GetArrayItem(src_array, k, tensor->type);
          Item dst = nNet_GetArrayItem(dst_array, l, tensor->type);

          success = nNet_ConvertNumT(src, faketensor.type, dst, tensor->type);
          l++;
        }
      }
    }

    success = success && nNet_SetArrayData(tensor, dst_array) != NULL;
    nNet_TryDropingArray(&faketensor, src_array);

    if (faketensor.release) faketensor.release(&faketensor, interpreter);
    if (faketensor._content) free(faketensor._content);
    if (faketensor.dims) free(faketensor.dims);
    if (faketensor._begins) free(faketensor._begins);
    if (faketensor._ends) free(faketensor._ends);
    if (!success) return nNet_RemoveTensor(tensor) != NULL;

    /* @NOTE: after checking or configure, we must clear tensor->dims and reshape
     * tensor throught change this property.
     */
    tensor = nNet_ReshapeTensorS(tensor, shape);

    if (!tensor || !tensor->dims){
      return false;
    } else if (tensor->release == DontRemoveTensorData){
      /* @NOTE: if tensor isn't recreated with lowlevel api, it's release callback
       * must be remove to prevent memory leak */

      if (faketensor.release != DontRemoveTensorData && 
          faketensor.release != JustRemoveTensorScales &&
          faketensor.release != JustRemoveTensorDims)
        tensor->release = faketensor.release;
      else if (!tensor->init) tensor->release = NULL;
    }
  }
  return true;
}

bool nNet_SetSubTensor(Tensor tensor, Ordinate ordinate, Tensor value){
  return nNet_SetSubTensorW(NULL, tensor, ordinate, value);
}
 
bool nNet_SetSubTensorW(Interpreter interpreter, Tensor tensor,
                          Ordinate ordinate, Tensor value){
  Tensor tmp     = value ? nNet_CreateTensorS() : NULL;
  bool   success = true;
  Array  src_array, dst_array;

  /* @NOTE: the easiest way is to get reference tensor and copy directly value
   * from tensor 'value' to this temporary tensor
   */

  if (!nNet_GetSubTensorW(interpreter, tensor, ordinate, tmp)){
    nNet_RemoveTensor(tmp);
    return false;
  }

  /* @NOTE: check if 2 tensor 'tmp' and 'value' have the same size and copy data
   * from src to dst
   */
  if (tmp->_size != value->_size){
    nNet_RemoveTensor(tmp);
    return false;
  }

  src_array = nNet_GetArrayData(value);
  dst_array = nNet_GetArrayData(tensor);

  for (int i = 0; success && i < tmp->_size; ++i){
    Item src = nNet_GetArrayItem(src_array, i, value->type);
    Item dst = nNet_GetArrayItem(dst_array, i, tmp->type);

    success = nNet_ConvertNumT(src, tensor->type, dst, tensor->type);
  }

  nNet_TryDropingArray(value, src_array);
  return success && nNet_SetArrayData(tensor, dst_array);
}

bool nNet_IsRefTensor(Tensor tensor){
  return tensor->_begins && tensor->_ends;
}

Index nNet_CalcIndex(Tensor tensor, int pos){
  int  *begins = NULL;
  int  *ends   = NULL;

  if (!tensor) return -1;
  else {
    begins = tensor->_begins;
    ends   = tensor->_ends;
  }

  if (!begins && !ends) return pos;
  else if (tensor->_size <= pos)
    return -1;
  else {
    Index result = 0, next = nNet_GetTensorSize(tensor);
    Index vresult = 0, vnext = tensor->_size;

    for (int i = tensor->ndims; i > 0; --i){
      int begin = (begins && begins[i - 1] >= 0)? begins[i - 1]: 0;
      int end   = (ends   && ends[i - 1]   >= 0)? ends[i - 1 ] : tensor->dims[i - 1];
      int to0 = next, from0 = result, to1 = vnext, from1 = vresult;

      for (int j = begin; j < end; ++j){
        vresult = from1 + (j - begin)*(to1 - from1)/(end - begin);
        vnext   = from1 + (j - begin + 1)*(to1 - from1)/(end - begin);

        if (vresult <= pos && pos < vnext){
          result = from0 + j*(to0 - from0)/tensor->dims[i - 1];
          next   = from0 + (j + 1)*(to0 - from0)/tensor->dims[i - 1];
          break;
        }
      }

      if (begin == end){
        result = from0 + begin*(to0 - from0)/tensor->dims[i - 1];
        next   = from0 + (begin + 1)*(to0 - from0)/tensor->dims[i - 1];
      }
    }

    return result;
  }
}

Item nNet_GetTensorItemS(Tensor tensor, Index index){
  Array array = nNet_GetArrayData(tensor);

  if (index < 0) return NULL;
  else {
    Item item = nNet_GetArrayItem(array, index, tensor->type);

    if (array != tensor->_content)
      item = nNet_DupplicatItem(item, tensor->type);

    nNet_TryDropingArray(tensor, array);
    return item;
  }
}

bool nNet_SetTensorItemS(Tensor tensor, Index index, Item item){
  if (index < 0 || !tensor || !item) return false;
  else {
    Array array = nNet_GetArrayData(tensor);

    if (!array) return false;
    else if (nNet_SetArrayItem(array, item, index, tensor->type)){
      nNet_SetArrayData(tensor, array);
      return true;
    } else {
      nNet_TryDropingArray(tensor, array);
      return false;
    }
  }
}

Item nNet_GetTensorItemW(Tensor tensor, Ordinate ordinate){
  if (!tensor || !ordinate) return NULL;
  return nNet_GetTensorItemS(tensor, nNet_OrdinateToIndex(tensor, ordinate));
}

bool nNet_SetTensorItemW(Tensor tensor, Ordinate ordinate, Item item){
  if (!tensor || !ordinate || !item) return false;
  return nNet_SetTensorItemS(tensor, nNet_OrdinateToIndex(tensor, ordinate), item);
}

/* @NOTE: hackable functions, they are very dangerous but they can speed up if 
 * they are used carefully 
 */
#if !USE_VECTORIZE
Array nNet_AllocateArray(long size, int type){
  return calloc((type >= 0? nNet_SizeType(type): 1), size);
}
#elif HAS_C_ALIGNED_ALLOC || HAS_C_POSIX_MEMALIGN || HAS_C_ALIGNED_MALLOC
Array nNet_AllocateArray(long size, int type){
  int   aligned = nNet_AlignedSizeOfType(type);
  Array result  = NULL;

  size = size*nNet_SizeType(type);
  size = size + size%aligned;
  size = size < aligned? aligned: size;

 #if HAS_C_ALIGNED_ALLOC
  result = aligned_alloc(aligned, size);

  if (!result)
    result = malloc(size);

  return result;
 #elif HAS_C_POSIX_MEMALIGN
  int   err_code = 0;

  if (aligned % sizeof(void*) || aligned <= sizeof(void*))
    return calloc(1, size);  
  else if (!(err_code = posix_memalign(&result, aligned, size)))
    return result;
  } else if (err_code == EINVAL){
    error(BadAccess, "The alignment argument was not a power of two, or was "
                     "not a multiple of sizeof(void *).");
  } else if (err_code == ENOMEM){
    error(DrainMem, "There was insufficient memory to fulfill the allocation "
                    "request.");

    if (!result)
      result = malloc(size);
  }
  return result;
#elif HAS_C_ALIGNED_MALLOC
  result = _aligned_malloc(size, aligned);

  if (!result)
    result = malloc(size);

  return result;
#endif
}
#elif !(HAS_CXX_ALIGNED_ALLOC || HAS_CXX_POSIX_MEMALIGN || HAS_CXX_ALIGNED_MALLOC)
Array nNet_AllocateArray(long size, int type){
  return calloc((type >= 0? nNet_SizeType(type): 1), size);
}
#endif  // !USE_VECTORIZE

Tensor nNet_ResetTensor(Tensor tensor){
  if (tensor->_begins && tensor->_ends){
    Item tmp = calloc(1, nNet_SizeType(tensor->type));

    if (tmp)
      memset(tmp, 0, nNet_SizeType(tensor->type));
    else return NULL;

    for (int i = 0; i < nNet_GetTensorSize(tensor); ++i)
      nNet_SetTensorItemS(tensor, nNet_CalcIndex(tensor, i), tmp);
    return tensor;
  } else {
    Array array = nNet_GetArrayData(tensor);
    int   size  = nNet_SizeType(tensor->type)*nNet_GetArraySizeT(tensor);

    if (array){
      memset(array, 0, size);
      return nNet_SetArrayData(tensor, array);
    } else return NULL;
  }
}

void nNet_TryDropingItem(Tensor tensor, Item item, Index index){
  Device dev   = (Device)tensor->_device;
  Array  array = nNet_GetArrayData(tensor);

  if (!item) return;
  if (dev->level != GENERIC_PLATFORM ||
      nNet_GetArrayItem(array, index, tensor->type) != item){
    free (item);
  }
}

void nNet_TryDropingArray(Tensor tensor, Array array){
  if (array && array != tensor->_content)
    free(array);
}

Array nNet_TrimArrayData(Tensor tensor){
  Array fetched = nNet_GetArrayData(tensor);


  if (tensor->_begins && tensor->_ends){
    Array result = nNet_AllocateArray(nNet_GetTensorSize(tensor), tensor->type);
    bool success = true;

    if (!result) return NULL;
    for (int i = 0; success && i < nNet_GetTensorSize(tensor); ++i){
      Item src = nNet_GetArrayItem(fetched, nNet_CalcIndex(tensor, i),
                                   tensor->type);
      Item dst = nNet_GetArrayItem(result, i, tensor->type);

      if (!(success = nNet_ConvertNumT(src, tensor->type, dst, tensor->type))){
        free(result);
        return NULL;
      }
    }

    nNet_TryDropingArray(tensor, fetched);

   #if USE_VECTORIZE
    ASSUME_ALIGNED(result, tensor->type)
   #else
    return result;
   #endif
  } else {
   #if USE_VECTORIZE
    ASSUME_ALIGNED(fetched, tensor->type)
   #else
    return fetched;
   #endif
  }
}

Array nNet_GetArrayData(Tensor tensor){
 #if USE_VECTORIZE
  if (tensor){
    Array result   = tensor->_array.get ? tensor->_array.get(tensor)
                                      : tensor->_content;

    ASSUME_ALIGNED(result, tensor->type);
  } else 
    return NULL;
 #else
  if (tensor){
    return tensor->_array.get ? tensor->_array.get(tensor)
                              : tensor->_content;
  } else 
    return NULL;
 #endif
}

Tensor nNet_SetArrayData(Tensor tensor, Array array){
  if (!tensor)
    return nNet_RemoveTensor(tensor);

  if (tensor->_array.set)
    return tensor->_array.set(tensor, array) ? tensor : NULL;

  if (tensor->_content == array){
    return tensor;
  } else {
    /* @NOTE: we must remove our own data first to prevent memory leak before
     * assign array to tensor->_context */

    if (tensor->release)
      tensor->release(tensor, NULL);

    if (tensor->_content)
      free(tensor->_content);

    tensor->_content = array;
    return tensor;
  }
}
