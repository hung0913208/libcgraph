#if !defined(LIBNN_INTERPRETER_INTERNAL_H_)
#define LIBNN_INTERPRETER_INTERNAL_H_
#include "interpreter.h"
#include "resolvers.h"
#include "schedule.h"

#if __cplusplus
#include <map>
#include <list>
#include <unistd.h>

#ifdef LIBCGRAPH_GIT_REFSPEC
#include <libbase/all.hpp>
#else
#include "logcat.hpp"
#include "auto.hpp"
#include "table.hpp"
#endif

namespace nn{
enum Status{
  Unknown   = 0,
  Initial   = 1,
  Running   = 2,
  Releasing = 3,
  Pausing   = 4
};

using namespace interpreter;

class InterpreterW;
class Consumer: public interpreter::Couple{
 private:
  using Yield = std::function<void(Gates)>;

 public:
  explicit Consumer(InterpreterW& interpreter, std::shared_ptr<Queue>& queue,
                    int index, pthread_mutex_t* lock, bool* flag);
  ~Consumer();

 public:
  base::Error init(InterpreterW& interpreter, Queue& queue, int index);
  base::Error fetch(Gates& inputs, Yield& yield);
  base::Error done(Gates& inputs);

 public:
  bool disconnect();
  bool wait();

 public:
  void flush();
  bool finished();
  bool error();

  bool error(base::Error& error);
  bool invoke(Gates inputs, std::function<void(Gates)> yield) final;

 private:
  bool _flushing;
  int  _index;

  Interpreter      _provider;
  pthread_mutex_t *_ilock, *_olock;
};

class InterpreterW: public interpreter::Couple{
 public:
  using Select = std::function<std::size_t(InterpreterW&)>;
  using Step   = std::pair<interpreter::Lock, std::vector<Operator>>;
  using Gates  = std::vector<std::pair<std::string, Tensor>>;
  using Yield  = std::function<void(Gates)>;

  enum Status{
    Stop     = 0,
    Initing  = 1,
    Running  = 2,
    Flushing = 3,
    Pause    = 4
  };

 private:
  using Placeholders = std::map<std::string, Tensor>;
  using TypeHolders  = std::map<std::string, bool>;
  using Operators    = std::map<std::string, Operator>;
  using Devices      = std::map<std::string, Device>;

  using GCIndex = std::tuple<int, int, bool>;
  using Task    = std::pair<Gates, Yield>;

 private:
  using Steps     = std::vector<Step>;
  using String    = std::string;
  using PConsumer = std::tuple<std::shared_ptr<Consumer>, 
                               pthread_mutex_t*, bool*>;
  using Error     = base::Error;

 public:
  explicit InterpreterW(std::string&& graph, bool force = false,
                        bool lazyload = false);
  explicit InterpreterW(std::fstream&& stream, bool force = false,
                        bool lazyload = false);
  explicit InterpreterW(std::fstream& stream, bool force = false,
                        bool lazyload = false);

 public:
  void operator=(InterpreterW&) = delete;
  InterpreterW(InterpreterW&) = delete;

 public:
  ~InterpreterW();

  /* @NOTE: load a session into a Interpreter */
  void operator()(std::function<void(InterpreterW&)> session);
  void operator()(std::function<void()> session);

  /* @NOTE: load a session in an open space inside these two method
   * For example:
   * InterpreterW interpreter{};
   * ...
   * interpreter.start()  // <-- do everything from hear
   * ...
   * interpreter.finish() // <-- remove everything inside interpreter now
   *
   * However, because everything will be remove immediately when InterpreterW
   * destroy itself so we must be careful about its declassor
   */
  base::Error start();
  base::Error finish();

 public:
  static InterpreterW& instance();
  static InterpreterW& gcollector();
  static base::Error   install(std::string name, Device config);

 public:
  /* @NOTE: load and render graph to memory */
  bool load(std::string&& graph, bool lazyload = false);
  bool load(std::fstream&& graph, bool lazyload = false);
  bool load(std::fstream&  graph, bool lazyload = false);

  /* @NOTE: invoke tensorgraph with input gates and yield result in many ways */
  bool invoke(InterpreterW::Gates inputs, Yield yield, bool streaming);
  bool invoke(Gates inputs, std::shared_ptr<Collector> collector);
  bool invoke(Gates inputs, Yield yield) final;
  bool invoke(Yield yield);

  /* @NOTE: these methods bellow can use with a collector */
  Gates invoke(Gates inputs);
  Gates invoke();

  /* @NOTE: after finishing everything, call wait2end to finish every or 
   * tearDownGraph to cancel everything*/
  void waitUtilEnd();
  void tearDownGraph();

  /* @NOTE: hook a new tensor into a specific device, the device would be 
   * indicated by its name, a operator of this device or use default device 
   */
  Tensor hook(std::string device, Tensor tensor);
  Tensor hook(Operator op, Tensor tensor);
  Tensor hook(Tensor tensor);

  /* @NOTE: assign or remove raw pointer to keep them into Interpreter's GC */
  bool assignPointer(Pointer pointer, int type, bool dummy = false);
  bool isDummyPointer(Pointer pointer);
  void removePointer(Pointer pointer);

  /* @NOTE: assign or remove operator to keep them live into Interpreter's GC */
  bool assignOperator(Operator op, bool* &link);
  bool assignOperator(std::string name, Operator op);
  bool removeOperator(std::string name);

  bool assignDevice(std::string name, Device device);
  bool removeDevice(Device device);
  bool removeDevice(std::string name);

  bool assignPlaceholder(std::string name, Tensor holder, bool variance = true);
  bool removePlaceholder(std::string name);

  /* @NOTE: serveral utilities to support finding Interpreter's stub */
  bool     findPointer(Pointer pointer, int type);
  Device   findDevice(Operator op);
  Device   findDevice(std::string name);
  Tensor   findPlaceholder(std::string name, int variance = -1);
  Operator findOperator(std::string name, bool only_graph = false);
  Operator findOperator(std::string space, std::string name, bool only_graph = false);
  CString  findOperator(Operator op);

  /* @NOTE: access throw private parameters */
  inline const Placeholders& placeholders(){ return _placeholders; }
  inline const Operators& operators(){ return _operators; }
  inline const Steps& flow(){ return _flowchart; }
  inline CInterpreter& interface(){ return _interpreter; }
  inline std::shared_ptr<Collector> collector(){ return _collector; }
  inline std::map<Thread, PConsumer>& consumers(){ return _consumers; }

  /* @NOTE: basic utilities to intercept  Interpreter when it's on running */
  inline void lock(){ pthread_mutex_lock(&_slock); }
  inline void unlock(){ pthread_mutex_unlock(&_slock); }

  base::Error error();
  base::Error error(base::Error&& error);
  base::Error error(base::Error& error);

  Status   status();
  Devices& devices();

  int  inactive();
  int  active();

  bool use(std::pair<const String, Device>& device);
  bool use(const String&& device);
  int  resolve(int method = -1);
  void render();

 protected:
  Device sysbase();
  int    complexity(Operator op);
  bool   own(Thread thread);

 protected:
  static Device get(std::string name);

 private:
  Tensor hook(Tensor tensor, Device device);
  bool   init(std::string&& graph, bool lazyload = false);
  void   request(Task&& task, InterpreterW* redirect = nullptr);
  void   clear();

 private:
  void makeConsumer(InterpreterW* redirect, bool wait, int index = -1);
  bool isOkey4Loading(std::string&& name, Device device);

  bool reconstruct();
  bool transcript(std::string&& graph, bool lazyload);
  bool arrange();

 private:
  Devices      _devices;
  Operators    _operators;
  Placeholders _placeholders;

 private:
  std::shared_ptr<Collector>  _collector;
  std::map<Thread, PConsumer> _consumers;
  std::map<Pointer, GCIndex>  _gcmapping;

  std::vector<Task>  _tasks;
  std::list<Pointer> _gcvector;

 public:
  std::vector<Step> _flowchart;
  pthread_mutex_t   _mlock, _ilock, _qlock, _slock;
  Status            _status;

 private:
  CInterpreter _interpreter;
  std::size_t  _index, _resolver;

  bool  _optimized, _notouched, _openspace;
  Yield _def_yield;
  Error _error;
};
} // namespace nn

namespace nn{
namespace interpreter{
/* @define: class OperatorW defines everything about a operator when it's
 * deployed to an interpreter */
class OperatorW{
 public:
  using Attrs  = std::map<std::string, base::Auto>;
  using Model  = std::function<base::Auto(base::Auto, const std::type_info&)>;
  using Config = std::function<bool(OperatorW&, Attrs&&, Model model)>;

 private:
 #if ENABLE_FORWARD_BACKWARD_OPERATORS
  using Gate = std::pair<Operator, Tensor>;
 #endif

  using Inputs   = std::vector<std::pair<CString, Tensor>>;
  using Outputs  = std::vector<Tensor>;
  using Supports = std::vector<Device>;

 public:
  explicit OperatorW(Operator op, std::size_t max_attrs, 
                     Interpreter interpreter, Config config);
  explicit OperatorW(Operator op, std::size_t max_attrs, Interpreter interpreter,
                     Device device, Config config);
  virtual ~OperatorW();

 public:
  std::function<bool(OperatorW&, Attrs&&, Model model)> updateAttr;
  base::Property<bool> isConnected;

 public:
  inline std::string name(){ return std::string{interface()->name}; }

  inline Interpreter& interpreter(){ return _interpreter; }

  inline Operator& interface(){
    _operator->priv = this;
    return _operator; 
  }

  inline Inputs&  inputs(){ return _inputs; }
  inline Outputs& outputs(){ return _outputs; }

 public:
  template<typename Type> Type get(std::string&& name){
    return _attrs.get(name).get<Type>();
  }

  template<typename Type> bool set(std::string&& name, Type value){
    return _attrs.put(name, base::Auto{}.set(value)) != -1;
  }

  inline void clr(std::string&& name){ _attrs.clr(name); }
  inline bool exist(std::string&& name){
    return _attrs.index(rvalue(name)) != -1;
  }

  bool error(base::Error& error) const;
  bool error(base::Error&& error) const;

 public:
  operator bool();

 public:
  static Config config(CString model, CString op);

 private:
  static bool inputs(Operator op, CString* names, int size);
  static bool link(Operator op, Operator* nodes, int size);

  bool isSupported(std::pair<const std::string, Device>& device);

 protected:
  base::Table<std::string, base::Auto> _attrs;

 protected:
  /* @NOTE: current profile of this operator */
  Interpreter _interpreter;
  Operator    _operator;

  /* @NOTE: database of this operator */
  Inputs   _inputs;
  Outputs  _outputs;
  Supports _supports;

 public:
 #if ENABLE_FORWARD_BACKWARD_OPERATORS
  std::vector<Gate>     next;
  std::vector<Operator> previous;
 #endif

 private:
  bool _connected, *_notouched;
};

/* @define: class DeviceW defines everything about a device including virtual
 * and real devices */
class DeviceW{
 public:
  using Operators = std::map<std::string, Operator>;

 public:
  explicit DeviceW(std::string name):
      _name{name}, _previous{nullptr}, _next{nullptr}{
    if (!(_system = (Device) calloc(1, sizeof(SystemDef))))
      throw DrainMem;
    else if (!(_system->opnum = (int*) calloc(1, sizeof(int))))
      throw DrainMem;
  }

  virtual ~DeviceW(){
    for (auto& item: _operators){
      auto op = std::get<1>(item);

      if (!nNet_IsTemplateOperator(op) || nNet_CheckStatus() == Releasing){
        if (op->_event._priv)
          free(op->_event._priv);
        free(op);
      }
    }

    if (!nNet_IsTemplateDevice(interface()) || nNet_CheckStatus() == Releasing){
      if (_system->opnum) free(_system->opnum);
      _system->opnum = nullptr;

      if (nNet_IsTemplateDevice(interface())){
        if (_system->_enable)
          free(_system->_enable);
        _system->_enable = nullptr;
      } else if (_system->_enable)
        *(_system->_enable) = false;

      free(_system);
    } else warn(BadLogic.reason("remove a template Device"));
  }

 public:
  inline Device& interface(){
    if (_system){
      if (_system->opnum)
        *(_system->opnum) = _operators.size();
      if (!_system->_priv) _system->_priv = this;
    }
    return _system;
  }

  inline void encircle(DeviceW* device){
    device->_next   = this;
    this->_previous = device;
  }

  inline std::string&& name(){ return rvalue(_name); }

 public:
  virtual Operators& operators(){ return _operators; }

 private:
  std::string _name;
  Operators   _operators;
  Device      _system;
  DeviceW    *_previous, *_next;
};
} // namespace interpreter

namespace model{
using Attribute = std::function<interpreter::OperatorW::Config(std::string&&)>;

typedef base::Error(*Import)(std::string&&, InterpreterW&, bool lazyload);
typedef base::Error(*Export)(InterpreterW&, std::string&);
} // namespace model
} // namespace nn

nn::interpreter::OperatorW& operator<<(nn::interpreter::OperatorW* op,
                                       base::Error&& error);
nn::interpreter::OperatorW& operator<<(const nn::interpreter::OperatorW& op,
                                       base::Error&& error);
nn::interpreter::OperatorW& operator<<(nn::interpreter::OperatorW* op,
                                       base::Error& error);
nn::interpreter::OperatorW& operator<<(const nn::interpreter::OperatorW& op,
                                       base::Error& error);
#endif

#if __cplusplus
extern "C"{
#endif
/* @NOTE: working with temporary tensors */
bool nNet_UseTemporaryTensor(Interpreter interpreter, Tensor tensor);
bool nNet_KillTemporaryTensor(Interpreter interpreter, Tensor tensor);

/* @NOTE: manipulate with NArray, the lowest classure of tensor, i don't
 * gurantee these function cause memory leak or bad access
 */
Tensor nNet_ResetTensor(Tensor tensor);
void   nNet_TryDropingArray(Tensor tensor, Array array);

Array  nNet_TrimArrayData(Tensor tensor);
Array  nNet_GetArrayData(Tensor tensor);
Tensor nNet_SetArrayData(Tensor tensor, Array array);

Item  nNet_GetArrayItem(Array array, long index, int type);
Array nNet_SetArrayItem(Array array, Item item, long index, int type);

Array nNet_DuplicateArray(Array array, long size, int type);
Item  nNet_DupplicatItem(Item item, int type);
#if __cplusplus
}
#endif

#if __cplusplus
namespace nn{
namespace tensor{
template<typename Type>
Type fetch(Array array, int index, int type){
  Type result;

  if (!nNet_ConvertNumT(nNet_GetArrayItem(array, index, type), type,
                       &result, nn::utils::type(typeid(Type))))
    throw BadAccess.reason("nNet_ConvertNumT()");
  return result;
}
} // namespace tensor
} // namespace nn
#endif
#endif  // LIBNN_INTEPRETER_H_
