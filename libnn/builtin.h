#if !defined(LIBNN_OP_MACROS_H_) && __cplusplus
#define LIBNN_OP_MACROS_H_
#include "interpreter.h"
#include "utils.h"

DEFINE_OP(macro, Placeholder);
DEFINE_OP(macro, Variable);
DEFINE_OP(macro, Identity);
DEFINE_OP(macro, Const);

namespace nn{
namespace internal{
inline void doRegisterBuiltinOp(){
  MACRO(Placeholder);
  MACRO(Identity);
  MACRO(Const);
}
} // namespace internal
} // namespace nn
#endif  // LIBNN_OP_MACROS_H_