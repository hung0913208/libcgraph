#ifndef LIBNN_INTERPRETER_H_
#define LIBNN_INTERPRETER_H_
#include "resolvers.h"
#include "tensor.h"
#include "type.h"

#ifndef LIBBASE_LOGCAT_HPP_
enum Code{
  ENoError = 0,
  EKeepContinue = 1,
  ENoSupport = 2,
  EBadLogic = 3,
  EBadAccess = 4,
  EOutOfRange = 5,
  ENotFound = 6,
  EDrainMem = 7,
  EWatchErrno = 8,
  EInterrupted = 9,
  EDoNothing = 10,
  EDoAgain = 11
};
#endif

#define GENERIC_PLATFORM      0
#define COMMON_GPU_PLATFORM   1
#define ACCELERATE_PLATFORM   1
#define SPECIFIC_GPU_PLATFORM 2

typedef unsigned long Thread;
typedef void* Pointer;

#if __cplusplus
extern "C"{
#endif
/* @NOTE: definition of methods of libnn's structure */
typedef struct Template{
  void* _priv;

  Device (*create)(Device pattern, Interpreter interpreter);
  Device (*release)(Device config);

  bool     (*inspect)(Device device);
  bool     (*install)(Device device, CString name, Operator op);
  bool     (*scan)(Device device, Operator op);
  Operator (*find)(Device device, CString name);
} Template;

typedef struct Event{
  void* _priv;

  Operator (*prepare)(Operator op, Interpreter interpreter, CString model);
  Operator (*release)(Operator op, Interpreter interpreter);
} Event;

typedef struct Registration{
  Event event;

  /* @NOTE: CALCULATING stage's APIs */
  bool (*eval)(Operator op, Interpreter interpreter);
  bool (*derive)(Operator op, Interpreter interpreter);

  /* @NOTE: COMMITING stage's APIs */
  bool (*inputnames)(Operator op, CString* inputs, int size);
  bool (*link)(Operator op, Operator* nodes, int size);
} Registration;

typedef struct AnyDef{
  void* value;
  int   type, size;

  void (*release)(struct AnyDef*);
} CAny;

typedef struct PairDef{
  CString name;
  CAny    value;

  void (*release)(struct PairDef*);
} CPair;

typedef CAny (*Convert)(CAny input, int output_type);

struct OperatorDef{
  Device device;
  void  *priv;
  char  *name;
  bool   type;

  Event _event;

  /* @setting */
  bool (*naming)(Operator op, CString name);
  bool (*inputnames)(Operator op, CString* inputs,  int size);
  int  (*setattr)(Operator op, CPair* mapping, int size, Convert convert);

 /* @config */
  bool (*link)(Operator op, Operator* nodes, int size);
  bool (*commit)(Operator op, Interpreter interpreter);

 /* @comunicate */
  bool (*inputs)(Operator op, Tensor** inputs, int* size);
  bool (*outputs)(Operator op, Tensor** outputs, int* size);

 /* @calculate */
  bool (*eval)(Operator op, Interpreter interpreter);
  bool (*derive)(Operator op, Interpreter interpreter);
};

struct SystemDef{
  int *opnum, level;

  Template _template;
  bool    *_enable;
  void    *_priv;

  Tensor (*tensor)(Interpreter interpreter, Tensor tensor);
  void   (*detach)(Interpreter interpreter, Device device);
};

struct CInterpreter{
  Pointer  priv;
  bool     manual;
  Thread   master;
  Device   sysdef;
};
#if __cplusplus
}
#endif

#if __cplusplus
extern "C"{
#endif
/* @NOTE: C-API for loading and unloading everything for developing nNet. The 
 * nNet_LoadEverything should be called first before doing anything for loading
 * builtin kernels and device devices and the nNet_UnloadEverything must be 
 * called on lastest after everything would be resolved. It uses for releasing 
 * everything before closing application to prevent memory leak.
 */
typedef struct CGates{
  Interpreter interpreter;
  CString* inputs;
  Tensor*  tensors;
  int      size;
} CGates;

typedef void (*CYield)(CGates outputs);

int nNet_LoadEverything();
int nNet_UnloadEverything();

/* @NOTE: C-API to configure CGates */
CGates nNet_CreateCGates(Interpreter interpreter, int how_many_gate);
bool   nNet_RemoveCGates(CGates* cgates);

bool   nNet_AssignTensorToCGates(CGates* cgates, CString name, Tensor tensor, 
                                 int index);

/* @NOTE: C-API to install, remove and manipulate with interpreter */
Interpreter nNet_CreateInterpreter(CString model);
Interpreter nNet_CreateInterpreterW(CString model, bool force, bool lazyload);

Interpreter nNet_CreateInterpreterFromFile(CString path);
Interpreter nNet_CreateInterpreterFromFileW(CString path, bool force, 
                                            bool lazyload);

Interpreter nNet_RemoveInterpreter(Interpreter interpreter);

/* @NOTE: C-API to help iteract with Interpreter's collector */
Tensor nNet_GetResultAtIndex(Interpreter interpreter, CString label, int index);

/* @NOTE: C-API to help start and stop an interpreter */
bool nNet_StartInterpreter(Interpreter interpreter);
bool nNet_StopInterpreter(Interpreter interpreter);

/* @NOTE: C-API for running and controling interpreter's graph */
int nNet_ParseToInterpreter(Interpreter interpreter, CString model);
int nNet_UseResolver(Interpreter interpreter, int resolver);
int nNet_InvokeGraph(Interpreter interpreter, CGates* inputs);
int nNet_ClearGraph(Interpreter interpreter);

#if DEV
int nNet_HaltGraph(Interpreter interpreter);
int nNet_InsertNodeToGraph(Interpreter interpreter, 
                           CString opname, CString name,
                           bool is_input_gate);
int nNet_SeekGraph(Interpreter interpreter, int offset, int whence,
                   bool is_on_vertical_align);
int nNet_TellGraph(Interpreter interpreter, Operator* result);

/* @NOTE: C-API for interactive with operator */
int nNet_SetInputOfOperator(Operator op, int index, Tensor tensor);
int nNet_GetOutputOfOperator(Operator op, int index, Tensor* result);

int nNet_SetOpAttribute(Operator op, CString name, int type, void* value);
int nNet_GetOpAttribute(Operator op, CString name, void* result);
#endif

/* @NOTE: C-API for install and manipulate with a device and operator */
bool nNet_InstallDevice(CString name, Device device);
bool nNet_InstallDeviceW(CString name, Device device, Interpreter interpreter);
bool nNet_InstallOperatorToInterpreter(CString name, Operator op, 
                                       Interpreter interpreter);
bool nNet_InstallOperatorToDevice(CString name, Operator op, Device device);

/* @NOTE: C-API to check system status and libnn'object status */
bool nNet_IsTemplateDevice(Device device);
bool nNet_IsTemplateOperator(Operator op);
bool nNet_IsTemplateOperatorW(Interpreter interpreter, Operator op);
int  nNet_CheckStatus();

/* @NOTE: C-API to do some thing with template operators */
bool   nNet_PackOperator(Operator op, Registration reg, bool macro);
Device nNet_GetDeviceOfOperator(Interpreter interpreter, Operator op);

/* @NOTE: C-API to removoe device in serveral cases */
Device nNet_RemoveDevice(Device device);
Device nNet_RemoveDeviceW(Interpreter interpreter, Device device);

/* @NOTE: C-API to remove operators in serveral cases */
Operator nNet_RemoveOperator(Operator op);
Operator nNet_RemoveOperatorW(Interpreter inter, Operator op);
Operator nNet_RemoveOperatorC1(Interpreter inter, CString device, Operator op);
Operator nNet_RemoveOperatorC2(Interpreter inter, CString device, CString op);
#if __cplusplus
}
#endif

#if __cplusplus
int nNet_EvalGraph(Interpreter interpreter, CGates* inputs,
                   std::function<void(CGates)> yield);
int nNet_FlushGraph(Interpreter interpreter, std::function<void(CGates)> yield);
int nNet_FlushGraph(Interpreter interpreter, std::size_t index,
                    std::function<void(std::vector<Tensor>)> yield);
int nNet_RunGraph(Interpreter interpreter, CGates* inputs,
                  std::function<void(CGates)> yield);
#else
int nNet_EvalGraph(Interpreter, CGates* inputs, CYield yield);
int nNet_FlushGraph(Interpreter interpreter, CYield yield);
int nNet_RunGraph(Interpreter interpreter, CGates* inputs, CYield yield);
#endif
#endif  // LIBNN_INTERPRETER_H_
