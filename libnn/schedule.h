#ifndef LIBNN_SCHEDULE_HPP_
#define LIBNN_SCHEDULE_HPP_

#if __cplusplus
#ifdef LIBCGRAPH_GIT_REFSPEC
#include <libbase/all.hpp>
#else
#include "logcat.hpp"
#endif

#include <stdint.h>

#include "interpreter.h"
#include "tensor.h"
#include "type.h"

namespace nn{
class InterpreterW;

namespace interpreter{
/* @define: class Queue is a thread-safe queue which is used to deloy task from
 * providers and return result to them */
class Queue{
 private:
  using Gates   = std::vector<std::pair<std::string, Tensor>>;
  using Yield   = std::function<void(Gates)>;
  using Task    = std::tuple<int, Gates, Yield>;
  using PLock   = pthread_mutex_t*;
  using Channel = pthread_mutex_t;

 public:
  Queue(Queue&) = delete;
  void operator=(Queue&) = delete;

 public:
  Queue();
  ~Queue();

 public:
  base::Error master(PLock lock, std::size_t peers);
  base::Error connect(Queue& another, std::size_t index);
  base::Error flush(PLock lock = nullptr);
  base::Error reset();

  base::Error exit();
  base::Error disconnect();

  bool retrigger(bool force_unlock_global);
  bool finished();
  bool reseted();
  bool flushing();
  bool registed();
  bool exited();
  bool single();
  bool connected();
  int  backlog(std::size_t index = std::string::npos);

  PLock channel(std::size_t index);
  PLock global();

  base::Error pin(Gates& value, Yield& yield, int index = 0);
  base::Error get(Gates& value, Yield& yield);
  base::Error fin(Gates& value);

 public:
  Yield yield;

 private:
  Queue*  _another;
  Thread  _provider, *_consumer;
  PLock   _outside, _inside, _global;
  PLock  *_channels;
  int     _index, _count, *_backlogs;
  bool    _flushing, _reseted, _role, _exit;

  std::vector<Task>* _tasks;
};

/* @define: class Lock is a enhanced mutex, and it's used to keep balancing 
 * between operators when switching from a device to another device */
class Lock{
 private:
  using Operators = std::vector<Operator>;

 public:
  explicit Lock(InterpreterW& interpreter);

 public:
  void wait(Operator op);
  void finish(Operator op);

 private:
  InterpreterW *_self;
  Operators     _operators;
};

/* @define: abstract class which's used to define a connection between 2 thread
 * Each thread needs to be connected to another, but the connection is single 
 * connection and the couple can communicate with another by using Queue
 */
class Couple: public std::enable_shared_from_this<Couple>{
 public:
  using Gates = std::vector<std::pair<std::string, Tensor>>;
  enum Status{ 
    Gray  = 0,
    Wait  = 1,
    Green = 2,
    Busy  = 3 
  };
 
 public:
  Couple(Couple&) = delete;

 public:
  /* @NOTE: when a node is root, it connects itself and support another nodes 
   * like a storage */
  explicit Couple(Thread remote);
  virtual ~Couple(){}

 public:
  inline Thread self(){ return _self; }
  inline Thread remote(){ return _remote; }
  inline Status status(){ return _status; }

 protected:
  virtual bool invoke(Gates inputs, std::function<void(Gates)> yield) = 0;

  std::shared_ptr<Queue> _queue;
  Thread _remote, _self;
  Status _status;
};
} // namespace interpreter

class Collector{
 public:
  using Result  = std::vector<std::pair<std::string, Tensor>>;
  using PResult = std::shared_ptr<Result>;

 public:
  Collector(Collector&) = delete;
  Collector();

 public:
  base::Error push_back(Result result);
  std::size_t size();
  void        clear();

  base::Error set(std::size_t index, Result result);
  PResult     get(std::size_t index);

 public:
  base::Property<std::function<void(Result)>> yield;
  base::Property<pthread_mutex_t*> lock;
  base::Property<Interpreter> interpreter;

 private:
  PResult clone(Result result);

 private:
  std::function<void(Result)> _yield;
  std::vector<PResult>        _results;

  pthread_mutex_t* _lock;
  Interpreter      _interpreter;
  Thread           _creator;
};
} // namespace nn
#endif
#endif  // LIBNN_SCHEDULE_HPP_
