#include "interpreter.h"
#include "internal.h"
#include "utils.h"

#define BACKLOG 0
#define PENDING _index

namespace nn{
namespace internal{
void wait(pthread_mutex_t& lock, std::function<bool()> before = nullptr, 
                                 std::function<void()> after = nullptr);
void safe(pthread_mutex_t* lock, std::function<void()>&& callback);
bool isLocked(pthread_mutex_t* lock);
} // namespace internal

namespace interpreter{
using Gates = std::vector<std::pair<std::string, Tensor>>;
using Yield = std::function<void(Gates)>;

Queue::Queue():
   _another{nullptr},
   _provider{(Thread)pthread_self()},
   _consumer{nullptr},
   _outside{nullptr},
   _inside{nullptr},
   _global{nullptr},
   _channels{nullptr},
   _index{0}, _count{0},
   _flushing{false}, _reseted{true}, _role{true}, _exit{false},
   _tasks{nullptr}{}

Queue::~Queue(){
  /* @NOTE: flush must be called to make sure everything is done before teardown
   */

  if (_tasks && (_tasks[BACKLOG].size() || _tasks[PENDING].size())){
    internal::safe(_inside, [&](){
      if (_another){
        _another->_flushing = (_flushing = true);
      }
    });
  }

  if (_another){
    if (!_inside) return;

    internal::safe(_inside, [&](){
      if ((Thread)pthread_self() == _provider){
        if (_role){
          if (_tasks) delete [] _tasks;
          _tasks  = nullptr;

          for (auto i = 0; i < _count - 1; ++i){
            pthread_mutex_destroy(_channels[i]);
            delete _channels[i];
          }

          if (_channels) delete [] _channels;
          _channels = nullptr;

          if (_backlogs) delete [] _backlogs;
          _backlogs = nullptr;
        }
      }

      if (_another && _another->_tasks){
        _another->_another = nullptr;
      }
    });

    if ((Thread)pthread_self() == _provider){
      if (_role){
        if (_inside) delete _inside;
        _inside = nullptr;
      }
    }
  } else {
    if ((Thread)pthread_self() == _provider && _role){
      if (_tasks) delete [] _tasks;
      if (_inside) delete _inside;
    }

    _tasks  = nullptr;
    _inside = nullptr;
  }

  if (_consumer){
    if (_role){
      delete [] _consumer;
    } else
      delete _consumer;
  }
}

base::Error Queue::master(pthread_mutex_t* outside, std::size_t peers){
  if (single() && _count == 0){
    pthread_mutexattr_t attr;

    pthread_mutexattr_init(&attr);
    pthread_mutexattr_settype(&attr, PTHREAD_MUTEX_RECURSIVE);

    _tasks    = new std::vector<Queue::Task>[peers + 1];
    _consumer = new Thread[peers];
    _channels = new PLock[peers];
    _backlogs = new int[peers];
    _inside   = _inside? _inside: new pthread_mutex_t; 
    _global   = _global? _global: new pthread_mutex_t;
    _outside  = outside;
    _count    = peers + 1;

    for (std::size_t i = 0; i < peers; ++i){
      _channels[i] = new pthread_mutex_t;

      pthread_mutex_init(_channels[i], nullptr);
      _backlogs[i] = 0;
    }

    if (pthread_mutex_init(_inside, nullptr) != 0)
      return BadAccess.reason("mutex init failed");
  
    if (pthread_mutex_init(_global, nullptr) != 0)
      return BadAccess.reason("mutex init failed");

    memset(_consumer, 0, peers*sizeof(Thread));
    return NoError;
  } else
    return BadLogic.reason("queue has been init with an exact master");
}

base::Error Queue::flush(pthread_mutex_t* lock){
  auto error = NoError;

  internal::safe(_inside, [&](){
    if (_tasks){
      auto keep  = false;

      for (auto i = 0; i < _count; ++i){
        if (_tasks[i].size()){
          keep = true;
        }

        if (i > 0 && internal::isLocked(_channels[i - 1]))
          pthread_mutex_unlock(_channels[i - 1]);
      }

      if (keep){
        if ((Thread)pthread_self() == _provider && lock){
          if (lock != _outside){
            warn(NoError << "queue doesn\'t gurantee"
                         << "if you provide differrent lock during flushing");
            pthread_mutex_unlock(_outside);
          }

          _outside  = lock;
          _flushing = true;
        }

        pthread_mutex_trylock(_outside);
        pthread_mutex_trylock(_global);

       #if DEBUG_MUTIL_THREAD
        printf("do flushing\n");
        fflush(stdout);
       #endif
      } else {
        error = DoNothing;

       #if DEBUG_MUTIL_THREAD
        printf("can't do flushing\n");
        fflush(stdout);
       #endif
      }
    } else {
      if (!_tasks)
        error = DoNothing.reason("queue is running on single mode");
      else
        error = warn(DoNothing.reason("queue is empty at this time"));
    }
  });

  return error;
}

base::Error Queue::reset(){
  auto error = NoError;

  internal::safe(_inside, [&](){
    if (_tasks && (_tasks[BACKLOG].size()))
      _tasks[BACKLOG].clear();
    else if (_reseted){
      error = info(DoNothing).reason("queue has been reseted");

      for (auto i = 1; i < _another->_count; ++i){
        if (_tasks[i].size())
          return;
      }
    }
    _reseted = true;
  });

  return error;
}

base::Error Queue::exit(){
  if ((Thread)pthread_self() != _provider || !_role)
    return DoNothing;
  
  internal::safe(_inside, [&]{ 
    _exit = true; 

    for (auto i = 0; i < _count - 1; ++i){
      if (internal::isLocked(_channels[i])){
        pthread_mutex_unlock(_channels[i]);
      }
    }

    if (internal::isLocked(_global)){
      pthread_mutex_unlock(_global);
    }
  });
  return NoError;
}

bool Queue::retrigger(bool force_unlock_global){
  if ((Thread)pthread_self() == _provider && _role){
    /* @NOTE: unlock channels since, some of them will be locked at this time
     * the reason is simple, if consumers run too fast and it came to state
     * 'flushing with _outside is unlocked', they will be locked by their own
     * lock instead of using global lock. So, retrigger will turn them on again
     * and redirect them to global lock.
     */

    for (auto i = 0; i < _count - 1; ++i){
      pthread_mutex_unlock(_channels[i]);
    }

    /* @NOTE: this flag will turn on if there is a posible fluctuated state 
     * happen. When this state is started, we will see consumer status is Wait
     * but its lock is still on unlocking. It usually happen on busy systems.
     */
    if (force_unlock_global)
      pthread_mutex_unlock(_global);
    return true;
  } else {
    return false;
  }
}

base::Error Queue::disconnect(){
  base::Error result;

  if (!connected())
    return DoNothing << "double disconnect with the same consumer";

  internal::safe(_inside, [&](){
    if ((Thread)pthread_self() != _provider || !_role){
      _tasks[PENDING].clear();

      if (_another && long(_another->_consumer[PENDING - 1]) >= 0){
        _another->_count--;
        _another->_consumer[PENDING - 1] = long(-1);

        /* @NOTE: switch status of queue from connected to single */
        if (_another->_count <= 1)
          _another->_another = nullptr;

        _another = nullptr;
        result = info(NoError << "disconnect from consumer");
      } else {
        result = DoNothing << "double disconnect with the same consumer";
      }
    } else {
      if (_consumer) 
        delete [] _consumer;

      if (_tasks){
        for (auto i = 0; i < _count; ++i)
          _tasks[i].clear();

        delete [] _tasks;
      }

      _consumer = nullptr;
      _tasks    = nullptr;
      _count    = 0;
      _reseted  = false;
      _role     = true;
      result = info(NoError << "disconnect from provider");
    }
  });

  return result;
}

base::Error Queue::connect(Queue& another, std::size_t index){
  if (another._provider == (Thread)pthread_self())
    return BadLogic.reason("can\'t connect itself");
  else if (index >= cast_(index, another._count)){
    return OutOfRange << "index must be smaller than" 
                      << std::to_string(another._count);
  } else if (connected())
    return DoNothing;

  _another  = &another;
  _provider = another._provider;
  _consumer = new Thread{(Thread)pthread_self()};
  _channels = another._channels;
  _backlogs = another._backlogs;
  _outside  = another._outside;
  _inside   = another._inside;
  _global   = another._global;
  _outside  = another._outside;
  _tasks    = another._tasks;

  _role  = false;
  _count = another._count;
  _index = index;

  internal::safe(_inside, [&](){
    another._consumer[index - 1] = *_consumer;
    another._another = this;
  });
  return NoError;
}

bool Queue::finished(){
  auto result = false;

  if (!_tasks || single())
    return true;
  else if ((Thread)pthread_self() == _provider){
    internal::safe(_inside, [&](){
      if (!_tasks[BACKLOG].size()){
        for (auto i = 1; i < _count; ++i){
          if (_tasks[i].size()) return;
        }

        result = _flushing && _exit;
      }
    });
  } else if ((Thread)pthread_self() == *_consumer){
    internal::safe(_inside, [&](){
      if (!_tasks[BACKLOG].size()){
        for (auto i = 1; i < _count; ++i){
          if (_tasks[i].size()) return;
        }

        result = _another->_flushing && _another->_exit;
      }
    });
  } else
    throw BadAccess;
  return result;
}

bool Queue::reseted(){
  bool result = true, locked = false;

  if (!_inside)
    return true;

  if (!internal::isLocked(_inside)){
    pthread_mutex_lock(_inside);
    locked = true;
  }

  result = _reseted;
  if (locked) pthread_mutex_unlock(_inside);
  return result;
}

bool Queue::flushing(){
  bool result = false, locked = false;

  if (!_inside || !_tasks || single())
    return false;

  if (!internal::isLocked(_inside)){
    pthread_mutex_lock(_inside);
    locked = true;
  }

  result = _flushing;
  if (locked) pthread_mutex_unlock(_inside);
  return result;
}

bool Queue::registed(){ 
  bool result = false, locked = false;

  if (!_inside)
    return false;

  if (!internal::isLocked(_inside)){
    pthread_mutex_lock(_inside);
    locked = true;
  }

  result = _count != 0;
  if (locked) pthread_mutex_unlock(_inside);
  return result;
}

bool Queue::exited(){ 
  bool result = true, locked = false;

  if (!_inside)
    return true;

  if (!internal::isLocked(_inside)){
    pthread_mutex_lock(_inside);
    locked = true;
  }

  result =_role? _exit: _another->_exit;
  if (locked) pthread_mutex_unlock(_inside);
  return result;
}

bool Queue::single(){
  bool result = true, locked = false;

  if (!_inside)
    return true;

  if (!internal::isLocked(_inside)){
    pthread_mutex_lock(_inside);
    locked = true;
  }
  result =_another == nullptr;
  if (locked) pthread_mutex_unlock(_inside);
  return result;
}

bool Queue::connected(){ 
  bool result = false, locked = false;

  if (!_inside) return false;
  if (!internal::isLocked(_inside)){
    pthread_mutex_lock(_inside);
    locked = true;
  }

  result = _another && _inside != nullptr; 
  if (locked) pthread_mutex_unlock(_inside);
  return result;
}

int Queue::backlog(std::size_t index){
  bool locked = false;
  int  result = 0;

  if (!_tasks || single())
    return 0;

  if (!internal::isLocked(_inside)){
    pthread_mutex_lock(_inside);
    locked = true;
  }

  if (index == std::string::npos){
    for (auto i = 0; i < _count; ++i){
      result += _tasks[i].size();
    }
  } else
    result = _backlogs[index];

  if (locked) pthread_mutex_unlock(_inside);
  return result;
}

pthread_mutex_t* Queue::channel(std::size_t index){
  PLock result = nullptr;

  internal::safe(_inside, [&](){

    if ((Thread)pthread_self() != _provider || !_role){
     #if DEBUG_MUTIL_THREAD
      if (_flushing){
        printf("global\n");
        fflush(stdout);
      } else {
        printf("local\n");
        fflush(stdout);
      }
     #endif

      result = _flushing? _global: _channels[index - 1];
    } else 
      result = _channels[index - 1];
  });

  return result;
}

pthread_mutex_t* Queue::global(){
  return _global;
}

base::Error Queue::fin(Gates& value){
  if ((Thread)pthread_self() != *_consumer){
    return BadAccess << "Queue::fin() must run on thread" 
                     << std::to_string(*_consumer);
  } else {
    auto error = NoError;

    internal::safe(_inside, [&](){
      if (!_tasks[PENDING].size())
        error = OutOfRange;
      else {
        auto it = _tasks[PENDING].begin();

        for (; it != _tasks[PENDING].end(); ++it){
          if (std::get<1>((*it)) == value){
            break;
          }
        }

        if (it != _tasks[PENDING].end())
          _tasks[PENDING].erase(it);
        else
          error = NotFound;
      }

      if (_tasks[PENDING].size() == 0 && !_flushing){
        if (_backlogs[_index - 1] == 0)
          pthread_mutex_unlock(_channels[_index - 1]);
      }

      if (!error && !_tasks[BACKLOG].size()){
        for (auto i = 1; i < _count; ++i){
          if (_tasks[i].size()) return;
        }

        if (_outside && _flushing){
          pthread_mutex_unlock(_outside);

         #if DEBUG_MUTIL_THREAD
          printf("unlock _outside\n");
          fflush(stdout);
         #endif

          _flushing = false;
          _reseted  = true;
        }
      }
    });

    return error;
  }
}

base::Error Queue::get(Gates& value, Yield& yield){
  if (!_consumer){
    return BadAccess << "Queue is on single mode"
                     << "can\'t get data from afar";
  } else if ((Thread) pthread_self() != *_consumer){
    return BadAccess << "Queue::get() must run on thread" 
                     << std::to_string(*_consumer);
  } else if (_tasks){
    auto error = NoError;

    internal::safe(_inside, [&](){
      if (_tasks[BACKLOG].size() > 0){
        int idx;

        for (idx = 0; idx < cast_(idx, _tasks[BACKLOG].size()); ++idx){
          if (std::get<0>(_tasks[BACKLOG][idx]) != _index - 1)
            continue;

          value   = std::get<1>(_tasks[BACKLOG][idx]);
          yield   = std::get<2>(_tasks[BACKLOG][idx]);
          break;
        }

        if (_index > 0){
          _backlogs[_index - 1] -= 1;

          /* @NOTE: channel is empty now, try lock it to optimize performance */
          if (_backlogs[_index - 1] == 0)
            pthread_mutex_lock(_channels[_index - 1]);
        }

        _tasks[BACKLOG].erase(_tasks[BACKLOG].begin() + idx);
        _tasks[PENDING].push_back(std::make_tuple(_index, value, yield));
      } else if (!_another->_flushing){
        if (!exited()){
          error = warn((OutOfRange << "BACKLOG is empty now"));
          
          for (auto i = 0; i < _count - 1; ++i){
            /* @NOTE: channel is empty now, try lock it to optimize performance */

            if (_backlogs[i] == 0)
              pthread_mutex_lock(_channels[i]);
          }
        } else
          error = info((DoNothing << "Queue is on closing now"));
      } else {
        /* @NOTE: by default, when queue jump to this state, it must release 
         * _outside lock since it must be locked from provider 
         */
        if (!internal::isLocked(_outside)){
          error = warn(DoNothing << "BACKLOG is on flushing but it seem it didn't"
                                    << "lock as expected");
        } else {
          error = warn((DoNothing << "BACKLOG is on flushing"));
          pthread_mutex_unlock(_outside);
        }

        for (auto i = 0; i < _count - 1; ++i){
            /* @NOTE: channel is empty now, try lock it to optimize performance */
          if (_backlogs[i] == 0)
            pthread_mutex_lock(_channels[i]);
        }
      }
    });
    return error;
  } else
    return DoNothing;
}

base::Error Queue::pin(Gates& value, Yield& yield, int index){
  if (_tasks){
    auto error = NoError;

    internal::safe(_inside, [&](){
      /* @NOTE: update backlog of channel[index] and unlock if it needs */

      if ((Thread)pthread_self() != _provider){
        _tasks[BACKLOG].push_back(std::make_tuple(index, value, yield));
      } else {
        _tasks[BACKLOG].push_back(std::make_tuple(0, value, yield));

        if (internal::isLocked(_outside))
          pthread_mutex_unlock(_outside);
        index = 0;
      }
      
      if (internal::isLocked(_channels[index]))
        pthread_mutex_unlock(_channels[index]);
      _backlogs[_index] += 1;

      if (index == 0){
        _flushing = false;
        _reseted = false;
      }
    });
    return error;
  } else
    return DoNothing;
}
} // namespace interpreter
} // namespace nn
