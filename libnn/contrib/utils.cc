#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

#include <iostream>
#include <sstream>
#include <cstdlib>

using namespace base;
#define EPSILON 0.0001

extern "C"{
void DontRemoveTensorData(Tensor tensor, Interpreter UNUSED(interpreter));
}

namespace nn{
using Pointer = void*;

#define SHAPE_T    0
#define ORDINATE_T 1
#define TENSOR_T   2

namespace utils{
int diff(Tensor left, Tensor right, double epsilon){
  if (epsilon <= 0) epsilon = EPSILON;
  if (!left || !right) return StructDiff;
  else {
    auto diff_ = left->diff;

    /* @NOTE: check if diff callback is supported by 2 tensors */
    if (left->diff != right->diff) {
      if (left->init != right->init || left->get != right->get ||
          left->set != right->set)
        return StructDiff;
      else
        diff_ = nullptr;
    }

    /* @NOTE: compare 2 tensors with distinct conditions */
    if (left->reshape != right->reshape) {
      return StructDiff;
    } else if (left->type != right->type) {
      return TypeDiff;
    } else if (left->_size != right->_size) {
      return SizeDiff;
    } else if (left->ndims != right->ndims) {
      return DimDiff;
    } else {
      /* @NOTE: diff dimemntion more detail */

      for (auto i = 0; i < left->ndims; ++i) {
        auto ldim = left->dims[i];
        auto rdim = right->dims[i];

        if (left->_begins && left->_ends) {
          auto begin = left->_begins[i] > 0 ? left->_begins[i] : 0;
          auto end = left->_ends[i] > 0 ? left->_ends[i] : ldim;

          ldim = end - begin;
        }
        if (right->_begins && right->_ends) {
          auto begin = right->_begins[i] > 0 ? right->_begins[i] : 0;
          auto end = right->_ends[i] > 0 ? right->_ends[i] : rdim;

          rdim = end - begin;
        }

        if (ldim != rdim) return DimDiff;
      }
    }

    if (diff_)
      return diff_(left, right) ? DataDiff : NoDiff;
    else {
      auto left_array = nNet_GetArrayData(left);
      auto right_array = nNet_GetArrayData(right);
      auto result = NoDiff;

      Real64 left_value, right_value;
      for (auto i = 0; i < nNet_GetTensorSize(left); ++i){
        auto left_item  = nNet_GetArrayItem(left_array, 
                                            nNet_CalcIndex(left, i),
                                            left->type);
        auto right_item = nNet_GetArrayItem(right_array,
                                            nNet_CalcIndex(right, i),
                                            right->type);

        nNet_ConvertNumT(left_item, left->type, &left_value, 
                         nn::utils::type(typeid(double)));
        nNet_ConvertNumT(right_item, right->type, &right_value, 
                         nn::utils::type(typeid(double)));

        if (!aequal(left_value, right_value, epsilon)){
          info((NoError << "left != right at index" << std::to_string(i)));
          result = DataDiff;
        }
      }

      nNet_TryDropingArray(left, left_array);
      nNet_TryDropingArray(right, right_array);

      return result;
    }
  }
}

unsigned long size(Tensor tensor){
  unsigned long result = 1;

  for (auto i = 0; i < tensor->ndims; ++i){
    auto begin = tensor->_begins? tensor->_begins[i]: 0;
    auto end   = tensor->_ends? tensor->_ends[i]: tensor->dims[i];

    if (end > begin) result *= end - begin;
  }
  return result;
}

void alloc(Shape shape){
  auto pointer = reinterpret_cast<Pointer>(shape);

  if (pointer && outdate(shape))
    InterpreterW::gcollector().assignPointer(pointer, SHAPE_T);
}

void alloc(Ordinate ordinate){
  auto pointer = reinterpret_cast<Pointer>(ordinate);

  if (pointer && outdate(ordinate))
    InterpreterW::gcollector().assignPointer(pointer, ORDINATE_T);
}

void alloc(Tensor tensor){
  auto pointer = reinterpret_cast<Pointer>(tensor);

  if (pointer && outdate(tensor))
    InterpreterW::gcollector().assignPointer(pointer, TENSOR_T);
}

void free(Shape shape){
  auto pointer = reinterpret_cast<Pointer>(shape);

  if (pointer && !outdate(shape))
    InterpreterW::gcollector().removePointer(pointer);
}

void free(Ordinate ordinate){
  auto pointer = reinterpret_cast<Pointer>(ordinate);

  if (pointer && !outdate(ordinate))
    InterpreterW::gcollector().removePointer(pointer);
}

void free(Tensor tensor){
  auto pointer = reinterpret_cast<Pointer>(tensor);

  if (pointer && !outdate(tensor))
    InterpreterW::gcollector().removePointer(pointer);
}

bool outdate(Shape shape){
  auto pointer = reinterpret_cast<Pointer>(shape);

  if (!pointer) return true;
  return !InterpreterW::gcollector().findPointer(pointer, SHAPE_T);
}

bool outdate(Tensor tensor){
  auto pointer = reinterpret_cast<Pointer>(tensor);

  if (!pointer) return true;
  return !InterpreterW::gcollector().findPointer(pointer, TENSOR_T);
}

bool outdate(Ordinate ordinate){
  auto pointer = reinterpret_cast<Pointer>(ordinate);

  if (!pointer) return true;
  return !InterpreterW::gcollector().findPointer(pointer, ORDINATE_T);
}

bool outdate(Interpreter interpreter, Shape shape){
  using namespace nn;

  if (interpreter && !interpreter->priv) return false;
  else try{
    auto interw  = interpreter?  reinterpret_cast<InterpreterW*>(interpreter->priv)
                              : &InterpreterW::instance();
    auto pointer = reinterpret_cast<Pointer>(shape);

    if (!pointer) return true;
    return !interw->findPointer(pointer, SHAPE_T);
  } catch(base::Error&){ return true; }
}

bool outdate(Interpreter interpreter, Tensor tensor){
  using namespace nn;

  if (interpreter && !interpreter->priv) return false;
  else try{
    auto interw  = interpreter?  reinterpret_cast<InterpreterW*>(interpreter->priv)
                              : &InterpreterW::instance();
    auto pointer = reinterpret_cast<Pointer>(tensor);

    if (!pointer) return true;
    return !interw->findPointer(pointer, TENSOR_T);
  } catch(base::Error&){ return true; }
}

bool outdate(Interpreter interpreter, Ordinate ordinate){
  using namespace nn;

  if (interpreter && !interpreter->priv) return false;
  else try{
    auto interw  = interpreter?  reinterpret_cast<InterpreterW*>(interpreter->priv)
                              : &InterpreterW::instance();
    auto pointer = reinterpret_cast<Pointer>(ordinate);

    if (!pointer) return true;
    return !interw->findPointer(pointer, ORDINATE_T);
  } catch(base::Error&){ return true; }
}

Operator clearFaulty(Interpreter interpreter, Operator op, base::Error error){
  if (interpreter){
    auto interpreterw = nNet_ConvertToInterpreterW(interpreter);

    if (interpreterw)
      interpreterw->error(rvalue(error));
    return nNet_RemoveOperatorW(interpreter, op);
  } else
    return op;
}

Tensor clearFaulty(Interpreter interpreter, Tensor tensor, Error error){
  interpreter = interpreter? interpreter: &(InterpreterW::instance().interface());

  if (interpreter){
    auto interpreterw = nNet_ConvertToInterpreterW(interpreter);

    if (interpreterw)
      interpreterw->error(rvalue(error));
    return nNet_RemoveTensorW(interpreter, tensor);
  } else
    return tensor;
}

Ordinate clearFaulty(Interpreter interpreter, Ordinate ordinate, Error error){  
  interpreter = interpreter? interpreter: &(InterpreterW::instance().interface());

  if (interpreter){
    auto interpreterw = nNet_ConvertToInterpreterW(interpreter);

    if (interpreterw)
      interpreterw->error(rvalue(error));
    return nNet_RemoveOrdinateW(interpreter, ordinate);
  } else 
    return ordinate;
}

Shape clearFaulty(Interpreter interpreter, Shape shape, Error error){
  interpreter = interpreter? interpreter: &(InterpreterW::instance().interface());

  if (interpreter){
    auto interpreterw = nNet_ConvertToInterpreterW(interpreter);

    if (interpreterw)
      interpreterw->error(rvalue(error));
    return nNet_RemoveTensorShapeW(interpreter, shape);
  } else 
    return shape;
}

int type(const std::type_info& info){
  if (info == typeid(int) || info == typeid(void)){
    /* @NOTE: specific case */
    return (sizeof(int) == sizeof(Int32))? Int32_t: Int64_t;
  }

  if (info == typeid(Real32))
    return Real32_t;

  if (info == typeid(Real64))
    return Real64_t;

  if (info == typeid(Int32))
    return Int32_t;

  if (info == typeid(Int64))
    return Int64_t;

  if (info == typeid(bool))
    return Bool_t;

  if (info == typeid(char*))
    return String_t;

  return Unknown;
}

std::string shape(Tensor tensor){
  auto result = std::string{"[ "};

  if (!tensor) return "null";
  for (auto i = tensor->ndims - 1; i >= 0; --i){
    result += std::to_string(tensor->dims[i]);

    if (i != 0)
      result += ", ";
  }

  return result + " ]";
}

std::string shape(std::vector<int> dims){
  auto result = std::string{"[ "};

  for (auto i = dims.size(); i > 0; --i){
    result += dims[i - 1] >= 0? std::to_string(dims[i - 1]): "?";

    if (i != 1)
      result += ", ";
  }

  return result + " ]";
}

std::string print(Ordinate ordinate){
  auto result = std::string{"[ "};

  if (!ordinate) return "null";
  for (auto i = ordinate->ndims - 1; i >= 0; --i){
    result += "(" + std::to_string(ordinate->begins[i]) + ", " 
                  + std::to_string(ordinate->ends[i]) + ")";
    if (i != 0) result += ", ";
  }

  return result + " ]";
}

std::string print(Shape shape){
  auto result = std::string{"[ "};

  if (!shape) return "null";
  for (auto i = shape->ndims - 1; i >= 0; --i){
    result += std::to_string(shape->dims[i]);

    if (i != 0)
      result += ", ";
  }

  return result + " ]";
}

std::string print(Item item, int type){
  if (!item) return "null";
  switch (type){
  case Bool_t:
    return *((bool*)item)? "T": "F";

  case UInt_t:
    return std::to_string(*((int*)item));

  case Int32_t:
    return std::to_string(*((Int32*)item));

  case Int64_t:
    return std::to_string(*((Int64*)item));

  case Real32_t:{
    std::stringstream stream;

    stream << std::setprecision(10) << /* std::scientific << */ *((Real32*)item);
    return stream.str();
  }

  case Real64_t:{
    std::stringstream stream;

    stream << std::setprecision(10) << /* std::scientific << */ *((Real64*)item);
    return stream.str();
  }

  case String_t:
  {
    char* value = *((char**)item);

    if (value)
      return std::string{value};
    else
      return "None";
  }

  case NumT::Unknown:
  default:
    return "?";

  }
}

std::string print(Array array, int begin, int end, int type, int first_column){
  auto result = std::string{};
  auto enter  = std::string{"\n"};
  auto length = first_column;

  if (!array) return "null";

  for (int i = 0; i < first_column; ++i)
    enter += ' ';

  for (int i = begin; i < end; ++i){
    auto flatten = print(nNet_GetArrayItem(array, i, type), type);

    if (1 + int(flatten.size()) + length >= 80 - first_column){
      result += enter;
      length  = first_column;
    }

    result += flatten;
    length += 1 + flatten.size();
    if (i + 1 < end) result += ", ";
  }

  return result;
}

std::string print(Tensor tensor){
  Array array{nullptr};
  base::Boundary bound{[](){},
                       [&](){ nNet_TryDropingArray(tensor, array); }};

  auto begin  = 0;
  auto result = std::string{};
  auto pos    = std::vector<int>{};

  if (nNet_GetTensorType(tensor) == NilT)
    return "";

  array = nNet_TrimArrayData(tensor);

  if (nNet_GetTensorType(tensor) == ScalarT)
    return print(array, nNet_GetTensorNumT(tensor));

  if (nNet_GetTensorType(tensor) == VectorT){
    return "[ " + print(array, 0, nNet_GetArraySizeT(tensor),
                        nNet_GetTensorNumT(tensor)) + " ]";
  }

  for (auto i = tensor->ndims; i > 0; --i){
    pos.push_back(0);
    result += "[";

    if (i > 1)
      continue;
    else {
      pos.pop_back();
    }

    while (pos.back() < tensor->dims[tensor->ndims - pos.size()]){
      /* @NOTE: print a row and update 'begin' */

      result += print(array, begin, begin + tensor->dims[0], 
                      nNet_GetTensorNumT(tensor), tensor->ndims);
      begin  += tensor->dims[0];

      /* @NOTE: increase counter until it reachs its limitation */
      pos.back()++;
      if (pos.back() < tensor->dims[tensor->ndims - pos.size()]){
        result += "],\n";

        /* @NOTE: add space to make presentation look beautiful */
        for (auto j = 0; j < tensor->ndims - 1; j++)
          result += " ";
        result += "[";
      } else
        result += "]";
    }

    if (tensor->ndims == 2){
      result += "]\n";
      return result;
    }

    /* @NOTE: group higher dimension when they finish at the same time */
    while (pos.back() == tensor->dims[tensor->ndims - pos.size()]){
      pos.pop_back();

      if (pos.size() > 0){
        /* @NOTE: prevent double free inside std::vector */
        pos.back()++;
      }

      i++;

      if (i == tensor->ndims){
        result += "]\n";
        return result;
      }

      if (pos.back() < tensor->dims[tensor->ndims - pos.size()]){
        /* @NOTE: stay back, need to recovery pos */
        result += "],\n\n";
        pos.push_back(0);

        for (auto j = 0; j < cast_(i, pos.size()); j++)
          result += " ";
      } else
        result += "]";
    }
  }

  return "";
}

std::string print(NumT type){
  switch(type){
  case NumT::Unknown:
    return "Unknown";

  case Bool_t:
    return "Bool_t";

  case UInt_t:
    return "UInt_t";

  case Int32_t:
    return "Int32_t";

  case Int64_t:
    return "Int64_t";

  case Real32_t:
    return "Real32_t";

  case Real64_t:
    return "Real64_t";

  case String_t:
    return "String_t";
  }
  throw NoSupport;
}

template<> Tensor copy<Tensor>(Interpreter d_inter, Tensor d_obj,
                               Interpreter s_inter, Tensor s_obj){
  if (outdate(s_inter, s_obj) || outdate(d_inter, d_obj))
    return nullptr;

  nNet_ClearTensorW(d_inter, d_obj);

  if (d_inter == s_inter){
    memcpy(d_obj, s_obj, sizeof(TensorDef));
    d_obj->release = DontRemoveTensorData;
  } else {
    auto shape = nNet_CreateTensorShapeC1(d_inter, 0);
    auto array = nNet_GetArrayData(s_obj);

    if (shape) return nullptr;
    else if (!nNet_GetTensorShape(s_obj, shape)){
      nNet_RemoveTensorShapeW(d_inter, shape);
      return nullptr;
    }

    if (nNet_GetTensorType(d_obj) != NilT)
      nNet_ClearTensorW(d_inter, d_obj);

    if (!nNet_SetTensorNumTW(d_inter, d_obj, nNet_GetTensorNumT(s_obj))){
      nNet_RemoveTensorShapeW(d_inter, shape);
      return nNet_RemoveTensorW(d_inter, d_obj);
    }

    if (!nNet_SetTensorShapeW(d_inter, d_obj, shape)){
      nNet_RemoveTensorShapeW(d_inter, shape);
      return nNet_RemoveTensorW(d_inter, d_obj);
    }

    if (!nNet_RemoveTensorShapeW(d_inter, shape))
      return nNet_RemoveTensorW(d_inter, d_obj);

    if (!nNet_SetArrayData(d_obj, array))
      return nNet_RemoveTensorW(d_inter, d_obj);
  }
  return d_obj;
}

template<> Shape copy<Shape>(Interpreter d_inter, Shape d_obj,
                             Interpreter s_inter, Shape s_obj){
  if (outdate(s_inter, s_obj) || outdate(d_inter, d_obj))
    return nullptr;

  if (d_inter == s_inter)
    memcpy(d_obj, s_obj, sizeof(ShapeDef));
  else {
    if (d_obj->dims) std::free(d_obj->dims);

    d_obj->dims  = (int*) calloc(s_obj->ndims, sizeof(int));
    d_obj->ndims = s_obj->ndims;
    d_obj->max   = s_obj->ndims;

    memcpy(d_obj->dims, s_obj->dims, sizeof(int)*s_obj->ndims);
  }
  return d_obj;
}

template<> Ordinate copy<Ordinate>(Interpreter d_inter, Ordinate d_obj,
                                   Interpreter s_inter, Ordinate s_obj){
  if (outdate(s_inter, s_obj) || outdate(d_inter, d_obj))
    return nullptr;

  if (d_inter == s_inter)
    memcpy(d_obj, s_obj, sizeof(ShapeDef));
  else {
    if (d_obj->begins) std::free(d_obj->begins);
    if (d_obj->ends) std::free(d_obj->ends);

    d_obj->begins = (int*) calloc(s_obj->ndims, sizeof(int));
    d_obj->ends   = (int*) calloc(s_obj->ndims, sizeof(int));

    memcpy(d_obj->begins, s_obj->begins, sizeof(int)*s_obj->ndims);
    memcpy(d_obj->ends, s_obj->ends, sizeof(int)*s_obj->ndims);

    d_obj->ndims = s_obj->ndims;
    d_obj->max   = s_obj->ndims;
  }
  return d_obj;
}

template<> base::Error assign<Int32>(Tensor tensor, std::vector<Int32>&& vector){
    if (nNet_GetArraySizeT(tensor) != squeeze(int, vector.size()))
    return BadLogic.reason(
      std::string("tensor size and array size doesn\'t match") + 
                  "("    + std::to_string(nNet_GetArraySizeT(tensor)) +
                  " != " + std::to_string(vector.size()) + ")");

  if (nNet_GetTensorNumT(tensor) != Int32_t)
    return DoNothing.reason("tensor type is incompatible with array");

  if (nNet_IsRefTensor(tensor)){
    for (auto i = 0; i < cast_(i, vector.size()); ++i){
      auto index = nNet_CalcIndex(tensor, i);

      if (!nNet_SetTensorItemS(tensor, index, &vector.at(i)))
        return BadAccess.reason(std::string("fail at index ") + 
                                std::to_string(i));
    }
  } else {
    Array array{nullptr};

    if (!(array = nNet_GetArrayData(tensor)))
      return DoNothing.reason("can\'t assign value to an empty tensor");
    else
      memcpy(array, vector.data(), vector.size()*nNet_SizeType(Int32_t));

    if (!nNet_SetArrayData(tensor, array))
      return BadAccess;
  }
  return NoError;
}

template<> base::Error assign<Int64>(Tensor tensor, std::vector<Int64>&& vector){
  if (nNet_GetArraySizeT(tensor) != squeeze(int, vector.size()))
    return BadLogic.reason(
      std::string("tensor size and array size doesn\'t match") + 
                  "("    + std::to_string(nNet_GetArraySizeT(tensor)) +
                  " != " + std::to_string(vector.size()) + ")");

  if (nNet_GetTensorNumT(tensor) != Int64_t)
    return DoNothing.reason("tensor type is incompatible with array");

  if (nNet_IsRefTensor(tensor)){
    for (auto i = 0; i < cast_(i, vector.size()); ++i){
      auto index = nNet_CalcIndex(tensor, i);

      if (!nNet_SetTensorItemS(tensor, index, &vector.at(i)))
        return BadAccess.reason(std::string("fail at index ") + 
                                std::to_string(i));
    }
  } else {
    Array array{nullptr};

    if (!(array = nNet_GetArrayData(tensor)))
      return DoNothing.reason("can\'t assign value to an empty tensor");
    else
      memcpy(array, vector.data(), vector.size()*nNet_SizeType(Int64_t));

    if (!nNet_SetArrayData(tensor, array))
      return BadAccess;
  }
  return NoError;
}

template<> base::Error assign<Real32>(Tensor tensor, std::vector<Real32>&& vector){
  if (nNet_GetArraySizeT(tensor) != squeeze(int, vector.size()))
    return BadLogic.reason(
      std::string("tensor size and array size doesn\'t match") + 
                  "("    + std::to_string(nNet_GetArraySizeT(tensor)) +
                  " != " + std::to_string(vector.size()) + ")");

  if (nNet_GetTensorNumT(tensor) != Real32_t)
    return DoNothing.reason("tensor type is incompatible with array");

  if (nNet_IsRefTensor(tensor)){
    for (auto i = 0; i < cast_(i, vector.size()); ++i){
      auto index = nNet_CalcIndex(tensor, i);

      if (!nNet_SetTensorItemS(tensor, index, &vector.at(i)))
        return BadAccess.reason(std::string("fail at index ") + 
                                std::to_string(i));
    }
  } else {
    Array array{nullptr};

    if (!(array = nNet_GetArrayData(tensor)))
      return DoNothing.reason("can\'t assign value to an empty tensor");
    else
      memcpy(array, vector.data(), vector.size()*nNet_SizeType(Real32_t));

    if (!nNet_SetArrayData(tensor, array))
      return BadAccess;
  }
  return NoError;
}

template<> base::Error assign<Real64>(Tensor tensor, std::vector<Real64>&& vector){
  if (nNet_GetArraySizeT(tensor) != squeeze(int, vector.size())){
    return BadLogic.reason(
      std::string("tensor size and array size doesn\'t match") + 
                  "("    + std::to_string(nNet_GetArraySizeT(tensor)) +
                  " != " + std::to_string(vector.size()) + ")");
  }

  if (nNet_GetTensorNumT(tensor) != Real64_t)
    return DoNothing.reason("tensor type is incompatible with array");

  if (nNet_IsRefTensor(tensor)){
    for (auto i = 0; i < cast_(i, vector.size()); ++i){
      auto index = nNet_CalcIndex(tensor, i);

      if (!nNet_SetTensorItemS(tensor, index, &vector.at(i)))
        return BadAccess.reason(std::string("fail at index ") + 
                                std::to_string(i));
    }
  } else {
    Array array{nullptr};

    if (!(array = nNet_GetArrayData(tensor)))
      return DoNothing.reason("can\'t assign value to an empty tensor");
    else
      memcpy(array, vector.data(), vector.size()*nNet_SizeType(Real32_t));

    if (!nNet_SetArrayData(tensor, array))
      return BadAccess;
  }

  return NoError;
}

template<> std::vector<Int32> values<Int32>(Tensor tensor){
  auto type = nNet_GetTensorNumT(tensor);

  if (type != Int32_t)
    throw BadLogic;

  try{
    auto array  = nNet_TrimArrayData(tensor);
    auto  size  = nNet_GetArraySizeT(tensor);
    auto result = std::vector<Int32>{};

    for (auto i = 0; i < size; ++i){
      result.push_back(*((Int32*)nNet_GetArrayItem(array, i, type)));
    }

    nNet_TryDropingArray(tensor, array);
    return result;
  } catch(base::Error& error){ throw base::Error{error}; }
}

template<> std::vector<Int64> values<Int64>(Tensor tensor){
  auto type = nNet_GetTensorNumT(tensor);

  if (type != Int64_t)
    throw BadLogic;

  try{
    auto array  = nNet_GetArrayData(tensor);
    auto  size  = nNet_GetArraySizeT(tensor);
    auto result = std::vector<Int64>{};

    for (auto i = 0; i < size; ++i){
      result.push_back(*((Int64*)nNet_GetArrayItem(array, i, type)));
    }

    nNet_TryDropingArray(tensor, array);
    return result;
  } catch(base::Error& error){ throw base::Error{error}; }
}

template<> std::vector<Real32> values<Real32>(Tensor tensor){
  auto type = nNet_GetTensorNumT(tensor);

  if (type != Real32_t)
    throw BadLogic;

  try{
    auto array  = nNet_GetArrayData(tensor);
    auto  size  = nNet_GetArraySizeT(tensor);
    auto result = std::vector<Real32>{};

    for (auto i = 0; i < size; ++i){
      result.push_back(*((Real32*)nNet_GetArrayItem(array, i, type)));
    }

    nNet_TryDropingArray(tensor, array);
    return result;
  } catch(base::Error& error){ throw base::Error{error}; }
}

template<> std::vector<Real64> values<Real64>(Tensor tensor){
  auto type = nNet_GetTensorNumT(tensor);

  if (type != Real64_t)
    throw BadLogic;

  try{
    auto array  = nNet_GetArrayData(tensor);
    auto  size  = nNet_GetArraySizeT(tensor);
    auto result = std::vector<Real64>{};

    for (auto i = 0; i < size; ++i){
      result.push_back(*((Real64*)nNet_GetArrayItem(array, i, type)));
    }

    nNet_TryDropingArray(tensor, array);
    return result;
  } catch(base::Error& error){ throw base::Error{error}; }
}

template<> Shape generate<Shape>(std::vector<int> dims){
  return nNet_CreateTensorShapeW(dims.size(), const_cast<int*>(dims.data()));
}

template<> Tensor generate<Tensor>(std::vector<int> dims){
  auto shape  = generate<Shape>(dims);
  auto result = nNet_CreateTensorW(shape);

  nNet_RemoveTensorShape(shape);
  return result;
}

template<> Ordinate generate<Ordinate>(std::vector<int> ords){
  return nNet_CreateOrdinateW1(nullptr, (int*)ords.data(),
                               static_cast<int>(ords.size()));
}

template<> Tensor move<Tensor>(Tensor dst, Tensor src){
  if (outdate(src) || outdate(dst))
    return nullptr;

  memcpy(dst, src, sizeof(TensorDef));
  return dst;
}

template<> Shape move<Shape>(Shape dst, Shape src){
  if (outdate(src) || outdate(dst))
    return nullptr;

  memcpy(dst, src, sizeof(ShapeDef));
  return dst;
}

template<> Ordinate move<Ordinate>(Ordinate dst, Ordinate src){
  if (outdate(src) || outdate(dst))
    return nullptr;

  memcpy(dst, src, sizeof(OrdinateDef));
  return dst;
}

template<> NumT best<NumT>(std::vector<Tensor> inputs){
  NumT result = NumT::Unknown;

  for (auto& input: inputs){
    if (input->type == NumT::Unknown)
      continue;
    else if (input->type == String_t)
      return String_t;

    if (result == NumT::Unknown)
      result = NumT(input->type);
    else if (nNet_SizeType(result) < nNet_SizeType(input->type))
      result = NumT(input->type);
    else if (result < input->type)
      result = NumT(input->type);
  }
  return result;
} 

void sequence(int begin, std::vector<std::function<int()>> tasks){
  if (begin < 0) begin  = 0;
  do {
    begin = tasks[begin]();
  } while(begin >= 0);
}
} // namespace utils
} // namespace nn