#include "interpreter.h"
#include "internal.h"

namespace nn{
namespace interpreter{
Lock::Lock(InterpreterW& interpreter): _self{&interpreter}{

}

void Lock::wait(Operator op){
}

void Lock::finish(Operator op){
  if (_self->resolve() == LOOPBACK_RESOLVE)
    return;

  /* @TODO: */
}
} // namespace interpreter
} // namespace nn