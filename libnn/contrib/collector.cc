#include <iterator>
#include <memory>

#include "interpreter.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
using Result = std::vector<std::pair<std::string, Tensor>>;

namespace internal{
void safe(pthread_mutex_t* lock, std::function<void()>&& callback);

Tensor clone(Interpreter interpreter, Tensor tensor){
  auto shape = nNet_CreateTensorShapeC1(interpreter, 0);

  if (nNet_GetTensorShape(tensor, shape)){
    auto result = nNet_CreateTensorC(interpreter, shape);

    if (result){
      if (!nNet_SetTensorNumTW(interpreter, result, tensor->type))
        return nNet_RemoveTensorW(interpreter, result);
      else {
        auto src_array = nNet_TrimArrayData(tensor);
        auto dst_array = nNet_GetArrayData(result);
        auto success   = true;

        for (long i = 0; success && i < nNet_GetTensorSize(tensor); ++i){
          auto src = nNet_GetArrayItem(src_array, i, tensor->type);
          auto dst = nNet_GetArrayItem(dst_array, i, result->type);

          success = nNet_ConvertNumT(src, tensor->type, dst, result->type);
        }

        nNet_SetArrayData(result, dst_array);
        nNet_TryDropingArray(tensor, src_array);

        if (!success)
          result = nNet_RemoveTensorW(interpreter, result);
      }
    }

    return result;
  } else
    return nullptr;
}
} // namespace internal


Collector::Collector(){
  _lock        = nullptr;
  _interpreter = nullptr;
  _creator     = (Thread)pthread_self();
  _yield       = [&](Result result){
    /* @NOTE: this callback will be called on a different thread and it's 
     * intended to collect result from there. Because result is temporary at 
     * this time, we must clone to keep them alive
     */
    push_back(result);
  };

  lock.getter([]() -> pthread_mutex_t*{ throw NoSupport; });
  lock.setter([&](pthread_mutex_t* lock){ 
    if (!_lock)
      _lock = lock; 
    else
      throw BadLogic.reason("don\'t accept double setting to Collector::lock");
  });

  interpreter.getter([&]() -> Interpreter{ return _interpreter; });
  interpreter.setter([&](Interpreter interpreter){ 
    if (!_interpreter)
      _interpreter = interpreter;
    else {
      throw BadLogic.reason("don\'t accept double setting to "
                            "Collector::interpreter");
    }
  });

  yield.setter([](std::function<void(Result)>){ throw NoSupport; });
  yield.getter([&]() -> std::function<void(Result)>{ return _yield; });
}

std::size_t Collector::size(){
  auto result = 0;

  internal::safe(_lock, [&](){ result = _results.size(); });
  return result;
}

base::Error Collector::push_back(Result result){
  if (_creator == (Thread)pthread_self())
    return BadAccess;
  else if (!_lock)
    return BadLogic.reason("please init Collector::lock first");

  internal::safe(_lock, [&](){ _results.push_back(clone(result)); });

  return NoError;
}

base::Error Collector::set(std::size_t index, Result result){
  if (_creator == (Thread)pthread_self())
    return BadAccess;
  else if (!_lock)
    return BadLogic.reason("please init Collector::lock first");

  internal::safe(_lock, [&](){
    if (index >= _results.size()){
      for (auto i = _results.size(); i < index; ++i){
        _results.push_back(std::shared_ptr<Result>{nullptr});
      }
    }

    _results[index] = clone(result);
  });
  return NoError;
}

std::shared_ptr<Result> Collector::get(std::size_t index){
  if (_creator != (Thread)pthread_self())
    throw BadAccess;

  if (index >= _results.size())
    return std::shared_ptr<Result>{nullptr};
  return _results[index];
}

Collector::PResult Collector::clone(Result src){
  PResult result{new Result{}};

  for (auto i = 0; i < cast_(i, src.size()); ++i){
    auto name   = std::get<0>(src[i]);
    auto tensor = internal::clone(_interpreter, std::get<1>(src[i]));

    result->push_back(std::make_pair(name, tensor));
  }
  return result;
}

void Collector::clear(){
  internal::safe(_lock, [&](){ _results.clear(); });
}
} // namespace internal