#include "internal.h"
#include "utils.h"

namespace nn{
int InterpreterW::complexity(Operator op){
  /* @TODO: */
  return -1;
}

bool InterpreterW::reconstruct(){
  using namespace nn::interpreter;
  using namespace nn::utils::wstring;
  using namespace nn::utils;

  if (_devices.size() > 1 && !_optimized){
    /* @TODO: detect which layer is used to be a bridge between devices */

    for (auto i = 0; i < cast_(i, _flowchart.size()); ++i){
      for (auto& op: std::get<1>(_flowchart[i])){
        auto opw    = nNet_GetOperatorW(op);
        auto target = op;

        /* @NOTE: check if op is derived from a generic device */
        if (!op->device || op->device->level == GENERIC_PLATFORM || 
            !op->device->tensor)
          continue;
        else {
          for (auto i = 0; i < cast_(i, opw->outputs().size()); ++i){
            opw->outputs()[i] = hook(op, opw->outputs()[i]);
          }
        }

        for (auto& input_item: opw->inputs()){
          std::string input_name{""};
          std::size_t index{0};
          bool        keep{_operators.size() == 1};

          wstring::tie(input_name, index) = split(std::get<0>(input_item), ':');

          /* @NOTE: okey now, check if there are another links with this input,
           * a tricky way is to prevent checking if this input is only 
           * connecting to an only one operator */

          if (!keep){
            /* @NOTE: only peform this block if we apply more than 1 op */

            for (auto& another_op_item: _operators){
              auto another_op  = std::get<1>(another_op_item);
              auto another_opw = nNet_GetOperatorW(another_op);

              if (another_op == op)
                continue;
              keep = true;

              for (auto& another_input_item: another_opw->inputs()){
                if (std::get<1>(input_item) != std::get<1>(another_input_item))
                  continue;

                /* @NOTE: check if we can raise the level of this input */
                if (!op->device) keep = true;
                else if (op->device->level == another_op->device->level)
                  continue;
                else {
                  keep = another_op->device->level != GENERIC_PLATFORM;

                  if (another_op->device->level < target->device->level)
                    target = another_op;
                  break;
                }
              }

              if (keep) break;
            }
          }

          if (!keep || complexity(op) >= 0)
            break;
          else
            keep = true;

          /* @NOTE: okey find the input and hook this input with the exact
           * device */
          for (auto& placeholder: _placeholders){
            if (std::get<0>(placeholder) != input_name)
              continue;

            /* @NOTE: found it, now hook placeholer with this op */
            std::get<1>(placeholder) = hook(target, std::get<1>(placeholder));

            if (!std::get<1>(placeholder)){
              return error(BadAccess << "InterpreterW:hook()");
            }
            /* @NOTE: tricky way to prevent searching whole operator when the 
             * input is hooked to a placeholder.
             */

            keep = false;
            break;
          }

          if (i > 0 && keep)
            for (auto& op_item: _operators){
              OperatorW* prev_opw = nNet_GetOperatorW(std::get<1>(op_item));

              if (std::get<0>(op_item) != input_name)
                continue;

              /* @NOTE: found it, now hook the output with this op */
              prev_opw->outputs()[index] = hook(target, prev_opw->outputs()[index]);

              if (!prev_opw->outputs()[index]){
                return error(BadAccess << "InterpreterW::hook()");
              } else {
                /* @NOTE: okey everything seems done but we need configure 
                 * bridges */

                if (std::get<1>(op_item)->device != op->device)
                  std::get<0>(_flowchart[i]).wait(prev_opw->interface());
                break;
              }
            }
        }
      }
    }

    _optimized = true;
  }
  return _optimized;
}
} // namespace nn