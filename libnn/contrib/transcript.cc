#include <fstream>

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
using PInterpreterW  = InterpreterW*;
using PInterpreterWs = std::vector<PInterpreterW>;

namespace global{
extern std::map<Thread, PInterpreterWs> topologies;
extern std::map<Thread, PInterpreterW>  interpreters;
extern std::map<std::string, Device>  devices;
extern std::map<std::string, Operator>  macros;
} // namespace global

namespace internal{
bool isExistInside(std::string& opname, std::vector<std::string>& saving,
                   std::map<std::string, Operator>& operators){
  /* @NOTE: scan if 'opname' have input link with any operator in 'saving' */
  if (operators.find(opname) != operators.end()){
    auto& op = operators[opname];

    if (!nNet_GetOperatorW(op))
      return false;

    for (auto& input: nNet_GetOperatorW(op)->inputs()){
      for (auto& candidate: saving){
        if (candidate == std::get<0>(input))
          return true;
      }
    }
  }

  return false;
}

template<typename Type>
bool isExist(const Type& value, std::vector<Type>& list, 
             std::function<bool(const Type&, const Type&)>compare = nullptr){
  /* @NOTE: check if 'value' existed in 'list' */
  for (auto& item: list){
    if (!compare){
      if (item == value)
        return true;
    } else if (compare(value, item))
      return true;
  }
  return false;
}

template<typename Type>
bool isExist(Type&& value, std::vector<Type>& list,  
             std::function<bool(const Type&, const Type&)>compare = nullptr){
  /* @NOTE: check if 'value' existed in 'list' */
  for (auto& item: list)
    if (compare == nullptr){
      if (item == value)
        return true;
    } else if (compare(value, item))
      return true;
  return false;
}
} // namespace internal

namespace internal{
namespace build{
base::Error transcript(std::string&& model, InterpreterW& interpreter,
                       bool lazyload);
} // namespace build

namespace save{
base::Error transcript(InterpreterW& interpreter, std::string& model);
} // namespace save
} // namespace internal

bool InterpreterW::arrange(){
  using namespace nn::utils::wstring;
  using namespace nn::utils;

  auto finished = std::vector<std::string>{};
  auto inputs   = std::vector<std::string>{};
  auto outputs  = std::vector<std::string>{};

  // Load default input, everything would prefer to begin with placeholders
  for (auto& placeholder: _placeholders){
    finished.push_back(std::get<0>(placeholder));
    inputs.push_back(std::get<0>(placeholder));
  }

  // Find the operators which it doesn't have any inputs
  for (auto& op: _operators){
    auto pass = true;

    /* @NOTE: avoid the case when lazyload is on but we didn't have any 
     * placeholders */
    for (auto& input: nNet_GetOperatorW(std::get<1>(op))->inputs()){
      std::string inputname = std::get<0>(input);

      if (!internal::isExist(inputname, inputs)){
        if (_operators.find(inputname) == _operators.end())
          continue;
      }
      pass = false;
      break;
    }

    if (pass){
      finished.push_back(std::get<0>(op));
      inputs.push_back(std::get<0>(op));
    }
  }

  if (_placeholders.size() == 0){
    auto chart = std::make_pair(interpreter::Lock{*this},
                                std::vector<Operator>{});

    /* @NOTE: add the first layer in the case lazyload is on */
    for (auto& input: inputs)
      std::get<1>(chart).push_back(_operators[input]);

    _flowchart.push_back(chart);
  }

  // scan and build a top-down 2D-graph
  for (auto i = 0; i < cast_(i, _operators.size());){
   #if ENABLE_FORWARD_BACKWARD_OPERATORS
    std::string input_name{};
    int         index{0};
   #endif

    outputs.clear();

    /* @NOTE: scan all _operators to find which one would link with the inputs 
     * and put them onto list 'outputs' */
    for (auto& op: _operators){
      auto opw  = nNet_GetOperatorW(std::get<1>(op));

      if (internal::isExist(std::get<0>(op), inputs))
        continue;

      if (!opw) return false;
      /* @NOTE: scan list 'output' just to make sure that we don't put the op
       * again and again. It helps improve performance by reducing searching 
       * many time with the same operator. */

      if (!internal::isExist(std::get<0>(op), outputs)){
        /* @NOTE: now, we must check if this op is connected with, at least, 
         * one of inputs */

        for (auto name: inputs){
          auto keep = true;

          for (auto op_input: opw->inputs()){
            if (name != std::get<0>(op_input))
              continue;
            else if (internal::isExist(std::get<0>(op), finished))
              return !(_error = BadLogic.reason("loop infinity"));

            outputs.push_back(std::get<0>(op));
            keep = false;
          }
          if (!keep) break;
        }
      }
    }

    /* @NOTE: if there is no outputs -> we don't need to do anymore now */
    if (outputs.size() == 0)
      break;
    inputs.clear();

    /* @NOTE: check link if outputs link together, if it's true, just turn down 
     * this node */
    for (auto& name: outputs){
      auto op   = nNet_GetOperatorW(_operators[name]);
      auto keep = true;

      if (internal::isExistInside(name, outputs, _operators))
        continue;

      /* @NOTE: check if this operator have more dependencies than expected */
      for (auto input: op->inputs()){
        std::string inputname = std::get<0>(input);

        if (_operators.find(inputname) == _operators.end())
          continue;
        if (internal::isExist(inputname, finished))
          continue;
        keep = false;
        break;
      }

      if (!keep) continue;
      else i++;

     #if ENABLE_FORWARD_BACKWARD_OPERATORS
      /* @NOTE: okey load this operator to flowchart and link with another 
       * operators, this is only userful for training */

      for (auto& item: op->inputs()){
        /* @NOTE: parse input parameters to name and index of the output gate of
         * this input operator */

        wstring::tie(input_name, index) = split(std::get<0>(item), ':');

        if (_operators.find(input_name) != _operators.end()){
          auto input = nNet_GetOperatorW(_operators[input_name]);

          /* @NOTE: add this operator to vector 'input->next' */
          if (!internal::isExist(op->interface(), input->next))
            input->next.push_back(op->interface());

          /* @NOTE: add input to vector 'op->previous' */
          if (!internal::isExist(input->interface(), op->previous))
            op->previous.push_back(input->interface());
        }
      }
     #endif

      finished.push_back(name);
      inputs.push_back(name);
    }

    if (inputs.size() == 0)
      return !(_error = BadLogic.reason("loop infinity"));
    else {
      auto chart = std::make_pair(interpreter::Lock{*this},
                                  std::vector<Operator>{});

      for(auto& input: inputs)
        std::get<1>(chart).push_back(_operators[input]);

      _flowchart.push_back(chart);
    }
  }

  return _flowchart.size() > 0? true: !(_error = DoNothing);
}

bool InterpreterW::transcript(std::string&& graph, bool lazyload){
  base::Boundary bound{[&](){
      lock();
      _openspace = true;
    }, 
    [&](){
      _status = Stop;
      _openspace = false;
      unlock();
    }};

  auto thread   = (Thread) pthread_self();
  /* @NOTE: step 1: we must translate graph and create operators  */

  if ((_error = nn::internal::build::transcript(rvalue(graph), *this, lazyload))){
    /* @NOTE: fail while translating graph -> clear everything and remove this
     * interpreter if it is created manually */

    if (!_interpreter.manual)
      global::topologies.erase(thread);

    /* @NOTE: clear everything now, we don't need anything from this graph */
    clear();
    return _error? false: !(_error = BadLogic);
  }

  /* @NOTE: step 2: verify and load task into flowchart, just to make sure
   * there were no infinited loopcast. */

  if (!arrange()) return false;
  return (_flowchart.size() > 0)? true: !(_error = DoNothing);
}

bool InterpreterW::load(std::string&& graph, bool lazyload){
  if (graph.size() > 0){
    _optimized = false;

    if (!transcript(rvalue(graph), lazyload)){
      return false;
    } else return true;
  } else return false;
}

bool InterpreterW::load(std::fstream&& stream, bool lazyload){
  return load(std::string(std::istreambuf_iterator<char>(stream), {}), lazyload);
}

bool InterpreterW::load(std::fstream&  stream, bool lazyload){
  return load(std::string(std::istreambuf_iterator<char>(stream), {}), lazyload);
}
} // namespace nn
