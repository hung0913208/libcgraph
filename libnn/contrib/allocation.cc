#include <cstdlib>
#include <map>

#include "internal.h"
#include "tensor.h"
#include "utils.h"

#define SHAPE_T    0
#define ORDINATE_T 1
#define TENSOR_T   2

extern "C" {
Shape    nNet_ReallocShapeW(Interpreter interpreter, Shape shape, int ndims);
Ordinate nNet_ReallocOrdinateW(Interpreter interpreter, Ordinate shape, int ndims);
Tensor   nNet_ReallocTensorW(Interpreter interpreter, Tensor tensor, int type, bool force);
Tensor   nNet_ReallocRefTensorW(Interpreter interpreter, Tensor tensor, Tensor src);

Shape    nNet_ReallocShape(Shape shape, int ndims);
Ordinate nNet_ReallocOrdinate(Ordinate shape, int ndims);
Tensor   nNet_ReallocTensor(Tensor tensor, int type, bool force);
Tensor   nNet_ReallocRefTensor(Tensor tensor, Tensor src);

void nNet_RemoveIfNeeded(Array array, int size, int type);
bool nNet_ConvertNumT(Item src, int stype, Item dst, int dtype);
}

namespace nn {
namespace utils {
void alloc(Shape shape);
void alloc(Ordinate ordinate);
void alloc(Tensor tensor);

void free(Shape shape);
void free(Ordinate ordinate);
void free(Tensor tensor);
} // namespace utils

namespace tensor{
using namespace base;
Tensor create(Interpreter interpreter, int type);

template<typename Type=void>
Tensor redefine(Interpreter interpreter, Tensor tensor, Type* &src, long size, 
                void* context,
                Tensor(*ok)(Interpreter, Tensor, Type*, void*),
                Tensor(*fail)(Interpreter, Tensor) = nullptr,
                long UNUSED(old) = 0, int UNUSED(type) = -1){
  /* @NOTE: redefine tensor's property */
  Type* result = nullptr;

  type = type < 0? nn::utils::type(typeid(Type)): type;
  if (0 < size && size*nNet_SizeType(type) < nNet_MaxPhysicalMemory()){
   #if USE_VECTORIZE
    if (src){
      result = (Type*)nNet_AllocateArray(size, type);

      memset(result, 0, size);
      if (old < size)
        memcpy(result, src, old);
      else
        memcpy(result, src, size);
      free(src);
    } else {
      result = (Type*)nNet_AllocateArray(size, type);
    }
   #else
    if (src)
      result = (Type*) realloc(src, size*nNet_SizeType(type));
    else {
      result = (Type*) malloc(size*nNet_SizeType(type));

      if (result)
        memset(result, 0, size);
    }
   #endif
  }
  if (result != NULL){
    if (ok)
      return ok(interpreter, tensor, result, context);
    else {
      src = result;
      return tensor;
    }
  } else if (size > 0){
    if (fail)
      return fail(interpreter, tensor);
    else {
      return nn::utils::clearFaulty(interpreter, tensor, 
        (DrainMem << "you have required" << std::to_string(size*nNet_SizeType(type))));
    }
  } else 
    return tensor;
}

Tensor ifref(Tensor tensor, std::function<Tensor(int, int)> callback) {
  /* @NOTE: if tensor is ref-tensor, then call the callback */
  if (tensor->_begins && tensor->_ends) {
    auto bdim = -1, edim = -1;

    for (auto i = 0; i < tensor->ndims; ++i) {
      if (tensor->_ends[i] - 1 != tensor->_begins[i]) {
        if (bdim < 0) {
          bdim = i;
        } else if (edim < 0) {
          edim = i;
          break;
        }
      }
    }

    if (bdim != -1) edim = bdim + 1;
    if (bdim != 0 || (edim != -1 && edim != tensor->ndims)) {
      return callback(bdim, edim);
    }
  }

  return tensor;
}

namespace scale {
Tensor del(Tensor tensor) {
  /* @NOTE: delete tensor scale */

  if (tensor->_begins) free(tensor->_begins);
  if (tensor->_ends) free(tensor->_ends);

  tensor->_begins = nullptr;
  tensor->_ends   = nullptr;

  return tensor;
}
} // namespace scale
} // namespace tensor
} // namespace nn

/* @NOTE: some private C-utilities of tensor */
extern "C" void JustRemoveTensorScales(Tensor tensor, Interpreter interpreter);
extern "C" void JustRemoveTensorDims(Tensor tensor, Interpreter interpreter);

/* @NOTE: interpreterfaces with C-API */
extern "C" Shape nNet_ReallocShape(Shape shape, int ndims){
  return nNet_ReallocShapeW(nullptr, shape, ndims);
}

extern "C" Ordinate nNet_ReallocOrdinate(Ordinate shape, int ndims){
  return nNet_ReallocOrdinateW(nullptr, shape, ndims);
}

extern "C" Tensor nNet_ReallocTensor(Tensor tensor, int type, bool force){
  return nNet_ReallocTensorW(nullptr, tensor, type, force);
}

extern "C" Shape nNet_RemoveTensorShape(Shape shape){
  return nNet_RemoveTensorShapeW(nullptr, shape);
}

extern "C" Ordinate nNet_RemoveOrdinate(Ordinate ordinate){
  return nNet_RemoveOrdinateW(nullptr, ordinate);
}

extern "C" Tensor nNet_RemoveTensor(Tensor tensor){
  return nNet_RemoveTensorW(nullptr, tensor);
}

extern "C" Tensor nNet_ReallocTensorW(Interpreter interpreter, Tensor tensor,
                                     int type, bool force){ 
  Tensor result = tensor;

  if (!result || nn::utils::outdate(interpreter, tensor))
    result = nn::tensor::create(interpreter, type);
  else {
    long old_size = result->_size;
    int  old_type = result->type;
    long new_size = 1;
    long max      = result->_max;

    /* @NOTE: reinit tensor again with another type by using tensor->init */
    if (!type){
      return nn::utils::null<Tensor>(interpreter,
        BadLogic.reason("type must not be unknown"));
    } else if (!force && type == result->type)
      return result;

    new_size = (result->_size > 0)? result->_size: nn::utils::size(result);
    if (tensor->_begins && tensor->_ends && !force){
      /* @NOTE: 'force' is used at this case and we just release everything,
       * because ref-tensor is always inited with our own callback 'release', 
       * it's always fine */
      warn(NoSupport).reason("calling nNet_ReallocTensorW with a ref-tensor "
                            "forces this function to recreate everything");

      /* @NOTE: when someone tries to trick this internal function by calling
       * with the wrong callback 'release', it cause big devastate. To prevent
       * it, i think i must return null without clearning tensor. This never 
       * cause any memory leak since we have a simple gabage collection */
      if (tensor->release != JustRemoveTensorScales &&
          tensor->release != JustRemoveTensorDims){
        BadLogic.reason("please don\'t change ref-tensor's callback \'release\'"
                        "since it causes many probleams");
        return nullptr;
      } else
        force = true;
    }

    /* @NOTE: step 1: create tensor's properties */
    if (force) {
      using namespace nn::tensor;

      auto ndims = result->ndims;
      auto dims = (int*)nullptr;

      /* @NOTE: recreate tensor->dims */
      if (result->_begins && result->_ends) {
        /* @NOTE: this tensor has been created by calling nNet_GetTensorItem,
         * its dims must be redefined again to shrink tensor to the its real
         * size */

        result = ifref(result, [&](int bdim, int edim) -> Tensor {
          /* @NOTE: if this is a ref-tensor, redefine it */
          result = redefine<int>(
            interpreter, tensor, dims, (edim - bdim), nullptr, nullptr, nullptr,
            0, -1);

          if (result) {
            auto ndims = edim - bdim;
            auto new_size = 1;

            /* @NOTE: calculate real dims base on tensor scales */
            for (auto i = bdim, j = 0; i < edim; ++i, ++j) {
              auto begin = result->_begins ? result->_begins[i] : 0;
              auto end = result->_ends ? result->_ends[i] : result->dims[i];

              dims[j] = end - begin;
              new_size *= end - begin;
            }

            /*
             @NOTE: just to make sure, we would like to call release first
             * to clean everything, if result->dims still insists on remain,
             * we must do clearing it manually
             */

            if (result->release) result->release(result, interpreter);
            if (result->dims) free(result->dims);

            /* @NOTE: okey now, we must update new dims */
            result->dims = dims;
            result->ndims = ndims;
          } else {
            return nn::utils::clearFaulty(
                    interpreter, tensor,
                    DrainMem.reason("redefine(tensor->dims) fail"));
          }
          return result;
        });
      } else {
        /* @NOTE: this tensor has been created by nNet_CreateTensor to be
         * reallocated */

        result = redefine<int>(
          interpreter, result, dims, ndims, nullptr, nullptr, nullptr, 0, -1);

        if (result) {
          /* @NOTE: the result->dims has been redefined, we must copy values
           * from old-array to new-array */
          memcpy(dims, result->dims, ndims * sizeof(int));

          /* @NOTE: to keep everything safe, we must release all value first
           * to prevent memory leak */
          if (result->release) result->release(result, interpreter);
          if (result->dims) free(result->dims);

          if (!result->init && result->_content) free(result->_content);

          /* @NOTE: okey now, we must update new dims */
          result->dims = dims;
          result->ndims = ndims;
        } else {
          return nn::utils::clearFaulty(
                  interpreter, tensor,
                  DrainMem.reason("redefine(tensor->_content) fail"));
        }
      }

      /* @NOTE: recreate tensor->_content, we intend recreate, only happen
       * when tensor->init == nullptr */

      if (result->init) {
        if (!result->init(result, interpreter, new_size, type, true))
          return nNet_RemoveTensorW(interpreter, result);
      } else {
        void* buffer{nullptr};

        result = redefine<void>(
          interpreter, result, buffer, new_size, nullptr, nullptr, nullptr,
          0, type);

        if (result)
          result->_content = buffer;
        else {
           return nn::utils::clearFaulty(
                  interpreter, tensor,
                  DrainMem.reason("redefine(tensor->_content) fail"));
        }
      }
    } else if (new_size > old_size || old_type < type) {
      using namespace nn::tensor;

      /* @NOTE: scale in or scale out tensor->_content, only happen when
       * tensor->init == nullptr */
      if (result->init) {
        if (!result->init(result, interpreter, new_size, type, false))
          return nNet_RemoveTensorW(interpreter, result);
      } else {
        result = redefine<void>(
          interpreter, result, result->_content, new_size,
          nullptr, nullptr, nullptr,
          old_size, type);

        if (!result){
           return nn::utils::clearFaulty(
                  interpreter, tensor,
                  DrainMem.reason("redefine(tensor->_content) fail"));
        }
      }
    }

    /* @NOTE: flag 'force' has a unique intension, to release tensor-scale */
    if (force) {
      result = nn::tensor::scale::del(result);
    }

    result->_size = new_size;
    result->_max = type ? result->_size * nNet_SizeType(type) : 0;
    result->_max = max < result->_max ? result->_max : max;

    /* @NOTE: step 2: update tensor's properties from cache */
    result->_size = new_size;
    result->type = type;
  }

  return result;
}

extern "C" Shape nNet_ReallocShapeW(Interpreter interpreter, Shape shape,
                                     int ndims){
  Shape result = nullptr;
  using namespace nn;

  if (ndims < 0 || (interpreter && !interpreter->priv)){
    if (ndims < 0){
      return nn::utils::clearFaulty(interpreter, shape,
                                    BadLogic.reason("ndims < 0"));
    } else {
      return nn::utils::clearFaulty(interpreter, shape,
                                    BadLogic.reason("interpreter broken"));
    }
  } else if (!shape || nn::utils::outdate(interpreter, shape)) {
    auto interpreterw = nNet_GetInterpreterW(interpreter);

    result = (Shape) calloc(1, sizeof(ShapeDef));
    interpreterw->assignPointer(result, SHAPE_T);

    if (result) {
      if (ndims > 0) {
        result->dims  = (int*)calloc(ndims, sizeof(int));
        result->ndims = ndims;
        result->max   = ndims;

        if (!result->dims) {
          result = nNet_RemoveTensorShapeW(interpreter, result);
        }
      }
    }
  } else {
    result = shape;

    /* @NOTE: force TensorShape to release its memory by assign ndims with 0 */
    if (ndims == 0) {
      if (result->dims) free(result->dims);

      result->dims  = nullptr;
      result->ndims = 0;
      result->max   = 0;
    } else if (shape->ndims < ndims) {
      result->dims  = (int*)realloc(shape->dims, sizeof(int) * ndims);
      result->ndims = ndims;
      result->max   = ndims;
    } else shape->ndims = ndims;
  }

  return result;
}

extern "C" Ordinate nNet_ReallocOrdinateW(Interpreter interpreter, 
                                          Ordinate ordinate, int ndims){
  Ordinate result = nullptr;
  using namespace nn;

  if (ndims <= 0 || (interpreter && !interpreter->priv)) return ordinate;
  else if (!ordinate){
    auto interpreterw = nNet_GetInterpreterW(interpreter);

    result = reinterpret_cast<Ordinate>(calloc(1, sizeof(OrdinateDef)));
    interpreterw->assignPointer(result, ORDINATE_T);

    if (result){
      result->begins = reinterpret_cast<int*>(calloc(ndims, sizeof(int)));
      result->ends   = reinterpret_cast<int*>(calloc(ndims, sizeof(int)));
      result->ndims  = ndims;
      result->max    = ndims;

      if (!result->begins || !result->ends) {
        if (result->begins) free(result->begins);
        if (result->ends) free(result->ends);

        result = nNet_RemoveOrdinateW(interpreter, result);
      }
    }
  } else {
    result = ordinate;

    if (ordinate->ndims < ndims) {
      result->begins = reinterpret_cast<int*>(realloc(ordinate->begins,
                                              sizeof(int) * ndims));
      result->ends   = reinterpret_cast<int*>(realloc(ordinate->ends,
                                              sizeof(int) * ndims));
      result->ndims  = ndims;
      result->max    = ndims;
    } else result->ndims = ndims;
  }

  return result;
}

extern "C" Tensor nNet_ReallocRefTensor(Tensor tensor, Tensor source) {
  return nNet_ReallocRefTensorW(nullptr, tensor, source);
}

extern "C" Tensor nNet_ReallocRefTensorW(Interpreter interpreter, Tensor tensor,
                                                                Tensor source) {
  if (!tensor || nn::utils::outdate(interpreter, source))
    return nNet_ReshapeTensorW(nNet_CreateTensorC(interpreter, nullptr), source);
  else if (source && !nn::utils::outdate(source)) {
    /* @NOTE: if source != nullptr -> we must clear and prepare tensor-scale */

    if (nNet_GetTensorNumT(tensor) != Unknown) {
      if (nn::utils::diff(tensor, source) > nn::utils::DimDiff)
        return nn::utils::clearFaulty(interpreter, tensor,                     
          BadLogic.reason("diff() > DimDiff"));
      else if (!(tensor = nNet_ClearTensor(tensor)))
        return nn::utils::clearFaulty(interpreter, tensor,                     
          BadLogic.reason("nNet_ClearTensor() error, tensor still alive"));
    }

    tensor->_begins = (int*)calloc(source->ndims, sizeof(int));
    tensor->_ends   = (int*)calloc(source->ndims, sizeof(int));
    tensor->type    = source->type;
    tensor->release = JustRemoveTensorScales;

    if (tensor->_begins && tensor->_ends)
      return tensor;

    return nn::utils::clearFaulty(interpreter, tensor,                         
      DrainMem.reason("tensor->_begins and tensor->_ends must not nullptr"));
    } else if (tensor->_begins && tensor->_ends) {
    /* @NOTE: if tensor-scales is inited, just remove and the tensor will use
     * its maximum dimemtion */

    free(tensor->_begins);
    free(tensor->_ends);

    tensor->_begins = nullptr;
    tensor->_ends   = nullptr;

    if (tensor->release == JustRemoveTensorScales ||
        tensor->release == JustRemoveTensorDims)
      tensor->release = nullptr;
  }
  return tensor;
}

extern "C" Tensor nNet_RemoveTensorW(Interpreter interpreter, Tensor tensor) {
  using namespace nn;

  if (interpreter && !interpreter->priv) return tensor;
  if (tensor && !nn::utils::outdate(interpreter, tensor)) {
    auto interpreterw = nNet_GetInterpreterW(interpreter);
    auto dummy        = interpreterw->isDummyPointer(tensor);

    if (tensor->release) tensor->release(tensor, interpreter);
    if (tensor->_content){
      /* @NOTE: type String_t is the special case and we must clear manually */

      nNet_RemoveIfNeeded(nNet_GetArrayData(tensor), nNet_GetArraySizeT(tensor),
                          nNet_GetTensorNumT(tensor));
      free(tensor->_content);
    }
    if (tensor->ndims > 0 && tensor->dims)
      free(tensor->dims);

    tensor->dims     = nullptr;
    tensor->_content = nullptr;

    if (tensor->_begins) free(tensor->_begins);
    if (tensor->_ends) free(tensor->_ends);

    tensor->_begins = nullptr;
    tensor->_ends   = nullptr;

    interpreterw->removePointer(tensor);
    if (!dummy) free(tensor);
    return nullptr;
  }
  return tensor;
}

extern "C" Shape nNet_RemoveTensorShapeW(Interpreter interpreter, Shape shape) {
  using namespace nn;

  if (interpreter && !interpreter->priv) return shape;
  if (shape && !nn::utils::outdate(interpreter, shape)) {
    auto interpreterw = nNet_GetInterpreterW(interpreter);
    auto dummy        = interpreterw->isDummyPointer(shape);

    if (shape->dims) free(shape->dims);

    shape->dims = nullptr;
    shape->ndims = 0;

    interpreterw->removePointer(shape);    
    if (!dummy) free(shape);
    return nullptr;
  }
  return shape;
}

extern "C" Ordinate nNet_RemoveOrdinateW(Interpreter interpreter, 
                                         Ordinate ordinate) {
  using namespace nn;

  if (interpreter && !interpreter->priv) return ordinate;
  if (ordinate && !nn::utils::outdate(interpreter, ordinate)) {
    auto interpreterw = nNet_GetInterpreterW(interpreter);
    auto dummy        = interpreterw->isDummyPointer(ordinate);

    if (ordinate->begins) free(ordinate->begins);
    if (ordinate->ends) free(ordinate->ends);

    ordinate->begins = nullptr;
    ordinate->ends = nullptr;

    interpreterw->removePointer(ordinate);
    if (!dummy) free(ordinate);
    return nullptr;
  }
  return ordinate;
}
