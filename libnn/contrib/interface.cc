#include "internal.h"
#include "utils.h"

#include <fstream>

using Gates = std::vector<std::pair<std::string, Tensor>>; 

namespace nn{
using PInterpreters = std::vector<Interpreter>;

namespace global{
extern std::map<Thread, PInterpreters> topologies;
} // namespace global

namespace internal{
Gates buildInputs(CGates* inputs, bool autodel = true){
  Gates result;

  for (auto i = 0; i < inputs->size; ++i){
    if (inputs->inputs[i]){
      result.push_back(std::make_pair(std::string{inputs->inputs[i]}, 
                       inputs->tensors[i]));
    }

    if (autodel){
      inputs->inputs[i]  = nullptr;
      inputs->tensors[i] = nullptr;
    }
  }
  return result;
}

CGates buildOutputs(Gates outputs){
  CGates result;

  memset(&result, 0, sizeof(CGates));
  if (outputs.size()){
    result.inputs  = (CString*)malloc(outputs.size()*sizeof(CString));
    result.tensors = (Tensor*)malloc(outputs.size()*sizeof(Tensor));

    if (!result.inputs || !result.tensors)
      goto fail_alloc;
    else
      result.size = outputs.size();
  }

  for (auto i = 0; i < cast_(i, outputs.size()); ++i){
    result.inputs[i]  = strdup(std::get<0>(outputs[i]).c_str());
    result.tensors[i] = std::get<1>(outputs[i]);
  }
  return result;

 fail_alloc:
  if (result.inputs) free(result.inputs);
  if (result.tensors) free(result.tensors);
  throw DrainMem;
}

bool stillExist(Interpreter interpreter){
  using namespace global;
  if (interpreter){
    auto thread_id = (Thread)pthread_self();

    if (topologies.find(thread_id) == topologies.end())
      return false;

    for (auto& child: topologies[thread_id]){
      if (child == interpreter)
        return true;
    }
  }

  return false;
}
} // namespace internal
} // namespace nn

int nNet_EvalGraph(Interpreter interpreter, CGates* inputs, 
                   std::function<void(Gates)> yield);
int nNet_RunGraph(Interpreter interpreter, CGates* inputs,
                  std::function<void(Gates)> yield);
int nNet_FlushGraph(Interpreter interpreter, std::function<void(Gates)> yield);

extern "C"{
CGates nNet_CreateCGates(Interpreter interpreter, int how_many_gate){
  CGates result;

  result.interpreter = interpreter;
  result.inputs      = (CString*) calloc(sizeof(CString), how_many_gate);
  result.tensors     = (Tensor*) calloc(sizeof(Tensor), how_many_gate);
  result.size        = how_many_gate;
  return result;
}

bool nNet_RemoveCGates(CGates* cgates){
  if (!cgates || !cgates->size || !cgates->inputs || !cgates->tensors)
    return false;
  else {
    free(cgates->inputs);
    free(cgates->tensors);

    cgates->size        = 0;
    cgates->inputs      = nullptr;
    cgates->tensors     = nullptr;
    cgates->interpreter = nullptr;
    return true;
  }
}

bool nNet_AssignTensorToCGates(CGates* cgates, CString name, Tensor tensor,
                               int index){
  if (!cgates || !cgates->inputs || !cgates->tensors)
    return false;
  else if (index > cast_(index, cgates->size))
    return false;
  else if (!name || !tensor)
    return false;
  else {
    if (cgates->tensors[index] != tensor && cgates->tensors[index])
      nNet_RemoveTensorW(cgates->interpreter, cgates->tensors[index]);

    cgates->inputs[index]  = name;
    cgates->tensors[index] = tensor;
    return true;
  }
}

Interpreter nNet_CreateInterpreter(CString model){
  return nNet_CreateInterpreterW(model, false, false);
}

Interpreter nNet_CreateInterpreterW(CString model, bool force, bool lazyload){
  auto interpreter = new nn::InterpreterW{model? model: "", force, lazyload};

  if (interpreter->error()){
    delete interpreter;
    return nullptr;
  } else {
    return &(interpreter->interface());
  }
}

Interpreter nNet_CreateInterpreterFromFile(CString path){
  if (!path)
    return nNet_CreateInterpreter(path);
  else
    return nNet_CreateInterpreterFromFileW(path, false, false);
}

Interpreter nNet_CreateInterpreterFromFileW(CString path, bool force, 
                                            bool lazyload){
  std::fstream stream{path, std::ios::in};

  if (stream.is_open()){
    auto interpreter = new nn::InterpreterW{stream, force, lazyload};

    if (interpreter->error()){
      delete interpreter;
      return nullptr;
    } else {
      return &(interpreter->interface());
    }
  } else
    return nullptr;
}

Interpreter nNet_RemoveInterpreter(Interpreter interpreter){
  auto context = nNet_GetInterpreterW(interpreter);

  if (!nn::internal::stillExist(interpreter))
    return nullptr;
  if (context) delete context;
  else
    return interpreter;
  return nullptr;
}

Tensor nNet_GetResultAtIndex(Interpreter interpreter, CString label, int index){
  auto context = nNet_GetInterpreterW(interpreter);

  if (context && context->collector()){
    auto result = context->collector()->get(index);

    for (auto i = 0; i < cast_(i, result->size()); ++i){
      if (std::get<0>((*result)[i]) == label)
        return std::get<1>((*result)[i]);
    }
    return nullptr;
  } else
    return nullptr;
}

bool nNet_StartInterpreter(Interpreter interpreter){
  auto context = nNet_GetInterpreterW(interpreter);

  if (context)
    return context->start().code == 0;
  else return false;
}

bool nNet_StopInterpreter(Interpreter interpreter){
  auto context = nNet_GetInterpreterW(interpreter);

  if (context)
    return context->finish().code == 0;
  else return false;
}

int nNet_ParseToInterpreter(Interpreter interpreter, CString model){
  auto context = nNet_GetInterpreterW(interpreter);

  if (!context)
    return DoNothing;
  else
    context->load(model);
  return context->error();
}

int nNet_InvokeGraph(Interpreter interpreter, CGates* inputs){
  auto context = nNet_GetInterpreterW(interpreter);

  using namespace nn::internal;
  if (!context)
    return DoNothing; 

  switch (context->status()){
  case nn::InterpreterW::Stop:
    /* @NOTE: usually, we call nNet_InvokeGraph right after create a new 
     * Interpreter. At this time, its status allways be 'Stop' so we must check
     * and start this interpreter on to change its status.
     */
    if (context->flow().size() == 0)
      return DoNothing;
    break;

  case nn::InterpreterW::Initing:
    /* @TODO: this status usually happens when we create a Interpreter and add 
     * operators to its flow -> call nNet_RunGraph now */
    break;

  case nn::InterpreterW::Pause:
    /* @TODO: this status only happens when we halt a running Interpreter or it
     * finish every task and wait a new task. We just put a new task to queue
     * and start it on now.
     */
    break;

  case nn::InterpreterW::Flushing:
    return false;

  case nn::InterpreterW::Running:
    break;
  }
  return nNet_RunGraph(interpreter, inputs, CYield{nullptr});
}

int nNet_RunGraph(Interpreter interpreter, CGates* inputs, CYield yield){
  using namespace nn::internal;

  return nNet_RunGraph(interpreter, inputs, 
    [&](Gates result){
      auto context = nNet_GetInterpreterW(interpreter);

      try{ 
        if (yield)
          yield(buildOutputs(result)); 
      } catch(base::Error& error){
        context->error(error); 
      }
    });
}

int nNet_EvalGraph(Interpreter interpreter, CGates* inputs, CYield yield){
  using namespace nn::internal;

  if (yield){
    return nNet_EvalGraph(interpreter, inputs, [&](Gates result){
      auto gate = buildOutputs(result);

      yield(gate);
      for (auto j = 0; j < gate.size; ++j) {
        if (gate.inputs[j])
          free(const_cast<char *>(gate.inputs[j]));

        if (gate.tensors[j])
          nNet_RemoveTensorW(gate.interpreter, gate.tensors[j]);
      }

      for (auto i = 0; i < gate.size; ++i){
        if (gate.inputs[i])
          free (const_cast<char*>(gate.inputs[i]));
      }

      free(gate.inputs);
      free(gate.tensors);
    });
  } else
    return nNet_EvalGraph(interpreter, inputs, nullptr);
}

int nNet_FlushGraph(Interpreter interpreter, CYield yield){
  using namespace nn::internal;

  if (yield){
    return nNet_FlushGraph(interpreter, [&](Gates result){
     /* @NOTE: everything must be done by callback 'yield', it's okey now to
      * remove output gate completely
      */
      auto gate = buildOutputs(result);

      yield(gate);
      for (auto j = 0; j < gate.size; ++j) {
        if (gate.inputs[j])
          free(const_cast<char *>(gate.inputs[j]));

        if (gate.tensors[j])
          nNet_RemoveTensorW(gate.interpreter, gate.tensors[j]);
      }

      free(gate.inputs);
      free(gate.tensors);
    });
  } else
    return nNet_FlushGraph(interpreter, std::function<void(Gates)>{nullptr});
}

int nNet_UseResolver(Interpreter interpreter, int resolver){
  auto context = nNet_GetInterpreterW(interpreter);

  /* @NOTE: by default we allways use resolver 'loopback', this C-API is used 
   * when we want to change to another resolver base on the hw architecture
   */

  if (context->resolve(resolver) < 0)
    return NoSupport;
  return base::error::ENoError;
}

#if DEV
int nNet_HaltGraph(Interpreter interpreter){
  /* @TODO: implement this function */
  return base::error::ENoError;
}

int nNet_HaltGraph(Interpreter interpreter){
  /* @TODO: implement this function */
  return base::error::ENoError;
}
int nNet_InsertNodeToGraph(Interpreter interpreter, 
                           CString opname, CString name,
                           bool is_input_gate){
  return base::error::ENoError;
}
int nNet_SeekGraph(Interpreter interpreter, int offset, int whence,
                   bool is_on_vertical_align){
  return base::error::ENoError;
}
int nNet_TellGraph(Interpreter interpreter, Operator* result){
  return base::error::ENoError;
}

int nNet_SetInputOfOperator(Operator op, int index, Tensor tensor){
  return base::error::ENoError;
}

int nNet_GetOutputOfOperator(Operator op, int index, Tensor* result){
  return base::error::ENoError;
}

int nNet_SetOpAttribute(Operator op, CString name, int type, void* value){
  return NoSupport;
}

int nNet_GetOpAttribute(Operator op, CString name, void* result){
  return NoSupport;
}
#endif
}

int nNet_EvalGraph(Interpreter interpreter, CGates* inputs, 
                   std::function<void(Gates)> yield){
  auto context = nNet_GetInterpreterW(interpreter);

  using namespace nn::internal;
  if (context){
    return context->invoke(buildInputs(inputs, false), yield, false);
  } else {
    return BadAccess.reason("Interpreter has been remove or doesn't created");
  }
}

int nNet_FlushGraph(Interpreter interpreter, std::function<void(Gates)> yield){
  auto context = nNet_GetInterpreterW(interpreter);

  using namespace nn::internal;
  if (!context)
    return DoNothing;

  /* @NOTE: everything is done, now we just wait consumers finish tasks */
  context->waitUtilEnd();
 
  if (context->collector() && !context->error() && yield){
    /* @NOTE: because we use a collector to fetch outcome, so we can apply
     * callback 'yield' at here to transfer result from collector to outside
     */

    try {
      if (yield){
        for (auto i = 0; i < cast_(i, context->collector()->size()); ++i)
          yield(*context->collector()->get(i));
      }

      if (stillExist(interpreter))
        context->collector()->clear();
    } catch(base::Error& error){ context->error(error); }
  }

  return context->error();
}

int nNet_FlushGraph(Interpreter interpreter, std::size_t index,
                    std::function<void(std::vector<Tensor>)> yield){
  auto context = nNet_GetInterpreterW(interpreter);

  using namespace nn::internal;
  if (!context)
    return DoNothing;

  /* @NOTE: everything is done, now we just wait consumers finish tasks */
  context->waitUtilEnd();
 
  if (context->collector() && !context->error() && yield){
    /* @NOTE: because we use a collector to fetch outcome, so we can apply
     * callback 'yield' at here to transfer result from collector to outside
     */

    try {
      if (yield){
        std::vector<Tensor> outputs;

        for (auto i = 0; i < cast_(i, context->collector()->size()); ++i){
          auto& record = *context->collector()->get(i);

          if (index < record.size()){
            outputs.push_back(std::get<1>(record[index]));
          }
        }

        yield(outputs);  
      }

      if (stillExist(interpreter))
        context->collector()->clear();
    } catch(base::Error& error){       
      if (stillExist(interpreter))
        context->error(error); 
      return error;
    }
  }

  if (stillExist(interpreter))
    return context->error();
  else
    return NoError;
}

int nNet_RunGraph(Interpreter interpreter, CGates* inputs,
                  std::function<void(Gates)> yield){
  using namespace nn::internal;
  using namespace nn;

  auto keep    = false;
  auto context = nNet_GetInterpreterW(interpreter);
  auto wrapper = yield;

  if (!context)
    return DoNothing;

  if (context->status() == nn::InterpreterW::Stop){    
    /* @NOTE: usually, we call nNet_InvokeGraph right after create a new 
     * Interpreter. At this time, its status allways be 'Stop' so we must check
     * and start this interpreter on to change its status.
     */
    keep = true;
    if (context->flow().size() == 0)
      return DoNothing;
    else
      context->start();
  }

  /* @NOTE: we only load callback 'yield' at once to prevent confuse or race 
   * condition on consumers
   */

  if (context->status() == nn::InterpreterW::Initing || keep){
    /* @NOTE: by default we usually catch result at the time when it occurs.
     * However, it might cause the results return randomly, so we can use a 
     * collector to earn a sorted results
     */

    if (!keep)
      context->start();

    if (yield){
      /* @NOTE: catch directly, this callback will run on after a consumer 
       * return value */

      if (context->invoke(buildInputs(inputs), wrapper)){
        return context->error();
      } else
        return base::error::ENoError;
    } else if (!context->collector()){
      /* @NOTE: use a collector to fetch data from consumers */

      if (context->invoke(buildInputs(inputs), std::make_shared<Collector>())){
        return context->error();
      } else 
        return base::error::ENoError;
    } else {
      if (context->invoke(buildInputs(inputs), context->collector())){
        return context->error();
      } else 
        return base::error::ENoError;
    }
  } else {
    if (context->collector()){
      if (context->invoke(buildInputs(inputs), context->collector())){
        return context->error();
      } else 
        return base::error::ENoError;
    } else if (context->invoke(buildInputs(inputs), wrapper)){
      return context->error();
    } else 
      return base::error::ENoError;
  }
}

int nNet_EvalGraph(Interpreter interpreter, CGates* inputs,
                   std::function<void(CGates)> yield){
  using namespace nn::internal;

  if (yield){
    return nNet_EvalGraph(interpreter, inputs, [&](Gates result){
      auto gate = buildOutputs(result);

      yield(gate);

      /* @NOTE: because EvalGraph call directly to CGraph so we will 
       * receive a tensor from CGraph, not from a Collector, so we must
       * not clear it because CGraph might reuse it again
       */
     for (auto i = 0; i < gate.size; ++i){
        if (gate.inputs[i])
          free(const_cast<char*>(gate.inputs[i]));
      }

      free(gate.inputs);
      free(gate.tensors);
    });
  } else
    return nNet_EvalGraph(interpreter, inputs, nullptr);
}

int nNet_FlushGraph(Interpreter interpreter, std::function<void(CGates)> yield){
  using namespace nn::internal;

  if (yield){
    return nNet_FlushGraph(interpreter, [&](Gates result){
     /* @NOTE: everything must be done by callback 'yield', it's okey now to
      * remove output gate completely
      */
      auto gate = buildOutputs(result);

      yield(gate);
      for (auto j = 0; j < gate.size; ++j) {
        if (gate.inputs[j])
          free(const_cast<char *>(gate.inputs[j]));

        if (gate.tensors[j])
          nNet_RemoveTensorW(gate.interpreter, gate.tensors[j]);
      }

      free(gate.inputs);
      free(gate.tensors);
    });
  } else
    return nNet_FlushGraph(interpreter, nullptr);
}

int nNet_RunGraph(Interpreter interpreter, CGates* inputs,
                  std::function<void(CGates)> yield){
  using namespace nn::internal;
  using namespace nn;

  auto keep    = false;
  auto context = nNet_GetInterpreterW(interpreter);
  auto wrapper = [&](Gates result){ 
    if (yield){
      auto gate = buildOutputs(result);

      yield(gate);

      /* @NOTE: because EvalGraph call directly to CGraph so we will 
       * receive a tensor from CGraph, not from a Collector, so we must
       * not clear it because CGraph might reuse it again
       */
      free(gate.inputs);
      free(gate.tensors);
    }
  };

  if (!context)
    return DoNothing;

  if (context->status() == nn::InterpreterW::Stop){    
    /* @NOTE: usually, we call nNet_InvokeGraph right after create a new 
     * Interpreter. At this time, its status allways be 'Stop' so we must check
     * and start this interpreter on to change its status.
     */
    keep = true;
    if (context->flow().size() == 0)
      return DoNothing;
    else
      context->start();
  }

  /* @NOTE: we only load callback 'yield' at once to prevent confuse or race 
   * condition on consumers
   */

  if (context->status() == nn::InterpreterW::Initing || keep){
    /* @NOTE: by default we usually catch result at the time when it occurs.
     * However, it might cause the results return randomly, so we can use a 
     * collector to earn a sorted results
     */

    if (!keep)
      context->start();

    if (yield){
      /* @NOTE: catch directly, this callback will run on after a consumer 
       * return value */

      if (context->invoke(buildInputs(inputs), wrapper)){
        return context->error();
      } else
        return base::error::ENoError;
    } else if (!context->collector()){
      /* @NOTE: use a collector to fetch data from consumers */

      if (context->invoke(buildInputs(inputs), std::make_shared<Collector>())){
        return context->error();
      } else 
        return base::error::ENoError;
    } else {
      if (context->invoke(buildInputs(inputs), context->collector())){
        return context->error();
      } else 
        return base::error::ENoError;
    }
  } else {
    if (context->collector()){
      if (context->invoke(buildInputs(inputs), context->collector())){
        return context->error();
      } else 
        return base::error::ENoError;
    } else if (context->invoke(buildInputs(inputs), wrapper)){
      return context->error();
    } else 
      return base::error::ENoError;
  }
}
