#include "utils.h"

namespace nn{
namespace utils{
namespace wstring{
std::vector<std::string> split(CString raw, const char sep){
  auto result = std::vector<std::string>{};
  auto size   = strlen(raw);
  auto begin  = 0;

  for (auto i = 0; i < cast_(i, size); ++i){
    if (raw[i] == sep){
      result.push_back(std::string{raw + begin, raw + i});
      begin = i + 1;
    }
  }

  result.push_back(std::string{raw + begin, raw + size});
  return result;
}
}
}
}