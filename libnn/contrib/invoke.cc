#include <unistd.h>
#include <time.h>

#ifdef LIBCGRAPH_GIT_REFSPEC
#include <libbase/all.hpp>
#else
#include "config.hpp"
#endif

#include "interpreter.h"
#include "internal.h"
#include "resolvers.h"
#include "tensor.h"
#include "utils.h"

extern "C" void JustRemoveTensorDims(Tensor tensor, Interpreter interpreter);

namespace nn{
using PInterpreterW  = InterpreterW*;
using PInterpreterWs = std::vector<PInterpreterW>;

namespace global{
extern std::map<Thread, PInterpreterWs> topologies;
extern std::map<Thread, PInterpreterW>  interpreters;
extern std::map<std::string, Device>    devices;
extern std::map<std::string, Operator>  macros;
} // namespace global

namespace internal{
namespace resolver{
base::Error loopback(InterpreterW& self);
base::Error hypperthread(InterpreterW& self);
} // namespace resolve

base::Error resolve(InterpreterW& self){
  int resolver = 0;

  if (self.resolve() >= 0)
    resolver = self.resolve();
  else try{
    if (base::config::get("libnn.resolver") != nullptr)
      resolver = base::config::get<int>("libnn.resolver");

    self.resolve(resolver);
  } catch(base::Error&){ }

  switch (resolver){
  case LOOPBACK_RESOLVE:
  default:
    return resolver::loopback(self);

  case HYPPERTHREAD_RESOLVE:
    return resolver::hypperthread(self);
  }
}

pthread_mutex_t* create_mutex() {
  pthread_mutex_t* result = new pthread_mutex_t;

  if (pthread_mutex_init(result, NULL) != 0)
    throw BadAccess.reason("mutex init failed");
  return result;
}

template<typename... Args>
pthread_t create_pthread(void*(*callback)(void*), Args... args) {
  pthread_t result;

  if (pthread_create(&result, NULL, callback, 
                     new std::tuple<Args...>(args...)) != 0)
    throw BadAccess.reason(strerror(errno));
  return result;
}

void safe(InterpreterW* interpreter, std::function<void()>&& callback){
  interpreter->lock();
  callback();
  interpreter->unlock();
}

void safe(pthread_mutex_t* lock, std::function<void()>&& callback){
  if (lock){
    pthread_mutex_lock(lock);
    callback();
    pthread_mutex_unlock(lock);
  } else 
    callback();
}

void wait(pthread_mutex_t& lock, 
          std::function<bool()> before = nullptr,
          std::function<void()> after = nullptr){
  if (before && !before())
    return;
  pthread_mutex_lock(&lock);
  pthread_mutex_unlock(&lock);
  if (after) after();
}

void wait(InterpreterW* interpreter,
          std::function<bool()> before = nullptr,
          std::function<void()> after = nullptr){
  if (before && !before())
    return;
  interpreter->lock();
  interpreter->unlock();
  if (after) after();
}

bool isLocked(pthread_mutex_t* lock){
  if (lock){
    auto result = pthread_mutex_trylock(lock) != 0;

    if (!result)
      pthread_mutex_unlock(lock);
    return result;
  } else return false;
}

bool isLocked(pthread_mutex_t& lock){ return isLocked(&lock); }
} // namespace internal

using Gates = std::vector<std::pair<std::string, Tensor>>;
using Yield = std::function<void(Gates)>;

bool InterpreterW::isOkey4Loading(std::string&& UNUSED(name), Device device) {
  auto UNUSED(result) = device->_template.inspect(device);

  if (result != *(device->_enable)) *(device->_enable) = result;
  return result;
}

Gates InterpreterW::invoke(Gates inputs) {
  auto result = Gates{};
  auto yield = [&](Gates yielded) { result = yielded; };

  invoke(inputs, yield, false);
  return result;
}

bool InterpreterW::invoke(InterpreterW::Gates inputs, Yield yield) {
  return invoke(inputs, yield, true);
}

bool InterpreterW::invoke(Gates inputs, std::shared_ptr<Collector> collector){
  if (!_collector){
    _collector = collector;
    _collector->lock = &_ilock;
    _collector->interpreter = &_interpreter;
  } else if (_collector != collector){
    return BadLogic.reason("don\'t allow assign more than 1 collector "
                           "to a single interpreter");
  }

  if (_collector != nullptr)
    return invoke(inputs, _collector->yield(), true);
  else
    return invoke(inputs, nullptr, true);
}

bool InterpreterW::invoke(InterpreterW::Gates inputs, Yield yield, 
                          bool streaming) {
  using namespace nn::interpreter;
  using namespace nn::utils::wstring;
  using namespace nn::utils;

  if (!yield)
    yield = _def_yield;
  else if (!_collector)
    _def_yield = yield;

  /* @NOTE: step 0: load placeholder if they aren't loaded into _placeholders */
  if (!_notouched){
    for (auto& item: _operators){
      auto op = std::get<1>(item);
      /* @NOTE: we don't known whether or not the placeholder is apply to 
       * interpreter, so we must remove op->eval after apply placeholers, just to
       * prevent wasting performance */

      if (op->type && op->eval && op->eval(op, &interface())){
        return _error = (BadAccess << op->name << "is underdeveloping");
      } else {
        /* @NOTE: apply this placeholder to all inputs */

        for (auto& item: _operators){
          auto input_op  = std::get<1>(item);
          auto input_opw = nNet_GetOperatorW(std::get<1>(item));

          if (input_op->type)
            continue;

          for (auto& input_item: input_opw->inputs()){
            std::string input_name{""};
            std::size_t index{0};

            wstring::tie(input_name, index) = split(std::get<0>(input_item), ':');

            if (input_name == op->name){
              std::get<1>(input_item) = nNet_GetOperatorW(op)->outputs()[index];
            }
          }
        }
      }
    }
  }

  /* @NOTE: step 1: check if operator is connecting with anothers */
  if (_error) return true;
  else if (!_notouched){
    std::size_t count{0};

    for (auto& item: _operators)
      nNet_GetOperatorW(std::get<1>(item))->isConnected = false;

    for (auto i = 0; i < cast_(i, _flowchart.size()); ++i){
      for (auto& node: std::get<1>(_flowchart[i])){
        auto opw = nNet_GetOperatorW(node);

        for (auto& input_item: opw->inputs()){
          std::string input_name{""};
          std::size_t index{0};

          if (count + 1 == _operators.size())
            break;

          wstring::tie(input_name, index) = split(std::get<0>(input_item), ':');

          for (auto& item: _operators){
            auto input_opw = nNet_GetOperatorW(std::get<1>(item));

            if (input_name != input_opw->name())
              continue;

            if (!input_opw->isConnected()){
              input_opw->isConnected = true;
              count++;
            }
          }
        }
      }
    }

    _notouched = true;
  }

  /* @NOTE: step 2: hook output tensor with devices if it needs */
  _optimized = reconstruct();

  /* @NOTE: select a way for our inputs: push input direct to CGraph or push to 
   * command queue 
   */

  if (streaming)
    request(std::make_pair(inputs, yield), this);
  else if (_queue->finished()){
    if (!_consumers.size()){
      return Consumer{*this, _queue, -1, nullptr, nullptr}.invoke(inputs, yield);
    }

    for (auto& item: _consumers){
      auto consumer = std::get<0>(std::get<1>(item));

      if (consumer->status() == Couple::Wait || 
          consumer->status() == Couple::Green)
        return consumer->invoke(inputs, yield);
    }
  } else {
    return warn(DoNothing.reason("CGraph is on busy now"));
  }
  return NoError;
}

bool InterpreterW::invoke(std::function<void(Gates)> yield) {
  return invoke(InterpreterW::Gates{}, yield);
}

Gates InterpreterW::invoke(){
  auto result = Gates{};
  auto yield = [&](Gates yielded) { result = yielded; };

  invoke(InterpreterW::Gates{}, yield, false);
  return result;
}

void InterpreterW::request(InterpreterW::Task&& task, InterpreterW* redirect){
  auto self = (Thread)pthread_self();

  using namespace nn::interpreter;
  using namespace nn::global;

  if (redirect || _interpreter.manual || interpreters.find(self) != interpreters.end()){
   retry:
    auto require = 0;

    if (_status != Running){
      if (internal::isLocked(&_slock))
        _status = Running;
      else
        internal::safe(&_slock, [&](){ _status = Running; });
    }

    if (!redirect) redirect = this;
    if (!_queue->registed()){
      /* @NOTE: LOOPBACK_RESOLVE requires only single thread because it's just a
       * single loop
       */

      if (resolve() == LOOPBACK_RESOLVE)
        require = 1;
      else {
        /* @TODO: bao nhieu consumer moi du de chay 1 he thong? Theo mac dinh,
         * thuong 1 device chi can 1 consumer de chay nhung co the can nhieu
         * hon. Nen can nhac ki ve so luong o thoi diem nay
         */
        require = cast_(require, _devices.size());
      }
      redirect->error(_queue->master(&_qlock, require));
    }

    redirect->error(_queue->pin(std::get<0>(task), std::get<1>(task)));

    if (!_error && _consumers.size() == 0){
      for (auto i = 0; i < require; ++i)
        makeConsumer(redirect, false, i);
    } 
  } else {
    /* @NOTE: allow interpreter to automatically redirect to itself if it can't 
     * not find any Interpreter at this thread
     */

    try{
      nn::InterpreterW::instance().request(rvalue(task), this);
    } catch(base::Error&){ goto retry; }
  }
}

void InterpreterW::makeConsumer(InterpreterW* redirect, bool wait, int index){
  using namespace nn::interpreter;
  using namespace nn::global;
  auto self   = (Thread)pthread_self();
  auto lock   = internal::create_mutex();
  auto status = new bool(true);

  base::Boundary  bound{ [&](){ pthread_mutex_lock(lock); },
                         [&](){ if (!wait) pthread_mutex_unlock(lock); }};

  auto consumer = std::make_shared<Consumer>(*this, _queue, index, lock, status);
  if (consumer->self() == std::string::npos){
    redirect->error(DrainMem.reason("consumer can\'t build a new thread"));
  } else if (consumer->self() == self){
    redirect->error(BadLogic << "consumer can\'t run on the same thread"
                             << "with interpreter");
  } else {
    _consumers[consumer->self()] = std::make_tuple(consumer, lock, status);
  }
}

void InterpreterW::waitUtilEnd(){
  if (status() != Stop){
    bool keep{false}, retrigger{false}, flushing{false}, planed{false},
         first{true};
    size_t count_down{_consumers.size()};

    for (auto& item: _consumers){
      pthread_mutex_lock(std::get<1>(std::get<1>(item)));
    }

    _status = InterpreterW::Flushing;
    for (auto& item: _consumers){
      pthread_mutex_unlock(std::get<1>(std::get<1>(item)));
    }

    if (!_queue->flush(&_qlock)){
      planed = true;

      internal::wait(_qlock,
      [&]() -> bool{
        if (_queue->connected() && _queue->backlog() != 0){
          flushing = true;
          bool result{error()};

          /* @NOTE: use this loop to make sure consumers will switch to use 
           * global lock
           */

          if (result) return false;

         #if DEBUG_MUTIL_THREAD
          printf("waiting\n");
          fflush(stdout);
         #endif
          do {
            int index;

            index = 1;
            keep  = false;
            for (auto& item: _consumers){
              auto& consumer = std::get<0>(std::get<1>(item));

              internal::safe(std::get<1>(std::get<1>(item)), [&](){
                if (consumer->status() == Wait){
                  if (!internal::isLocked(_queue->channel(index))){
                    /* @NOTE: it's on going to be locked */
                    keep       = true;
                    count_down = _consumers.size();
                  } else if (_queue->backlog() > 0){
                    keep = true;

                    /* @NOTE: in somecases consumers will run too fast and 
                     * missing using our global lock 
                     */
                    if (internal::isLocked(_queue->channel(index)))
                      pthread_mutex_unlock(_queue->channel(index));
                  } else if (count_down > 0){
                    count_down--;
                    keep = true;
                  }
                  retrigger = true;
                } else if (!internal::isLocked(_queue->channel(index))){
                  /* @NOTE: consumers wait on their local locks */
                  keep   = true;
                  result = false;
                }

                if (first){
                  /* @NOTE: only perform flushing on consumers at the first
                   * doing it many time will cause disturbance our consumers*/
                  consumer->flush();
                }
              });

              index++;
            }

            first = false;
          } while(keep);

          if (!result && _queue->backlog())
            result = true;

         #if DEBUG_MUTIL_THREAD
          printf("done: %s\n", result? "keep": "do nothing");
          fflush(stdout);
         #endif

          /* @NOTE: now our consumers will use our global lock it's the time
           * to wait our consumers finish its task
           */
          if (result)
            _queue->retrigger(retrigger);

          return result;
        } else {
          pthread_mutex_unlock(_queue->global());
          return false;
        }
      }, 
      [&](){
        if (internal::isLocked(_queue->global()))
          pthread_mutex_unlock(_queue->global());
       #if DEBUG_MUTIL_THREAD
        printf("flushing waitUtilEnd\n");
        fflush(stdout);
       #endif
      });
    }

    if (!flushing){
      do {
        auto index = 1;

        /* @NOTE: use this loop to make sure consumers will finish their
         * tasks completedly
         */

        keep = false;
        for (auto& item: _consumers){
          auto& consumer = std::get<0>(std::get<1>(item));

          internal::safe(std::get<1>(std::get<1>(item)), [&](){
            if (consumer->status() == Wait){
              if (!internal::isLocked(_queue->channel(index))){
                /* @NOTE: it's on going to be locked */

                keep       = true;
                count_down = _consumers.size();
              } else if (_queue->backlog() > 0){
                keep       = true;
                count_down = _consumers.size();
              } else if (count_down > 0){
                /* @NOTE: keep track the last one since sometime it can't 
                 * switch perfectly
                 */

                keep = true;
                count_down--;
              }
            } else if (planed && (!internal::isLocked(_queue->global()))){
                /* @NOTE: keep track the last one since sometime it can't 
                 * switch perfectly
                 */

              keep = true;
            }

            if (!planed && first)
              consumer->flush();
          });

          index++;
        }

        first = false;
      } while(keep);
    }

    for (auto& item: _consumers)
      pthread_mutex_lock(std::get<1>(std::get<1>(item)));

    _status = InterpreterW::Running;

   #if DEBUG_MUTIL_THREAD
    printf("finish waitUtilEnd\n");
    fflush(stdout);
   #endif
    for (auto& item: _consumers)
      pthread_mutex_unlock(std::get<1>(std::get<1>(item)));
  }
}

void InterpreterW::tearDownGraph(){
  auto thread_id = (Thread)pthread_self();
  _queue->reset();

  for (auto& item: _consumers){
    internal::safe(std::get<1>(std::get<1>(item)), [&](){
      *std::get<2>(std::get<1>(item)) = false;
    });
  }
  
  for (auto& item: _consumers){
    if (std::get<0>(item) == thread_id)
      return;
  }

  waitUtilEnd();
}

Consumer::Consumer(InterpreterW& interpreter,
                   std::shared_ptr<interpreter::Queue>& queue,
                   int index, pthread_mutex_t* lock, bool* flag):
    Couple(interpreter.self()), _index{index + 1},
    _provider{&interpreter.interface()}, _ilock{lock}, _olock{nullptr}{
  using namespace nn::interpreter;
  using namespace nn::internal;

  using Yield = std::function<void(Gates)>;
  using Arguments = std::tuple<Consumer*, Queue*, InterpreterW*, 
                               pthread_mutex_t*, int, bool*>;

  auto provider = nNet_GetInterpreterW(_provider);
  if (lock && flag && index >= 0){
    auto consumer = [](void* pointer) -> void*{
      auto args  = reinterpret_cast<Arguments*>(pointer);
      auto error = NoError;

      Gates inputs;
      Yield yield;

      auto thiz     = std::get<0>(*args);
      auto queue    = std::get<1>(*args);
      auto provider = std::get<2>(*args);
      auto lock     = std::get<3>(*args);
      auto index    = std::get<4>(*args);
      auto keep     = std::get<5>(*args);

      /* @NOTE: wait until Consumer is registed to Interpreter */
      internal::wait(*lock);
      if (!(error = thiz->init(*provider, *queue, index))){
        if (thiz->status() == Couple::Gray)
          goto finish;
  
        while (true){
          /* @NOTE: wait until fetching a new task */
          pthread_mutex_lock(lock);

          if (!*keep && thiz->error()){
            pthread_mutex_unlock(lock);
            break;
          }

          error = thiz->fetch(inputs, yield);
          if (error || error.code == base::error::EOutOfRange){
           waiting:
            /* @NOTE: goto waiting state, all consumers will become stuck now
             * before a new task has been added 
             */

           #if DEBUG_MUTIL_THREAD
            printf("empty ");
            fflush(stdout);
           #endif
            thiz->wait();
            continue;
          } else if (error.code == base::error::EDoNothing){
            /* @NOTE: backlog is on flushing, provider must be on waiting to 
             * unlock 
             */

            error = NoError;
            if (*keep){
             #if DEBUG_MUTIL_THREAD
              printf("flushing ");
              fflush(stdout);
             #endif
              thiz->wait();
              continue;
            } else
              goto finish;
          } else if (error.code == base::error::EKeepContinue){
            sleep(0);
          } else {
            /* @NOTE: okey, we have a new request, now run this neural network
             * and earn outcome.
             */
            auto status = (*keep = !thiz->invoke(inputs, yield));

            if ((error = thiz->done(inputs))){
              *keep = false;
            }
              
            if (!*keep){
              *keep = !thiz->finished();

              if (*keep && status)
                goto waiting;
              else {
                *keep = false;
              }
            }
          }
          pthread_mutex_unlock(lock);
        }

        if (error && !thiz->error()){
          thiz->error(error);
        } else {
          provider->tearDownGraph();
        }
      } else {
        thiz->error(error);
      }

     finish:
      thiz->disconnect();
      delete args;
      return nullptr;
    };

    _self = (Thread)create_pthread(consumer, this, queue.get(), provider, lock, 
                                   interpreter.consumers().size() + 1,
                                   flag);
  }
  _status = Couple::Wait;
}

Consumer::~Consumer(){ }

bool Consumer::disconnect(){
  return _queue->disconnect();
}

bool Consumer::wait(){
  auto lock = _queue->channel(_index);

  _status = Wait;
  if ((Thread)pthread_self() != _self)
    return false;

  internal::wait(*lock, [&]() -> bool{
    if (_flushing && lock == _olock){
     #if DEBUG_MUTIL_THREAD
      printf("unlock flushing\n");
      fflush(stdout);
     #endif
      _flushing = false;
    }

    if (internal::isLocked(_ilock))
      pthread_mutex_unlock(_ilock);
   #if DEBUG_MUTIL_THREAD
    printf("wait\n");
    fflush(stdout);
   #endif
    return true;
  }, [&](){
    _status = Green;
  });

  return true;
}

void Consumer::flush(){
  /* @NOTE: set flag _flushing on, we will clear it after finish flushing task
   */
  _flushing = true;
}

bool Consumer::error(){
  auto interpreterw = nNet_GetInterpreterW(_provider);

  if (interpreterw->status() == nn::InterpreterW::Pause){
    auto result = false;

    internal::safe(interpreterw, [&](){
      result = interpreterw->error();
    });
    return result;
  } else
    return interpreterw->error();
}

bool Consumer::error(base::Error& error){
  auto interpreterw = nNet_GetInterpreterW(_provider);

  if (interpreterw->status() == nn::InterpreterW::Running){
    auto result = false;

    internal::safe(interpreterw, [&](){
      result = interpreterw->error(error);
    });
    return result;
  } else
    return interpreterw->error(error);
}

base::Error Consumer::init(InterpreterW& interpreter, 
                           interpreter::Queue& src,
                           int index){
  auto self = (Thread)pthread_self();

  using namespace nn::interpreter;

  _provider = &(interpreter.interface());
  _remote   = interpreter.self();
  _status   = src.reseted()? Couple::Gray: Couple::Green;

  if (self == _remote){
    return BadLogic << "Consumer mustn\'t be inited in remote Thread("
                    << std::to_string(_remote) << ")";
  }

  if (_status == Couple::Green){
    _queue->connect(src, index);
    _olock = _queue->channel(index);
  }

  return NoError;
}

base::Error Consumer::fetch(Gates &inputs, Yield& yield){ 
  auto result = _queue->get(inputs, yield);

  if (result || result.code == base::error::EOutOfRange)
    _status = Wait;
  return result;
}

base::Error Consumer::done(Gates &inputs){ return _queue->fin(inputs); }

bool Consumer::finished(){ return _queue->finished(); }

bool Consumer::invoke(Gates inputs, std::function<void(Gates)> yield){
  typedef void (*Release)(struct TensorDef *self, Interpreter interpreter);

  auto provider = nNet_GetInterpreterW(_provider);
  auto savers   = std::vector<std::pair<bool, Release>>{};

    /* @NOTE: step 1: put inputs into placeholders */
  if (provider->error()) return true;
  for (auto& input: inputs){
    auto& name = std::get<0>(input);

    savers.push_back(std::make_pair(false, Release(nullptr)));

    if (provider->placeholders().find(name) == provider->placeholders().end()){
      warn((NotFound << name << "didn't exit and will be release to preserve"
                                "memory"));
      nNet_RemoveTensorW(_provider, std::get<1>(input));
    } else {
      auto shape = nNet_CreateTensorShapeC1(_provider, 0);
      auto dst   = provider->findPlaceholder(name);
      auto src   = std::get<1>(input);

      /* @NOTE: check input type with placeholders */
      std::get<1>(savers.back()) = dst->release;

      if (nNet_GetTensorNumT(dst) != Unknown){
        if (nNet_GetTensorNumT(src) != nNet_GetTensorNumT(dst)){
          provider->error(BadLogic << name << "require input with type" 
                                   << nNet_NameType(nNet_GetTensorNumT(dst)));
          break;
        }
      } else {
        std::get<0>(savers.back()) = true;
      }

      /* @NOTE: replace placeholder with dst->type */
      if (!nNet_ClearTensorW(_provider, dst)){
        return provider->error(BadAccess << "nNet_ClearTensorW()");
      }

      if (!nNet_SetTensorNumTW(_provider, dst, nNet_GetTensorNumT(src))){
        return provider->error(BadAccess << "nNet_SetTensorNumTW()");
      }

      /* @NOTE: scalar would be an exception */
      if (nNet_GetTensorType(src) != ScalarT){
        if (!nNet_GetTensorShapeW(_provider, src, shape)){
          return provider->error(BadAccess << "nNet_GetTensorShape()");
        }
        if (!nNet_SetTensorShapeW(_provider, dst, shape)){
          return provider->error(BadAccess << "nNet_SetTensorShapeW()");
        }
      }

      /* @NOTE: welldone, remove shape to preserve memory and put data to 
       * placeholder */
      if (nNet_RemoveTensorShapeW(_provider, shape)){
        return provider->error(BadAccess << "nNet_RemoveTensorShapeW()");
      }

      if (!nNet_SetArrayData(dst, nNet_GetArrayData(src))){
        return provider->error(BadAccess << "nNet_SetArrayData()");
      } else {
        /* @NOTE: just to prevent double free, we must set dst->release 
         * to remove only information about structure of tensor */
 
        if (dst->_content == src->_content)
          dst->release = JustRemoveTensorDims;
      }
    }
  }

  /* @NOTE step 2: everything is done, now perform the resolving algorithm which
   * is set before */
  if (!(provider->error(internal::resolve(*provider)))){
    Gates result{};

    /* @NOTE: step 3: CGraph has finished its tasks, now we need gather outcomes
     * from unconnected operators
     */

    for (auto& item: provider->operators()){
      auto opw = nNet_GetOperatorW(std::get<1>(item));

      if (opw->isConnected())
        continue;

      for (auto i = 0; i < cast_(i, opw->outputs().size()); ++i){
        result.push_back(std::make_pair(opw->name() + ":" + std::to_string(i),
                                        opw->outputs()[i]));
      }
    }

    if (yield){
      yield(result); // <-- callback will be used to transport result
    } else {
      provider->error(warn((DoNothing << "callback \'yield\' is null,"
                                      << "so you can\'t gather result"
                                      << "automatically")));
    }
  }

  /* @NOTE: step 3: clear placeholders */
  for (auto i = 0; i < cast_(i, inputs.size()); ++i){
    auto placeholder = provider->findPlaceholder(std::get<0>(inputs[i]));

    /* @NOTE: reapply placeholder->release back from buffer */
    if (!placeholder) continue;
    else if (std::get<0>(savers[i])){
      /* @NOTE: default placeholder use Unknown type, so cleaning tensor
       * is enough for this case */
 
      if (!nNet_ClearTensorW(_provider, placeholder)){
        return provider->error(BadAccess << "nNet_ClearTensorW()");
      }
    } else {
      auto type = nNet_GetTensorNumT(placeholder);

      /* @NOTE: placeholder with specific type, must clear first and then
       * re-establish again  */
 
      if (!nNet_ClearTensorW(_provider, placeholder)){
        return provider->error(BadAccess << "nNet_ClearTensorW()");
      } else {
        /* @NOTE: this is the tricky way to improve performance while 
	       * keeping the same behavious with nNet_SetTensorNumTW() 
	       */
	      placeholder->type    = type;
        placeholder->release = std::get<1>(savers[i]);
      }
    }
  }
  return provider->error();
}
} // namespace nn

base::Error nn::internal::resolver::loopback(InterpreterW& self){
  /* @NOTE: loopback is the easiest resolver method, it just scan throught out
   * flowchart from left-top to right-down */
  if (self.operators().size() == 0)
    return DoNothing;

  for (auto& requests: self.flow()){
    /* @NOTE: perform parallel tasks with just using a single loop */

    for (auto& op: std::get<1>(requests)){
      if (!op->type){
        if (!op->eval){
          return self.error(BadLogic << op->name 
                                     << "\'s eval is underdeveloping"); 
        } else if (op->eval(op, &self.interface())){
          return self.error();
        }
      }
    }
  }

  return NoError;
}

base::Error nn::internal::resolver::hypperthread(InterpreterW& self){
  /* @NOTE: hypperthread is design to be used like a */

  return NoSupport;
}
