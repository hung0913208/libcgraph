#include <iterator>
#include <memory>
#include <fstream>

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

#define UNKNOWN_T -1
#define SHAPE_T    0
#define ORDINATE_T 1
#define TENSOR_T   2
#define OPERATOR_T 3

namespace nn{
using PInterpreterW  = InterpreterW*;
using Interpreters = std::vector<Interpreter>;

namespace global{
extern std::map<Thread, Interpreters> topologies;
extern std::map<Thread, PInterpreterW>  interpreters;
extern std::map<std::string, Device>    devices;
extern std::map<std::string, Operator>  macros;
} // namespace global

namespace internal{
Device remove(Device device);
void safe(InterpreterW* interpreter, std::function<void()>&& callback);
void safe(pthread_mutex_t* lock, std::function<void()>&& callback);
bool isLocked(pthread_mutex_t* lock);
} // namespace internal

namespace interpreter{
Couple::Couple(Thread remote){
  _remote = remote;
  _self   = (Thread) pthread_self();
  _queue  = std::make_shared<Queue>();
}
} // namespace interpreter

InterpreterW::InterpreterW(std::fstream&& stream, bool force, bool lazyload):
    Couple{(Thread)pthread_self()},
    _status{InterpreterW::Stop}, _index{std::string::npos},
    _resolver{std::string::npos},
    _openspace{false}{
  auto ok = false;

  _interpreter.priv   = this;
  _interpreter.master = (Thread) pthread_self();
  _interpreter.manual = force;
  _interpreter.sysdef = nullptr;

  if (stream.is_open())
    ok = init(std::string(std::istreambuf_iterator<char>(stream), {}), lazyload);
  else ok = init("");
  if (!ok)
    _error = _error? _error: BadAccess;
  else
    _status = Initing;
}

InterpreterW::InterpreterW(std::fstream& stream, bool force, bool lazyload):
    Couple{(Thread)pthread_self()},
    _status{InterpreterW::Stop}, _index{std::string::npos},
    _resolver{std::string::npos},
    _openspace{false}{
  auto ok = false;

  _interpreter.priv   = this;
  _interpreter.master = (Thread) pthread_self();
  _interpreter.manual = force;
  _interpreter.sysdef = nullptr;

  if (stream.is_open())
    ok = init(std::string(std::istreambuf_iterator<char>(stream), {}), lazyload);
  else {
    warn(BadAccess).reason("std::fstream isn't blank or didn't open afterall");
    ok = init("");
  }
  if (!ok)
    _error = _error? _error: BadAccess;
  else
    _status = Initing;
}

InterpreterW::InterpreterW(std::string&& graph, bool force, bool lazyload):
    Couple{(Thread)pthread_self()},
    _status{InterpreterW::Stop}, _index{std::string::npos},
    _resolver{std::string::npos},
    _openspace{false}{
  _interpreter.priv   = this;
  _interpreter.master = (Thread) pthread_self();
  _interpreter.manual = force;
  _interpreter.sysdef = nullptr;

  if (!init(rvalue(graph), lazyload))
    _error = _error? _error: BadAccess;
  else
    _status = Initing;
}

bool InterpreterW::init(std::string&& graph, bool lazyload){
  auto allowed = false;
  auto thread  = (Thread) pthread_self();

  pthread_mutex_init(&_mlock, nullptr);
  pthread_mutex_init(&_ilock, nullptr);
  pthread_mutex_init(&_slock, nullptr);
  pthread_mutex_init(&_qlock, nullptr);

  _status    = Initing;
  _optimized = false;
  _notouched = false;

  if (global::topologies.size() == 0) nNet_LoadEverything();
  if (global::topologies.find(thread) == global::topologies.end())
    global::topologies[thread] = std::vector<Interpreter>{};

  if (global::interpreters.find(thread) == global::interpreters.end() ||
      _interpreter.manual){
    allowed = true;

    /* @NOTE: check if this thread has been owned by some other interpreter, if
     * this is true, this thread is allowed to be a master of interpreters */
    if (!_interpreter.manual){
      for (auto& interpreter: global::interpreters){
        if (!std::get<1>(interpreter)->own(thread))
          continue;

        _interpreter.master = std::get<0>(interpreter);
        allowed = false;
        break;
      }
    }

    /* @NOTE: this interpreter is pass checking, now we must generate
     * interpreter's device from global devices */
    if (allowed){
      for (auto& item: global::devices){
        auto& template_ = std::get<1>(item);

        while (true){
          if (template_->_enable && *(template_->_enable)){
            auto device = template_;

            if (template_->_template.create)
              device = template_->_template.create(device, &interface());

            if (!device) break;
            else if (device != template_){
              device->_template.create = nullptr;

              /* @NOTE: we must avoid mixing global device with local device
               * if it has been initialed by kernel but forgotten clearing */
              if (device->_priv != template_->_priv)
                device->_enable = template_->_enable;
            }
            _devices[std::get<0>(item)] = device;
          } else if (template_->_template.inspect(template_)){
            /* @NOTE: device has changed and allows our device to be used */

            if (!template_->_enable)
              template_->_enable = (bool*) calloc(1, sizeof(bool));

            *(template_->_enable) = true;
            continue;
          } 
          break;
        }
      }

      global::interpreters[thread] = this;
      global::topologies[thread].push_back(&interface());
    }
  } else {
    _remote  = InterpreterW::instance().self();
    _devices = InterpreterW::instance().devices();
  }

  return (graph.size() == 0) || transcript(rvalue(graph), lazyload);
}

InterpreterW::~InterpreterW(){
  auto thread = (Thread) pthread_self();;

  /* @NOTE: make sure everything will be done before destroy everything */
  if (_openspace) finish();
  else if (_gcmapping.size() && _gcvector.size()){    
    /* @NOTE: we must clear everything before remove device to prevent
     * SEGMENT_FAULT */

    clear();
  }

  pthread_mutex_destroy(&_slock);
  pthread_mutex_destroy(&_ilock);
  pthread_mutex_destroy(&_qlock);
  pthread_mutex_destroy(&_mlock);

  /* @NOTE: check if this interpreter is a master, and clear it out of global */
  if (global::interpreters.find(thread) != global::interpreters.end()){
    if (global::interpreters[thread] == this)
      global::interpreters.erase(thread);
  }

  /* @NOTE: use global callbacks to release interpreter's device */
  for (auto& context : _devices){
    auto name   = std::get<0>(context);
    auto device = std::get<1>(context);

    if (global::devices.find(name) != global::devices.end()){
      /* @NOTE: in somecase, global device also use with local device, to 
       * keep it safe, we must prevent cleaning this, everything will be clean 
       * at the end of the program, after calling nNet_UnloadEverything */

      if (global::devices[name] != device){
        /* @NOTE: there are two options or using callback 'release' or clearing
         * manually by using free() */

        if (global::devices[name]->_template.release)
          device = global::devices[name]->_template.release(device);

        if (device) free(device);
      } else if (device->detach)
        device->detach(&_interpreter, device);
    } else if (device){
      device->_template.release(device);
    }
  }
   global::topologies.erase(thread);
}

void InterpreterW::operator()(std::function<void(InterpreterW&)> session){
  if (sysbase() == nullptr)
    _error = DoNothing;
  else if (!_interpreter.manual)
    (*this)([&](){ session(*this); });
  else {
    start();
    session(*this);
    finish();
  }
}

void InterpreterW::operator()(std::function<void()> session){
  auto thread = (Thread) pthread_self();;
  using namespace global;

  if (_interpreter.manual) _error = NoSupport;
  else if (sysbase() == nullptr)
    _error = DoNothing;
  else if (!_error){
    base::Boundary boundary{
      [&](){
        start();
        if (topologies.find(thread) == topologies.end())
          topologies[thread] = std::vector<Interpreter>{};
        _index = topologies[thread].size();

        topologies[thread].push_back(&interface());
      },
      [&](){
        finish();
      }
    };
    session();
  }
}

base::Error InterpreterW::start(){
  Status status;

  internal::safe(&_mlock, [&](){ status = _status; });
  if (this->status() == Stop){
    /* @NOTE: this status only happen when a interpreter finish flushing. At 
     * this state, all consumers are completely closed but we still don't have
     * a chance to call pthread_join, so we must call them now to prevent 
     * memory-leak 
     */
    void* res{nullptr};

    for (auto& item: _consumers){
      auto thread   = std::get<0>(item);
      auto consumer = std::get<0>(std::get<1>(item));
      auto lock     = std::get<1>(std::get<1>(item));

      if (consumer->status() != Couple::Gray){
        if (internal::isLocked(lock))
          pthread_mutex_unlock(lock);
      }

      pthread_join((pthread_t)thread, &res);
      pthread_detach((pthread_t)thread);
      delete lock;
    }

    pthread_mutex_unlock(&_qlock);
    _consumers.clear();
    _queue->disconnect();
  } else if (_openspace){
    return warn((DoNothing << "double call start()," 
                          << "you must call method finish() before call"
                          << "start() again"));
  }

  internal::safe(&_mlock, [&](){
    _openspace = true;
    _status    = Running;

    pthread_mutex_lock(&_qlock);
  });
  return NoError;
}

base::Error InterpreterW::finish(){
  using namespace nn::internal;

  auto res = (void*) nullptr;

  if (!_openspace){
    return warn((DoNothing << "you must call method start()"
                           << "before call finish()"));
  } else {
    if (_queue->connected())
      waitUtilEnd();
    base::Boundary bound{[&](){ safe(&_mlock, [&](){ _status = Pause; }); }, 
                         [&](){ safe(&_mlock, [&](){ _status = Stop; }); }};

    for (auto& item: _consumers){
      auto& lock = std::get<1>(std::get<1>(item));
      auto& flag = std::get<2>(std::get<1>(item));

      if (lock && flag){
        /* @NOTE: force consumer to close now */

        pthread_mutex_lock(lock);
        *flag = false;
        pthread_mutex_unlock(lock);
      }
    }


   #if DEBUG_MUTIL_THREAD
    printf("_queue->exit()\n");
    fflush(stdout);
   #endif

    _queue->exit();
    for (auto& item: _consumers){
      auto& thread = std::get<0>(item);
      auto& lock   = std::get<1>(std::get<1>(item));
      auto& flag   = std::get<2>(std::get<1>(item));

      if (lock && flag){
        /* @NOTE: force consumer to close now */

       #if DEBUG_MUTIL_THREAD
        printf("pthread_join\n");
        fflush(stdout);
       #endif

        pthread_join((pthread_t)thread, &res);
        pthread_detach((pthread_t)thread);
        if (lock)
          delete lock;

        if (flag)
          delete flag;
      }
    }

    unlock();
    clear();
    _consumers.clear();
    _openspace = false;
  }

  return NoError;
}

InterpreterW& InterpreterW::instance(){
  auto thread = (Thread) pthread_self();;
  using namespace global;

  if (interpreters.find(thread) == interpreters.end()){
    /* @NOTE: check if it's a consumer so we can return its master */

    for (auto& master: interpreters){
      if (std::get<1>(master)->own(thread))
        return *std::get<1>(master);
    }
    throw BadLogic;
  } else if (interpreters[thread] == nullptr)
    throw BadLogic;
  return *(interpreters[thread]);
}

InterpreterW& InterpreterW::gcollector(){
  auto thread = (Thread) pthread_self();;
  using namespace global;

  if (topologies.find(thread) == topologies.end()){
    /* @NOTE: check if it's a consumer so we can return its master */

    for (auto& topo: topologies){
      if (nNet_GetInterpreterW(std::get<1>(topo).back())->own(thread))
        return *(nNet_GetInterpreterW(std::get<1>(topo).back()));
    }
    throw BadLogic;
  } else if (topologies[thread].size() == 0)
    throw OutOfRange;
  return *(nNet_GetInterpreterW(topologies[thread].back()));
}

base::Error InterpreterW::install(std::string name, Device device){
  if (global::interpreters.size() == 0){
    /* @NOTE: if global::interpreters.size() == 0, it mean we intend to install
     * device to global::devices */

    if (global::devices.find(name) != global::devices.end())
      return BadAccess.reason("module " + name + " has been installed");

    global::devices[name] = device;
  } else {
    /* @NOTE: happen after an interpreter is created, every new device will
     * be custom devices and they will be install directly into the current
     * interpreter */

    if (instance()._devices.find(name) != instance()._devices.end())
      return BadAccess;

    instance()._devices[name] = device;
  }
  return NoError;
}

TensorDef* InterpreterW::hook(std::string module, Tensor tensor){
  if (_devices.find(module) != _devices.end())
    return hook(tensor, _devices[module]);

  return nn::utils::null<Tensor>(&interface(),                                 \
    NotFound.reason("module " + module + " didn't exist or it was disabled"));
}

TensorDef* InterpreterW::hook(Operator op, Tensor tensor){
  auto name = findOperator(op);
  auto dev  = findDevice(op);

  if (!dev){ /* @NOTE: this operator isn't created from a template */
    dev = op->device;
  }

  if (!name || !dev || operators().find(name) == operators().end())
    return nn::utils::clearFaulty(&interface(), tensor,                        \
      NotFound.reason("op " + std::string{op->name} + " didn't exist"));
  else
    return hook(tensor, dev);
}

TensorDef* InterpreterW::hook(TensorDef* tensor){ return hook(tensor, nullptr); }

TensorDef* InterpreterW::hook(TensorDef* tensor, Device device){
  /* @NOTE: this function doesn't check input because it need to keep performance
   * so, make sure everything before use it. By default, i limit it inside 
   * private, just to keep it work well.
   */

  if (!_openspace && _status != Initing)
    return nNet_RemoveTensorW(&_interpreter, tensor);

  if (!device){
    if (nNet_GetTensorType(tensor) != NilT){
      info(BadAccess).reason("device is null, apply default default");
    }
    device = sysbase();
  }

  if (device){
    if (device == sysbase() && tensor->_device != sysbase() && tensor->_device){
      /* @NOTE: move data back to generic device just to optimize performance */
      auto array  = nNet_GetArrayData(tensor);
      auto size   = nNet_GetArraySizeT(tensor);
      auto type   = nNet_GetTensorNumT(tensor);
      auto ndims  = tensor->ndims;
      auto dims   = tensor->dims;

      if (tensor->_begins && tensor->_ends){
        /* @NOTE: we can't hook a ref-tensor back to generic device because it 
         * will break ref-link between tensors, push a warning about that
         */
        nNet_TryDropingArray(tensor, array);
        error(warn(DoNothing)
              << "don\'t support hook this ref-tensor back to generic");
        return tensor;
      } else
        tensor->_device = device;

      /* @NOTE: a tricky way to prevent realloc again, just improve performance 
       * because hook might be called many times 
       */
      tensor->dims = nullptr;
      nNet_ClearTensorW(&interface(), tensor);

      tensor->_content = array;
      tensor->_size    = size;

      tensor->ndims = ndims;
      tensor->dims  = dims;
      tensor->type  = type;

      tensor->_array.get = nullptr;
      tensor->_array.set = nullptr;

      tensor->init    = nullptr;
      tensor->diff    = nullptr;
      tensor->get     = nullptr;
      tensor->set     = nullptr;
      tensor->reshape = nullptr;
      tensor->release = nullptr;

      nNet_SetArrayData(tensor, array);
    } else if (device->tensor){
      if (tensor->_device != device){
        tensor->_device = device;
        tensor = device->tensor(&interface(), tensor);
      }
    } else {
      tensor->_device = device;

      if (device != sysbase()){
        return utils::clearFaulty(&interface(), tensor,
                   DoNothing.reason("your device must be generic, isn\'t it?"));
      }

      tensor->_array.get = nullptr;
      tensor->_array.set = nullptr;

      tensor->init    = nullptr;
      tensor->diff    = nullptr;
      tensor->get     = nullptr;
      tensor->set     = nullptr;
      tensor->reshape = nullptr;
      tensor->release = nullptr;
    }
    return tensor;
  }
  return nullptr;
}

void InterpreterW::clear(){
  auto thread = (Thread)pthread_self();
  using namespace global;

  for (auto it = _gcvector.begin(); it != _gcvector.end(); ++it) {
    auto pointer = *it;
    auto type    = UNKNOWN_T;

    if (_gcmapping.find(pointer) != _gcmapping.end())
      type = std::get<0>(_gcmapping[pointer]);

    switch (type) {
    case SHAPE_T:
      nNet_RemoveTensorShapeW(&interface(), (Shape)pointer);
      break;

    case ORDINATE_T:
      nNet_RemoveOrdinateW(&interface(), (Ordinate)pointer);
      break;

    case TENSOR_T:
      nNet_RemoveTensorW(&interface(), (Tensor)pointer);
      break;

    case OPERATOR_T:
      nNet_RemoveOperatorW(&interface(), (Operator)pointer);
      break;

    case UNKNOWN_T:
      break;

    default:
      free(pointer);
      break;
    }
  }

  if (_index != std::string::npos) {
    topologies[thread].erase(topologies[thread].begin() + _index);
    _index = std::string::npos;
  }
  _gcmapping.clear();
  _gcvector.clear();
}

bool InterpreterW::assignPointer(void* pointer, int type, bool dummy){
  bool result = false;

  internal::safe(&_mlock, [&](){
    if (_gcmapping.find(pointer) != _gcmapping.end()){
      if (std::get<0>(_gcmapping[pointer]) != UNKNOWN_T){
        return;
      }
    }

    _gcmapping[pointer] = std::make_tuple(type, _gcvector.size(), dummy);
    _gcvector.push_back(pointer);
    result = true;
  });
  return result;
}

bool InterpreterW::assignOperator(Operator op, bool* &link){
  link = &_notouched;
  return assignPointer(op, OPERATOR_T);
}

bool InterpreterW::assignOperator(std::string name, Operator op){
  if (op->name == nullptr)
    return BadLogic.reason("don\'t support append operator's templates");
  else if (assignPointer(op, OPERATOR_T) ||
           _operators.find(name) == _operators.end()){
    _operators[name] = op;
    return true;
  }
  return false;
}

bool InterpreterW::assignDevice(std::string name, Device device){
  if (_devices.find(name) != _devices.end())
    return false;
  if (!device->_enable)
    device->_enable = (bool*) calloc(1, sizeof(bool));

  if (!(*(device->_enable) = device->_template.inspect(device)))
    return false;
  _devices[name] = device;
  _optimized = false;
  return true;
}

bool InterpreterW::assignPlaceholder(std::string name, Tensor holder, 
                                    bool variance){
  if (_placeholders.find(name) != _placeholders.end())
    return false;

  holder->_const = !variance;
  _placeholders[name] = holder;
  return true;
}

bool InterpreterW::isDummyPointer(Pointer pointer){
  if (_gcmapping.find(pointer) == _gcmapping.end())
    throw NotFound;
  return (std::get<2>(_gcmapping[pointer]));
}

void InterpreterW::removePointer(void* pointer){
  if (std::get<0>(_gcmapping[pointer]) == OPERATOR_T)
    _notouched = false;
  std::get<0>(_gcmapping[pointer]) = UNKNOWN_T;
}

bool InterpreterW::removeOperator(std::string name){
  auto op = findOperator(name);

  if (op && !nNet_IsTemplateOperatorW(&_interpreter, op)){
    _operators.erase(name);
    removePointer(op);
    return true;
  }
  return false;
}

bool InterpreterW::removeDevice(Device device){
  for (auto& item: _devices){
    if (std::get<1>(item) == device)
      return removeDevice(std::get<0>(item));
  }
  return false;
}

bool InterpreterW::removePlaceholder(std::string name){
  if (_placeholders.find(name) != _placeholders.end()){
    if (nNet_RemoveTensorW(&_interpreter, _placeholders[name]))
      return false;
  }
  return true;
}
bool InterpreterW::removeDevice(std::string name){
  if (_devices.find(name) == _devices.end())
    return false;
  _devices.erase(name);
  return true;
}

bool InterpreterW::findPointer(void* pointer, int type){
  if (_gcmapping.find(pointer) == _gcmapping.end())
    return false;

  return std::get<0>(_gcmapping[pointer]) == type;
}

Device InterpreterW::findDevice(std::string name){
  if (_devices.find(name) != _devices.end())
    return _devices[name];
  return nullptr;
}

Device InterpreterW::findDevice(Operator op){
  using namespace nn::interpreter;

  if (!op) return nullptr;
  for (auto& item: _devices){
    auto device = reinterpret_cast<DeviceW*>(std::get<1>(item)->_priv);

    if (device){
      for (auto& item: device->operators()){
        if (std::get<1>(item) == op)
          return device->interface();
      }
    }
  }
  return nullptr;
}

const char* InterpreterW::findOperator(Operator op){
  using namespace nn::interpreter;

  if (findPointer(op, OPERATOR_T)){
    return op->name;
  }

  for (auto& item: _devices){
    auto device = reinterpret_cast<DeviceW*>(std::get<1>(item)->_priv);

    if (device){
      for (auto& item: device->operators()){
        if (std::get<1>(item) == op)
          return std::get<0>(item).c_str();
      }
    }
  }
  return nullptr;
}

Operator InterpreterW::findOperator(std::string name, bool only_graph){
  return findOperator(std::string{""}, name, only_graph);
}

Operator InterpreterW::findOperator(std::string space, std::string name, bool only_graph){
  Operator result{nullptr};
  int      level{-1};

  if (_operators.find(name) != _operators.end()){
    _notouched = false;
    return _operators[name];
  }

  if (only_graph) return result;
  else if (global::macros.find(name) != global::macros.end())
    return global::macros[name];

  if (space.size() > 0){
    auto fullname = space + "::" + name;

    for (auto item: _devices){
      auto device  = std::get<1>(item);
      auto candidate = Operator{nullptr};

      if (!(candidate = device->_template.find(device, fullname.c_str())))
        continue;
      else if (level < device->level && isOkey4Loading(rvalue(fullname), device)){
        result = candidate;
        level  = device->level;
      }
    }
  }

  if (!result){
    for (auto item: _devices){
      auto device  = std::get<1>(item);
      auto candidate = Operator{nullptr};

      if (!(candidate = device->_template.find(device, name.c_str())))
        continue;
      else if (level < device->level && isOkey4Loading(rvalue(name), device)){
        result = candidate;
        level  = device->level;
      }
    }
  }

  return result;
}

Tensor InterpreterW::findPlaceholder(std::string name, int variance){
  if (_placeholders.find(name) == _placeholders.end())
    return nullptr;
  else if (variance < 0) return _placeholders[name];

  if ((variance > 0) ^ (_placeholders[name]->_const))
    return nullptr;
  return _placeholders[name];
}

base::Error InterpreterW::error(){
  base::Error result;

  internal::safe(&_mlock, [&]() { result = _error; });
  return result;
}

base::Error InterpreterW::error(base::Error&& error){
  base::Error result;

  internal::safe(&_mlock, [&]() {
    /* @NOTE: fata-error will cause disturbance and release everything */
    if ((_error = error) && _queue->single() && !_queue->reseted())
      _queue->reset();

    _status = error? Stop: _status;
    result  = _error;
  });
  return result;
}

base::Error InterpreterW::error(base::Error& error){
  base::Error result;

  internal::safe(&_mlock, [&]() {
    /* @NOTE: fata-error will cause disturbance and release everything */
    if ((_error = error) && !_queue->single() && !_queue->reseted())
      _queue->reset();
    return rvalue(_error);
  });
  return result;
}

InterpreterW::Status InterpreterW::status(){
  InterpreterW::Status result;

  internal::safe(&_mlock, [&]() { result = _status; });
  return result;
}

int InterpreterW::inactive(){
  int result = 0;

  internal::safe(&_mlock, [&]() { 
    for (auto& item: _consumers){
      internal::safe(std::get<1>(std::get<1>(item)), [&](){
        if (std::get<0>(std::get<1>(item))->status() == Couple::Wait){
          result++;
        }
      });
    }
  });

  return result;
}

int InterpreterW::active(){
  int result = 0;

  internal::safe(&_mlock, [&]() { 
    for (auto& item: _consumers){
      internal::safe(std::get<1>(std::get<1>(item)), [&](){
        if (std::get<0>(std::get<1>(item))->status() != Couple::Wait){
          result++;
        }
      });
    }
  });

  return result;
}

InterpreterW::Devices& InterpreterW::devices(){
  using namespace global;

  if (interpreters.find(_interpreter.master) == interpreters.end())
    throw OutOfRange;
  return interpreters[_interpreter.master]->_devices;
}

int InterpreterW::resolve(int method){
  /* @NOTE: everything call from resolve is on thread-safe or isolated
   * we don't care anymore about race condition from here */

  if (method >= 0 && _status != Running){
    _resolver = method;
  }

  return _resolver != std::string::npos? _resolver: LOOPBACK_RESOLVE;
}

bool InterpreterW::use(std::pair<const String, Device>& item){
  auto device = std::get<1>(item);

  if (!device->_enable || !*(device->_enable))
    return false;

  _interpreter.sysdef = device;
  return true;
}

bool InterpreterW::use(const String&& name){
  auto device = _devices[name];

  if (!device->_enable || !*(device->_enable))
    return false;

  _interpreter.sysdef = device;
  return true;
}

Device InterpreterW::sysbase(){
  try{
    if (!_interpreter.sysdef){
      auto opmax = -1;

      for (auto& item: devices()){
        auto& device = std::get<1>(item);

        if (device->opnum && *(device->opnum) > opmax){
          _interpreter.sysdef = device;
          opmax = *(device->opnum);
        }
      }
    }

    return _interpreter.sysdef;
  } catch (base::Error& error){
    _error = error;
    return nullptr;
  }
}

bool InterpreterW::own(Thread thread){
  return _consumers.find(thread) != _consumers.end();
}
} // namespace nn

bool nNet_InstallDeviceW(CString name, Device device, Interpreter interpreter){
  return nNet_GetInterpreterW(interpreter)->assignDevice(name, device);
}

extern "C" bool nNet_InstallOperatorToInterpreter(CString name, Operator op, 
    Interpreter interpreter){
  using namespace nn::global;

  if (nNet_CheckStatus() != nn::Running && interpreter)
    return BadAccess.reason("this interpreter is outdate");
  else if (nNet_CheckStatus() == nn::Initial){
    /* @NOTE: at early states, operators must be installed to global::macros */

    if (interpreter)
      return BadAccess.reason("this interpreter is outdate");
    else if (op->name != nullptr)
      return BadLogic.reason("install a real operator at "
                             "state Initial is forbidden");
    else if (macros.find(std::string{name}) == macros.end())
      nn::global::macros[std::string{name}] = op;
    else
      return BadAccess.reason("this op " + std::string{name} + 
                              " is already defined");
  } else if (interpreter)
    return nNet_GetInterpreterW(interpreter)->assignOperator(name, op);
  return true;
}

extern "C" bool nNet_InstallOperatorToDevice(CString name, 
                                             Operator op, Device device){
  using namespace nn::global;
  auto result = false;

  if (!device) return BadAccess.reason("device must not be nullptr and ");
  if (nNet_CheckStatus() != nn::Running && nNet_CheckStatus() != nn::Initial)
    return BadLogic.reason("system status must be Initial or Running to activate"
                           " installing steps");
  if (device->_template.install){
    result = device->_template.install(device, name, op);

    if (result && !op->device)
      op->device = device;
    return result;
  } else if (nNet_CheckStatus() ==  nn::Initial){
    nNet_GetDeviceW(device)->operators()[name] = op;
    return true;
  } else {
    return !(NoSupport << nNet_GetDeviceW(device)->name() 
                       << "\'s install is underdevelopment");
  }
}

extern "C" Device nNet_RemoveDevice(Device device){
  if (device){
    if (!nNet_IsTemplateDevice(device)){

      if (device->_template.release)
        device = device->_template.release(device);
     
      if (device->_priv) free(device->_priv);
      if (nNet_CheckStatus() == nn::Initial){
        if (device->opnum) free(device->opnum);
        if (device->_enable) free(device->_enable);
        if (device->_template._priv) free(device->_template._priv);
      }
    } else if (nNet_CheckStatus() == nn::Releasing){
      /* @NOTE: global::devices are only released at state Releasing */

      if (device->_template.release)
        device = device->_template.release(device);

      if (device){
        if (device->opnum) free(device->opnum);
        if (device->_enable) free(device->_enable);
        if (device->_template._priv) free(device->_template._priv);
      }
    } else device = nullptr;

    if (device) free(device);
  }
  return nullptr;
}

extern "C" Operator nNet_RemoveOperator(Operator op){
  if (op) {
    if (!nNet_IsTemplateOperator(op)){
      if (op->_event.release)
        op = op->_event.release(op, &nn::InterpreterW::instance().interface());

      if (op){
        if (op->priv)
          delete reinterpret_cast<nn::interpreter::OperatorW*>(op->priv);
        if (op->name) free(op->name);
        free(op);
      }

      if (!nn::InterpreterW::instance().removeOperator(op->name))
        nn::InterpreterW::instance().removePointer(op);
    } else {
      /* @NOTE: there are 2 cases: this op is a builtin operator or this op is
       * a custom operator. In the case of builtin operators, they are only 
       * allowed to be removed when status != Running */

      if (nNet_CheckStatus() != nn::Running){
        /* @NOTE: allow to remove this builtin operator */

        if (op->_event.release)
          op = op->_event.release(op, nullptr);
      } else if (nn::InterpreterW::instance().findOperator(op)){ 
        /* @NOTE: confirm this is an actual op */

        if (op->_event.release)
          op = op->_event.release(op, &nn::InterpreterW::instance().interface());
        nn::InterpreterW::instance().removePointer(op);
      } else return op;

      if (op){
        if (op->_event._priv)
          free(op->_event._priv);
        if (op->priv)
          delete reinterpret_cast<nn::interpreter::OperatorW*>(op->priv);
        if (op->name) free(op->name);
        free(op);
      }
    }
  }
  return nullptr;
}

extern "C" Device nNet_RemoveDeviceW(Interpreter interpreter, 
                                         Device device){
  if (!interpreter || !device) return nullptr;
  return nNet_GetInterpreterW(interpreter)->removeDevice(device)? nullptr
                                                                    : device;
}

extern "C" Operator nNet_RemoveOperatorW(Interpreter interpreter, Operator op){
  auto interpreterw = interpreter? nNet_GetInterpreterW(interpreter): nullptr;
  auto result = op;

  if (result && interpreterw){
    auto name = interpreter? interpreterw->findOperator(op): nullptr;

    if (name != nullptr && interpreterw->findPointer(op, OPERATOR_T)){
      /* @NOTE: found name of this operator is an actual operator not a template */

      if (op->_event.release)
        op = op->_event.release(op, interpreter);
      else 
        result = nNet_RemoveOperator(op);

      /* @NOTE: name == op->name so we must call interpreterw->removeOperator 
       * before free this pointer */
      if (!interpreterw->removeOperator(name))
        interpreterw->removePointer(op);

      if (op){
        if (op->_event._priv)
          free(op->_event._priv);
        if (op->name) free(op->name);
        if (op->priv)
          delete reinterpret_cast<nn::interpreter::OperatorW*>(op->priv);
        free(op);
      }
    } else if (name == nullptr) {
      /* @NOTE: not sure, maybe this is a global template macro or an 
       * actual macro which is created from a template macro */
      auto result = nNet_RemoveOperator(op);

      if (!result && interpreterw->findPointer(op, OPERATOR_T))
        interpreterw->removePointer(op);
      return result;
    }
  } else if (!interpreterw)
    return nNet_RemoveOperator(op);
  return result;
}


extern "C" Operator nNet_RemoveOperatorC1(Interpreter interpreter,
  CString device, Operator op){
  using namespace nn::utils;

  if (!interpreter || !device || !op) 
    return nullptr;
  else if (!nNet_GetInterpreterW(interpreter)->findDevice(device))
    return op;
  else if (nn::global::devices.find(device) == nn::global::devices.end()){
    auto  interpreterw = nNet_GetInterpreterW(interpreter);
    auto  devicew    = nNet_GetDeviceW(interpreterw->findDevice(device));
    auto& operators    = devicew->operators();

    for (auto& item: operators){
      if (std::get<1>(item) == op){
        if (op->_event.release)
          op = op->_event.release(op, interpreter);

        if (op){
          if (op->_event._priv)
            free(op->_event._priv);
          if (op->name) free(op->name);
          if (op->priv)
            delete reinterpret_cast<nn::interpreter::OperatorW*>(op->priv);
          free(op);
        }

        operators.erase(std::get<0>(item));
        return nullptr;
      }
    }
    return op;
  }
  return null<Operator>(interpreter, BadAccess.reason("can\'t remove global operators"));
}

Operator nNet_RemoveOperatorC2(Interpreter interpreter, CString device, CString op){
  using namespace nn::utils;

  if (!interpreter || !device || !op)
    return nullptr;
  else if (!nNet_GetInterpreterW(interpreter)->findDevice(device))
    return null<Operator>(interpreter, NotFound.reason("device " + std::string(device)));
  else if (nn::global::devices.find(device) == nn::global::devices.end()){
    auto  interpreterw = nNet_GetInterpreterW(interpreter);
    auto  devicew    = nNet_GetDeviceW(interpreterw->findDevice(device));
    auto& operators    = devicew->operators();

    if (operators.find(op) == operators.end()){
      return null<Operator>(interpreter, 
        NotFound.reason("device " + std::string{device} + 
                        " didn\'t have op " + std::string{op}));
    } else {
      auto opp = operators[op];

      if (opp->_event.release)
      opp = opp->_event.release(opp, interpreter);

      if (opp){
        if (opp->_event._priv)
          free(opp->_event._priv);
        if (opp->name) free(opp->name);
        if (opp->priv)
          delete reinterpret_cast<nn::interpreter::OperatorW*>(opp->priv);
        free(opp);
      }

      operators.erase(op);
      return nullptr;
    }
  } 
  return null<Operator>(interpreter, BadAccess.reason("can\'t remove global operators"));
}

Device nNet_GetDeviceOfOperator(Interpreter interpreter, Operator op){
  return nNet_GetInterpreterW(interpreter)->findDevice(op);
}
