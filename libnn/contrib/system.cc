#define DISABLE_PROTOBUF 1

#ifdef LIBCGRAPH_GIT_REFSPEC
#include "models/tfgraph/tfgraph.h"
#else
#include "tfgraph.h"
#endif

#include "interpreter.h"
#include "internal.h"
#include "builtin.h"
#include "utils.h"

namespace nn{
namespace device{
base::Error useCPU(Device* result);

#if ENABLE_METAL
base::Error useMetal(std::vector<Device>& result);
#endif
#if ENABLE_OPENCL
base::Error useOpenCL(std::vector<Device>& result);
#endif
#if ENABLE_CUDA
base::Error useCUDA(std::vector<Device>& result);
#endif
} // namespace device
} // namespace nn

namespace nn{  
using PInterpreterW = InterpreterW*;
using Interpreters  = std::vector<Interpreter>;

namespace global{
using namespace std;
using namespace interpreter;

Status status = nn::Unknown;

/* @NOTE: these global variables are used to manage interpreters */  
std::map<Thread, Interpreters> topologies{};
std::map<Thread, PInterpreterW>  interpreters{};
std::map<string, Device>  devices{};
std::map<string, Operator>  macros{};

/* @NOTE: these global variables are used to manage io framework */
std::map<std::string, model::Attribute> attributes{};
std::vector<model::Export> exporters{};
std::vector<model::Import> importers{};
} // namespace global

namespace internal{
pthread_mutex_t lock;

namespace build{
typedef bool (*Macro)(Operator op, Interpreter interpreter);

base::Error transcript(std::string&& model, InterpreterW& interpreter, 
                       bool lazyload){
  /* @NOTE: this function is used to translate a binary model to graph and load
   * it into an interpreter */
  auto success = false;

  for (auto& importer: global::importers){
    if ((success = !importer(rvalue(model), interpreter, lazyload)))
      break;
  }

  return success? NoError: DoNothing;
}

base::Error pack(Operator op, Registration reg, bool macro){
  using namespace nn::interpreter;

  if (!op) return DoNothing;
  else
    memcpy(&op->_event, &reg.event, sizeof(Event));
  op->type = macro;

  /* @NOTE: if event.prepare is NULL, it must be set with default function */
  if (!reg.event._priv){
    if (macro || !op->_event._priv){
      op->_event._priv = calloc(1, sizeof(Registration));

      if (op->_event._priv)
        memcpy(op->_event._priv, &reg, sizeof(Registration));
      else return DrainMem;
    }

    if (macro){
      op->_event.release = nullptr;
      op->_event.prepare =
      [](Operator op, Interpreter interpreter, CString model) -> Operator{
        auto reg = (Registration*)op->_event._priv;
        auto ret = op;

        if (reg && reg->event.prepare)
          ret = reg->event.prepare(op, interpreter, model);
        else if (!(ret = (Operator) calloc(1, sizeof(OperatorDef))))
            return nullptr;

        if (!ret) return nullptr;
        else if (reg && ret != op){
          ret->_event._priv = reg->event._priv;

          if (reg->event.release)
            ret->_event.release = reg->event.release;

          if (!ret->commit) ret->commit = reg->eval;
          if (!ret->link)   ret->link   = reg->link? reg->link: op->link;
          if (!ret->inputnames)
            ret->inputnames = reg->inputnames? reg->inputnames: op->inputnames;
        } else {
          ret = (Operator) calloc(1, sizeof(OperatorDef));
          if (ret) memcpy(ret, op, sizeof(OperatorDef));

          ret->_event.release = [](Operator op, Interpreter) -> Operator{
            if (op->name) free(op->name);
            if (op) free(op);
            return nullptr;
          };
        }
        return ret;
      };
    } else
      op->_event.prepare = 
      [](Operator op, Interpreter interpreter, CString model) -> Operator{
        auto reg = reinterpret_cast<Registration*>(op->_event._priv);
        auto ret = op;

        if (reg && !op->link && !op->priv){
          /* @NOTE: if reg->event.prepare != nullptr, we must use it */

          if (reg->event.prepare)
            ret = reg->event.prepare(op, interpreter, model);
          else if (!(ret = (Operator) calloc(1, sizeof(OperatorDef))))
            return nullptr;

          /* @NOTE: check and redefine callback 'commit', 'inputs' and link,
           * it's not like macro, function must be on 2 stages: commiting and
           * calculating
           * - Commiting: apply eval and derive to outputs and inputs 
           * respectively 
           * - Calculating: perform calculation on the flows: backward and 
           * forward
           */

          /* @NOTE: callback prepare is used when a operator is created, this is
           * used to create, allocate a new operator and init another callback
           * for the COMMITING stage */

          if (!ret) return nullptr;
          else if (ret != op){
            ret->_event._priv = reg;
            ret->commit = op->commit? op->commit: nullptr;

            if (reg->event.release)
              ret->_event.release = reg->event.release;
            if (!ret->inputnames)
              ret->inputnames = reg->inputnames? reg->inputnames: op->inputnames;
            if (!ret->link) ret->link = reg->link? reg->link: op->link;
          }
        } else {
          ret = (Operator) calloc(1, sizeof(OperatorDef));

          if (ret){
            memcpy(ret, op, sizeof(OperatorDef));

            ret->_event.release = [](Operator op, Interpreter) -> Operator{
              if (op->name) free(op->name);
              if (op) free(op);
              return nullptr;
            };
          } else return nullptr;
        }
        return ret;
      };
  }

  /* @NOTE: if event.release is NULL, it must be set with default function */
  if (!op->_event.release && !reg.event._priv){
    if (macro)
      op->_event.release = [](Operator op, Interpreter) -> Operator{
        if (op->_event._priv) free(op->_event._priv);
        op->_event._priv = nullptr;
        return op;
      };
    else
      op->_event.release = [](Operator op, Interpreter interpreter) -> Operator{
        if (op->priv)
          delete reinterpret_cast<interpreter::OperatorW*>(op->priv);
        op->priv = nullptr;

        if (!nNet_IsTemplateOperatorW(interpreter, op) &&
            nNet_CheckStatus() == nn::Running)
          op->_event._priv = nullptr;
        return op;
      };
  }

  /* @NOTE: if event.commit is NULL, it must be set with default function */
  if (!op->commit){
    if (macro){
      op->commit = [](Operator op, Interpreter interpreter){
        return nNet_RemoveOperatorW(interpreter, op) == nullptr; 
      };
    } else {
      op->commit = [](Operator op, Interpreter interpreter){
        auto reg    = reinterpret_cast<Registration*>(op->_event._priv);
        auto interw = reinterpret_cast<InterpreterW*>(interpreter->priv);

        /* @NOTE: this callback is called when COMMITING stage is proposed and 
         * it will be used to assign the operator and prepare callbacks to the
         * CALCULATING stage */
        op->eval   = reg->eval;
        op->derive = reg->derive;

        if (interw)
          return !interw->assignOperator(std::string{op->name}, op);
        return false;
      };
    }
  }

  /* @NOTE: if event.naming is NULL, it must be set with default function */
  if (!op->naming && !op->type)
    op->naming = [](Operator op, CString name) -> bool{
      return (op->name = strdup(name)) == nullptr;
    };

  /* @NOTE: if op->link is NULL, it must be set with default function */
  if (!op->link && macro) op->link = reg.link;
  return NoError;
}
} // namespace build

base::Error switchTo(nn::Status status){
  base::Boundary boundary{
    [](){ pthread_mutex_lock(&internal::lock); }, 
    [](){ pthread_mutex_unlock(&internal::lock); }
  };

  if (status == Unknown && global::status != Releasing)
    return NoSupport;
  else if (global::status == Running && status != Running){
    for (auto& item: global::topologies){
      for (auto interpreter: std::get<1>(item))
        delete interpreter;

      std::get<1>(item).clear();
    }
    global::interpreters.clear();
  }

  global::status = status;
  return NoError;
}
} // namespace internal
} // namespace nn
extern "C" int  nNet_InitModels(){
  auto count = 0;

  if (nn::tfgraph::load()){
    nn::global::attributes[TFGRAPH] = nn::tfgraph::kernels::attribute;
    count++;
  }
  return count;
}

extern "C" void nNet_ReleaseModels(){
  nn::tfgraph::exit();

  if (nn::global::status == nn::Unknown)
    pthread_mutex_destroy(&nn::internal::lock);
}

extern "C" int nNet_LoadExporters(){
  using namespace nn;

  global::exporters.push_back(nn::save::tfgraph);
  return global::exporters.size();
}

extern "C" int nNet_UnloadExporters(){
  using namespace nn;

  global::exporters.clear();
  return global::exporters.size();
}

extern "C" int nNet_LoadImporters(){
  using namespace nn;

  global::importers.push_back(nn::build::tfgraph);
  return global::importers.size();
}

extern "C" int nNet_UnloadImporters(){
  using namespace nn;

  global::importers.clear();
  return global::importers.size();
}

extern "C" bool nNet_InstallDevice(CString name, Device device){
  return nn::InterpreterW::install(std::string{name}, device);
}

extern "C" int nNet_LoadMacros(){ 
  nn::internal::doRegisterBuiltinOp();
  nn::internal::switchTo(nn::Running);

  return nn::global::macros.size();
}

extern "C" int nNet_UnloadOperators(){
  if (!nn::internal::switchTo(nn::Releasing)){
    auto count = nn::global::macros.size();

    for (auto& item: nn::global::macros){
      auto& op     = std::get<1>(item);
      auto  remain = nNet_RemoveOperator(op);

      if (remain){
        if(op == remain) free(op);
        else {
          warn(BadAccess.reason("op " + std::get<0>(item) + 
                                " still remain"));
          break;
        }
      }

      std::get<1>(item) = nullptr;
      count--;
    }

    if (count == 0)
      nn::global::macros.clear();
    else return count;
  }
  return nn::global::macros.size();
}

extern "C" int nNet_LoadDevices(){
  int count{0};

  if (nn::internal::switchTo(nn::Initial))
    return 0;
  {
    Device definition{nullptr};

    if (!nn::device::useCPU(&definition)){
      nn::global::devices["generic"] = definition;
      definition = nullptr;
      count++;
    } else definition = nNet_RemoveDevice(definition);
  }

 #if ENABLE_METAL
  {
    std::vector<Device> definitions{};

    if (!nn::device::useMetal(definitions)){
      if (definitions.size() == 1){
        nn::global::devices["metal"] = definitions[0];
        count++;
      } else {
        auto i = 0;

        for (auto& definition: definitions){
          nn::global::devices["metal:" + std::to_string(i)] = definition;
          count++;
        }
      }
    } 
  }
 #endif
 #if ENABLE_OPENCL
  {
    std::vector<Device> definitions{};

    if (!nn::device::useOpenCL(definitions)){
      if (definitions.size() == 1){
        nn::global::devices["opencl"] = definitions[0];
        count++;
      } else {
        auto i = 0;

        for (auto& definition: definitions){
          nn::global::devices["opencl:" + std::to_string(i)] = definition;
          count++;
        }
      }
    }
  }
 #endif
 #if ENABLE_CUDA
  {
    std::vector<Device> definitions{};

    if (!nn::device::useCUDA(definitions)){
      if (definitions.size() == 1){
        nn::global::devices["cuda"] = definitions[0];
        count++;
      } else {
        auto i = 0;

        for (auto& definition: definitions){
          nn::global::devices["cuda:" + std::to_string(i)] = definition;
          count++;
        }
      }
    } 
  }
 #endif
  return count;
}

extern "C" int nNet_UnloadDevices(){
  auto count = nn::global::devices.size();

  if (!nn::internal::switchTo(nn::Releasing)){
    for (auto& item: nn::global::devices){
      auto& name     = std::get<0>(item);
      auto  device = std::get<1>(item);
      auto  remain   = nNet_RemoveDevice(device);

      if (remain && (device == remain)) free(device);
      else if (remain){
        warn(DoAgain.reason(name + " got probleams and can't remove"));
        continue;
      }
      count--;
    }

    nn::internal::switchTo(nn::Unknown);
  }
  return count;
}

extern "C" int nNet_CheckStatus(){
  base::Boundary boundary{
    [](){ pthread_mutex_lock(&nn::internal::lock); },
    [](){ pthread_mutex_unlock(&nn::internal::lock); }
  };
  return nn::global::status;
}

extern "C" bool nNet_IsTemplateDevice(Device device){
  if (nNet_CheckStatus() == nn::Initial) return true;
  for (auto& item: nn::global::devices){
    if (device == std::get<1>(item))
      return true;
  }
  return false;
}

extern "C" bool nNet_IsTemplateOperator(Operator op){
  using namespace nn::global;

  auto thread = (Thread) pthread_self();

  if (nNet_CheckStatus() == nn::Initial || nNet_CheckStatus() == nn::Releasing)
    return true;
  else if (nNet_CheckStatus() != nn::Running)
    return false;

  if (interpreters.find(thread) != interpreters.end())
    return nNet_IsTemplateOperatorW(&nn::InterpreterW::instance().interface(), op);
  else {
    for (auto& item: macros){
      if (std::get<1>(item) == op)
        return true;
    }

    for (auto& item: devices){
      auto device = nNet_GetDeviceW(std::get<1>(item));

      for (auto& item: device->operators())
        if (std::get<1>(item) == op)
          return true;
    }

    return false;
  }
}

extern "C" bool nNet_IsTemplateOperatorW(Interpreter interpreter, Operator op){
  using namespace nn::global;

  if (nNet_CheckStatus() == nn::Initial) return true;
  if (nn::global::status != nn::Initial){
    for (auto& item: macros){
      if (std::get<1>(item) == op)
        return true;
    }
    if (!op->device) return false;

    for (auto& item: devices){
      if (std::get<1>(item)->_template.scan(op->device, op))
        return true;
    }

    if (status == nn::Running && interpreter){
      auto name = nNet_GetInterpreterW(interpreter)->findOperator(op);

      if (name && name != op->name)
        return true;
      else return false;
    }
  } else return true;
  return false;
}

extern "C" bool nNet_PackOperator(Operator op, Registration reg, bool macro){
  return nn::internal::build::pack(op, reg, macro);
}