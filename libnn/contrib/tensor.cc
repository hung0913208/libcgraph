#ifdef LIBCGRAPH_GIT_REFSPEC
#include <libbase/macros.hpp>
#else
#include "macros.hpp"
#endif

#if APPLE
#define _DARWIN_C_SOURCE
#endif

#if USE_VECTORIZE
#if HAS_CXX_ALIGNED_ALLOC
#if !defined(_ISOC11_SOURCE)
#define _ISOC11_SOURCE
#endif
#elif HAS_CXX_POSIX_MEMALIGN
#if LINUX
#if defined(_POSIX_C_SOURCE) && _POSIX_C_SOURCE < 200112L
#undef _POSIX_C_SOURCE
#endif

#define _POSIX_C_SOURCE 200112L
#elif APPLE
#if defined(_DARWIN_C_SOURCE)
#undef _DARWIN_C_SOURCE
#endif

#if defined(_POSIX_C_SOURCE)
#undef _POSIX_C_SOURCE
#endif

#define _DARWIN_C_SOURCE 200112L
#endif
#endif

extern "C" int nNet_AlignedSizeOfType(int type);
#endif

#include <algorithm>
#include <cstdlib>
#include <map>

#include "interpreter.h"
#include "tensor.h"
#include "utils.h"

#define SHAPE_T    0
#define ORDINATE_T 1
#define TENSOR_T   2

extern "C"{
Shape    nNet_ReallocShapeW(Interpreter interpreter, Shape shape, int ndims);
Ordinate nNet_ReallocOrdinateW(Interpreter interpreter, Ordinate shape, int ndims);
Tensor   nNet_ReallocTensorW(Interpreter interpreter, Tensor tensor,
                             int type, bool force);

Shape    nNet_ReallocShape(Shape shape, int ndims);
Ordinate nNet_ReallocOrdinate(Ordinate shape, int ndims);
Tensor   nNet_ReallocTensor(Tensor tensor, int type, bool force);
Tensor   nNet_ReallocRefTensor(Tensor tensor, Tensor src);

bool nNet_ConvertNumT(Item src, int stype, Item dst, int dtype);
long nNet_GetTensorSize(Tensor tensor);

void DontRemoveTensorData(Tensor tensor, Interpreter interpreter);
void JustRemoveTensorDims(Tensor tensor, Interpreter interpreter);
}

namespace nn{
namespace utils {
void alloc(Shape shape);
void alloc(Ordinate ordinate);
void alloc(Tensor tensor);

void free(Shape shape);
void free(Ordinate ordinate);
void free(Tensor tensor);
} // namespace utils

namespace tensor{
Tensor ones(std::vector<int> dims, int type, Interpreter interpreter){
  auto result = nNet_CreateTensorC(interpreter, nullptr);

  std::reverse(dims.begin(), dims.end());
  if (result){
    ShapeDef fakeshape;
    Array    array;
    int      size;

    memset (&fakeshape, 0, sizeof(ShapeDef));
    fakeshape.max   = dims.size();
    fakeshape.ndims = dims.size();
    fakeshape.dims  = const_cast<int*>(dims.data());

    if (!nNet_SetTensorNumTW(interpreter, result, type))
      return nNet_RemoveTensorW(interpreter, result);

    if (!nNet_SetTensorShapeW(interpreter, result, &fakeshape))
      return nNet_RemoveTensorW(interpreter, result);

    if (!(result = nNet_ReallocTensorW(interpreter, result, type, true)))
      return nNet_RemoveTensorW(interpreter, result);

    array = nNet_GetArrayData(result);
    size  = nNet_GetArraySizeT(result);

    if (!array)
      return nNet_RemoveTensorW(interpreter, result);

    switch (type){
    case Int32_t:
      for (auto i = 0; i < size; ++i){
        ((Int32*)array)[i] = 1;
      }
      break;

    case Int64_t:
      for (auto i = 0; i < size; ++i){
        ((Int64*)array)[i] = 1;
      }
      break;
    case Real32_t:
      for (auto i = 0; i < size; ++i){
        ((Real32*)array)[i] = 1.0;
      }
      break;

    case Real64_t:
      for (auto i = 0; i < size; ++i){
        ((Real64*)array)[i] = 1.0;
      }
      break;

    default:
      return nNet_RemoveTensorW(interpreter, result);
    }

    nNet_SetArrayData(result, array);
  }
  return result;
}

Tensor zeros(std::vector<int> dims, int type, Interpreter interpreter){
  auto result = nNet_CreateTensorC(interpreter, nullptr);

  std::reverse(dims.begin(), dims.end());
  if (result){
    ShapeDef fakeshape;
    Array    array;
    int      size;

    memset(&fakeshape, 0, sizeof(ShapeDef));
    fakeshape.ndims = dims.size();
    fakeshape.dims  = const_cast<int*>(dims.data());

    if (!nNet_SetTensorNumTW(interpreter, result, type))
      return utils::clearFaulty(interpreter, result,
                                BadAccess.reason("nNet_SetTensorNumT()"));
    if (!nNet_SetTensorShapeW(interpreter, result, &fakeshape))
      return utils::clearFaulty(interpreter, result,
                                BadAccess.reason("nNet_SetTensorShape()"));

    if (!(result = nNet_ReallocTensorW(interpreter, result, type, true)))
      return nNet_RemoveTensor(result);

    array = nNet_GetArrayData(result);
    size  = nNet_GetArraySizeT(result);

    if (!array)
      return utils::clearFaulty(interpreter, result,
                                BadAccess.reason("nNet_GetArrayData()"));
    switch (type){
    case Int32_t:
      for (auto i = 0; i < size; ++i){
        ((Int32*)array)[i] = 0;
      }
      break;

    case Int64_t:
      for (auto i = 0; i < size; ++i){
        ((Int64*)array)[i] = 0;
      }
      break;

    case Real32_t:
      for (auto i = 0; i < size; ++i){
        ((Real32*)array)[i] = 0.0;
      }
      break;

    case Real64_t:
      for (auto i = 0; i < size; ++i){
        ((Real64*)array)[i] = 0.0;
      }
      break;

    default:
      return utils::clearFaulty(interpreter, result, 
                                NoSupport.reason("type is Unknown"));
    }

    nNet_SetArrayData(result, array);
  }
  return result;
}

Tensor ones(int type, int num_rows, int num_columns){
  if (num_rows <= 0) return ones(1, type);
  if (num_columns <= 0) return ones(num_rows, type);
  return ones({num_rows, num_columns}, type);
}

Tensor zeros(int type, int num_rows, int num_columns){
  if (num_rows <= 0) return zeros(1, type);
  if (num_columns <= 0) return zeros(num_rows, type);
  return zeros({num_rows, num_columns}, type);
}

Tensor eye(int type, int num_rows, int num_columns){
  auto result = zeros(type, num_rows, num_columns);
  auto array  = nNet_GetArrayData(result);

  if (!result || !array)
   return utils::clearFaulty(nullptr, result,
                             BadAccess.reason("nNet_GetArrayData()"));
  else if (num_rows <= 0){
    num_rows    = 1;
    num_columns = 1;
  }

  switch (type){
  case Real32_t:
    for (auto i = 0; i < num_rows && i < num_columns; ++i){
      ((Real32*)array)[i*(num_columns + 1)] = 1.0;
    }
    break;

  case Real64_t:
    for (auto i = 0; i < num_rows && i < num_columns; ++i){
      ((Real64*)array)[i*(num_columns + 1)] = 0.0;
    }
    break;

  default:
      return utils::clearFaulty(nullptr, result,
                                NoSupport.reason("type is Unknown"));
  }

  nNet_SetArrayData(result, array);
  return result;
}

Tensor create(Interpreter interpreter, int type){
  auto result = (Tensor)calloc(1, sizeof(TensorDef));
  auto interw = nNet_GetInterpreterW(interpreter);

  interw->assignPointer(result, TENSOR_T);
  if (result) {
    result->type = type;
    result       = interw->hook(result);

    if (!result)
      return nNet_RemoveTensorW(interpreter, result);
    return result;
  }

  return nullptr;
}

template<> Tensor tensor<char*>(std::vector<char*>&& data, Interpreter interpreter){
  auto shape = data.size() > 1? nNet_CreateTensorShapeC1(interpreter, 1, data.size())
                              : nullptr;
  auto result  = nNet_CreateTensorC(interpreter, shape);
  auto success = true;

  int   type;
  Array array;

  if (data.size() == 0) return nNet_RemoveTensorW(interpreter, result);
  if (nNet_RemoveTensorShapeW(interpreter, shape))
    return nNet_RemoveTensorW(interpreter, result);

  if (!(type = utils::type(typeid(char*))))
    return nNet_RemoveTensorW(interpreter, result);

  if (!nNet_SetTensorNumTW(interpreter, result, type))
    return nNet_RemoveTensorW(interpreter, result);

  if (!(array = nNet_GetArrayData(result)))
    return nNet_RemoveTensorW(interpreter, result);

  for (auto i = 0; success && i < cast_(i, data.size()); ++i){
    auto dst = nNet_GetArrayItem(array, i, type);
    auto src = strdup(data[i]);

    success = nNet_ConvertNumT(&src, type, dst, type);
    continue;
  }

  nNet_SetArrayData(result, array);
  return (success)? result: nNet_RemoveTensorW(interpreter, result);
}

template<> Tensor tensor<std::string>(std::vector<std::string>&& data,
                                             Interpreter interpreter){
  auto shape = data.size() > 1? nNet_CreateTensorShapeC1(interpreter, 1, data.size())
                              : nullptr;
  auto result  = nNet_CreateTensorC(interpreter, shape);
  auto success = true;

  int   type;
  Array array;

  if (data.size() == 0)
    return nNet_RemoveTensorW(interpreter, result);
  else if (nNet_RemoveTensorShapeW(interpreter, shape))
    return nNet_RemoveTensorW(interpreter, result);
  else if (!(type = utils::type(typeid(char*))))
    return nNet_RemoveTensorW(interpreter, result);
  else if (!nNet_SetTensorNumTW(interpreter, result, type))
    return nNet_RemoveTensorW(interpreter, result);
  else if (!(array = nNet_GetArrayData(result)))
    return nNet_RemoveTensorW(interpreter, result);

  for (auto i = 0; success && i < cast_(i, data.size()); ++i){
    auto dst = nNet_GetArrayItem(array, i, nNet_GetTensorNumT(result));
    auto src = malloc(data[i].size());

    if (!src)
      return nNet_RemoveTensorW(interpreter, result);
    else
      memcpy(src, data[i].c_str(), data[i].size());

    success = nNet_ConvertNumT(&src, type, dst, type);
  }

  nNet_SetArrayData(result, array);
  return (success)? result: nNet_RemoveTensorW(interpreter, result);
}
} // namespace tensor
} // namespace nn

extern "C" Shape nNet_CreateTensorShapeC1(Interpreter interpreter, int ndims, ...){
  Shape result = nNet_ReallocShapeW(interpreter, nullptr, ndims);

  if (!result) return nullptr;
  else{
    va_list vlist;

    va_start(vlist, ndims);
    for (int i = 0; i < ndims; ++i){
      auto value = va_arg(vlist, int);

      result->dims[i] = value <= 0? 1: value;
    }
    va_end(vlist);
  }
  return result;
}

extern "C" Shape nNet_CreateTensorShapeC2(Interpreter interpreter, int ndims,
                                          int *dims){
  Shape result = nNet_ReallocShapeW(interpreter, nullptr, ndims);

  memcpy(result->dims, dims, sizeof(int) * ndims);

  for (int i = 0; i < ndims; ++i){
    if (result->dims[i] <= 0)
      result->dims[i] = 1;
  }
  return result;
}

extern "C" Ordinate nNet_CreateOrdinateC1(Interpreter interpreter, Tensor tensor,
                                          int size, ...){
   if (tensor != nullptr){
    Ordinate result = nNet_ReallocOrdinateW(interpreter, nullptr, size);

    va_list vlist;
    int i;

    if (size == 0)
      result = nNet_ReallocOrdinateW(interpreter, nullptr, tensor->ndims);
    if (!result) return nullptr;

    va_start(vlist, size);
    for (i = 0; i < size && i < tensor->ndims; ++i){
      int hook = va_arg(vlist, int);

      if (hook < 0) hook = -1;

      if (tensor->dims[i] <= hook){
        result->ends[i] = -1;
      } else if (tensor->_ends && hook > tensor->_ends[i]){
        result->ends[i] = -1;
      }

      if (tensor->_begins && hook < tensor->_begins[i])
        result->begins[i] = tensor->_begins[i];

      if (result->ends[i] >= 0 && result->ends[i] < result->begins[i])
        result->ends[i] = result->begins[i] + 1;
    }

    for (; i < tensor->ndims; ++i){
      result->begins[i] = -1;
      result->ends[i] = -1;
    }

    va_end(vlist);
    return result;
  } else
    return nullptr;
}

extern "C" Ordinate nNet_CreateOrdinateC2(Interpreter interpreter, Tensor tensor, 
                               int *ordinate, int size){
  auto ndims  = tensor? tensor->ndims: size, i = 0;
  auto result = ndims > 0? nNet_ReallocOrdinateW(interpreter, nullptr, ndims)
                         : nullptr;

  if (!result || !tensor) return nullptr;
  if (ordinate){
    for (; i < tensor->ndims && i < size; ++i){
      result->begins[i] = ordinate[i];
      result->ends[i] = ordinate[i] + 1;
    }
  }

  for (; i < tensor->ndims; i++){
    result->begins[i] = -1;
    result->ends[i] = -1;
  }
  return result;
}

extern "C" Ordinate nNet_CreateOrdinateC3(Interpreter interpreter, Tensor tensor,
                               int *begins, int bsize, int *ends, int esize){
  if (tensor != nullptr){
    Ordinate result = nNet_ReallocOrdinateW(interpreter, nullptr, tensor->ndims);
    int i = 0;

    if (!result || !tensor) return nullptr;
    if (begins){
      for (i = 0; i < tensor->ndims && i < bsize; ++i)
        result->begins[i] = begins[i];
    }

    for (; i < tensor->ndims; i++)
      result->begins[i] = -1;

    if (ends){
      for (i = 0; i < tensor->ndims && i < esize; ++i)
        result->ends[i] = ends[i] + 1;
    }

    for (; i < tensor->ndims; i++){
      result->ends[i] = -1;
    }

    return result;
  } else
    return nullptr;
}

extern "C" Tensor nNet_CreateTensorC(Interpreter interpreter, Shape shape){
  if (!interpreter) return nNet_CreateTensorW(shape);
  else if (!shape)
    return nNet_ReallocTensorW(interpreter, nullptr, Unknown, false);
  else if (shape->ndims != 0 && shape->dims != nullptr){
    auto result = nn::tensor::create(interpreter, Unknown);

    if (!result) return nullptr;

    result->ndims = shape->ndims;
    result->dims  = shape->dims;

    shape->dims = nullptr;
    shape->ndims = 0;

    return result;
  }
  return nullptr;
}

extern "C" Tensor nNet_ReshapeTensorS(Tensor tensor, Shape shape){
  return nNet_ReshapeTensorC1(nullptr, tensor, shape);
}

extern "C" Tensor nNet_ReshapeTensorC1(Interpreter interpreter, Tensor tensor,
                                       Shape shape) {
  auto result = tensor;
  auto size   = 1;

  if (!result || nn::utils::outdate(interpreter, result))
    return nullptr;

  /* @NOTE: if shape is a nil-shape (it is resigned but it value is null), tensor
   * will clear its data */

  if (!shape->max && !shape->ndims && !shape->dims) {
    if (tensor->release) tensor->release(tensor, interpreter);
    if (tensor->dims) free(tensor->dims);
    tensor->dims  = nullptr;

    if (tensor->_content) {
      free(tensor->_content);
      tensor->_content = nullptr;
    }

    if (!tensor->_begins && !tensor->_ends) {
      if (tensor->init)
        tensor->init(tensor, interpreter, 0, Unknown, false);
    } else {
      tensor->_content = nullptr;
      if (tensor->_begins) free(tensor->_begins);
      if (tensor->_ends) free(tensor->_ends);

      tensor->_begins = nullptr;
      tensor->_ends   = nullptr;
    }

    tensor->_size  = 0;
    tensor->ndims = 0;
    return tensor;
  }

  if (tensor->reshape) {
    if (tensor->reshape(tensor, shape))
      return tensor;
    else return nullptr;
  }

  /* @NOTE: check if shape is compatible with tensor data */

  for (auto i = 0; i < shape->ndims; ++i)
    size *= shape->dims[i];

  if (size != tensor->_size && tensor->_size != 0)
    return nullptr;
  else if (result->_begins && result->_ends) {
    /* @NOTE: create new tensor with 'shape' */
    Array src_array, dst_array;
    bool  success = true;

    if (!(result = nNet_CreateTensorC(interpreter, shape)))
      return nullptr;
    result->_size = size;

    result = nNet_ReallocTensorW(interpreter, result, result->type, true);
    if (!result) return nullptr;

    src_array = nNet_GetArrayData(tensor);
    dst_array = nNet_GetArrayData(result);

    for (int i = 0; success && i < result->_size; ++i) {
      Item src = nNet_GetArrayItem(src_array, i, tensor->type);
      Item dst = nNet_GetArrayItem(dst_array, i, result->type);

      success = nNet_ConvertNumT(src, result->type, dst, tensor->type);
    }

    nNet_TryDropingArray(tensor, src_array);
    if (!success) result = nNet_RemoveTensor(result);
    else
      result = nNet_SetArrayData(result, dst_array);
  } else {
    auto tmp     = result->dims;
    auto release = result->release;

    // result->release = JustRemoveTensorScales;
    tensor->dims = (int*)(calloc(shape->ndims, sizeof(int)));

    if (!result->dims) {
      /* @NOTE: restore tensor->dims in the case when calloc fails */

      result->dims = tmp;
      return nullptr;
    } else {
      memcpy(result->dims, shape->dims, sizeof(int)*shape->ndims);

      if (tmp && !result->_begins && !result->_ends)
        free(tmp);
    }

    result->ndims = shape->ndims;
    if (result->_size == 0 && !result->_content) {
      result = nNet_ReallocTensorW(interpreter, result, result->type, true);
    }

    if (result) {
      if (release)
        result->release = release;
      result->ndims = shape->ndims;
    }
  }
  return result;
}

extern "C" Tensor nNet_ReshapeTensorW(Tensor tensor, Tensor source) {
  return nNet_ReshapeTensorC2(nullptr, tensor, source);
}

extern "C" Tensor nNet_ReshapeTensorC2(Interpreter interpreter, Tensor tensor,
                                       Tensor source) {
  ShapeDef fakeshape;

  if (!source || nn::utils::outdate(interpreter, source))
    return nullptr;

  /* @NOTE: create a fake-shape and use nNet_ReshapeTensorS */

  fakeshape.ndims = source->ndims;
  fakeshape.dims  = source->dims;

  return nNet_ReshapeTensorC1(interpreter, tensor, &fakeshape);
}

extern "C" long nNet_GetArraySizeTC(Interpreter interpreter, Tensor tensor) {
  if (!tensor || nn::utils::outdate(interpreter, tensor))
    return -1;
  return nNet_GetTensorSize(tensor);
}

extern "C" long nNet_GetArraySizeSC(Interpreter interpreter, Shape shape) {
  long result = 1;

  if (!shape || nn::utils::outdate(interpreter, shape))
    return -1;

  for (auto i = 0; i < shape->ndims; ++i)
    result *= shape->dims[i];
  return result;
}

extern "C" long nNet_GetArraySizeT(Tensor tensor) {
  if (!tensor) return -1;
  return nNet_GetTensorSize(tensor);
}

extern "C" long nNet_GetArraySizeS(Shape shape) {
  long result = 1;

  if (!shape || nn::utils::outdate(shape))
    return -1;

  for (auto i = 0; i < shape->ndims; ++i)
    result *= shape->dims[i];
  return result;
}

extern "C" bool nNet_CheckTensorOutdate(Interpreter interpreter, Tensor tensor){
  if (interpreter){
    auto interw = reinterpret_cast<nn::InterpreterW*>(interpreter->priv);
    return interw->findPointer(tensor, TENSOR_T);
  } else 
    return !nn::utils::outdate(tensor);
}

extern "C" bool nNet_CheckTensorShapeOutdate(Interpreter interpreter, Shape shape){
  if (interpreter){
    auto interw = reinterpret_cast<nn::InterpreterW*>(interpreter->priv);
    return interw->findPointer(shape, SHAPE_T);
  } else 
    return !nn::utils::outdate(shape);
}

extern "C" bool nNet_CheckOrdinateOutdate(Interpreter interpreter, 
                                          Ordinate ordinate){
  if (interpreter){
    auto interw = reinterpret_cast<nn::InterpreterW*>(interpreter->priv);
    return interw->findPointer(ordinate, ORDINATE_T);
  } else 
    return !nn::utils::outdate(ordinate);
}

extern "C" Device nNet_GetDeviceOfTensor(Tensor tensor){
  return nNet_GetDeviceOfTensorW(nullptr, tensor);
}

extern "C" Device nNet_GetDeviceOfTensorW(Interpreter interpreter,
                                              Tensor tensor){
  nn::InterpreterW* interpreterw = nullptr;

  if (tensor->device)
    return tensor->device(tensor, interpreter);
  else if (!interpreter)
    interpreterw = &nn::InterpreterW::instance();
  else 
    interpreterw = reinterpret_cast<nn::InterpreterW*>(interpreter->priv);

  if (interpreterw){
    for (auto& device: interpreterw->devices()){
      auto sample = reinterpret_cast<Device>(tensor->_device);
      if (std::get<1>(device) == sample)
        return sample;
    }
  }
  return nullptr;
}

extern "C" CString nNet_GetTensorTypeNameW(Interpreter interpreter, Tensor tensor){
  if (!nn::utils::outdate(interpreter, tensor))
    return "Unknown";
  return nNet_GetTensorTypeName(tensor);
}

extern "C" int nNet_GetTensorTypeW(Interpreter interpreter, Tensor tensor){
  if (!nn::utils::outdate(interpreter, tensor))
    return Unknown;
  return nNet_GetTensorType(tensor);
}

extern "C" bool nNet_UseTemporaryTensor(Interpreter interpreter, Tensor tensor){
  auto interpreterw = nNet_GetInterpreterW(interpreter);

  if (interpreterw)
    return interpreterw->assignPointer(tensor, TENSOR_T, true);
  else
    return false;
}

extern "C" bool nNet_KillTemporaryTensor(Interpreter interpreter, Tensor tensor){
  auto interpreterw = nNet_GetInterpreterW(interpreter);

  if (interpreterw){
    if (!interpreterw->findPointer(tensor, TENSOR_T))
      return false;
    else if (!interpreterw->isDummyPointer(tensor))
      return false;
    else{
      interpreterw->removePointer(tensor);
      return true;
    }
  }
  return false;
}

#if USE_VECTORIZE
#if !(HAS_C_ALIGNED_ALLOC || HAS_C_POSIX_MEMALIGN || HAS_C_ALIGNED_MALLOC)
#if HAS_CXX_ALIGNED_ALLOC || HAS_CXX_POSIX_MEMALIGN || HAS_CXX_ALIGNED_MALLOC
extern "C" Array nNet_AllocateArray(long size, int type) {
  int   aligned = nNet_AlignedSizeOfType(type);
  Array result  = nullptr;

  size = size*nNet_SizeType(type);
  size = size + size%aligned;
  size = size < aligned? aligned: size;

 #if HAS_CXX_ALIGNED_ALLOC
  result = aligned_alloc(aligned, size);

  if (!result)
    result = malloc(size);

  return result;
 #elif HAS_CXX_POSIX_MEMALIGN
  int   err_code = 0;

  if (aligned % sizeof(void*) || aligned <= sizeof(void*))
    return calloc(1, size);  
  else if (!(err_code = posix_memalign(&result, aligned, size)))
    return result;
  else if (err_code == EINVAL){
    BadAccess << "The alignment argument(" << std::to_string(aligned)
              << ") was not a power of two, or was "
              << "not a multiple of sizeof(void *).";
  } else if (err_code == ENOMEM){
    DrainMem << "There was enough memory to fulfill the allocation request"
             << std::to_string(size);
    if (!result)
      result = malloc(size);
  } else {
    WatchErrno;
  }
  return result;
 #elif HAS_CXX_ALIGNED_MALLOC
  result = _aligned_malloc(size, aligned);

  if (!result)
    result = malloc(size);

  return result;
 #else
  return calloc(1, size);
 #endif
}
#endif
#endif
#endif // USE_VECTORIZE
