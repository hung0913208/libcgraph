#include "interpreter.h"
#include "internal.h"
#include "utils.h"

#include <string>
#include <map>

#define OPERATOR_T 3
using namespace nn::interpreter;

namespace nn{
namespace global{
extern std::map<std::string, model::Attribute> attributes;
} // namespace global

namespace interpreter{
bool OperatorW::error(base::Error&& error) const{
  if (_interpreter && error){
    nNet_GetInterpreterW(_interpreter)->error(rvalue(error));
  }
  return error;
}

bool OperatorW::error(base::Error& error) const{
  if (_interpreter && error){
    nNet_GetInterpreterW(_interpreter)->error(const_cast<base::Error&>(error));
  }
  return error.code != 0;
}

OperatorW::operator bool(){ 
  return nNet_GetInterpreterW(_interpreter)->error().code != 0;
}

bool OperatorW::inputs(Operator op, CString* names, int size){
  auto opw = nNet_GetOperatorW(op);

  if (!opw) return true;
  for (auto i = 0; i < size; ++i)
    opw->_inputs.push_back(std::make_pair(strdup(names[i]), Tensor{nullptr}));
  return false;
}

bool OperatorW::link(Operator op, Operator* nodes, int size){
  auto opw   = nNet_GetOperatorW(op);
  auto name  = std::string{};
  auto index = 0;

  /* @NOTE: check opw and opw->output to prevent running multi-time or 
   * op is created illegal or */
  op->link = nullptr;

  if (!opw) return true;
  else if (opw->inputs().size() == 0)
    return opw << (BadLogic.reason("op " + std::string{op->name} + 
                                      " must have at least 1 input"));

  for (auto& input: opw->inputs()){
    auto iparam = std::get<0>(input);

    utils::wstring::tie(name, index) = utils::wstring::split(iparam, ':');

    /* @NOTE: link outputs from nodes to inputs of this operator */
    for (auto id_node = 0; id_node < size; ++id_node){
      auto nodew = nNet_GetOperatorW(nodes[id_node]);
      auto gate  = Tensor{nullptr};

      /* @NOTE: search the gate by searching node and index of node's output */
      if (name != nodes[id_node]->name) continue;
      else if (nodes[id_node]->outputs){
        auto outputs = (Tensor*){nullptr};
        auto ts_size = 0;

        if (nodes[id_node]->outputs(nodes[id_node], &outputs, &ts_size))
          return true;

        gate = outputs[index];
      } else if (cast_(index, nodew->outputs().size()) > index)
        gate = nodew->outputs()[index];
      else
        return opw << (NotFound.reason("gate \'" + std::string{iparam} + "\'"));

      if (gate)
        std::get<1>(input) = gate;
      break;
    }
  }
  return false;
}

interpreter::OperatorW::Config OperatorW::config(CString model, CString op){
  return global::attributes[std::string{model}](std::string{op});
}

OperatorW::OperatorW(Operator op, std::size_t max_attrs, 
                     Interpreter interpreter, Config config):
  updateAttr{config}, 
  _attrs{max_attrs, [](std::string &&key){ 
    return std::hash<std::string>{}(key); 
  }}, 
  _interpreter{interpreter},
  _connected{false}
{
  if (!nNet_IsTemplateOperator(op))
    throw NoSupport.reason("OperatorW must be created from a template operator");
  if (!(_operator = (Operator) calloc(1, sizeof(OperatorDef))))
    throw DrainMem;

  memcpy(&_operator->_event, &op->_event, sizeof(Event));
  _operator->device = nullptr;
  _operator->priv   = this; 

  nNet_GetInterpreterW(interpreter)->assignOperator(_operator, _notouched);

  isConnected.setter([&](bool value){
    if (value != _connected){
      _connected    = value;
      (*_notouched) = false;
    }
  });
  isConnected.getter([&]() -> bool&{ return _connected; });
}

OperatorW::OperatorW(Operator op, std::size_t max_attrs, Interpreter interpreter, 
                     Device device, Config config):
  updateAttr{config},
  _attrs{max_attrs, [](std::string &&key){ 
    return std::hash<std::string>{}(key); 
  }}, 
  _interpreter{interpreter},
  _connected{false}
{
  auto interpreterw = nNet_GetInterpreterW(interpreter);
  auto opname       = interpreterw? interpreterw->findOperator(op): nullptr;

  if (!nNet_IsTemplateOperator(op))
    throw NoSupport.reason("OperatorW must be created from a template operator");
  if (!(_operator = (Operator) calloc(1, sizeof(OperatorDef))))
    throw DrainMem;

  memcpy(&_operator->_event, &op->_event, sizeof(Event));
  _operator->device     = device;
  _operator->priv       = this; 
  _operator->link       = device? OperatorW::link: nullptr;
  _operator->inputnames = OperatorW::inputs;

  for (auto& item: interpreterw->devices()){
    auto device = std::get<1>(item);

    if (isSupported(item))
      continue;
    if (device->_template.find(device, opname)){
      _supports.push_back(device);
    }
  }

  interpreterw->assignOperator(_operator, _notouched);
  isConnected.setter([&](bool value){
    if (value != _connected){
      _connected    = value;
      (*_notouched) = false;
    }
  });
  isConnected.getter([&]() -> bool&{ return _connected; });
}

OperatorW::~OperatorW(){
  for (auto& input: _inputs){
    free(const_cast<char*>(std::get<0>(input)));
  }
}

bool OperatorW::isSupported(std::pair<const std::string, Device>& sample){
  CString sample_name;

  if (nNet_GetDeviceW(std::get<1>(sample)))
    sample_name = nNet_GetDeviceW(std::get<1>(sample))->name().c_str();
  else{
    sample_name = strndup(std::get<0>(sample).c_str(), 
                          std::get<0>(sample).find(':'));
  }

  for (auto& branch: _supports){
    if (nNet_GetDeviceW(branch)){
      if (nNet_GetDeviceW(branch)->name() == sample_name){
        if (!nNet_GetDeviceW(std::get<1>(sample)))
          free(const_cast<char*>(sample_name));
        return true;
      }
    }
  }

  if (!nNet_GetDeviceW(std::get<1>(sample)))
    free(const_cast<char*>(sample_name));
  return false;
}

template<>
bool OperatorW::set<char*>(std::string&& name, char* value){
  auto tmp = base::Auto{}.set(value);
  
  tmp.delete_() = [](void* data){
    free(((char**)data)[0]);
    delete (char**)data;
  };
  return _attrs.put(name, tmp) != -1;
}

template<>
base::Auto OperatorW::get<base::Auto>(std::string&& name){
  return _attrs.get(name);
}

template<>
bool OperatorW::set<base::Auto>(std::string&& name, base::Auto value){
  return _attrs.put(name, value) != -1;
}
} // namespace interpreter
} // namespace nn

OperatorW& operator<<(OperatorW* op, base::Error&& error){
  op->error(error);
  return *op;
}

OperatorW& operator<<(const OperatorW& op, base::Error&& error){
  op.error(error);
  return const_cast<OperatorW&>(op);
}

OperatorW& operator<<(OperatorW* op, base::Error& error){
  op->error(error);
  return *op;
}

OperatorW& operator<<(const OperatorW& op, base::Error& error){
  op.error(error);
  return const_cast<OperatorW&>(op);
}
