#ifdef LIBCGRAPH_GIT_REFSPEC
#include "libbase/all.hpp"
#else
#include "trace.hpp"
#endif
#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
void InterpreterW::render(){
  /* @TODO: can nhieu thoi gian hon de design ham render */

  for (auto& layer: _flowchart){
    for (auto& node: std::get<1>(layer))
      base::trace::console << std::string{node->name} << "\t";

    base::trace::console << base::trace::eol;
  }
}
} // namespace nn
