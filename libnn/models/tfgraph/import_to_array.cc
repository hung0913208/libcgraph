#include "tfgraph.h"

namespace nn{
namespace tfgraph{
namespace tensor{
void copyToBuffer(const TensorProto& input, Array output){
  memcpy(output, input.tensor_content().c_str(), input.tensor_content().size());
}

base::Error importToRArray(const TensorProto& input, Array output, int size){
  auto output_float_data = reinterpret_cast<Real32*>(output);

  if (input.float_val_size() == 1){
    for (int i = 0; i < size; i++)
      output_float_data[i] = input.float_val(0);
  } else if (input.float_val_size() == size){
    for (int i = 0; i < input.float_val_size(); i++)
      output_float_data[i] = input.float_val(i);
  } else if (squeeze(int, input.tensor_content().size()) == 
             size*nNet_SizeType(Real32_t))
    copyToBuffer(input, output);
  else return BadLogic;

  return NoError;
}

base::Error importToDRArray(const TensorProto& input, Array output, int size){
  auto output_float_data = reinterpret_cast<Real64*>(output);

  if (input.float_val_size() == 1){
    for (int i = 0; i < size; i++)
      output_float_data[i] = input.float_val(0);
  } else if (input.float_val_size() == size){
    for (int i = 0; i < input.float_val_size(); i++)
      output_float_data[i] = input.float_val(i);
  } else if (squeeze(int, input.tensor_content().size()) == 
             size*nNet_SizeType(Real64_t))
    copyToBuffer(input, output);
  else return BadLogic;

  return NoError;
}

base::Error importToUIntArray(const TensorProto& input, Array output, int size){
  if (input.int_val_size()){
    auto output_int_data = reinterpret_cast<UInt8*>(output);

    for (int i = 0; i < input.int_val_size(); i++)
      output_int_data[i] = input.int_val(i);
  } else if (squeeze(int, input.tensor_content().size()) == 
             size*nNet_SizeType(UInt_t))
    copyToBuffer(input, output);
  else return BadLogic;

  return NoError;
}

base::Error importToDWArray(const TensorProto& input, Array output, int size){
  if (input.int_val_size()){
    auto output_int_data = reinterpret_cast<Int32*>(output);

    for (int i = 0; i < input.int_val_size(); i++)
      output_int_data[i] = input.int_val(i);
  } else if (int(input.tensor_content().size()) == size*nNet_SizeType(Int32_t))
    copyToBuffer(input, output);
  else return BadLogic;

  return NoError;
}

base::Error importToLWArray(const TensorProto& input, Array output, int size){
  if (input.int64_val_size()) {
    auto output_int_data = reinterpret_cast<Int64*>(output);

    for (int i = 0; i < input.int64_val_size(); i++)
      output_int_data[i] = input.int64_val(i);
  } else if (squeeze(int, input.tensor_content().size()) ==
             size*nNet_SizeType(Int64_t))
    copyToBuffer(input, output);
  else return BadLogic;

  return NoError;
}

base::Error importToBoolArray(const TensorProto& input, Array output, int size){
  auto output_bool_data = reinterpret_cast<bool*>(output);

  if (input.bool_val_size()){
    for (auto i = 0; i < cast_(i, input.bool_val_size()); i++)
      output_bool_data[i] = input.bool_val(i);
  } else if (squeeze(int, input.tensor_content().size()) == size) {
    std::vector<char> buf(input.tensor_content().size());

    copyToBuffer(input, buf.data());
    for (auto i = 0; i < cast_(i, input.tensor_content().size()); i++)
      output_bool_data[i] = static_cast<bool>(buf[i]);
  } else if (size != 1)
    return BadLogic;
  else output_bool_data[0] = false;
  return NoError;
}

base::Error importToStrArray(const TensorProto& input, Array output, int size){
  auto output_string_data = reinterpret_cast<CString*>(output);

  if (size != input.string_val_size())
    return BadLogic;

  for (auto i = 0; i < size; ++i)
    output_string_data[i] = strdup(input.string_val(i).c_str());

  return NoError;
}

} // namespace tensor
} // namespace tfgraph
} // namespace nn