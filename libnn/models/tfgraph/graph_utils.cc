#include "tfgraph.h"
#include "internal.h"

using tensorflow::DT_BOOL;
using tensorflow::DT_FLOAT;
using tensorflow::DT_DOUBLE;
using tensorflow::DT_INT32;
using tensorflow::DT_INT64;
using tensorflow::DT_QUINT8;
using tensorflow::DT_STRING;
using tensorflow::DT_UINT8;
using tensorflow::DT_INVALID;

using tensorflow::AttrValue;
using tensorflow::GraphDef;
using tensorflow::NodeDef;
using tensorflow::DataType;
using tensorflow::TensorProto;
using tensorflow::TensorShapeProto;
using tensorflow::AttrValue_ListValue;

namespace nn{
namespace tfgraph{
namespace attr{
using namespace std;

bool has(const NodeDef& node, const string& attr_name){
  return node.attr().count(attr_name) > 0;
}

int64_t getInt(const NodeDef& node, const string& attr_name){
  if (has(node, attr_name)){
    const auto& attr = node.attr().at(attr_name);

    if (attr.value_case() == AttrValue::kI)
      return attr.i();
  }
  throw NotFound;
}

float getFloat(const NodeDef& node, const string& attr_name){
  if (has(node, attr_name)){
    const auto& attr = node.attr().at(attr_name);

    if (attr.value_case() == AttrValue::kF)
    return attr.f();
  }
  throw NotFound;
}

bool getBool(const NodeDef& node, const string& attr_name){
  if (has(node, attr_name)){
    const auto& attr = node.attr().at(attr_name);

    if (attr.value_case() == AttrValue::kB)
      return attr.b();
  }
  throw NotFound;
}

DataType getDataType(const NodeDef& node, const string& attr_name){
  if (has(node, attr_name)){
    const auto& attr = node.attr().at(attr_name);

  if (attr.value_case() == AttrValue::kType)
    return attr.type();
  }
  throw NotFound;
}

const string& getString(const NodeDef& node, const string& attr_name){
  if (has(node, attr_name)){
    const auto& attr = node.attr().at(attr_name);
    
    if (attr.value_case() == AttrValue::kS)
      return attr.s();
  }
  throw NotFound;
}

const TensorShapeProto& getShape(const NodeDef& node, const string& attr_name){
  if (has(node, attr_name)){
    const auto& attr = node.attr().at(attr_name);

    if (attr.value_case() == AttrValue::kShape)
      return attr.shape();
  }
  throw NotFound;
}

const TensorProto& getTensor(const NodeDef& node, const string& attr_name){
  if (has(node, attr_name)){
    const auto& attr = node.attr().at(attr_name);

    if (attr.value_case() == AttrValue::kTensor)
      return attr.tensor();
  }
  throw NotFound;
}

const AttrValue::ListValue& getList(const NodeDef& node, const string& attr_name){
  if (has(node, attr_name)){
    const auto& attr = node.attr().at(attr_name);
  
    if (attr.value_case() == AttrValue::kList)
      return attr.list();
  }
  throw NotFound;
}
} // namespace attr
} // namespace tfgraph
} // namespace nn
using namespace base;
using namespace std;

template<> vector<int> nn::attr::ListValue2Array<int>(Auto& attr){
  std::vector<int> result;

  if (attr.type().get() != typeid(const AttrValue_ListValue&)){}
  return result;
}

template<> vector<float> nn::attr::ListValue2Array<float>(Auto& attr){
  vector<float> result;

  if (attr.type().get() != typeid(const AttrValue_ListValue&)){}
  return result;
}

template<> vector<bool> nn::attr::ListValue2Array<bool>(Auto& attr){
  std::vector<bool> result;

  if (attr.type().get() != typeid(const AttrValue_ListValue&)){}
  return result;
}

template<> vector<Shape> nn::attr::ListValue2Array<Shape>(Auto& attr){
  std::vector<Shape> result;

  if (attr.type().get() != typeid(const AttrValue_ListValue&)){}
  return result;
}

template<> vector<Tensor> nn::attr::ListValue2Array<Tensor>(Auto& attr){
  std::vector<Tensor> result;

  if (attr.type().get() != typeid(const AttrValue_ListValue&)){}
  return result;
}
