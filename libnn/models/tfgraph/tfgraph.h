#if !defined(LIBNN_MODEL_TFGRAPH_H_) && __cplusplus
#define LIBNN_MODEL_TFGRAPH_H_

#define TFGRAPH "tfgraph"

#ifndef DISABLE_PROTOBUF
#include "graph.pb.h"
#include "node_def.pb.h"
#include "tensor.pb.h"
#include "attr_value.pb.h"
#endif

#ifdef LIBCGRAPH_GIT_REFSPEC
#include "libbase/logcat.hpp"
#else
#include "logcat.hpp"
#endif
#include "interpreter.h"
#include "internal.h"

namespace nn{
#ifndef DISABLE_PROTOBUF
namespace tfgraph{
using tensorflow::AttrValue;
using tensorflow::DT_BOOL;
using tensorflow::DT_FLOAT;
using tensorflow::DT_INT32;
using tensorflow::DT_INT64;
using tensorflow::DT_QUINT8;
using tensorflow::DT_STRING;
using tensorflow::DT_UINT8;
using tensorflow::DataType;
using tensorflow::GraphDef;
using tensorflow::NodeDef;
using tensorflow::TensorProto;
using tensorflow::TensorShapeProto;

namespace tensor{
base::Error importToRArray(const TensorProto& input, Array output, int size);
base::Error importToDRArray(const TensorProto& input, Array output, int size);
base::Error importToUIntArray(const TensorProto& input, Array output, int size);
base::Error importToDWArray(const TensorProto& input, Array output, int size);
base::Error importToLWArray(const TensorProto& input, Array output, int size);
base::Error importToStrArray(const TensorProto& input, Array output, int size);
} // namespace tensor

namespace attr{
bool has(const NodeDef& node, const std::string& attr_name);

bool     getBool(const NodeDef& node, const std::string& name);
int64_t  getInt(const NodeDef& node, const std::string& name);
float    getFloat(const NodeDef& node, const std::string& name);
DataType getDataType(const NodeDef& node, const std::string& name);

const std::string&      getString(const NodeDef& node, const std::string& name);
const TensorShapeProto& getShape(const NodeDef& node, const std::string& name);
const TensorProto&      getTensor(const NodeDef& node, const std::string& name);
const AttrValue::ListValue& getList(const NodeDef& node, const std::string& name);
} // namespace attr
} // namespace tfgraph
#endif

namespace attr{
template<typename Type> 
std::vector<Type> ListValue2Array(base::Auto& UNUSED(attr)){
  throw NoSupport;
}

template<> 
std::vector<int> ListValue2Array<int>(base::Auto& attr);
template<> 
std::vector<float> ListValue2Array<float>(base::Auto& attr);
template<> 
std::vector<bool> ListValue2Array<bool>(base::Auto& attr);
template<> 
std::vector<Shape> ListValue2Array<Shape>(base::Auto& attr);
template<> 
std::vector<Tensor> ListValue2Array<Tensor>(base::Auto& attr);
} // namespace attr

namespace build{
base::Error tfgraph(std::string&& model, InterpreterW& interpreter, bool lazyload);
} // namespace build

namespace save{
base::Error tfgraph(InterpreterW& interpreter, std::string& model);
} // namespace save

namespace tfgraph{
enum Type{
  DataType_t = 10
};

namespace kernels{
interpreter::OperatorW::Config attribute(std::string&& op);
} // namespace kernels;

bool load();
void exit();
} // namespace tfgraph
} // namespace nn
#endif  // LIBNN_MODEL_TFGRAPH_H_
