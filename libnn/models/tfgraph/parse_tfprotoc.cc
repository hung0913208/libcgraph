#include <google/protobuf/text_format.h>

#include "tfgraph.h"
#include "utils.h"

using tensorflow::DT_BOOL;
using tensorflow::DT_FLOAT;
using tensorflow::DT_DOUBLE;
using tensorflow::DT_INT32;
using tensorflow::DT_INT64;
using tensorflow::DT_QUINT8;
using tensorflow::DT_STRING;
using tensorflow::DT_UINT8;
using tensorflow::DT_INVALID;

using tensorflow::AttrValue;
using tensorflow::GraphDef;
using tensorflow::NodeDef;
using tensorflow::DataType;
using tensorflow::TensorProto;
using tensorflow::TensorShapeProto;

namespace nn {
namespace internal {
using Inputs = std::vector<char*>;
using Attrs  = std::map<std::string, base::Auto>;

Tensor flatten(const TensorProto& proto, InterpreterW& interpreter){
  base::Error error;
  Array array;

  using namespace nn::tfgraph::tensor;
  if (interpreter.error())
    return nullptr;
  else if (proto.dtype() == DT_INVALID)
    return nullptr;
  else {
    auto dims = std::vector<int>{};
    auto tensor = Tensor{nullptr};
    auto shape = Shape{nullptr};
    auto size = 0;

    for (auto item : proto.tensor_shape().dim()) {
      dims.push_back(item.size());
    }
    std::reverse(dims.begin(), dims.end());

    if (dims.size() > 0){
      shape  = nNet_CreateTensorShapeC2(&interpreter.interface(), dims.size(), 
                                        dims.data());
      size   = nNet_GetArraySizeSC(&interpreter.interface(), shape);
      tensor = nNet_CreateTensorC(&interpreter.interface(), shape);
      shape  = nNet_RemoveTensorShapeW(&interpreter.interface(), shape);
    } else {
      tensor = nNet_CreateTensorC(&interpreter.interface(), nullptr);
    }

    /* @NOTE: copy data from TFProto to Tensor */
    if (tensor) {
      switch (proto.dtype()) {
        case DT_FLOAT:
          if (!nNet_SetTensorNumT(tensor, Real32_t))
            return nullptr;
          else
            array = nNet_GetArrayData(tensor);

          if (!array)
            return nNet_RemoveTensorW(&interpreter.interface(), tensor);
          else if ((error = importToRArray(proto, array, size))) {
            tensor = nNet_RemoveTensorW(&interpreter.interface(), tensor);
            interpreter.error(error);
          } else nNet_SetArrayData(tensor, array);
          break;

        case DT_DOUBLE:
          if (!nNet_SetTensorNumT(tensor, Real64_t))
            return nNet_RemoveTensorW(&interpreter.interface(), tensor);
          else
            array = nNet_GetArrayData(tensor);

          if (!array)
            return nNet_RemoveTensorW(&interpreter.interface(), tensor);
          else if ((error = importToDRArray(proto, array, size))) {
            tensor = nNet_RemoveTensorW(&interpreter.interface(), tensor);
            interpreter.error(error);
          } else nNet_SetArrayData(tensor, array);
          break;

        case DT_INT32:
          if (!nNet_SetTensorNumT(tensor, Int32_t))
            return nullptr;
          else
            array = nNet_GetArrayData(tensor);

          if (!array)
            return nNet_RemoveTensorW(&interpreter.interface(), tensor);
          else if ((error = importToDWArray(proto, array, size))) {
            tensor = nNet_RemoveTensorW(&interpreter.interface(), tensor);
            interpreter.error(error);
          } else nNet_SetArrayData(tensor, array);
          break;

        case DT_QUINT8:
          if (!nNet_SetTensorNumT(tensor, UInt_t))
            return nullptr;
          else
            array = nNet_GetArrayData(tensor);

          if (!array)
            return nNet_RemoveTensorW(&interpreter.interface(), tensor);
          else if ((error = importToUIntArray(proto, array, size))) {
            tensor = nNet_RemoveTensorW(&interpreter.interface(), tensor);
            interpreter.error(error);
          } else nNet_SetArrayData(tensor, array);
          break;

        case DT_INT64:
          if (!nNet_SetTensorNumT(tensor, Int64_t))
            return nullptr;
          else
            array = nNet_GetArrayData(tensor);

          if (!array)
            return nNet_RemoveTensorW(&interpreter.interface(), tensor);
          else if ((error = importToLWArray(proto, array, size))) {
            tensor = nNet_RemoveTensorW(&interpreter.interface(), tensor);
            interpreter.error(error);
          } else nNet_SetArrayData(tensor, array);
          break;

        case DT_STRING:
          if (!nNet_SetTensorNumT(tensor, String_t))
            return nNet_RemoveTensorW(&interpreter.interface(), tensor);
          else
            array = nNet_GetArrayData(tensor);

          if (!array)
            return nNet_RemoveTensorW(&interpreter.interface(), tensor);
          else if ((error = importToStrArray(proto, array, size))) {
            tensor = nNet_RemoveTensorW(&interpreter.interface(), tensor);
            interpreter.error(error);
          } else nNet_SetArrayData(tensor, array);
          break;

        case DT_BOOL:
          if (!nNet_SetTensorNumT(tensor, Bool_t)) return nullptr;
          break;

        default:
          return nNet_RemoveTensorW(&interpreter.interface(), tensor);
      }
    }
    return tensor;
  }
}

Shape flatten(const TensorShapeProto& proto, InterpreterW& interpreter){
  auto dim = std::vector<int> {};

  for (auto i = proto.dim_size(); i > 0; --i){
    dim.push_back(proto.dim(i - 1).size());
  }

  return nNet_CreateTensorShapeC2(&interpreter.interface(), dim.size(), dim.data());
}

base::Auto flatten(const AttrValue::ListValue& list, InterpreterW& interpreter){
  base::Auto result{};

  if (result.type().get() == typeid(nullptr) && list.s_size() > 0){
    std::vector<char*> buffer{};

    for (auto& s: list.s())
      buffer.push_back(const_cast<char*>(s.c_str()));
    result.set(rvalue(buffer));
  }
  if (result.type().get() == typeid(nullptr) && list.i_size() > 0){
    std::vector<int64_t> buffer{};

    for (auto& i: list.i()) buffer.push_back(i);
    result.set(rvalue(buffer));
  }
  if (result.type().get() == typeid(nullptr) && list.f_size() > 0){
    std::vector<float> buffer{};

    for (auto& f: list.f()) buffer.push_back(f);
    result.set(rvalue(buffer));
  }
  if (result.type().get() == typeid(nullptr) && list.b_size() > 0){
    std::vector<bool> buffer{};

    for (auto& b: list.b()) buffer.push_back(b);
    result.set(rvalue(buffer));
  }
  if (result.type().get() == typeid(nullptr) && list.type_size() > 0){
    std::vector<NumT> buffer{};

    for (auto type: list.type()){
      if (type == DT_DOUBLE)
        buffer.push_back(Real64_t);
      else if (type == DT_FLOAT)
        buffer.push_back(Real32_t);
      else if (type == DT_BOOL)
        buffer.push_back(Bool_t);
      else if (type == DT_INT64)
        buffer.push_back(Int64_t);
      else if (type == DT_INT32)
        buffer.push_back(Int32_t);
      else if (type == DT_STRING)
        buffer.push_back(String_t);
    }
    result.set(rvalue(buffer));
  }
  if (result.type().get() == typeid(nullptr) && list.shape_size() > 0){
    std::vector<Shape> buffer{};

    for (auto& shape: list.shape())
      buffer.push_back(flatten(shape, interpreter));
    result.set(rvalue(buffer));
  }
  if (result.type().get() == typeid(nullptr) && list.tensor_size() > 0){
    std::vector<Tensor>buffer{};

    for (auto& tensor: list.tensor())
      buffer.push_back(flatten(tensor, interpreter));
    result.set(rvalue(buffer));
  }
  return result;
}

Inputs getInputs(const tensorflow::NodeDef& node){
  Inputs result{};

  for (auto& name : node.input())
    result.push_back(const_cast<char*>(name.c_str()));
  return result;
}

Attrs getAttrs(const tensorflow::NodeDef& node, InterpreterW& interpreter){
  Attrs  result{};
  Tensor tensor{nullptr};
  Shape  shape{nullptr};

  for (auto item : node.attr()){
    auto& name = item.first;
    auto& attr = item.second;

    if (interpreter.error())
      throw interpreter.error();

    switch (attr.value_case()){
    case AttrValue::kI:
      result[name] = base::Auto{}.set(attr.i());
      break;

    case AttrValue::kS:
    {
      /* @NOTE: std::string allocates exactly with the size of string but 
       * CString requires adding character '\0' at the end of the string and 
       * it will cause bad access when we use strlen() with std::string.c_str()
       */

      auto dup = (char*) calloc(attr.s().size() + 1, sizeof(char));

      if (!dup) throw DrainMem;
      else {
        memcpy(dup, attr.s().c_str(), attr.s().size());
        result[name] = base::Auto{}.set(dup);
        break;
      }
    }

    case AttrValue::kF:
      result[name] = base::Auto{}.set(attr.f());
      break;

    case AttrValue::kB:
      result[name] = base::Auto{}.set(attr.b());
      break;

    case AttrValue::kType:
      result[name] = base::Auto{}.set(attr.type());
      break;

    case AttrValue::kShape:
      shape = flatten(attr.shape(), interpreter);

      if (!shape) throw BadLogic;
      else
        result[name] = base::Auto{}.set(shape);
      break;

    case AttrValue::kTensor:
      tensor = flatten(attr.tensor(), interpreter);

      if (!tensor) throw BadLogic;
      else
        result[name] = base::Auto{}.set(tensor);
      break;

    case AttrValue::kList:
      result[name] = flatten(attr.list(), interpreter);
      break;

    case AttrValue::kPlaceholder:
    case AttrValue::kFunc:
    case AttrValue::VALUE_NOT_SET:
      break;
    }
  }

  return result;
}

base::Auto api(base::Auto input, const std::type_info& type){
  base::Auto result{};

  if (type == typeid(NumT)) {
    if (input.type().get() != typeid(tensorflow::DataType))
      throw NotFound.reason("attr with type \'tensorflow::DataType\'");
    else if (input.get<tensorflow::DataType>() == DT_DOUBLE)
      result = Real64_t;
    else if (input.get<tensorflow::DataType>() == DT_FLOAT)
      result = Real32_t;
    else if (input.get<tensorflow::DataType>() == DT_BOOL)
      result = Bool_t;
    else if (input.get<tensorflow::DataType>() == DT_INT64)
      result = Int64_t;
    else if (input.get<tensorflow::DataType>() == DT_INT32)
      result = Int32_t;
    else if (input.get<tensorflow::DataType>() == DT_STRING)
      result = String_t;
    else
      result = Unknown;

    return result;
  }
  return input;
}

base::Error getAttrs(const tensorflow::NodeDef& node, InterpreterW& interpreter,
                     std::vector<CPair>& result){
  return NoSupport;
}

CAny capi(CAny input, int type){
  CAny result;

  if (type == tfgraph::DataType_t) {
    if (input.type != tfgraph::DataType_t)
      throw NotFound.reason("attr with type \'tensorflow::DataType\'");

    result.value = malloc(sizeof(int));
    result.size  = sizeof(int);
    result.type  = NumT_t;

    if (*((int*)input.value) == DT_DOUBLE)
      *((int*)result.value) = Real64_t;
    else if (*((int*)input.value) == DT_FLOAT)
      *((int*)result.value) = Real32_t;
    else if (*((int*)input.value) == DT_BOOL)
      *((int*)result.value) = Bool_t;
    else if (*((int*)input.value) == DT_INT64)
      *((int*)result.value) = Int64_t;
    else if (*((int*)input.value) == DT_INT32)
      *((int*)result.value) = Int32_t;
    else if (*((int*)input.value) == DT_STRING)
      *((int*)result.value) = String_t;
    else
      *((int*)result.value) = Unknown;

    result.release = [](CAny* context){ 
      if (context->value)
        free(context->value);

      context->value = NULL;
      context->type  = Unknown;
      context->size  = 0;
    };
    return result;
  }
  return input;
}
} // namespace internal

namespace build{
base::Error tfgraph(std::string&& model, InterpreterW& interpreter, bool lazyload){
  using namespace nn::utils;

  tensorflow::GraphDef graph;
  std::vector<Operator> ops;

  base::Boundary b{[]() {},
    [&]() {
      if (interpreter.error()){
        for (auto i = 0; i < cast_(i, ops.size()); ++i) {
          if (!ops[i])
            continue;
          else
            nNet_RemoveOperatorW(&interpreter.interface(), ops[i]);
        }
      } else if (!lazyload){
        /* @NOTE: remove finished operators */

        for (auto i = 0; i < cast_(i, ops.size()); ++i) {
          if (ops[i]->type)
            nNet_RemoveOperatorW(&interpreter.interface(), ops[i]);
        }
      }
  }};

  using namespace interpreter;
  using Inputs = std::vector<char*>;
  using OperatorW = interpreter::OperatorW*;

  if (!graph.ParseFromString(model)){
    if (!google::protobuf::TextFormat::ParseFromString(model, &graph)){
      return NoSupport.reason("this model didn\'t created from tensorflow");
    }
  }

  /* @NOTE: step 1: load nodes to a buffer and config them */
  for (auto& node : graph.node()) {
    auto pattern = interpreter.findOperator("tfgraph", node.op());
    auto error   = NoError;
    auto device  = pattern? pattern->device: nullptr;
  
    Operator building{nullptr};
    OperatorW opw{nullptr};
    Inputs inputs{};

    /* @NOTE: load a template from interpreter and build a new operator */
    if (!pattern)
      return (interpreter.error(NotFound.reason(std::string{node.op()})));

    if (!pattern->device && !pattern->type){
      pattern->device = interpreter.findDevice(pattern);
    }

    building = pattern->_event.prepare(pattern, &interpreter.interface(), TFGRAPH);
    pattern->device = device;

    if (!building)
      return (interpreter.error(DoNothing << node.op() << " is still blank"));
    else if (building->type != pattern->type)
      building->type = pattern->type;

    if (!building->device && !building->type){
      building->device = interpreter.findDevice(pattern);
    }

    ops.push_back(building);

    /* @NOTE: naming our operator, this name must be used to link operators
     * after loading steps */
    if (node.name().size() > 0) {
      if (building->naming) {
        if (building->naming(building, node.name().c_str()))
          return interpreter.error() ? interpreter.error()
                                     : (interpreter.error(BadLogic));
      }
      if (!building->name && !building->naming)
        building->name = strdup(node.name().c_str());
    }

    /* @NOTE: prepare operator's input-tensors and load them from GraphDef */
    if (building->inputnames) {
      inputs = internal::getInputs(node);
      if (building->inputnames(building, const_cast<CString*>(inputs.data()),
                               inputs.size()))
        return interpreter.error() ? interpreter.error()
                                   : (interpreter.error(BadLogic));
    }

    /* @NOTE: prepare operator's attributes, it's acttually a map of string ->
     * base::Auto which supports a c++ high level of type Any */
    if (building->setattr) {
      std::vector<CPair> mapping;
      
      if ((error = internal::getAttrs(node, interpreter, mapping))){
        return interpreter.error(error);
      } else {
        auto ecode = building->setattr(building, mapping.data(), mapping.size(),
                                       internal::capi);

        if (ecode){
          return interpreter.error(pError(error, ""));
        }
      }
    } else if ((opw = nNet_GetOperatorW(building))) {
      try {
        if (opw->updateAttr(*opw, internal::getAttrs(node, interpreter),
                            internal::api))
          return interpreter.error();
      } catch (base::Error& error) {
        return (interpreter.error(error));
      }
    }
  }

  /* @NOTE: step 2: links and commit functions and perform macros */
  for (auto i = 0; i < cast_(i, ops.size()); ++i) {
    auto op = ops[i];

    /* @NOTE: link IO's operator to another operators and commit this op to the
     * interpreter. If this operator is macro operator, it will be perform here
     */

    if (!op->link && !op->type) {
      return (interpreter.error(BadLogic << "node \'" 
                                         << std::string{op->name} << "\'"
                                         << "needs callback \'link\'"));
    } else if (op->link && op->link(op, ops.data(), ops.size()))
      return interpreter.error() ? interpreter.error()
                                 : (interpreter.error(BadLogic));

    /* @NOTE: commit op to the interpreter */
    if (lazyload && op->type) {
      op->eval = op->commit;
  
      if (!interpreter.assignOperator(std::string{op->name}, op)){
        return interpreter.error() ? interpreter.error()
                                   : (interpreter.error(BadLogic));
      }
    } else if (op->commit(op, &interpreter.interface())){
      return interpreter.error() ? interpreter.error()
                                 : (interpreter.error(BadLogic));
    }

    /* @NOTE: check if a macro tries to add itself onto the graph */
    if (!lazyload || !op->type){
      auto not_inserted = interpreter.operators().find(op->name) == 
                                              interpreter.operators().end();
      if (op->type ^ not_inserted){
        clearFaulty(&interpreter.interface(), op, 
                             BadLogic << "macro can\'t apply into interpreter");
        return interpreter.error();
      }
    }
  }

  /* @NOTE: prevent protobuf's memory leak */
  graph.Clear();
  return NoError;
}
} // namespace build

namespace save{
base::Error tfgraph(InterpreterW& UNUSED(interpreter), std::string& UNUSED(model)){
  return NoSupport;
}
} // namespace save

namespace tfgraph{
bool load(){ return true; }

void exit(){ google::protobuf::ShutdownProtobufLibrary(); }
}
} // namespace nn
