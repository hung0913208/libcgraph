#include "tfgraph.h"
#include "interpreter.h"
#include "internal.h"
#include "utils.h"

namespace nn{
namespace tfgraph{
namespace kernels{
using namespace nn::interpreter;
using Int64Vector = std::vector<int64_t>;

bool Add(OperatorW& op, OperatorW::Attrs&& attrs, OperatorW::Model api){
  try{
    if (attrs.find("T") == attrs.end())
      return op.error(NotFound.reason("attribute \'T\'"));

    if (!op.set<NumT>("T", api(attrs["T"], typeid(NumT)).get<NumT>()))
      return op.error(BadAccess.reason("can\'t access \'T\'"));

    op.set<bool>("expand", false);
    if (attrs.find("expand_left_tensor") != attrs.end()){
      op.set<int>("expand.left", 
                  (int)api(attrs["T"], typeid(bool)).get<bool>());
      op.set<bool>("expand", true);
    } else 
      op.set<int>("expand.left_tensor", 0);

    if (attrs.find("expand_right_tensor") != attrs.end()){
      op.set<int>("expand.right",
                  (int)api(attrs["T"], typeid(bool)).get<bool>());
      op.set<bool>("expand", true);
    } else 
      op.set<int>("expand_right_tensor", 0);

  } catch(base::Error& error){ return op.error(error); }
  return false;
}

bool AudioSpectrogram(OperatorW& op, OperatorW::Attrs&& attrs, OperatorW::Model api){
  try{
    int64_t stride, wdsize;
    bool    magnitude;

    if (attrs.find("magnitude_squared") == attrs.end())
      return op.error(NotFound.reason("attribute \'magnitude_squared\'"));
    else if (attrs.find("stride") == attrs.end())
      return op.error(NotFound.reason("attribute \'stride\'"));
    else if (attrs.find("window_size") == attrs.end())
      return op.error(NotFound.reason("attribute \'window_size\'"));

    magnitude = api(attrs["magnitude_squared"], typeid(bool)).get<bool>();
    if (!op.set<bool>("magnitude_squared", magnitude))
      return op.error(BadAccess.reason("can\'t access \'magnitude_squared\'"));

    stride = api(attrs["stride"], typeid(int64_t)).get<int64_t>();
    if (!op.set<int64_t>("stride", stride))
      return op.error(BadAccess.reason("can\'t access \'stride\'"));

    wdsize = api(attrs["window_size"], typeid(int64_t)).get<int64_t>();
    if (!op.set<int64_t>("window_size", wdsize))
      return op.error(BadAccess.reason("can\'t access \'window_size\'")); 
  } catch(base::Error& error){ return op.error(error); }
  return false;
}

bool Const(OperatorW& op, OperatorW::Attrs&& attrs, OperatorW::Model api){
  try{
    if (attrs.find("value") == attrs.end())
      return op.error(NotFound.reason("attribute \'value\'"));

    if (attrs.find("dtype") != attrs.end())
      if (!op.set<NumT>("dtype", api(attrs["dtype"], typeid(NumT)).get<NumT>()))
        return op.error(BadAccess.reason("can\'t access \'dtype\'"));  

    if (!op.set<Tensor>("value", api(attrs["value"], typeid(Tensor)).get<Tensor>()))
      return op.error(BadAccess.reason("can\'t access \'dtype\'"));

    op.outputs().push_back(op.get<Tensor>("value"));
  } catch(base::Error& error){ return op.error(error); }
  return false;
}

bool Conv2D(OperatorW& op, OperatorW::Attrs&& attrs, OperatorW::Model api){
  try{
    char*       padding;
    Int64Vector strides;

    if (attrs.find("T") == attrs.end())
      return op.error(NotFound.reason("attribute \'T\'"));
    else if (attrs.find("data_format") == attrs.end())
      return op.error(NotFound.reason("attribute \'data_format\'"));
    else if (attrs.find("strides") == attrs.end())
      return op.error(NotFound.reason("attribute \'strides\'"));
    else if (attrs.find("padding") == attrs.end())
      return op.error(NotFound.reason("attribute \'padding\'"));

    if (attrs.find("use_cudnn_on_gpu") != attrs.end()){
      auto cudnn = api(attrs["use_cudnn_on_gpu"], typeid(bool)).get<bool>();

      if (!op.set<bool>("use_cudnn_on_gpu", cudnn))
        return op.error(BadAccess.reason("can\'t access \'data_format\'"));
    }

    if (attrs.find("dilations") != attrs.end()){
      auto dilations = api(attrs["dilations"], typeid(std::vector<int64_t>));

      if (!op.set<Int64Vector>("dilations", dilations.get<Int64Vector>()))
        return op.error(BadAccess.reason("can\'t access \'dilations\'"));
    } else if (!op.set<Int64Vector>("dilations",
                                    std::vector<int64_t>{1, 1, 1, 1})){
        return op.error(BadAccess.reason("can\'t access \'dilations\'"));
    }

    if (!op.set<NumT>("T", api(attrs["T"], typeid(NumT)).get<NumT>()))
      return op.error(BadAccess.reason("can\'t access \'T\'"));
    else {
      auto format = api(attrs["data_format"], typeid(char*)).get<char*>();

      if (!op.set<char*>("data_format", format))
        return op.error(BadAccess.reason("can\'t access \'data_format\'"));
    }

    padding = api(attrs["padding"], typeid(char*)).get<char*>();
    if (!op.set<char*>("padding", padding))
      return op.error(BadAccess.reason("can\'t access \'padding\'"));

    strides = api(attrs["strides"], typeid(Int64Vector)).get<Int64Vector>();
    if (!op.set<Int64Vector>("strides", strides))
      return op.error(BadAccess.reason("can\'t access \'strides\'"));
  } catch(base::Error& error){ return op.error(error); }
  return false;
}

bool DecodeWav(OperatorW& op, OperatorW::Attrs&& attrs, OperatorW::Model api){
  try{
    int64_t samples, channels;

    if (attrs.find("desired_samples") == attrs.end())
      return op.error(NotFound.reason("attribute \'desired_samples\'"));
    else if (attrs.find("desired_channels") == attrs.end())
      return op.error(NotFound.reason("attribute \'desired_channels\'"));

    samples = api(attrs["desired_samples"], typeid(int64_t)).get<int64_t>();
    if (!op.set<int64_t>("desired_samples", samples))
      return op.error(BadAccess.reason("can\'t access \'desired_samples\'"));

    channels = api(attrs["desired_channels"], typeid(int64_t)).get<int64_t>();
    if (!op.set<int64_t>("desired_channels", channels))
      return op.error(BadAccess.reason("can\'t access \'desired_channels\'"));

    /* @NOTE: attribute 'method' is new and will use to define specific methods
     * for reading from file to tensor */

    if (attrs.find("method") == attrs.end()){
      /* @NOTE: by default, method 'TopDown' will be used */

      if (!op.set<char*>("method", strdup("TopDown")))
        return op.error(BadAccess.reason("can\'t set attribute method"));
    } else {
      auto method = api(attrs["method"], typeid(char*)).get<char*>();

      if (!op.set<char*>("method", method))
        return op.error(BadAccess.reason("can\'t access \'method\'"));
    }

  } catch(base::Error& error){ return op.error(error); }
  return false;
}

bool Identity(OperatorW& op, OperatorW::Attrs&& attrs, OperatorW::Model api){
  try{
    if (attrs.find("T") == attrs.end())
      return op.error(NotFound.reason("attribute \'T\'"));
    else if (!op.set<NumT>("T", api(attrs["T"], typeid(NumT)).get<NumT>()))
      return op.error(BadAccess.reason("can\'t access \'dtype\'"));
  } catch(base::Error& error){ return op.error(error); }
  return false;
}

bool MatMul(OperatorW& op, OperatorW::Attrs&& attrs, OperatorW::Model api){
  try{
    bool transpose_a, transpose_b;

    if (attrs.find("T") == attrs.end())
      return op.error(NotFound.reason("attribute \'T\'"));
    else if (attrs.find("transpose_a") == attrs.end())
      return op.error(NotFound.reason("attribute \'transpose_a\'"));
    else if (attrs.find("transpose_b") == attrs.end())
      return op.error(NotFound.reason("attribute \'transpose_b\'"));

    if (!op.set<NumT>("T", api(attrs["T"], typeid(NumT)).get<NumT>()))
      return op.error(BadAccess.reason("can\'t access \'T\'"));

    transpose_a = api(attrs["transpose_a"], typeid(bool)).get<bool>();
    if (!op.set<bool>("transpose_a", transpose_a))
      return op.error(BadAccess.reason("can\'t access \'transpose_a\'"));

    transpose_b = api(attrs["transpose_b"], typeid(bool)).get<bool>();
    if (!op.set<bool>("transpose_b", transpose_b))
      return op.error(BadAccess.reason("can\'t access \'transpose_b\'"));

    op.set<bool>("expand", false);
    if (attrs.find("expand_left_tensor") != attrs.end()){
      op.set<int>("expand.left", 
                  (int)api(attrs["T"], typeid(bool)).get<bool>());
      op.set<bool>("expand", true);
    } else 
      op.set<int>("expand_left_tensor", 0);

    if (attrs.find("expand_right_tensor") != attrs.end()){
      op.set<int>("expand.right", 
                  (int)api(attrs["T"], typeid(bool)).get<bool>());
      op.set<bool>("expand", true);
    } else 
      op.set<int>("expand_right_tensor", 0);
  } catch(base::Error& error){ return op.error(error); }
  return false;
}

bool MaxPool(OperatorW& op, OperatorW::Attrs&& attrs, OperatorW::Model api){
  using namespace std;

  try{
    Int64Vector ksize, strides;
    char* format, *padding;

    if (attrs.find("T") == attrs.end())
      return op.error(NotFound.reason("attribute \'T\'"));
    else if (attrs.find("data_format") == attrs.end())
      return op.error(NotFound.reason("attribute \'data_format\'"));
    else if (attrs.find("ksize") == attrs.end())
      return op.error(NotFound.reason("attribute \'ksize\'"));
    else if (attrs.find("strides") == attrs.end())
      return op.error(NotFound.reason("attribute \'strides\'"));
    else if (attrs.find("padding") == attrs.end())
      return op.error(NotFound.reason("attribute \'padding\'"));

    if (!op.set<NumT>("T", api(attrs["T"], typeid(NumT)).get<NumT>()))
      return op.error(BadAccess.reason("can\'t access \'T\'"));

    format = api(attrs["data_format"], typeid(char*)).get<char*>();
    if (!op.set<char*>("data_format", format))
      return op.error(BadAccess.reason("can\'t access \'data_format\'"));

    ksize = api(attrs["ksize"], typeid(Int64Vector)).get<Int64Vector>();
    if (!op.set<Int64Vector>("ksize", ksize))
      return op.error(BadAccess.reason("can\'t access \'ksize\'"));

    padding = api(attrs["padding"], typeid(char*)).get<char*>();
    if (!op.set<char*>("padding", padding))
      return op.error(BadAccess.reason("can\'t access \'padding\'"));

    strides = api(attrs["strides"], typeid(Int64Vector)).get<Int64Vector>();
    if (!op.set<Int64Vector>("strides", strides))
       return op.error(BadAccess.reason("can\'t access \'strides\'"));
  } catch(base::Error& error){ return op.error(error); }
  return false;
}

bool Mfcc(OperatorW& op, OperatorW::Attrs&& attrs, OperatorW::Model api){
   try{
    int64_t dct_count, fltbank_ch_count;
    float   lowfr_limit, upfr_limit;

    if (attrs.find("dct_coefficient_count") == attrs.end())
      return op.error(NotFound.reason("attribute \'dct_coefficient_count\'"));
    else if (attrs.find("filterbank_channel_count") == attrs.end())
      return op.error(NotFound.reason("attribute \'filterbank_channel_count\'"));
    else if (attrs.find("lower_frequency_limit") == attrs.end())
      return op.error(NotFound.reason("attribute \'lower_frequency_limit\'"));
    else if (attrs.find("upper_frequency_limit") == attrs.end())
      return op.error(NotFound.reason("attribute \'upper_frequency_limit\'"));

    dct_count = api(attrs["dct_coefficient_count"], typeid(int64_t)).get<int64_t>();
    if (!op.set<int64_t>("dct_coefficient_count", dct_count))
      return op.error(BadAccess.reason("can\'t access \'dct_coefficient_count\'"));

    fltbank_ch_count = api(attrs["filterbank_channel_count"], typeid(int64_t)).get<int64_t>();
    if (!op.set<int64_t>("filterbank_channel_count", fltbank_ch_count))
      return op.error(BadAccess.reason("can\'t access \'filterbank_channel_count\'"));

    lowfr_limit = api(attrs["lower_frequency_limit"], typeid(float)).get<float>();
    if (!op.set<float>("lower_frequency_limit", lowfr_limit))
      return op.error(BadAccess.reason("can\'t access \'lower_frequency_limit\'"));

    upfr_limit = api(attrs["upper_frequency_limit"], typeid(float)).get<float>();
    if (!op.set<float>("upper_frequency_limit", upfr_limit))
      return op.error(BadAccess.reason("can\'t access \'upper_frequency_limit\'"));
  } catch(base::Error& error){ return op.error(error); }
  return false;
}

bool Placeholder(OperatorW& op, OperatorW::Attrs&& attrs, OperatorW::Model api){
  try{
    if (attrs.find("dtype") == attrs.end())
      return op.error(NotFound.reason("attribute \'dtype\'"));
    else if (attrs.find("shape") == attrs.end())
      return op.error(NotFound.reason("attribute \'shape\'"));

    if (!op.set<NumT>("dtype", api(attrs["dtype"], typeid(NumT)).get<NumT>()))
      return op.error(BadAccess.reason("can\'t access \'dtype\'"));
    if (!op.set<Shape>("shape", api(attrs["shape"], typeid(Shape)).get<Shape>()))
      return op.error(BadAccess.reason("can'\t access \'shape\'"));

    op.outputs().push_back(Tensor{nullptr});
  } catch(base::Error& error){ return op.error(error); }
  return false;
}

bool Relu(OperatorW& op, OperatorW::Attrs&& attrs, OperatorW::Model api){
  try{
    if (attrs.find("T") == attrs.end())
      return op.error(NotFound.reason("attribute \'T\'"));

    if (!op.set<NumT>("T", api(attrs["T"], typeid(NumT)).get<NumT>()))
      return op.error(BadAccess.reason("can\'t access \'T\'")); 
  } catch(base::Error& error){ return op.error(error); }
  return false;
}

bool Reshape(OperatorW& op, OperatorW::Attrs&& attrs, OperatorW::Model api){
  try{
    if (attrs.find("T") == attrs.end())
      return op.error(NotFound.reason("attribute \'T\'"));
    else if (attrs.find("Tshape") == attrs.end())
      return op.error(NotFound.reason("attribute \'Tshape\'"));

    if (!op.set<NumT>("T", api(attrs["T"], typeid(NumT)).get<NumT>()))
      return op.error(BadAccess.reason("can\'t access \'T\'"));
    else if (!op.set<NumT>("Tshape", api(attrs["Tshape"], typeid(NumT)).get<NumT>()))
      return op.error(BadAccess.reason("can\'t access \'Tshape\'"));
  } catch(base::Error& error){ return op.error(error); }
  return false;
}

bool Softmax(OperatorW& op, OperatorW::Attrs&& attrs, OperatorW::Model api){
  try{
    if (attrs.find("T") == attrs.end())
      return op.error(NotFound.reason("attribute \'T\'"));

    if (!op.set<NumT>("T", api(attrs["T"], typeid(NumT)).get<NumT>()))
      return op.error(BadAccess.reason("can\'t access \'T\'")); 
  } catch(base::Error& error){ return op.error(error); }
  return false;
}

interpreter::OperatorW::Config attribute(std::string&& op){
  if (op == "Add") return kernels::Add;
  else if (op == "AudioSpectrogram") return kernels::AudioSpectrogram;
  else if (op == "Const") return kernels::Const;
  else if (op == "Conv2D") return kernels::Conv2D;
  else if (op == "DecodeWav") return kernels::DecodeWav;
  else if (op == "Identity") return kernels::Identity;
  else if (op == "MatMul") return kernels::MatMul;
  else if (op == "MaxPool") return kernels::MaxPool;
  else if (op == "Mfcc") return kernels::Mfcc;
  else if (op == "Placeholder") return kernels::Placeholder;
  else if (op == "Relu") return kernels::Relu;
  else if (op == "Reshape") return kernels::Reshape;
  else if (op == "Softmax") return kernels::Softmax;
  else return nullptr;
}
} // namespace kernels
} // namespace tfgraph
} // namespace nn