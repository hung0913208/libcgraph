#if !defined(LIBNN_DEVICE_COMMON_HPP_) && __cplusplus
#define LIBNN_DEVICE_COMMON_HPP_

#ifdef LIBCGRAPH_GIT_REFSPEC
#include "libnn/interpreter.h"
#include "libnn/internal.h"
#include "libnn/tensor.h"
#include "libnn/utils.h"
#else
#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"
#endif

#define TENSOR_T 2

namespace nn{
namespace tensor{
using CPContent = Array;

class GPContent{
 public:
  explicit GPContent(Interpreter interpreter, Tensor tensor, bool manual):
      _interpreter{interpreter}{
    auto interpreterw = nNet_GetInterpreterW(interpreter);
    auto device       = (Device)tensor->_device;

    if (interpreterw && device->level != GENERIC_PLATFORM){
      if (!manual && !interpreterw->assignPointer(cache(), TENSOR_T, true))
        throw BadLogic;
      init();
    } else throw NoSupport;
  }

  virtual ~GPContent(){ }

 public:
  inline Interpreter interpreter(){ return _interpreter; }
  inline Tensor      cache(){ return &_cache; }

  virtual void init(){};
  virtual void clear(){};

 private:
  Interpreter _interpreter;
  TensorDef   _cache;
}; 
} // namespace tensor
} // namesapce nn
#endif  // LIBNN_DEVICE_COMMON_HPP_
