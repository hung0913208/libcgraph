#ifdef LIBCGRAPH_GIT_REFSPEC
#include <libbase/logcat.hpp>
#else
#include "logcat.hpp"
#endif
#include <memory>

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"
#include "generic.h"

DEFINE_OP(generic, Mfcc);
DEFINE_OP(generic, DecodeWav);
DEFINE_OP(generic, AudioSpectrogram);

DEFINE_OP(generic, Add);
DEFINE_OP(generic, MatMul);
DEFINE_OP(generic, Reshape);

DEFINE_OP(generic, Conv2D);
DEFINE_OP(generic, Relu);
DEFINE_OP(generic, MaxPool);

DEFINE_OP(generic, Softmax);

namespace nn{
namespace device{
namespace generic{
Device create(Device pattern, Interpreter UNUSED(interpreter)){
  return pattern;
}

Device release(Device device){
  if (nNet_CheckStatus() == nn::Releasing && nNet_IsTemplateDevice(device)){
    auto manager = dynamic_cast<CPUManager*>(nNet_GetDeviceW(device));

    if (manager){
      delete manager;
      return nullptr;
    }
  }
  return nNet_RemoveDevice(device);
}

bool inspect(Device){ return true; }

bool install(Device device, const char* name, Operator op){
  auto  delegate  = reinterpret_cast<CPUManager*>(device->_template._priv);
  auto& operators = delegate->operators();

  if (operators.find(std::string{name}) != operators.end())
    return BadLogic.reason("duplicate installing op " + std::string{name});

  operators[std::string(name)] = op;
  return true;
}

bool scan(Device device, Operator sample){
  auto  delegate  = reinterpret_cast<CPUManager*>(device->_template._priv);

  for (auto& item: delegate->operators()){
    if (std::get<1>(item) == sample)
      return true;
  }
  return false;
}

Operator find(Device device, const char* name){
  auto  delegate  = reinterpret_cast<CPUManager*>(device->_template._priv);
  auto& operators = delegate->operators();

  if (!name) return nullptr;
  else if (operators.find(std::string(name)) != operators.end())
    return operators[std::string(name)];
  return nullptr;
}

CPUManager::CPUManager(): DeviceW("generic"){
  /* @NOTE: install global configure */
  interface()->level = GENERIC_PLATFORM;

  interface()->_template._priv   = this;
  interface()->_template.find    = generic::find;
  interface()->_template.scan    = generic::scan;
  interface()->_template.inspect = generic::inspect;
  interface()->_template.create  = generic::create;
  interface()->_template.release = generic::release;
  interface()->_template.install = generic::install;

 #if defined(_OPENMP)
  // omp_set_dynamic(0);     
  omp_set_num_threads(16); 
 #endif
  /* @NOTE: install generic's operators */
  FUNCTION(generic, interface(), DecodeWav);
  FUNCTION(generic, interface(), AudioSpectrogram);
  FUNCTION(generic, interface(), Mfcc);
  FUNCTION(generic, interface(), Add);
  FUNCTION(generic, interface(), MatMul);
  FUNCTION(generic, interface(), Reshape);
  FUNCTION(generic, interface(), Conv2D);
  FUNCTION(generic, interface(), Relu);
  FUNCTION(generic, interface(), MaxPool);
  FUNCTION(generic, interface(), Softmax);
}

} // namespace generic

base::Error useCPU(Device* result){
  try{ 
    if (!result) return DoNothing;
    else
      *result = (new generic::CPUManager())->interface();
  } catch(base::Error& error){ return error; }
  return NoError;
}
} // namespace device
} // namespace nn
