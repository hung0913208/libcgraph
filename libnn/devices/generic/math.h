#ifndef LIBNN_GENERIC_MATH_H_
#define LIBNN_GENERIC_MATH_H_

#if __cplusplus
#ifdef LIBCGRAPH_GIT_REFSPEC
#include "libnn/devices/generic/math/fft.h"
#include "libnn/devices/generic/math/dot.h"
#include "libnn/devices/generic/math/im2col.h"
#include "libnn/devices/generic/math/col2im.h"
#include "libnn/devices/generic/math/padding.h"
#else
#include "fft.h"
#include "dot.h"
#include "im2col.h"
#include "col2im.h"
#include "padding.h"
#endif
#endif
#endif  // LIBNN_GENERIC_MATH_H_
