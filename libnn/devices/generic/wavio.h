#if !defined(LIBNN_WAVEIO_H_) && __cplusplus
#define LIBNN_WAVEIO_H_

#ifdef LIBCGRAPH_GIT_REFSPEC
#include <libbase/stream.hpp>
#else
#include "stream.hpp"
#endif

#include <cmath>

extern "C" struct WavHeader {
  uint8_t  chunkId[4];      // RIFF string
  uint32_t chunkSize;       // overall size of file in bytes
  uint8_t  format[4];       // WavFE string
  uint8_t  schunk1Id[4];    // fmt string with trailing null char
  uint32_t schunk1Size;     // length of the format data
  uint16_t audioFormat;     // format type. 1-PCM, 3- IEEE float, 6 - 8bit A law, 7 - 8bit mu law
  uint16_t numChannels;     // no.of channels
  uint32_t sampleRate;      // sampling rate (blocks per second)
  uint32_t byteRate;        // SampleRate * NumChannels * BitsPerSample/8
  uint16_t blockAlign;      // NumChannels * BitsPerSample/8
  uint16_t bitsPerSample;   // bits per sample, 8- 8bits, 16- 16 bits etc
  uint8_t  schunk2ID[4];    // DATA string or FLLR string
  uint32_t schunk2Size;     // NumSamples * NumChannels * BitsPerSample/8 - size of the next chunk that will be read
};

namespace nn{
namespace generic{
namespace audio{
template <typename OutputT = float>
class WavIO: protected base::Stream {
 public:
  explicit WavIO(): base::Stream{
    [&](uint8_t* buffer, std::size_t size) -> base::Error{ /* reading callback */
       if (_fstream.is_open()){
        if (!_loaded){
          _fstream.read(reinterpret_cast<char*>(buffer), size);
        } else if (size != (_header.bitsPerSample/8) && 
                   (size % (_header.bitsPerSample/8)))
          return NoSupport;
        else {
          _fstream.read(reinterpret_cast<char*>(buffer), size);

          for (auto i = size/(_header.bitsPerSample/8); i > 0; --i){
            OutputT reverse{0};

            if (typeid(OutputT) == typeid(float)){
              auto max = std::pow(2.0, _header.bitsPerSample - 1);

             #if BYTE_ORDER==BIG_ENDIAN
              reverse = get(buffer + _header.bitsPerSample*(i - 1)/8);
              reverse = base::reverse(reverse)/max;
             #else
              reverse = get(buffer + _header.bitsPerSample*(i - 1)/8);
              reverse = reverse / max;
             #endif
            } else{
             #if BYTE_ORDER==BIG_ENDIAN
              reverse = base::reverse(get(buffer + _header.bitsPerSample*i/8));
             #else
              reverse = get(buffer + _header.bitsPerSample*(i - 1)/8);
             #endif
            }
            *(reinterpret_cast<OutputT*>(buffer + sizeof(OutputT)*(i -1))) = reverse;
          }
        }
        return NoError;
      } else return NotFound;
    },
    [&](uint8_t* buffer, std::size_t size) -> base::Error{ /* writing callback */
     if (_fstream.is_open()){
        auto cur = _fstream.tellp();

        /* @TODO: tu dong tai cap nhat header tai day truoc khi ghi nguoc lai 
         * file */
        _fstream.seekp(0, std::ios_base::beg);
        _fstream.write(reinterpret_cast<char*>(buffer), size);

        if (!_loaded) _loaded = true;
        else
          _fstream.seekp(cur, std::ios_base::beg);

        if (size != sizeof(OutputT))
          return NoSupport;
        else {
          auto max = std::pow(2.0, sizeof(OutputT));
  
          for (auto i = 0; i < cast_(i, size); i += sizeof(OutputT)){
           #if BYTE_ORDER==BIG_ENDIAN
            if (typeid(OutputT) == typeid(float)){
              auto reverse = base::reverse(*(reinterpret_cast<OutputT*>(buffer + i)));

              set(buffer + i, int32_t(reverse/max));
            } else {
              set(buffer + i, base::reverse(*(reinterpret_cast<OutputT*>(buffer + i))));
            }
           #else
            if (typeid(OutputT) == typeid(float)){
              set(buffer + i, int32_t(*(reinterpret_cast<OutputT*>(buffer + i))/max));
            } else {
              set(buffer + i, *(reinterpret_cast<OutputT*>(buffer + i))/max);
            }
           #endif
          }

          _fstream.write((char*)buffer, size);
        }
        return NoError;
      } else return DoNothing;
    }},
    _offset{std::string::npos}, _boffset{std::string::npos},
    _pbody{std::string::npos}, _fstream{}, _header{}, _loaded{false}
  {
    if (sizeof(OutputT) > 4 && typeid(OutputT) != typeid(double))
      throw NoSupport;
    memcpy(_header.chunkId, "RIFF", 4);
    memcpy(_header.format, "WAVE", 4);
    memcpy(_header.schunk1Id, "fmt ", 4);
  }

  explicit WavIO(std::string&& path,
                std::ios_base::openmode mode = std::ios::in | std::ios::out):
    base::Stream{
    [&](uint8_t* buffer, std::size_t size) -> base::Error{ /* reading callback */
      if (_fstream.is_open()){
        if (!_loaded){
          _fstream.read(reinterpret_cast<char*>(buffer), size);
        } else if (size != (_header.bitsPerSample/8) && 
                   (size % (_header.bitsPerSample/8)))
          return NoSupport;
        else {
          _fstream.read(reinterpret_cast<char*>(buffer), size);

          for (auto i = size/(_header.bitsPerSample/8); i > 0; --i){
            OutputT reverse{0};

            if (typeid(OutputT) == typeid(float)){
              auto max = std::pow(2.0, _header.bitsPerSample);

             #if BYTE_ORDER==BIG_ENDIAN
              reverse = get(buffer + _header.bitsPerSample*(i - 1)/8);
              reverse = base::reverse(reverse)/max;
             #else
              reverse = get(buffer + _header.bitsPerSample*(i - 1)/8) / max;
             #endif
            } else{
             #if BYTE_ORDER==BIG_ENDIAN
              reverse = base::reverse(get(buffer + _header.bitsPerSample*i/8));
             #else
              reverse = get(buffer + _header.bitsPerSample*(i - 1)/8);
             #endif
            }
            *(reinterpret_cast<OutputT*>(buffer + sizeof(OutputT)*(i -1))) = reverse;
          }
        }
        return NoError;
      } else return NotFound;
    },
    [&](uint8_t* buffer, std::size_t size) -> base::Error{ /* writing callback */
      if (_fstream.is_open()){
        auto cur = _fstream.tellp();

        /* @TODO: tu dong tai cap nhat header tai day truoc khi ghi nguoc lai 
         * file */
        _fstream.seekp(0, std::ios_base::beg);
        _fstream.write(reinterpret_cast<char*>(buffer), size);

        if (!_loaded) _loaded = true;
        else
          _fstream.seekp(cur, std::ios_base::beg);

        if (size != sizeof(OutputT))
          return NoSupport;
        else {
          auto max = std::pow(2.0, sizeof(OutputT));
  
          for (auto i = 0; i < cast_(i, size); i += sizeof(OutputT)){
           #if BYTE_ORDER==BIG_ENDIAN
            if (typeid(OutputT) == typeid(float)){
              auto reverse = base::reverse(*(reinterpret_cast<OutputT*>(buffer + i)));

              set(buffer + i, int32_t(reverse/max));
            } else {
              set(buffer + i, base::reverse(*(reinterpret_cast<OutputT*>(buffer + i))));
            }
           #else
            if (typeid(OutputT) == typeid(float)){
              set(buffer + i, int32_t(*(reinterpret_cast<OutputT*>(buffer + i))/max));
            } else {
              set(buffer + i, *(reinterpret_cast<OutputT*>(buffer + i))/max);
            }
           #endif
          }

          _fstream.write((char*)buffer, size);
        }
        return NoError;
      } else return DoNothing;
    }},
    _offset{std::string::npos}, _boffset{std::string::npos},
    _pbody{std::string::npos}, _fstream{path, mode}, _header{}, _loaded{false}
  {
    if (sizeof(OutputT) > 4 && typeid(OutputT) != typeid(double))
      throw NoSupport;
    else{
      auto error = config();

      if (error) throw error;
    }
  }

 public:
  base::Error open(std::string&& path, std::ios_base::openmode mode = std::ios_base::in | std::ios_base::out){
    _fstream.open(path, mode);
    return _fstream.is_open() ? config() : DoNothing;
  }

  void close(){ _fstream.close(); }

 public:
  base::Error seek(long offset, int whence){
    if (!_fstream.is_open()) return DoNothing;
    else {
      auto szfile = size();

      switch (whence){
      case SEEK_SET:
        BAssert(offset * cast_(offset, sizeof(OutputT)));

        _boffset = _pbody + offset * cast_(offset, sizeof(OutputT));
        _offset  = offset;
        break;

      case SEEK_CUR:
        BAssert(offset * cast_(offset, sizeof(OutputT)) < szfile - _boffset);

        _boffset += offset * cast_(offset, sizeof(OutputT));
        _offset  += offset;
        break;

      case SEEK_END:
        BAssert(offset < 0);

        _boffset += size() + offset * cast_(offset, sizeof(OutputT));
        _offset  += offset;
        break;
      }

      _fstream.seekg(_boffset, std::ios::beg);
      return NoError;
    }
  }

  std::size_t tell(){ return _offset; }

  WavHeader& header(){ return _header; }

 public:
  WavIO<OutputT>& operator>>(OutputT& value){
    auto error = read(reinterpret_cast<uint8_t*>(&value), _header.bitsPerSample/8);

    if (error) throw error;
    return *this;
  }

  WavIO<OutputT>& operator<<(OutputT&& value){
    auto error = NoError;
    /* @TODO: xa6y dung tham so mac dinh cho file wav */

    if (!_loaded){
      memcpy(_header.chunkId, "RIFF", 4);
      memcpy(_header.format, "WAVE", 4);
      memcpy(_header.schunk1Id, "fmt ", 4);

      _header.schunk1Size   = 16;
      _header.bitsPerSample = 8*sizeof(OutputT);
      _header.byteRate      = _header.sampleRate * _header.bitsPerSample *
                              _header.numChannels;
      _header.blockAlign    = 2;
    // _header.schunk2ID     = {'d', 'a', 't', 'a'};
    }

    error =  write(reinterpret_cast<uint8_t*>(&value), sizeof(value));
    if (error) throw error;
    return *this;
  }

  std::size_t size(){
    if (_fstream.is_open()){
      std::size_t save = _fstream.tellg(), result = 0;

      _fstream.seekg(0, std::ios::end);
      result = _fstream.tellg();
      _fstream.seekg(save, std::ios::beg);

      return result;
    } else return 0;
  }

  std::size_t countSamples(){
    return (size() - sizeof(_header))/
           (_header.numChannels*_header.bitsPerSample/8);
  }

  const std::type_info& outputType(){ return typeid(OutputT); }

 private:
  base::Error config(){
    if (!_loaded){
      auto error = read(reinterpret_cast<uint8_t*>(&_header), sizeof(_header));

      if (error) return error;
      else {
       #if BYTE_ORDER == BIG_ENDIAN
        _header.chunkSize     = base::reverse(_header.chunkSize);
        _header.schunk1Size   = base::reverse(_header.schunk1Size);
        _header.audioFormat   = base::reverse(_header.audioFormat);
        _header.numChannels   = base::reverse(_header.numChannels);
        _header.sampleRate    = base::reverse(_header.sampleRate);
        _header.byteRate      = base::reverse(_header.byteRate);
        _header.blockAlign    = base::reverse(_header.blockAlign);
        _header.bitsPerSample = base::reverse(_header.bitsPerSample);
        _header.schunk2Size   = base::reverse(_header.schunk2Size);
       #endif

        _pbody   = (_boffset = _fstream.tellg());
        _loaded  = !memcmp(_header.chunkId, "RIFF", 4);
        _offset  = 0;

        if (_header.bitsPerSample/8 > sizeof(OutputT))
          return NoSupport;
      }
    }
    return NoError;
  }

  template<typename InputT>
  base::Error set(uint8_t* buffer, InputT value){
    if (sizeof(InputT) != sizeof(OutputT))
      memcpy(buffer , &value, sizeof(OutputT));
    return NoSupport;
  }

  inline OutputT get(uint8_t* buffer){
    OutputT result;

    switch (_header.bitsPerSample/8){
    case 1:
      result = OutputT(buffer[0]);
      break;

    case 2:
      result = OutputT(*reinterpret_cast<int16_t*>(buffer));
      break;

    case 4:
      result = OutputT(*reinterpret_cast<int32_t*>(buffer));
      break;

    default:
      throw NoSupport;
    }
    return result;
  }

 private:
  std::size_t  _offset, _boffset, _pbody;
  std::fstream _fstream;

 private:
  WavHeader _header;
  bool      _loaded;
};
} // namespace audio
} // namespace generic
} // namesapce nn
#endif  // LIBNN_WAVEIO_H_
