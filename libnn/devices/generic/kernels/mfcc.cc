#ifdef LIBCGRAPH_GIT_REFSPEC
#include "devices/generic/generic.h"
#include "devices/generic/math.h"
#else
#include "generic.h"
#include "math.h"
#endif

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
namespace kernels{
namespace generic{
namespace mfcc{
using namespace nn::interpreter;

#define kFilterbankFloor 1e-12
#define kLogFilterbankFloor -12

const double kDefaultUpperFrequencyLimit = 4000;
const double kDefaultLowerFrequencyLimit = 20;
const int kDefaultFilterbankChannelCount = 40;
const int kDefaultDCTCoefficientCount = 13;

namespace internal{
struct Coefficient{
 public:
  Coefficient():
    coefficient_count{kDefaultDCTCoefficientCount},
    initialized{false}
  {}

 public:
  base::Error initialize(int input_length){
    if (initialized) return NoError;

   #ifdef M_PI
    const double pi = M_PI;
   #else
    // Some platforms don't have M_PI, so define a local constant here.
    const double pi = std::atan(1) * 4;
   #endif

    auto arg   = pi / input_length;
    auto fnorm = sqrt(2.0 / input_length);

    if (coefficient_count < 1)
      return NoSupport;
    else if (input_length < 1)
      return NoSupport;
    else if (coefficient_count > input_length)
      return NoSupport;

    _cosines.resize(coefficient_count);

    // Some platforms don't have M_PI, so define a local constant here.
    for (int i = 0; i < coefficient_count; ++i) {
      _cosines[i].resize(input_length);

      for (int j = 0; j < input_length; ++j) {
        _cosines[i][j] = fnorm * cos(i * arg * (j + 0.5));
      }
    }

    initialized = true;
    return NoError;
  }

  template<typename Output>
  CError compute(std::vector<double>&& input, Output* output){
    int length = input.size();

    for (int i = 0; i < coefficient_count; ++i) {
      output[i] = 0.0;

      for (int j = 0; j < length; ++j) {
        output[i] += _cosines[i][j]*input[j];
      }
    }

    return base::error::ENoError;
  }

  int expect() { return coefficient_count; }

 public:
  int  coefficient_count;
  bool initialized;

 private:
  std::vector<std::vector<double>> _cosines;
};

struct Filterbank{
 public:
  Filterbank():
    num_channels{kDefaultFilterbankChannelCount},
    lower_frequency_limit{kDefaultLowerFrequencyLimit},
    upper_frequency_limit{kDefaultUpperFrequencyLimit},
    initialized{false}
  {}

 public:
  base::Error initialize(int input_length, int sample_rate){
    const double mel_low = freq2mel(lower_frequency_limit);
    const double mel_hi = freq2mel(upper_frequency_limit);
    const double mel_span = mel_hi - mel_low;
    const double mel_spacing = mel_span / static_cast<double>(num_channels + 1);
    const double hz_per_sbin =
                      0.5 * sample_rate / static_cast<double>(input_length - 1);
 
    auto channel = 0;
    auto bad_channels = std::vector<int>{};

    if (initialized) return NoError;
    if (num_channels < 1){
      return BadLogic << "Number of filterbank channels must be positive";
    }

    if (sample_rate <= 0){
      return BadLogic << "Sample rate must be positive";
    }

    if (input_length < 2){
      return BadLogic << "Input length must greater than 1";
    }

    if (lower_frequency_limit < 0){
      return BadLogic << "Lower frequence limit must be nonnegative";
    }

    if (upper_frequency_limit <= lower_frequency_limit){
      return BadLogic << "Upper frequency limit must be gretaer than"
                      << "lower frequency limit"; 
    }


    // An extra center frequency is computed at the top to get the upper
    // limit on the high side of the final triangular filter.
    _center_frequencies.resize(num_channels + 1);
    for (int i = 0; i < num_channels + 1; ++i) {
      _center_frequencies[i] = mel_low + (mel_spacing * (i + 1));
    }

    // Always exclude DC; emulate HTK.
    _start_index = static_cast<int>(1.5 + (lower_frequency_limit / hz_per_sbin));
    _end_index = static_cast<int>(upper_frequency_limit / hz_per_sbin);

    // Maps the input spectrum bin indices to filter bank channels/indices. For
    // each FFT bin, band_mapper tells us which channel this bin contributes to
    // on the right side of the triangle.  Thus this bin also contributes to the
    // left side of the next channel's triangle response.
    _band_mapper.resize(input_length);
    for (int i = 0; i < input_length; ++i) {
      double melf = freq2mel(i * hz_per_sbin);

      if ((i < _start_index) || (i > _end_index))
        _band_mapper[i] = -2;  // Indicate an unused Fourier coefficient.
      else {
        while ((_center_frequencies[channel] < melf) &&
               (channel < num_channels)) {
          ++channel;
        }
        _band_mapper[i] = channel - 1;  // Can be == -1
      }
    }

    // Create the weighting functions to taper the band edges.  The contribution
    // of any one FFT bin is based on its distance along the continuum between two
    // mel-channel center frequencies.  This bin contributes weights_[i] to the
    // current channel and 1-weights_[i] to the next channel.
    _weights.resize(input_length);

    for (int i = 0; i < input_length; ++i) {
      channel = _band_mapper[i];
      if ((i < _start_index) || (i > _end_index)) {
        _weights[i] = 0.0;
      } else {
        if (channel >= 0) {
          _weights[i] =
              (_center_frequencies[channel + 1] - freq2mel(i * hz_per_sbin)) /
              (_center_frequencies[channel + 1] - _center_frequencies[channel]);
        } else {
          _weights[i] = (_center_frequencies[0] - freq2mel(i * hz_per_sbin)) /
                        (_center_frequencies[0] - mel_low);
        }
      }
    }

    // Check the sum of FFT bin weights for every mel band to identify
    // situations where the mel bands are so narrow that they don't get
    // significant weight on enough (or any) FFT bins -- i.e., too many
    // mel bands have been requested for the given FFT size.
    for (int c = 0; c < num_channels; ++c) {
      float band_weights_sum = 0.0;

      for (int i = 0; i < input_length; ++i) {
        if (_band_mapper[i] == c - 1) {
          band_weights_sum += (1.0 - _weights[i]);
        } else if (_band_mapper[i] == c) {
          band_weights_sum += _weights[i];
        }
      }
      // The lowest mel channels have the fewest FFT bins and the lowest
      // weights sum.  But given that the target gain at the center frequency
      // is 1.0, if the total sum of weights is 0.5, we're in bad shape.
      if (band_weights_sum < 0.5) {
        bad_channels.push_back(c);
      }
    }
    if (!bad_channels.empty()) {
      return BadLogic << "Missing" << std::to_string(bad_channels.size()) << "bands"
                      << "starting at" << std::to_string(bad_channels[0])
                      << "in mel-frequency design."
                      << "Perhaps too many channels or"
                      << "not enough frequency resolution in spectrum. ( "
                      << "input_length:" << std::to_string(input_length)
                      << "input_sample_rate: " << std::to_string(sample_rate)
                      << "output_channel_count:" << std::to_string(num_channels)
                      << "lower_frequency_limit:" << std::to_string(lower_frequency_limit)
                      << "upper_frequency_limit:" << std::to_string(upper_frequency_limit)
                      << " )";
    }

    initialized = true;
    return NoError;
  }

  template<typename Input>
  void compute(Input* input, std::vector<double>& output){
    // Ensure output is right length and reset all values.
    output.assign(num_channels, 0.0);

    for (int i = _start_index; i <= _end_index; i++) {  // For each FFT bin
      auto spec_val = sqrt(input[i]);
      auto weighted = spec_val*_weights[i];
      auto channel  = _band_mapper[i];

      if (channel >= 0)
        output[channel] += weighted;  // Right side of triangle, downward slope

      channel++;
      if (channel < num_channels)
        output[channel] += spec_val - weighted;  // Left side of triangle
    }
  }

  int expect(){ return _end_index - _start_index; }

 public:
  int    num_channels;
  double lower_frequency_limit;
  double upper_frequency_limit;
  bool   initialized;

 private:
  static double freq2mel(double freq){ return 1127.0 * log1p(freq / 700.0); }

 private:
  std::vector<double> _center_frequencies, _weights;
  std::vector<int>    _band_mapper;

  int _start_index = 0;
  int _end_index = 0;
};

struct Context{
 public:
  Context(){
    filterbank_channel_count.getter([&]() -> int&{
      return _filterbank.num_channels; 
    });
    filterbank_channel_count.setter([&](int value){
      _filterbank.num_channels = value; 
    });

    upper_frequency_limit.getter([&]() -> double&{
      return _filterbank.upper_frequency_limit;
    });
    upper_frequency_limit.setter([&](double value){
      _filterbank.upper_frequency_limit = value;
    });

    lower_frequency_limit.getter([&]() -> double&{
      return _filterbank.lower_frequency_limit;
    });
    lower_frequency_limit.setter([&](double value){
      _filterbank.lower_frequency_limit = value;
    });

    dct_coefficient_count.getter([&]() -> int&{
      return _coefficient.coefficient_count;
    });
    dct_coefficient_count.setter([&](int value){
      _coefficient.coefficient_count = value;
    });
  }

 public:
  base::Property<int>    dct_coefficient_count;
  base::Property<int>    filterbank_channel_count;
  base::Property<double> upper_frequency_limit;
  base::Property<double> lower_frequency_limit;

 public:
  base::Error initialize(int input_length, int sample_rate){
    auto error = NoError;

    if ((error = _filterbank.initialize(input_length, sample_rate)))
      return error;
    if ((error = _coefficient.initialize(filterbank_channel_count())))
      return error;
    return NoError;
  }

  template<typename Input, typename Output>
  CError compute(Input* input, int isize,
                 Output* output, int osize){
    std::vector<double> working;

    if (_filterbank.expect() > isize)
      return NoSupport.reason("filterbank > input_size");

    if (_coefficient.expect() > osize)
      return NoSupport.reason("coefficient > output_size");

    _filterbank.compute<Input>(input, working);

    for (auto i = 0; i < cast_(i, working.size()); ++i){
      working[i] = working[i] < kFilterbankFloor? kLogFilterbankFloor
                                                : log(working[i]);
    }

    return _coefficient.compute<Output>(rvalue(working), output);
  }

 private:
  Filterbank  _filterbank;
  Coefficient _coefficient;
};
} // namespace internal

Operator Prepare(Operator op, Interpreter interpreter, CString model){
  try{
    auto attr = OperatorW::config(model, "Mfcc");

    if (!op->device)
      nNet_GetInterpreterW(interpreter)->error(
                    BadLogic.reason("op \'Mfcc' has been pluged like a macro"));
    else if (attr){
      auto mfcc = nNet_CreateTensorC(interpreter, nullptr);
      auto opw  = new interpreter::OperatorW(op, COUNT_MFCC_ARGUMENTS, 
                                             interpreter, op->device, attr);
      
      if (!mfcc)
        return nNet_RemoveOperatorW(interpreter, opw->interface());

      opw->outputs().push_back(mfcc);
      return opw->interface();
    } else
      nNet_GetInterpreterW(interpreter)->error(NotFound);
  } catch(base::Error& error){
    nNet_GetInterpreterW(interpreter)->error(rvalue(error));
  }
  return nullptr;
}

bool Check(Operator op, Interpreter interpreter){
  using namespace interpreter;
  using namespace device::generic;

  auto opw    = nNet_GetOperatorW(op);
  auto input  = std::get<1>(opw->inputs()[0]);
  auto rate   = std::get<1>(opw->inputs()[1]);
  auto output = opw->outputs()[0];
  auto error  = NoError;

  if (input->ndims != 3){
    return opw << (BadLogic << "input's operator" << op->name 
                            << "must be a 3D-Tensor");
  }

  if (nNet_GetTensorType(rate) != ScalarT)
    return opw << (BadLogic << "rate's operator" << op->name 
                            << "must be a scalar");
  try{
    auto spectrogram_channels  = input->dims[0];
    auto sample_rate           = tensor::scalar<Int32>(rate);
    auto spectrogram_samples   = input->dims[1];
    auto audio_channels        = input->dims[2];
    auto dct_coefficient_count = int(opw->get<int64_t>("dct_coefficient_count"));
    
    internal::Context* mfcc;

    if (!opw->exist("context")){
      auto context = base::Auto{}.set(mfcc = new internal::Context());

      context.delete_() = [](void* data){
        delete (((internal::Context**)data)[0]);
        delete (internal::Context**)data;
      };

      opw->set<base::Auto>("context",context);
    } else
      mfcc = opw->get<internal::Context*>("context");

    mfcc->upper_frequency_limit = opw->get<float>("upper_frequency_limit");
    mfcc->lower_frequency_limit = opw->get<float>("lower_frequency_limit");
    mfcc->dct_coefficient_count = opw->get<int64_t>("dct_coefficient_count");
    mfcc->filterbank_channel_count = opw->get<int64_t>("filterbank_channel_count");

    if (nNet_GetTensorType(output) != NilT) {
      if ((output->ndims == 3) && (output->type == nNet_GetTensorNumT(input)) &&
          (output->dims[0] == dct_coefficient_count) &&
          (output->dims[1] == spectrogram_samples) &&
          (output->dims[2] == audio_channels)) {
        if (!nNet_ResetTensor(output))
          return BadAccess.reason("nNet_ResetTensor()");
        return false;
      }
    }

    if ((error = mfcc->initialize(spectrogram_channels, sample_rate)))
      return opw << error;
    else {
      int   dims[3];
      Shape shape;

      dims[0] = dct_coefficient_count;
      dims[1] = spectrogram_samples;
      dims[2] = audio_channels;

      shape = nNet_CreateTensorShapeC2(interpreter, 3, dims);

      if (!shape)
        return opw << (DrainMem << "nNet_CreateTensorShapeC1()");

      if (!nNet_SetTensorNumTW(interpreter, output, nNet_GetTensorNumT(input)))
        return opw << (BadAccess << "nNet_SetTensorNumTW()");
      else if (!nNet_SetTensorShapeW(interpreter, output, shape))
        return opw << (BadAccess << "nNet_SetTensorShapeW()");
      else if (nNet_RemoveTensorShapeW(interpreter, shape))
        return opw << (BadAccess << "nNet_RemoveTensorShapeW()");
      return false;
    }
  } catch(base::Error& error){
    return opw << error;
  }
}

bool Eval(Operator op, Interpreter interpreter) {
  auto opw = nNet_GetOperatorW(op);
  auto input = std::get<1>(opw->inputs()[0]);
  auto output = opw->outputs()[0];

  if (Check(op, interpreter))
    return true;
  else {
    auto mfcc = opw->get<internal::Context *>("context");
    auto stype = nNet_GetTensorNumT(input);
    auto dtype = nNet_GetTensorNumT(output);

    auto error = CError{base::error::ENoError};

    auto spectrogram_channels = input->dims[0];
    auto spectrogram_samples = input->dims[1];
    auto audio_channels = input->dims[2];

    switch (stype) {
    case Real64_t: {
      auto sarray = (Real64 *)nNet_TrimArrayData(input);
      auto darray = (Real64 *)nNet_GetArrayData(output);

      base::Boundary bound{[]() {},
                           [&]() { nNet_TryDropingArray(input, sarray); }};
      for (auto channel = 0; channel < audio_channels; ++channel) {
        for (auto sample = 0; sample < spectrogram_samples; ++sample) {
          auto s_idx = channel * audio_channels * spectrogram_samples +
                       sample * spectrogram_channels;
          auto d_idx =
              channel * spectrogram_samples * mfcc->dct_coefficient_count() +
              sample * mfcc->dct_coefficient_count();

          if ((error = mfcc->compute<Real64, Real64>(
                   &sarray[s_idx], spectrogram_channels, &darray[d_idx],
                   mfcc->dct_coefficient_count())))
            return opw << pError(error, "mfcc->compute<double>()");
        }
      }

      if (!nNet_SetArrayData(output, darray))
        return opw << BadAccess.reason("nNet_SetArrayData()");
      break;
    }

    case Real32_t: {
      auto sarray = (Real32 *)nNet_TrimArrayData(input);
      auto darray = (Real32 *)nNet_GetArrayData(output);

      base::Boundary bound{[]() {},
                           [&]() { nNet_TryDropingArray(input, sarray); }};
      for (auto channel = 0; channel < audio_channels; ++channel) {
        for (auto sample = 0; sample < spectrogram_samples; ++sample) {
          auto s_idx = channel * audio_channels * spectrogram_samples +
                       sample * spectrogram_channels;
          auto d_idx =
              channel * spectrogram_samples * mfcc->dct_coefficient_count() +
              sample * mfcc->dct_coefficient_count();

          if ((error = mfcc->compute<Real32, Real32>(
                   &sarray[s_idx], spectrogram_channels, &darray[d_idx],
                   mfcc->dct_coefficient_count())))
            return opw << pError(error, "mfcc->compute<double>()");
        }
      }

      if (!nNet_SetArrayData(output, darray))
        return opw << BadAccess.reason("nNet_SetArrayData()");
      break;
    }

    default:
      return opw << NoSupport;
    }
    return NoError;
  }
}
} // namespace audiospectrogram

Registration doRegisterMfcc(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = mfcc::Prepare;
  result.eval = mfcc::Eval;
  return result;
}
} // namespace generic
} // namespace kernels
} // namespace nn
