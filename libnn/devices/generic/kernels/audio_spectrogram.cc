#ifdef LIBCGRAPH_GIT_REFSPEC
#include "devices/generic/generic.h"
#include "devices/generic/math.h"
#else
#include "generic.h"
#include "math.h"
#endif

#include "interpreter.h"
#include "tensor.h"
#include "utils.h"

#define _USE_MATH_DEFINES
#include <cmath>

namespace nn{
namespace kernels{
namespace generic{
namespace audio_spectrogram{
using namespace nn::interpreter;

namespace internal{
template<typename Type>
using Spectrogram = std::unique_ptr<std::vector<Type>[]>;

inline int Log2Floor(unsigned int n) {
  if (n == 0) return -1;
  int log = 0;
  unsigned int value = n;

  for (int i = 4; i >= 0; --i) {
    int shift = (1 << i);
    unsigned int x = value >> shift;
    if (x != 0) {
      value = x;
      log += shift;
    }
  }
  return log;
}

inline int Log2Ceiling(unsigned int n) {
  int floor = Log2Floor(n);

  if (n == (n & ~(n - 1)))  // zero or a power of two
    return floor;
  else
    return floor + 1;
}

inline unsigned int NextPowerOfTwo(unsigned int value) {
  int exponent = Log2Ceiling(value);
  return 1 << exponent;
}

struct Context{
 public:
  std::vector<double> window;

 public:
  std::vector<double> fft_real_working_area;
  std::vector<int>    fft_int_working_area;

 public:
  Context():
    _output_frequency_channels{0},
    _window_size{0},
    _stride{0},
    _fft_length{0}
  {}

 public:
  base::Error initialize(int window_size, int stride){
    /* @NOTE: improve peformance by using caching */
    if (window_size == _window_size && stride == _stride)
      return NoError;
    else {
      /* @NOTE: by default, we always use Hann window */
      std::vector<double> window_;

      getPeriodicHann(window_size, window_);
      return initialize(window_, stride);
    }
  }

  base::Error initialize(std::vector<double>& window_, int step_length) {
    window = window_;

    if ((_window_size = window_.size()) < 2)
      return BadLogic.reason("window_length too short");
    else if ((_stride = step_length) < 1)
      return BadLogic.reason("step_length must be positive");
    else {
      _fft_length = NextPowerOfTwo(_window_size);
      _output_frequency_channels = 1 + _fft_length/2;

      fft_int_working_area.assign(2 + static_cast<int>(sqrt(_fft_length/2)), 0);
      fft_real_working_area.assign(_fft_length / 2, 0.0);
      return NoError;
    }
  }

 public:
  inline void lock(){
    /* @TODO: need to implement when we use threadpool */
  }

  inline void unlock(){ 
    /* @TODO: need to implement when we use threadpool */
  }

 public:
  inline int outputFrequencyChannels(){ return _output_frequency_channels; }
  inline int fft_length(){ return _fft_length; }
  inline int window_size(){ return _window_size; }
  inline int step_length(){ return _stride; }

 private:
  static void getPeriodicHann(int window_length, std::vector<double>& window){
   #ifdef M_PI
    const double pi = M_PI;
   #else
    // Some platforms don't have M_PI, so define a local constant here.
    const double pi = std::atan(1) * 4;
   #endif

    window.resize(window_length);
    for (int i = 0; i < window_length; ++i) {
      window[i] = 0.5 - 0.5 * cos((2 * pi * i) / window_length);
    }
  }

 private:
  int _output_frequency_channels, _window_size, _stride, _fft_length;
};

template<typename Input, typename Output>
CError computeSquaredMagnitudeSpectrogram(
    Spectrogram<Output>& result, Tensor input, Context& context, int gid){
  auto array = (Input*)nNet_TrimArrayData(input);
  auto begin = gid * context.step_length();

  auto fft_length = context.fft_length();
  auto fft_input_output = std::vector<double>{};

  base::Boundary bound{[](){}, [&](){ nNet_TryDropingArray(input, array); }};

  fft_input_output.assign(2 + fft_length, 0.0);
  for (int i = begin, n = 0; n < context.fft_length(); ++i, ++n){
    /* @NOTE: fetch data and put it into rdft */

    if (n < cast_(n, context.window.size())){
      double src = array[i*input->dims[0]];

      fft_input_output[n] = src * context.window[n];
    } else 
      fft_input_output[n] = 0.0;
  }

  context.lock();
  rdft(context.fft_length(), 1, &fft_input_output[0],
       &context.fft_int_working_area[0], 
       &context.fft_real_working_area[0]);
  fft_input_output[fft_length] = fft_input_output[1];
  fft_input_output[fft_length + 1] = 0;
  fft_input_output[1] = 0;
  context.unlock();

  for (int j = 0; j < context.outputFrequencyChannels(); ++j) {
    // Similar to the Complex case, except storing the norm.
    // But the norm function is known to be a performance killer,
    // so do it this way with explicit real and imagninary temps.
    auto re = fft_input_output[2 * j];
    auto im = fft_input_output[2 * j + 1];

    // Which finally converts double to float if it needs to.
    result[gid].push_back(re * re + im * im);
  }
  return base::error::ENoError;
}
} // namespace internal

Operator Prepare(Operator op, Interpreter interpreter, CString model){
  try{
    auto attr = OperatorW::config(model, "AudioSpectrogram");

    if (!op->device)
      nNet_GetInterpreterW(interpreter)->error(
        BadLogic.reason("op \'AudioSpectrogram' has been pluged like a macro"));
    else if (attr){
      auto opw = new OperatorW(op, COUNT_AUDIO_SPECTROGRAM_ARGUMENTS,
                               interpreter, op->device, attr);
      auto spectrogram = nNet_CreateTensorC(interpreter, nullptr);

      if (!spectrogram)
        return nNet_RemoveOperatorW(interpreter, opw->interface());

      opw->outputs().push_back(spectrogram);
      return opw->interface();
    } else
      nNet_GetInterpreterW(interpreter)->error(NotFound);
  } catch(base::Error& error){
    nNet_GetInterpreterW(interpreter)->error(rvalue(error));
  }
  return nullptr;
}

bool Check(Operator op, Interpreter interpreter){
  using namespace interpreter;
  using namespace device::generic;

  auto opw    = nNet_GetOperatorW(op);
  auto device = utils::response<DeviceW, CPUManager>(op->device->_priv);

  if (!device)
    return opw << (NoSupport << op->name << "only perform on cpu");
  else {
    auto input  = std::get<1>(opw->inputs()[0]);
    auto output = opw->outputs()[0];

    auto window_size = opw->get<int64_t>("window_size");
    auto stride      = opw->get<int64_t>("stride");

    internal::Context* context;
    base::Error error;
    Shape shape;

    if (nNet_GetTensorNumT(input) == String_t){
      return opw << (NoSupport << "input of" << op->name 
                                             << "must not be a string tensor");
    }
    if (nNet_GetTensorNumT(input) == Bool_t){
      return opw << (NoSupport << "input of" << op->name 
                                             << "must not be bool tensor");
    }
    if (nNet_GetTensorType(input) != MatrixT){
      return opw << (NoSupport << op->name << "only support Matrix");
    }

    /* @NOTE: init FFT table if it's needed */
    if (!opw->exist("context")){
      auto tmp = base::Auto{}.set(context = new internal::Context());

      tmp.delete_() = [](void* data){
        delete (((internal::Context**)data)[0]);
        delete (internal::Context**)data;
      };

      opw->set<base::Auto>("context", tmp);
    } else
      context = opw->get<internal::Context*>("context");

    if ((error = context->initialize(window_size, stride)))
      return opw << error;
    else {
      auto csample = input->dims[1];
      auto owidth = context->outputFrequencyChannels();
      auto oheight =
          (csample > window_size) ? 1 + (csample - window_size) / stride : 0;
      auto type = nNet_GetTensorNumT(input);

      if (nNet_GetTensorType(output) != NilT) {
        if ((output->ndims == 3) && (output->type == type) &&
            (output->dims[0] == owidth) && (output->dims[1] == oheight) &&
            (output->dims[2] == input->dims[0])) {
          if (!nNet_ResetTensor(output))
            return BadAccess.reason("nNet_ResetTensor()");
          return NoError;
        }
      }

      if (oheight > 0) {
        int dims[3];

        dims[0] = owidth;
        dims[1] = oheight;
        dims[2] = input->dims[0];

        shape = nNet_CreateTensorShapeC2(interpreter, 3, dims);
      } else {
        int dims[3];

        dims[0] = owidth;
        dims[1] = oheight;
        dims[2] = input->dims[0];

        shape = nNet_CreateTensorShapeC2(interpreter, 3, dims);
      }

      if (!shape) return opw << DrainMem.reason("nNet_CreateTensorShapeC1()");
      if (!nNet_SetTensorNumTW(interpreter, output, type)) {
        return opw << (BadAccess << "nNet_GetTensorNumT() with type"
                                 << "\'" << nNet_NameType(type) << "\'");
      }

      if (!nNet_SetTensorShapeW(interpreter, output, shape))
        return opw << (BadAccess << "nNet_SetTensorShapeW()");
      if (nNet_RemoveTensorShapeW(interpreter, shape))
        return opw << (BadAccess << "nNet_RemoveTensorShapeW()");

      return NoError;
    }
  }
}

bool Eval(Operator op, Interpreter interpreter){
  using namespace internal;

  if (Check(op, interpreter))
    return true;

  auto opw    = nNet_GetOperatorW(op);
  auto input  = std::get<1>(opw->inputs()[0]);
  auto output = opw->outputs()[0];
  auto error  = int{base::error::ENoError};

  auto mag_squared = opw->get<bool>("magnitude_squared");
  auto context     = opw->get<internal::Context*>("context");

  auto array = nNet_GetArrayData(output);
  auto type  = nNet_GetTensorNumT(output);
  auto idx   = 0;

  switch(type){
  case Real32_t:
  {
    auto spectrograms = Spectrogram<Real32>(new std::vector<Real32>[output->dims[1]]);

    switch(nNet_GetTensorNumT(input)){
    case Real32_t:
      for (auto n = 0; n < output->dims[1]; ++n){
        error = computeSquaredMagnitudeSpectrogram<Real32, Real32>(
            spectrograms, input, *context, n);
      }
      break;

    case Real64_t:
      for (auto n = 0; n < output->dims[1]; ++n){
        error = computeSquaredMagnitudeSpectrogram<Real64, Real32>(
            spectrograms, input, *context, n);
      }
      break;

    default:
      return opw << NoSupport;
    }

    if (error)
      return opw << pError(error, "computeSquaredMagnitudeSpectrogram<Real32>");

    for (auto c = 0; c < output->dims[2]; ++c){
      for (auto n = 0; n < output->dims[1]; ++n){
        for (auto i = 0; i < cast_(i, spectrograms[n].size()); ++i){
          auto value = (mag_squared)? spectrograms[n][i]: sqrtf(spectrograms[n][i]);
          auto dst   = nNet_GetArrayItem(array, idx++, type);

          if (!nNet_ConvertNumT(&value, type, dst, type)){
            return opw << BadAccess.reason("nNet_ConvertNumT(Real32 -> Real32)");
          }
        }
      }
    }
    break;
  }

  case Real64_t:
  {
    auto spectrograms = Spectrogram<Real64>(new std::vector<Real64>[input->dims[0]]);

    
    switch(nNet_GetTensorNumT(input)){
    case Real32_t:
      for (auto n = 0; n < output->dims[1]; ++n){
        error = computeSquaredMagnitudeSpectrogram<Real32, Real64>(
            spectrograms, input, *context, n);
      }
      break;

    case Real64_t:
      for (auto n = 0; n < output->dims[1]; ++n){
        error = computeSquaredMagnitudeSpectrogram<Real64, Real64>(
            spectrograms, input, *context, n);
      }
      break;

    default:
      return opw << NoSupport;
    }

    if (error)
      return opw << pError(error, "computeSquaredMagnitudeSpectrogram<Real64>()");

    for (auto n = 0; n < input->dims[1]; ++n){
      for (auto i = 0; i < cast_(i, spectrograms[n].size()); ++i){
        auto value = (mag_squared)? spectrograms[n][i]: sqrtf(spectrograms[n][i]);
        auto dst   = nNet_GetArrayItem(array, idx++, type);

        if (!nNet_ConvertNumT(&value, type, dst, type)){
          return opw << BadAccess.reason("nNet_ConvertNumT(Real32 -> Real32)");
        }
      }
    }
    break;
  }

  default:
    return opw << NoSupport;
  }

  return !nNet_SetArrayData(output, array);
}
} // namespace audio_spectrogram

Registration doRegisterAudioSpectrogram(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = audio_spectrogram::Prepare;
  result.eval = audio_spectrogram::Eval;
  return result;
}
} // namespace generic
} // namespace kernels
} // namespace nn
