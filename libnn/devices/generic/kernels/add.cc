#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

#ifdef LIBCGRAPH_GIT_REFSPEC
#include "devices/generic/generic.h"
#else
#include "generic.h"
#endif

#include <stdio.h>
namespace nn{
namespace kernels{
namespace generic{
namespace add{
using namespace nn::interpreter;

namespace internal{
template<typename Input, typename Output>
bool runAddWithoutOpenMP(OperatorW* opw, Tensor input, Tensor output) {
  auto dst = (Output*)nNet_GetArrayData(output);  
  auto src = (Input*)nNet_TrimArrayData(input);

  base::Boundary bound{[]() {}, [&]() { nNet_TryDropingArray(input, src); }};

  /* @NOTE: perform operator Add between matrix with matrix */
  if (nNet_GetTensorType(input) != ScalarT) {
    for (auto di = 0; di < nNet_GetArraySizeT(output);) {
      for (auto si = 0; si < nNet_GetArraySizeT(input); ++si, ++di) {
        dst[di] += src[si];
      }
    }
  } else {
    for (auto di = 0; di < nNet_GetArraySizeT(output); ++di) {
      dst[di] += *src;
    }
  }

  if (nNet_SetArrayData(output, dst))
    return false;
  else
    return opw << (BadAccess.reason("nNet_SetArrayData()"));
}

template<typename Input, typename Output>
bool runAddWithOpenMP(OperatorW* opw, Tensor input, Tensor output){
 #if defined(_OPENMP)
  auto dst   = (Output*)nNet_GetArrayData(output);  
  auto src   = (Input*)nNet_TrimArrayData(input);
  auto dsize = nNet_GetArraySizeT(output);
  auto ssize = nNet_GetTensorSize(input);

  base::Boundary bound{[]() {}, [&]() { nNet_TryDropingArray(input, src); }};

  /* @NOTE: perform operator Add between matrix with matrix */
  if (nNet_GetTensorType(input) != ScalarT) {
    if (input->type == String_t || input->type == Bool_t)
      return opw << NoSupport.reason("math::add()");

    /* @NOTE: by default, our interpreter only accept accessing Tensor within
     * consumer and provider threads */
    #pragma omp parallel for
    for (int di = 0; di < dsize; ++di) {
      dst[di] += src[di%ssize];
    }
  } else {
    /* @NOTE: by default, our interpreter only accept accessing Tensor within
     * consumer and provider threads */
    #pragma omp parallel for
    for (int di = 0; di < dsize; ++di) {
      dst[di] += *src;
    }
  }

  if (nNet_SetArrayData(output, dst))
    return NoError;
  else
    return opw << (BadAccess.reason("nNet_SetArrayData()"));
 #else
  return runAddWithoutOpenMP<Input, Output>(opw, input, output);
 #endif
}
} // namespace internal

Operator Prepare(Operator op, Interpreter interpreter, CString model){
  try{
    auto attr = OperatorW::config(model, "Add");

    if (!op->device){
      nNet_GetInterpreterW(interpreter)->error(
                    BadLogic.reason("op \'Add' has been pluged like a macro"));
    } else if (attr){
      auto add = nNet_CreateTensorC(interpreter, nullptr);
      auto opw = new interpreter::OperatorW(op, COUNT_ADD_ARGUMENTS, interpreter,
                                            op->device, attr);

      /* @NOTE: prepare outputs */
      if (!add)
        return nNet_RemoveOperatorW(interpreter, opw->interface());
      opw->outputs().push_back(add);
      return opw->interface();
    } else
      nNet_GetInterpreterW(interpreter)->error(NotFound);
  } catch(base::Error& error){
    nNet_GetInterpreterW(interpreter)->error(rvalue(error));
  }
  return nullptr;
}

std::string PrintDimension(std::vector<int>& dims, int size){
  std::string result = "[ ";

  if (size > cast_(size, dims.size()))
    result += "...,";

  for (auto i = 0; i < cast_(i, dims.size() - 1); ++i){
    result += std::to_string(dims[i]) + ", ";
  }

  result += std::to_string(dims.back()) + " ]";
  return result;
}

bool Check(Operator op, Interpreter interpreter){
  auto opw = nNet_GetOperatorW(op);

  /* @NOTE: check inputs, it's necessity because everything will change before 
   * Eval can be called directly from an interpreter */

  if (opw->inputs().size() < 2){
    return opw << (BadLogic << "need at least 2 inputs but" << op->name 
                            << "have" << std::to_string(opw->inputs().size()));
  } else {
    auto dims   = std::vector<int>{};
    auto output = opw->outputs()[0];
    auto shape  = Shape{nullptr};
    auto keep   = false;

    /* @NOTE: check tensors, we must make sure everything is still on alive and
     * have the same size */
    if (nn::utils::outdate(interpreter, output))
      return opw << (BadAccess << "output is outdate");

    for (auto& item: opw->inputs()){
      auto input = std::get<1>(item);
      auto name  = std::get<0>(item);

      if (input->type == String_t || input->type == Bool_t){
        return opw << (BadLogic << "Operator \'Add\' can't perform because"
                                << "input" << op->name << "is string tensor");
      }

      if (nn::utils::outdate(interpreter, input)){
        return opw << (BadAccess << "input" << name << "is out date");
      } else {
        for (auto i = 0; i < cast_(i, dims.size()) && i < input->ndims; ++i){
          if (dims[i] != input->dims[i]){
            return opw << (BadAccess << "shape(" << name << ") = " 
                                     << nn::utils::shape(input)
                                     << " isn\'t compatible with " 
                                     << PrintDimension(dims, input->ndims));
          }
        }

        for (auto i = dims.size(); i < cast_(i, input->ndims); ++i){
          dims.push_back(input->dims[i]);
        }
      }
    }

    /* @NOTE: improve performance of streaming to CGraph by reducing recreating
     * output too much 
     */
    if (nNet_GetTensorType(output) == NilT)
      keep = true;
    else if (nNet_GetTensorNumT(output) != opw->get<NumT>("T"))
      keep = true;
    else {
      for (auto i = 0; keep && i < cast_(i, dims.size()); ++i){
        if (dims[i] != output->dims[i]){
          keep = true;
        }
      }
    }

    if (!keep){
      if (!nNet_ResetTensor(output))
        return opw << BadAccess.reason("nNet_ResetTensor()");
    } else if (!(shape = nNet_CreateTensorShapeC2(interpreter, dims.size(), 
                                                dims.data())))
      return opw << (BadAccess.reason("nNet_CreateTensorShapeC2()"));
    else if (nNet_GetTensorType(output = opw->outputs()[0]) == NilT){
      auto type = opw->get<NumT>("T");

      if (!nNet_SetTensorNumTW(interpreter, output, type))
        return opw << (BadAccess.reason("nNet_SetTensorNumTW"));
      if (!nNet_SetTensorShapeW(interpreter, output, shape))
        return opw << (BadAccess.reason("nNet_SetTensorShapeW"));

      if (nNet_RemoveTensorShapeW(interpreter, shape))
        return opw << (BadLogic.reason("nNet_RemoveTensorShapeW"));

     #if USE_VECTORIZE
      if (!nNet_ResetTensor(output))
        return opw << BadAccess.reason("nNet_ResetTensor()");
     #endif
    }
    return NoError;
  }
}

bool Eval(Operator op, Interpreter interpreter){
  auto opw = nNet_GetOperatorW(op);

  if (Check(op, interpreter)) return true;
  else{
    /* @NOTE: by default, OpenMP will redirect to runAddWithOpenMP if the 
     * flag -fopenmp isn't set on 
     */
    auto output = opw->outputs()[0];


    switch(output->type){
    case Int32_t:
      for (auto i = 0; i < cast_(i, opw->inputs().size()); ++i){
        auto input = std::get<1>(opw->inputs()[i]);

        switch (input->type){
        case Int32_t:
          internal::runAddWithOpenMP<Int32, Int32>(opw, input, output);
          break;

        case Int64_t:
          internal::runAddWithOpenMP<Int64, Int32>(opw, input, output);
          break;

        case Real32_t:
          internal::runAddWithOpenMP<Real32, Int32>(opw, input, output);
          break;

        case Real64_t:
          internal::runAddWithOpenMP<Real64, Int32>(opw, input, output);
          break;

        default:
          return opw << NoSupport;
        }
      }
      break;

    case Int64_t:
      for (auto i = 0; i < cast_(i, opw->inputs().size()); ++i){
        auto input = std::get<1>(opw->inputs()[i]);

        switch(input->type){
        case Int32_t:
          internal::runAddWithOpenMP<Int32, Int64>(opw, input, output);
          break;

        case Int64_t:
          internal::runAddWithOpenMP<Int64, Int64>(opw, input, output);
          break;

        case Real32_t:
          internal::runAddWithOpenMP<Real32, Int64>(opw, input, output);
          break;

        case Real64_t:
          internal::runAddWithOpenMP<Real64, Int64>(opw, input, output);
          break;

        default:
          return opw << NoSupport;
        }
      }
      break;

    case Real32_t:
      for (auto i = 0; i < cast_(i, opw->inputs().size()); ++i){
        auto input = std::get<1>(opw->inputs()[i]);

        switch(input->type){
        case Int32_t:
          internal::runAddWithOpenMP<Int32, Real32>(opw, input, output);
          break;

        case Int64_t:
          internal::runAddWithOpenMP<Int64, Real32>(opw, input, output);
          break;

        case Real32_t:
          internal::runAddWithOpenMP<Real32, Real32>(opw, input, output);
          break;

        case Real64_t:
          internal::runAddWithOpenMP<Real64, Real32>(opw, input, output);
          break;

        default:
          return opw << NoSupport;
        }
      }
      break;

    case Real64_t:
      for (auto i = 0; i < cast_(i, opw->inputs().size()); ++i){
        auto input = std::get<1>(opw->inputs()[i]);

        switch(input->type){
        case Int32_t:
          internal::runAddWithOpenMP<Int32, Real64>(opw, input, output);
          break;

        case Int64_t:
          internal::runAddWithOpenMP<Int64, Real64>(opw, input, output);
          break;

        case Real32_t:
          internal::runAddWithOpenMP<Real32, Real64>(opw, input, output);
          break;

        case Real64_t:
          internal::runAddWithOpenMP<Real64, Real64>(opw, input, output);
          break;

        default:
          return opw << NoSupport;
        }
      }
      break;

    default:
      return opw << NoSupport;
    }
    return false;
  }
}

bool Derive(Operator op, Interpreter interpreter){
  return NoError;
}
} // namespace add

Registration doRegisterAdd(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = add::Prepare;
  result.eval   = add::Eval;
  result.derive = add::Derive;
  return result;
}
} // namespace generic
} // namespace kernels
} // namespace nn
