#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

#ifdef LIBCGRAPH_GIT_REFSPEC
#include "devices/generic/generic.h"
#else
#include "generic.h"
#endif

namespace nn{
namespace kernels{
namespace generic{
namespace matmul{
using namespace nn::interpreter;

namespace internal{
template<typename Left, typename Right, typename Output>
bool runMatmulWithoutOpenMP(OperatorW* opw) {
  auto left   = std::get<1>(opw->inputs()[0]);
  auto right  = std::get<1>(opw->inputs()[1]);
  auto output = opw->outputs()[0];

  auto dst_array   = (Output*)nNet_GetArrayData(output);
  auto left_array  = (Left*)nNet_TrimArrayData(left);
  auto right_array = (Right*)nNet_TrimArrayData(right);

  auto done = false;
  auto n = left->dims[0];
  auto k = right->dims[0];
  auto m = left->ndims == 2 ? left->dims[1] : 0;

 #if ENABLE_STRASSEN_ALGORITHM
  /* @NOTE: this is faster slightly than naive algorithm and has more effect
   * with large matrix but it only works on square matrix */

  if (n == k && n == m) {
    done = true;
  }
 #elif ENABLE_COPPERSMITH_WINOGRAD_ALGORITHM
 #endif

  if (!done) {
    /* @NOTE: by default MatMul will use naive algorithm, it's a litter bit
     * slow but it's quite simple and very easy to implement */

    for (auto x = 0; x < k; ++x) {
      for (auto y = 0; y < m; ++y) {
        dst_array[k * y + x] = 0;

        for (auto i = 0; i < n; ++i) {
          dst_array[k*y + x] += left_array[n * y + i]*right_array[k * i + x];
        }
      }
    }
  }

  if (!nNet_SetArrayData(output, dst_array))
    opw << (BadAccess << "nNet_SetArrayData()");

  nNet_TryDropingArray(left, left_array);
  nNet_TryDropingArray(right, right_array);
  return *opw;
}

bool runInnerLoopWithOpenMP(){
  return false;
}

template<typename Left, typename Right, typename Output>
bool runMatmulWithOpenMP(OperatorW* opw){

 #if defined(_OPENMP)
  auto left = std::get<1>(opw->inputs()[0]);
  auto right = std::get<1>(opw->inputs()[1]);
  auto output = opw->outputs()[0];

  auto dst_array   = (Output*)nNet_GetArrayData(output);
  auto left_array  = (Left*)nNet_TrimArrayData(left);
  auto right_array = (Right*)nNet_TrimArrayData(right);

  auto done = false;
  auto n = left->dims[0];
  auto k = right->dims[0];
  auto m = left->ndims == 2 ? left->dims[1] : 0;

 #if ENABLE_STRASSEN_ALGORITHM
  /* @TODO: this is faster slightly than naive algorithm and has more effect
   * with large matrix but it only works on square matrix */

  if (n == k && n == m) {
    done = true;
  }
 #elif ENABLE_COPPERSMITH_WINOGRAD_ALGORITHM
  /* @TDO: this is fastest algorithm to do matmul */
 #endif

  if (!done) {
    /* @NOTE: by default MatMul will use naive algorithm, it's a litter bit
     * slow but it's quite simple and very easy to implement */

    omp_set_nested(1);

    #pragma opm parallel for collapse(2)
    for (auto x = 0; x < k; ++x) {
      for (auto y = 0; y < m; ++y) {
        Output dst = 0;

        #pragma omp simd reduction(+:dst)
        for (auto i = 0; i < n; ++i) {
          dst += left_array[n*y + i]*right_array[k*i + x];
        }

        dst_array[k*y + x] = dst;
      }
    }
  }

  if (!nNet_SetArrayData(output, dst_array))
    opw << (BadAccess << "nNet_SetArrayData()");

  nNet_TryDropingArray(left, left_array);
  nNet_TryDropingArray(right, right_array);
  return *opw;
 #else
  /* @NOTE: reroute to runMathmulWithoutOpenMP if OpenMP is not supported */

  return runMatmulWithoutOpenMP<Left, Right, Output>(opw);
 #endif
}
} // namespace internal

Operator Prepare(Operator op, Interpreter interpreter, CString model){
  try{
    auto attr = OperatorW::config(model, "MatMul");

    if (!op->device)
      nNet_GetInterpreterW(interpreter)->error(
                 BadLogic.reason("op \'MatMul' has been pluged like a macro"));
    else if (attr){
      auto matmul = nNet_CreateTensorC(interpreter, nullptr);
      auto opw    = new interpreter::OperatorW(op, COUNT_MATMUL_ARGUMENTS, 
                                               interpreter, op->device,
                                               attr);

      if (!matmul)
        return nNet_RemoveOperatorW(interpreter, opw->interface());
      opw->outputs().push_back(matmul);
      return opw->interface();
    } else
      nNet_GetInterpreterW(interpreter)->error(NotFound);
  } catch(base::Error& error){
    nNet_GetInterpreterW(interpreter)->error(rvalue(error));
  }
  return nullptr;
}

bool Check(Operator op, Interpreter interpreter){
  auto opw  = nNet_GetOperatorW(op);
  auto keep = true;

  Tensor left{nullptr}, right{nullptr};

  /* @NOTE: check inputs, it's necessity because everything will change before 
   * Eval can be called directly from an interpreter */

  if (opw->inputs().size() != 2){
    return opw << (BadLogic << "need 2 inputs but op" << op->name << "have" 
                               << std::to_string(opw->inputs().size()));
  } else {
    auto output = opw->outputs()[0];

    /* @NOTE: check tensors, we must make sure everything is still on alive and
     * have the same size */
    if (nn::utils::outdate(interpreter, output))
      return opw << (BadAccess << "output is outdate");

    if (nNet_GetTensorType(output) == NilT)
      output = opw->inputs().begin()->second;

    for (auto& item: opw->inputs()){
      auto input = std::get<1>(item);
      auto name  = std::get<0>(item);

      if (nn::utils::outdate(interpreter, input)){
        return opw << (BadAccess << "input" << name << "is out date");
      } else if (nNet_GetTensorTypeW(interpreter, input) == TensorT){
        return opw << (BadAccess << "input" << name << "shouldn't be a tensor");
      } else if (!left){
        left = input;
      } else {
        right = input;
        break;
      }
    }

    /* @NOTE: verify dimensions */
    if (left->ndims == 1 && right->dims[1] != 1)
      return opw << BadLogic;
    else if (right->ndims == 1 && (left->ndims == 1 || left->dims[0] != 1))
      return opw << BadLogic;
    else if (left->dims[0] != right->dims[1])
      return opw << BadLogic;

    /* @NOTE: verify output and allocate output tensor */
    if (nNet_GetTensorType(output = opw->outputs()[0]) != NilT){
      if ((output->ndims == 2) &&
          (output->type == opw->get<NumT>("T")) &&
          (output->dims[0] == right->dims[0]) &&
          (output->dims[1] == left->dims[1])){
        keep = false;
      }
    }

    if (keep){
      auto shape = nNet_CreateTensorShapeC1(interpreter, 2,
                                            right->dims[0], left->dims[1]);
      auto type  = opw->get<NumT>("T");

      if (!nNet_SetTensorNumTW(interpreter, output, type))
        return opw << (BadAccess.reason("nNet_SetTensorNumTW"));
      if (!nNet_SetTensorShapeW(interpreter, output, shape))
        return opw << (BadAccess.reason("nNet_SetTensorShapeW"));

      if (nNet_RemoveTensorShapeW(interpreter, shape))
        return opw << (BadLogic.reason("nNet_RemoveTensorShape"));
    } else {
      if (!nNet_ResetTensor(output))
        return BadAccess.reason("nNet_ResetTensor()");
    }

    return false;
  }
}

bool Eval(Operator op, Interpreter interpreter){
  auto opw = nNet_GetOperatorW(op);

  if (Check(op, interpreter))
    return true;
  else{
    auto left = std::get<1>(opw->inputs()[0]);
    auto right = std::get<1>(opw->inputs()[1]);
    auto output = opw->outputs()[0];

    switch(left->type){
    case Int32_t:
      switch(right->type){
      case Int32_t:
        switch(output->type){
        case Int32_t:
          return internal::runMatmulWithOpenMP<Int32, Int32, Int32>(opw);

        case Int64_t:
          return internal::runMatmulWithOpenMP<Int32, Int32, Int64>(opw);

        case Real32_t:
          return internal::runMatmulWithOpenMP<Int32, Int32, Real32>(opw);

        case Real64_t:
          return internal::runMatmulWithOpenMP<Int32, Int32, Real64>(opw);

        default:
          return opw << NoSupport;
        }

      case Int64_t:
        switch(output->type){
        case Int32_t:
          return internal::runMatmulWithOpenMP<Int32, Int64, Int32>(opw);

        case Int64_t:
          return internal::runMatmulWithOpenMP<Int32, Int64, Int64>(opw);

        case Real32_t:
          return internal::runMatmulWithOpenMP<Int32, Int64, Real32>(opw);

        case Real64_t:
          return internal::runMatmulWithOpenMP<Int32, Int64, Real64>(opw);

        default:
          return opw << NoSupport;
        }

      case Real32_t:
        switch(output->type){
        case Int32_t:
          return internal::runMatmulWithOpenMP<Int32, Real32, Int32>(opw);

        case Int64_t:
          return internal::runMatmulWithOpenMP<Int32, Real32, Int64>(opw);

        case Real32_t:
          return internal::runMatmulWithOpenMP<Int32, Real32, Real32>(opw);

        case Real64_t:
          return internal::runMatmulWithOpenMP<Int32, Real32, Real64>(opw);

        default:
          return opw << NoSupport;
        }

      case Real64_t:
        switch(output->type){
        case Int32_t:
          return internal::runMatmulWithOpenMP<Int32, Real64, Int32>(opw);

        case Int64_t:
          return internal::runMatmulWithOpenMP<Int32, Real64, Int64>(opw);

        case Real32_t:
          return internal::runMatmulWithOpenMP<Int32, Real64, Real32>(opw);

        case Real64_t:
          return internal::runMatmulWithOpenMP<Int32, Real64, Real64>(opw);

        default:
          return opw << NoSupport;
        }

      default:
        break;
      }
      return opw << NoSupport;

    case Int64_t:
      switch(right->type){
      case Int32_t:
        switch(output->type){
        case Int32_t:
          return internal::runMatmulWithOpenMP<Int64, Int32, Int32>(opw);

        case Int64_t:
          return internal::runMatmulWithOpenMP<Int64, Int32, Int64>(opw);

        case Real32_t:
          return internal::runMatmulWithOpenMP<Int64, Int32, Real32>(opw);

        case Real64_t:
          return internal::runMatmulWithOpenMP<Int64, Int32, Real64>(opw);

        default:
          return opw << NoSupport;
        }

      case Int64_t:
        switch(output->type){
        case Int32_t:
          return internal::runMatmulWithOpenMP<Int64, Int64, Int32>(opw);

        case Int64_t:
          return internal::runMatmulWithOpenMP<Int64, Int64, Int64>(opw);

        case Real32_t:
          return internal::runMatmulWithOpenMP<Int64, Int64, Real32>(opw);

        case Real64_t:
          return internal::runMatmulWithOpenMP<Int64, Int64, Real64>(opw);

        default:
          return opw << NoSupport;
        }

      case Real32_t:
        switch(output->type){
        case Int32_t:
          return internal::runMatmulWithOpenMP<Int64, Real32, Int32>(opw);

        case Int64_t:
          return internal::runMatmulWithOpenMP<Int64, Real32, Int64>(opw);

        case Real32_t:
          return internal::runMatmulWithOpenMP<Int64, Real32, Real32>(opw);

        case Real64_t:
          return internal::runMatmulWithOpenMP<Int64, Real32, Real64>(opw);

        default:
          return opw << NoSupport;
        }

      case Real64_t:
        switch(output->type){
        case Int32_t:
          return internal::runMatmulWithOpenMP<Int64, Real64, Int32>(opw);

        case Int64_t:
          return internal::runMatmulWithOpenMP<Int64, Real64, Int64>(opw);

        case Real32_t:
          return internal::runMatmulWithOpenMP<Int64, Real64, Real32>(opw);

        case Real64_t:
          return internal::runMatmulWithOpenMP<Int64, Real64, Real64>(opw);

        default:
          return opw << NoSupport;
        }

      default:
        break;
      }
      return opw << NoSupport;

    case Real32_t:
      switch(right->type){
      case Int32_t:
        switch(output->type){
        case Int32_t:
          return internal::runMatmulWithOpenMP<Real32, Int32, Int32>(opw);

        case Int64_t:
          return internal::runMatmulWithOpenMP<Real32, Int32, Int64>(opw);

        case Real32_t:
          return internal::runMatmulWithOpenMP<Real32, Int32, Real32>(opw);

        case Real64_t:
          return internal::runMatmulWithOpenMP<Real32, Int32, Real64>(opw);

        default:
          return opw << NoSupport;
        }

      case Int64_t:
        switch(output->type){
        case Int32_t:
          return internal::runMatmulWithOpenMP<Real32, Int64, Int32>(opw);

        case Int64_t:
          return internal::runMatmulWithOpenMP<Real32, Int64, Int64>(opw);

        case Real32_t:
          return internal::runMatmulWithOpenMP<Real32, Int64, Real32>(opw);

        case Real64_t:
          return internal::runMatmulWithOpenMP<Real32, Int64, Real64>(opw);

        default:
          return opw << NoSupport;
        }

      case Real32_t:
        switch(output->type){
        case Int32_t:
          return internal::runMatmulWithOpenMP<Real32, Real32, Int32>(opw);

        case Int64_t:
          return internal::runMatmulWithOpenMP<Real32, Real32, Int64>(opw);

        case Real32_t:
          return internal::runMatmulWithOpenMP<Real32, Real32, Real32>(opw);

        case Real64_t:
          return internal::runMatmulWithOpenMP<Real32, Real32, Real64>(opw);

        default:
          return opw << NoSupport;
        }

      case Real64_t:
        switch(output->type){
        case Int32_t:
          return internal::runMatmulWithOpenMP<Real32, Real64, Int32>(opw);

        case Int64_t:
          return internal::runMatmulWithOpenMP<Real32, Real64, Int64>(opw);

        case Real32_t:
          return internal::runMatmulWithOpenMP<Real32, Real64, Real32>(opw);

        case Real64_t:
          return internal::runMatmulWithOpenMP<Real32, Real64, Real64>(opw);

        default:
          return opw << NoSupport;
        }

      default:
        break;
      }
      return opw << NoSupport;

    case Real64_t:
      switch(right->type){
      case Int32_t:
        switch(output->type){
        case Int32_t:
          return internal::runMatmulWithOpenMP<Real64, Int32, Int32>(opw);

        case Int64_t:
          return internal::runMatmulWithOpenMP<Real64, Int32, Int64>(opw);

        case Real32_t:
          return internal::runMatmulWithOpenMP<Real64, Int32, Real32>(opw);

        case Real64_t:
          return internal::runMatmulWithOpenMP<Real64, Int32, Real64>(opw);

        default:
          return opw << NoSupport;
        }

      case Int64_t:
        switch(output->type){
        case Int32_t:
          return internal::runMatmulWithOpenMP<Real64, Int64, Int32>(opw);

        case Int64_t:
          return internal::runMatmulWithOpenMP<Real64, Int64, Int64>(opw);

        case Real32_t:
          return internal::runMatmulWithOpenMP<Real64, Int64, Real32>(opw);

        case Real64_t:
          return internal::runMatmulWithOpenMP<Real64, Int64, Real64>(opw);

        default:
          return opw << NoSupport;
        }

      case Real32_t:
        switch(output->type){
        case Int32_t:
          return internal::runMatmulWithOpenMP<Real64, Real32, Int32>(opw);

        case Int64_t:
          return internal::runMatmulWithOpenMP<Real64, Real32, Int64>(opw);

        case Real32_t:
          return internal::runMatmulWithOpenMP<Real64, Real32, Real32>(opw);

        case Real64_t:
          return internal::runMatmulWithOpenMP<Real64, Real32, Real64>(opw);

        default:
          return opw << NoSupport;
        }

      case Real64_t:
        switch(output->type){
        case Int32_t:
          return internal::runMatmulWithOpenMP<Real64, Real64, Int32>(opw);

        case Int64_t:
          return internal::runMatmulWithOpenMP<Real64, Real64, Int64>(opw);

        case Real32_t:
          return internal::runMatmulWithOpenMP<Real64, Real64, Real32>(opw);

        case Real64_t:
          return internal::runMatmulWithOpenMP<Real64, Real64, Real64>(opw);

        default:
          return opw << NoSupport;
        }

      default:
        break;
      }
      return opw << NoSupport;

    default:
      return opw << NoSupport;
    }
  }
}

bool Derive(Operator op, Interpreter interpreter){
  return false;
}
} // namespace matmul

Registration doRegisterMatMul(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = matmul::Prepare;
  result.eval   = matmul::Eval;
  result.derive = matmul::Derive;

  return result;
}
} // namespace generic
} // namespace kernels
} // namespace nn
