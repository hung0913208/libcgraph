#include "interpreter.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
namespace kernels{
namespace generic{
namespace reshape{
using namespace nn::interpreter;

Operator Prepare(Operator op, Interpreter interpreter, CString model){
  try{
    auto attr = OperatorW::config(model, "Reshape");

    if (!op->device)
      nNet_GetInterpreterW(interpreter)->error(
                 BadLogic.reason("op \'Reshape' has been pluged like a macro"));
    else if (attr){
      auto reshape = nNet_CreateTensorC(interpreter, nullptr);
      auto opw     = new interpreter::OperatorW(op, 2, interpreter, 
                                                op->device, attr);

      if (!reshape)
        return nNet_RemoveOperatorW(interpreter, opw->interface());
      opw->outputs().push_back(reshape);
      return opw->interface();
    } else
      nNet_GetInterpreterW(interpreter)->error(NotFound);
  } catch(base::Error& error){
    nNet_GetInterpreterW(interpreter)->error(rvalue(error));
  }
  return nullptr;
}

bool Eval(Operator op, Interpreter interpreter){
  auto opw    = nNet_GetOperatorW(op);
  auto input  = std::get<1>(opw->inputs()[0]);
  auto vector = std::get<1>(opw->inputs()[1]);
  auto output = opw->outputs()[0];
  auto stype  = nNet_GetTensorNumT(input);
  auto dtype  = opw->get<NumT>("T");
  auto tshape = opw->get<NumT>("Tshape");
  auto dims   = std::vector<int>{};

  int   fixing_point{-1}, divisor_value{1};
  Shape shape{nullptr};

  /* @NOTE: check if inputs are null */
  if (nNet_GetTensorType(input) == NilT){
    return opw << (BadLogic << "tensor \'input\' of operator" << op->name 
                            << "mustn\'t be null");
  } else if (nNet_GetTensorType(input) == NilT){
    return opw << (BadLogic << "tensor \'shape\' of operator" << op->name 
                            << "mustn\'t be null");
  }

  /* @NOTE: clear output if it stored value */
  if (nNet_GetTensorType(output) != NilT)
    nNet_ClearTensorW(interpreter, output);

  /* @NOTE: check type of input 'shape' */
  if (nNet_GetTensorNumT(vector) != tshape)
    return opw << (BadLogic << "input" << std::get<0>(opw->inputs()[1])
                            << "must be build with type" 
                            << nn::utils::print(NumT(tshape)));

  switch (tshape){
  case Int32_t:
    for (auto d: nn::utils::values<Int32>(vector))
      dims.push_back(d);
    break;

  case Int64_t:
    for (auto d: nn::utils::values<Int64>(vector))
      dims.push_back(d);
    break;

  case Real32_t:
    for (auto d: nn::utils::values<Real32>(vector))
      dims.push_back(int(d));
    break;

  case Real64_t:
    for (auto d: nn::utils::values<Real64>(vector))
      dims.push_back(int(d));
    break;

  default:
    return opw << (NoSupport << "TShape == " << nn::utils::print(NumT(tshape)));
  }

  /* @NOTE: check again to make sure dims[i] > 0 with i in range(0, ndims) */
  for (auto i = 0; i < cast_(i, dims.size()); ++i){
    if (dims[i] > 0){
      divisor_value *= dims[i];
      continue;
    } else if (dims[i] == 0){
      return opw << (BadLogic << "op" << op->name << "require input != 0");
    } else if (fixing_point != -1){
      return opw << (BadLogic << "Only one input size with value -1, not more");
    } else {
      fixing_point = i;
    }
  }

  if ((nNet_GetArraySizeT(input) % divisor_value)){
    return opw << (BadLogic << "Dimension size must be evenly divisible by"
                            << std::to_string(divisor_value)
                            << "but input shape" << nn::utils::shape(input)
                            << "(" << std::to_string(nNet_GetArraySizeT(input))
                            << ") can\'t apply with" << nn::utils::shape(dims));
  } else if (fixing_point >= 0) {
    dims[fixing_point] = nNet_GetArraySizeT(input)/divisor_value;
  }

  std::reverse(dims.begin(), dims.end());

  if (!(shape = nNet_CreateTensorShapeC2(interpreter, dims.size(), dims.data())))
    return opw << BadAccess;

  if (!nNet_SetTensorNumTW(interpreter, output, dtype))
    return opw << BadAccess;
  else if (!nNet_SetTensorShapeW(interpreter, output, shape))
    return opw << BadAccess;
  else {
    auto dst_array = nNet_GetArrayData(output);
    auto src_array = nNet_GetArrayData(input);
    auto success   = true;

    for (auto i = 0; success && i < nNet_GetArraySizeT(input); ++i){
      auto src = nNet_GetArrayItem(src_array, i, dtype);
      auto dst = nNet_GetArrayItem(dst_array, i, dtype);

      success = nNet_ConvertNumT(src, stype, dst, dtype);
    }

    if (!success)
      return opw << BadAccess.reason("nNet_ConvertNumT()");
    else if(!nNet_SetArrayData(output, dst_array))
      return opw << BadAccess.reason("nNet_SetArrayData()");
  }
  return NoError;
}

bool Derive(Operator op, Interpreter interpreter){
  return false;
}
} // namespace reshape

Registration doRegisterReshape(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = reshape::Prepare;
  result.eval   = reshape::Eval;
  result.derive = reshape::Derive;
  return result;
}
} // namespace generic
} // namespace kernels
} // namespace nn
