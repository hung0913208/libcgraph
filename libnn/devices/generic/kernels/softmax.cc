#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

#include <cmath>

namespace nn{
namespace kernels{
namespace generic{
namespace softmax{
using namespace nn::interpreter;

namespace internal{
template<typename Output>
bool runSoftmaxWithoutOpenMP(int loop, OperatorW* opw, Output reduced){
  auto output    = opw->outputs()[0];
  auto dst_array = (Output*)nNet_GetArrayData(output);

  for (auto i = 0; i < loop; ++i){
    dst_array[i] = dst_array[i]/reduced;
  }

  if (!nNet_SetArrayData(output, dst_array))
    return opw << BadAccess.reason("nNet_SetArrayData()");
  return false;
}

template<typename Input, typename Output>
bool runSoftmaxWithOpenMP(int loop, OperatorW* opw, Output reduced){
 #if defined(_OPENMP)
  auto output    = opw->outputs()[0];
  auto dst_array = (Output*)nNet_GetArrayData(output);

  #pragma omp parallel for
  for (auto i = 0; i < loop; ++i){
    dst_array[i] = dst_array[i]/reduced;
  }

  if (!nNet_SetArrayData(output, dst_array))
    return opw << BadAccess.reason("nNet_SetArrayData()");
  return false;
 #else
  return runSoftmaxWithoutOpenMP<Output>(loop, opw, reduced);
 #endif
}

template<typename Input, typename Output>
void runReduceSumWithoutOpenMP(OperatorW* opw, Output& result){
  auto input  = std::get<1>(opw->inputs()[0]);
  auto output = opw->outputs()[0];
  auto src_array = (Output*)nNet_GetArrayData(input);
  auto dst_array = (Output*)nNet_GetArrayData(output);

  result = 0;
  for (auto i = 0; i < nNet_GetTensorSize(input); ++i){
    result += (dst_array[i] = std::exp(src_array[i]));
  }
}

template<typename Input, typename Output>
void runReduceSumWithOpenMP(OperatorW* opw, Output& result){
 #if defined(_OPENMP)
  auto input  = std::get<1>(opw->inputs()[0]);
  auto output = opw->outputs()[0];
  auto src_array = (Output*)nNet_GetArrayData(input);
  auto dst_array = (Output*)nNet_GetArrayData(output);

  Output sumup = 0;
  #pragma omp parallel for reduction(+:sumup)
  for (auto i = 0; i < nNet_GetTensorSize(input); ++i){
    sumup += (dst_array[i] = std::exp(src_array[i]));
  }
  result = sumup;

 #else
  runReduceSumWithoutOpenMP<Input, Output>(opw, result);
 #endif
}
} // namespace internal

Operator Prepare(Operator op, Interpreter interpreter, CString model){
  try{
    auto attr = OperatorW::config(model, "Softmax");

    if (!op->device)
      nNet_GetInterpreterW(interpreter)->error(
                 BadLogic.reason("op \'Softmax' has been pluged like a macro"));
    else if (attr){
      auto softmax = nNet_CreateTensorC(interpreter, nullptr);
      auto opw     = new interpreter::OperatorW(op, 1, interpreter,
                                                op->device, attr);

      if (!softmax)
        return nNet_RemoveOperatorW(interpreter, opw->interface());

      opw->outputs().push_back(softmax);
      return opw->interface();
    } else
      nNet_GetInterpreterW(interpreter)->error(NotFound);
  } catch(base::Error& error){
    nNet_GetInterpreterW(interpreter)->error(rvalue(error));
  }
  return nullptr;
}

bool Check(Operator op, Interpreter interpreter){
  auto opw = nNet_GetOperatorW(op);

  if (opw->inputs().size() != 1){
    return opw << (BadLogic << "operator" << op->name << "require only 1 input"
                            << "but you provide"
                            << std::to_string(opw->inputs().size()));
  } else {
    auto output = opw->outputs()[0];
    auto type   = opw->get<NumT>("T");
    auto input  = std::get<1>(opw->inputs()[0]);
    auto keep   = false;

    /* @NOTE: softmax must require input and output types are real integers */
    if (input->type == Int32_t || input->type == Int64_t){
      return opw << NoSupport.reason("input->type != Int32_t and "
                                     "input->type != Int64_t");
    }
    if (type == Int32_t || type == Int64_t){
      return opw << NoSupport.reason("output->type != Int32_t and "
                                     "output->type != Int64_t");
    }

    /* @NOTE: softmax can't perform with String or Bool */
    if (input->type == String_t || input->type == Bool_t){
      return opw << NoSupport.reason("input->type != String_t and "
                                     "input->type != Bool_t");
    }
    if (type == String_t || type == Bool_t){
      return opw << NoSupport.reason("output->type != String_t and "
                                     "output->type != Bool_t");
    }

    if ((output->type == type) &&
        (input->ndims == output->ndims)){
      for (auto i = 0; !keep && i < input->ndims; ++i){
        if (output->dims[i] != input->dims[i])
          keep = true;
      }
    } else keep = true;

    if (keep){
      auto shape = nNet_CreateTensorShapeC2(interpreter, input->ndims, 
                                            input->dims);

      if (!nNet_SetTensorNumTW(interpreter, output, type))
        return opw << (BadAccess.reason("nNet_SetTensorNumTW"));
      if (!nNet_SetTensorShapeW(interpreter, output, shape))
        return opw << (BadAccess.reason("nNet_SetTensorShapeW"));

      if (nNet_RemoveTensorShapeW(interpreter, shape))
        return opw << (BadLogic.reason("nNet_RemoveTensorShape"));
    } else {
      if (!nNet_ResetTensor(output))
        return BadAccess.reason("nNet_ResetTensor()");
    }
    return NoError;
  }
}

bool Eval(Operator op, Interpreter interpreter){
  auto opw   = nNet_GetOperatorW(op);

  if (Check(op, interpreter))
    return true;
  else { 
    auto input  = std::get<1>(opw->inputs()[0]);
    auto output = opw->outputs()[0];

    auto src_array = nNet_TrimArrayData(input);
    auto dst_array = nNet_GetArrayData(output);

    base::Boundary bound{[](){}, [&](){ nNet_TryDropingArray(input, src_array); }};

    if (dst_array && src_array){
      switch (output->type){
      case Real32_t:{
        Real32 reduced;

        switch (input->type){
        case Real32_t:
          internal::runReduceSumWithOpenMP<Real32, Real32>(opw, reduced);
          return internal::runSoftmaxWithOpenMP<Real32, Real32>(
              nNet_GetTensorSize(input), opw, reduced);
          break;

        case Real64_t:
          internal::runReduceSumWithOpenMP<Real64, Real32>(opw, reduced);
          return internal::runSoftmaxWithOpenMP<Real64, Real32>(
              nNet_GetTensorSize(input), opw, reduced);
          break;
        }
        break;
      }

      case Real64_t:{
        Real64 reduced;

        switch (input->type){
        case Real32_t:
          internal::runReduceSumWithOpenMP<Real32, Real64>(opw, reduced);
          return internal::runSoftmaxWithOpenMP<Real32, Real64>(
              nNet_GetTensorSize(input), opw, reduced);
          break;

        case Real64_t:
          internal::runReduceSumWithOpenMP<Real64, Real64>(opw, reduced);
          return internal::runSoftmaxWithOpenMP<Real64>(
              nNet_GetTensorSize(input), opw, reduced);
          break;
        }
        break;
      }

      default:
        return opw << NoSupport;
      }
    }
    return NoError;
  }
}

bool Derive(Operator op, Interpreter interpreter){
  return false;
}
} // namespace softmax

Registration doRegisterSoftmax(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = softmax::Prepare;
  result.eval   = softmax::Eval;
  result.derive = softmax::Derive;
  return result;
}
} // namespace generic
} // namespace kernels
} // namespace nn
