#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
namespace kernels{
namespace generic{
namespace relu{
using namespace nn::interpreter;

namespace internal{
template<typename Input, typename Output>
bool runReluWithoutOpenMP(OperatorW* opw){
  auto input  = opw->inputs().begin()->second;
  auto output = opw->outputs()[0];

  auto src_array = (Input*)nNet_TrimArrayData(input);
  auto dst_array = (Output*)nNet_GetArrayData(output);

  for (auto i = 0; i < nNet_GetArraySizeT(input); ++i){
   dst_array[i] = std::max(src_array[i], Input(0));
  }

  if (!nNet_SetArrayData(opw->outputs()[0], dst_array))
    return opw << (BadAccess.reason("nNet_SetArrayData()"));
  return false;
}

template<typename Input, typename Output>
bool runReluWithOpenMP(OperatorW* opw){
 #if defined(_OPENMP)
  auto input  = opw->inputs().begin()->second;
  auto output = opw->outputs()[0];

  auto src_array = (Input*)nNet_TrimArrayData(input);
  auto dst_array = (Output*)nNet_GetArrayData(output);
  auto src_size  = nNet_GetArraySizeT(input);

  #pragma omp parallel for
  for (auto i = 0; i < src_size; ++i){
    dst_array[i] = std::max(src_array[i], Input(0));
  }

  if (!nNet_SetArrayData(opw->outputs()[0], dst_array))
    return opw << (BadAccess.reason("nNet_SetArrayData()"));
  return false;
 #else
  return runReluWithoutOpenMP<Input, Output>(opw);
 #endif
}
} // namespace internal

Operator Prepare(Operator op, Interpreter interpreter, CString model){
  try{
    auto attr = OperatorW::config(model, "Relu");

    if (!op->device)
      nNet_GetInterpreterW(interpreter)->error(
                    BadLogic.reason("op \'Relu' has been pluged like a macro"));
    else if (attr){
      auto relu = nNet_CreateTensorC(interpreter, nullptr);
      auto opw  = new interpreter::OperatorW(op, 1, interpreter, op->device, attr);

      if (!relu)
        return nNet_RemoveOperatorW(interpreter, opw->interface());
      opw->outputs().push_back(relu);
      return opw->interface();
    } else
      nNet_GetInterpreterW(interpreter)->error(NotFound);
  } catch(base::Error& error){
    nNet_GetInterpreterW(interpreter)->error(rvalue(error));
  }
  return nullptr;
}

bool Check(Operator op, Interpreter interpreter){
  auto opw    = nNet_GetOperatorW(op);
  auto output = opw->outputs()[0];
  auto type   = opw->get<NumT>("T");
  auto input  = std::get<1>(opw->inputs()[0]);
  auto keep   = false;

  if (opw->inputs().size() != 1){
    return opw << (BadLogic << "operator \'Relu\' require only 1 input"
                            << "but you have" 
                            << std::to_string(opw->inputs().size()));
  }

  switch(opw->inputs().begin()->second->type){
  case String_t:
  case Bool_t:
  case Unknown:
    return opw << (NoSupport);

  default:
    break;
  }

  if ((output->type == type) &&
      (input->ndims == output->ndims)){
    for (auto i = 0; !keep && i < input->ndims; ++i){
      if (output->dims[i] != input->dims[i])
        keep = true;
    }
  } else keep = true;

  if (keep){
    auto shape  = nNet_CreateTensorShapeC2(interpreter, input->ndims, 
                                           input->dims);

    if (!nNet_SetTensorNumTW(interpreter, output, type))
      return opw << (BadAccess.reason("nNet_SetTensorNumTW"));
    if (!nNet_SetTensorShapeW(interpreter, output, shape))
      return opw << (BadAccess.reason("nNet_SetTensorShapeW"));

    if (nNet_RemoveTensorShapeW(interpreter, shape))
      return opw << (BadLogic.reason("nNet_RemoveTensorShape"));
  } else {
    if (!nNet_ResetTensor(output))
      return BadAccess.reason("nNet_ResetTensor()");
  }

  return false;
}

bool Eval(Operator op, Interpreter interpreter){
  auto opw = nNet_GetOperatorW(op);

  if (Check(op, interpreter))
    return true;
  else{
    auto output = opw->outputs()[0];
    auto input  = std::get<1>(opw->inputs()[0]);

    switch(input->type){
    case Int32_t:
      switch(output->type){
      case Int32_t:
        return internal::runReluWithOpenMP<Int32, Int32>(opw);

      case Int64_t:
        return internal::runReluWithOpenMP<Int32, Int64>(opw);

      case Real32_t:
        return internal::runReluWithOpenMP<Int32, Real32>(opw);

      case Real64_t:
        return internal::runReluWithOpenMP<Int32, Real64>(opw);

      default:
        return opw << NoSupport;
      }

    case Int64_t:
      switch(output->type){
      case Int32_t:
        return internal::runReluWithOpenMP<Int64, Int32>(opw);

      case Int64_t:
        return internal::runReluWithOpenMP<Int64, Int64>(opw);

      case Real32_t:
        return internal::runReluWithOpenMP<Int64, Real32>(opw);

      case Real64_t:
        return internal::runReluWithOpenMP<Int64, Real64>(opw);

      default:
        return opw << NoSupport;
      }

    case Real32_t:
      switch(output->type){
      case Int32_t:
        return internal::runReluWithOpenMP<Real32, Int32>(opw);

      case Int64_t:
        return internal::runReluWithOpenMP<Real32, Int64>(opw);

      case Real32_t:
        return internal::runReluWithOpenMP<Real32, Real32>(opw);

      case Real64_t:
        return internal::runReluWithOpenMP<Real32, Real64>(opw);

      default:
        return opw << NoSupport;
      }

    case Real64_t:
      switch(output->type){
      case Int32_t:
        return internal::runReluWithOpenMP<Real64, Int32>(opw);

      case Int64_t:
        return internal::runReluWithOpenMP<Real64, Int64>(opw);

      case Real32_t:
        return internal::runReluWithOpenMP<Real64, Real32>(opw);

      case Real64_t:
        return internal::runReluWithOpenMP<Real64, Real64>(opw);

      default:
        return opw << NoSupport;
      }

    default:
      return opw << NoSupport;
    }
  }
  return NoError;
}

bool Derive(Operator op, Interpreter interpreter){
  return false;
}
} // namespace relu

Registration doRegisterRelu(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = relu::Prepare;
  result.eval   = relu::Eval;
  result.derive = relu::Derive;
  return result;
}
} // namespace generic
} // namespace kernels
} // namespace nn
