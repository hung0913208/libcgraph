#ifdef LIBCGRAPH_GIT_REFSPEC
#include "devices/generic/generic.h"
#include "devices/generic/wavio.h"

#include <libbase/stream.hpp>
#else
#include "generic.h"
#include "wavio.h"
#include "stream.hpp"
#endif

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
namespace kernels{
namespace generic{
namespace decode_wav{
using namespace nn::interpreter;
using namespace nn::generic::audio;

bool IsOkeyToCreateSignal(OperatorW* opw, WavIO<>& wavio){
  auto signal = opw->outputs()[0];

  if (nNet_GetTensorType(signal) != MatrixT)
    return true;
  else if (nNet_GetTensorNumT(signal) != utils::type(wavio.outputType()))
    return true;
  else if (nNet_GetArraySizeT(signal) != opw->get<int64_t>("desired_samples"))
    return true;
  return false;
}

bool IsOkeyToCreateRate(OperatorW* opw){
  auto rate = opw->outputs()[1];

  if (nNet_GetTensorType(rate) != ScalarT)
    return true;
  else if (nNet_GetTensorNumT(rate) != Int32_t)
    return true;
  return false;
}

namespace internal{
template<typename Type=float>
bool topdown(Operator op, Interpreter interpreter){
  using namespace base;
  WavIO<Type> wavio;

  CString path{nullptr};
  Shape   shape{nullptr};
  Error   error;

  auto opw    = nNet_GetOperatorW(op);
  auto input  = std::get<1>(opw->inputs()[0]);
  auto signal = opw->outputs()[0];
  auto rate   = opw->outputs()[1];
  auto array  = nNet_GetArrayData(input);

  base::Boundary bound{[](){}, [&](){ nNet_TryDropingArray(input, array); }};

  /* @NOTE: okey, now we will use WavIO to support reading wav from files */
  path = *((CString*)nNet_GetArrayItem(array, 0, String_t));

  if (!path)
    return opw << (BadLogic << "input mustn\'t be nullptr");
  else if ((error = wavio.open(path, std::ios::in)))
    return opw << error;

  if (IsOkeyToCreateSignal(opw, wavio)){
    int dims[2];

    /* @NOTE: prepare a matrix with heigh == wav's size*/
    dims[0] = wavio.header().numChannels;
    dims[1] = opw->get<int64_t>("desired_samples");

    shape = nNet_CreateTensorShapeC2(interpreter, 2, dims);
    if (!shape)
      opw << (DrainMem << "nNet_CreateTensorShapeC1()");

    /* @NOTE: okey allocate signal and rate */
    if (!nNet_SetTensorNumTW(interpreter, signal, utils::type(wavio.outputType())))
      return opw << (BadAccess << "nNet_SetTensorNumTW()");
    else if (!nNet_SetTensorShapeW(interpreter, signal, shape))
      return opw << (BadAccess << "nNet_SetTensorShapeW()");
    else if (!nNet_SetTensorNumTW(interpreter, rate, Int64_t))
      return opw << (BadAccess << "nNet_SetTensorNumT()");
    else if (nNet_RemoveTensorShapeW(interpreter, shape))
      return opw << (BadAccess << "nNet_RemoveTensorShapeW()");
  } else {
    if (!nNet_ResetTensor(signal))
      return opw << (BadAccess.reason("nNet_ResetTensor()"));
  }

  if (IsOkeyToCreateRate(opw)){
    if (nNet_GetTensorType(rate) != NilT)
      nNet_ClearTensorW(interpreter, rate);
    nNet_SetTensorNumTW(interpreter, rate, Int32_t);
  }

  {
    auto dst_array = (Type*)nNet_GetArrayData(signal);

    /* @NOTE: copy data from stream 'wavio' to tensor 'signal' */
    if (!dst_array)
      return opw << (BadAccess << "nNet_GetArrayData()");

    for (auto i = 0; i < nNet_GetArraySizeT(signal) &&
                     i < cast_(i, wavio.countSamples()); ++i){
      wavio >> dst_array[i];
    }

    if (!nNet_SetArrayData(signal, dst_array))
      return opw << BadAccess.reason("nNet_SetArrayData()");
    else {
      auto dst_array = nNet_GetArrayData(rate);

      /* @NOTE: set sample rate to tensor rate */
      if (!dst_array)
        return opw << (BadAccess << "nNet_GetArrayData()");
      else{
        nNet_SetArrayItem(dst_array, &wavio.header().sampleRate, 0, Int32_t);

        if (!nNet_SetArrayData(rate, dst_array))
          return opw << BadAccess.reason("nNet_SetArrayData()");
      }
    }

    return NoError;
  }
}
} // namespace internal

Operator Prepare(Operator op, Interpreter interpreter, CString model){
  try{
    auto attr = OperatorW::config(model, "DecodeWav");

    if (!op->device){
      nNet_GetInterpreterW(interpreter)->error(
              BadLogic.reason("op \'DecodeWav' has been pluged like a macro"));
    } else if (attr){
      /* @NOTE: install outputs of operator 'DecodeWav' */

      auto opw    = new interpreter::OperatorW(op, 3, interpreter, op->device,
                                               attr);
      auto shape  = nNet_CreateTensorShapeC1(interpreter, 1, 1);
      auto signal = nNet_CreateTensorC(interpreter, nullptr);
      auto rate   = nNet_CreateTensorC(interpreter, shape);

      if (!signal || !rate || !nNet_SetTensorNumTW(interpreter, rate, Int64_t)){
        signal = nNet_RemoveTensorW(interpreter, signal);
        rate   = nNet_RemoveTensorW(interpreter, rate);

        return nNet_RemoveOperatorW(interpreter, opw->interface());
      }

      /* @NOTE: prepare outputs */
      if (signal)
        opw->outputs().push_back(signal);

      if (rate)
        opw->outputs().push_back(rate);

      nNet_RemoveTensorShapeW(interpreter, shape);
      return opw->interface();
    } else
      nNet_GetInterpreterW(interpreter)->error(NotFound);
  } catch(base::Error& error){
    nNet_GetInterpreterW(interpreter)->error(rvalue(error));
  }
  return nullptr;
}

bool Check(Operator op, Interpreter UNUSED(interpreter)){
  using namespace interpreter;
  using namespace device::generic;

  auto opw    = nNet_GetOperatorW(op);
  auto device = utils::response<DeviceW, CPUManager>(op->device->_priv);

  if (!device)
    return opw << (NoSupport << op->name << "only perform on cpu");
  else {
    auto input = std::get<1>(opw->inputs()[0]);

    if (nNet_GetTensorNumT(input) != String_t){
      return opw << (NoSupport << "input of" << op->name << "must be String_t");
    }

    if (nNet_GetTensorType(input) >= VectorT){
      return opw << (NoSupport << op->name << "only support Scalar");
    }
    return NoError;
  }
}

bool Eval(Operator op, Interpreter interpreter){
  auto opw = nNet_GetOperatorW(op);

  try{
    if (Check(op, interpreter))
      return true;
    if (!strcmp(opw->get<char*>("method"), "TopDown"))
      return internal::topdown<>(op, interpreter);

    return opw << NoSupport;
  } catch(base::Error& error){ return opw << error; }
}
} // namespace decode_wav

Registration doRegisterDecodeWav(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = decode_wav::Prepare;
  result.eval = decode_wav::Eval;
  return result;
}
} // namespace generic
} // namespace kernels
} // namespace nn
