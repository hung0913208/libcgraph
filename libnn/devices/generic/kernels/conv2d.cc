#ifdef LIBCGRAPH_GIT_REFSPEC
#include "devices/generic/generic.h"
#include "devices/generic/math.h"
#else
#include "generic.h"
#include "math.h"
#endif

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

namespace nn {
namespace kernels {
namespace generic {
namespace conv2d {
using namespace nn::interpreter;

namespace internal {
namespace nhwc{
template<typename Input, typename Filter, typename Output>
CError runConv2dWithoutOpenMP(Operator op) {
  using namespace nn::generic::math;

  OperatorW* opw    = nNet_GetOperatorW(op);
  Tensor output = opw->outputs()[0];
  Tensor input  = std::get<1>(opw->inputs()[0]);
  Tensor filter = std::get<1>(opw->inputs()[1]);

  auto output_N  = nn::tensor::dimemsion(output, 0);
  auto output_H  = nn::tensor::dimemsion(output, 1);
  auto output_W  = nn::tensor::dimemsion(output, 2);
  auto output_C  = nn::tensor::dimemsion(output, 3);
  auto input_H   = nn::tensor::dimemsion(input, 1);
  auto input_W   = nn::tensor::dimemsion(input, 2);
  auto input_C   = nn::tensor::dimemsion(input, 3);
  auto filter_H  = nn::tensor::dimemsion(filter, 0);
  auto filter_W  = nn::tensor::dimemsion(filter, 1);
  auto filter_iC = nn::tensor::dimemsion(filter, 2);
  auto filter_oC = nn::tensor::dimemsion(filter, 3);

  auto strides_W = opw->get<int64_t>("strides.W");
  auto strides_H = opw->get<int64_t>("strides.H");
  auto dilate_W  = opw->get<int64_t>("dilations.W");
  auto dilate_H  = opw->get<int64_t>("dilations.H");

  auto in_begin  = input_W * input_H * input_C;
  auto out_begin = output_W * output_H * output_C;

  auto padding_W = computePadding(strides_W, dilate_W, input_W, filter_W, output_W);
  auto padding_H = computePadding(strides_H, dilate_H, input_H, filter_H, output_H);

  auto fblock_N = filter_iC * filter_oC;
  auto shift_W  = input_C;
  auto shift_H  = input_C * input_W;

  auto stride_H   = opw->get<int64_t>("strides.H");
  auto stride_W   = opw->get<int64_t>("strides.W");
  auto dilation_H = opw->get<int64_t>("dilations.H");
  auto dilation_W = opw->get<int64_t>("dilations.W");

  auto output_array = (Output*)nNet_GetArrayData(output);
  auto input_array  = (Input*)nNet_GetArrayData(input);
  auto filter_array = (Filter*)nNet_GetArrayData(filter);

  base::Boundary bound{[](){}, [&](){
    nNet_TryDropingArray(input, input_array);
    nNet_TryDropingArray(filter, filter_array);
  }};

  for (auto out_H = 0; out_H < output_H; ++out_H) {
    auto hstart = (out_H * stride_H - padding_H) * shift_H;
    auto hend   = (out_H * stride_H + filter_H - padding_H) * shift_H;

    for (auto out_W = 0; out_W < output_W; ++out_W) {
      auto wstart = (out_W * stride_W - padding_W) * shift_W;
      auto wend   = (out_W * stride_W + filter_W - padding_W) * shift_W;
      auto idx    = (out_H*output_W + out_W)*output_C;

      auto origin_in_H = out_H * stride_H - padding_H;
      auto origin_in_W = out_W * stride_W - padding_W;

      for (int x = 0, h = hstart; h < hend; ++x, h += dilation_H * shift_H) {
        auto in_X = x * dilation_H + origin_in_H;

        for (int y = 0, w = wstart; w < wend; ++y, w += dilation_W * shift_W) {
          auto in_Y = y * dilation_W + origin_in_W;
          auto filt = (x * filter_W + y) * fblock_N;

          if (in_X >= 0 && in_Y >= 0 && in_X < input_H && in_Y < input_W) {
            for (auto n = 0; n < output_N; ++n) {
              auto ifixed  = n*in_begin;
              auto ofixed  = n*out_begin;
              auto outputs = &output_array[ofixed + idx];
              auto inputs  = &input_array[ifixed + h + w];

            for (int ic = 0; ic < filter_iC; ++ic) {
                auto ffixed_gen = ic * filter_oC;
                auto filters    = &filter_array[filt + ffixed_gen];
                auto in         = inputs[ic];

                for (int oc = 0; oc < output_C; ++oc) {
                  outputs[oc] += in*filters[oc];
                }
              }
            }
          }
        }
      }
    }
  }

  for (auto i = 0; i < nNet_GetTensorSize(output); ++i){
    output_array[i] = std::min(std::max(float(output_array[i]), 
                                        std::numeric_limits<float>::lowest()), 
                               std::numeric_limits<float>::max());
  }
  if (!nNet_SetArrayData(output, output_array))
    return opw << BadAccess.reason("nNet_SetArrayData()");
  return 0; 
}

template<typename Input, typename Filter, typename Output>
CError runConv2dWithOpenMP(Operator op) {
  using namespace nn::generic::math;

 #if defined(_OPENMP)
  auto opw    = nNet_GetOperatorW(op);
  auto output = opw->outputs()[0];
  auto input  = std::get<1>(opw->inputs()[0]);
  auto filter = std::get<1>(opw->inputs()[1]);

  auto output_N  = nn::tensor::dimemsion(output, 0);
  auto output_H  = nn::tensor::dimemsion(output, 1);
  auto output_W  = nn::tensor::dimemsion(output, 2);
  auto output_C  = nn::tensor::dimemsion(output, 3);
  auto input_H   = nn::tensor::dimemsion(input, 1);
  auto input_W   = nn::tensor::dimemsion(input, 2);
  auto input_C   = nn::tensor::dimemsion(input, 3);
  auto filter_H  = nn::tensor::dimemsion(filter, 0);
  auto filter_W  = nn::tensor::dimemsion(filter, 1);
  auto filter_iC = nn::tensor::dimemsion(filter, 2);
  auto filter_oC = nn::tensor::dimemsion(filter, 3);

  auto strides_W = opw->get<int64_t>("strides.W");
  auto strides_H = opw->get<int64_t>("strides.H");
  auto dilate_W  = opw->get<int64_t>("dilations.W");
  auto dilate_H  = opw->get<int64_t>("dilations.H");

  auto in_begin  = input_W * input_H * input_C;
  auto out_begin = output_W * output_H * output_C;

  auto padding_W = computePadding(strides_W, dilate_W, input_W, filter_W, output_W);
  auto padding_H = computePadding(strides_H, dilate_H, input_H, filter_H, output_H);

  auto shift_W = input_C;
  auto shift_H = input_C * input_W;

  auto stride_H   = opw->get<int64_t>("strides.H");
  auto stride_W   = opw->get<int64_t>("strides.W");
  auto dilation_H = opw->get<int64_t>("dilations.H");
  auto dilation_W = opw->get<int64_t>("dilations.W");

  auto output_array = (Output*)nNet_GetArrayData(output);
  auto input_array  = (Input*)nNet_GetArrayData(input);
  auto filter_array = (Filter*)nNet_GetArrayData(filter);

  base::Boundary bound{[](){}, [&](){
    nNet_TryDropingArray(input, input_array);
    nNet_TryDropingArray(filter, filter_array);
  }};

  omp_set_nested(1);

  #pragma opm parallel for collapse(2)
  for (auto out_H = 0; out_H < output_H; ++out_H) {
    for (auto out_W = 0; out_W < output_W; ++out_W) {
      auto begin  = input_W * input_H * input_C;
      auto hstart = (out_H * stride_H - padding_H) * shift_H;
      auto hend   = (out_H * stride_H + filter_H - padding_H) * shift_H;
      auto wstart = (out_W * stride_W - padding_W) * shift_W;
      auto wend   = (out_W * stride_W + filter_W - padding_W) * shift_W;
      auto idx    = (out_H*output_W + out_W)*output_C;

      auto fblock_N = filter_iC * filter_oC;
      auto origin_in_H = out_H * stride_H - padding_H;
      auto origin_in_W = out_W * stride_W - padding_W;

      #pragma opm parallel for nowait collapse(2)
      for (int x = 0, h = hstart; h < hend; ++x, h += dilation_H * shift_H) {
        for (int y = 0, w = wstart; w < wend; ++y, w += dilation_W * shift_W) {
          auto in_X = x * dilation_H + origin_in_H;
          auto in_Y = y * dilation_W + origin_in_W;
          auto filt = (x * filter_W + y) * fblock_N;

          if (in_X >= 0 && in_Y >= 0 && in_X < input_H && in_Y < input_W) {
            for (auto n = 0; n < output_N; ++n) {
              auto ifixed  = n*in_begin;
              auto ofixed  = n*out_begin;
              auto inputs  = &input_array[n*begin + h + w];
              auto outputs = &output_array[ofixed + idx];

              #pragma simd
              for (int ic = 0; ic < filter_iC; ++ic) {
                auto ffixed  = ic*filter_oC;
                auto filters = &filter_array[filt + ffixed];
                auto in      = inputs[ic];

                #pragma simd
                for (int oc = 0; oc < output_C; ++oc) {
                  outputs[oc] += in * filters[oc];
                }
              }
            }
          }
        }
      }
    }
  }

  #pragma opm parallel for
  for (auto i = 0; i < nNet_GetTensorSize(output); ++i){
    output_array[i] = std::min(std::max(float(output_array[i]), 
                                        std::numeric_limits<float>::lowest()), 
                               std::numeric_limits<float>::max());
  }
  if (!nNet_SetArrayData(output, output_array))
    return opw << BadAccess.reason("nNet_SetArrayData()");
  return 0;
 #else
  return runConv2dWithoutOpenMP<Input, Filter, Output>(op);
 #endif
}
} // namespace nhwc

namespace nchw{
template<typename Input, typename Filter, typename Output>
CError runConv2dWithoutOpenMP(Operator op){
  using namespace nn::generic::math;

  auto opw    = nNet_GetOperatorW(op);
  auto output = opw->outputs()[0];
  auto input  = std::get<1>(opw->inputs()[0]);
  auto filter = std::get<1>(opw->inputs()[1]);

  auto output_N  = nn::tensor::dimemsion(output, 0);
  auto output_C  = nn::tensor::dimemsion(output, 1);
  auto output_H  = nn::tensor::dimemsion(output, 2);
  auto output_W  = nn::tensor::dimemsion(output, 3);
  auto input_C   = nn::tensor::dimemsion(input, 1);
  auto input_H   = nn::tensor::dimemsion(input, 2);
  auto input_W   = nn::tensor::dimemsion(input, 3);
  auto filter_H  = nn::tensor::dimemsion(filter, 0);
  auto filter_W  = nn::tensor::dimemsion(filter, 1);
  auto filter_iC = nn::tensor::dimemsion(filter, 2);
  auto filter_oC = nn::tensor::dimemsion(filter, 3);

  auto strides_W = opw->get<int64_t>("strides.W");
  auto strides_H = opw->get<int64_t>("strides.H");
  auto dilate_W  = opw->get<int64_t>("dilations.W");
  auto dilate_H  = opw->get<int64_t>("dilations.H");

  auto padding_W = computePadding(strides_W, dilate_W, input_W, filter_W, output_W);
  auto padding_H = computePadding(strides_H, dilate_H, input_H, filter_H, output_H);

  auto stride_H = opw->get<int64_t>("strides.H");
  auto stride_W = opw->get<int64_t>("strides.W");
  auto dilation_H = opw->get<int64_t>("dilations.H");
  auto dilation_W = opw->get<int64_t>("dilations.W");
  
  auto output_array = (Output*)nNet_GetArrayData(output);
  auto input_array  = (Input*)nNet_GetArrayData(input);
  auto filter_array = (Filter*)nNet_GetArrayData(filter);

  auto max_with_output_C = output_H*output_W*output_C;
  auto shift_oC  = output_H*output_W;
  auto in_shift  = input_W * input_H * input_C;
  auto out_shift = output_W * output_H * output_C;

  base::Boundary bound{[]() {},[&]() {
    nNet_TryDropingArray(input, input_array);
    nNet_TryDropingArray(filter, filter_array);
  }};

  for (auto out_H = 0; out_H < output_H; ++out_H) {
    for (auto out_W = 0; out_W < output_W; ++out_W) {
      auto fblock_N    = filter_iC * filter_oC;
      auto origin_in_H = out_H * stride_H - padding_H;
      auto origin_in_W = out_W * stride_W - padding_W;

      for (int i = 0; i < filter_H; i++) {
        for (int j = 0; j < filter_W; j++) {
          auto in_X = j * dilation_W + origin_in_W;
          auto in_Y = i * dilation_H + origin_in_H;
          auto filt = (i * filter_W + j) * fblock_N;

          if (in_X >= 0 && in_X < input_W && in_Y >= 0 && in_Y < input_H) {
            for (int c = 0; c < filter_iC; c++) {
              auto ffixed   = c*filter_oC;
              auto cbegin   = c*input_W*input_H;
              auto oblock_N = 0;
              auto iblock_N = 0;

              for (auto out_N = 0; out_N < output_N; 
                   ++out_N, oblock_N += out_shift, iblock_N += in_shift) {
                auto ofixed = out_W + out_H*output_W;
                auto in     = input_array[iblock_N + cbegin + in_Y*input_W + in_X];

                for (auto ext_C = 0, out_C = 0; ext_C < max_with_output_C; 
                                     ext_C += shift_oC, ++out_C) {
                  output_array[ofixed + oblock_N + ext_C] += 
                    in*filter_array[filt + ffixed + out_C];
                }
              }
            }
          }
        }
      }
    }
  }

  for (auto i = 0; i < nNet_GetTensorSize(output); ++i){
    output_array[i] = std::min(std::max(float(output_array[i]), 
                                        std::numeric_limits<float>::lowest()), 
                               std::numeric_limits<float>::max());
  }
  return false;
}

template<typename Input, typename Filter, typename Output>
CError runConv2dWithOpenMP(Operator op){
  using namespace nn::generic::math;

 #if defined(_OPENMP)
  auto opw    = nNet_GetOperatorW(op);
  auto output = opw->outputs()[0];
  auto input  = std::get<1>(opw->inputs()[0]);
  auto filter = std::get<1>(opw->inputs()[1]);

  auto output_N  = nn::tensor::dimemsion(output, 0);
  auto output_C  = nn::tensor::dimemsion(output, 1);
  auto output_H  = nn::tensor::dimemsion(output, 2);
  auto output_W  = nn::tensor::dimemsion(output, 3);
  auto input_C   = nn::tensor::dimemsion(input, 1);
  auto input_H   = nn::tensor::dimemsion(input, 2);
  auto input_W   = nn::tensor::dimemsion(input, 3);
  auto filter_H  = nn::tensor::dimemsion(filter, 0);
  auto filter_W  = nn::tensor::dimemsion(filter, 1);
  auto filter_iC = nn::tensor::dimemsion(filter, 2);
  auto filter_oC = nn::tensor::dimemsion(filter, 3);

  auto strides_W = opw->get<int64_t>("strides.W");
  auto strides_H = opw->get<int64_t>("strides.H");
  auto dilate_W  = opw->get<int64_t>("dilations.W");
  auto dilate_H  = opw->get<int64_t>("dilations.H");

  auto padding_W = computePadding(strides_W, dilate_W, input_W, filter_W, output_W);
  auto padding_H = computePadding(strides_H, dilate_H, input_H, filter_H, output_H);

  auto stride_H = opw->get<int64_t>("strides.H");
  auto stride_W = opw->get<int64_t>("strides.W");
  auto dilation_H = opw->get<int64_t>("dilations.H");
  auto dilation_W = opw->get<int64_t>("dilations.W");
  
  auto output_array = (Output*)nNet_GetArrayData(output);
  auto input_array  = (Input*)nNet_GetArrayData(input);
  auto filter_array = (Filter*)nNet_GetArrayData(filter);

  auto max_with_output_C = output_H*output_W*output_C;
  auto shift_oC  = output_H*output_W;
  auto in_shift  = input_W * input_H * input_C;
  auto out_shift = output_W * output_H * output_C;

  base::Boundary bound{[]() {},[&]() {
    nNet_TryDropingArray(input, input_array);
    nNet_TryDropingArray(filter, filter_array);
  }};

  omp_set_nested(1);

  #pragma opm parallel for collapse(2)
  for (auto out_H = 0; out_H < output_H; ++out_H) {
    for (auto out_W = 0; out_W < output_W; ++out_W) {
      auto fblock_N    = filter_iC * filter_oC;
      auto origin_in_H = out_H * stride_H - padding_H;
      auto origin_in_W = out_W * stride_W - padding_W;

      #pragma opm parallel for nowait collapse(2)
      for (int i = 0; i < filter_H; i++) {
        for (int j = 0; j < filter_W; j++) {
          auto in_X = j * dilation_W + origin_in_W;
          auto in_Y = i * dilation_H + origin_in_H;
          auto filt = (i * filter_W + j) * fblock_N;

          if (in_X >= 0 && in_X < input_W && in_Y >= 0 && in_Y < input_H) {
            for (int c = 0; c < filter_iC; c++) {
              auto ffixed   = c*filter_oC;
              auto cbegin   = c*input_W*input_H;
              auto oblock_N = 0;
              auto iblock_N = 0;

              #pragma simd
              for (auto out_N = 0; out_N < output_N; 
                   ++out_N, oblock_N += out_shift, iblock_N += in_shift) {
                auto ofixed = out_W + out_H*output_W;
                auto in     = input_array[iblock_N + cbegin + in_Y*input_W + in_X];

                #pragma simd
                for (auto ext_C = 0, out_C = 0; ext_C < max_with_output_C; 
                                     ext_C += shift_oC, ++out_C) {
                  output_array[ofixed + oblock_N + ext_C] += 
                    in*filter_array[filt + ffixed + out_C];
                }
              }
            }
          }
        }
      }
    }
  }

  #pragma opm parallel for
  for (auto i = 0; i < nNet_GetTensorSize(output); ++i){
    output_array[i] = std::min(std::max(float(output_array[i]), 
                                        std::numeric_limits<float>::lowest()), 
                               std::numeric_limits<float>::max());
  }

  return false;
 #else
  return runConv2dWithoutOpenMP<Input, Filter, Output>(op);
 #endif
}
} // namesapce nchw
} // namespace internal

bool IsOkeyToCreateOutput(OperatorW* opw, 
                          std::string&& data_format, std::string&& padding,
                          int strides_W, int strides_H,
                          int dilation_W, int dilation_H){
  auto input  = std::get<1>(opw->inputs()[0]);
  auto filter = std::get<1>(opw->inputs()[1]);
  auto output = opw->outputs()[0];

  auto filter_H  = nn::tensor::dimemsion(filter, 0);
  auto filter_W  = nn::tensor::dimemsion(filter, 1);
  auto filter_oC = nn::tensor::dimemsion(filter, 3);
  auto fixed     = 1;

  if (nNet_GetTensorType(output) != TensorT)
    return true;
  else if (output->type != opw->get<NumT>("T"))
    return true;
  else if (output->ndims != 4)
    return true;
  else if (data_format == "NHWC"){
    auto input_N = nn::tensor::dimemsion(input, 0);
    auto input_H = nn::tensor::dimemsion(input, 1);
    auto input_W = nn::tensor::dimemsion(input, 2);

    auto effective_filter_W = (filter_W - 1) * dilation_W;
    auto effective_filter_H = (filter_H - 1) * dilation_H;

    if (padding == "SAME") fixed = 0;
    if (output->dims[0] != filter_oC)
      return true;
    if (output->dims[1] != (input_W - fixed*effective_filter_H + strides_H - 1) / strides_W)
      return true;
    if (output->dims[2] != (input_H - fixed*effective_filter_W + strides_W - 1) / strides_H)
      return true;
    if (output->dims[3] != input_N)
      return true;
  } else if (data_format == "NCHW"){
    auto input_N = nn::tensor::dimemsion(input, 0);
    auto input_H = nn::tensor::dimemsion(input, 2);
    auto input_W = nn::tensor::dimemsion(input, 3);

    auto effective_filter_W = (filter_W - 1) * dilation_W;
    auto effective_filter_H = (filter_H - 1) * dilation_H;

    if (padding == "SAME") fixed = 0;
    if (output->dims[0] != (input_W - fixed*effective_filter_W + strides_W - 1)/strides_W)
      return true;
    if (output->dims[1] != (input_H - fixed*effective_filter_H + strides_W - 1)/strides_H)
      return true;
    if (output->dims[2] != filter_oC)
      return true;
    if (output->dims[3] != input_N)
      return true;
  }
  return false;
}

Operator Prepare(Operator op, Interpreter interpreter, CString model) {
  try {
    auto attr = OperatorW::config(model, "Conv2D");

    if (!op->device)
      nNet_GetInterpreterW(interpreter)->error(
                  BadLogic.reason("op \'Conv2D' has been pluged like a macro"));
    else if (attr) {
      auto conv2d = nNet_CreateTensorC(interpreter, nullptr);
      auto opw    = new interpreter::OperatorW(op, COUNT_CONV2D_ARGUMENTS,
                                               interpreter, op->device, attr);

      if (!conv2d)
        return nNet_RemoveOperatorW(interpreter, opw->interface());
      opw->outputs().push_back(conv2d);
      return opw->interface();
    } else
      nNet_GetInterpreterW(interpreter)->error(NotFound);
  } catch (base::Error& error) {
    nNet_GetInterpreterW(interpreter)->error(rvalue(error));
  }
  return nullptr;
}

bool Check(Operator op, Interpreter UNUSED(interpreter)) {
  using Int64Vector = std::vector<int64_t>;

  auto opw    = nNet_GetOperatorW(op);
  auto input  = Tensor{nullptr};
  auto filter = Tensor{nullptr};
  auto output = opw->outputs()[0];

  /* @NOTE: Conv2d requires exactly only 2 inputs */
  if (opw->inputs().size() != 2) {
    return opw << (BadLogic << "operator" << op->name << "require 2 input,"
                            << "but you provide"
                            << std::to_string(opw->inputs().size()));
  } else {
    input  = std::get<1>(opw->inputs()[0]);
    filter = std::get<1>(opw->inputs()[1]);
  }

  /* @NOTE: check input dimension */
  if (!input || input->ndims != 4) {
    return opw << (NoSupport << "input of operator" << op->name
                             << "must be a 4-D tensor");
  } else if (!filter || filter->ndims != 4) {
    return opw << (NoSupport << "filter of operator" << op->name
                             << "must be a 4-D tensor");
  } else {
    auto type      = opw->get<NumT>("T");
    auto strides   = opw->get<Int64Vector>("strides");
    auto dilations = opw->get<Int64Vector>("dilations");
    auto format    = std::string{opw->get<char*>("data_format")};
    auto padding   = std::string{opw->get<char*>("padding")};

    if (type == String_t || type == Bool_t)
      return opw << (NoSupport << "T == String_t or T == Bool_t");
    else if (strides.size() != 4) {
      return opw << (BadLogic << "MaxPool require strides contain 4 values, but"
                              << "you provided" << std::to_string(strides.size())
                              << "values");
    } else if (filter->ndims != 4) {
      return opw << (BadLogic << op->name << "require strides contain 4 values,"
                              << "but you provided" 
                              << std::to_string(strides.size()) << "values");
    } else {
      auto strides_N = strides[format.find('N')];
      auto strides_H = strides[format.find('H')];
      auto strides_W = strides[format.find('W')];
      auto strides_C = strides[format.find('C')];

      auto dilation_N = dilations[format.find('N')];
      auto dilation_H = dilations[format.find('H')];
      auto dilation_W = dilations[format.find('W')];
      auto dilation_C = dilations[format.find('C')];

      auto filter_H  = nn::tensor::dimemsion(filter, 0);
      auto filter_W  = nn::tensor::dimemsion(filter, 1);
      auto filter_iC = nn::tensor::dimemsion(filter, 2);
      auto filter_oC = nn::tensor::dimemsion(filter, 3);

      int input_H, input_W, input_N, input_C;
      int fixed = 1;

      Shape shape;

      /* @NOTE: config strides attribute */
      if (strides_N != 1) {
        opw << (NoSupport << "strides.N{" << std::to_string(strides_N)
                          << "} != 1");
      } else if (strides_C != 1) {
        opw << (NoSupport << "strides.C{" << std::to_string(strides_C)
                          << "} != 1");
      } else {
        opw->set("strides.N", strides_N);
        opw->set("strides.H", strides_H);
        opw->set("strides.W", strides_W);
        opw->set("strides.C", strides_C);
      }

      /* @NOTE: config dilations attribute */
      if (strides_N != 1) {
        opw << (NoSupport << "dilations.N{" << std::to_string(dilation_N)
                          << "} != 1");
      } else if (strides_C != 1) {
        opw << (NoSupport << "dilations.C{" << std::to_string(dilation_C)
                          << "} != 1");
      } else {
        opw->set("dilations.N", dilation_N);
        opw->set("dilations.H", dilation_H);
        opw->set("dilations.W", dilation_W);
        opw->set("dilations.C", dilation_C);
      }

      /* @NOTE: prepare output */
      if (!strcmp(opw->get<char*>("data_format"), "NHWC")) {
        input_N = nn::tensor::dimemsion(input, 0);
        input_H = nn::tensor::dimemsion(input, 1);
        input_W = nn::tensor::dimemsion(input, 2);
        input_C = nn::tensor::dimemsion(input, 3);
      } else if (!strcmp(opw->get<char*>("data_format"), "NCHW")) {
        input_N = nn::tensor::dimemsion(input, 0);
        input_C = nn::tensor::dimemsion(input, 1);
        input_H = nn::tensor::dimemsion(input, 2);
        input_W = nn::tensor::dimemsion(input, 3);
      } else {
        return opw << (NoSupport << "operator" << op->name
                                 << "only support format \'NHWC\' or \'NCHW\'");
      }

      if (padding == "SAME")
        fixed = 0;
      else if (padding != "VALID"){
        return opw << (NoSupport << "attribute \'padding\' only support 2 options"
                                 << "\'VALID\' or \'SAME\' but you provided"
                                 << padding);
      }

      if (input_C != filter_iC) {
        opw << (NoSupport << "filter.in_C{" << std::to_string(filter_iC)
                          << "} != input.C{" << std::to_string(input_H)
                          << "}");
      }

      if (*opw) return *opw;

      /* @NOTE: we must be carefull about the differnet between tensor dimension
       * and our dimension */

      if (IsOkeyToCreateOutput(opw, opw->get<char*>("data_format"), rvalue(padding), 
                               strides_W, strides_H, dilation_W, dilation_H)){
        if (!strcmp(opw->get<char*>("data_format"), "NHWC")) {
          int  dims[4];

          auto effective_filter_W = (filter_W - 1) * dilation_W;
          auto effective_filter_H = (filter_H - 1) * dilation_H;

          dims[0] = filter_oC;
          dims[1] = (input_W - fixed*effective_filter_W + strides_W - 1)/strides_W;
          dims[2] = (input_H - fixed*effective_filter_H + strides_H - 1)/strides_H;
          dims[3] = input_N;

          shape = nNet_CreateTensorShapeC2(interpreter, 4, dims);
        } else {
          int  dims[4];

          auto effective_filter_W = (filter_W - 1) * dilation_W;
          auto effective_filter_H = (filter_H - 1) * dilation_H;

          dims[0] = (input_W - fixed*effective_filter_W + strides_W - 1)/strides_W;
          dims[1] = (input_H - fixed*effective_filter_H + strides_H - 1)/strides_H,
          dims[2] = filter_oC;
          dims[3] = input_N;

          shape = nNet_CreateTensorShapeC2(interpreter, 4, dims);
        }

        if (!shape)
          return opw << DrainMem.reason("nNet_CreateTensorShapeC1()");

        if (!nNet_SetTensorNumTW(interpreter, output, type))
          return opw << BadAccess.reason("nNet_SetTensorNumTW()");
        else if (!nNet_SetTensorShapeW(interpreter, output, shape))
          return opw << BadAccess.reason("nNet_SetTensorShapeW()");
        nNet_RemoveTensorShapeW(interpreter, shape);
       #if USE_VECTORIZE
        if (!nNet_ResetTensor(output))
          return BadAccess.reason("nNet_ResetTensor()");
       #endif
      } else {
        if (!nNet_ResetTensor(output))
          return BadAccess.reason("nNet_ResetTensor()");
      }

      return false;
    }
  }
}

bool Eval(Operator op, Interpreter interpreter) {
  auto opw = nNet_GetOperatorW(op);

  if (Check(op, interpreter))
    return true;

  if (!strcmp(opw->get<char*>("data_format"), "NHWC")){
    auto opw    = nNet_GetOperatorW(op);
    auto output = opw->outputs()[0];
    auto input  = std::get<1>(opw->inputs()[0]);
    auto filter = std::get<1>(opw->inputs()[1]);

    switch(input->type){
    case Int32_t:
      switch(filter->type){
      case Int32_t:
        switch(output->type){
        case Int32_t:
          return internal::nhwc::runConv2dWithOpenMP<Int32, Int32, Int32>(op);

        case Int64_t:
          return internal::nhwc::runConv2dWithOpenMP<Int32, Int32, Int64>(op);

        case Real32_t:
          return internal::nhwc::runConv2dWithOpenMP<Int32, Int32, Real32>(op);

        case Real64_t:
          return internal::nhwc::runConv2dWithOpenMP<Int32, Int32, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Int64_t:
        switch(output->type){
        case Int32_t:
          return internal::nhwc::runConv2dWithOpenMP<Int32, Int64, Int32>(op);

        case Int64_t:
          return internal::nhwc::runConv2dWithOpenMP<Int32, Int64, Int64>(op);

        case Real32_t:
          return internal::nhwc::runConv2dWithOpenMP<Int32, Int64, Real32>(op);

        case Real64_t:
          return internal::nhwc::runConv2dWithOpenMP<Int32, Int64, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Real32_t:
        switch(output->type){
        case Int32_t:
          return internal::nhwc::runConv2dWithOpenMP<Int32, Real32, Int32>(op);

        case Int64_t:
          return internal::nhwc::runConv2dWithOpenMP<Int32, Real32, Int64>(op);

        case Real32_t:
          return internal::nhwc::runConv2dWithOpenMP<Int32, Real32, Real32>(op);

        case Real64_t:
          return internal::nhwc::runConv2dWithOpenMP<Int32, Real32, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Real64_t:
        switch(output->type){
        case Int32_t:
          return internal::nhwc::runConv2dWithOpenMP<Int32, Real64, Int32>(op);

        case Int64_t:
          return internal::nhwc::runConv2dWithOpenMP<Int32, Real64, Int64>(op);

        case Real32_t:
          return internal::nhwc::runConv2dWithOpenMP<Int32, Real64, Real32>(op);

        case Real64_t:
          return internal::nhwc::runConv2dWithOpenMP<Int32, Real64, Real64>(op);

        default:
          return opw << NoSupport;
        }

      default:
        break;
      }
      return opw << NoSupport;

    case Int64_t:
      switch(filter->type){
      case Int32_t:
        switch(output->type){
        case Int32_t:
          return internal::nhwc::runConv2dWithOpenMP<Int64, Int32, Int32>(op);

        case Int64_t:
          return internal::nhwc::runConv2dWithOpenMP<Int64, Int32, Int64>(op);

        case Real32_t:
          return internal::nhwc::runConv2dWithOpenMP<Int64, Int32, Real32>(op);

        case Real64_t:
          return internal::nhwc::runConv2dWithOpenMP<Int64, Int32, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Int64_t:
        switch(output->type){
        case Int32_t:
          return internal::nhwc::runConv2dWithOpenMP<Int64, Int64, Int32>(op);

        case Int64_t:
          return internal::nhwc::runConv2dWithOpenMP<Int64, Int64, Int64>(op);

        case Real32_t:
          return internal::nhwc::runConv2dWithOpenMP<Int64, Int64, Real32>(op);

        case Real64_t:
          return internal::nhwc::runConv2dWithOpenMP<Int64, Int64, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Real32_t:
        switch(output->type){
        case Int32_t:
          return internal::nhwc::runConv2dWithOpenMP<Int64, Real32, Int32>(op);

        case Int64_t:
          return internal::nhwc::runConv2dWithOpenMP<Int64, Real32, Int64>(op);

        case Real32_t:
          return internal::nhwc::runConv2dWithOpenMP<Int64, Real32, Real32>(op);

        case Real64_t:
          return internal::nhwc::runConv2dWithOpenMP<Int64, Real32, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Real64_t:
        switch(output->type){
        case Int32_t:
          return internal::nhwc::runConv2dWithOpenMP<Int64, Real64, Int32>(op);

        case Int64_t:
          return internal::nhwc::runConv2dWithOpenMP<Int64, Real64, Int64>(op);

        case Real32_t:
          return internal::nhwc::runConv2dWithOpenMP<Int64, Real64, Real32>(op);

        case Real64_t:
          return internal::nhwc::runConv2dWithOpenMP<Int64, Real64, Real64>(op);

        default:
          return opw << NoSupport;
        }

      default:
        break;
      }
      return opw << NoSupport;

    case Real32_t:
      switch(filter->type){
      case Int32_t:
        switch(output->type){
        case Int32_t:
          return internal::nhwc::runConv2dWithOpenMP<Real32, Int32, Int32>(op);

        case Int64_t:
          return internal::nhwc::runConv2dWithOpenMP<Real32, Int32, Int64>(op);

        case Real32_t:
          return internal::nhwc::runConv2dWithOpenMP<Real32, Int32, Real32>(op);

        case Real64_t:
          return internal::nhwc::runConv2dWithOpenMP<Real32, Int32, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Int64_t:
        switch(output->type){
        case Int32_t:
          return internal::nhwc::runConv2dWithOpenMP<Real32, Int64, Int32>(op);

        case Int64_t:
          return internal::nhwc::runConv2dWithOpenMP<Real32, Int64, Int64>(op);

        case Real32_t:
          return internal::nhwc::runConv2dWithOpenMP<Real32, Int64, Real32>(op);

        case Real64_t:
          return internal::nhwc::runConv2dWithOpenMP<Real32, Int64, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Real32_t:
        switch(output->type){
        case Int32_t:
          return internal::nhwc::runConv2dWithOpenMP<Real32, Real32, Int32>(op);

        case Int64_t:
          return internal::nhwc::runConv2dWithOpenMP<Real32, Real32, Int64>(op);

        case Real32_t:
          return internal::nhwc::runConv2dWithOpenMP<Real32, Real32, Real32>(op);

        case Real64_t:
          return internal::nhwc::runConv2dWithOpenMP<Real32, Real32, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Real64_t:
        switch(output->type){
        case Int32_t:
          return internal::nhwc::runConv2dWithOpenMP<Real32, Real64, Int32>(op);

        case Int64_t:
          return internal::nhwc::runConv2dWithOpenMP<Real32, Real64, Int64>(op);

        case Real32_t:
          return internal::nhwc::runConv2dWithOpenMP<Real32, Real64, Real32>(op);

        case Real64_t:
          return internal::nhwc::runConv2dWithOpenMP<Real32, Real64, Real64>(op);

        default:
          return opw << NoSupport;
        }

      default:
        break;
      }
      return opw << NoSupport;

    case Real64_t:
      switch(filter->type){
      case Int32_t:
        switch(output->type){
        case Int32_t:
          return internal::nhwc::runConv2dWithOpenMP<Real64, Int32, Int32>(op);

        case Int64_t:
          return internal::nhwc::runConv2dWithOpenMP<Real64, Int32, Int64>(op);

        case Real32_t:
          return internal::nhwc::runConv2dWithOpenMP<Real64, Int32, Real32>(op);

        case Real64_t:
          return internal::nhwc::runConv2dWithOpenMP<Real64, Int32, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Int64_t:
        switch(output->type){
        case Int32_t:
          return internal::nhwc::runConv2dWithOpenMP<Real64, Int64, Int32>(op);

        case Int64_t:
          return internal::nhwc::runConv2dWithOpenMP<Real64, Int64, Int64>(op);

        case Real32_t:
          return internal::nhwc::runConv2dWithOpenMP<Real64, Int64, Real32>(op);

        case Real64_t:
          return internal::nhwc::runConv2dWithOpenMP<Real64, Int64, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Real32_t:
        switch(output->type){
        case Int32_t:
          return internal::nhwc::runConv2dWithOpenMP<Real64, Real32, Int32>(op);

        case Int64_t:
          return internal::nhwc::runConv2dWithOpenMP<Real64, Real32, Int64>(op);

        case Real32_t:
          return internal::nhwc::runConv2dWithOpenMP<Real64, Real32, Real32>(op);

        case Real64_t:
          return internal::nhwc::runConv2dWithOpenMP<Real64, Real32, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Real64_t:
        switch(output->type){
        case Int32_t:
          return internal::nhwc::runConv2dWithOpenMP<Real64, Real64, Int32>(op);

        case Int64_t:
          return internal::nhwc::runConv2dWithOpenMP<Real64, Real64, Int64>(op);

        case Real32_t:
          return internal::nhwc::runConv2dWithOpenMP<Real64, Real64, Real32>(op);

        case Real64_t:
          return internal::nhwc::runConv2dWithOpenMP<Real64, Real64, Real64>(op);

        default:
          return opw << NoSupport;
        }

      default:
        break;
      }
      return opw << NoSupport;

    default:
      return opw << NoSupport;
    }
  } else if (!strcmp(opw->get<char*>("data_format"), "NCHW")){
    auto opw    = nNet_GetOperatorW(op);
    auto output = opw->outputs()[0];
    auto input  = std::get<1>(opw->inputs()[0]);
    auto filter = std::get<1>(opw->inputs()[1]);

    switch(input->type){
    case Int32_t:
      switch(filter->type){
      case Int32_t:
        switch(output->type){
        case Int32_t:
          return internal::nchw::runConv2dWithOpenMP<Int32, Int32, Int32>(op);

        case Int64_t:
          return internal::nchw::runConv2dWithOpenMP<Int32, Int32, Int64>(op);

        case Real32_t:
          return internal::nchw::runConv2dWithOpenMP<Int32, Int32, Real32>(op);

        case Real64_t:
          return internal::nchw::runConv2dWithOpenMP<Int32, Int32, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Int64_t:
        switch(output->type){
        case Int32_t:
          return internal::nchw::runConv2dWithOpenMP<Int32, Int64, Int32>(op);

        case Int64_t:
          return internal::nchw::runConv2dWithOpenMP<Int32, Int64, Int64>(op);

        case Real32_t:
          return internal::nchw::runConv2dWithOpenMP<Int32, Int64, Real32>(op);

        case Real64_t:
          return internal::nchw::runConv2dWithOpenMP<Int32, Int64, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Real32_t:
        switch(output->type){
        case Int32_t:
          return internal::nchw::runConv2dWithOpenMP<Int32, Real32, Int32>(op);

        case Int64_t:
          return internal::nchw::runConv2dWithOpenMP<Int32, Real32, Int64>(op);

        case Real32_t:
          return internal::nchw::runConv2dWithOpenMP<Int32, Real32, Real32>(op);

        case Real64_t:
          return internal::nchw::runConv2dWithOpenMP<Int32, Real32, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Real64_t:
        switch(output->type){
        case Int32_t:
          return internal::nchw::runConv2dWithOpenMP<Int32, Real64, Int32>(op);

        case Int64_t:
          return internal::nchw::runConv2dWithOpenMP<Int32, Real64, Int64>(op);

        case Real32_t:
          return internal::nchw::runConv2dWithOpenMP<Int32, Real64, Real32>(op);

        case Real64_t:
          return internal::nchw::runConv2dWithOpenMP<Int32, Real64, Real64>(op);

        default:
          return opw << NoSupport;
        }

      default:
        break;
      }
      return opw << NoSupport;

    case Int64_t:
      switch(filter->type){
      case Int32_t:
        switch(output->type){
        case Int32_t:
          return internal::nchw::runConv2dWithOpenMP<Int64, Int32, Int32>(op);

        case Int64_t:
          return internal::nchw::runConv2dWithOpenMP<Int64, Int32, Int64>(op);

        case Real32_t:
          return internal::nchw::runConv2dWithOpenMP<Int64, Int32, Real32>(op);

        case Real64_t:
          return internal::nchw::runConv2dWithOpenMP<Int64, Int32, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Int64_t:
        switch(output->type){
        case Int32_t:
          return internal::nchw::runConv2dWithOpenMP<Int64, Int64, Int32>(op);

        case Int64_t:
          return internal::nchw::runConv2dWithOpenMP<Int64, Int64, Int64>(op);

        case Real32_t:
          return internal::nchw::runConv2dWithOpenMP<Int64, Int64, Real32>(op);

        case Real64_t:
          return internal::nchw::runConv2dWithOpenMP<Int64, Int64, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Real32_t:
        switch(output->type){
        case Int32_t:
          return internal::nchw::runConv2dWithOpenMP<Int64, Real32, Int32>(op);

        case Int64_t:
          return internal::nchw::runConv2dWithOpenMP<Int64, Real32, Int64>(op);

        case Real32_t:
          return internal::nchw::runConv2dWithOpenMP<Int64, Real32, Real32>(op);

        case Real64_t:
          return internal::nchw::runConv2dWithOpenMP<Int64, Real32, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Real64_t:
        switch(output->type){
        case Int32_t:
          return internal::nchw::runConv2dWithOpenMP<Int64, Real64, Int32>(op);

        case Int64_t:
          return internal::nchw::runConv2dWithOpenMP<Int64, Real64, Int64>(op);

        case Real32_t:
          return internal::nchw::runConv2dWithOpenMP<Int64, Real64, Real32>(op);

        case Real64_t:
          return internal::nchw::runConv2dWithOpenMP<Int64, Real64, Real64>(op);

        default:
          return opw << NoSupport;
        }

      default:
        break;
      }
      return opw << NoSupport;

    case Real32_t:
      switch(filter->type){
      case Int32_t:
        switch(output->type){
        case Int32_t:
          return internal::nchw::runConv2dWithOpenMP<Real32, Int32, Int32>(op);

        case Int64_t:
          return internal::nchw::runConv2dWithOpenMP<Real32, Int32, Int64>(op);

        case Real32_t:
          return internal::nchw::runConv2dWithOpenMP<Real32, Int32, Real32>(op);

        case Real64_t:
          return internal::nchw::runConv2dWithOpenMP<Real32, Int32, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Int64_t:
        switch(output->type){
        case Int32_t:
          return internal::nchw::runConv2dWithOpenMP<Real32, Int64, Int32>(op);

        case Int64_t:
          return internal::nchw::runConv2dWithOpenMP<Real32, Int64, Int64>(op);

        case Real32_t:
          return internal::nchw::runConv2dWithOpenMP<Real32, Int64, Real32>(op);

        case Real64_t:
          return internal::nchw::runConv2dWithOpenMP<Real32, Int64, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Real32_t:
        switch(output->type){
        case Int32_t:
          return internal::nchw::runConv2dWithOpenMP<Real32, Real32, Int32>(op);

        case Int64_t:
          return internal::nchw::runConv2dWithOpenMP<Real32, Real32, Int64>(op);

        case Real32_t:
          return internal::nchw::runConv2dWithOpenMP<Real32, Real32, Real32>(op);

        case Real64_t:
          return internal::nchw::runConv2dWithOpenMP<Real32, Real32, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Real64_t:
        switch(output->type){
        case Int32_t:
          return internal::nchw::runConv2dWithOpenMP<Real32, Real64, Int32>(op);

        case Int64_t:
          return internal::nchw::runConv2dWithOpenMP<Real32, Real64, Int64>(op);

        case Real32_t:
          return internal::nchw::runConv2dWithOpenMP<Real32, Real64, Real32>(op);

        case Real64_t:
          return internal::nchw::runConv2dWithOpenMP<Real32, Real64, Real64>(op);

        default:
          return opw << NoSupport;
        }

      default:
        break;
      }
      return opw << NoSupport;

    case Real64_t:
      switch(filter->type){
      case Int32_t:
        switch(output->type){
        case Int32_t:
          return internal::nchw::runConv2dWithOpenMP<Real64, Int32, Int32>(op);

        case Int64_t:
          return internal::nchw::runConv2dWithOpenMP<Real64, Int32, Int64>(op);

        case Real32_t:
          return internal::nchw::runConv2dWithOpenMP<Real64, Int32, Real32>(op);

        case Real64_t:
          return internal::nchw::runConv2dWithOpenMP<Real64, Int32, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Int64_t:
        switch(output->type){
        case Int32_t:
          return internal::nchw::runConv2dWithOpenMP<Real64, Int64, Int32>(op);

        case Int64_t:
          return internal::nchw::runConv2dWithOpenMP<Real64, Int64, Int64>(op);

        case Real32_t:
          return internal::nchw::runConv2dWithOpenMP<Real64, Int64, Real32>(op);

        case Real64_t:
          return internal::nchw::runConv2dWithOpenMP<Real64, Int64, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Real32_t:
        switch(output->type){
        case Int32_t:
          return internal::nchw::runConv2dWithOpenMP<Real64, Real32, Int32>(op);

        case Int64_t:
          return internal::nchw::runConv2dWithOpenMP<Real64, Real32, Int64>(op);

        case Real32_t:
          return internal::nchw::runConv2dWithOpenMP<Real64, Real32, Real32>(op);

        case Real64_t:
          return internal::nchw::runConv2dWithOpenMP<Real64, Real32, Real64>(op);

        default:
          return opw << NoSupport;
        }

      case Real64_t:
        switch(output->type){
        case Int32_t:
          return internal::nchw::runConv2dWithOpenMP<Real64, Real64, Int32>(op);

        case Int64_t:
          return internal::nchw::runConv2dWithOpenMP<Real64, Real64, Int64>(op);

        case Real32_t:
          return internal::nchw::runConv2dWithOpenMP<Real64, Real64, Real32>(op);

        case Real64_t:
          return internal::nchw::runConv2dWithOpenMP<Real64, Real64, Real64>(op);

        default:
          return opw << NoSupport;
        }

      default:
        break;
      }
      return opw << NoSupport;

    default:
      return opw << NoSupport;
    }
  }
  else return opw << NoSupport;
}

bool Derive(Operator op, Interpreter interpreter) {
  return false;
}
} // namespace conv2d

Registration doRegisterConv2D() {
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = conv2d::Prepare;
  result.eval   = conv2d::Eval;
  result.derive = conv2d::Derive;
  return result;
}
} // namespace generic
} // namespace kernels
} // namespace nn
