#ifdef LIBCGRAPH_GIT_REFSPEC
#include "devices/generic/generic.h"
#include "devices/generic/math.h"
#else
#include "generic.h"
#include "math.h"
#endif

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
namespace kernels{
namespace generic{
namespace maxpool{
using namespace nn::interpreter;

namespace internal{
namespace nhwc{
template<typename Input, typename Output>
bool runMaxpoolWithoutOpenMP(Operator op){
  using namespace nn::generic::math;

  auto opw = nNet_GetOperatorW(op);
  auto idx = 0;

  auto input  = std::get<1>(opw->inputs()[0]);
  auto output = opw->outputs()[0];

  auto in_W = int64_t(input->dims[1]);
  auto in_H = int64_t(input->dims[2]);

  auto output_N = nn::tensor::dimemsion(output, 0);
  auto output_H = nn::tensor::dimemsion(output, 1);
  auto output_W = nn::tensor::dimemsion(output, 2);
  auto output_C = nn::tensor::dimemsion(output, 3);
  auto input_H  = nn::tensor::dimemsion(input, 1);
  auto input_W  = nn::tensor::dimemsion(input, 2);
  auto input_C  = nn::tensor::dimemsion(input, 3);

  auto ksize_H   = opw->get<int64_t>("ksize.H");
  auto ksize_W   = opw->get<int64_t>("ksize.W");
  auto strides_H = opw->get<int64_t>("strides.H");
  auto strides_W = opw->get<int64_t>("strides.W");

  auto padding_H = computePadding(strides_H, 1, in_H, ksize_H, output->dims[2]);
  auto padding_W = computePadding(strides_W, 1, in_W, ksize_W, output->dims[1]);

  auto shift_W = input_C;
  auto shift_H = input_C*input_W;

  auto input_array  = (Input*)nNet_GetArrayData(input);
  auto output_array = (Output*)nNet_GetArrayData(output);

  base::Boundary bound{[]() {},[&]() {
    nNet_TryDropingArray(input, input_array);
  }};

  for (auto out_N = 0; out_N < output_N; ++out_N) {
    auto begin  = out_N*input_W*input_H*input_C;

    for (auto out_H = 0; out_H < output_H; ++out_H) {
      auto origin_in_H = int(out_H * strides_H - padding_H);
      auto hstart = (origin_in_H + std::max(0, -origin_in_H))*shift_H;
      auto hend   = (origin_in_H + std::min(ksize_H, in_H - origin_in_H))*shift_H;

      for (auto out_W = 0; out_W < output_W; ++out_W) {
        auto origin_in_W = int(out_W * strides_W - padding_W);
        auto wstart = (origin_in_W + std::max(0, -origin_in_W))*shift_W;
        auto wend   = (origin_in_W + std::min(ksize_W, in_W - origin_in_W))*shift_W;

        for (auto out_C = 0; out_C < output_C; ++out_C) {
          bool first = true;

          for (auto h = hstart; h < hend; h += shift_H) {
            for (auto w = wstart; w < wend; w += shift_W) {
              if (first){
                output_array[idx] = input_array[begin + h + w + out_C];
                first = false;
              } else {
                output_array[idx] =
                    std::max((Output)input_array[begin + h + w + out_C],
                             output_array[idx]);
              }
            }
          }

          idx++;
        }
      }
    }
  }

  if (!nNet_SetArrayData(output, output_array))
    return opw << BadAccess.reason("nNet_SetArrayData()");
  return false;
}

template<typename Input, typename Output>
bool runMaxpoolWithOpenMP(Operator op){
 #if defined(_OPENMP)
  using namespace nn::generic::math;

  auto opw = nNet_GetOperatorW(op);
  auto idx = 0;

  auto input  = std::get<1>(opw->inputs()[0]);
  auto output = opw->outputs()[0];

  auto in_W = int64_t(input->dims[1]);
  auto in_H = int64_t(input->dims[2]);

  auto output_N = nn::tensor::dimemsion(output, 0);
  auto output_H = nn::tensor::dimemsion(output, 1);
  auto output_W = nn::tensor::dimemsion(output, 2);
  auto output_C = nn::tensor::dimemsion(output, 3);
  auto input_H  = nn::tensor::dimemsion(input, 1);
  auto input_W  = nn::tensor::dimemsion(input, 2);
  auto input_C  = nn::tensor::dimemsion(input, 3);

  auto ksize_H   = opw->get<int64_t>("ksize.H");
  auto ksize_W   = opw->get<int64_t>("ksize.W");
  auto strides_H = opw->get<int64_t>("strides.H");
  auto strides_W = opw->get<int64_t>("strides.W");

  auto padding_H = computePadding(strides_H, 1, in_H, ksize_H, output->dims[2]);
  auto padding_W = computePadding(strides_W, 1, in_W, ksize_W, output->dims[1]);

  auto shift_W = input_C;
  auto shift_H = input_C*input_W;

  auto input_array  = (Input*)nNet_GetArrayData(input);
  auto output_array = (Output*)nNet_GetArrayData(output);

  base::Boundary bound{[]() {},[&]() {
    nNet_TryDropingArray(input, input_array);
  }};

  #pragma omp parallel for collapse(2)
  for (auto out_N = 0; out_N < output_N; ++out_N) {
    for (auto out_H = 0; out_H < output_H; ++out_H) {
      auto origin_in_H = int(out_H * strides_H - padding_H);
      auto begin  = out_N*input_W*input_H*input_C;
      auto hstart = (origin_in_H + std::max(0, -origin_in_H))*shift_H;
      auto hend   = (origin_in_H + std::min(ksize_H, in_H - origin_in_H))*shift_H;


      for (auto out_W = 0; out_W < output_W; ++out_W) {
        auto origin_in_W = int(out_W * strides_W - padding_W);
        auto wstart = (origin_in_W + std::max(0, -origin_in_W))*shift_W;
        auto wend   = (origin_in_W + std::min(ksize_W, in_W - origin_in_W))*shift_W;

        for (auto out_C = 0; out_C < output_C; ++out_C) {
          bool first = true;

          for (auto h = hstart; h < hend; h += shift_H) {
            for (auto w = wstart; w < wend; w += shift_W) {
              if (first){
                output_array[idx] = input_array[begin + h + w + out_C];
                first = false;
              } else {
                output_array[idx] =
                    std::max((Output)input_array[begin + h + w + out_C],
                             output_array[idx]);
              }
            }
          }

          idx++;
        }
      }
    }
  }

  if (!nNet_SetArrayData(output, output_array))
    return opw << BadAccess.reason("nNet_SetArrayData()");
  return false;
 #else
  return runMaxpoolWithoutOpenMP<Input, Output>(op);
 #endif
}
} // namespace nhwc

namespace nchw{
template<typename Input, typename Output>
bool runMaxpoolWithoutOpenMP(Operator op){
  using namespace nn::generic::math;

  auto opw    = nNet_GetOperatorW(op);
  auto input  = std::get<1>(opw->inputs()[0]);
  auto output = opw->outputs()[0];

  auto output_N = nn::tensor::dimemsion(output, 0);
  auto output_C = nn::tensor::dimemsion(output, 1);
  auto output_H = nn::tensor::dimemsion(output, 2);
  auto output_W = nn::tensor::dimemsion(output, 3);
  auto input_N  = nn::tensor::dimemsion(input, 0);
  auto input_C  = nn::tensor::dimemsion(input, 1);
  auto input_H  = nn::tensor::dimemsion(input, 2);
  auto input_W  = nn::tensor::dimemsion(input, 3);

  auto ksize_H   = (int)opw->get<int64_t>("ksize.H");
  auto ksize_W   = (int)opw->get<int64_t>("ksize.W");
  auto strides_H = (int)opw->get<int64_t>("strides.H");
  auto strides_W = (int)opw->get<int64_t>("strides.W");

  auto padding_H = computePadding(strides_H, 1, input_H, ksize_H, output->dims[1]);
  auto padding_W = computePadding(strides_W, 1, input_W, ksize_W, output->dims[0]);

  auto input_array  = (Input*)nNet_GetArrayData(input);
  auto output_array = (Output *)nNet_GetArrayData(output);

  base::Boundary bound{[]() {},[&]() {
    nNet_TryDropingArray(input, input_array);
  }};

  int idx = 0;
  for (auto out_N = 0; out_N < output_N; ++out_N){
    for (auto out_C = 0; out_C < output_C; ++out_C){
      auto begin  = out_N*input_W*input_H*input_C*input_N + 
                    out_C*input_W*input_H;

      for (auto out_H = 0; out_H < output_H; ++out_H){
        auto origin_in_H = int(out_H*strides_H - padding_H);
        auto hstart = std::max(0, -origin_in_H);
        auto hend   = std::min(ksize_H, input_H - origin_in_H);

        for (auto out_W = 0; out_W < output_W; ++out_W){
          auto origin_in_W = int(out_W*strides_W - padding_W);
          auto wstart = std::max(0, -origin_in_W);
          auto wend   = std::min(ksize_W, input_W - origin_in_W);
          auto first  = false;

          for (int i = hstart, h = begin + input_W*(hstart + origin_in_H);
               i < hend; ++i, h += input_W){
            for (auto j = h + origin_in_W + wstart; j < h + origin_in_W + wend;
                 ++j) {

              if (first){
                output_array[idx] = input_array[j];
                first = false;
              } else {
                output_array[idx] =
                    std::max((Output)input_array[j], output_array[idx]);
              }
            }
          }

          idx++;
        }
      }
    }
  }

  if (!nNet_SetArrayData(output, output_array))
    return opw << BadAccess.reason("nNet_SetArrayData()");
  return false;
}

template<typename Input, typename Output>
bool runMaxpoolWithOpenMP(Operator op){
 #if defined(_OPENMP)
  using namespace nn::generic::math;

  auto opw    = nNet_GetOperatorW(op);
  auto input  = std::get<1>(opw->inputs()[0]);
  auto output = opw->outputs()[0];

  auto output_N = nn::tensor::dimemsion(output, 0);
  auto output_C = nn::tensor::dimemsion(output, 1);
  auto output_H = nn::tensor::dimemsion(output, 2);
  auto output_W = nn::tensor::dimemsion(output, 3);
  auto input_N  = nn::tensor::dimemsion(input, 0);
  auto input_C  = nn::tensor::dimemsion(input, 1);
  auto input_H  = nn::tensor::dimemsion(input, 2);
  auto input_W  = nn::tensor::dimemsion(input, 3);

  auto ksize_H   = (int)opw->get<int64_t>("ksize.H");
  auto ksize_W   = (int)opw->get<int64_t>("ksize.W");
  auto strides_H = (int)opw->get<int64_t>("strides.H");
  auto strides_W = (int)opw->get<int64_t>("strides.W");

  auto padding_H = computePadding(strides_H, 1, input_H, ksize_H, output->dims[1]);
  auto padding_W = computePadding(strides_W, 1, input_W, ksize_W, output->dims[0]);

  auto input_array  = (Input*)nNet_GetArrayData(input);
  auto output_array = (Output *)nNet_GetArrayData(output);

  base::Boundary bound{[]() {},[&]() {
    nNet_TryDropingArray(input, input_array);
  }};

  int idx = 0;

  omp_set_nested(1);

  #pragma omp parallel for collapse(2)
  for (auto out_N = 0; out_N < output_N; ++out_N){
    for (auto out_C = 0; out_C < output_C; ++out_C){
      auto begin  = out_N*input_W*input_H*input_C*input_N + 
                    out_C*input_W*input_H;

      for (auto out_H = 0; out_H < output_H; ++out_H){
        auto origin_in_H = int(out_H*strides_H - padding_H);
        auto hstart = std::max(0, -origin_in_H);
        auto hend   = std::min(ksize_H, input_H - origin_in_H);

        for (auto out_W = 0; out_W < output_W; ++out_W){
          auto origin_in_W = int(out_W*strides_W - padding_W);
          auto wstart = std::max(0, -origin_in_W);
          auto wend   = std::min(ksize_W, input_W - origin_in_W);
          auto first  = false;

          for (int i = hstart, h = begin + input_W*(hstart + origin_in_H);
               i < hend; ++i, h += input_W){
            for (auto j = h + origin_in_W + wstart; j < h + origin_in_W + wend;
                 ++j) {

              if (first){
                output_array[idx] = input_array[j];
                first = false;
              } else {
                output_array[idx] =
                    std::max((Output)input_array[j], output_array[idx]);
              }
            }
          }

          idx++;
        }
      }
    }
  }

  if (!nNet_SetArrayData(output, output_array))
    return opw << BadAccess.reason("nNet_SetArrayData()");
  return false;
 #else
  return runMaxpoolWithoutOpenMP<Input, Output>(op);
 #endif
}
} // namespace nchw
} // namespace internal

bool IsOkeyToCreateOutput(OperatorW* opw,
                          std::string&& data_format, std::string&& padding,
                          int ksize_W, int ksize_H,
                          int strides_W, int strides_H){
  auto input  = std::get<1>(opw->inputs()[0]);
  auto output = opw->outputs()[0];
  auto fixed  = 1, wfixed = 0, hfixed = 0;

  if (nNet_GetTensorType(output) != TensorT)
    return true;
  else if (output->type != opw->get<NumT>("T"))
    return true;
  else if (output->ndims != 4)
    return true;
  else if (data_format == "NHWC"){
    auto input_N = nn::tensor::dimemsion(input, 0);
    auto input_H = nn::tensor::dimemsion(input, 1);
    auto input_W = nn::tensor::dimemsion(input, 2);
    auto input_C = nn::tensor::dimemsion(input, 3);

    if (padding == "SAME"){
      wfixed = (input_W%strides_W > 0)? 1: 0;
      hfixed = (input_H%strides_H > 0)? 1: 0;
      fixed = 0;
    }

    if (output->dims[0] != input_C)
      return true;
    if (output->dims[1] != ((input_W - fixed*ksize_W)/strides_W + fixed + wfixed))
      return true;
    if (output->dims[2] != ((input_H - fixed*ksize_H)/strides_H + fixed + hfixed))
      return true;
    if (output->dims[3] != input_N)
      return true;
  } else if (data_format == "NCHW"){
    auto input_N = nn::tensor::dimemsion(input, 0);
    auto input_C = nn::tensor::dimemsion(input, 1);
    auto input_H = nn::tensor::dimemsion(input, 2);
    auto input_W = nn::tensor::dimemsion(input, 3);
  
    if (padding == "SAME"){
      wfixed = (input_W%strides_W > 0)? 1: 0;
      hfixed = (input_H%strides_H > 0)? 1: 0;
      fixed = 0;
    }

    if (output->dims[0] != ((input_W - fixed*ksize_W)/strides_W + fixed + wfixed))
      return true;
    if (output->dims[1] != ((input_H - fixed*ksize_H)/strides_H + fixed + hfixed))
      return true;
    if (output->dims[2] != input_C)
      return true;
    if (output->dims[3] != input_N)
      return true;
  }

  return false;
}

Operator Prepare(Operator op, Interpreter interpreter, CString model){
  try{
    auto attr = OperatorW::config(model, "MaxPool");

    if (!op->device)
      nNet_GetInterpreterW(interpreter)->error(
                 BadLogic.reason("op \'MaxPool' has been pluged like a macro"));
    else if (attr){
      auto maxpool = nNet_CreateTensorC(interpreter, nullptr);
      auto opw     = new interpreter::OperatorW(op, COUNT_MAXPOOL_ARGUMENTS, 
                                                interpreter, op->device, attr);

      if (!maxpool)
        return nNet_RemoveOperatorW(interpreter, opw->interface());
      opw->outputs().push_back(maxpool);
      return opw->interface();
    } else
      nNet_GetInterpreterW(interpreter)->error(NotFound);
  } catch(base::Error& error){
    nNet_GetInterpreterW(interpreter)->error(rvalue(error));
  }
  return nullptr;
}

bool Check(Operator op, Interpreter UNUSED(interpreter)){
  using Int64Vector = std::vector<int64_t>;

  auto opw    = nNet_GetOperatorW(op);
  auto input  = Tensor{nullptr};
  auto output = opw->outputs()[0];

  /* @NOTE: MaxPool requires exactly only 1 input */
  if (opw->inputs().size() > 1){
    return opw << (BadLogic << "operator" << op->name << "require 1 input,"
                            << "but you provide" 
                            << std::to_string(opw->inputs().size()));
  } else input = std::get<1>(opw->inputs()[0]);

  /* @NOTE: check input dimension */
  if (!input || input->ndims != 4){
    return opw << (NoSupport << "input of operator" << op->name
                             << "must be a 4-D tensor");
  } else {
    auto type    = opw->get<NumT>("T");
    auto ksize   = opw->get<Int64Vector>("ksize");
    auto strides = opw->get<Int64Vector>("strides");
    auto format  = std::string{opw->get<char*>("data_format")};
    auto padding = std::string{opw->get<char*>("padding")};

    if (type == String_t || type == Bool_t)
      return opw << (NoSupport << "T == String_t or T == Bool_t");
    else if (ksize.size() != 4){
      return opw << (BadLogic << "MaxPool require ksize contain 4 values, but"
                              << "you provided" << std::to_string(ksize.size())
                              << "values");
    } else if (strides.size() != 4){
      return opw << (BadLogic << "MaxPool require strides contain 4 values, but"
                              << "you provided" << std::to_string(ksize.size())
                              << "values");
    } else {
      auto ksize_N = ksize[format.find('N')];
      auto ksize_H = ksize[format.find('H')];
      auto ksize_W = ksize[format.find('W')];
      auto ksize_C = ksize[format.find('C')];

      auto strides_N = strides[format.find('N')];
      auto strides_H = strides[format.find('H')];
      auto strides_W = strides[format.find('W')];
      auto strides_C = strides[format.find('C')];

      int input_H, input_W, input_N, input_C;
      Shape shape;

      /* @NOTE: config ksize attributes */
      if (ksize_N != 1){
        opw << (NoSupport << "ksize.N{" << std::to_string(ksize_N) << "} != 1");
      } else if (ksize_C != 1){
        opw << (NoSupport << "ksize.C{" << std::to_string(ksize_C) << "} != 1");
      } else {
        opw->set("ksize.N", ksize_N);
        opw->set("ksize.H", ksize_H);
        opw->set("ksize.W", ksize_W);
        opw->set("ksize.C", ksize_C);
      }

      /* @NOTE: config strides attribute */
      if (strides_N != 1){
        opw << (NoSupport << "strides.N{" << std::to_string(strides_N) 
                          << "} != 1");
      } else if (strides_C != 1){
        opw << (NoSupport << "strides.C{" << std::to_string(strides_C) 
                          << "} != 1");
      } else {
        opw->set("strides.N", strides_N);
        opw->set("strides.H", strides_H);
        opw->set("strides.W", strides_W);
        opw->set("strides.C", strides_C);
      }

      /* @NOTE: prepare output */
      if (!strcmp(opw->get<char*>("data_format"), "NHWC")){
        input_N = nn::tensor::dimemsion(input, 0);
        input_H = nn::tensor::dimemsion(input, 1);
        input_W = nn::tensor::dimemsion(input, 2);
        input_C = nn::tensor::dimemsion(input, 3);
      } else if (!strcmp(opw->get<char*>("data_format"), "NCHW")){
        input_N = nn::tensor::dimemsion(input, 0);
        input_C = nn::tensor::dimemsion(input, 1);
        input_H = nn::tensor::dimemsion(input, 2);
        input_W = nn::tensor::dimemsion(input, 3);
      } else {
        return opw << (NoSupport << "operator" << op->name
                                 << "only support format \'NHWC\' or \'NCHW\'");
      }

      if (input_W == ksize_W){
        opw << (NoSupport << "ksize.W{" << std::to_string(ksize_W)
                                 << "} == input.W{" << std::to_string(input_W) 
                                 << "}");
      }
      if (input_H == ksize_H){
        opw << (NoSupport << "ksize.H{" << std::to_string(ksize_H)
                                 << "} == input.H{" << std::to_string(input_H)
                                 << "}");
      }

      if (*opw) return *opw;
      
      /* @NOTE: we must be carefull about the differnet between tensor dimension
       * and our dimension */
      if (IsOkeyToCreateOutput(opw, opw->get<char*>("data_format"), rvalue(padding),
                               ksize_W, ksize_H, strides_W, strides_H)){
        int fixed = 1, wfixed = 0, hfixed = 0;

        if (padding == "SAME"){
          wfixed = (input_W%strides_W > 0)? 1: 0;
          hfixed = (input_H%strides_H > 0)? 1: 0;
          fixed = 0;
        } else if (padding != "VALID"){
          return opw << (NoSupport << "attribute \'padding\' only support 2 options"
                                   << "\'VALID\' or \'SAME\' but you provided"
                                   << padding);
        }

        if (!strcmp(opw->get<char*>("data_format"), "NHWC")){
          int dims[4];

          dims[0] = input_C;
          dims[1] = (input_W - fixed*ksize_W)/strides_W + fixed + wfixed;
          dims[2] = (input_H - fixed*ksize_H)/strides_H + fixed + hfixed;
          dims[3] = input_N;

          shape = nNet_CreateTensorShapeC2(interpreter, 4, dims);
        } else {
          int dims[4];

          dims[0] = (input_W - fixed*ksize_W)/strides_W + fixed + wfixed;
          dims[1] = (input_H - fixed*ksize_H)/strides_H + fixed + hfixed;
          dims[2] = input_C;
          dims[3] = input_N;

          shape = nNet_CreateTensorShapeC2(interpreter, 4, dims);
        }
        if (!shape)
          return opw << DrainMem.reason("nNet_CreateTensorShapeC1()");

        if (!nNet_SetTensorNumTW(interpreter, output, type))
          return opw << BadAccess.reason("nNet_SetTensorNumTW()");
        else if (!nNet_SetTensorShapeW(interpreter, output, shape))
          return opw << BadAccess.reason("nNet_SetTensorShapeW()");

       #if USE_VECTORIZE
        if (!nNet_ResetTensor(output))
          return BadAccess.reason("nNet_ResetTensor()");
       #endif
      } else {
        if (!nNet_ResetTensor(output))
          return BadAccess.reason("nNet_ResetTensor()");
      }

      return false;
    }
  }
}

bool Eval(Operator op, Interpreter interpreter){
  auto opw = nNet_GetOperatorW(op);

  if (Check(op, interpreter))
    return true;

  if (!strcmp(opw->get<char*>("data_format"), "NHWC")){
    auto opw    = nNet_GetOperatorW(op);
    auto output = opw->outputs()[0];
    auto input  = std::get<1>(opw->inputs()[0]);

    switch(input->type){
    case Int32_t:
      switch(output->type){
      case Int32_t:
        return internal::nhwc::runMaxpoolWithoutOpenMP<Int32, Int32>(op);

      case Int64_t:
        return internal::nhwc::runMaxpoolWithoutOpenMP<Int32, Int64>(op);

      case Real32_t:
        return internal::nhwc::runMaxpoolWithoutOpenMP<Int32, Real32>(op);

      case Real64_t:
        return internal::nhwc::runMaxpoolWithoutOpenMP<Int32, Real64>(op);

      default:
        return opw << NoSupport;
      }

    case Int64_t:
      switch(output->type){
      case Int32_t:
        return internal::nhwc::runMaxpoolWithoutOpenMP<Int64, Int32>(op);

      case Int64_t:
        return internal::nhwc::runMaxpoolWithoutOpenMP<Int64, Int64>(op);

      case Real32_t:
        return internal::nhwc::runMaxpoolWithoutOpenMP<Int64, Real32>(op);

      case Real64_t:
        return internal::nhwc::runMaxpoolWithoutOpenMP<Int64, Real64>(op);

      default:
        return opw << NoSupport;
      }

    case Real32_t:
      switch(output->type){
      case Int32_t:
        return internal::nhwc::runMaxpoolWithoutOpenMP<Real32, Int32>(op);

      case Int64_t:
        return internal::nhwc::runMaxpoolWithoutOpenMP<Real32, Int64>(op);

      case Real32_t:
        return internal::nhwc::runMaxpoolWithoutOpenMP<Real32, Real32>(op);

      case Real64_t:
        return internal::nhwc::runMaxpoolWithoutOpenMP<Real32, Real64>(op);

      default:
        return opw << NoSupport;
      }

    case Real64_t:
      switch(output->type){
      case Int32_t:
        return internal::nhwc::runMaxpoolWithoutOpenMP<Real64, Int32>(op);

      case Int64_t:
        return internal::nhwc::runMaxpoolWithoutOpenMP<Real64, Int64>(op);

      case Real32_t:
        return internal::nhwc::runMaxpoolWithoutOpenMP<Real64, Real32>(op);

      case Real64_t:
        return internal::nhwc::runMaxpoolWithoutOpenMP<Real64, Real64>(op);

      default:
        return opw << NoSupport;
      }

    default:
      return opw << NoSupport;
    }
  } else if (!strcmp(opw->get<char*>("data_format"), "NCHW")){
    auto opw    = nNet_GetOperatorW(op);
    auto output = opw->outputs()[0];
    auto input  = std::get<1>(opw->inputs()[0]);

    switch(input->type){
    case Int32_t:
      switch(output->type){
      case Int32_t:
        return internal::nchw::runMaxpoolWithoutOpenMP<Int32, Int32>(op);

      case Int64_t:
        return internal::nchw::runMaxpoolWithoutOpenMP<Int32, Int64>(op);

      case Real32_t:
        return internal::nchw::runMaxpoolWithoutOpenMP<Int32, Real32>(op);

      case Real64_t:
        return internal::nchw::runMaxpoolWithoutOpenMP<Int32, Real64>(op);

      default:
        return opw << NoSupport;
      }

    case Int64_t:
      switch(output->type){
      case Int32_t:
        return internal::nchw::runMaxpoolWithoutOpenMP<Int64, Int32>(op);

      case Int64_t:
        return internal::nchw::runMaxpoolWithoutOpenMP<Int64, Int64>(op);

      case Real32_t:
        return internal::nchw::runMaxpoolWithoutOpenMP<Int64, Real32>(op);

      case Real64_t:
        return internal::nchw::runMaxpoolWithoutOpenMP<Int64, Real64>(op);

      default:
        return opw << NoSupport;
      }

    case Real32_t:
      switch(output->type){
      case Int32_t:
        return internal::nchw::runMaxpoolWithoutOpenMP<Real32, Int32>(op);

      case Int64_t:
        return internal::nchw::runMaxpoolWithoutOpenMP<Real32, Int64>(op);

      case Real32_t:
        return internal::nchw::runMaxpoolWithoutOpenMP<Real32, Real32>(op);

      case Real64_t:
        return internal::nchw::runMaxpoolWithoutOpenMP<Real32, Real64>(op);

      default:
        return opw << NoSupport;
      }

    case Real64_t:
      switch(output->type){
      case Int32_t:
        return internal::nchw::runMaxpoolWithoutOpenMP<Real64, Int32>(op);

      case Int64_t:
        return internal::nchw::runMaxpoolWithoutOpenMP<Real64, Int64>(op);

      case Real32_t:
        return internal::nchw::runMaxpoolWithoutOpenMP<Real64, Real32>(op);

      case Real64_t:
        return internal::nchw::runMaxpoolWithoutOpenMP<Real64, Real64>(op);

      default:
        return opw << NoSupport;
      }

    default:
      return opw << NoSupport;
    }
  } else return opw << NoSupport;
}

bool Derive(Operator op, Interpreter interpreter){
  return false;
}
} // namespace maxpool

Registration doRegisterMaxPool(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = maxpool::Prepare;
  result.eval   = maxpool::Eval;
  result.derive = maxpool::Derive;
  return result;
}
} // namespace generic
} // namespace kernels
} // namespace nn
