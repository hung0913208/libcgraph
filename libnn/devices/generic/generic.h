#if !defined(LIBNN_PLATFORM_CPU_H_)  && __cplusplus
#define LIBNN_PLATFORM_CPU_H_
#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "objmacros.h"

#if defined(_OPENMP)
#include <omp.h>
#endif

#define COUNT_DECODE_WAV_ARGUMENTS        13
#define COUNT_AUDIO_SPECTROGRAM_ARGUMENTS  9
#define COUNT_MFCC_ARGUMENTS               5

#define COUNT_ADD_ARGUMENTS     3
#define COUNT_MATMUL_ARGUMENTS  5
#define COUNT_RESHAPE_ARGUMENTS 13

#define COUNT_CONV2D_ARGUMENTS  15
#define COUNT_RELU_ARGUMENTS    13
#define COUNT_MAXPOOL_ARGUMENTS 13

#define COUNT_SOFTMAX_ARGUMENTS 13

CHECK_OP(generic, add);
CHECK_OP(generic, matmul);
CHECK_OP(generic, reshape);

CHECK_OP(generic, conv2d);
CHECK_OP(generic, relu);
CHECK_OP(generic, maxpool);

CHECK_OP(generic, softmax);

PREPARE_OP(generic, mfcc);
PREPARE_OP(generic, decode_wav);
PREPARE_OP(generic, audio_spectrogram);

PREPARE_OP(generic, add);
PREPARE_OP(generic, matmul);
PREPARE_OP(generic, reshape);

PREPARE_OP(generic, conv2d);
PREPARE_OP(generic, relu);
PREPARE_OP(generic, maxpool);

PREPARE_OP(generic, softmax);

namespace nn{

namespace device{
namespace generic{
class CPUManager: public interpreter::DeviceW{
 public:
  CPUManager();
};
} // namespace generic
} // namespace device
} // namespace nn
#endif  // LIBNN_PLATFORM_CPU_H_