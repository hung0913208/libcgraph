#ifndef LIBNN_GENERIC_DOT_H_
#define LIBNN_GENERIC_DOT_H_

#ifdef LIBCGRAPH_GIT_REFSPEC
#include "libnn/tensor.h"
#else
#include "tensor.h"
#endif

namespace nn {
namespace generic {
namespace math{
namespace dot{
#if USE_ACCELERATE == 1 && APPLE /* ACCELERATE */
template<typename Type>
int dot(Tensor left, Tensor right, Tensor result){
  if (nNet_GetTensorType(left) == VectorT && 
      nNet_GetTensorType(right) == VectorT){
    /* @NOTE: level 1: dot(vector, vector) */

  } else if (nNet_GetTensorType(left) == MatrixT && 
      nNet_GetTensorType(right) == VectorT){
    /* @NOTE: level 2: dot(matrix, vector) */

  } else if (nNet_GetTensorType(left) == MatrixT && 
      nNet_GetTensorType(right) == MatrixT){
    /* @NOTE: level 3: dot(matrix, matrix) */
    vDSP_mmul(left, 1, right, 1, output, 1, 1000, 1000, 1000);
  } else if (nNet_GetTensorType(left) == VectorT && 
    nNet_GetTensorType(right) == MatrixT){
    /* @NOTE: level 2: dot(vector, matrix) transport to (matrix, vector) */

    Tensor left_T;
    Tensor right_T;

    return dot<Type>(left_T, right_T);
  }
}
#elif USE_ACCELERATE == 2 		 /* OPENBLAS   */
template<typename Type>
int dot(Tensor left, Tensor right, Tensor result){
  if (nNet_GetTensorType(left) == VectorT && 
      nNet_GetTensorType(right) == VectorT){
    /* @NOTE: level 1: dot(vector, vector) */

  } else if (nNet_GetTensorType(left) == MatrixT && 
      nNet_GetTensorType(right) == VectorT){
    /* @NOTE: level 2: dot(matrix, vector) */

  } else if (nNet_GetTensorType(left) == MatrixT && 
      nNet_GetTensorType(right) == MatrixT){
    /* @NOTE: level 3: dot(matrix, matrix) */
    cblas_sgemm(CblasRowMajor, CblasNoTrans, CblasNoTrans, 1000, 1000, 1000, 1.0f, 
                left, 1000, right, 1000, 0.0f, output, 1000);
  } else if (nNet_GetTensorType(left) == VectorT && 
    nNet_GetTensorType(right) == MatrixT){
    /* @NOTE: level 2: dot(vector, matrix) transport to (matrix, vector) */

    Tensor left_T;
    Tensor right_T;

    return dot<Type>(left_T, right_T);
  }
}
#elif USE_ACCELERATE == 3 		 /* INTEL MKL  */
template<typename Type>
int dot(Tensor left, Tensor right, Tensor result){
}
#else
#endif
} // namespace dot
} // namespace math
} // namespace generic
} // namespace nn
#endif  // LIBNN_GENERIC_DOT_H_
