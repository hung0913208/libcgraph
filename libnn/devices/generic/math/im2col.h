#ifndef LIBNN_GENERIC_CONV2D_IM2COL_H_
#define LIBNN_GENERIC_CONV2D_IM2COL_H_
#ifdef LIBCGRAPH_GIT_REFSPEC
#include <libbase/all.hpp>
#include <libnn/utils.h>
#include <libnn/tensor.h>
#include <libnn/interpreter.h>
#else
#include "utils.h"
#include "tensor.h"
#include "interpreter.h"
#endif
#include "padding.h"

#if _OPENMP
#include <omp.h>
#endif

namespace nn {
namespace generic {
namespace math{
namespace conv2d {
namespace nhwc{
template<typename Type>
bool im2col(Interpreter interpreter,
            Tensor input, Tensor filter, Tensor* &result,
            std::vector<int> strides, std::vector<int> dilations,
            std::string padding, int index = -1){
  using namespace nn::tensor;
  using namespace nn::generic::math;

  if (!filter || !input) return false;
  Type** out_arrays = nullptr;

  Shape shape{nullptr};
  int   dims[2], fixed = padding != "SAME";

  int filter_H  = dimemsion(filter, 0);
  int filter_W  = dimemsion(filter, 1); 
  int filter_iC = dimemsion(filter, 2);
  int filter_oC = dimemsion(filter, 3);

  int input_N = dimemsion(input, 0);
  int input_H = dimemsion(input, 1);
  int input_W = dimemsion(input, 2); 
  int input_C = dimemsion(input, 3);

  int effective_filter_W = (filter_W - 1) * dilations[1];
  int effective_filter_H = (filter_H - 1) * dilations[0];
  int output_W = (input_W - fixed*effective_filter_W + strides[1] - 1)/strides[1];
  int output_H = (input_H - fixed*effective_filter_H + strides[0] - 1)/strides[0];

  long filter_col_count = dimemsion(filter, 0)*dimemsion(filter, 1);
  long filter_row_count = dimemsion(filter, 3);

  long input_col_count = dimemsion(filter, 0)*dimemsion(filter, 1);
  long input_row_count = output_H*output_W*dimemsion(filter, 3);
  bool renew_result = result == nullptr;
  bool status = false;

  if (input_col_count * input_row_count >= nNet_MaxPhysicalMemory()){
    return !(DrainMem << "you require (" 
                      << std::to_string(input_col_count * input_row_count) << ")"
                      << "more than" 
                      << std::to_string(nNet_MaxPhysicalMemory()));
  } else if (filter_row_count * filter_col_count >= nNet_MaxPhysicalMemory()){
    return !(DrainMem << "you require (" 
                      << std::to_string(filter_row_count * filter_col_count) << ")"
                      << "more than" 
                      << std::to_string(nNet_MaxPhysicalMemory()));
  }

  if (index < 0)
    out_arrays = (Type**)malloc(dimemsion(filter, 2)*sizeof(Type*));
  else if (index < dimemsion(filter, 2))
    out_arrays = (Type**)malloc(sizeof(Type*));
  else
    return false;

  if (!result){
    if (index < 0)
      result = (Tensor*)malloc((2*dimemsion(filter, 2))*sizeof(Tensor));
    else
      result = (Tensor*)malloc(2*sizeof(Tensor));
  }

  if (nullptr == result){
    DrainMem;
    goto fail;
  }

  /* @NOTE: build input's results */
  if (index < 0){
    for (auto i = 0; i < dimemsion(filter, 2); ++i){
      dims[0] = input_col_count;
      dims[1] = input_row_count;

      if (renew_result || result[i]->ndims != 2 || 
          result[i]->dims[0] != input_col_count || 
          result[i]->dims[1] != input_row_count){
        if (!(shape = nNet_CreateTensorShapeC2(interpreter, 2, dims))){
          BadAccess << "nNet_CreateTensorShapeC2";
          goto fail;
        }
        else {
          if (!(result[i] = nNet_CreateTensorC(interpreter, shape))){
            BadAccess << "nNet_CreateTensorC";
            goto fail;
          }

          if (!result[i] || !nNet_SetTensorNumTW(interpreter, result[i], filter->type)){
            BadAccess << "nNet_SetTensorNumTW";
            goto fail;
          }
          shape = nNet_RemoveTensorShapeW(interpreter, shape);
        }
      }
    }
  } else {
    dims[0] = input_col_count;
    dims[1] = input_row_count;

    if (!(shape = nNet_CreateTensorShapeC2(interpreter, 2, dims))){
      BadAccess << "nNet_CreateTensorShapeC2";
      goto fail;
    } else {
      if (!(result[0] = nNet_CreateTensorC(interpreter, shape))){
        BadAccess << "nNet_CreateTensorC";
        goto fail;
      }

      if (!result[0] || !nNet_SetTensorNumTW(interpreter, result[0], filter->type)){
        BadAccess << "nNet_SetTensorNumTW";
        goto fail;
      }
      shape = nNet_RemoveTensorShapeW(interpreter, shape);
    }
  }

  /* @NOTE: build filter's results */
  if (index < 0){
    for (auto i = dimemsion(filter, 2); i < 2*dimemsion(filter, 2); ++i){
      dims[0] = filter_col_count;
      dims[1] = filter_row_count;

      if (renew_result ||result[i]->ndims != 2 || 
          result[i]->dims[0] != filter_col_count || 
          result[i]->dims[1] != filter_row_count){
        if (!(shape = nNet_CreateTensorShapeC2(interpreter, 2, dims))){
          BadAccess << "nNet_CreateTensorShapeC2";
          goto fail;
        } else {
          if (!(result[i] = nNet_CreateTensorC(interpreter, shape))){
            BadAccess << "nNet_CreateTensorC";
            goto fail;
          }

          if (!result[i] || !nNet_SetTensorNumTW(interpreter, result[i], filter->type)){
            BadAccess << "nNet_SetTensorNumTW";
            goto fail;
          }
          shape = nNet_RemoveTensorShapeW(interpreter, shape);
        }
      }
    }
  } else {
    dims[0] = filter_col_count;
    dims[1] = filter_row_count;

    if (!(shape = nNet_CreateTensorShapeC2(interpreter, 2, dims))){
      BadAccess << "nNet_CreateTensorShapeC2";
      goto fail;
    } else {
      if (!(result[1] = nNet_CreateTensorC(interpreter, shape))){
        BadAccess << "nNet_CreateTensorC";
        goto fail;
      }

      if (!result[1] || !nNet_SetTensorNumTW(interpreter, result[1], filter->type)){
        BadAccess << "nNet_SetTensorNumTW";
        goto fail;
      }
      shape = nNet_RemoveTensorShapeW(interpreter, shape);
    }
  }

  if (out_arrays){
    Type* in_array  = (Type*)nNet_TrimArrayData(filter);

    int ic_shift = filter_H*filter_W;
    int oc_shift = filter_iC*ic_shift;

    /* @NOTE: convert filter -> result[1] */

    if (index < 0){
      for (auto ic = 0; ic < filter_iC; ++ic){
        Type* out_array = (Type*)nNet_GetArrayData(result[dimemsion(filter, 2) + ic]);
        int   begin     = dimemsion(filter, 2);

        if (!out_array){
          BadAccess << "nNet_GetArrayData";
          goto fail;
        }

        for (auto idx = 0; idx < filter_H*filter_W*filter_oC; ++idx){
          out_array[idx] = in_array[idx];
        }

        nNet_TryDropingArray(filter, in_array);
        nNet_SetArrayData(result[dimemsion(filter, 2) + ic], out_array);
      }
    } else {
      Type* out_array = (Type*)nNet_GetArrayData(result[1]);
      int   begin     = dimemsion(filter, 2);

      if (!out_array){
        BadAccess << "nNet_GetArrayData";
        goto fail;
      }

      for (auto idx = 0; idx < filter_H*filter_W*filter_oC; ++idx){
        out_array[idx] = in_array[idx];
      }

      nNet_TryDropingArray(filter, in_array);
      nNet_SetArrayData(result[1], out_array);
    }
  }

  if (out_arrays){
    Type* in_array = (Type*)nNet_TrimArrayData(input);
    int padding_H  = computePadding(strides[0], dilations[0], input_H, 
                                      filter_H, output_H);
    int padding_W  = computePadding(strides[1], dilations[1], input_W, 
                                      filter_W, output_W);
    int shift_N = input_H*input_H*input_C;
    int shift_H = input_W*input_C;
    int shift_W = input_C;
    int shift_C = 1;
    int col_idx = 0;

    status = true;

    if (index < 0){
      for (int idx = 0; idx < dimemsion(filter, 2); ++idx){
        out_arrays[idx] = (Type*)nNet_GetArrayData(result[idx]);

        if (!out_arrays[idx]){
          BadAccess << "nNet_GetArrayData";
          break;
        }
        memset(out_arrays[idx], 0, sizeof(Type)*nNet_GetArraySizeT(result[idx]));
      }
    } else {
      out_arrays[0] = (Type*)nNet_GetArrayData(result[0]);

      if (!out_arrays[0]){
        BadAccess << "nNet_GetArrayData";
        status = false;
      }
    }

    /* @NOTE: convert input -> result[1] */

    if (status) {
      for (int out_h = 0; out_h < output_H; ++out_h){
        for (int out_w = 0; out_w < output_W; ++out_w){
          int origin_in_x = out_h*strides[0] - padding_H;
          int origin_in_y = out_w*strides[1] - padding_W;

          for (int filt_h = 0, idx_h = (out_h*strides[0] - padding_H)*shift_H; 
               filt_h < filter_H;
               filt_h += strides[0], idx_h += dilations[0]*shift_H){
            for (int filt_w = 0, idx_w = (out_w*strides[1] - padding_W)*shift_W;
                 filt_w < filter_W;
                 filt_w += strides[1], idx_w += dilations[1]*shift_W){
              int x = origin_in_x + filt_h*dilations[0];
              int y = origin_in_y + filt_w*dilations[1];

              if (x >= 0 && y >= 0 && x < input_H && y < input_W){
                for (int n = 0, idx_n = 0; n < input_N; ++n, idx_n += shift_H){
                  if (index < 0){
                    for (int c = 0; c < input_C; ++c){
                      out_arrays[c][idx_n + col_idx] =
                        in_array[n*shift_N + idx_h + idx_w + c*shift_C];
                    }
                  } else {
                    out_arrays[0][idx_n + col_idx] =
                      in_array[n*shift_N + idx_h + idx_w + index*shift_C];
                  }
                }
              }

              col_idx++;
            }
          }
        }
      }
    }

    status = true;
    nNet_TryDropingArray(input, in_array);
    free(out_arrays);
    out_arrays = nullptr;
  }

  if (status)
    return true;

 fail:
  if (result && renew_result){
    if (index < 0){
      for (int i = 0; i < 2*dimemsion(filter, 2); ++i){
        if (result[i])
          result[i] = nNet_RemoveTensorW(interpreter, result[i]);
      }

    } else {
      nNet_RemoveTensorW(interpreter, result[0]);
    }

    free(result);
    result = nullptr;
  }

  if (shape)
    nNet_RemoveTensorShapeW(interpreter, shape);

  if (out_arrays)
    free(out_arrays);
  return false;
}
} // namespace nhwc

namespace nchw{
template<typename Type>
bool im2col(Interpreter interpreter,
            Tensor input, Tensor filter, Tensor* &result,
            std::vector<int> strides, std::vector<int> dilations,
            std::string padding, int index = -1){
  using namespace nn::tensor;
  using namespace nn::generic::math;

  if (!filter || !input) return false;
  Type** out_arrays = (Type**)malloc(dimemsion(filter, 2)*sizeof(Type*));

  Shape shape{nullptr};
  int   dims[2], fixed = padding != "SAME";

  int filter_H  = dimemsion(filter, 0);
  int filter_W  = dimemsion(filter, 1); 
  int filter_iC = dimemsion(filter, 2);
  int filter_oC = dimemsion(filter, 3);

  int input_N = dimemsion(input, 0);
  int input_C = dimemsion(input, 1);
  int input_H = dimemsion(input, 2); 
  int input_W = dimemsion(input, 3);

  int effective_filter_W = (filter_W - 1) * dilations[1];
  int effective_filter_H = (filter_H - 1) * dilations[0];
  int output_H = (input_W - fixed*effective_filter_W + strides[1] - 1)/strides[1];
  int output_W = (input_H - fixed*effective_filter_H + strides[0] - 1)/strides[0];

  int filter_col_count = dimemsion(filter, 0)*dimemsion(filter, 1);
  int filter_row_count = dimemsion(filter, 3);

  int input_col_count = dimemsion(filter, 0)*dimemsion(filter, 1);
  int input_row_count = output_H*output_W*dimemsion(filter, 3);
  bool renew_result = result == nullptr;

  if (input_col_count * input_row_count >= nNet_MaxPhysicalMemory()){
    return !(DrainMem << "you require (" 
                      << std::to_string(input_col_count * input_row_count) << ")"
                      << "more than" 
                      << std::to_string(nNet_MaxPhysicalMemory()));
  } else if (filter_row_count * filter_col_count >= nNet_MaxPhysicalMemory()){
    return !(DrainMem << "you require (" 
                      << std::to_string(filter_row_count * filter_col_count) << ")"
                      << "more than" 
                      << std::to_string(nNet_MaxPhysicalMemory()));
  }

  if (index < 0)
    out_arrays = (Type**)malloc(dimemsion(filter, 2)*sizeof(Type*));
  else if (index < dimemsion(filter, 2))
    out_arrays = (Type**)malloc(sizeof(Type*));
  else
    return false;

  if (!result){
    if (index < 0)
      result = (Tensor*)malloc((2*dimemsion(filter, 2))*sizeof(Tensor));
    else
      result = (Tensor*)malloc(2*sizeof(Tensor));
  }

  if (nullptr == result)
    goto fail;

  /* @NOTE: build input's results */
  if (index < 0){
    for (auto i = 0; i < dimemsion(filter, 2); ++i){
      dims[0] = input_col_count;
      dims[1] = input_row_count;

      if (renew_result || result[i]->ndims != 2 || 
          result[i]->dims[0] != input_col_count || 
          result[i]->dims[1] != input_row_count){
        if (!(shape = nNet_CreateTensorShapeC2(interpreter, 2, dims)))
          goto fail;
        else {
          if (!(result[i] = nNet_CreateTensorC(interpreter, shape))){
            goto fail;
          }

          if (!result[i] || !nNet_SetTensorNumTW(interpreter, result[i], filter->type)){
            goto fail;
          }
          shape = nNet_RemoveTensorShapeW(interpreter, shape);
        }
      }
    }
  } else {
    dims[0] = input_col_count;
    dims[1] = input_row_count;

    if (!(shape = nNet_CreateTensorShapeC2(interpreter, 2, dims)))
      goto fail;
    else {
      if (!(result[0] = nNet_CreateTensorC(interpreter, shape))){
        goto fail;
      }

      if (!result[0] || !nNet_SetTensorNumTW(interpreter, result[0], filter->type)){
        goto fail;
      }
      shape = nNet_RemoveTensorShapeW(interpreter, shape);
    }
  }

  /* @NOTE: build filter's results */
  if (index < 0){
    for (auto i = dimemsion(filter, 2); i < 2*dimemsion(filter, 2); ++i){
      dims[0] = filter_col_count;
      dims[1] = filter_row_count;

      if (renew_result ||result[i]->ndims != 2 || 
          result[i]->dims[0] != filter_col_count || 
          result[i]->dims[1] != filter_row_count){
        if (!(shape = nNet_CreateTensorShapeC2(interpreter, 2, dims)))
          goto fail;
        else {
          if (!(result[i] = nNet_CreateTensorC(interpreter, shape))){
            goto fail;
          }

          if (!result[i] || !nNet_SetTensorNumTW(interpreter, result[i], filter->type)){
            goto fail;
          }
          shape = nNet_RemoveTensorShapeW(interpreter, shape);
        }
      }
    }
  } else {
    dims[0] = filter_col_count;
    dims[1] = filter_row_count;

    if (!(shape = nNet_CreateTensorShapeC2(interpreter, 2, dims)))
      goto fail;
    else {
      if (!(result[1] = nNet_CreateTensorC(interpreter, shape))){
        goto fail;
      }

      if (!result[1] || !nNet_SetTensorNumTW(interpreter, result[1], filter->type)){
        goto fail;
      }
      shape = nNet_RemoveTensorShapeW(interpreter, shape);
    }
  }

  if (out_arrays){
    Type* in_array  = (Type*)nNet_TrimArrayData(filter);

    int ic_shift = filter_H*filter_W;
    int oc_shift = filter_iC*ic_shift;

    /* @NOTE: convert filter -> result[1] */
    if (index < 0){
      for (auto ic = 0; ic < filter_iC; ++ic){
        Type* out_array = (Type*)nNet_GetArrayData(result[dimemsion(filter, 2) + ic]);
        int   begin     = dimemsion(filter, 2);

        if (!out_array)
          goto fail;

        for (auto idx = 0; idx < filter_H*filter_W*filter_oC; ++idx){
          out_array[idx] = in_array[idx];
        }

        nNet_TryDropingArray(filter, in_array);
        nNet_SetArrayData(result[dimemsion(filter, 2) + ic], out_array);
      }
    } else {
      Type* out_array = (Type*)nNet_GetArrayData(result[1]);
      int   begin     = dimemsion(filter, 2);

      if (!out_array)
        goto fail;

      for (auto idx = 0; idx < filter_H*filter_W*filter_oC; ++idx){
        out_array[idx] = in_array[idx];
      }

      nNet_TryDropingArray(filter, in_array);
      nNet_SetArrayData(result[1], out_array);
    }
  } else
    goto fail;

  if (out_arrays){
    Type* in_array   = (Type*)nNet_TrimArrayData(input);
    Type* out_array  = (Type*)nNet_GetArrayData(result[0]);
    int   padding_H  = computePadding(strides[0], dilations[0], input_H, 
                                      filter_H, output_H);
    int   padding_W  = computePadding(strides[1], dilations[1], input_W, 
                                      filter_W, output_W);
    int  shift_N = input_H*input_H*input_C;
    int  shift_C = input_H*input_W;
    int  shift_H = input_W;
    int  shift_W = 1;
    int  col_idx = 0;
    bool status = true;

    if (index < 0){
      for (int idx = 0; idx < dimemsion(filter, 2); ++idx){
        out_arrays[idx] = (Type*)nNet_GetArrayData(result[idx]);

        if (out_arrays[idx])
          memset(out_arrays[idx], 0, sizeof(Type)*nNet_GetArraySizeT(result[idx]));
        else
          status = false;
      }
    } else {
      out_arrays[0] = (Type*)nNet_GetArrayData(result[index]);

      if (out_arrays[0])
        memset(out_arrays[0], 0, sizeof(Type)*nNet_GetArraySizeT(result[index]));
      else
        status = false;
    }

    /* @NOTE: convert input -> result[1] */
    if (status){
      for (int out_h = 0; out_h < output_H; ++out_h){
        for (int out_w = 0; out_w < output_W; ++out_w){
          int origin_in_x = out_h*strides[0] - padding_H;
          int origin_in_y = out_w*strides[1] - padding_W;

          for (int filt_h = 0, idx_h = (out_h*strides[0] - padding_H)*shift_H; 
               filt_h < filter_H;
               filt_h += strides[0], idx_h += dilations[0]*shift_H){
            for (int filt_w = 0, idx_w = (out_w*strides[1] - padding_W)*shift_W;
                 filt_w < filter_W;
                 filt_w += strides[1], idx_w += dilations[1]*shift_W){
              int x = origin_in_x + filt_h*dilations[0];
              int y = origin_in_y + filt_w*dilations[1];

              if (x >= 0 && y >= 0 && x < input_H && y < input_W){
                for (int n = 0, idx_n = 0; n < input_N; ++n, idx_n += shift_C){
                  if (index < 0){
                    for (int c = 0; c < input_C; ++c){
                      out_arrays[c][idx_n + col_idx] =
                        in_array[n*shift_N + idx_h + idx_w + c*shift_C];
                    }
                  } else {
                    out_arrays[0][idx_n + col_idx] =
                        in_array[n*shift_N + idx_h + idx_w + index*shift_C];
                  }
                }
              }
              col_idx++;
            }
          }
        }
      }
    }

    free(out_arrays);
    out_arrays = nullptr;

    if (!status) goto fail;
  } else
    goto fail;

  return true;
 fail:
  if (result && renew_result){
    if (index < 0){
      for (int i = 0; i < 2*dimemsion(filter, 2); ++i){
        if (result[i])
          result[i] = nNet_RemoveTensorW(interpreter, result[i]);
      }

    } else {
      nNet_RemoveTensorW(interpreter, result[0]);
    }

    free(result);
    result = nullptr;
  }

  if (shape)
    nNet_RemoveTensorShapeW(interpreter, shape);

  if (out_arrays)
    free(out_arrays);
  return false;
}
} // namespace nchw
} // namespace conv2d
} // namespace math
} // namespace generic
} // namespace nn
#endif  // LIBNN_DEVICE_GENERIC_CONV2D_IM2COL_H_
