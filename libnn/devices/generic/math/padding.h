#ifndef LIBNN_GENERIC_PADDING_H_
#define LIBNN_GENERIC_PADDING_H_

namespace nn{
namespace generic{
namespace math{
inline int computePadding(int stride, int dilation_rate, int in_size,
                   int filter_size, int out_size) {
  int effective_filter_size = (filter_size - 1)*dilation_rate + 1;
  int padding = ((out_size - 1)*stride + effective_filter_size - in_size)/2;

  return padding > 0 ? padding : 0;
}
} // namespace math
} // namespace generic
} // namespace nn
#endif  // LIBNN_GENERIC_PADDING_H_