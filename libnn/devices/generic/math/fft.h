#ifndef LIBNN_GENERIC_FFT_H_
#define LIBNN_GENERIC_FFT_H_

/* @NOTE: Declarations for the third party 1D FFT lib which has been promoted
 * from http://www.kurims.kyoto-u.ac.jp/~ooura/fft.tgz 
 */

#if __cplusplus
extern "C"{
#endif
void cdft(int n, int isgn, double *a, int *ip, double *w);
void rdft(int n, int isgn, double *a, int *ip, double *w);
void ddct(int n, int isgn, double *a, int *ip, double *w);
void ddst(int n, int isgn, double *a, int *ip, double *w);
void dfct(int n, double *a, double *t, int *ip, double *w);
void dfst(int n, double *a, double *t, int *ip, double *w);
#if __cplusplus
}
#endif
#endif  // LIBNN_GENERIC_FFT_H_