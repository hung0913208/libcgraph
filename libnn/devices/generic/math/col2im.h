#ifndef LIBNN_GENERIC_CONV2D_COL2IM_H_
#define LIBNN_GENERIC_CONV2D_COL2IM_H_
#ifdef LIBCGRAPH_GIT_REFSPEC
#include <libbase/all.hpp>
#include <libnn/utils.h>
#include <libnn/tensor.h>
#include <libnn/interpreter.h>
#else
#include "utils.h"
#include "tensor.h"
#include "interpreter.h"
#endif

#include "padding.h"

#if _OPENMP
#include <omp.h>
#endif

namespace nn{
namespace generic {
namespace math{
namespace conv2d {
namespace nhwc{
template<typename Type>
bool col2im(Interpreter interpreter, Tensor* columns, Tensor output){
  using namespace nn::tensor;
  using namespace nn::generic::math;

  bool result = true;
  int N = dimemsion(output, 0);
  int H = dimemsion(output, 1);
  int W = dimemsion(output, 2);
  int C = dimemsion(output, 3);

  int shift_N = H*W*C;
  int shift_H = W*C;
  int shift_W = C;
  int shift_C = 1;

  Type*  out_array  = (Type*)nNet_GetArrayData(output);
  Type** col_arrays = (Type**)malloc(size_t(C*sizeof(Type*)));

  for (auto c = 0; c < C; ++c){
    col_arrays[c] = (Type*)nNet_GetArrayData(columns[c]);

    if (!(result = col_arrays[c] != nullptr))
      goto finish;
  }

  for (auto n = 0; n < N; ++n){
    int i_idx = 0, o_idx = 0;
    int n_idx = n*shift_N;

    for (auto h = 0; h < H; ++h){
      for (auto w = 0; w < W; ++w){
        auto fixed = n_idx + h*shift_H + w*shift_W;

        for (auto c = 0; c < C; ++c){
          out_array[fixed + c] = col_arrays[c][i_idx];
        }
        i_idx++;
      }
    }
  }

 finish:
  for (auto c = 0; c < C; ++c){
    nNet_TryDropingArray(columns[c], col_arrays[c]);
    nNet_RemoveTensorW(interpreter, columns[c]);
  }

  if (result)
    nNet_SetArrayData(output, out_array);
  return result;
}
} // namespace nhwc

namespace nchw{
template<typename Type>
bool col2im(Interpreter interpreter, Tensor* columns, Tensor output){
  using namespace nn::tensor;
  using namespace nn::generic::math;

  int N = dimemsion(output, 0);
  int C = dimemsion(output, 1);
  int H = dimemsion(output, 2);
  int W = dimemsion(output, 3);

  int shift_N = H*W*C;
  int shift_C = H*W;
  int shift_H = W;
  int shift_W = 1;

  Type* out_array = (Type*)nNet_GetArrayData(output);

  for (auto c = 0; c < C; ++c){
    Type* col_array = (Type*)nNet_GetArrayData(columns[c]);
    int i_idx = 0;

    if (!col_array) return false;
    for (auto n = 0; n < N; ++n){
      auto nc_fixed = n*shift_N + c*shift_C;
      auto o_idx = 0;

      for (auto h = 0; h < H; ++h){
        for (auto w = 0; w < W; ++w){
          out_array[nc_fixed + o_idx] = col_array[i_idx];
          i_idx++;
          o_idx++;
        }
      }
    }
    nNet_RemoveTensorW(interpreter, columns[c]);
  }

  nNet_SetArrayData(output, out_array);
  return true;
}
} // namespace nchw
} // namespace conv2d
} // namespace math
} // namespace generic
} // namespace nn
#endif  // LIBNN_GENERIC_CONV2D_COL2IM_H_
