#include "devices/opencl/opencl.h"

namespace nn {
namespace device {
namespace opencl {
std::string explainWhyCreateContextFail(cl_int cl_error) {
  switch (cl_error) {
    case CL_INVALID_PLATFORM:
      return "properties is NULL and no platform could be selected or if "
             "platform value specified in properties is not a valid platform.";

    case CL_INVALID_VALUE:
      return "context property name in properties is not a supported property "
             "name or if pfn_notify is NULL but user_data is not NULL";

    case CL_DEVICE_NOT_AVAILABLE:
      return "no devices that match device_type are currently available.";

    case CL_DEVICE_NOT_FOUND:
      return "no devices that match device_type were found.";

    case CL_OUT_OF_HOST_MEMORY:
      return "there is a failure to allocate resources required by the OpenCL "
             "implementation on the host.";

    case CL_INVALID_DEVICE_TYPE:
      return "device_type is not a valid value.";

    default:
      return "";
  }
}

std::string explainWhyGetPlatformInfoFail(cl_int cl_error) {
  switch (cl_error) {
    case CL_INVALID_PLATFORM:
      return "platform is not a valid platform.";

    case CL_INVALID_VALUE:
      return "param_name is not one of the supported values or if size in "
             "bytes "
             "specified by param_value_size is less than size of return type "
             "and "
             "param_value is not a NULL value.";
    default:
      return "";
  }
}

std::string explainWhyCreateCommandQueueFail(cl_int cl_error) {
  switch (cl_error) {
    case CL_INVALID_CONTEXT:
      return "context is not a valid context.";

    case CL_INVALID_DEVICE:
      return "device is not a valid device or is not associated with context.";

    case CL_INVALID_VALUE:
      return "values specified in properties are not valid.";

    case CL_INVALID_QUEUE_PROPERTIES:
      return "values specified in properties are valid but are not supported "
             "by the device.";

    case CL_OUT_OF_RESOURCES:
      return "there is a failure to allocate resources required by the OpenCL "
             "implementation on the device.";

    case CL_OUT_OF_HOST_MEMORY:
      return "there is a failure to allocate resources required by the OpenCL "
             "implementation on the host.";

    default:
      return "";
  }
}

std::string explainWhyCreateProgramFail(cl_int cl_error) {
  switch (cl_error) {
    case CL_INVALID_CONTEXT:
      return "context is not a valid context.";

    case CL_INVALID_VALUE:
      return "device_list is NULL or num_devices is zero; or if lengths or "
             "binaries are NULL or if any entry in lengths[i] or binaries[i] "
             "is NULL.";

    case CL_INVALID_DEVICE:
      return "OpenCL devices listed in device_list are not in the list "
             "of devices associated with context.";

    case CL_INVALID_BINARY:
      return "an invalid program binary was encountered for any device. "
             "binary_status will return specific status for each device.";

    case CL_OUT_OF_HOST_MEMORY:
      return "there is a failure to allocate resources required by the OpenCL "
             "implementation on the host.";

    default:
      return "";
  }
}

std::string explainWhyBuildProgramFail(cl_int cl_error) {
  switch (cl_error) {
    case CL_INVALID_PROGRAM:
      return "program is not a valid program object.";

    case CL_INVALID_VALUE:
      return "device_list is NULL and num_devices is greater than zero or "
             "device_list is not NULL and num_devices is zero or pfn_notify is "
             "NULL but user_data is not NULL";

    case CL_INVALID_DEVICE:
      return "OpenCL devices listed in device_list are not in the list of "
             "devices associated with program.";

    case CL_INVALID_BINARY:
      return "program is created with clCreateWithProgramWithBinary and "
             "devices "
             "listed in device_list do not have a valid program binary loaded.";

    case CL_INVALID_BUILD_OPTIONS:
      return "the build options specified by options are invalid.";

    case CL_INVALID_OPERATION:
      return "the build of a program executable for any of the devices listed "
             "in device_list by a previous call to clBuildProgram for program "
             "has not completed.";

    case CL_COMPILER_NOT_AVAILABLE:
      return "program is created with clCreateProgramWithSource and a compiler "
             "is not available i.e. CL_DEVICE_COMPILER_AVAILABLE specified in "
             "the table of OpenCL Device Queries for clGetDeviceInfo is set to "
             "CL_FALSE.";

    case CL_BUILD_PROGRAM_FAILURE:
      return "there is a failure to build the program executable. This error "
             "will be returned if clBuildProgram does not return until the "
             "build has completed.";

    case CL_OUT_OF_HOST_MEMORY:
      return "there is a failure to allocate resources required by the OpenCL "
             "implementation on the host.";

    default:
      return "";
  }
}

std::string explainWhyCreateKernelFail(cl_int cl_error) {
  switch (cl_error) {
    case CL_INVALID_PROGRAM:
      return "program is not a valid program object.";

    case CL_INVALID_PROGRAM_EXECUTABLE:
      return "there is no successfully built executable for program.";

    case CL_INVALID_KERNEL_NAME:
      return "kernel_name is not found in program.";

    case CL_INVALID_KERNEL_DEFINITION:
      return "the function definition for __kernel function given by "
             "kernel_name such as the number of arguments, the argument types "
             "are not the same for all devices for which the program "
             "executable has been built.";

    case CL_INVALID_VALUE:
      return "kernel_name is NULL.";

    case CL_OUT_OF_HOST_MEMORY:
      return "there is a failure to allocate resources required by the OpenCL "
             "implementation on the host.";

    default:
      return "";
  }
}

std::string explainWhySetKernelArgFail(cl_int cl_error) {
  switch (cl_error) {
    case CL_INVALID_KERNEL:
      return "kernel is not a valid kernel object.";

    case CL_INVALID_ARG_INDEX:
      return "arg_index is not a valid argument index.";

    case CL_INVALID_ARG_VALUE:
      return "arg_value specified is NULL for an argument that is not declared "
             "with the __local qualifier or vice-versa.";

    case CL_INVALID_MEM_OBJECT:
      return "an argument declared to be a memory object when the specified "
             "arg_value is not a valid memory object.";

    case CL_INVALID_SAMPLER:
      return " an argument declared to be of type sampler_t when the specified "
             "arg_value is not a valid sampler object.";

    case CL_INVALID_ARG_SIZE:
      return "arg_size does not match the size of the data type for an "
             "argument that is not a memory object or if the argument is a "
             "memory object and arg_size != sizeof(cl_mem) or if arg_size is "
             "zero and the argument is declared with the __local qualifier or "
             "if the argument is a sampler and arg_size != sizeof(cl_sampler).";

    default:
      return "";
  }
}

std::string CLConsumer::readable(std::string method, std::string where,
                                 cl_int code) {
  std::string reason = "";

  if (method == "CLConsumer") {
    /* @NOTE: return reason to explain why CLConsumer is fail */

    if (where == "clGetPlatformInfo")
      reason = explainWhyGetPlatformInfoFail(code);
    else if (where.compare(0, sizeof("clCreateCommandQueue") - 1,
                           "clCreateCommandQueue"))
      reason = explainWhyCreateCommandQueueFail(code);
    else if (where.compare(0, sizeof("clCreateContext") - 1, "clCreateContext"))
      reason = explainWhyCreateContextFail(code);
  } else if (method == "install") {
    /* @NOTE: return reason to explain why CLConsumer::install is fail */

    if (where == "clCreateProgramWithBinary")
      reason = explainWhyCreateProgramFail(code);
  } else if (method == "pipeline") {
    /* @NOTE: return reason to explain why CLConsumer::pipeline is fail */

    if (where == "clBuildProgram")
      reason = explainWhyBuildProgramFail(code);
    else if (where == "clCreateKernel")
      reason = explainWhyCreateKernelFail(code);
  } else if (method == "put") {
    /* @NOTE: return reason to explain why putting data to kernel is fail */

    if (where == "clSetKernelArg") reason = explainWhySetKernelArgFail(code);
  }

  return where + (reason.size() ? ": " + reason : "");
}

std::string CLConsumer::readable(std::string method, std::string where,
                                 cl_int cl_error, std::string errlogs) {
  if (method == "pipeline") {
    return readable(method, where, cl_error) + "\nBuild log error:\n\n" +
           errlogs;
  } else
    return readable(method, where, cl_error);
}
}  // namespace opencl
}  // namespace device
}  // namespace nn
