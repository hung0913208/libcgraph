#ifdef LIBCGRAPH_GIT_REFSPEC
#include "devices/generic/generic.h"
#include "devices/opencl/opencl.h"
#include "devices/opencl/opencl_macros.h"
#else
#include "generic.h"
#include "opencl.h"
#include "opencl_macros.h"
#endif

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
namespace kernels{
namespace opencl{
DEFINE_EVAL(softmax);
DEFINE_DERIVE(softmax);

SELECT(softmax){ return { "Buffer_Softmax_Forward" }; }
MATRIX(softmax){ 
  auto params = { std::get<1>(opw->inputs()[0]), opw->outputs()[0] };

  char  *source, *cltemp, *left{nullptr}, *right{nullptr};
  size_t size;

  std::string prefix;
  if (function == "Buffer_Softmax_Forward"){
    cltemp = const_cast<char*>(
      "__kernel void %s_Buffer_Softmax_Forward(__global const %s* inputs,"
                                                     "uint        max_input,"
                                                     "__global %s* outputs)"
      "{"
      "  size_t gid = get_global_id(0);"
      "  %s Z = 0;"
      ""
      "  for (int i = 0; i < max_input; ++i)"
      "    Z = Z + exp(inputs[i]);"
      "  outputs[gid] = exp(inputs[gid])/Z;"
      "}");
  }

  for (auto param: params){
    switch (nNet_GetTensorNumT(param)){
    case Int32_t:
      prefix += "I";

      if (left)
        right = const_cast<char*>("int");
      else
        left = const_cast<char*>("int");
      break;

    case Real32_t:
      prefix += "R";

      if (left)
        right = const_cast<char*>("float");
      else
        left = const_cast<char*>("float");
      break;

    default:
      break;
    }
  }
  prefix += '\0';

  size = snprintf(nullptr, 0, cltemp, prefix.c_str(), left, right, right);

  if (size == 0)
    throw BadAccess.reason("snprintf");
  else if (!(source = (char*)malloc(size + 1))){
    throw DrainMem << "can\'t allocate a array with size" 
                   << std::to_string(size + 1);
  } else {
    snprintf(source, size + 1, cltemp, prefix.c_str(), left, right, right);
  }

  prefix.resize(prefix.size() - 1);
  return std::make_pair(prefix, std::vector<CString>{source});
}

PERFORM(softmax){
  using namespace nn::device::opencl;
  using namespace nn::interpreter;
  using namespace nn::tensor;

 CREATE_COMMAND_ENCODER()
  auto  type   = TensorW::Unknown;

  if (function == "Buffer_Softmax_Forward"){
    Tensor output = opw->outputs()[0];
    Tensor input  = std::get<1>(opw->inputs()[0]);

    type = TensorW::BufferT;

    PUT_BUFFER_INPUT(0, input);
    PUT_BUFFER_OUTPUT(2, output);
    PUT_VALUE(1, nNet_GetArraySizeT(output))
  } else return NoSupport;

 FINISH_CONFIGURE_ENCODER(type, 0, 0, 0)
  return NoSupport;
}

Registration doRegisterSoftmax(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = kernels::generic::softmax::Prepare;
  result.eval   = softmax::Eval;
  result.derive = softmax::Derive;
  return result;
}
} // namespace opencl
} // namespace kernels
} // namespace nn
