#if !defined(LIBNN_DEVICE_OPENCL_MACORS_H_) && __cplusplus
#define LIBNN_DEVICE_OPENCL_MACORS_H_
#include <libbase/macros.hpp>
#include <signal.h>

#if APPLE
#include <OpenCL/opencl.h>
#import <Foundation/Foundation.h> 
#else
#include <CL/opencl.h>
#endif

#include "devices/opencl/opencl.h"
#include "interpreter.h"
#include "internal.h"
#include "tensor.h"

namespace nn{
namespace device{
namespace opencl{
namespace helper{
base::Error putBufferInput(CLConsumer* consumer,
                           cl_kernel kernel, cl_context context,
                           int index, Tensor tensor);
base::Error putBufferOutput(CLConsumer* consumer, cl_kernel kernel,
                            int index, Tensor tensor);

template<typename Type>
base::Error putValue(CLConsumer* consumer, cl_kernel kernel, cl_context context,
                     int index, Type&& value){
  auto clbuffer = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(Type),
                                 nullptr, nullptr);
  auto code     = 0;

  if (!clbuffer) {
    return DrainMem << "clCreateBuffer() at index" << std::to_string(index);
  }

  code = clEnqueueWriteBuffer(consumer->queue(), clbuffer, CL_TRUE, 0,
                              sizeof(Type), &value, 0, nullptr, nullptr);
  if (code) {
    return BadAccess << "clEnqueueWriteBuffer() at index"
                     << std::to_string(index);
  }

  code = clSetKernelArg(kernel, index, sizeof(cl_mem), clbuffer);
  if (code) {
    return BadAccess << "clSetKernelArg() at index" << std::to_string(index);
  }
  return NoError;
}

template<typename Type>
base::Error putValue(CLConsumer* consumer, cl_kernel kernel, cl_context context,
                     int index, Type& value){
  auto clbuffer = clCreateBuffer(context, CL_MEM_READ_ONLY, sizeof(Type),
                                 nullptr, nullptr);
  auto code     = 0;

  if (!clbuffer) {
    return DrainMem << "clCreateBuffer() at index" << std::to_string(index);
  }

  code = clEnqueueWriteBuffer(consumer->queue(), clbuffer, CL_TRUE, 0,
                              sizeof(Type), &value, 0, nullptr, nullptr);
  if (code) {
    return BadAccess << "clEnqueueWriteBuffer() at index"
                     << std::to_string(index);
  }

  code = clSetKernelArg(kernel, index, sizeof(cl_mem), clbuffer);
  if (code) {
    return BadAccess << "clSetKernelArg() at index" << std::to_string(index);
  }
}

template <typename Type>
base::Error putArray(CLConsumer* consumer, cl_kernel kernel, cl_context context,
                     int index, Type* array, int size) {
  auto clbuffer = clCreateBuffer(context, CL_MEM_READ_ONLY, size, 
                                 nullptr, nullptr);
  auto code     = 0;

  if (!clbuffer) {
    return DrainMem << "clCreateBuffer() at index" << std::to_string(index);
  }

  code = clEnqueueWriteBuffer(consumer->queue(), clbuffer, CL_TRUE, 0, size,
                              array, 0, nullptr, nullptr);
  if (code) {
    return BadAccess << "clEnqueueWriteBuffer() at index"
                     << std::to_string(index);
  }

  code = clSetKernelArg(kernel, index, sizeof(cl_mem), clbuffer);
  if (code) {
    return BadAccess << "clSetKernelArg() at index" << std::to_string(index);
  }
}
} // namespace helper
} // namesapce opencl
} // namespace device
} // namesapce nn

using Commands = std::vector<std::string>; 
using Source   = std::pair<std::string, std::vector<CString>>;

#define SELECT(opname)                                                         \
  Commands opname::selector::opname(                                           \
      device::opencl::CLConsumer* UNUSED(consumer),                            \
      nn::interpreter::OperatorW* UNUSED(opw),                                 \
      std::vector<std::pair<CString, Tensor>>& UNUSED(inputs))

#define PERFORM(opname)                                                        \
  base::Error opname::perform::opname(std::string& UNUSED(function),           \
                              nn::device::opencl::CLConsumer* consumer,        \
                              nn::interpreter::OperatorW* opw,                 \
                              cl_kernel kernel)

#define MATRIX(opname)                                                         \
  Source opname::matrix::opname(std::string& UNUSED(function),                 \
                                nn::interpreter::OperatorW* UNUSED(opw))

#define DEFINE_EVAL(opname)                                                    \
  namespace opname {                                                           \
  namespace selector {                                                         \
  Commands opname(nn::device::opencl::CLConsumer* consumer,                    \
                  nn::interpreter::OperatorW* UNUSED(opw),                     \
                  std::vector<std::pair<CString, Tensor>>& inputs);            \
  }                                                                            \
                                                                               \
  namespace perform {                                                          \
  base::Error opname(std::string& function,                                    \
                     nn::device::opencl::CLConsumer* consumer,                 \
                     nn::interpreter::OperatorW* opw, cl_kernel kernel);       \
  }                                                                            \
                                                                               \
  namespace matrix {                                                           \
  Source opname(std::string& function, nn::interpreter::OperatorW* opw);       \
  }                                                                            \
                                                                               \
  bool Eval(Operator op, Interpreter interpreter) {                            \
    using namespace nn::kernels::opencl::opname;                               \
    using DeviceW = nn::interpreter::DeviceW;                                  \
    using Consumer = nn::device::opencl::CLConsumer;                           \
                                                                               \
    auto opw = nNet_GetOperatorW(op);                                          \
    auto consumer = utils::response<DeviceW, Consumer>(op->device->_priv);     \
                                                                               \
    /* @NOTE: check input and alloc output if it needs */                      \
    if (!consumer) {                                                           \
      return opw << (BadLogic << "operator" << op->name << "must be created"   \
                              << "by device \'opencl\'");                      \
    } else if (generic::opname::Check(op, interpreter))                        \
      return true;                                                             \
    else if (interpreter) {                                                    \
      auto commands = selector::opname(consumer, opw, opw->inputs());          \
                                                                               \
      try {                                                                    \
        for (auto& function : commands) {                                      \
          auto error = NoError;                                                \
          auto realfunct = matrix::opname(function, opw);                      \
          auto pipeline = cl_kernel{nullptr};                                  \
                                                                               \
          base::Boundary bound{[]() {},                                        \
                               [&]() {                                         \
                                 for (auto source : std::get<1>(realfunct)) {  \
                                   free(const_cast<char*>(source));            \
                                 }                                             \
                               }};                                             \
                                                                               \
          /* @NOTE: okey create function and perform it now */                 \
          std::get<0>(realfunct) += "_" + function;                            \
                                                                               \
          if (!(pipeline = consumer->pipeline(realfunct)))                     \
            return NotFound.reason(std::string{std::get<0>(realfunct)});       \
          else {                                                               \
            if (!(error = perform::opname(function, consumer, opw, pipeline))) \
              consumer->commit(pipeline);                                      \
            return nNet_GetInterpreterW(interpreter)->error(rvalue(error));    \
          }                                                                    \
        }                                                                      \
      } catch (base::Error & error) {                                          \
        opw << error;                                                          \
      }                                                                        \
    }                                                                          \
    return NoError;                                                            \
  }                                                                            \
  }  // namespace ##opname

#define DEFINE_DERIVE(opname)                                                  \
namespace opname{                                                              \
bool Derive(Operator op, Interpreter interpreter){                             \
  return false;                                                                \
}                                                                              \
} // namesapce ##opname

/* @NOTE: a single pipeline can be use with many more command encoder since this
 * object is used like input and output parameters */
#define CREATE_COMMAND_ENCODER()                                  \
  size_t global_work_count[3], local_work_count[3], work_dim = 1; \
                                                                  \
  auto context = consumer->context();                             \
  auto error   = NoError;

/* @NOTE: by default, OpenCL support maximum 3 dim of workers */
#define FINISH_CONFIGURE_ENCODER(Type, NWorkDim0, NWorkDim1, NWorkDim2)      \
  if (Type == TensorW::BufferT) {                                            \
    auto cl_error = clGetKernelWorkGroupInfo(kernel, consumer->device(),     \
                                             CL_KERNEL_WORK_GROUP_SIZE,      \
                                             sizeof(local_work_count),       \
                                             &local_work_count, nullptr);    \
    if (cl_error != CL_SUCCESS)                                              \
      return BadAccess.reason("Unable to get kernel work-group size");       \
                                                                             \
    if (NWorkDim0 <= 0) {                                                    \
      global_work_count[0] = 0;                                              \
      work_dim = 1;                                                          \
                                                                             \
      for (auto i = 0; i < cast_(i, opw->outputs().size()); ++i) {           \
        auto tensor_size = nNet_GetArraySizeT(opw->outputs()[i]);            \
                                                                             \
        if (tensor_size > cast_(tensor_size, global_work_count[0])) {        \
          global_work_count[0] = tensor_size;                                \
        }                                                                    \
      }                                                                      \
    } else {                                                                 \
      work_dim = NWorkDim0 != 0 + NWorkDim1 && NWorkDim0 + NWorkDim2 &&      \
                 NWorkDim1 && NWorkDim0;                                     \
      global_work_count[0] = NWorkDim0;                                      \
      global_work_count[1] = NWorkDim1;                                      \
      global_work_count[2] = NWorkDim2;                                      \
    }                                                                        \
                                                                             \
    clEnqueueNDRangeKernel(consumer->queue(), kernel, work_dim, nullptr,     \
                           global_work_count, local_work_count, 0, nullptr,  \
                           nullptr);                                         \
  } else                                                                     \
    error = NoSupport.reason(nn::device::opencl::TensorW::nametype(Type));   \
                                                                             \
  return error;

#define PUT_VALUE(index, value)                                      \
  {                                                                  \
    using namespace nn::device::opencl::helper;                      \
    if ((error = putValue(consumer, kernel, context, index, value))) \
      return error;                                                  \
  }

#define PUT_BUFFER_INPUT(index, tensor)                                     \
  {                                                                         \
    using namespace nn::device::opencl::helper;                             \
    if ((error = putBufferInput(consumer, kernel, context, index, tensor))) \
      return error;                                                         \
  }

#define PUT_BUFFER_OUTPUT(index, tensor)                            \
  {                                                                 \
    using namespace nn::device::opencl::helper;                     \
    if ((error = putBufferOutput(consumer, kernel, index, tensor))) \
      return error;                                                 \
  }

#define PUT_ARRAY(index, array, size)                                \
  {                                                                  \
    using namespace nn::device::opencl::helper;                      \
    if ((error = putArray(consumer, kernel, context, index, value))) \
      return error;                                                  \
  }
#endif  // LIBNN_DEVICE_OPENCL_MACORS_H_