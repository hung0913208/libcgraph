#include "devices/opencl/opencl.h"

namespace nn{
namespace device{
namespace opencl{
namespace helper{
base::Error putBufferInput(CLConsumer* consumer, cl_kernel kernel, cl_context context,
                           int index, Tensor tensor) {
  using namespace nn::tensor;

  auto buff = (TensorW*)nullptr;
  auto dev = consumer->device();
  auto code = 0;

  if ((Device(tensor->_device))->level == GENERIC_PLATFORM) {
    auto size = nNet_SizeType(tensor->type) * nNet_GetArraySizeT(tensor);
    auto clbuffer =
        clCreateBuffer(context, CL_MEM_READ_ONLY, size, nullptr, nullptr);

    if (!clbuffer) {
      return DrainMem << "clCreateBuffer() at index" << std::to_string(index);
    }

    code = clEnqueueWriteBuffer(consumer->queue(), clbuffer, CL_TRUE, 0, size,
                                nNet_GetArrayData(tensor), 0, nullptr, nullptr);
    if (code) {
      return BadAccess << "clEnqueueWriteBuffer() at index"
                       << std::to_string(index);
    }

    code = clSetKernelArg(kernel, index, sizeof(cl_mem), clbuffer);
    if (code) {
      return BadAccess << consumer->readable("put", "clSetKernelArg", code) 
                       << "at index" << std::to_string(index);
    }
  } else if ((buff = utils::response<GPContent, TensorW>(tensor->_content))) {
    if ((code = clSetKernelArg(kernel, index, sizeof(cl_mem),
                               buff->cache().get<TensorW::Buffer>()))) {
      return BadAccess << consumer->readable("put", "clSetKernelArg", code) 
                       << "at index" << std::to_string(index);
    }
  } else {
    auto size     = nNet_SizeType(tensor->type) * nNet_GetArraySizeT(tensor);
    auto array    = nNet_GetArrayData(tensor);
    auto clbuffer = clCreateBuffer(context, CL_MEM_READ_ONLY, size, 
                                   nullptr, nullptr);

    base::Boundary bound{[](){}, [&](){ nNet_TryDropingArray(tensor, array); }};

    if (!clbuffer) {
      return DrainMem << "clCreateBuffer() at index" << std::to_string(index);
    }

    code = clEnqueueWriteBuffer(consumer->queue(), clbuffer, CL_TRUE, 0, size,
                                array, 0, nullptr, nullptr);
    if (code) {
      return BadAccess << "clEnqueueWriteBuffer() at index"
                       << std::to_string(index);
    }

    code = clSetKernelArg(kernel, index, sizeof(cl_mem), clbuffer);
    if (code) {
      return BadAccess << consumer->readable("put", "clSetKernelArg", code) 
                       << "at index" << std::to_string(index);
    }
  }

  return NoError;
}

base::Error putBufferOutput(CLConsumer* consumer, cl_kernel kernel,
                            int index, Tensor tensor){
  using namespace nn::tensor;

  auto buff = (TensorW*)nullptr;
  auto code = 0;

  if ((Device(tensor->_device))->level == GENERIC_PLATFORM) {
    return BadAccess.reason("output must be a opencl::TensorW");
  } else if ((buff = utils::response<GPContent, TensorW>(tensor->_content))) {
    if ((code = clSetKernelArg(kernel, index, sizeof(cl_mem),
                               buff->cache().get<TensorW::Buffer>()))) {
      return BadAccess << consumer->readable("put", "clSetKernelArg", code) 
                       << "at index" << std::to_string(index);
    } else
      return NoError;
  } else
    return BadAccess.reason("output must be a opencl::TensorW");
}
} // namespace helper
} // namespace opencl
} // namespace device
} // namespace nn