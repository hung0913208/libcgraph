#include "devices/opencl/opencl.h"

namespace nn {
namespace device {
namespace opencl {
TensorW::~TensorW() {}
TensorW::TensorW(Interpreter interpreter, Tensor tensor, int type)
    : GPContent{interpreter, tensor, true}, _self{tensor} {
  init(nNet_GetArraySizeTC(interpreter, tensor), type, true);
}

bool TensorW::init(long size, int type, bool force) {
  using namespace nn::interpreter;

  auto device   = reinterpret_cast<Device>(_self->_device);
  auto max_size = std::max(nNet_SizeType(_self->type), nNet_SizeType(type));
  auto max_mem  = std::max(size, nNet_GetArraySizeTC(interpreter(), _self));

  if (!(_device = utils::response<DeviceW, CLConsumer>(device->_priv)))
    return false;
  
  /* @NOTE: force == true -> create a new cache */
  if (size == 0) return true;
  if (force) 
    return !make(nullptr, size*nNet_SizeType(type), BufferT);

  /* @NOTE: otherwide, create a new buffer with and copy data from the old cache
   * to the new cache */
  if ((_cache.is(typeid(std::nullptr_t)) && nNet_GetTensorType(_self) != NilT) || 
      type != _self->type || size != nNet_GetArraySizeTC(interpreter(), _self)){
    auto size    = nNet_GetArraySizeTC(interpreter(), _self)*nNet_SizeType(type);
    auto buffer  = clone();
    auto success = false;

    if (buffer){
      success = !make(buffer, size, BufferT);
      free(buffer);
    }
    return success;
  } else
    return !nNet_GetInterpreterW(interpreter())->error(BadLogic);
  return true;
}

Array TensorW::clone(){
  auto cache = (void*)nullptr;
  auto remove_cache = [&](){ if (cache) free(cache); };

  auto ord = nNet_CreateOrdinateC1(interpreter(), _self, 0);
  auto shape = nNet_CreateTensorShapeC1(interpreter(), 0);

  auto data = nNet_GetArrayData(_self);
  auto type = nNet_GetTensorNumT(_self);

  auto dims    = _self->ndims > 0 ? new int[_self->ndims] : nullptr;
  auto dst_idx = 0, src_idx = 0, size = 1;
  auto success = false;

  base::Boundary bound{[]() {}, [&]() {
    if (dims) delete[] dims; 
    if (_self->get) free(data);
  }};

  switch (nNet_GetTensorType(_self)) {
    case NilT:
    default:
      return utils::null(interpreter(), NoSupport);

    case ScalarT:
      if (!(cache = malloc(nNet_SizeType(type)))){
        return utils::null(interpreter(), DrainMem);
      } else if (!nNet_ConvertNumT(data, type, cache, type)){
        return utils::make_null(remove_cache, interpreter(),
                                BadAccess << "nNet_ConvertNumT()");
      }
      return cache;

    case VectorT:
    case MatrixT:
    case TensorT:
      break;
  }

  if (!ord) {
    return utils::make_null(remove_cache, interpreter(), 
                            DrainMem.reason("nNet_CreateOrdinateC1()"));
  }

  if (!shape) {
    return utils::make_null(remove_cache, interpreter(), 
                            DrainMem.reason("nNet_CreateTensorShapeC1()"));
  }

  /* @NOTE: get Tensor's Shape which indicate the real size of this tensor */
  if (!nNet_GetTensorShapeW(interpreter(), _self, shape)) {
    return utils::make_null(remove_cache, interpreter(),
                            BadAccess.reason("nNet_GetTensorShape()"));
  } else {
    for (auto i = 0; i < shape->ndims; i++) {
      size *= shape->dims[i];
    }
  }

  /* @NOTE: allocate cache */
  if (!(cache = malloc(size * nNet_SizeType(type)))) {
    return utils::make_null(remove_cache, interpreter(), DrainMem);
  }

  /* @NOTE: everything is done, now copy data from tensor to cache */
  for (auto i = 0; i < _self->ndims; ++i) {
    dims[i] = 0;
  }
  while (dims[_self->ndims - 1] < _self->dims[_self->ndims - 1]) {
    auto editing = 0;
    auto lasted = true;

    for (; dims[editing] < _self->dims[editing]; ++dims[editing]) {
      ord = nNet_RewriteOrdinateW1(ord, _self, _self->ndims, dims);
      src_idx = nNet_OrdinateToIndexW(interpreter(), _self, ord);

      if (src_idx >= 0) {
        auto src = nNet_GetArrayItem(data, src_idx, type);
        auto dst = nNet_GetArrayItem(cache, dst_idx, type);

        if (!(success = nNet_ConvertNumT(src, type, dst, type))) {
          return utils::make_null(remove_cache, interpreter(),
                                  BadAccess.reason("nNet_ConvertNumT()"));
        }
        dst_idx++;
      }
    }

    if (_self->ndims == 1) break;
    for (auto i = editing + 1; i < _self->ndims; ++i) {
      dims[i - 1] = 0;

      if (dims[i] + 1 < _self->dims[i]) {
        lasted = false;
        dims[i]++;
        break;
      }
    }

    if (lasted) dims[_self->ndims - 1]++;
    dims[0] = 0;
  }
  return cache;
}

base::Error TensorW::get(Array array) {
  using Buffer = cl_mem;

  if (_cache.is(typeid(Buffer))){
    auto size  = nNet_SizeType(_self->type) * nNet_GetArraySizeT(_self);
    auto error = clEnqueueReadBuffer(_device->queue(), _cache.get<Buffer>(),
                                     CL_TRUE, 
                                     0, size, array, 
                                     0, nullptr, nullptr); 

    return error? BadAccess: NoError;
  }
  return NoSupport;
}

base::Error TensorW::set(Array array) {
  using Buffer = cl_mem;

  if (_cache.is(typeid(Buffer))){
    auto size  = nNet_SizeType(_self->type) * nNet_GetArraySizeT(_self);
    auto error = clEnqueueWriteBuffer(_device->queue(), _cache.get<Buffer>(), 
                                      CL_TRUE,
                                      0, size, array,
                                      0, nullptr, nullptr);

    return error? BadAccess: NoError;
  }
  return NoSupport;
}

base::Error TensorW::make(Array data, int length, TensorW::Type type){
  if (type == TensorW::BufferT){
    auto error  = CL_SUCCESS;
    auto buffer = clCreateBuffer(_device->context(), CL_MEM_READ_ONLY, length, 
                                 nullptr, nullptr);

    if (!buffer)
      return DrainMem.reason("clCreateBuffer()");
    else if (data){
      error = clEnqueueWriteBuffer(_device->queue(), buffer, CL_TRUE, 
                                   0, length, data,
                                   0, nullptr, nullptr);
    }

    if (error != CL_SUCCESS){
      return BadAccess;
    } else {
      _cache = buffer;
      _cache.delete_() = [](void* data){
        clReleaseMemObject((((TensorW::Buffer*)data)[0]));
        delete (TensorW::Buffer*)data;
      };
    }
    return NoError;
  }
  return NoSupport;
}


void CLConsumer::commit(cl_kernel UNUSED(kernel)){
  /* @TODO: it's not like metal, OpenCL only support waiting until ending
   * so we might consider to implement opencl's catcher by using base::Thread.
   * At each time, a kernel is only called only one, in the case when we want to 
   * call the same kernel, we must wait until it finishes or use feature 'expand'
   */

  clFinish(_queue);
}
} // namespace opencl
} // namespace device
} // namespace nn