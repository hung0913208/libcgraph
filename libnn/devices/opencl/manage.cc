#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

#include "devices/opencl/opencl.h"

DEFINE_OP(opencl, Add);
DEFINE_OP(opencl, MatMul);
DEFINE_OP(opencl, Reshape);

DEFINE_OP(opencl, Conv2D);
DEFINE_OP(opencl, Relu);
DEFINE_OP(opencl, MaxPool);

DEFINE_OP(opencl, Softmax);

extern "C" int findint(const char* input, char** next){
  int begin = -1, end = -1;

  if (input == nullptr)
    return 0;
  else if (next)
    *next = nullptr;
    
  for (auto i = 0; input[i] != '\0'; ++i){
    if (begin < 0 && '0' <= input[i] && input[i] <= '9'){
      begin = i;
    } else if (begin >= 0 && ('0' >= input[i] || input[i] >= '9')){
      end = i;
      break;
    }
  }
  
  if (end < 0 && begin < 0)
    return 0;
  else if (end < 0 && begin >= 0)
    end = strlen(input);

  if (next)
    *next = const_cast<char*>(input) + end;
  return std::stoi(std::string{input + begin, size_t(end - begin)});
}

namespace nn{
namespace device{
namespace opencl{
Device create(Device pattern, Interpreter interpreter);
Device release(Device device);

Operator find(Device device, const char *name);
Tensor   tensor(Interpreter interpreter, Tensor tensor);

bool inspect(Device device);
bool install(Device device, const char *name, Operator op);
bool scan_manager(Device device, Operator op);
bool scan_consumer(Device device, Operator op);

CLManager::CLManager(cl_platform_id platform, cl_device_id device, int index)
    : DeviceW("opencl"), _platform{platform}, _device{device}, _index{index} {      
  /* @NOTE: install global configure, notice this device is an abstract device
   * and it can't be used to create any operators */

  if (_device != nullptr && isSupport()){
    interface()->_template._priv = this;

    interface()->level = SPECIFIC_GPU_PLATFORM;

    interface()->_template.find    = opencl::find;
    interface()->_template.scan    = opencl::scan_manager;
    interface()->_template.inspect = opencl::inspect;
    interface()->_template.create  = opencl::create;
    interface()->_template.release = opencl::release;

    if ((interface()->_enable = (bool *)calloc(1, sizeof(bool))))
      *(interface()->_enable) = _device != nullptr;

    /* @NOTE: install generic's operators */
    // FUNCTION(opencl, interface(), Add);
    // FUNCTION(opencl, interface(), MatMul);
    // FUNCTION(opencl, interface(), Reshape);
    // FUNCTION(opencl, interface(), Conv2D);
    // FUNCTION(opencl, interface(), Relu);
    // FUNCTION(opencl, interface(), MaxPool);
    FUNCTION(opencl, interface(), Softmax);
  } else
    throw NoSupport;
}

bool CLManager::isSupport(){
  /* @TODO: implement this function */
  return true;
}

CLConsumer::CLConsumer(CLManager *manager, Interpreter interpreter)
    : DeviceW{"opencl"}, _manager{manager}, _interpreter{interpreter}, 
                         _library{nullptr}{
 #if defined(CL_VERSION_2_0)
  cl_context_properties props[3];
 #endif
  cl_int cl_error;

  /* @NOTE: implement CLConsumer, a helper which is used to interact directly
   * with GPU. CLConsumer doesn't serve exact only a real device, it should 
   * easily swift between device if it want to improve performance */

  interface()->_template._priv = this;

  interface()->level = SPECIFIC_GPU_PLATFORM;
  interface()->_enable = manager->interface()->_enable;
  interface()->tensor = opencl::tensor;

  interface()->_template.find    = opencl::find;
  interface()->_template.scan    = opencl::scan_consumer;
  interface()->_template.inspect = opencl::inspect;
  interface()->_template.release = opencl::release;
  interface()->_template.install = opencl::install;

 #if !defined(CL_VERSION_2_0)
  _context = clCreateContext(0, 1, &device(), NULL, NULL, &cl_error);
 #else
  props[0] = CL_CONTEXT_PLATFORM;
  props[1] = (cl_context_properties) platform();
  props[2] = 0;

  _context = clCreateContextFromType(props, CL_DEVICE_TYPE_ALL, NULL,
                                     NULL, &cl_error);
 #endif

  if (cl_error == CL_SUCCESS){
   #if !defined(CL_VERSION_2_0) || defined(__NVCC__)
    _queue = clCreateCommandQueue(_context, device(), 0, &cl_error);
   #else
    char buffer[1024];

    cl_error = clGetPlatformInfo(platform(), CL_PLATFORM_VERSION, sizeof(buffer), buffer,
                                 nullptr);

    if (cl_error == CL_SUCCESS){
      if (findint(&(buffer[0]), nullptr) >= 2){
        cl_queue_properties queue_props[] = {
          CL_QUEUE_PROPERTIES,
          (const cl_queue_properties)(CL_QUEUE_ON_DEVICE &
                                      CL_QUEUE_OUT_OF_ORDER_EXEC_MODE_ENABLE),
          0
        };

        _queue = clCreateCommandQueueWithProperties(_context, device(), 
                                                    queue_props, &cl_error);
        if (!_queue || cl_error != CL_SUCCESS)
          throw BadAccess.reason("clCreateCommandQueueWithProperties()");
      } else {
        _queue = clCreateCommandQueue(_context, device(), 0, &cl_error);
        if (!_queue || cl_error != CL_SUCCESS)
          throw BadAccess.reason("clCreateCommandQueue()");
      }
    } else {
      throw BadAccess << "clGetPlatformInfo()" << "reason:" 
                      << readable("CLConsumer", "clGetPlatformInfo", cl_error);
    }
   #endif
  } else {
   #if !defined(CL_VERSION_2_0)
    throw BadAccess << "clCreateContextFromType()" << "reason:" 
                    << readable("CLConsumer", "clCreateContextFromType", cl_error);
   #else
    throw BadAccess << "clCreateContext()" << "reason:" 
                    << readable("CLConsumer", "clCreateContext", cl_error);
   #endif
  }
}

CLConsumer::~CLConsumer(){
  clReleaseCommandQueue(_queue);
  clReleaseContext(_context);
  interface()->_enable = nullptr;
}

base::Error CLConsumer::install(){
  using Sources = std::map<std::string, std::string>;
  using namespace base;

  Error error;

  if (config::get<std::string>("opencl") == "file"){
    /* @NOTE: get field 'opencl.file' and load openclib to memory */

    if (config::get("opencl.file").type().get() == typeid(std::string))
      error = install(config::get<std::string>("opencl.file"));
    else
      error = BadAccess << "field \'opencl.file\'";
  }
 #if APPLE
  else if (config::get<std::string>("opencl") == "bundle"){
    /* @NOTE: get field 'opencl.bundle' and load openclib to memory */

    if (config::get("opencl.bundle").type().get() == typeid(std::string)){
      auto bundle = base::config::get<std::string>("opencl.bundle").c_str();

      NSString* nsbundle = [NSString stringWithCString: bundle 
                                              encoding: NSUTF8StringEncoding];
      NSString* nspath   = [[NSBundle mainBundle] pathForResource: nsbundle
                                                           ofType: @"cllib"];
      error = install(nspath);
    } else {
      error = BadAccess << "field \'opencl.bundle\'";
    }
  }
 #endif
  else if (config::get<std::string>("opencl") == "bundle"){
    if (config::get("opencl.sources").type().get() == typeid(std::string)){
    /* @NOTE: get field 'opencl.source' and set _resource */

   #if DEV
   # if APPLE
    if (config::get<std::string>("opencl.sources") == "bundle"){
      /* @TODO: get everything inside bundle */
    } else {
      auto path = config::get<std::string>("opencl.sources");

      if (!path.compare(0, sizeof("bundle://"), "bundle://")){
        /* @TODO: this path is a part of main bundle */
      } else {
        /* @TODO: read all file inside path and build a library */
      }
    }
   # endif
   #else
    error = NoSupport;
   #endif
    } else if (config::get("opencl.sources").type().get() == typeid(Sources)){
      _resource = config::get<Sources>("opencl.sources");
      return NoError;
    }
  } else if (config::get<std::string>("opencl") == "auto"){
    return NoError;
  }

  return _library? NoError: error;
}

base::Error CLConsumer::install(std::string&& path){
  using uchar = unsigned char;

  std::fstream stream{path, std::ios::in};
  std::string  source;

  if (!stream.is_open())
    return BadAccess.reason(rvalue(path));
  else
    source = std::string(std::istreambuf_iterator<char>(stream), {});

  if (source.size() > 0){
    cl_int cl_error, status;
    auto binary = reinterpret_cast<const uchar*>(source.c_str());
    auto size   = source.size();

    _library = clCreateProgramWithBinary(_context, 1, &device(), &size,
                                         &binary, &status, &cl_error);
    if (cl_error != CL_SUCCESS)
      return BadAccess << readable("install", "clCreateProgramWithBinary", 
                                   cl_error);
    else
      return NoError;
  } else return DoNothing;
}

#if APPLE
base::Error CLConsumer::install(NSString* path){
  return install(std::string([path UTF8String]));
}
#endif

cl_kernel CLConsumer::pipeline(CLConsumer::Source source){
  cl_kernel  result   = nullptr;
  cl_program program  = nullptr; // compute program
  cl_int     cl_error = CL_SUCCESS;

  auto interw    = nNet_GetInterpreterW(_interpreter);
  auto functions = std::get<1>(source);
  auto name      = std::get<0>(source);

  if (functions.size() == 0)
    return pipeline(std::get<0>(source));

  /* @NOTE: build a opencl-function from source */
  program = clCreateProgramWithSource(_context,
                                      functions.size(), functions.data(), 
                                      nullptr,
                                      &cl_error);
  if (program){
    /* @TODO: redesgin a way to customize our own source code*/
    cl_error = clBuildProgram(program, 0, nullptr, 
                              nullptr, nullptr, nullptr);

    if (cl_error != CL_SUCCESS){
      char   buffer[2048];
      size_t length;

      memset(buffer, 0, sizeof(buffer));
      clGetProgramBuildInfo(program, device(), CL_PROGRAM_BUILD_LOG,
                            sizeof(buffer), buffer, &length);

      interw->error(BadAccess << readable("pipeline", "clBuildProgram", 
                                          cl_error, buffer));
      goto finish;
    }
    result = clCreateKernel(program, name.c_str(), &cl_error);

    if (cl_error != CL_SUCCESS){
      interw->error(BadAccess << readable("pipeline", "clCreateKernel", cl_error));
      result = nullptr;
    }
  } else {
    interw->error(BadAccess << readable("pipeline", "clCreateProgramWithSource", 
                                        cl_error))
                            << "when load function" << name;
  }

 finish:
  return result;
}

cl_kernel CLConsumer::pipeline(std::string name){
  cl_kernel result   = nullptr;
  cl_int    cl_error = CL_SUCCESS;

  auto interw = nNet_GetInterpreterW(_interpreter);

  if (!_library)
    interw->error(DoNothing << "please install cllib first");
  else {
    /* @NOTE: build a opencl-function from the library */
    result = clCreateKernel(_library, name.c_str(), &cl_error);

    if (cl_error != CL_SUCCESS){
      interw->error(BadAccess
                    << readable("pipeline", "clCreateKernel", cl_error));
    } else
      return result;
  }

  return nullptr;
}

Device create(Device pattern, Interpreter interpreter){
  auto manager = dynamic_cast<CLManager *>(nNet_GetDeviceW(pattern));

  if (manager){
    auto consumer = (new opencl::CLConsumer(manager, interpreter));

    if (!consumer->install())
      return consumer->interface();

    delete consumer;
    return nullptr;
  } else
    return pattern;
}

Device release(Device device){
  if (nNet_CheckStatus() == nn::Releasing && nNet_IsTemplateDevice(device)){
    auto manager = dynamic_cast<CLManager *>(nNet_GetDeviceW(device));

    if (manager){
      delete manager;
      return nullptr;
    }
  }

  if (nNet_CheckStatus() == nn::Releasing || !nNet_IsTemplateDevice(device)){
    auto consumer = dynamic_cast<CLConsumer *>(nNet_GetDeviceW(device));

    if (consumer){
      delete consumer;
      return nullptr;
    }
  }
  return nNet_RemoveDevice(device);
}

Operator find(Device device, const char *name){
  auto devicew = nNet_GetDeviceW(device);
  auto delegate = dynamic_cast<CLConsumer *>(devicew);
  auto operators = delegate ? (&delegate->operators()) : nullptr;

  if (!name || !operators) return nullptr;
  else if (operators->find(std::string(name)) != operators->end())
    return (*operators)[std::string(name)];
  return nullptr;
}

Tensor tensor(Interpreter interpreter, Tensor tensor){
  using namespace nn::interpreter;
  using namespace nn::tensor;

  auto device = reinterpret_cast<Device>(tensor->_device);
  auto result = tensor;

  if (!utils::response<DeviceW, CLConsumer>(device->_priv))
    return nNet_RemoveTensorW(interpreter, tensor);
  /* @NOTE: because we need to convert tensor from device to another device, 
   * so we must convert data first 
   */
  if (nNet_GetTensorType(tensor) != NilT){
    try{
      auto content = new TensorW(interpreter, tensor, nNet_GetTensorNumT(tensor));
      auto shape   = nNet_CreateTensorShapeC1(interpreter, 0);

      if (nNet_GetTensorTypeW(interpreter, tensor) >= VectorT){

        /* @NOTE: only call tensor->release because we don't want to remove tensor
         * completely, we only care about remove its content and rebuild a new 
         * content. In somecase, we must preseve its shape too.
         */

        if (!nNet_GetTensorShapeW(interpreter, tensor, shape)){
          return utils::clearFaulty(interpreter, tensor,
                                    BadAccess.reason("nNet_GetTensorShape()"));
        }
      }

      if (tensor->release)
        tensor->release(tensor, interpreter);
      else {
        if (tensor->_content)
          free (tensor->_content);
        tensor->_content = nullptr;
      }

      tensor->_content = content;
      if (nNet_GetTensorTypeW(interpreter, tensor) >= VectorT){
        if (!nNet_SetTensorShapeW(interpreter, tensor, shape)){
          result = utils::clearFaulty(interpreter, tensor,
                                      BadAccess <<"nNet_SetTensorShapeW()");
        }
      }

      if (nNet_RemoveTensorShapeW(interpreter, shape)){
        nNet_GetInterpreterW(interpreter)->error(
                                      BadAccess << "nNet_RemoveTensorShapeW()");
      }

      if (!result) return result;
    } catch(base::Error& error){
      return utils::clearFaulty(interpreter, tensor, error);
    }
  }

  /* @NOTE: install static functions which are used to manipulate with this 
   * tensor */

  /* @NOTE: constructor and destructor */
  tensor->init = [](Tensor self, Interpreter interpreter, int size, 
                    int type, bool force) -> bool{
    auto device = reinterpret_cast<Device>(self->_device);

    if (!self->_content || force || self->type != type || type == Unknown){
      /* @NOTE: verify this tensor must be created by CLConsumer */

      if (utils::response<DeviceW, CLConsumer>(device->_priv) == nullptr)
        return false;

      /* @NOTE: init will be call in 3 cases
       * - Realloc
       * - Change numtype 
       * - Reshape with shape is nil
       */

      if ((self->_content && force) || type == Unknown){
        if (self->_content){
          delete utils::response<GPContent, TensorW>(self->_content);
          self->_content = nullptr;
        }
      }

      if (!self->_content && type != Unknown){
        self->_content = new TensorW(interpreter, self, type);
        self->release  = [](Tensor self, Interpreter UNUSED(interpreter)){
          if (utils::response<GPContent, TensorW>(self->_content))
            delete utils::response<TensorW>(self->_content);
          self->_content = nullptr;
        };
      }

      if (type != Unknown){
        auto content = utils::response<GPContent, TensorW>(self->_content);
        return (content)? content->init(size, type, force): false;
      }
    }
    return true;
  };

  tensor->release = [](Tensor self, Interpreter UNUSED(interpreter)){
    if (utils::response<GPContent, TensorW>(self->_content))
      delete utils::response<TensorW>(self->_content);
    self->_content = nullptr;
  };

  tensor->_array.get = [](Tensor self) -> Array{
    auto control = utils::response<GPContent, TensorW>(self->_content);
    auto result  = nNet_AllocateArray(nNet_GetArraySizeT(self),
                                      nNet_GetTensorNumT(self));
    if (!result) return nullptr;
    else if (control && !control->get(result))
      return result;
    else {
      free(result);
      return nullptr;
    }
  };

  tensor->_array.set = [](Tensor self, Array array) -> bool{
    auto control = utils::response<GPContent, TensorW>(self->_content);
    auto result  = (control)? !control->set(array): false;

    if (result)
      free(array);
    return result;
  };

  return tensor;
}

bool inspect(Device device){
  auto devicew = nNet_GetDeviceW(device);
  auto consumer = dynamic_cast<CLConsumer *>(devicew);
  auto manager = dynamic_cast<CLManager *>(devicew);

  if (consumer){
    return *(device->_enable);
  } else if (manager){
    return *(device->_enable);
  } else
    return false;
}

bool install(Device device, const char *name, Operator op){
  auto delegate = dynamic_cast<CLConsumer *>(nNet_GetDeviceW(device));
  auto operators = delegate ? &delegate->operators() : nullptr;

  if (operators->find(std::string{name}) != operators->end())
    return BadLogic.reason("duplicate installing op " + std::string{name});

  (*operators)[std::string(name)] = op;
  return true;
}

bool scan_manager(Device device, Operator sample){
  auto delegate = dynamic_cast<CLConsumer *>(nNet_GetDeviceW(device));

  if (!delegate)
    return false;
  for (auto &item : delegate->operators()){
    if (std::get<1>(item) == sample)
      return true;
  }
  return false;
}

bool scan_consumer(Device device, Operator sample){
  auto delegate = dynamic_cast<CLConsumer *>(nNet_GetDeviceW(device));

  if (!delegate)
    return false;
  for (auto &item : delegate->operators()){
    if (std::get<1>(item) == sample)
      return true;
  }
  return false;
}
} // namespace opencl

base::Error useOpenCL(std::vector<Device> &result){
  using namespace interpreter;
  using namespace base;

  /* @NOTE: OpenCL support multi-devices and this cause a big trouble. To solve
   * it, i think we will decice 2 things:
   * - Each device will be create separatedly by device_id and device_name
   * (more than 1 device for 1 abstract device like 'OpenCL', 'Metal', ...).
   * - Operator will cling with real device's session, which was create by
   * global devices.
   */
  if (config::get("opencl").type().get() != typeid(std::string))
    return BadAccess << "please config field group \'opencl\' first";

  try {
    DeviceW *first{nullptr}, *previous{nullptr}, *last{nullptr};

    cl_platform_id* platforms{NULL};
    cl_uint         num_platforms{0};

    /* @NOTE: get all platforms */
    clGetPlatformIDs(0, NULL, &num_platforms);

    platforms = (cl_platform_id*)calloc(num_platforms, sizeof(cl_platform_id));
    if (platforms)
      clGetPlatformIDs(num_platforms, platforms, NULL);
    else return DrainMem;

    /* @NOTE: get all devices from platforms */
    for (auto i = 0; i < cast_(i, num_platforms); ++i){
      cl_uint num_devices{0};

      clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_GPU, 0, NULL, &num_devices);

      if (num_devices > 0){
        auto devices = (cl_device_id*)calloc(num_devices, sizeof(cl_device_id));

        /* @NOTE: load all device-id and create CLManager with each device-id */
        if (devices){
          clGetDeviceIDs(platforms[i], CL_DEVICE_TYPE_GPU, num_devices, devices, 
                         NULL);
        } else return DrainMem;

        for (auto j = 0; j <  cast_(j, num_devices); ++j){
          last = new opencl::CLManager(platforms[i], devices[j], j);

          if (dynamic_cast<opencl::CLManager*>(last)->device() != nullptr){
            /* @NOTE: makesure that device is ready to support opencl gpu */

            if (!last->interface()->_enable){
              delete last;
              continue;
            }
            
            if (*(last->interface()->_enable) == false){
              delete last;
              continue;
            }

            /* @NOTE: pass everything, ready to install this manager */
            if (previous)
              last->encircle(previous);
            else
              first = last;

            result.push_back(last->interface());
          } else delete last; 
        }

        free(devices);
      }
    }

    free(platforms);

    /* @NOTE: connect the begin and the end to be a completed enqueue */
    if (!first || !last)
      return DoNothing.reason("opencl only support real devices");
    else if (first != last) first->encircle(last);
  } catch (base::Error &error){
    return error;
  }

  return (result.size() > 0) ? NoError : NoSupport;
}
} // namespace device
} // namespace nn