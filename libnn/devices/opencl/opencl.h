#if !defined(LIBNN_DEVICE_OPENCL_H_) && __cplusplus
#define LIBNN_DEVICE_OPENCL_H_

#ifdef LIBCGRAPH_GIT_REFSPEC
#include <libbase/macros.hpp>
#else
#ifndef APPLE
#if __APPLE__
#define APPLE 1
#endif
#endif
#endif

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

#ifdef LIBCGRAPH_GIT_REFSPEC
#include "devices/dev_common.h"
#else
#include "dev_common.h"
#endif

#if APPLE
#import <Foundation/Foundation.h> 
#include <OpenCL/opencl.h>
#else
#include <CL/opencl.h>
#endif

namespace nn{
namespace device{
namespace opencl{
class CLManager: public interpreter::DeviceW{
 private:
  using Device   = cl_device_id;
  using Platform = cl_platform_id;

 public:
  explicit CLManager(Platform platform, Device device, int index);

 public:
  inline Platform& platform(){ return _platform; }
  inline Device&   device(){ return _device; }
  inline int       index(){ return  _index; }

 private:
  bool isSupport();

 private:
  Platform _platform;
  Device   _device;
  int      _index;
};

class CLConsumer: public interpreter::DeviceW{
 private:
  using Context = cl_context;

 public:
  using CommandQueue   = cl_command_queue;
  using CommandEncoder = cl_kernel;

 private: 
  using Source    = std::pair<std::string, std::vector<CString>>;
  using Sources   = std::map<std::string, std::string>;
  using Operators = std::map<std::string, Operator>;
  using Device    = cl_device_id;
  using Platform  = cl_platform_id;
  using Library   = cl_program;

 public:
  ~CLConsumer();
  explicit CLConsumer(CLManager *manager, Interpreter interpreter);

 public:
  inline Device&   device(){ return _manager->device(); }
  inline Platform& platform(){ return _manager->platform(); }

  inline CommandQueue& queue(){ return _queue; }
  inline Context&      context(){ return _context; }

 public:
  inline CLManager &manager(){ return *_manager; }
  inline Operators &operators(){ return _manager->operators(); }

 public:
  cl_kernel   pipeline(Source source);
  cl_kernel   pipeline(std::string name);
  base::Error install();
  base::Error install(std::string&& path);

 #if APPLE
  base::Error install(NSString* path);
 #endif

  void commit(cl_kernel kernel);

  std::string readable(std::string method, std::string where, cl_int cl_error);
  std::string readable(std::string method, std::string where, cl_int cl_error,
                       std::string errlogs);

 private:
  CLManager *_manager;

 private:
  CommandQueue _queue;
  Interpreter  _interpreter;
  Sources      _resource;

 private:
  Library _library;
  Context _context;
};

class TensorW: public tensor::GPContent{
 public:
  enum Type{ Unknown = 0, BufferT = 1, Texture2dT = 2, MTLTexture3dT = 3 };
  using Buffer = cl_mem;

 private:
  using Driver = CLConsumer;

 public:
  static std::string nametype(Type type){
    switch (type){
    case Unknown:
    default:
      return "Unknown";

    case BufferT:
      return "Buffer";

    case Texture2dT:
      return "Texture2d";
    }
  }
  
 public:
  explicit TensorW(Interpreter interpreter, Tensor tensor, int type);
  virtual ~TensorW();

 public:
  inline Type type(){ return _type; }
  inline base::Auto& cache(){ return _cache; }

 public:
  bool init(long size, int type, bool force);

  base::Error get(Array array);
  base::Error set(Array array);
  base::Error make(Array data, int length, Type type);

 private:
  void init() final{}
  Array clone();
 
 private:
  base::Auto  _cache;
  std::size_t _length;

 private:
  Driver *_device;
  Tensor  _self;
  Type    _type;
};
} // namespace opencl
} // namespace device
} // namespace nn
#endif  // LIBNN_DEVICE_OPENCL_H_