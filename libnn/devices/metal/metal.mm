#include "devices/metal/metal.h"

#if APPLE
#include <Foundation/Foundation.h>

namespace nn {
namespace device {
namespace metal {  
TensorW::~TensorW() {}
TensorW::TensorW(Interpreter interpreter, Tensor tensor, int type, bool renew)
    : GPContent{interpreter, tensor, true}, _self{tensor} {
  auto result = init(nNet_GetArraySizeT(tensor), type, renew);

  if (!result && !nNet_GetInterpreterW(interpreter)->error())
    nNet_GetInterpreterW(interpreter)->error(BadAccess);
}

TensorW::Buffer TensorW::newBuffer(Array data, int length){
  /* @NOTE: create a MTLBuffer which associate with a pointer in GPU Metal */

  if (_driver == nullptr) return nullptr;
  if (data != nullptr)
    return [_driver newBufferWithBytes:data
                                length:length
                               options:MTLResourceCPUCacheModeDefaultCache];
  else
    return [_driver newBufferWithLength:length
                                options:MTLResourceCPUCacheModeDefaultCache];
}

TensorW::Texture TensorW::newTexture2D(Array data, MTLPixelFormat format, 
                                     int width, int height){
  if (_driver){
   #if DEV
    auto descriptor =
        [MTLTextureDescriptor texture2DDescriptorWithPixelFormat:format
                                                           width:width
                                                          height:height
                                                      mipmapped:false];
    auto texture = [_driver newTextureWithDescriptor:descriptord];
    auto region  = MTLRegionMake2D(0, 0, width, height);

    [texture replaceRegion: region mipmaplevel:0 slice:0 withBytes:data
               bytesPerRow: width*4*1
             bytesPerImage: ]
    return texture;
   #else
    return nullptr;
   #endif
  } else return nullptr;
}

bool TensorW::init(long size, int type, bool force) {
  using namespace nn::interpreter;

  auto device   = reinterpret_cast<Device>(_self->_device);
  auto consumer = utils::response<DeviceW, MetalConsumer>(device->_priv);

  if (!consumer) return false;
  _driver = utils::response<MetalConsumer>(consumer)->device();

  /* @NOTE: force == true -> create a new cache */
  if (size == 0) return true;
  if (force) 
    return !make(nullptr, size*nNet_SizeType(type), BufferT);

  /* @NOTE: otherwide, create a new buffer with and copy data from the old cache
   * to the new cache */
  if ((_cache == nil && nNet_GetTensorType(_self) != NilT) || 
      type != _self->type || size != nNet_GetArraySizeTC(interpreter(), _self)){
    auto size    = nNet_GetArraySizeTC(interpreter(), _self)*nNet_SizeType(type);
    auto buffer  = clone();
    auto success = false;

    if (buffer){
      success = !make(buffer, size, BufferT);
      free(buffer);
    }
    return success;
  } else {
    return !nNet_GetInterpreterW(interpreter())->error(BadLogic);
  }
  return true;
}

Array TensorW::clone(){
  auto cache = (void*)nullptr;
  auto remove_cache = [&](){ if (cache) free(cache); };

  auto ord = nNet_CreateOrdinateC1(interpreter(), _self, 0);
  auto shape = nNet_CreateTensorShapeC1(interpreter(), 0);

  auto data = nNet_GetArrayData(_self);
  auto type = nNet_GetTensorNumT(_self);

  auto dims    = _self->ndims > 0 ? new int[_self->ndims] : nullptr;
  auto dst_idx = 0, src_idx = 0, size = 1;
  auto success = false;

  base::Boundary bound{[]() {}, [&]() { 
    if (dims) delete[] dims;
    nNet_TryDropingArray(_self, data);
  }};

  switch (nNet_GetTensorType(_self)) {
    case NilT:
    default:
      return utils::null(interpreter(), NoSupport);

    case ScalarT:
      if (!(cache = malloc(nNet_SizeType(type)))){
        return utils::null(interpreter(), DrainMem);
      } else if (!nNet_ConvertNumT(data, type, cache, type)){
        return utils::make_null(remove_cache, interpreter(),
                                BadAccess << "nNet_ConvertNumT()");
      }
      return cache;

    case VectorT:
    case MatrixT:
    case TensorT:
      break;
  }

  if (!ord) {
    return utils::make_null(remove_cache, interpreter(), 
                            DrainMem.reason("nNet_CreateOrdinateC1()"));
  }

  if (!shape) {
    return utils::make_null(remove_cache, interpreter(), 
                            DrainMem.reason("nNet_CreateTensorShapeC1()"));
  }

  /* @NOTE: get Tensor's Shape which indicate the real size of this tensor */
  if (!nNet_GetTensorShapeW(interpreter(), _self, shape)) {
    return utils::make_null(remove_cache, interpreter(),
                            BadAccess.reason("nNet_GetTensorShape()"));
  } else {
    for (auto i = 0; i < shape->ndims; i++) {
      size *= shape->dims[i];
    }
  }

  /* @NOTE: allocate cache */
  if (!(cache = malloc(size * nNet_SizeType(type)))) {
    return utils::make_null(remove_cache, interpreter(), DrainMem);
  }

  /* @NOTE: everything is done, now copy data from tensor to cache */
  for (auto i = 0; i < _self->ndims; ++i) {
    dims[i] = 0;
  }
  while (dims[_self->ndims - 1] < _self->dims[_self->ndims - 1]) {
    auto editing = 0;
    auto lasted = true;

    for (; dims[editing] < _self->dims[editing]; ++dims[editing]) {
      ord     = nNet_RewriteOrdinateW1(ord, _self, _self->ndims, dims);
      src_idx = nNet_OrdinateToIndexW(interpreter(), _self, ord);

      if (src_idx >= 0) {
        auto src = nNet_GetArrayItem(data, src_idx, type);
        auto dst = nNet_GetArrayItem(cache, dst_idx, type);

        if (!(success = nNet_ConvertNumT(src, type, dst, type))) {
          return utils::make_null(remove_cache, interpreter(),
                                  BadAccess.reason("nNet_ConvertNumT()"));
        }
        dst_idx++;
      }
    }

    if (_self->ndims == 1) break;
    for (auto i = editing + 1; i < _self->ndims; ++i) {
      dims[i - 1] = 0;

      if (dims[i] + 1 < _self->dims[i]) {
        lasted = false;
        dims[i]++;
        break;
      }
    }

    if (lasted) dims[_self->ndims - 1]++;
    dims[0] = 0;
  }
  return cache;
}

base::Error TensorW::get(Array array) {
  if (_type == BufferT){
    auto buffer  = (Buffer)_cache;
    auto content = [[NSData alloc] initWithBytesNoCopy: [buffer contents]
                                                length: _length
                                          freeWhenDone: false];

    [content getBytes:array length:_length];
  } 
 #if DEV
  else if (_type == Texture2dT){
    auto buffer  = cache.get<TensorW::Texture>(); 
    auto content = [[NSData alloc] initWithBytesNoCopy: [buffer contents]
                                                length: _length
                                          freeWhenDone: false];

    [content getBytes:array length:_length];
  }
 #endif
  else return NoSupport;
  return NoError;
}

base::Error TensorW::set(Array array) {
  if (_type == BufferT){
    Buffer buffer = _cache;

    switch(_self->type){
    case Real32_t:{
      auto pointer = (Real32*)buffer.contents;

      for (auto i = 0; i < cast_(i, _length); ++i)
        pointer[i] = ((Real32*)array)[i];
      break;
    }

    case Int32_t:{
      auto pointer = (Int32*)buffer.contents;

      for (auto i = 0; i < _length; ++i)
        pointer[i] = ((Int32*)array)[i];
      break;
    }

    default:
      return NoSupport;
    }

    [buffer didModifyRange: NSMakeRange(0, _length)];
  }
 #if DEV
  else if (_type == Texture2dT){
    auto buffer = newTenxture2D(array, _length);

    if (!buffer) return DrainMem;
    _cache = buffer;
  }
 #endif
  else return NoSupport;
  return NoError;
}

base::Error TensorW::make(Array data, int length, TensorW::Type type){
  if (type == BufferT){
    auto buffer = newBuffer(data, _length = length);

    if (!buffer) return DrainMem;
    else {
      _cache = buffer;
      _type  = BufferT;
      return NoError;
    }
  } 
 #if DEV
  else if (type == Texture2dT){
    auto texture2d = newTexture2D(data, _length = length);

    if (!texture2d) return DrainMem;
    else {
      _cache = texture2d;
      _type  = Texture2dT;
      return NoError;
    }
  }
 #endif
  else 
    return NoSupport;
}

void MetalConsumer::commit(CommandBuffer buffer){
  [buffer addCompletedHandler: [&](id<MTLCommandBuffer> buffer){
  }];

  switch (nNet_GetInterpreterW(_interpreter)->resolve()){
  default:
  case LOOPBACK_RESOLVE:
    [buffer commit];
    [buffer waitUntilCompleted];

    _tasks.erase(buffer);
    break;
  }
}
} // namespace metal
} // namespace device
} // namespace nn
#endif