#if !defined(LIBNN_DEVICE_METAL_H_) && __cplusplus
#define LIBNN_DEVICE_METAL_H_

#if LIBCGRAPH_GIT_REFSPEC
#include <libbase/macros.hpp>
#else
#ifndef APPLE
#if __APPLE__
#define APPLE 1
#endif
#endif
#endif

#if APPLE
#include <Metal/Metal.h>
#include <signal.h>

#ifdef LIBCGRAPH_GIT_REFSPEC
#include "devices/dev_common.h"
#else
#include "dev_common.h"
#endif

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"

namespace nn {
namespace device {
namespace metal {
class MetalManager: public interpreter::DeviceW {
 private:
  using Device = id<MTLDevice>;

 public:
  explicit MetalManager(id<MTLDevice> device, int index);

 public:
  inline Device device() { return _device; }
  inline int    index() { return _index; }

 #if DEBUG
  inline std::string &&hardware() { return rvalue(_hardware); }
  inline std::string &&os() { return rvalue(_os); }
 #endif

 private:
  bool isSupport();

 private:
  Device _device;
  int    _index;

 #if DEBUG
  std::string _hardware, _os;
 #endif
};

class MetalConsumer: public interpreter::DeviceW {
 public:
  using CommandQueue   = id<MTLCommandQueue>;
  using CommandBuffer  = id<MTLCommandBuffer>;
  using CommandEncoder = id<MTLCommandEncoder>;

 private:
  using Sources   = std::map<std::string, std::string>;
  using Operators = std::map<std::string, Operator>;
  using Library   = id<MTLLibrary>;
  using Device    = id<MTLDevice>;

 public:
  explicit MetalConsumer(MetalManager *manager, Interpreter interpreter);
  ~MetalConsumer();

 public:
  inline CommandBuffer commandBuffer() {
    auto cmdbuffer = [_queue commandBufferWithUnretainedReferences];

    if (cmdbuffer)
      _tasks[cmdbuffer] = nullptr;
    return cmdbuffer;
  }

  inline Library    library() { return _library; }
  inline Device     device() { return _manager->device(); }
  inline Operators& operators() final { return _manager->operators(); }

 public:
  base::Error install();
  base::Error install(std::string &&path);
  base::Error install(NSString *path);

  void commit(CommandBuffer buffer);
  id<MTLComputePipelineState> pipeline(std::string name);

 private:
  MetalManager *_manager;
  Device        _device;
  CommandQueue  _queue;
  Interpreter   _interpreter;
  Library       _library;
  Sources       _resource;

 private:
  std::map<CommandBuffer, CommandEncoder> _tasks;
};

class TensorW: public tensor::GPContent {
 public:
  using Buffer  = id<MTLBuffer>;
  using Texture = id<MTLTexture>;

  enum Type { Unknown = 0, BufferT = 1, Texture2dT = 2, Texture3dT = 3 };

 public:
  static std::string nametype(Type type){
    switch (type){
    case Unknown:
    default:
      return "Unknown";

    case BufferT:
      return "Buffer";

    case Texture2dT:
      return "Texture2d";

    case Texture3dT:
      return "Texture3d";
    }
  }

 private:
  using Driver = id<MTLDevice>;

 public:
  explicit TensorW(Interpreter interpreter, Tensor tensor, int type,
                   bool renew = false);
  virtual ~TensorW();

 public:
  inline Type type() { return _type; }
  inline id&  cache() { return _cache; }

 public:
  bool  init(long size, int type, bool force);

  base::Error get(Array array);
  base::Error set(Array array);
  base::Error make(Array data, int length, Type type);

 private:
  Buffer  newBuffer(Array data, int size);
  Texture newTexture2D(Array data, MTLPixelFormat format, int width, int height);

  void  init() final {}
  Array clone();

 private:
  id     _cache;
  size_t _length;

 private:
  Driver _driver;
  Tensor _self;
  Type   _type;
};
} // namespace metal
} // namespace device
} // namespace nn
#endif
#endif // LIBNN_DEVICE_METAL_H_