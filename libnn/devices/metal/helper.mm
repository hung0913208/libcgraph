#include <Metal/Metal.h>

#include "devices/metal/macros.h"
#include "devices/metal/metal.h"
#include "internal.h"

namespace nn{
namespace device{
namespace metal{
namespace helper{
base::Error putBufferInput(MetalConsumer* consumer, 
                           id<MTLComputeCommandEncoder> commandEncoder,
                           int index, Tensor tensor) {
  using namespace nn::tensor;

  auto buff = (TensorW*)nullptr;
  auto dev = consumer->device();

  if ((Device(tensor->_device))->level == GENERIC_PLATFORM) {
    auto size = nNet_GetArraySizeT(tensor)*nNet_SizeType(tensor->type);
    auto mtlbuffer = 
      [dev newBufferWithBytes: nNet_GetArrayData(tensor)
                       length: size
                      options: MTLResourceCPUCacheModeDefaultCache];

    if (mtlbuffer) {
      [commandEncoder setBuffer: mtlbuffer 
                         offset: 0
                        atIndex: index];
    } else {
      return BadAccess.reason("newBufferWithBytes()");
    }
  } else if ((buff = utils::response<GPContent, TensorW>(tensor->_content))) {
    [commandEncoder setBuffer: buff->cache() 
                       offset: 0 
                      atIndex: index];
  } else {
    auto array     = nNet_GetArrayData(tensor);
    auto size      = nNet_GetArraySizeT(tensor)*nNet_SizeType(tensor->type);
    auto mtlbuffer = 
      [dev newBufferWithBytes: array
                       length: size
                      options: MTLResourceCPUCacheModeDefaultCache];

    base::Boundary bound{[](){}, [&](){ nNet_TryDropingArray(tensor, array); }};

    if (mtlbuffer) {
      [commandEncoder setBuffer: mtlbuffer
                         offset: 0
                        atIndex: index];
    } else {
      return BadAccess.reason("newBufferWithBytes()");
    }
  }
  return NoError;
}

base::Error putBufferOutput(id<MTLComputeCommandEncoder> commandEncoder,
                           int index, Tensor tensor) {
  using namespace nn::tensor;

  auto buff = (TensorW*)nullptr;

  if ((Device(tensor->_device))->level == GENERIC_PLATFORM)
    return BadAccess.reason("output must be a metal::TensorW");
  else if ((buff = utils::response<GPContent, TensorW>(tensor->_content))) {
    [commandEncoder setBuffer: buff->cache()
                       offset: 0
                      atIndex: index];
  } else
    return BadAccess.reason("output must be a metal::TensorW");

  return NoError;
}
} // namespace helper
} // namespace metal
} // namespace device
} // namespace nn
