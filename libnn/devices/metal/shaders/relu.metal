#include <metal_stdlib>

using namespace metal;

kernel void RR_Buffer_Relu_Forward(const device float *input [[ buffer(0) ]],
                                   device float *output      [[ buffer(1) ]],
                                   uint gid    [[ thread_position_in_grid ]])
{
  output[gid] = fmax(0.0, input[gid]);
}


kernel void II_Buffer_Relu_Forward(const device int *input [[ buffer(0) ]],
                                   device int *output      [[ buffer(1) ]],
                                   uint gid  [[ thread_position_in_grid ]])
{
  output[gid] = fmax(0.0, input[gid]);
}


kernel void RI_Buffer_Relu_Forward(const device float *input [[ buffer(0) ]],
                                   device int *output        [[ buffer(1) ]],
                                   uint gid    [[ thread_position_in_grid ]])
{
  output[gid] = fmax(0.0, input[gid]);
}


kernel void IR_Buffer_Relu_Forward(const device int *input [[ buffer(0) ]],
                                   device float *output    [[ buffer(1) ]],
                                   uint gid  [[ thread_position_in_grid ]])
{
  output[gid] = fmax(0.0, input[gid]);
}
