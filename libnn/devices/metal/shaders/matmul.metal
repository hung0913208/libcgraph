#include <metal_stdlib>

using namespace metal;
kernel void  RRR_Buffer_MatMul_Forward(
  const device float* left      [[ buffer(0) ]],
  const device float* right     [[ buffer(1) ]],
  const device uint* dims       [[ buffer(2) ]],
  device float* outputs         [[ buffer(3) ]],
  uint gid   [[ thread_position_in_grid ]])
{
  int x = gid%dims[0];
  int y = gid/dims[0];
  int i = 0;

  int lbeg = 0;
  int rbeg = 0;
  int left_idx = 0;
  int right_idx = 0;

 #if ENABLE_STRASSEN_ALGORITHM
  /**/
 #elif ENABLE_COPPERSMITH_WINOGRAD_ALGORITHM
 #else
  /* @NOTE: naive algorithm will be used by default */

  if (gid < dims[0] * dims[2]){
    for (; i < dims[1]; ++i){
        left_idx  = y*dims[1] + i;
        right_idx = i*dims[0] + x;

        outputs[gid] += left[left_idx] * right[right_idx];
    }
  }
 #endif
}

kernel void RRI_Buffer_MatMul_Forward(
  const device float* left      [[ buffer(0) ]],
  const device float* right     [[ buffer(1) ]],
  const device uint* dims       [[ buffer(2) ]],
  device int* outputs           [[ buffer(3) ]],
  uint gid   [[ thread_position_in_grid ]])
{
  int x = gid%dims[0];
  int y = gid/dims[0];
  int i = 0;

  int lbeg = 0;
  int rbeg = 0;
  int left_idx = 0;
  int right_idx = 0;

 #if ENABLE_STRASSEN_ALGORITHM
  /**/
 #elif ENABLE_COPPERSMITH_WINOGRAD_ALGORITHM
 #else
  /* @NOTE: naive algorithm will be used by default */

  if (gid < dims[0] * dims[2]){
    for (; i < dims[1]; ++i){
        left_idx  = y*dims[1] + i;
        right_idx = i*dims[0] + x;

        outputs[gid] += left[left_idx] * right[right_idx];
    }
  }
 #endif
}

kernel void RIR_Buffer_MatMul_Forward(
  const device float* left      [[ buffer(0) ]],
  const device int* right       [[ buffer(1) ]],
  const device uint* dims       [[ buffer(2) ]],
  device float* outputs         [[ buffer(3) ]],
  uint gid   [[ thread_position_in_grid ]])
{
  int x = gid%dims[0];
  int y = gid/dims[0];
  int i = 0;

  int lbeg = 0;
  int rbeg = 0;
  int left_idx = 0;
  int right_idx = 0;

 #if ENABLE_STRASSEN_ALGORITHM
  /**/
 #elif ENABLE_COPPERSMITH_WINOGRAD_ALGORITHM
 #else
  /* @NOTE: naive algorithm will be used by default */

  if (gid < dims[0] * dims[2]){
    for (; i < dims[1]; ++i){
        left_idx  = y*dims[1] + i;
        right_idx = i*dims[0] + x;

        outputs[gid] += left[left_idx] * right[right_idx];
    }
  }
 #endif
}

kernel void IRR_Buffer_MatMul_Forward(
  const device int* left        [[ buffer(0) ]],
  const device float* right     [[ buffer(1) ]],
  const device uint* dims       [[ buffer(2) ]],
  device float* outputs         [[ buffer(3) ]],
  uint gid   [[ thread_position_in_grid ]])
{
  int x = gid%dims[0];
  int y = gid/dims[0];
  int i = 0;

  int lbeg = 0;
  int rbeg = 0;
  int left_idx = 0;
  int right_idx = 0;

 #if ENABLE_STRASSEN_ALGORITHM
  /**/
 #elif ENABLE_COPPERSMITH_WINOGRAD_ALGORITHM
 #else
  /* @NOTE: naive algorithm will be used by default */

  if (gid < dims[0] * dims[2]){
    for (; i < dims[1]; ++i){
        left_idx  = y*dims[1] + i;
        right_idx = i*dims[0] + x;

        outputs[gid] += left[left_idx] * right[right_idx];
    }
  }
 #endif
}

kernel void RII_Buffer_MatMul_Forward(
  const device float* left      [[ buffer(0) ]],
  const device int* right       [[ buffer(1) ]],
  const device uint* dims       [[ buffer(2) ]],
  device int* outputs           [[ buffer(3) ]],
  uint gid   [[ thread_position_in_grid ]])
{
  int x = gid%dims[0];
  int y = gid/dims[0];
  int i = 0;

  int lbeg = 0;
  int rbeg = 0;
  int left_idx = 0;
  int right_idx = 0;

 #if ENABLE_STRASSEN_ALGORITHM
  /**/
 #elif ENABLE_COPPERSMITH_WINOGRAD_ALGORITHM
 #else
  /* @NOTE: naive algorithm will be used by default */

  if (gid < dims[0] * dims[2]){
    for (; i < dims[1]; ++i){
        left_idx  = y*dims[1] + i;
        right_idx = i*dims[0] + x;

        outputs[gid] += left[left_idx] * right[right_idx];
    }
  }
 #endif
}

kernel void IIR_Buffer_MatMul_Forward(
  const device int* left        [[ buffer(0) ]],
  const device int* right       [[ buffer(1) ]],
  const device uint* dims       [[ buffer(2) ]],
  device float* outputs         [[ buffer(3) ]],
  uint gid   [[ thread_position_in_grid ]])
{
  int x = gid%dims[0];
  int y = gid/dims[0];
  int i = 0;

  int lbeg = 0;
  int rbeg = 0;
  int left_idx = 0;
  int right_idx = 0;

 #if ENABLE_STRASSEN_ALGORITHM
  /**/
 #elif ENABLE_COPPERSMITH_WINOGRAD_ALGORITHM
 #else
  /* @NOTE: naive algorithm will be used by default */

  if (gid < dims[0] * dims[2]){
    for (; i < dims[1]; ++i){
        left_idx  = y*dims[1] + i;
        right_idx = i*dims[0] + x;

        outputs[gid] += left[left_idx] * right[right_idx];
    }
  }
 #endif
}

kernel void IRI_Buffer_MatMul_Forward(
  const device int* left        [[ buffer(0) ]],
  const device float* right     [[ buffer(1) ]],
  const device uint* dims       [[ buffer(2) ]],
  device int* outputs           [[ buffer(3) ]],
  uint gid   [[ thread_position_in_grid ]])
{
  int x = gid%dims[0];
  int y = gid/dims[0];
  int i = 0;

  int lbeg = 0;
  int rbeg = 0;
  int left_idx = 0;
  int right_idx = 0;

 #if ENABLE_STRASSEN_ALGORITHM
  /**/
 #elif ENABLE_COPPERSMITH_WINOGRAD_ALGORITHM
 #else
  /* @NOTE: naive algorithm will be used by default */

  if (gid < dims[0] * dims[2]){
    for (; i < dims[1]; ++i){
        left_idx  = y*dims[1] + i;
        right_idx = i*dims[0] + x;

        outputs[gid] += left[left_idx] * right[right_idx];
    }
  }
 #endif
}

kernel void III_Buffer_MatMul_Forward(
  const device int* left        [[ buffer(0) ]],
  const device int* right       [[ buffer(1) ]],
  const device uint* dims       [[ buffer(2) ]],
  device int* outputs         [[ buffer(3) ]],
  uint gid   [[ thread_position_in_grid ]])
{
  int x = gid%dims[0];
  int y = gid/dims[0];
  int i = 0;

  int lbeg = 0;
  int rbeg = 0;
  int left_idx = 0;
  int right_idx = 0;

 #if ENABLE_STRASSEN_ALGORITHM
  /**/
 #elif ENABLE_COPPERSMITH_WINOGRAD_ALGORITHM
 #else
  /* @NOTE: naive algorithm will be used by default */

  if (gid < dims[0] * dims[2]){
    for (; i < dims[1]; ++i){
        left_idx  = y*dims[1] + i;
        right_idx = i*dims[0] + x;

        outputs[gid] += left[left_idx] * right[right_idx];
    }
  }
 #endif
}
