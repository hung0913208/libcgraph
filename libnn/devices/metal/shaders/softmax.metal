#include <metal_stdlib>

using namespace metal;
kernel void RR_Buffer_Softmax_Forward(
    const device float* inputs   [[ buffer(0) ]],
   	const device int* max_input  [[ buffer(1) ]],
  	device float* outputs        [[ buffer(2) ]],
    uint i         [[ thread_position_in_grid ]])
{
  float Z = 0;

  for (int i = 0; i < max_input[0]; ++i)
    Z = Z + exp(inputs[i]);
  outputs[i] = exp(inputs[i]) / Z;
} 