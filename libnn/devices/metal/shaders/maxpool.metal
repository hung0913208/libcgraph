
#include <metal_stdlib>

using namespace metal;

kernel void RR_Buffer_MaxPool_Forward_NHWC(
  const device float* input        [[ buffer(0) ]],
  const device int* ptr_ksize_N    [[ buffer(2) ]],
  const device int* ptr_ksize_H    [[ buffer(3) ]],
  const device int* ptr_ksize_W    [[ buffer(4) ]],
  const device int* ptr_ksize_C    [[ buffer(5) ]],
  const device int* ptr_strides_N  [[ buffer(6) ]],
  const device int* ptr_strides_H  [[ buffer(7) ]],
  const device int* ptr_strides_W  [[ buffer(8) ]],
  const device int* ptr_strides_C  [[ buffer(9) ]],
  const device int* ptr_input_N    [[ buffer(10) ]],
  const device int* ptr_input_H    [[ buffer(11) ]],
  const device int* ptr_input_W    [[ buffer(12) ]],
  const device int* ptr_input_C    [[ buffer(13) ]],
  const device int* ptr_output_N   [[ buffer(14) ]],
  const device int* ptr_output_H   [[ buffer(15) ]],
  const device int* ptr_output_W   [[ buffer(16) ]],
  const device int* ptr_output_C   [[ buffer(17) ]],
  const device int* ptr_padding_H  [[ buffer(18) ]],
  const device int* ptr_padding_W  [[ buffer(19) ]],
  device float* output             [[ buffer(1) ]],
  uint  gid          [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int W = *ptr_output_W;
  int H = *ptr_output_H;
  int C = *ptr_output_C;

  int ksize_H = *ptr_ksize_H;
  int ksize_W = *ptr_ksize_W;

  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int out_N = ((gid / C) / W) / H;
  int out_H = ((gid / C) / W) % H;
  int out_W = ((gid / C)) % W;
  int out_C = gid % C;

  bool  first   = true;
  float max_val = 0;

  int shift_W = in_C;
  int shift_H = in_C*in_W;

  int origin_in_H = out_H*strides_H - padding_H;
  int origin_in_W = out_W*strides_W - padding_W;

  int begin  = out_N*in_W*in_H*in_C;
  int hstart = (origin_in_H + max(0, -origin_in_H))*shift_H;
  int wstart = (origin_in_W + max(0, -origin_in_W))*shift_W;
  int hend   = (origin_in_H + min(ksize_H, in_H - origin_in_H))*shift_H;
  int wend   = (origin_in_W + min(ksize_W, in_W - origin_in_W))*shift_W;

  if (gid < N*H*W*C){
    for (int h = hstart; h < hend; h += shift_H){
      for (int w = wstart; w < wend; w += shift_W){
        auto curr = input[begin + h + w + out_C];

        if (first) first = false;
        else if (curr <= max_val)
          continue;
        max_val = curr;
      }
    }
    output[gid] = max_val;
  }
}

kernel void RR_Buffer_MaxPool_Forward_NCHW(
  const device float* input        [[ buffer(0) ]],
  const device int* ptr_ksize_N    [[ buffer(2) ]],
  const device int* ptr_ksize_H    [[ buffer(3) ]],
  const device int* ptr_ksize_W    [[ buffer(4) ]],
  const device int* ptr_ksize_C    [[ buffer(5) ]],
  const device int* ptr_strides_N  [[ buffer(6) ]],
  const device int* ptr_strides_H  [[ buffer(7) ]],
  const device int* ptr_strides_W  [[ buffer(8) ]],
  const device int* ptr_strides_C  [[ buffer(9) ]],
  const device int* ptr_input_N    [[ buffer(10) ]],
  const device int* ptr_input_H    [[ buffer(11) ]],
  const device int* ptr_input_W    [[ buffer(12) ]],
  const device int* ptr_input_C    [[ buffer(13) ]],
  const device int* ptr_output_N   [[ buffer(14) ]],
  const device int* ptr_output_H   [[ buffer(15) ]],
  const device int* ptr_output_W   [[ buffer(16) ]],
  const device int* ptr_output_C   [[ buffer(17) ]],
  const device int* ptr_padding_H  [[ buffer(18) ]],
  const device int* ptr_padding_W  [[ buffer(19) ]],
  device float* output             [[ buffer(1) ]],
  uint  gid           [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int W = *ptr_output_W;
  int H = *ptr_output_H;
  int C = *ptr_output_C;

  int ksize_H = *ptr_ksize_H;
  int ksize_W = *ptr_ksize_W;

  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;
  int strides_C = *ptr_strides_C;

  int in_N = *ptr_input_N;
  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int out_N = ((gid / W) / H) / C;
  int out_C = ((gid / W) / H) % C;
  int out_H = ((gid / W)) % H;
  int out_W = gid % W;

  int origin_in_H = int(out_H*strides_H - padding_H);
  int origin_in_W = int(out_W*strides_W - padding_W);

  bool  first = true;
  float max_val   = 0;

  int begin  = out_N*in_W*in_H*in_C*in_N + out_C*in_W*in_H;
  int hstart = max(0, -origin_in_H);
  int hend   = min(ksize_H, in_H - origin_in_H);
  int wstart = max(0, -origin_in_W);
  int wend   = min(ksize_W, in_W - origin_in_W);

  if (gid < N*H*W*C){
    for (int i = hstart, h = begin + in_W*(hstart + origin_in_H); 
             i < hend; ++i, h += in_W){
      for (int j = h + origin_in_W + wstart; j < h + origin_in_W + wend; ++j){
        float curr = input[j];

        if (first) first = false;
        else if (max_val >= curr)
          continue;

        max_val = curr;
      }
    }
    output[gid] = max_val;
  }
}

kernel void II_Buffer_MaxPool_Forward_NHWC(
  const device int* input        [[ buffer(0) ]],
  const device int* ptr_ksize_N    [[ buffer(2) ]],
  const device int* ptr_ksize_H    [[ buffer(3) ]],
  const device int* ptr_ksize_W    [[ buffer(4) ]],
  const device int* ptr_ksize_C    [[ buffer(5) ]],
  const device int* ptr_strides_N  [[ buffer(6) ]],
  const device int* ptr_strides_H  [[ buffer(7) ]],
  const device int* ptr_strides_W  [[ buffer(8) ]],
  const device int* ptr_strides_C  [[ buffer(9) ]],
  const device int* ptr_input_N    [[ buffer(10) ]],
  const device int* ptr_input_H    [[ buffer(11) ]],
  const device int* ptr_input_W    [[ buffer(12) ]],
  const device int* ptr_input_C    [[ buffer(13) ]],
  const device int* ptr_output_N   [[ buffer(14) ]],
  const device int* ptr_output_H   [[ buffer(15) ]],
  const device int* ptr_output_W   [[ buffer(16) ]],
  const device int* ptr_output_C   [[ buffer(17) ]],
  const device int* ptr_padding_H  [[ buffer(18) ]],
  const device int* ptr_padding_W  [[ buffer(19) ]],
  device int* output             [[ buffer(1) ]],
  uint  gid           [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int W = *ptr_output_W;
  int H = *ptr_output_H;
  int C = *ptr_output_C;

  int ksize_H = *ptr_ksize_H;
  int ksize_W = *ptr_ksize_W;

  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int out_N = ((gid / C) / W) / H;
  int out_H = ((gid / C) / W) % H;
  int out_W = ((gid / C)) % W;
  int out_C = gid % C;

  bool  first = true;
  float max_val   = 0;

  int shift_W = in_C;
  int shift_H = in_C*in_W;

  int origin_in_H = out_H*strides_H - padding_H;
  int origin_in_W = out_W*strides_W - padding_W;

  int begin  = out_N*in_W*in_H*in_C;
  int hstart = (origin_in_H + max(0, -origin_in_H))*shift_H;
  int wstart = (origin_in_W + max(0, -origin_in_W))*shift_W;
  int hend   = (origin_in_H + min(ksize_H, in_H - origin_in_H))*shift_H;
  int wend   = (origin_in_W + min(ksize_W, in_W - origin_in_W))*shift_W;

  if (gid < N*H*W*C){
    for (int h = hstart; h < hend; h += shift_H){
      for (int w = wstart; w < wend; w += shift_W){
        auto curr = input[begin + h + w + out_C];

        if (first) first = false;
        else if (curr <= max_val)
          continue;
        max_val = curr;
      }
    }
    output[gid] = max_val;
  }
}

kernel void II_Buffer_MaxPool_Forward_NCHW(
  const device float* input        [[ buffer(0) ]],
  const device int* ptr_ksize_N    [[ buffer(2) ]],
  const device int* ptr_ksize_H    [[ buffer(3) ]],
  const device int* ptr_ksize_W    [[ buffer(4) ]],
  const device int* ptr_ksize_C    [[ buffer(5) ]],
  const device int* ptr_strides_N  [[ buffer(6) ]],
  const device int* ptr_strides_H  [[ buffer(7) ]],
  const device int* ptr_strides_W  [[ buffer(8) ]],
  const device int* ptr_strides_C  [[ buffer(9) ]],
  const device int* ptr_input_N    [[ buffer(10) ]],
  const device int* ptr_input_H    [[ buffer(11) ]],
  const device int* ptr_input_W    [[ buffer(12) ]],
  const device int* ptr_input_C    [[ buffer(13) ]],
  const device int* ptr_output_N   [[ buffer(14) ]],
  const device int* ptr_output_H   [[ buffer(15) ]],
  const device int* ptr_output_W   [[ buffer(16) ]],
  const device int* ptr_output_C   [[ buffer(17) ]],
  const device int* ptr_padding_H  [[ buffer(18) ]],
  const device int* ptr_padding_W  [[ buffer(19) ]],
  device int* output             [[ buffer(1) ]],
  uint  gid           [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int W = *ptr_output_W;
  int H = *ptr_output_H;
  int C = *ptr_output_C;

  int ksize_H = *ptr_ksize_H;
  int ksize_W = *ptr_ksize_W;

  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;
  int strides_C = *ptr_strides_C;

  int in_N = *ptr_input_N;
  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int out_N = ((gid / W) / H) / C;
  int out_C = ((gid / W) / H) % C;
  int out_H = ((gid / W)) % H;
  int out_W = gid % W;

  int origin_in_H = int(out_H*strides_H - padding_H);
  int origin_in_W = int(out_W*strides_W - padding_W);

  bool  first = true;
  float max_val   = 0;

  int begin  = out_N*in_W*in_H*in_C*in_N + out_C*in_W*in_H;
  int hstart = max(0, -origin_in_H);
  int hend   = min(ksize_H, in_H - origin_in_H);
  int wstart = max(0, -origin_in_W);
  int wend   = min(ksize_W, in_W - origin_in_W);

  if (gid < N*H*W*C){
    for (int i = hstart, h = begin + in_W*(hstart + origin_in_H); 
             i < hend; ++i, h += in_W){
      for (int j = h + origin_in_W + wstart; j < h + origin_in_W + wend; ++j){
        float curr = input[j];

        if (first) first = false;
        else if (max_val >= curr)
          continue;

        max_val = curr;
      }
    }
    output[gid] = max_val;
  }

}

kernel void RI_Buffer_MaxPool_Forward_NHWC(
  const device float* input        [[ buffer(0) ]],
  const device int* ptr_ksize_N    [[ buffer(2) ]],
  const device int* ptr_ksize_H    [[ buffer(3) ]],
  const device int* ptr_ksize_W    [[ buffer(4) ]],
  const device int* ptr_ksize_C    [[ buffer(5) ]],
  const device int* ptr_strides_N  [[ buffer(6) ]],
  const device int* ptr_strides_H  [[ buffer(7) ]],
  const device int* ptr_strides_W  [[ buffer(8) ]],
  const device int* ptr_strides_C  [[ buffer(9) ]],
  const device int* ptr_input_N    [[ buffer(10) ]],
  const device int* ptr_input_H    [[ buffer(11) ]],
  const device int* ptr_input_W    [[ buffer(12) ]],
  const device int* ptr_input_C    [[ buffer(13) ]],
  const device int* ptr_output_N   [[ buffer(14) ]],
  const device int* ptr_output_H   [[ buffer(15) ]],
  const device int* ptr_output_W   [[ buffer(16) ]],
  const device int* ptr_output_C   [[ buffer(17) ]],
  const device int* ptr_padding_H  [[ buffer(18) ]],
  const device int* ptr_padding_W  [[ buffer(19) ]],
  device int* output               [[ buffer(1) ]],
  uint  gid           [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int W = *ptr_output_W;
  int H = *ptr_output_H;
  int C = *ptr_output_C;

  int ksize_H = *ptr_ksize_H;
  int ksize_W = *ptr_ksize_W;

  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int out_N = ((gid / C) / W) / H;
  int out_H = ((gid / C) / W) % H;
  int out_W = ((gid / C)) % W;
  int out_C = gid % C;

  bool  first = true;
  float max_val   = 0;

  int shift_W = in_C;
  int shift_H = in_C*in_W;

  int origin_in_H = out_H*strides_H - padding_H;
  int origin_in_W = out_W*strides_W - padding_W;

  int begin  = out_N*in_W*in_H*in_C;
  int hstart = (origin_in_H + max(0, -origin_in_H))*shift_H;
  int wstart = (origin_in_W + max(0, -origin_in_W))*shift_W;
  int hend   = (origin_in_H + min(ksize_H, in_H - origin_in_H))*shift_H;
  int wend   = (origin_in_W + min(ksize_W, in_W - origin_in_W))*shift_W;

  if (gid < N*H*W*C){
    for (int h = hstart; h < hend; h += shift_H){
      for (int w = wstart; w < wend; w += shift_W){
        auto curr = input[begin + h + w + out_C];

        if (first) first = false;
        else if (curr <= max_val)
          continue;
        max_val = curr;
      }
    }
    output[gid] = max_val;
  }
}

kernel void RI_Buffer_MaxPool_Forward_NCHW(
  const device float* input        [[ buffer(0) ]],
  const device int* ptr_ksize_N    [[ buffer(2) ]],
  const device int* ptr_ksize_H    [[ buffer(3) ]],
  const device int* ptr_ksize_W    [[ buffer(4) ]],
  const device int* ptr_ksize_C    [[ buffer(5) ]],
  const device int* ptr_strides_N  [[ buffer(6) ]],
  const device int* ptr_strides_H  [[ buffer(7) ]],
  const device int* ptr_strides_W  [[ buffer(8) ]],
  const device int* ptr_strides_C  [[ buffer(9) ]],
  const device int* ptr_input_N    [[ buffer(10) ]],
  const device int* ptr_input_H    [[ buffer(11) ]],
  const device int* ptr_input_W    [[ buffer(12) ]],
  const device int* ptr_input_C    [[ buffer(13) ]],
  const device int* ptr_output_N   [[ buffer(14) ]],
  const device int* ptr_output_H   [[ buffer(15) ]],
  const device int* ptr_output_W   [[ buffer(16) ]],
  const device int* ptr_output_C   [[ buffer(17) ]],
  const device int* ptr_padding_H  [[ buffer(18) ]],
  const device int* ptr_padding_W  [[ buffer(19) ]],
  device int* output               [[ buffer(1) ]],
  uint  gid           [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int W = *ptr_output_W;
  int H = *ptr_output_H;
  int C = *ptr_output_C;

  int ksize_H = *ptr_ksize_H;
  int ksize_W = *ptr_ksize_W;

  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;
  int strides_C = *ptr_strides_C;

  int in_N = *ptr_input_N;
  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int out_N = ((gid / W) / H) / C;
  int out_C = ((gid / W) / H) % C;
  int out_H = ((gid / W)) % H;
  int out_W = gid % W;

  int origin_in_H = int(out_H*strides_H - padding_H);
  int origin_in_W = int(out_W*strides_W - padding_W);

  bool  first = true;
  float max_val   = 0;

  int begin  = out_N*in_W*in_H*in_C*in_N + out_C*in_W*in_H;
  int hstart = max(0, -origin_in_H);
  int hend   = min(ksize_H, in_H - origin_in_H);
  int wstart = max(0, -origin_in_W);
  int wend   = min(ksize_W, in_W - origin_in_W);

  if (gid < N*H*W*C){
    for (int i = hstart, h = begin + in_W*(hstart + origin_in_H); 
             i < hend; ++i, h += in_W){
      for (int j = h + origin_in_W + wstart; j < h + origin_in_W + wend; ++j){
        float curr = input[j];

        if (first) first = false;
        else if (max_val >= curr)
          continue;

        max_val = curr;
      }
    }
    output[gid] = max_val;
  }
}

kernel void IR_Buffer_MaxPool_Forward_NHWC(
  const device int* input        [[ buffer(0) ]],
  const device int* ptr_ksize_N    [[ buffer(2) ]],
  const device int* ptr_ksize_H    [[ buffer(3) ]],
  const device int* ptr_ksize_W    [[ buffer(4) ]],
  const device int* ptr_ksize_C    [[ buffer(5) ]],
  const device int* ptr_strides_N  [[ buffer(6) ]],
  const device int* ptr_strides_H  [[ buffer(7) ]],
  const device int* ptr_strides_W  [[ buffer(8) ]],
  const device int* ptr_strides_C  [[ buffer(9) ]],
  const device int* ptr_input_N    [[ buffer(10) ]],
  const device int* ptr_input_H    [[ buffer(11) ]],
  const device int* ptr_input_W    [[ buffer(12) ]],
  const device int* ptr_input_C    [[ buffer(13) ]],
  const device int* ptr_output_N   [[ buffer(14) ]],
  const device int* ptr_output_H   [[ buffer(15) ]],
  const device int* ptr_output_W   [[ buffer(16) ]],
  const device int* ptr_output_C   [[ buffer(17) ]],
  const device int* ptr_padding_H  [[ buffer(18) ]],
  const device int* ptr_padding_W  [[ buffer(19) ]],
  device float* output             [[ buffer(1) ]],
  uint  gid           [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int W = *ptr_output_W;
  int H = *ptr_output_H;
  int C = *ptr_output_C;

  int ksize_H = *ptr_ksize_H;
  int ksize_W = *ptr_ksize_W;

  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int out_N = ((gid / C) / W) / H;
  int out_H = ((gid / C) / W) % H;
  int out_W = ((gid / C)) % W;
  int out_C = gid % C;

  bool  first = true;
  float max_val   = 0;

  int shift_W = in_C;
  int shift_H = in_C*in_W;

  int origin_in_H = out_H*strides_H - padding_H;
  int origin_in_W = out_W*strides_W - padding_W;

  int begin  = out_N*in_W*in_H*in_C;
  int hstart = (origin_in_H + max(0, -origin_in_H))*shift_H;
  int wstart = (origin_in_W + max(0, -origin_in_W))*shift_W;
  int hend   = (origin_in_H + min(ksize_H, in_H - origin_in_H))*shift_H;
  int wend   = (origin_in_W + min(ksize_W, in_W - origin_in_W))*shift_W;

  if (gid < N*H*W*C){
    for (int h = hstart; h < hend; h += shift_H){
      for (int w = wstart; w < wend; w += shift_W){
        auto curr = input[begin + h + w + out_C];

        if (first) first = false;
        else if (curr <= max_val)
          continue;
        max_val = curr;
      }
    }
    output[gid] = max_val;
  }
}

kernel void IR_Buffer_MaxPool_Forward_NCHW(
  const device int* input        [[ buffer(0) ]],
  const device int* ptr_ksize_N    [[ buffer(2) ]],
  const device int* ptr_ksize_H    [[ buffer(3) ]],
  const device int* ptr_ksize_W    [[ buffer(4) ]],
  const device int* ptr_ksize_C    [[ buffer(5) ]],
  const device int* ptr_strides_N  [[ buffer(6) ]],
  const device int* ptr_strides_H  [[ buffer(7) ]],
  const device int* ptr_strides_W  [[ buffer(8) ]],
  const device int* ptr_strides_C  [[ buffer(9) ]],
  const device int* ptr_input_N    [[ buffer(10) ]],
  const device int* ptr_input_H    [[ buffer(11) ]],
  const device int* ptr_input_W    [[ buffer(12) ]],
  const device int* ptr_input_C    [[ buffer(13) ]],
  const device int* ptr_output_N   [[ buffer(14) ]],
  const device int* ptr_output_H   [[ buffer(15) ]],
  const device int* ptr_output_W   [[ buffer(16) ]],
  const device int* ptr_output_C   [[ buffer(17) ]],
  const device int* ptr_padding_H  [[ buffer(18) ]],
  const device int* ptr_padding_W  [[ buffer(19) ]],
  device float* output             [[ buffer(1) ]],
  uint  gid           [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int W = *ptr_output_W;
  int H = *ptr_output_H;
  int C = *ptr_output_C;

  int ksize_H = *ptr_ksize_H;
  int ksize_W = *ptr_ksize_W;

  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;
  int strides_C = *ptr_strides_C;

  int in_N = *ptr_input_N;
  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int out_N = ((gid / W) / H) / C;
  int out_C = ((gid / W) / H) % C;
  int out_H = ((gid / W)) % H;
  int out_W = gid % W;

  int origin_in_H = int(out_H*strides_H - padding_H);
  int origin_in_W = int(out_W*strides_W - padding_W);

  bool  first = true;
  float max_val   = 0;

  int begin  = out_N*in_W*in_H*in_C*in_N + out_C*in_W*in_H;
  int hstart = max(0, -origin_in_H);
  int hend   = min(ksize_H, in_H - origin_in_H);
  int wstart = max(0, -origin_in_W);
  int wend   = min(ksize_W, in_W - origin_in_W);

  if (gid < N*H*W*C){
    for (int i = hstart, h = begin + in_W*(hstart + origin_in_H); 
             i < hend; ++i, h += in_W){
      for (int j = h + origin_in_W + wstart; j < h + origin_in_W + wend; ++j){
        float curr = input[j];

        if (first) first = false;
        else if (max_val >= curr)
          continue;

        max_val = curr;
      }
    }
    output[gid] = max_val;
  }
}
