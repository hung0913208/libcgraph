#include <metal_stdlib>
using namespace metal;

kernel void Buffer_Sigmoid_Forward(const device float *input    [[ buffer(0) ]],
                                   device float *output         [[ buffer(1) ]],
                                   uint id        [[ thread_position_in_grid ]])
{
    output[id] = 1.0 / (1.0 + exp(-input[id]));
}

kernel void Buffer_Rectifier_Forward(const device float *input  [[ buffer(0) ]],
                                     device float *output       [[ buffer(1) ]],
                                     uint id      [[ thread_position_in_grid ]])
{
    output[id] = max(0.0, input[id]);
}

kernel void Buffer_Tanh_Forward(const device float *input       [[ buffer(0) ]],
                                device float *output            [[ buffer(1) ]],
                                uint id           [[ thread_position_in_grid ]])
{
    output[id] = tanh(input[id]);
}