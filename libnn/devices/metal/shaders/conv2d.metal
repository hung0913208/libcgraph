#include <metal_stdlib>

using namespace metal;

kernel void RRR_Buffer_Conv2D_Forward_NHWC(
  const device float *inputs       [[ buffer(0) ]],
  const device float *filter       [[ buffer(1) ]],
  const device int* ptr_input_N    [[ buffer(3) ]],
  const device int* ptr_input_H    [[ buffer(4) ]],
  const device int* ptr_input_W    [[ buffer(5) ]],
  const device int* ptr_input_C    [[ buffer(6) ]],
  const device int* ptr_filter_H   [[ buffer(7) ]],
  const device int* ptr_filter_W   [[ buffer(8) ]],
  const device int* ptr_filter_iC  [[ buffer(9) ]],
  const device int* ptr_filter_oC  [[ buffer(10) ]],
  const device int* ptr_output_N   [[ buffer(11) ]],
  const device int* ptr_output_H   [[ buffer(12) ]],
  const device int* ptr_output_W   [[ buffer(13) ]],
  const device int* ptr_output_C   [[ buffer(14) ]],
  const device int* ptr_strides_N   [[ buffer(15) ]],
  const device int* ptr_strides_H   [[ buffer(16) ]],
  const device int* ptr_strides_W   [[ buffer(17) ]],
  const device int* ptr_strides_C   [[ buffer(18) ]],
  const device int* ptr_dilate_H   [[ buffer(19) ]],
  const device int* ptr_dilate_W   [[ buffer(20) ]],
  const device int* ptr_padding_H  [[ buffer(21) ]],
  const device int* ptr_padding_W  [[ buffer(22) ]],
  device float* output             [[ buffer(2) ]],
  uint gid     [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int H = *ptr_output_H;
  int W = *ptr_output_W;
  int C = *ptr_output_C;

  int filter_H = *ptr_filter_H;
  int filter_W = *ptr_filter_W;
  int filter_iC = *ptr_filter_iC;
  int filter_oC = *ptr_filter_oC;

  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int dilate_H = *ptr_dilate_H;
  int dilate_W = *ptr_dilate_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int out_N = ((gid / C) / W) / H;
  int out_H = ((gid / C) / W) % H;
  int out_W = ((gid / C)) % W;
  int out_C = gid % C;

  int out_size = N*H*W*C;
  int shift_W  = in_C;
  int shift_H  = in_C * in_W;

  int begin  = out_N * in_W * in_H * in_C;
  int hstart = (out_H * strides_H - padding_H) * shift_H;
  int wstart = (out_W * strides_W - padding_W) * shift_W;
  int hend   = (out_H * strides_H + filter_H - padding_H) * shift_H;
  int wend   = (out_W * strides_W + filter_W - padding_W) * shift_W;

  int fblock_N    = filter_iC*filter_oC;
  int origin_in_H = out_H * strides_H - padding_H;
  int origin_in_W = out_W * strides_W - padding_W;

  if (gid < out_size){
    for (int i = 0, h = hstart; h < hend; ++i, h += dilate_H*shift_H) {
      for (int j = 0, w = wstart; w < wend; ++j, w += dilate_W*shift_W) {
        int in_X = i*dilate_H + origin_in_H;
        int in_Y = j*dilate_W + origin_in_W;

        if (in_X >= 0 && in_Y >= 0 && in_X < in_H && in_Y < in_W){
          for (int c = 0; c < filter_iC; ++c){
            int ffixed = c*filter_oC + out_C;

            output[gid] += inputs[begin + h + w + c] *
                           filter[(i*filter_W + j)*fblock_N + ffixed];
          }
        }
      }
    }
  }
}

kernel void RRR_Buffer_Conv2D_Forward_NCHW(
  const device float *inputs       [[ buffer(0) ]],
  const device float *filter       [[ buffer(1) ]],
  const device int* ptr_input_N    [[ buffer(3) ]],
  const device int* ptr_input_H    [[ buffer(4) ]],
  const device int* ptr_input_W    [[ buffer(5) ]],
  const device int* ptr_input_C    [[ buffer(6) ]],
  const device int* ptr_filter_H   [[ buffer(7) ]],
  const device int* ptr_filter_W   [[ buffer(8) ]],
  const device int* ptr_filter_iC  [[ buffer(9) ]],
  const device int* ptr_filter_oC  [[ buffer(10) ]],
  const device int* ptr_output_N   [[ buffer(11) ]],
  const device int* ptr_output_H   [[ buffer(12) ]],
  const device int* ptr_output_W   [[ buffer(13) ]],
  const device int* ptr_output_C   [[ buffer(14) ]],
  const device int* ptr_strides_N   [[ buffer(15) ]],
  const device int* ptr_strides_H   [[ buffer(16) ]],
  const device int* ptr_strides_W   [[ buffer(17) ]],
  const device int* ptr_strides_C   [[ buffer(18) ]],
  const device int* ptr_dilate_H   [[ buffer(19) ]],
  const device int* ptr_dilate_W   [[ buffer(20) ]],
  const device int* ptr_padding_H  [[ buffer(21) ]],
  const device int* ptr_padding_W  [[ buffer(22) ]],
  device float* output             [[ buffer(2) ]],
  uint gid     [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int H = *ptr_output_H;
  int W = *ptr_output_W;
  int C = *ptr_output_C;

  int filter_H  = *ptr_filter_H;
  int filter_W  = *ptr_filter_W;
  int filter_iC = *ptr_filter_iC;
  int filter_oC = *ptr_filter_oC;

  int strides_N = *ptr_strides_N;
  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int dilate_H = *ptr_dilate_H;
  int dilate_W = *ptr_dilate_W;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int out_N = ((gid / W) / H) / C;
  int out_C = ((gid / W) / H) % C;
  int out_H = ((gid / W)) % H;
  int out_W = gid % W;

  int iblock_N = out_N * in_W * in_H * in_C;
  int fblock_N = filter_iC * filter_oC;

  int origin_in_H = out_H * strides_H - padding_H;
  int origin_in_W = out_W * strides_W - padding_W;

  if (gid < N*H*W*C){
    for (int i = 0; i < filter_H; ++i) {
      for (int j = 0; j < filter_W; ++j){
        int in_X = j*dilate_W + origin_in_W;
        int in_Y = i*dilate_H + origin_in_H;
      
        if (in_X >= 0 && in_X < in_W && in_Y >= 0 && in_Y < in_H){
          for (int c = 0; c < filter_iC; ++c) {
            int ffixed = c*filter_oC + out_C;
            int cbegin = c*in_W*in_H;

            int in = inputs[iblock_N + cbegin + in_Y*in_W + in_X];
            int wn = filter[(i*filter_W + j)*fblock_N + ffixed];

            output[gid] += in * wn;
          }
        }
      }
    }
  }
}

kernel void RRI_Buffer_Conv2D_Forward_NHWC(
  const device float *inputs       [[ buffer(0) ]],
  const device float *filter       [[ buffer(1) ]],
  const device int* ptr_input_N    [[ buffer(3) ]],
  const device int* ptr_input_H    [[ buffer(4) ]],
  const device int* ptr_input_W    [[ buffer(5) ]],
  const device int* ptr_input_C    [[ buffer(6) ]],
  const device int* ptr_filter_H   [[ buffer(7) ]],
  const device int* ptr_filter_W   [[ buffer(8) ]],
  const device int* ptr_filter_iC  [[ buffer(9) ]],
  const device int* ptr_filter_oC  [[ buffer(10) ]],
  const device int* ptr_output_N   [[ buffer(11) ]],
  const device int* ptr_output_H   [[ buffer(12) ]],
  const device int* ptr_output_W   [[ buffer(13) ]],
  const device int* ptr_output_C   [[ buffer(14) ]],
  const device int* ptr_strides_N   [[ buffer(15) ]],
  const device int* ptr_strides_H   [[ buffer(16) ]],
  const device int* ptr_strides_W   [[ buffer(17) ]],
  const device int* ptr_strides_C   [[ buffer(18) ]],
  const device int* ptr_dilate_H   [[ buffer(19) ]],
  const device int* ptr_dilate_W   [[ buffer(20) ]],
  const device int* ptr_padding_H  [[ buffer(21) ]],
  const device int* ptr_padding_W  [[ buffer(22) ]],
  device int* output               [[ buffer(2) ]],
  uint gid     [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int H = *ptr_output_H;
  int W = *ptr_output_W;
  int C = *ptr_output_C;

  int filter_H = *ptr_filter_H;
  int filter_W = *ptr_filter_W;
  int filter_iC = *ptr_filter_iC;
  int filter_oC = *ptr_filter_oC;

  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int dilate_H = *ptr_dilate_H;
  int dilate_W = *ptr_dilate_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int out_N = ((gid / C) / W) / H;
  int out_H = ((gid / C) / W) % H;
  int out_W = ((gid / C)) % W;
  int out_C = gid % C;

  int out_size = N*H*W*C;
  int shift_W  = in_C;
  int shift_H  = in_C * in_W;

  int begin  = out_N * in_W * in_H * in_C;
  int hstart = (out_H * strides_H - padding_H) * shift_H;
  int wstart = (out_W * strides_W - padding_W) * shift_W;
  int hend   = (out_H * strides_H + filter_H - padding_H) * shift_H;
  int wend   = (out_W * strides_W + filter_W - padding_W) * shift_W;

  int fblock_N    = filter_iC*filter_oC;
  int origin_in_H = out_H * strides_H - padding_H;
  int origin_in_W = out_W * strides_W - padding_W;

  if (gid < out_size){
    for (int i = 0, h = hstart; h < hend; ++i, h += dilate_H*shift_H) {
      for (int j = 0, w = wstart; w < wend; ++j, w += dilate_W*shift_W) {
        int in_X = i*dilate_H + origin_in_H;
        int in_Y = j*dilate_W + origin_in_W;

        if (in_X >= 0 && in_Y >= 0 && in_X < in_H && in_Y < in_W){
          for (int c = 0; c < filter_iC; ++c){
            int ffixed = c*filter_oC + out_C;

            output[gid] += inputs[begin + h + w + c] *
                           filter[(i*filter_W + j)*fblock_N + ffixed];
          }
        }
      }
    }
  }
}

kernel void RRI_Buffer_Conv2D_Forward_NCHW(
  const device float *inputs       [[ buffer(0) ]],
  const device float *filter       [[ buffer(1) ]],
  const device int* ptr_input_N    [[ buffer(3) ]],
  const device int* ptr_input_H    [[ buffer(4) ]],
  const device int* ptr_input_W    [[ buffer(5) ]],
  const device int* ptr_input_C    [[ buffer(6) ]],
  const device int* ptr_filter_H   [[ buffer(7) ]],
  const device int* ptr_filter_W   [[ buffer(8) ]],
  const device int* ptr_filter_iC  [[ buffer(9) ]],
  const device int* ptr_filter_oC  [[ buffer(10) ]],
  const device int* ptr_output_N   [[ buffer(11) ]],
  const device int* ptr_output_H   [[ buffer(12) ]],
  const device int* ptr_output_W   [[ buffer(13) ]],
  const device int* ptr_output_C   [[ buffer(14) ]],
  const device int* ptr_strides_N   [[ buffer(15) ]],
  const device int* ptr_strides_H   [[ buffer(16) ]],
  const device int* ptr_strides_W   [[ buffer(17) ]],
  const device int* ptr_strides_C   [[ buffer(18) ]],
  const device int* ptr_dilate_H   [[ buffer(19) ]],
  const device int* ptr_dilate_W   [[ buffer(20) ]],
  const device int* ptr_padding_H  [[ buffer(21) ]],
  const device int* ptr_padding_W  [[ buffer(22) ]],
  device int* output               [[ buffer(2) ]],
  uint gid     [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int H = *ptr_output_H;
  int W = *ptr_output_W;
  int C = *ptr_output_C;

  int filter_H  = *ptr_filter_H;
  int filter_W  = *ptr_filter_W;
  int filter_iC = *ptr_filter_iC;
  int filter_oC = *ptr_filter_oC;

  int strides_N = *ptr_strides_N;
  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int dilate_H = *ptr_dilate_H;
  int dilate_W = *ptr_dilate_W;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int out_N = ((gid / W) / H) / C;
  int out_C = ((gid / W) / H) % C;
  int out_H = ((gid / W)) % H;
  int out_W = gid % W;

  int iblock_N = out_N * in_W * in_H * in_C;
  int fblock_N = filter_iC * filter_oC;

  int origin_in_H = out_H * strides_H - padding_H;
  int origin_in_W = out_W * strides_W - padding_W;

  if (gid < N*H*W*C){
    for (int i = 0; i < filter_H; ++i) {
      for (int j = 0; j < filter_W; ++j){
        int in_X = j*dilate_W + origin_in_W;
        int in_Y = i*dilate_H + origin_in_H;
      
        if (in_X >= 0 && in_X < in_W && in_Y >= 0 && in_Y < in_H){
          for (int c = 0; c < filter_iC; ++c) {
            int ffixed = c*filter_oC + out_C;
            int cbegin = c*in_W*in_H;

            int in = inputs[iblock_N + cbegin + in_Y*in_W + in_X];
            int wn = filter[(i*filter_W + j)*fblock_N + ffixed];

            output[gid] += in * wn;
          }
        }
      }
    }
  }
}

kernel void RIR_Buffer_Conv2D_Forward_NHWC(
  const device float *inputs       [[ buffer(0) ]],
  const device int *filter         [[ buffer(1) ]],
  const device int* ptr_input_N    [[ buffer(3) ]],
  const device int* ptr_input_H    [[ buffer(4) ]],
  const device int* ptr_input_W    [[ buffer(5) ]],
  const device int* ptr_input_C    [[ buffer(6) ]],
  const device int* ptr_filter_H   [[ buffer(7) ]],
  const device int* ptr_filter_W   [[ buffer(8) ]],
  const device int* ptr_filter_iC  [[ buffer(9) ]],
  const device int* ptr_filter_oC  [[ buffer(10) ]],
  const device int* ptr_output_N   [[ buffer(11) ]],
  const device int* ptr_output_H   [[ buffer(12) ]],
  const device int* ptr_output_W   [[ buffer(13) ]],
  const device int* ptr_output_C   [[ buffer(14) ]],
  const device int* ptr_strides_N   [[ buffer(15) ]],
  const device int* ptr_strides_H   [[ buffer(16) ]],
  const device int* ptr_strides_W   [[ buffer(17) ]],
  const device int* ptr_strides_C   [[ buffer(18) ]],
  const device int* ptr_dilate_H   [[ buffer(19) ]],
  const device int* ptr_dilate_W   [[ buffer(20) ]],
  const device int* ptr_padding_H  [[ buffer(21) ]],
  const device int* ptr_padding_W  [[ buffer(22) ]],
  device float* output             [[ buffer(2) ]],
  uint gid     [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int H = *ptr_output_H;
  int W = *ptr_output_W;
  int C = *ptr_output_C;

  int filter_H = *ptr_filter_H;
  int filter_W = *ptr_filter_W;
  int filter_iC = *ptr_filter_iC;
  int filter_oC = *ptr_filter_oC;

  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int dilate_H = *ptr_dilate_H;
  int dilate_W = *ptr_dilate_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int out_N = ((gid / C) / W) / H;
  int out_H = ((gid / C) / W) % H;
  int out_W = ((gid / C)) % W;
  int out_C = gid % C;

  int out_size = N*H*W*C;
  int shift_W  = in_C;
  int shift_H  = in_C * in_W;

  int begin  = out_N * in_W * in_H * in_C;
  int hstart = (out_H * strides_H - padding_H) * shift_H;
  int wstart = (out_W * strides_W - padding_W) * shift_W;
  int hend   = (out_H * strides_H + filter_H - padding_H) * shift_H;
  int wend   = (out_W * strides_W + filter_W - padding_W) * shift_W;

  int fblock_N    = filter_iC*filter_oC;
  int origin_in_H = out_H * strides_H - padding_H;
  int origin_in_W = out_W * strides_W - padding_W;

  if (gid < out_size){
    for (int i = 0, h = hstart; h < hend; ++i, h += dilate_H*shift_H) {
      for (int j = 0, w = wstart; w < wend; ++j, w += dilate_W*shift_W) {
        int in_X = i*dilate_H + origin_in_H;
        int in_Y = j*dilate_W + origin_in_W;

        if (in_X >= 0 && in_Y >= 0 && in_X < in_H && in_Y < in_W){
          for (int c = 0; c < filter_iC; ++c){
            int ffixed = c*filter_oC + out_C;

            output[gid] += inputs[begin + h + w + c] *
                           filter[(i*filter_W + j)*fblock_N + ffixed];
          }
        }
      }
    }
  }
}

kernel void RIR_Buffer_Conv2D_Forward_NCHW(
  const device float *inputs       [[ buffer(0) ]],
  const device int *filter         [[ buffer(1) ]],
  const device int* ptr_input_N    [[ buffer(3) ]],
  const device int* ptr_input_H    [[ buffer(4) ]],
  const device int* ptr_input_W    [[ buffer(5) ]],
  const device int* ptr_input_C    [[ buffer(6) ]],
  const device int* ptr_filter_H   [[ buffer(7) ]],
  const device int* ptr_filter_W   [[ buffer(8) ]],
  const device int* ptr_filter_iC  [[ buffer(9) ]],
  const device int* ptr_filter_oC  [[ buffer(10) ]],
  const device int* ptr_output_N   [[ buffer(11) ]],
  const device int* ptr_output_H   [[ buffer(12) ]],
  const device int* ptr_output_W   [[ buffer(13) ]],
  const device int* ptr_output_C   [[ buffer(14) ]],
  const device int* ptr_strides_N   [[ buffer(15) ]],
  const device int* ptr_strides_H   [[ buffer(16) ]],
  const device int* ptr_strides_W   [[ buffer(17) ]],
  const device int* ptr_strides_C   [[ buffer(18) ]],
  const device int* ptr_dilate_H   [[ buffer(19) ]],
  const device int* ptr_dilate_W   [[ buffer(20) ]],
  const device int* ptr_padding_H  [[ buffer(21) ]],
  const device int* ptr_padding_W  [[ buffer(22) ]],
  device float* output             [[ buffer(2) ]],
  uint gid     [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int H = *ptr_output_H;
  int W = *ptr_output_W;
  int C = *ptr_output_C;

  int filter_H  = *ptr_filter_H;
  int filter_W  = *ptr_filter_W;
  int filter_iC = *ptr_filter_iC;
  int filter_oC = *ptr_filter_oC;

  int strides_N = *ptr_strides_N;
  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int dilate_H = *ptr_dilate_H;
  int dilate_W = *ptr_dilate_W;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int out_N = ((gid / W) / H) / C;
  int out_C = ((gid / W) / H) % C;
  int out_H = ((gid / W)) % H;
  int out_W = gid % W;

  int iblock_N = out_N * in_W * in_H * in_C;
  int fblock_N = filter_iC * filter_oC;

  int origin_in_H = out_H * strides_H - padding_H;
  int origin_in_W = out_W * strides_W - padding_W;

  if (gid < N*H*W*C){
    for (int i = 0; i < filter_H; ++i) {
      for (int j = 0; j < filter_W; ++j){
        int in_X = j*dilate_W + origin_in_W;
        int in_Y = i*dilate_H + origin_in_H;
      
        if (in_X >= 0 && in_X < in_W && in_Y >= 0 && in_Y < in_H){
          for (int c = 0; c < filter_iC; ++c) {
            int ffixed = c*filter_oC + out_C;
            int cbegin = c*in_W*in_H;

            int in = inputs[iblock_N + cbegin + in_Y*in_W + in_X];
            int wn = filter[(i*filter_W + j)*fblock_N + ffixed];

            output[gid] += in * wn;
          }
        }
      }
    }
  }
}

kernel void IRR_Buffer_Conv2D_Forward_NHWC(
  const device int *inputs         [[ buffer(0) ]],
  const device float *filter       [[ buffer(1) ]],
  const device int* ptr_input_N    [[ buffer(3) ]],
  const device int* ptr_input_H    [[ buffer(4) ]],
  const device int* ptr_input_W    [[ buffer(5) ]],
  const device int* ptr_input_C    [[ buffer(6) ]],
  const device int* ptr_filter_H   [[ buffer(7) ]],
  const device int* ptr_filter_W   [[ buffer(8) ]],
  const device int* ptr_filter_iC  [[ buffer(9) ]],
  const device int* ptr_filter_oC  [[ buffer(10) ]],
  const device int* ptr_output_N   [[ buffer(11) ]],
  const device int* ptr_output_H   [[ buffer(12) ]],
  const device int* ptr_output_W   [[ buffer(13) ]],
  const device int* ptr_output_C   [[ buffer(14) ]],
  const device int* ptr_strides_N   [[ buffer(15) ]],
  const device int* ptr_strides_H   [[ buffer(16) ]],
  const device int* ptr_strides_W   [[ buffer(17) ]],
  const device int* ptr_strides_C   [[ buffer(18) ]],
  const device int* ptr_dilate_H   [[ buffer(19) ]],
  const device int* ptr_dilate_W   [[ buffer(20) ]],
  const device int* ptr_padding_H  [[ buffer(21) ]],
  const device int* ptr_padding_W  [[ buffer(22) ]],
  device float* output             [[ buffer(2) ]],
  uint gid     [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int H = *ptr_output_H;
  int W = *ptr_output_W;
  int C = *ptr_output_C;

  int filter_H = *ptr_filter_H;
  int filter_W = *ptr_filter_W;
  int filter_iC = *ptr_filter_iC;
  int filter_oC = *ptr_filter_oC;

  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int dilate_H = *ptr_dilate_H;
  int dilate_W = *ptr_dilate_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int out_N = ((gid / C) / W) / H;
  int out_H = ((gid / C) / W) % H;
  int out_W = ((gid / C)) % W;
  int out_C = gid % C;

  int out_size = N*H*W*C;
  int shift_W  = in_C;
  int shift_H  = in_C * in_W;

  int begin  = out_N * in_W * in_H * in_C;
  int hstart = (out_H * strides_H - padding_H) * shift_H;
  int wstart = (out_W * strides_W - padding_W) * shift_W;
  int hend   = (out_H * strides_H + filter_H - padding_H) * shift_H;
  int wend   = (out_W * strides_W + filter_W - padding_W) * shift_W;

  int fblock_N    = filter_iC*filter_oC;
  int origin_in_H = out_H * strides_H - padding_H;
  int origin_in_W = out_W * strides_W - padding_W;

  if (gid < out_size){
    for (int i = 0, h = hstart; h < hend; ++i, h += dilate_H*shift_H) {
      for (int j = 0, w = wstart; w < wend; ++j, w += dilate_W*shift_W) {
        int in_X = i*dilate_H + origin_in_H;
        int in_Y = j*dilate_W + origin_in_W;

        if (in_X >= 0 && in_Y >= 0 && in_X < in_H && in_Y < in_W){
          for (int c = 0; c < filter_iC; ++c){
            int ffixed = c*filter_oC + out_C;

            output[gid] += inputs[begin + h + w + c] *
                           filter[(i*filter_W + j)*fblock_N + ffixed];
          }
        }
      }
    }
  }
}

kernel void IRR_Buffer_Conv2D_Forward_NCHW(
  const device int *inputs         [[ buffer(0) ]],
  const device float *filter       [[ buffer(1) ]],
  const device int* ptr_input_N    [[ buffer(3) ]],
  const device int* ptr_input_H    [[ buffer(4) ]],
  const device int* ptr_input_W    [[ buffer(5) ]],
  const device int* ptr_input_C    [[ buffer(6) ]],
  const device int* ptr_filter_H   [[ buffer(7) ]],
  const device int* ptr_filter_W   [[ buffer(8) ]],
  const device int* ptr_filter_iC  [[ buffer(9) ]],
  const device int* ptr_filter_oC  [[ buffer(10) ]],
  const device int* ptr_output_N   [[ buffer(11) ]],
  const device int* ptr_output_H   [[ buffer(12) ]],
  const device int* ptr_output_W   [[ buffer(13) ]],
  const device int* ptr_output_C   [[ buffer(14) ]],
  const device int* ptr_strides_N   [[ buffer(15) ]],
  const device int* ptr_strides_H   [[ buffer(16) ]],
  const device int* ptr_strides_W   [[ buffer(17) ]],
  const device int* ptr_strides_C   [[ buffer(18) ]],
  const device int* ptr_dilate_H   [[ buffer(19) ]],
  const device int* ptr_dilate_W   [[ buffer(20) ]],
  const device int* ptr_padding_H  [[ buffer(21) ]],
  const device int* ptr_padding_W  [[ buffer(22) ]],
  device float* output             [[ buffer(2) ]],
  uint gid     [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int H = *ptr_output_H;
  int W = *ptr_output_W;
  int C = *ptr_output_C;

  int filter_H  = *ptr_filter_H;
  int filter_W  = *ptr_filter_W;
  int filter_iC = *ptr_filter_iC;
  int filter_oC = *ptr_filter_oC;

  int strides_N = *ptr_strides_N;
  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int dilate_H = *ptr_dilate_H;
  int dilate_W = *ptr_dilate_W;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int out_N = ((gid / W) / H) / C;
  int out_C = ((gid / W) / H) % C;
  int out_H = ((gid / W)) % H;
  int out_W = gid % W;

  int iblock_N = out_N * in_W * in_H * in_C;
  int fblock_N = filter_iC * filter_oC;

  int origin_in_H = out_H * strides_H - padding_H;
  int origin_in_W = out_W * strides_W - padding_W;

  if (gid < N*H*W*C){
    for (int i = 0; i < filter_H; ++i) {
      for (int j = 0; j < filter_W; ++j){
        int in_X = j*dilate_W + origin_in_W;
        int in_Y = i*dilate_H + origin_in_H;
      
        if (in_X >= 0 && in_X < in_W && in_Y >= 0 && in_Y < in_H){
          for (int c = 0; c < filter_iC; ++c) {
            int ffixed = c*filter_oC + out_C;
            int cbegin = c*in_W*in_H;

            int in = inputs[iblock_N + cbegin + in_Y*in_W + in_X];
            int wn = filter[(i*filter_W + j)*fblock_N + ffixed];

            output[gid] += in * wn;
          }
        }
      }
    }
  }
}

kernel void RII_Buffer_Conv2D_Forward_NHWC(
  const device float *inputs       [[ buffer(0) ]],
  const device int *filter         [[ buffer(1) ]],
  const device int* ptr_input_N    [[ buffer(3) ]],
  const device int* ptr_input_H    [[ buffer(4) ]],
  const device int* ptr_input_W    [[ buffer(5) ]],
  const device int* ptr_input_C    [[ buffer(6) ]],
  const device int* ptr_filter_H   [[ buffer(7) ]],
  const device int* ptr_filter_W   [[ buffer(8) ]],
  const device int* ptr_filter_iC  [[ buffer(9) ]],
  const device int* ptr_filter_oC  [[ buffer(10) ]],
  const device int* ptr_output_N   [[ buffer(11) ]],
  const device int* ptr_output_H   [[ buffer(12) ]],
  const device int* ptr_output_W   [[ buffer(13) ]],
  const device int* ptr_output_C   [[ buffer(14) ]],
  const device int* ptr_strides_N   [[ buffer(15) ]],
  const device int* ptr_strides_H   [[ buffer(16) ]],
  const device int* ptr_strides_W   [[ buffer(17) ]],
  const device int* ptr_strides_C   [[ buffer(18) ]],
  const device int* ptr_dilate_H   [[ buffer(19) ]],
  const device int* ptr_dilate_W   [[ buffer(20) ]],
  const device int* ptr_padding_H  [[ buffer(21) ]],
  const device int* ptr_padding_W  [[ buffer(22) ]],
  device int* output               [[ buffer(2) ]],
  uint gid     [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int H = *ptr_output_H;
  int W = *ptr_output_W;
  int C = *ptr_output_C;

  int filter_H = *ptr_filter_H;
  int filter_W = *ptr_filter_W;
  int filter_iC = *ptr_filter_iC;
  int filter_oC = *ptr_filter_oC;

  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int dilate_H = *ptr_dilate_H;
  int dilate_W = *ptr_dilate_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int out_N = ((gid / C) / W) / H;
  int out_H = ((gid / C) / W) % H;
  int out_W = ((gid / C)) % W;
  int out_C = gid % C;

  int out_size = N*H*W*C;
  int shift_W  = in_C;
  int shift_H  = in_C * in_W;

  int begin  = out_N * in_W * in_H * in_C;
  int hstart = (out_H * strides_H - padding_H) * shift_H;
  int wstart = (out_W * strides_W - padding_W) * shift_W;
  int hend   = (out_H * strides_H + filter_H - padding_H) * shift_H;
  int wend   = (out_W * strides_W + filter_W - padding_W) * shift_W;

  int fblock_N    = filter_iC*filter_oC;
  int origin_in_H = out_H * strides_H - padding_H;
  int origin_in_W = out_W * strides_W - padding_W;

  if (gid < out_size){
    for (int i = 0, h = hstart; h < hend; ++i, h += dilate_H*shift_H) {
      for (int j = 0, w = wstart; w < wend; ++j, w += dilate_W*shift_W) {
        int in_X = i*dilate_H + origin_in_H;
        int in_Y = j*dilate_W + origin_in_W;

        if (in_X >= 0 && in_Y >= 0 && in_X < in_H && in_Y < in_W){
          for (int c = 0; c < filter_iC; ++c){
            int ffixed = c*filter_oC + out_C;

            output[gid] += inputs[begin + h + w + c] *
                           filter[(i*filter_W + j)*fblock_N + ffixed];
          }
        }
      }
    }
  }
}

kernel void RII_Buffer_Conv2D_Forward_NCHW(
  const device float *inputs       [[ buffer(0) ]],
  const device int *filter         [[ buffer(1) ]],
  const device int* ptr_input_N    [[ buffer(3) ]],
  const device int* ptr_input_H    [[ buffer(4) ]],
  const device int* ptr_input_W    [[ buffer(5) ]],
  const device int* ptr_input_C    [[ buffer(6) ]],
  const device int* ptr_filter_H   [[ buffer(7) ]],
  const device int* ptr_filter_W   [[ buffer(8) ]],
  const device int* ptr_filter_iC  [[ buffer(9) ]],
  const device int* ptr_filter_oC  [[ buffer(10) ]],
  const device int* ptr_output_N   [[ buffer(11) ]],
  const device int* ptr_output_H   [[ buffer(12) ]],
  const device int* ptr_output_W   [[ buffer(13) ]],
  const device int* ptr_output_C   [[ buffer(14) ]],
  const device int* ptr_strides_N   [[ buffer(15) ]],
  const device int* ptr_strides_H   [[ buffer(16) ]],
  const device int* ptr_strides_W   [[ buffer(17) ]],
  const device int* ptr_strides_C   [[ buffer(18) ]],
  const device int* ptr_dilate_H   [[ buffer(19) ]],
  const device int* ptr_dilate_W   [[ buffer(20) ]],
  const device int* ptr_padding_H  [[ buffer(21) ]],
  const device int* ptr_padding_W  [[ buffer(22) ]],
  device int* output               [[ buffer(2) ]],
  uint gid     [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int H = *ptr_output_H;
  int W = *ptr_output_W;
  int C = *ptr_output_C;

  int filter_H  = *ptr_filter_H;
  int filter_W  = *ptr_filter_W;
  int filter_iC = *ptr_filter_iC;
  int filter_oC = *ptr_filter_oC;

  int strides_N = *ptr_strides_N;
  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int dilate_H = *ptr_dilate_H;
  int dilate_W = *ptr_dilate_W;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int out_N = ((gid / W) / H) / C;
  int out_C = ((gid / W) / H) % C;
  int out_H = ((gid / W)) % H;
  int out_W = gid % W;

  int iblock_N = out_N * in_W * in_H * in_C;
  int fblock_N = filter_iC * filter_oC;

  int origin_in_H = out_H * strides_H - padding_H;
  int origin_in_W = out_W * strides_W - padding_W;

  if (gid < N*H*W*C){
    for (int i = 0; i < filter_H; ++i) {
      for (int j = 0; j < filter_W; ++j){
        int in_X = j*dilate_W + origin_in_W;
        int in_Y = i*dilate_H + origin_in_H;
      
        if (in_X >= 0 && in_X < in_W && in_Y >= 0 && in_Y < in_H){
          for (int c = 0; c < filter_iC; ++c) {
            int ffixed = c*filter_oC + out_C;
            int cbegin = c*in_W*in_H;

            int in = inputs[iblock_N + cbegin + in_Y*in_W + in_X];
            int wn = filter[(i*filter_W + j)*fblock_N + ffixed];

            output[gid] += in * wn;
          }
        }
      }
    }
  }
}

kernel void IIR_Buffer_Conv2D_Forward_NHWC(
  const device int *inputs         [[ buffer(0) ]],
  const device int *filter         [[ buffer(1) ]],
  const device int* ptr_input_N    [[ buffer(3) ]],
  const device int* ptr_input_H    [[ buffer(4) ]],
  const device int* ptr_input_W    [[ buffer(5) ]],
  const device int* ptr_input_C    [[ buffer(6) ]],
  const device int* ptr_filter_H   [[ buffer(7) ]],
  const device int* ptr_filter_W   [[ buffer(8) ]],
  const device int* ptr_filter_iC  [[ buffer(9) ]],
  const device int* ptr_filter_oC  [[ buffer(10) ]],
  const device int* ptr_output_N   [[ buffer(11) ]],
  const device int* ptr_output_H   [[ buffer(12) ]],
  const device int* ptr_output_W   [[ buffer(13) ]],
  const device int* ptr_output_C   [[ buffer(14) ]],
  const device int* ptr_strides_N   [[ buffer(15) ]],
  const device int* ptr_strides_H   [[ buffer(16) ]],
  const device int* ptr_strides_W   [[ buffer(17) ]],
  const device int* ptr_strides_C   [[ buffer(18) ]],
  const device int* ptr_dilate_H   [[ buffer(19) ]],
  const device int* ptr_dilate_W   [[ buffer(20) ]],
  const device int* ptr_padding_H  [[ buffer(21) ]],
  const device int* ptr_padding_W  [[ buffer(22) ]],
  device float* output               [[ buffer(2) ]],
  uint gid     [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int H = *ptr_output_H;
  int W = *ptr_output_W;
  int C = *ptr_output_C;

  int filter_H = *ptr_filter_H;
  int filter_W = *ptr_filter_W;
  int filter_iC = *ptr_filter_iC;
  int filter_oC = *ptr_filter_oC;

  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int dilate_H = *ptr_dilate_H;
  int dilate_W = *ptr_dilate_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int out_N = ((gid / C) / W) / H;
  int out_H = ((gid / C) / W) % H;
  int out_W = ((gid / C)) % W;
  int out_C = gid % C;

  int out_size = N*H*W*C;
  int shift_W  = in_C;
  int shift_H  = in_C * in_W;

  int begin  = out_N * in_W * in_H * in_C;
  int hstart = (out_H * strides_H - padding_H) * shift_H;
  int wstart = (out_W * strides_W - padding_W) * shift_W;
  int hend   = (out_H * strides_H + filter_H - padding_H) * shift_H;
  int wend   = (out_W * strides_W + filter_W - padding_W) * shift_W;

  int fblock_N    = filter_iC*filter_oC;
  int origin_in_H = out_H * strides_H - padding_H;
  int origin_in_W = out_W * strides_W - padding_W;

  if (gid < out_size){
    for (int i = 0, h = hstart; h < hend; ++i, h += dilate_H*shift_H) {
      for (int j = 0, w = wstart; w < wend; ++j, w += dilate_W*shift_W) {
        int in_X = i*dilate_H + origin_in_H;
        int in_Y = j*dilate_W + origin_in_W;

        if (in_X >= 0 && in_Y >= 0 && in_X < in_H && in_Y < in_W){
          for (int c = 0; c < filter_iC; ++c){
            int ffixed = c*filter_oC + out_C;

            output[gid] += inputs[begin + h + w + c] *
                           filter[(i*filter_W + j)*fblock_N + ffixed];
          }
        }
      }
    }
  }
}

kernel void IIR_Buffer_Conv2D_Forward_NCHW(
  const device int *inputs         [[ buffer(0) ]],
  const device int *filter         [[ buffer(1) ]],
  const device int* ptr_input_N    [[ buffer(3) ]],
  const device int* ptr_input_H    [[ buffer(4) ]],
  const device int* ptr_input_W    [[ buffer(5) ]],
  const device int* ptr_input_C    [[ buffer(6) ]],
  const device int* ptr_filter_H   [[ buffer(7) ]],
  const device int* ptr_filter_W   [[ buffer(8) ]],
  const device int* ptr_filter_iC  [[ buffer(9) ]],
  const device int* ptr_filter_oC  [[ buffer(10) ]],
  const device int* ptr_output_N   [[ buffer(11) ]],
  const device int* ptr_output_H   [[ buffer(12) ]],
  const device int* ptr_output_W   [[ buffer(13) ]],
  const device int* ptr_output_C   [[ buffer(14) ]],
  const device int* ptr_strides_N   [[ buffer(15) ]],
  const device int* ptr_strides_H   [[ buffer(16) ]],
  const device int* ptr_strides_W   [[ buffer(17) ]],
  const device int* ptr_strides_C   [[ buffer(18) ]],
  const device int* ptr_dilate_H   [[ buffer(19) ]],
  const device int* ptr_dilate_W   [[ buffer(20) ]],
  const device int* ptr_padding_H  [[ buffer(21) ]],
  const device int* ptr_padding_W  [[ buffer(22) ]],
  device float* output             [[ buffer(2) ]],
  uint gid     [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int H = *ptr_output_H;
  int W = *ptr_output_W;
  int C = *ptr_output_C;

  int filter_H  = *ptr_filter_H;
  int filter_W  = *ptr_filter_W;
  int filter_iC = *ptr_filter_iC;
  int filter_oC = *ptr_filter_oC;

  int strides_N = *ptr_strides_N;
  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int dilate_H = *ptr_dilate_H;
  int dilate_W = *ptr_dilate_W;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int out_N = ((gid / W) / H) / C;
  int out_C = ((gid / W) / H) % C;
  int out_H = ((gid / W)) % H;
  int out_W = gid % W;

  int iblock_N = out_N * in_W * in_H * in_C;
  int fblock_N = filter_iC * filter_oC;

  int origin_in_H = out_H * strides_H - padding_H;
  int origin_in_W = out_W * strides_W - padding_W;

  if (gid < N*H*W*C){
    for (int i = 0; i < filter_H; ++i) {
      for (int j = 0; j < filter_W; ++j){
        int in_X = j*dilate_W + origin_in_W;
        int in_Y = i*dilate_H + origin_in_H;
      
        if (in_X >= 0 && in_X < in_W && in_Y >= 0 && in_Y < in_H){
          for (int c = 0; c < filter_iC; ++c) {
            int ffixed = c*filter_oC + out_C;
            int cbegin = c*in_W*in_H;

            int in = inputs[iblock_N + cbegin + in_Y*in_W + in_X];
            int wn = filter[(i*filter_W + j)*fblock_N + ffixed];

            output[gid] += in * wn;
          }
        }
      }
    }
  }
}

kernel void IRI_Buffer_Conv2D_Forward_NHWC(
  const device int *inputs         [[ buffer(0) ]],
  const device float *filter       [[ buffer(1) ]],
  const device int* ptr_input_N    [[ buffer(3) ]],
  const device int* ptr_input_H    [[ buffer(4) ]],
  const device int* ptr_input_W    [[ buffer(5) ]],
  const device int* ptr_input_C    [[ buffer(6) ]],
  const device int* ptr_filter_H   [[ buffer(7) ]],
  const device int* ptr_filter_W   [[ buffer(8) ]],
  const device int* ptr_filter_iC  [[ buffer(9) ]],
  const device int* ptr_filter_oC  [[ buffer(10) ]],
  const device int* ptr_output_N   [[ buffer(11) ]],
  const device int* ptr_output_H   [[ buffer(12) ]],
  const device int* ptr_output_W   [[ buffer(13) ]],
  const device int* ptr_output_C   [[ buffer(14) ]],
  const device int* ptr_strides_N   [[ buffer(15) ]],
  const device int* ptr_strides_H   [[ buffer(16) ]],
  const device int* ptr_strides_W   [[ buffer(17) ]],
  const device int* ptr_strides_C   [[ buffer(18) ]],
  const device int* ptr_dilate_H   [[ buffer(19) ]],
  const device int* ptr_dilate_W   [[ buffer(20) ]],
  const device int* ptr_padding_H  [[ buffer(21) ]],
  const device int* ptr_padding_W  [[ buffer(22) ]],
  device int* output               [[ buffer(2) ]],
  uint gid     [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int H = *ptr_output_H;
  int W = *ptr_output_W;
  int C = *ptr_output_C;

  int filter_H = *ptr_filter_H;
  int filter_W = *ptr_filter_W;
  int filter_iC = *ptr_filter_iC;
  int filter_oC = *ptr_filter_oC;

  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int dilate_H = *ptr_dilate_H;
  int dilate_W = *ptr_dilate_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int out_N = ((gid / C) / W) / H;
  int out_H = ((gid / C) / W) % H;
  int out_W = ((gid / C)) % W;
  int out_C = gid % C;

  int out_size = N*H*W*C;
  int shift_W  = in_C;
  int shift_H  = in_C * in_W;

  int begin  = out_N * in_W * in_H * in_C;
  int hstart = (out_H * strides_H - padding_H) * shift_H;
  int wstart = (out_W * strides_W - padding_W) * shift_W;
  int hend   = (out_H * strides_H + filter_H - padding_H) * shift_H;
  int wend   = (out_W * strides_W + filter_W - padding_W) * shift_W;

  int fblock_N    = filter_iC*filter_oC;
  int origin_in_H = out_H * strides_H - padding_H;
  int origin_in_W = out_W * strides_W - padding_W;

  if (gid < out_size){
    for (int i = 0, h = hstart; h < hend; ++i, h += dilate_H*shift_H) {
      for (int j = 0, w = wstart; w < wend; ++j, w += dilate_W*shift_W) {
        int in_X = i*dilate_H + origin_in_H;
        int in_Y = j*dilate_W + origin_in_W;

        if (in_X >= 0 && in_Y >= 0 && in_X < in_H && in_Y < in_W){
          for (int c = 0; c < filter_iC; ++c){
            int ffixed = c*filter_oC + out_C;

            output[gid] += inputs[begin + h + w + c] *
                           filter[(i*filter_W + j)*fblock_N + ffixed];
          }
        }
      }
    }
  }
}

kernel void IRI_Buffer_Conv2D_Forward_NCHW(
  const device int *inputs         [[ buffer(0) ]],
  const device float *filter       [[ buffer(1) ]],
  const device int* ptr_input_N    [[ buffer(3) ]],
  const device int* ptr_input_H    [[ buffer(4) ]],
  const device int* ptr_input_W    [[ buffer(5) ]],
  const device int* ptr_input_C    [[ buffer(6) ]],
  const device int* ptr_filter_H   [[ buffer(7) ]],
  const device int* ptr_filter_W   [[ buffer(8) ]],
  const device int* ptr_filter_iC  [[ buffer(9) ]],
  const device int* ptr_filter_oC  [[ buffer(10) ]],
  const device int* ptr_output_N   [[ buffer(11) ]],
  const device int* ptr_output_H   [[ buffer(12) ]],
  const device int* ptr_output_W   [[ buffer(13) ]],
  const device int* ptr_output_C   [[ buffer(14) ]],
  const device int* ptr_strides_N   [[ buffer(15) ]],
  const device int* ptr_strides_H   [[ buffer(16) ]],
  const device int* ptr_strides_W   [[ buffer(17) ]],
  const device int* ptr_strides_C   [[ buffer(18) ]],
  const device int* ptr_dilate_H   [[ buffer(19) ]],
  const device int* ptr_dilate_W   [[ buffer(20) ]],
  const device int* ptr_padding_H  [[ buffer(21) ]],
  const device int* ptr_padding_W  [[ buffer(22) ]],
  device int* output               [[ buffer(2) ]],
  uint gid     [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int H = *ptr_output_H;
  int W = *ptr_output_W;
  int C = *ptr_output_C;

  int filter_H  = *ptr_filter_H;
  int filter_W  = *ptr_filter_W;
  int filter_iC = *ptr_filter_iC;
  int filter_oC = *ptr_filter_oC;

  int strides_N = *ptr_strides_N;
  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int dilate_H = *ptr_dilate_H;
  int dilate_W = *ptr_dilate_W;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int out_N = ((gid / W) / H) / C;
  int out_C = ((gid / W) / H) % C;
  int out_H = ((gid / W)) % H;
  int out_W = gid % W;

  int iblock_N = out_N * in_W * in_H * in_C;
  int fblock_N = filter_iC * filter_oC;

  int origin_in_H = out_H * strides_H - padding_H;
  int origin_in_W = out_W * strides_W - padding_W;

  if (gid < N*H*W*C){
    for (int i = 0; i < filter_H; ++i) {
      for (int j = 0; j < filter_W; ++j){
        int in_X = j*dilate_W + origin_in_W;
        int in_Y = i*dilate_H + origin_in_H;
      
        if (in_X >= 0 && in_X < in_W && in_Y >= 0 && in_Y < in_H){
          for (int c = 0; c < filter_iC; ++c) {
            int ffixed = c*filter_oC + out_C;
            int cbegin = c*in_W*in_H;

            int in = inputs[iblock_N + cbegin + in_Y*in_W + in_X];
            int wn = filter[(i*filter_W + j)*fblock_N + ffixed];

            output[gid] += in * wn;
          }
        }
      }
    }
  }
}

kernel void III_Buffer_Conv2D_Forward_NHWC(
  const device int *inputs         [[ buffer(0) ]],
  const device int *filter         [[ buffer(1) ]],
  const device int* ptr_input_N    [[ buffer(3) ]],
  const device int* ptr_input_H    [[ buffer(4) ]],
  const device int* ptr_input_W    [[ buffer(5) ]],
  const device int* ptr_input_C    [[ buffer(6) ]],
  const device int* ptr_filter_H   [[ buffer(7) ]],
  const device int* ptr_filter_W   [[ buffer(8) ]],
  const device int* ptr_filter_iC  [[ buffer(9) ]],
  const device int* ptr_filter_oC  [[ buffer(10) ]],
  const device int* ptr_output_N   [[ buffer(11) ]],
  const device int* ptr_output_H   [[ buffer(12) ]],
  const device int* ptr_output_W   [[ buffer(13) ]],
  const device int* ptr_output_C   [[ buffer(14) ]],
  const device int* ptr_strides_N   [[ buffer(15) ]],
  const device int* ptr_strides_H   [[ buffer(16) ]],
  const device int* ptr_strides_W   [[ buffer(17) ]],
  const device int* ptr_strides_C   [[ buffer(18) ]],
  const device int* ptr_dilate_H   [[ buffer(19) ]],
  const device int* ptr_dilate_W   [[ buffer(20) ]],
  const device int* ptr_padding_H  [[ buffer(21) ]],
  const device int* ptr_padding_W  [[ buffer(22) ]],
  device int* output               [[ buffer(2) ]],
  uint gid     [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int H = *ptr_output_H;
  int W = *ptr_output_W;
  int C = *ptr_output_C;

  int filter_H = *ptr_filter_H;
  int filter_W = *ptr_filter_W;
  int filter_iC = *ptr_filter_iC;
  int filter_oC = *ptr_filter_oC;

  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int dilate_H = *ptr_dilate_H;
  int dilate_W = *ptr_dilate_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int out_N = ((gid / C) / W) / H;
  int out_H = ((gid / C) / W) % H;
  int out_W = ((gid / C)) % W;
  int out_C = gid % C;

  int out_size = N*H*W*C;
  int shift_W  = in_C;
  int shift_H  = in_C * in_W;

  int begin  = out_N * in_W * in_H * in_C;
  int hstart = (out_H * strides_H - padding_H) * shift_H;
  int wstart = (out_W * strides_W - padding_W) * shift_W;
  int hend   = (out_H * strides_H + filter_H - padding_H) * shift_H;
  int wend   = (out_W * strides_W + filter_W - padding_W) * shift_W;

  int fblock_N    = filter_iC*filter_oC;
  int origin_in_H = out_H * strides_H - padding_H;
  int origin_in_W = out_W * strides_W - padding_W;

  if (gid < out_size){
    for (int i = 0, h = hstart; h < hend; ++i, h += dilate_H*shift_H) {
      for (int j = 0, w = wstart; w < wend; ++j, w += dilate_W*shift_W) {
        int in_X = i*dilate_H + origin_in_H;
        int in_Y = j*dilate_W + origin_in_W;

        if (in_X >= 0 && in_Y >= 0 && in_X < in_H && in_Y < in_W){
          for (int c = 0; c < filter_iC; ++c){
            int ffixed = c*filter_oC + out_C;

            output[gid] += inputs[begin + h + w + c] *
                           filter[(i*filter_W + j)*fblock_N + ffixed];
          }
        }
      }
    }
  }
}

kernel void III_Buffer_Conv2D_Forward_NCHW(
  const device int *inputs         [[ buffer(0) ]],
  const device int *filter         [[ buffer(1) ]],
  const device int* ptr_input_N    [[ buffer(3) ]],
  const device int* ptr_input_H    [[ buffer(4) ]],
  const device int* ptr_input_W    [[ buffer(5) ]],
  const device int* ptr_input_C    [[ buffer(6) ]],
  const device int* ptr_filter_H   [[ buffer(7) ]],
  const device int* ptr_filter_W   [[ buffer(8) ]],
  const device int* ptr_filter_iC  [[ buffer(9) ]],
  const device int* ptr_filter_oC  [[ buffer(10) ]],
  const device int* ptr_output_N   [[ buffer(11) ]],
  const device int* ptr_output_H   [[ buffer(12) ]],
  const device int* ptr_output_W   [[ buffer(13) ]],
  const device int* ptr_output_C   [[ buffer(14) ]],
  const device int* ptr_strides_N   [[ buffer(15) ]],
  const device int* ptr_strides_H   [[ buffer(16) ]],
  const device int* ptr_strides_W   [[ buffer(17) ]],
  const device int* ptr_strides_C   [[ buffer(18) ]],
  const device int* ptr_dilate_H   [[ buffer(19) ]],
  const device int* ptr_dilate_W   [[ buffer(20) ]],
  const device int* ptr_padding_H  [[ buffer(21) ]],
  const device int* ptr_padding_W  [[ buffer(22) ]],
  device int* output               [[ buffer(2) ]],
  uint gid     [[ thread_position_in_grid ]])
{
  int N = *ptr_output_N;
  int H = *ptr_output_H;
  int W = *ptr_output_W;
  int C = *ptr_output_C;

  int filter_H  = *ptr_filter_H;
  int filter_W  = *ptr_filter_W;
  int filter_iC = *ptr_filter_iC;
  int filter_oC = *ptr_filter_oC;

  int strides_N = *ptr_strides_N;
  int strides_H = *ptr_strides_H;
  int strides_W = *ptr_strides_W;

  int dilate_H = *ptr_dilate_H;
  int dilate_W = *ptr_dilate_W;

  int padding_H = *ptr_padding_H;
  int padding_W = *ptr_padding_W;

  int in_H = *ptr_input_H;
  int in_W = *ptr_input_W;
  int in_C = *ptr_input_C;

  int out_N = ((gid / W) / H) / C;
  int out_C = ((gid / W) / H) % C;
  int out_H = ((gid / W)) % H;
  int out_W = gid % W;

  int iblock_N = out_N * in_W * in_H * in_C;
  int fblock_N = filter_iC * filter_oC;

  int origin_in_H = out_H * strides_H - padding_H;
  int origin_in_W = out_W * strides_W - padding_W;

  if (gid < N*H*W*C){
    for (int i = 0; i < filter_H; ++i) {
      for (int j = 0; j < filter_W; ++j){
        int in_X = j*dilate_W + origin_in_W;
        int in_Y = i*dilate_H + origin_in_H;
      
        if (in_X >= 0 && in_X < in_W && in_Y >= 0 && in_Y < in_H){
          for (int c = 0; c < filter_iC; ++c) {
            int ffixed = c*filter_oC + out_C;
            int cbegin = c*in_W*in_H;

            int in = inputs[iblock_N + cbegin + in_Y*in_W + in_X];
            int wn = filter[(i*filter_W + j)*fblock_N + ffixed];

            output[gid] += in * wn;
          }
        }
      }
    }
  }
}
