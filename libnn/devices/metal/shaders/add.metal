#include <metal_stdlib>

using namespace metal;

kernel void RRR_Buffer_Add_Forward(
  const device float* left       [[ buffer(0) ]],
  const device float* right      [[ buffer(1) ]],
  const device uint* output_size [[ buffer(3) ]],
  const device uint* left_size   [[ buffer(4) ]],
  const device uint* right_size  [[ buffer(5) ]],
  device float* output           [[ buffer(2) ]],
  uint  gid   [[ thread_position_in_grid ]])
{
  if (gid < *output_size){
    output[gid] = left[gid % (*left_size)] + right[gid % (*right_size)];
  }
}

kernel void RRI_Buffer_Add_Forward(
  const device float* left       [[ buffer(0) ]],
  const device float* right      [[ buffer(1) ]],
  const device uint* output_size [[ buffer(3) ]],
  const device uint* left_size   [[ buffer(4) ]],
  const device uint* right_size  [[ buffer(5) ]],
  device int* output             [[ buffer(2) ]],
  uint  gid   [[ thread_position_in_grid ]])
{
  if (gid < *output_size){
    output[gid] = left[gid % (*left_size)] + right[gid % (*right_size)];
  }
}

kernel void RIR_Buffer_Add_Forward(
  const device float* left       [[ buffer(0) ]],
  const device int* right        [[ buffer(1) ]],
  const device uint* output_size [[ buffer(3) ]],
  const device uint* left_size   [[ buffer(4) ]],
  const device uint* right_size  [[ buffer(5) ]],
  device float* output           [[ buffer(2) ]],
  uint  gid   [[ thread_position_in_grid ]])
{
  if (gid < *output_size){
    output[gid] = left[gid % (*left_size)] + right[gid % (*right_size)];
  }
}

kernel void IRR_Buffer_Add_Forward(
  const device int* left         [[ buffer(0) ]],
  const device float* right      [[ buffer(1) ]],
  const device uint* output_size [[ buffer(3) ]],
  const device uint* left_size   [[ buffer(4) ]],
  const device uint* right_size  [[ buffer(5) ]],
  device float* output           [[ buffer(2) ]],
  uint  gid   [[ thread_position_in_grid ]])
{
  if (gid < *output_size){
    output[gid] = left[gid % (*left_size)] + right[gid % (*right_size)];
  }
}

kernel void RII_Buffer_Add_Forward(
  const device float* left       [[ buffer(0) ]],
  const device int* right        [[ buffer(1) ]],
  const device uint* output_size [[ buffer(3) ]],
  const device uint* left_size   [[ buffer(4) ]],
  const device uint* right_size  [[ buffer(5) ]],
  device int* output             [[ buffer(2) ]],
  uint  gid   [[ thread_position_in_grid ]])
{
  if (gid < *output_size){
    output[gid] = left[gid % (*left_size)] + right[gid % (*right_size)];
  }
}

kernel void IIR_Buffer_Add_Forward(
  const device int* left         [[ buffer(0) ]],
  const device int* right        [[ buffer(1) ]],
  const device uint* output_size [[ buffer(3) ]],
  const device uint* left_size   [[ buffer(4) ]],
  const device uint* right_size  [[ buffer(5) ]],
  device float* output           [[ buffer(2) ]],
  uint  gid   [[ thread_position_in_grid ]])
{
  if (gid < *output_size){
    output[gid] = left[gid % (*left_size)] + right[gid % (*right_size)];
  }
}

kernel void IRI_Buffer_Add_Forward(
  const device int* left         [[ buffer(0) ]],
  const device float* right      [[ buffer(1) ]],
  const device uint* output_size [[ buffer(3) ]],
  const device uint* left_size   [[ buffer(4) ]],
  const device uint* right_size  [[ buffer(5) ]],
  device int* output             [[ buffer(2) ]],
  uint  gid   [[ thread_position_in_grid ]])
{
  if (gid < *output_size){
    output[gid] = left[gid % (*left_size)] + right[gid % (*right_size)];
  }
}

kernel void III_Buffer_Add_Forward(
  const device int* left         [[ buffer(0) ]],
  const device int* right        [[ buffer(1) ]],
  const device uint* output_size [[ buffer(3) ]],
  const device uint* left_size   [[ buffer(4) ]],
  const device uint* right_size  [[ buffer(5) ]],
  device int* output             [[ buffer(2) ]],
  uint  gid   [[ thread_position_in_grid ]])
{
  if (gid < *output_size){
    output[gid] = left[gid % (*left_size)] + right[gid % (*right_size)];
  }
}
