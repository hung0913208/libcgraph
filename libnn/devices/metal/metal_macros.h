#if !defined(LIBNN_DEVICE_METAL_MACORS_H_) && __cplusplus
#define LIBNN_DEVICE_METAL_MACORS_H_
#include <libbase/macros.hpp>

#if APPLE
#include <Metal/Metal.h>
#include <signal.h>

#include "devices/metal/metal.h"
#include "internal.h"

using Commands = std::vector<std::string>;

namespace nn{
namespace device{
namespace metal{
namespace helper{
base::Error putBufferInput(nn::device::metal::MetalConsumer* consumer,
                           id<MTLComputeCommandEncoder> commandEncoder,
                           int index, Tensor tensor);
base::Error putBufferOutput(id<MTLComputeCommandEncoder> commandEncoder,
                           int index, Tensor tensor);

template <typename Type>
base::Error putValue(nn::device::metal::MetalConsumer* consumer,
                     id<MTLComputeCommandEncoder> commandEncoder,
                     int index, Type& value) {
  auto dev  = consumer->device();
  auto buff = [dev newBufferWithBytes: &value
                               length: sizeof(Type)
                              options: MTLResourceCPUCacheModeDefaultCache];
  if (buff) {
    [commandEncoder setBuffer:buff offset:0 atIndex:index];
  } else
    return BadAccess.reason("newBufferWithBytes()");
  return NoError;
}

template <typename Type>
base::Error putValue(nn::device::metal::MetalConsumer* consumer,
                     id<MTLComputeCommandEncoder> commandEncoder, 
                     int index, Type&& value) {
  auto dev  = consumer->device();
  auto buff = [dev newBufferWithBytes: &value
                               length: sizeof(Type)
                              options: MTLResourceCPUCacheModeDefaultCache];
  if (buff) {
    [commandEncoder setBuffer:buff offset:0 atIndex:index];
  } else
    return BadAccess.reason("newBufferWithBytes()");
  return NoError;
}

template <typename Type>
base::Error putArray(nn::device::metal::MetalConsumer* consumer,
                     id<MTLComputeCommandEncoder> commandEncoder, 
                     int index, Type* array, int size) {
  auto dev = consumer->device();
  auto buff = [dev newBufferWithBytes: array
                               length: size
                              options: MTLResourceCPUCacheModeDefaultCache];
  if (buff) {
    [commandEncoder setBuffer:buff offset:0 atIndex:index];
  } else
    return BadAccess.reason("newBufferWithBytes()");
  return NoError;
}
} // namespace helper
} // namespace metal
} // namespace device
} // namespace nn

#define SELECT(opname)                                                         \
Commands opname::selector::opname(                                             \
    device::metal::MetalConsumer* UNUSED(consumer),                            \
    nn::interpreter::OperatorW* UNUSED(opw),                                   \
    std::vector<std::pair<CString, Tensor>>& UNUSED(inputs))

#define PERFORM(opname)                                                        \
base::Error opname::perform::opname(std::string& UNUSED(function),             \
                            nn::interpreter::OperatorW* UNUSED(opw),           \
                            device::metal::MetalConsumer* UNUSED(consumer),    \
                            id<MTLCommandBuffer> UNUSED(commandBuffer),        \
                            id<MTLComputePipelineState> pipeline,              \
                            id<MTLComputeCommandEncoder> commandEncoder)

#define MATRIX(opname)                                                         \
std::string opname::matrix::opname(std::string& UNUSED(function),              \
                                   nn::interpreter::OperatorW* UNUSED(opw))

#define DEFINE_EVAL(opname)                                                    \
namespace opname{                                                              \
namespace selector{                                                            \
Commands opname(nn::device::metal::MetalConsumer* consumer,                    \
                nn::interpreter::OperatorW* UNUSED(opw),                       \
                std::vector<std::pair<CString, Tensor>>& inputs);              \
}                                                                              \
                                                                               \
namespace perform{                                                             \
base::Error opname(std::string& function, nn::interpreter::OperatorW* op,      \
                   device::metal::MetalConsumer* consumer,                     \
                   id<MTLCommandBuffer> commandBuffer,                         \
                   id<MTLComputePipelineState> pipeline,                       \
                   id<MTLComputeCommandEncoder> commandEncoder);               \
}                                                                              \
                                                                               \
namespace matrix{                                                              \
std::string opname(std::string& function, nn::interpreter::OperatorW* opw);    \
}                                                                              \
                                                                               \
bool Eval(Operator op, Interpreter interpreter){                               \
  using namespace nn::kernels::metal::opname;                                  \
  using DeviceW  = nn::interpreter::DeviceW;                                   \
  using Consumer = nn::device::metal::MetalConsumer;                           \
                                                                               \
  auto opw      = nNet_GetOperatorW(op);                                       \
  auto consumer = utils::response<DeviceW, Consumer>(op->device->_priv);       \
                                                                               \
  /* @NOTE: check input and alloc output if it needs */                        \
  if (!consumer){                                                              \
    return opw << (BadLogic << "operator" << op->name << "must be created"     \
                            << "by device \'metal\'");                         \
  } else if (generic::opname::Check(op, interpreter))                          \
    return true;                                                               \
  else if (interpreter){                                                       \
    auto commands = selector::opname(consumer, opw, opw->inputs());            \
                                                                               \
    for (auto& function: commands){                                            \
      auto realfunct = matrix::opname(function, opw) + "_" + function;         \
      auto pipeline  = consumer->pipeline(realfunct);                          \
      auto cmdbuffer = consumer->commandBuffer();                              \
      auto error     = NoError;                                                \
                                                                               \
      /* @NOTE: okey create function and perform it now */                     \
      if (!pipeline)                                                           \
        return NotFound.reason(rvalue(realfunct));                             \
      else if (cmdbuffer){                                                     \
        auto encoder = [cmdbuffer computeCommandEncoder];                      \
                                                                               \
        if (encoder){                                                          \
          if (!(error = perform::opname(function, opw, consumer, cmdbuffer,    \
                                        pipeline, encoder)))                   \
            consumer->commit(cmdbuffer);                                       \
        } else error = BadAccess.reason("computeCommandEncoder()");            \
        return nNet_GetInterpreterW(interpreter)->error(error);                \
      }                                                                        \
    }                                                                          \
  }                                                                            \
  return NoError;                                                              \
}                                                                              \
} // namespace ##opname


#define DEFINE_DERIVE(opname)                                                  \
namespace opname{                                                              \
bool Derive(Operator op, Interpreter interpreter){                             \
  return false;                                                                \
}                                                                              \
} // namesapce ##opname

/* @NOTE: a single pipeline can be use with many more command encoder since this
 * object is used like input and output parameters */
#define CREATE_COMMAND_ENCODER()                                               \
  base::Boundary bound{[](){}, [&](){ [commandEncoder endEncoding]; }};        \
                                                                               \
  auto error = NoError;                                                        \
  auto threadCount = [pipeline threadExecutionWidth];                          \
  auto maxThreadPerGroup = [pipeline maxTotalThreadsPerThreadgroup];           \
  auto groupCount = opw->outputs().size();                                     \
                                                                               \
  if (!commandEncoder) return BadAccess << "computeCommandEncoder()";          \
  [commandEncoder setComputePipelineState: pipeline];

#define FINISH_CONFIGURE_ENCODER(Type, ThreadsPerThreadgroup)                 \
  if (Type == TensorW::BufferT) {                                             \
     if (ThreadsPerThreadgroup == 0) {                                        \
      threadCount = 0;                                                        \
                                                                              \
      if (opw->outputs().size() > 1)                                          \
        return NoSupport.reason("This macro is only support single output");  \
                                                                              \
      for (auto i = 0; i < cast_(i, opw->outputs().size()); ++i) {            \
        auto tensor_size = nNet_GetArraySizeT(opw->outputs()[i]);             \
                                                                              \
        if (tensor_size > cast_(tensor_size, threadCount)) {                  \
          threadCount = tensor_size;                                          \
        }                                                                     \
      }                                                                       \
    } else                                                                    \
      threadCount = ThreadsPerThreadgroup;                                    \
                                                                              \
    if (threadCount > 0){                                                     \
      auto groupCount     = threadCount/maxThreadPerGroup;                    \
      auto threadPerGroup = maxThreadPerGroup;                                \
                                                                              \
      if (groupCount == 0){                                                   \
        groupCount = 1;                                                       \
        threadPerGroup = threadCount;                                         \
      } else {                                                                \
        groupCount += threadCount%maxThreadPerGroup? 1: 0;                    \
      }                                                                       \
      [commandEncoder dispatchThreadgroups:MTLSizeMake(groupCount, 1, 1)      \
                     threadsPerThreadgroup:MTLSizeMake(threadPerGroup, 1, 1)];\
    }                                                                         \
  } else                                                                      \
    error = NoSupport.reason(nn::device::metal::TensorW::nametype(Type));     \
                                                                              \
  return error;

#define PUT_VALUE(index, value)                                     \
  {                                                                 \
    using namespace nn::device::metal::helper;                      \
    if ((error = putValue(consumer, commandEncoder, index, value))) \
      return error;                                                 \
  }

#define PUT_BUFFER_INPUT(index, tensor)                                    \
  {                                                                        \
    using namespace nn::device::metal::helper;                             \
    if ((error = putBufferInput(consumer, commandEncoder, index, tensor))) \
      return error;                                                        \
  }

#define PUT_BUFFER_OUTPUT(index, tensor)                          \
  {                                                               \
    using namespace nn::device::metal::helper;                    \
    if ((error = putBufferOutput(commandEncoder, index, tensor))) \
      return error;                                               \
  }

#define PUT_ARRAY(index, array, size)                                     \
  {                                                                       \
    using namespace nn::device::metal::helper;                            \
    if ((error = putArray(consumer, commandEncoder, index, array, size))) \
      return error;                                                       \
  }
#endif
#endif  // LIBNN_DEVICE_METAL_MACORS_H_
