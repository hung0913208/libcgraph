#include "devices/generic/generic.h"
#include "devices/metal/metal.h"
#include "devices/metal/macros.h"

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
namespace kernels{
namespace metal{
DEFINE_EVAL(matmul);
DEFINE_DERIVE(matmul);
SELECT(matmul){
  return { "Buffer_MatMul_Forward" };
}

MATRIX(matmul){
  auto params = { std::get<1>(opw->inputs()[0]),
                  std::get<1>(opw->inputs()[1]), 
                  opw->outputs()[0] };
  std::string result;

  for (auto param: params){
    switch (nNet_GetTensorNumT(param)){
    case Int32_t:
      result += "I";
      break;

    case Real32_t:
      result += "R";
      break;

    default:
      break;
    }
  }

  return result;
}
PERFORM(matmul){
  using namespace nn::device::metal;
  using namespace nn::interpreter;
  using namespace nn::tensor;

 CREATE_COMMAND_ENCODER()
  auto left   = std::get<1>(opw->inputs()[0]);
  auto right  = std::get<1>(opw->inputs()[1]);
  auto output = opw->outputs()[0];

  auto type = TensorW::Unknown;
  auto dims = std::vector<int>{output->dims[0], left->dims[0], output->dims[1]};

  if (function == "Buffer_MatMul_Forward"){

    type = TensorW::BufferT;

    PUT_ARRAY(2, const_cast<int*>(dims.data()), sizeof(int)*dims.size());
    PUT_BUFFER_INPUT(0, left);
    PUT_BUFFER_INPUT(1, right);
    PUT_BUFFER_OUTPUT(3, output);
  }
 #if ENABLE_METAL_TEXTURE2D
  else if (function == "Texture2D_Add_Forward"){
    return NoSupport;
  }
 #endif
  else return NoSupport;

 FINISH_CONFIGURE_ENCODER(type, 0)
}

Registration doRegisterMatMul(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = kernels::generic::matmul::Prepare;
  result.eval   = matmul::Eval;
  result.derive = matmul::Derive;

  return result;
}
} // namespace metal
} // namespace kernels
} // namespace nn
