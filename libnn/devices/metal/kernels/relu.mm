#include "devices/generic/generic.h"
#include "devices/metal/metal.h"
#include "devices/metal/macros.h"

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
namespace kernels{
namespace metal{
DEFINE_EVAL(relu);
DEFINE_DERIVE(relu);
SELECT(relu){
  return { "Buffer_Relu_Forward" };
}

MATRIX(relu){
  auto params = { std::get<1>(opw->inputs()[0]),
                  opw->outputs()[0] };
  std::string result;

  for (auto param: params){
    switch (nNet_GetTensorNumT(param)){
    case Int32_t:
      result += "I";
      break;

    case Real32_t:
      result += "R";
      break;

    default:
      break;
    }
  }

  return result;
}

PERFORM(relu){
  using namespace nn::device::metal;
  using namespace nn::interpreter;
  using namespace nn::tensor;

 CREATE_COMMAND_ENCODER()
  auto type   = TensorW::Unknown;

  if (function == "Buffer_Relu_Forward"){
    type = TensorW::BufferT;

    PUT_BUFFER_INPUT(0, std::get<1>(opw->inputs()[0]));
    PUT_BUFFER_OUTPUT(1, opw->outputs()[0]);
  }

 #if ENABLE_METAL_TEXTURE2D
  else if (function == "Texture2D_Relu_Forward"){
    return NoSupport;
  }
 #endif
  else return NoSupport;
 FINISH_CONFIGURE_ENCODER(type, 0)
}

Registration doRegisterRelu(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = kernels::generic::relu::Prepare;
  result.eval   = relu::Eval;
  result.derive = relu::Derive;
  return result;
}
} // namespace metal
} // namespace kernels
} // namespace nn
