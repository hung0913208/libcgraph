#include "devices/generic/generic.h"
#include "devices/metal/metal.h"
#include "devices/metal/macros.h"
#include "devices/generic/math.h"
#include "devices/generic/math.h"

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
namespace kernels{
namespace metal{
DEFINE_EVAL(maxpool);
DEFINE_DERIVE(maxpool);
SELECT(maxpool){
  if (!strcmp(opw->get<char*>("data_format"), "NHWC"))
    return { "Buffer_MaxPool_Forward_NHWC" };
  else
    return { "Buffer_MaxPool_Forward_NCHW" };
}

MATRIX(maxpool){
  auto params = { std::get<1>(opw->inputs()[0]), opw->outputs()[0] };
  std::string result;

  for (auto param: params){
    switch (nNet_GetTensorNumT(param)){
    case Int32_t:
      result += "I";
      break;

    case Real32_t:
      result += "R";
      break;

    default:
      break;
    }
  }

  return result;
}

PERFORM(maxpool){
  using namespace nn::generic::math;
  using namespace nn::device::metal;
  using namespace nn::interpreter;
  using namespace nn::tensor;

  const std::string buffer_function = "Buffer_MaxPool_Forward";

 CREATE_COMMAND_ENCODER()
  auto type   = TensorW::Unknown;

  if (function.compare(0, buffer_function.length() - 1, buffer_function)){
    auto input     = std::get<1>(opw->inputs()[0]);
    auto output    = opw->outputs()[0];
    auto ksize_H   = int(opw->get<int64_t>("ksize.H"));
    auto ksize_W   = int(opw->get<int64_t>("ksize.W"));
    auto strides_H = int(opw->get<int64_t>("strides.H"));
    auto strides_W = int(opw->get<int64_t>("strides.W"));

    int in_H = 0, in_W = 0, out_H = 0, out_W = 0;

    type = TensorW::BufferT;
    PUT_BUFFER_INPUT(0, std::get<1>(opw->inputs()[0]));
    PUT_BUFFER_OUTPUT(1, opw->outputs()[0]);

    PUT_VALUE(2, int(opw->get<int64_t>("ksize.N")));
    PUT_VALUE(3, ksize_H);
    PUT_VALUE(4, ksize_W);
    PUT_VALUE(5, int(opw->get<int64_t>("ksize.C")));

    PUT_VALUE(6, int(opw->get<int64_t>("strides.N")));
    PUT_VALUE(7, strides_H);
    PUT_VALUE(8, strides_W);
    PUT_VALUE(9, int(opw->get<int64_t>("strides.C")));

    if (!strcmp(opw->get<char*>("data_format"), "NHWC")){
      in_H  = input->dims[2];
      in_W  = input->dims[1];
      out_H = output->dims[2];
      out_W = output->dims[1];

      PUT_VALUE(10, input->dims[3]); // N
      PUT_VALUE(11, input->dims[2]); // H
      PUT_VALUE(12, input->dims[1]); // W
      PUT_VALUE(13, input->dims[0]); // C

      PUT_VALUE(14, output->dims[3]); // N
      PUT_VALUE(15, output->dims[2]); // H
      PUT_VALUE(16, output->dims[1]); // W
      PUT_VALUE(17, output->dims[0]); // C
    } else if (!strcmp(opw->get<char*>("data_format"), "NCHW")){
      in_H  = input->dims[1];
      in_W  = input->dims[0];
      out_H = output->dims[1];
      out_W = output->dims[0];

      PUT_VALUE(10, input->dims[3]); // N
      PUT_VALUE(11, input->dims[1]); // H
      PUT_VALUE(12, input->dims[0]); // W
      PUT_VALUE(13, input->dims[2]); // C

      PUT_VALUE(14, output->dims[3]); // N
      PUT_VALUE(15, output->dims[1]); // H
      PUT_VALUE(16, output->dims[0]); // W
      PUT_VALUE(17, output->dims[2]); // C
    }

    PUT_VALUE(18, computePadding(strides_H, 1, in_H, ksize_H, out_H));
    PUT_VALUE(19, computePadding(strides_W, 1, in_W, ksize_W, out_W));
  }
 #if ENABLE_METAL_TEXTURE2D
  else if (function == "Texture2D_MaxPool_Forward"){
    return NoSupport;
  }
 #endif
  else
    return NoSupport << "can\'t recognize function" << function;

 FINISH_CONFIGURE_ENCODER(type, 0)
}

Registration doRegisterMaxPool(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = kernels::generic::maxpool::Prepare;
  result.eval   = maxpool::Eval;
  result.derive = maxpool::Derive;
  return result;
}
} // namespace metal
} // namespace kernels
} // namespace nn
