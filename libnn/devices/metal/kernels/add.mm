#include "devices/generic/generic.h"
#include "devices/metal/metal.h"
#include "devices/metal/macros.h"
#include "devices/common.h"

#include "interpreter.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
namespace kernels{
namespace metal{
DEFINE_EVAL(add);
DEFINE_DERIVE(add);

SELECT(add){
  return { "Buffer_Add_Forward" };
}

MATRIX(add){
  auto params = { std::get<1>(opw->inputs()[0]),
                  std::get<1>(opw->inputs()[1]), 
                  opw->outputs()[0] };
  std::string result;

  for (auto param: params){
    switch (nNet_GetTensorNumT(param)){
    case Int32_t:
      result += "I";
      break;

    case Real32_t:
      result += "R";
      break;

    default:
      break;
    }
  }

  return result;
}

PERFORM(add){
  using namespace nn::device::metal;
  using namespace nn::interpreter;
  using namespace nn::tensor;

 CREATE_COMMAND_ENCODER()
  auto left     = std::get<1>(opw->inputs()[0]);
  auto right    = std::get<1>(opw->inputs()[1]);
  auto output   = opw->outputs()[0];
  auto type     = TensorW::Unknown;

  if (function == "Buffer_Add_Forward"){
    type = TensorW::BufferT;

    PUT_BUFFER_INPUT(0, left);
    PUT_BUFFER_INPUT(1, right);
    PUT_BUFFER_OUTPUT(2, output);
    PUT_VALUE(3, nNet_GetArraySizeT(output));
    PUT_VALUE(4, nNet_GetArraySizeT(left));
    PUT_VALUE(5, nNet_GetArraySizeT(right));
  }
 #if ENABLE_METAL_TEXTURE2D
  else if (function == "Texture2D_Add_Forward"){
    return NoSupport;
  }
 #endif
  else return NoSupport;

 FINISH_CONFIGURE_ENCODER(type, 0)
}

Registration doRegisterAdd(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = kernels::generic::add::Prepare;
  result.eval   = add::Eval;
  result.derive = add::Derive;
  return result;
}
} // namespace metal
} // namespace kernels
} // namespace nn
