#include "devices/generic/generic.h"
#include "devices/metal/metal.h"
#include "devices/metal/macros.h"

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
namespace kernels{
namespace generic{
namespace reshape{
bool Eval(Operator op, Interpreter interpreter);
bool Derive(Operator op, Interpreter interpreter);
} // namespace reshape
} // namespace generic

namespace metal{
namespace reshape{
bool Eval(Operator op, Interpreter interpreter){
  return generic::reshape::Eval(op, interpreter);
}

bool Derive(Operator op, Interpreter interpreter){
  return generic::reshape::Derive(op, interpreter);
}
} // namespace 

Registration doRegisterReshape(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = kernels::generic::reshape::Prepare;
  result.eval   = reshape::Eval;
  result.derive = reshape::Derive;
  return result;
}
} // namespace metal
} // namespace kernels
} // namespace nn
