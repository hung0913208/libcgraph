#include "devices/generic/generic.h"
#include "devices/generic/math.h"

#include "devices/metal/metal.h"
#include "devices/metal/macros.h"

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

#define CONV2D_FUNCTION_POS      0
#define CONV2D_FUNCTION_TYPE_POS 14
#define CONV2D_FILTER_TYPE_POS   22

namespace nn{
namespace kernels{
namespace metal{
DEFINE_EVAL(conv2d);
DEFINE_DERIVE(conv2d);

MATRIX(conv2d){
  auto params = {
    std::get<1>(opw->inputs()[0]), 
    std::get<1>(opw->inputs()[1]), 
    opw->outputs()[0]
  };
  std::string result;

  for (auto param: params){
    switch (nNet_GetTensorNumT(param)){
    case Int32_t:
      result += "I";
      break;

    case Real32_t:
      result += "R";
      break;

    default:
      break;
    }
  }

  return result;
}

SELECT(conv2d){
  if (!strcmp(opw->get<char*>("data_format"), "NHWC"))
    return { "Buffer_Conv2D_Forward_NHWC" };
  else
    return { "Buffer_Conv2D_Forward_NCHW" };
}

PERFORM(conv2d){
  using namespace nn::generic::math;
  using namespace nn::device::metal;
  using namespace nn::interpreter;
  using namespace nn::tensor;

  const std::string buffer_function = "Buffer_Conv2D_Forward";

 CREATE_COMMAND_ENCODER()
  auto type   = TensorW::Unknown;

  if (function.compare(0, buffer_function.length() - 1, buffer_function)){
    auto input  = std::get<1>(opw->inputs()[0]);
    auto filter = std::get<1>(opw->inputs()[1]);
    auto output = opw->outputs()[0];

    auto strides_H = int(opw->get<int64_t>("strides.H"));
    auto strides_W = int(opw->get<int64_t>("strides.W"));
    auto dilate_H  = int(opw->get<int64_t>("dilations.H"));
    auto dilate_W  = int(opw->get<int64_t>("dilations.W"));
    auto filter_H  = filter->dims[3];
    auto filter_W  = filter->dims[2];

    int output_W, output_H, input_W, input_H;

    type = TensorW::BufferT;
    PUT_BUFFER_INPUT(0, input);
    PUT_BUFFER_INPUT(1, filter);
    PUT_BUFFER_OUTPUT(2, output);

    if (!strcmp(opw->get<char*>("data_format"), "NHWC")){
      output_H = output->dims[2]; // H
      output_W = output->dims[1]; // H
      input_H  = input->dims[2]; // W
      input_W  = input->dims[1]; // W

      PUT_VALUE(3, input->dims[3]); // N
      PUT_VALUE(4, input->dims[2]); // H
      PUT_VALUE(5, input->dims[1]); // W
      PUT_VALUE(6, input->dims[0]); // C

      PUT_VALUE(11, output->dims[3]); // N
      PUT_VALUE(12, output->dims[2]); // H
      PUT_VALUE(13, output->dims[1]); // W
      PUT_VALUE(14, output->dims[0]); // C
    } else {
      output_H = output->dims[1]; // H
      output_W = output->dims[0]; // W
      input_H  = input->dims[1]; // H
      input_W  = input->dims[0]; // W

      PUT_VALUE(3, input->dims[3]); // N
      PUT_VALUE(4, input->dims[1]); // H
      PUT_VALUE(5, input->dims[0]); // W
      PUT_VALUE(6, input->dims[2]); // C

      PUT_VALUE(11, output->dims[3]); // N
      PUT_VALUE(12, output->dims[1]); // H
      PUT_VALUE(13, output->dims[0]); // W
      PUT_VALUE(14, output->dims[2]); // C
    }

    PUT_VALUE(7, filter_H);         // H
    PUT_VALUE(8, filter_W);         // W
    PUT_VALUE(9, filter->dims[1]);  // iC
    PUT_VALUE(10, filter->dims[0]); // oC

    PUT_VALUE(15, int(opw->get<int64_t>("strides.N")));
    PUT_VALUE(16, strides_H);
    PUT_VALUE(17, strides_W);
    PUT_VALUE(18, int(opw->get<int64_t>("strides.C")));

    PUT_VALUE(19, dilate_H);
    PUT_VALUE(20, dilate_W);
    PUT_VALUE(21, computePadding(strides_H, dilate_H, input_H, filter_H, output_H));
    PUT_VALUE(22, computePadding(strides_W, dilate_W, input_W, filter_W, output_W));
  }
 #if ENABLE_METAL_TEXTURE2D
  else if (function == "Texture2D_Conv2D_Forward"){
    return NoSupport;
  }
 #endif
  FINISH_CONFIGURE_ENCODER(type, 0)
}

Registration doRegisterConv2D(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = kernels::generic::conv2d::Prepare;
  result.eval   = conv2d::Eval;
  result.derive = conv2d::Derive;
  return result;
}
} // namespace metal
} // namespace kernels
} // namespace nn
