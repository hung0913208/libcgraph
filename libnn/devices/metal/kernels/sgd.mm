#include "devices/generic/generic.h"
#include "devices/metal/metal.h"
#include "devices/metal/macros.h"

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
namespace kernels{
namespace metal{
#if ENABLE_FORWARD_BACKWARD_OPERATORS
DEFINE_EVAL(sgd);
DEFINE_DERIVE(sgd);
SELECT(sgd){
  return { "Buffer_Stochastic_Gradient_Descent" };
}

PERFORM(sgd){
  return NoSupport;
}

Registration doRegisterSGD(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = kernels::generic::Prepare;
  result.eval = softmax::Eval;
  return result;
}
#endif
} // namespace metal
} // namespace kernels
} // namespace nn
