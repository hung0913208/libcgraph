#ifdef LIBCGRAPH_GIT_REFSPEC
#include "devices/generic/generic.h"
#include "devices/generic/math.h"
#include "devices/metal/metal.h"
#include "devices/metal/macros.h"
#include "devices/common.h"
#else
#include "generic.h"
#include "math.h"
#include "metal.h"
#include "metal_macros.h"
#include "dev_common.h"
#endif

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

namespace nn{
namespace kernels{
namespace metal{
DEFINE_EVAL(softmax);
DEFINE_DERIVE(softmax);

SELECT(softmax){ return { "Buffer_Softmax_Forward" }; }
MATRIX(softmax){ 
  auto params = { std::get<1>(opw->inputs()[0]), opw->outputs()[0] };
  std::string result;

  for (auto param: params){
    switch (nNet_GetTensorNumT(param)){
    case Int32_t:
      result += "I";
      break;

    case Real32_t:
      result += "R";
      break;

    default:
      break;
    }
  }
  return result;
}

PERFORM(softmax){
  using namespace nn::device::metal;
  using namespace nn::interpreter;
  using namespace nn::tensor;

 CREATE_COMMAND_ENCODER()
  auto  type   = TensorW::Unknown;

  if (function == "Buffer_Softmax_Forward"){
    Tensor output = opw->outputs()[0];
    Tensor input  = std::get<1>(opw->inputs()[0]);

    type = TensorW::BufferT;

    PUT_BUFFER_INPUT(0, input);
    PUT_BUFFER_OUTPUT(2, output);
    PUT_VALUE(1, nNet_GetArraySizeT(output))
  }
 #if ENABLE_METAL_TEXTURE2D
  else if (function == "Texture2D_Softmax_Forward"){
    return NoSupport;
  }
 #endif
  else return NoSupport;
 FINISH_CONFIGURE_ENCODER(type, 0)
  return NoSupport;
}

Registration doRegisterSoftmax(){
  Registration result;

  memset(&result, 0, sizeof(Registration));
  result.event.prepare = kernels::generic::softmax::Prepare;
  result.eval   = softmax::Eval;
  result.derive = softmax::Derive;
  return result;
}
} // namespace metal
} // namespace kernels
} // namespace nn
