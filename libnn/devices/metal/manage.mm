#include "interpreter.h"
#include "tensor.h"
#include "utils.h"

#import <Metal/Metal.h>

#include "./metal.h"

DEFINE_OP(metal, Add);
DEFINE_OP(metal, MatMul);
DEFINE_OP(metal, Reshape);

DEFINE_OP(metal, Conv2D);
DEFINE_OP(metal, Relu);
DEFINE_OP(metal, MaxPool);

DEFINE_OP(metal, Softmax);

#if APPLE
namespace nn {
namespace device {
namespace metal {
Device create(Device pattern, Interpreter interpreter);
Device release(Device device);

Operator find(Device device, const char *name);
Tensor   tensor(Interpreter interpreter, Tensor tensor);

bool inspect(Device device);
bool install(Device device, const char *name, Operator op);
bool scan_manager(Device device, Operator op);
bool scan_consumer(Device device, Operator op);

MetalManager::MetalManager(id<MTLDevice> device, int index)
    : DeviceW("metal"), _device{device}, _index{index} {
  /* @NOTE: install global configure, notice this device is an abstract device
   * and it can't be used to create any operators */

  if (_device != nullptr && isSupport()){
    interface()->_template._priv = this;

    interface()->level = SPECIFIC_GPU_PLATFORM;

    interface()->_template.create  = metal::create;
    interface()->_template.release = metal::release;
    interface()->_template.scan    = metal::scan_manager;
    interface()->_template.inspect = metal::inspect;

    if ((interface()->_enable = (bool*) calloc(1, sizeof(bool))))
      *(interface()->_enable) = _device != nullptr;

    /* @NOTE: install generic's operators */
    FUNCTION(metal, interface(), Add);
    FUNCTION(metal, interface(), MatMul);
    FUNCTION(metal, interface(), Reshape);
    FUNCTION(metal, interface(), Conv2D);
    FUNCTION(metal, interface(), Relu);
    FUNCTION(metal, interface(), MaxPool);
    FUNCTION(metal, interface(), Softmax);
  } else
    throw NoSupport;
}

bool MetalManager::isSupport(){
  std::vector<MTLFeatureSet> device_ids;

 #if DEBUG
  std::vector<std::string> device_infos;
  std::vector<std::string> os_versions;
 #endif
 #if IOS_DEVICE
  if (@available(iOS 8.0, *)){
    device_ids = {MTLFeatureSet_iOS_GPUFamily1_v1,
                  MTLFeatureSet_iOS_GPUFamily2_v1};
   #if DEBUG
    device_infos = {"Apple A7", "Apple A8"};
    os_versions = {"iOS 8", "iOS 8"};
   #endif
  } else if (@available(iOS 9.0, *)){
    device_ids = {
        MTLFeatureSet_iOS_GPUFamily1_v1, MTLFeatureSet_iOS_GPUFamily2_v1,
        MTLFeatureSet_iOS_GPUFamily1_v2, MTLFeatureSet_iOS_GPUFamily2_v2};
   #if DEBUG
    device_infos = {"Apple A7", "Apple A8", "Apple A7",
                    "Apple A8", "Apple A9", "Apple A7"};
    os_versions = {"iOS 8", "iOS 8", "iOS 9", "iOS 9", "iOS 9"};
   #endif
  } else if (@available(iOS 10.0, *)){
    device_ids = {
        MTLFeatureSet_iOS_GPUFamily1_v1, MTLFeatureSet_iOS_GPUFamily2_v1,
        MTLFeatureSet_iOS_GPUFamily1_v2, MTLFeatureSet_iOS_GPUFamily2_v2,
        MTLFeatureSet_iOS_GPUFamily3_v1, MTLFeatureSet_iOS_GPUFamily1_v3,
        MTLFeatureSet_iOS_GPUFamily2_v3};
   #if DEBUG
    device_infos = {"Apple A7", "Apple A8", "Apple A7", "Apple A8",
                    "Apple A9", "Apple A7", "Apple A8", "Apple A9"};
    os_versions = {"iOS 8", "iOS 8",  "iOS 9", "iOS 9",
                   "iOS 9", "iOS 10", "iOS 10"};
   #endif
  } else if (@available(iOS 11.0, *)){
    device_ids = {
        MTLFeatureSet_iOS_GPUFamily1_v1, MTLFeatureSet_iOS_GPUFamily2_v1,
        MTLFeatureSet_iOS_GPUFamily1_v2, MTLFeatureSet_iOS_GPUFamily2_v2,
        MTLFeatureSet_iOS_GPUFamily3_v1, MTLFeatureSet_iOS_GPUFamily1_v3,
        MTLFeatureSet_iOS_GPUFamily2_v3, MTLFeatureSet_iOS_GPUFamily3_v2,
        MTLFeatureSet_iOS_GPUFamily1_v4, MTLFeatureSet_iOS_GPUFamily2_v4,
        MTLFeatureSet_iOS_GPUFamily3_v3, MTLFeatureSet_iOS_GPUFamily4_v1};
   #if DEBUG
    device_infos = {"Apple A7", "Apple A8", "Apple A7", "Apple A8",
                    "Apple A9", "Apple A7", "Apple A8", "Apple A9",
                    "Apple A7", "Apple A8", "Apple A9", "Apple A11"};
    os_versions = {"iOS 8",  "iOS 8",  "iOS 9",  "iOS 9",
                   "iOS 9",  "iOS 10", "iOS 10", "iOS 10",
                   "iOS 11", "iOS 11", "iOS 11", "iOS 11"};
   #endif
  } else if (@available(tvOS 9.0, *)){
    device_ids = {MTLFeatureSet_tvOS_GPUFamily1_v1};
    #if DEBUG
    device_infos = {"Apple A8"};
    os_versions = {"tvOS 9"};
   #endif
  } else if (@available(tvOS 10.0, *)){
    device_ids = {MTLFeatureSet_tvOS_GPUFamily1_v1,
                  MTLFeatureSet_tvOS_GPUFamily1_v2};
   #if DEBUG
    device_infos = {"Apple A8", "Apple A8"};
    os_versions = {"tvOS 9", "tvOS 10"};
   #endif
  } else if (@available(tvOS 11.0, *)){
    device_ids = {
        MTLFeatureSet_tvOS_GPUFamily1_v1, MTLFeatureSet_tvOS_GPUFamily1_v2,
        MTLFeatureSet_tvOS_GPUFamily1_v3, MTLFeatureSet_tvOS_GPUFamily2_v1 };
   #if DEBUG
    device_infos = {"Apple A8", "Apple A8", "Apple A8", "Apple A10"};
    os_versions = {"tvOS 9", "tvOS 10", "tvOS 11", "tvOS 11"};
   #endif
  }
 #elif OSX_DEVICE
 #if defined(OSX_PRODUCT_VERSION) && OSX_PRODUCT_VERSION >= 101304
  device_ids = { MTLFeatureSet_macOS_GPUFamily1_v1,
                 MTLFeatureSet_macOS_GPUFamily1_v2,
                 MTLFeatureSet_macOS_GPUFamily1_v3 };
 #else
  device_ids = { MTLFeatureSet_OSX_GPUFamily1_v1,
                 MTLFeatureSet_OSX_GPUFamily1_v2 };
 #endif

 #if DEBUG
 #if defined(OSX_PRODUCT_VERSION) && OSX_PRODUCT_VERSION >= 101304
  device_infos = {"Mac PC", "Mac PC", "Mac PC"};
  os_versions = {"macOS 10.11", "macOS 10.12", "macOS 10.13"};
 #else
  device_infos = {"Mac PC", "Mac PC"};
  os_versions = {"macOS 10.11", "macOS 10.12"};
 #endif
 #endif  // DEBUG
 #endif  // OSX_DEVICE || IOS_DEVICE

  for (int i = 0; i < cast_(i, device_ids.size()); ++i){
    if ([_device supportsFeatureSet:device_ids[i]]){
     #if DEBUG
      _hardware = device_infos[i];
      _os = os_versions[i];
     #endif
      return true;
    }
  }

  return false;
}

MetalConsumer::MetalConsumer(MetalManager *manager, Interpreter interpreter)
    : DeviceW{"metal"}, _manager{manager}, _interpreter{interpreter}{
  /* @TODO: implement MetalConsumer, a helper which is used to interact directly
   * with GPU. MetalConsumer doesn't serve exact only a real device, it should 
   * easily swift between device if it want to improve performance */

  interface()->_template._priv = this;

  interface()->level = SPECIFIC_GPU_PLATFORM;
  interface()->_enable = manager->interface()->_enable;
  interface()->tensor = metal::tensor;

  interface()->_template.find    = metal::find;
  interface()->_template.scan    = metal::scan_consumer;
  interface()->_template.inspect = metal::inspect;
  interface()->_template.release = metal::release;
  interface()->_template.install = metal::install;

  _library = nullptr;
  _queue   = [_manager->device() newCommandQueue];

  if (!_queue) 
    throw BadAccess.reason("newCommandQueue()");
 // #if DEBUG
 //  if ([_queue respondsToSelector:@selector(insertDebugCaptureBoundary)]){
 //    [_queue insertDebugCaptureBoundary];
 //  } else if (@available(iOS 11.0, *) && 
 //      [MTLCaptureManager respondsToSelector:@selector(respondsToSelector:)]){
 //    [[MTLCaptureManager sharedCaptureManager] startCaptureWithCommandQueue:_queue];
 //  }
 // #endif
}

MetalConsumer::~MetalConsumer(){
 // #if DEBUG
 //  if (@available(iOS 11.0, *)){
 //    if (![_queue respondsToSelector:@selector(insertDebugCaptureBoundary)]){
 //      if ([MTLCaptureManager respondsToSelector:@selector(respondsToSelector:)])
 //        [[MTLCaptureManager sharedCaptureManager] stopCapture];
 //    }
 //  }
 // #endif

  interface()->_enable = nullptr;
}

base::Error MetalConsumer::install(){
  using Sources = std::map<std::string, std::string>;
  using namespace base;

  Error error;

  if (config::get<std::string>("metal") == "file"){
    /* @NOTE: get field 'metal.file' and load metalib to memory */

    if (config::get("metal.file").type().get() == typeid(std::string))
      error = install(config::get<std::string>("metal.file"));
    else
      error = BadAccess << "field \'metal.file\'";
  } else if (config::get<std::string>("metal") == "bundle"){
    /* @NOTE: get field 'metal.bundle' and load metalib to memory */

    if (config::get("metal.bundle").type().get() == typeid(std::string)){
      auto bundle = base::config::get<std::string>("metal.bundle").c_str();

      NSString* nsbundle = [NSString stringWithCString:bundle 
                                              encoding:NSUTF8StringEncoding];
      NSString* nspath   = [[NSBundle mainBundle] pathForResource:nsbundle
                                                           ofType:@"metallib"]; 
      error = install(nspath);
    } else {
      error = BadAccess << "field \'metal.bundle\'";
    }
  } else if (config::get("metal.sources").type().get() == typeid(std::string)){
    /* @NOTE: get field 'metal.source' and set _resource */
   #if DEV
    if (config::get<std::string>("metal.sources") == "bundle"){
      /* @TODO: get everything inside bundle */
    } else {
      auto path = config::get<std::string>("metal.sources");

      if (!path.compare(0, sizeof("bundle://"), "bundle://")){
        /* @TODO: this path is a part of main bundle */
      } else {
        /* @TODO: read all file inside path and build a library */
      }
    }
   #else
    error = NoSupport;
   #endif
  } else if (config::get("metal.sources").type().get() == typeid(Sources)){
    _resource = config::get<Sources>("metal.sources");
  }

  if (_library == nullptr)
    _library = [_manager->device() newDefaultLibrary];

  return _library? NoError: error;
}

base::Error MetalConsumer::install(std::string&& path){
  NSString* nspath  = [NSString stringWithCString: path.c_str()
                                         encoding: NSUTF8StringEncoding];
  NSError*  nserror = nil;

  _library = [_manager->device() newLibraryWithFile:nspath error:&nserror];

  if (_library == nullptr)
    return BadLogic.reason([[nserror localizedDescription] UTF8String]);
  return NoError;
}

base::Error MetalConsumer::install(NSString* path){
  NSError*  nserror = nil;

  _library = [_manager->device() newLibraryWithFile:path error:&nserror];

  if (_library == nullptr)
    return BadLogic.reason([[nserror localizedDescription] UTF8String]);
  return NoError;
}

id<MTLComputePipelineState> MetalConsumer::pipeline(std::string name){
  NSError*  nserror = nil;
  NSString* nsname  = [NSString stringWithCString: name.c_str()
                                         encoding: NSUTF8StringEncoding];

  id<MTLComputePipelineState> result;
  id<MTLFunction>             mtlfunction;

  auto interw = nNet_GetInterpreterW(_interpreter);

  if (!library()){
    interw->error(DoNothing << "please install metalib first");
  }

  /* @NOTE: build a metal-function from the library */
  mtlfunction = [library() newFunctionWithName: nsname];

  if (mtlfunction == nullptr){
      interw->error(NotFound << name);
  } else nserror = nil;

  /* @TODO: how to implement a way to use many more functions on a single 
   * pipeline */
  result = [device() newComputePipelineStateWithFunction:  mtlfunction
                                                   error: &nserror];
  if (result == nullptr){
    if (nserror){
      interw->error(BadLogic << [[nserror localizedDescription] UTF8String]);
    } else {
      interw->error(BadAccess << "newComputePipelineStateWithFunction()");
    }
  }

  return result;
}

Device create(Device pattern, Interpreter interpreter){
  auto manager = dynamic_cast<MetalManager *>(nNet_GetDeviceW(pattern));

  if (manager){
    auto consumer = (new metal::MetalConsumer(manager, interpreter));

    if (!consumer->install())
      return consumer->interface();

    delete consumer;
    return nullptr;
  } else
    return pattern;
}

Device release(Device device){
  if (nNet_CheckStatus() == nn::Releasing && nNet_IsTemplateDevice(device)){
    auto manager = dynamic_cast<MetalManager *>(nNet_GetDeviceW(device));

    if (manager){
      delete manager;
      return nullptr;
    }
  }

  if (nNet_CheckStatus() == nn::Releasing || !nNet_IsTemplateDevice(device)){
    auto consumer = dynamic_cast<MetalConsumer *>(nNet_GetDeviceW(device));

    if (consumer){
      delete consumer;
      return nullptr;
    }
  }
  return nNet_RemoveDevice(device);
}

Operator find(Device device, const char *name){
  auto devicew = nNet_GetDeviceW(device);
  auto delegate = dynamic_cast<MetalConsumer *>(devicew);
  auto operators = delegate ? (&delegate->operators()) : nullptr;

  if (!name || !operators) return nullptr;
  else if (operators->find(std::string(name)) != operators->end())
    return (*operators)[std::string(name)];
  return nullptr;
}

Tensor tensor(Interpreter interpreter, Tensor tensor){
  using namespace nn::interpreter;
  using namespace nn::tensor;

  auto device = reinterpret_cast<Device>(tensor->_device);
  auto result = tensor;

  if (nn::utils::outdate(interpreter, tensor))
    return nNet_RemoveTensor(tensor);

  if (!utils::response<DeviceW, MetalConsumer>(device->_priv))
    return nNet_RemoveTensor(tensor);

  /* @NOTE: because we need to convert tensor from device to another device, 
   * so we must convert data first 
   */
  if (nNet_GetTensorType(tensor) != NilT){
    try{
      auto content = new TensorW(interpreter, tensor, nNet_GetTensorNumT(tensor));
      auto shape   = nNet_CreateTensorShapeC1(interpreter, 0);

      if (nNet_GetTensorType(tensor) >= VectorT){

        /* @NOTE: only call tensor->release because we don't want to remove tensor
         * completely, we only care about remove its content and rebuild a new 
         * content. In somecase, we must preseve its shape too.
         */

        if (!nNet_GetTensorShapeW(interpreter, tensor, shape)){
          return utils::clearFaulty(interpreter, tensor,
                                    BadAccess.reason("nNet_GetTensorShape()"));
        }
      }

      if (tensor->release)
        tensor->release(tensor, interpreter);
      else {
        if (tensor->_content)
          free (tensor->_content);
        tensor->_content = nullptr;
      }

      tensor->_content = content;
      if (nNet_GetTensorType(tensor) >= VectorT){
        if (!nNet_SetTensorShapeW(interpreter, tensor, shape)){
          result = utils::clearFaulty(interpreter, tensor,
                                      BadAccess <<"nNet_SetTensorShapeW()");
        }
      }

      if (nNet_RemoveTensorShapeW(interpreter, shape)){
        nNet_GetInterpreterW(interpreter)->error(
                                      BadAccess << "nNet_RemoveTensorShapeW()");
      }
    } catch(base::Error& error){
      return utils::clearFaulty(interpreter, tensor, error);
    }
  }

  /* @NOTE: install static functions which are used to manipulate with this 
   * tensor */

  /* @NOTE: constructor and destructor */
  tensor->init = [](Tensor self, Interpreter interpreter, int size, 
                    int type, bool force) -> bool{
    auto device = reinterpret_cast<Device>(self->_device);

    if (!self->_content || force || self->type != type || type == Unknown){
      /* @NOTE: verify this tensor must be created by MetalConsumer */

      if (utils::response<DeviceW, MetalConsumer>(device->_priv) == nullptr)
        return false;

      /* @NOTE: init will be call in 3 cases
       * - Realloc
       * - Change numtype 
       * - Reshape with shape is nil
       */

      if ((self->_content && force) || type == Unknown){
        if (self->_content){
          delete utils::response<GPContent, TensorW>(self->_content);
          self->_content = nullptr;
        }
      }

      if (!self->_content && type != Unknown){
        self->_content = new TensorW(interpreter, self, type, true);
        self->release  = [](Tensor self, Interpreter UNUSED(interpreter)){
          if (utils::response<GPContent, TensorW>(self->_content))
            delete utils::response<TensorW>(self->_content);
          self->_content = nullptr;
        };
      }

      if (type != Unknown){
        auto content = utils::response<GPContent, TensorW>(self->_content);
        return (content)? content->init(size, type, force): false;
      }
    }
    return true;
  };

  tensor->release = [](Tensor self, Interpreter UNUSED(interpreter)){
    if (utils::response<GPContent, TensorW>(self->_content))
      delete utils::response<TensorW>(self->_content);
    self->_content = nullptr;
  };

  tensor->_array.get = [](Tensor self) -> Array{
    auto control = utils::response<GPContent, TensorW>(self->_content);
    auto result  = nNet_AllocateArray(nNet_GetArraySizeT(self), 
                                      nNet_GetTensorNumT(self));

    if (!result) return nullptr;
    else if (control && !control->get(result))
      return result;
    else {
      free(result);
      return nullptr;
    }
  };

  tensor->_array.set = [](Tensor self, Array array) -> bool{
    auto control = utils::response<GPContent, TensorW>(self->_content);
    auto result  = (control)? !control->set(array): false;

    if (result)
      free(array);
    return result;
  };

  return tensor;
}

bool inspect(Device device){
  auto devicew = nNet_GetDeviceW(device);
  auto consumer = dynamic_cast<MetalConsumer *>(devicew);
  auto manager = dynamic_cast<MetalManager *>(devicew);

  if (consumer){
    return *(device->_enable);
  } else if (manager){
    return *(device->_enable);
  } else
    return false;
}

bool install(Device device, const char *name, Operator op){
  auto delegate = dynamic_cast<MetalConsumer *>(nNet_GetDeviceW(device));
  auto operators = delegate ? &delegate->operators() : nullptr;

  if (operators->find(std::string{name}) != operators->end())
    return BadLogic.reason("duplicate installing op " + std::string{name});

  (*operators)[std::string(name)] = op;
  return true;
}

bool scan_manager(Device device, Operator sample){
  auto delegate = dynamic_cast<MetalManager*>(nNet_GetDeviceW(device));

  if (!delegate) return false;
  for (auto &item : delegate->operators()){
    if (std::get<1>(item) == sample)
      return true;
  }
  return false;
}

bool scan_consumer(Device device, Operator sample){
  auto delegate = dynamic_cast<MetalManager*>(nNet_GetDeviceW(device));

  if (!delegate)
    return false;
  for (auto &item : delegate->operators()){
    if (std::get<1>(item) == sample)
      return true;
  }
  return false;
}
} // namespace metal

base::Error useMetal(std::vector<Device> &result){
  using namespace interpreter;
  using namespace base;

  /* @NOTE: Metal support multi-devices and this cause a big trouble. To solve
   * it, i think we will decice 2 things:
   * - Each device will be create separatedly by device_id and device_name
   * (more than 1 device for 1 abstract device like 'OpenCL', 'Metal', ...).
   * - Operator will cling with real device's session, which was create by
   * global devices.
   */
  if (config::get("metal").type().get() != typeid(std::string))
    return BadAccess << "please config field group \'metal\' first";

  try {
   #if OSX_DEVICE
    NSArray<id<MTLDevice>> *devices = MTLCopyAllDevices();
    DeviceW *first{nullptr}, *previous{nullptr}, *last{nullptr};

    /* @NOTE: check if there are more than 1 device, we must create a enqueue
     * which support a better way to access devices inside an abstract device
     */

    for (std::size_t i = 0; i < [devices count]; ++i){
      last = new metal::MetalManager(devices[i], i);

      if (dynamic_cast<metal::MetalManager*>(last)->device() != nullptr){
        /* @NOTE: makesure that device is ready to support metal gpu */

        if (*(last->interface()->_enable) == false){
          delete last;
          continue;
        } 

        /* @NOTE: pass everything, ready to install this manager */
        if (previous)
          last->encircle(previous);
        else
          first = last;

        result.push_back(last->interface());
      } else delete last;
    }

    /* @NOTE: connect the begin and the end to be a completed enqueue */
    if (!first || !last)
      return DoNothing.reason("metal only support real device");
    else if (first != last) first->encircle(last);

   #elif IOS_DEVICE
    if (@available(iOS 8.0, *)){
      auto device   = MTLCreateSystemDefaultDevice();

      if (device && [device respondsToSelector:@selector(supportsFeatureSet:)]){
        auto manager = new metal::MetalManager(device, 0);

        if (dynamic_cast<metal::MetalManager*>(manager)->device() != nullptr){
          /* @NOTE: makesure that device is ready to support metal gpu */

          if ((*(manager->interface()->_enable)) == false)
            delete manager;
          else
            result.push_back(manager->interface());
        } else delete manager;
      }
    }
   #else
    return NoSupport.reason("only support Mac devices")
   #endif
  } catch (base::Error &error){
    return error;
  }

  return (result.size() > 0) ? NoError : NoSupport;
}
} // namespace device
} // namespace nn
#endif