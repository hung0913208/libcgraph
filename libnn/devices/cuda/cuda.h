#if !defined(LIBNN_DEVICE_CUDA_H_) && __cplusplus
#define LIBNN_DEVICE_METAL_H_
#include <libbase/all.hpp>

#include "interpreter.h"
#include "internal.h"
#include "tensor.h"
#include "utils.h"

#include "devices/common.h"

namespace nn{
namespace device{
namespace cuda{
struct TensorW: tensor::GPContent{
 public:
  enum Type{ Unknown = 0, BufferT = 1, Texture2dT = 2, MTLTexture3dT = 3 };

 private:

 public:
  explicit TensorW(Interpreter interpreter, Tensor tensor, int type);
  virtual ~TensorW();

 public:
  inline Type type(){ return _type; }
  inline base::Auto& cache(){ return _cache; }

 public:
  bool init(int size, int type, bool force);

  base::Error get(Array array);
  base::Error set(Array array);
  base::Error make(Array data, int length, Type type);

 private:
  void init() final{}

 private:
 
 private:
  base::Auto  _cache;
  std::size_t _length;

 private:
  Tensor _self;
  Type   _type;
};
} // namespace cuda
} // namespace device
} // namespace nn
#endif  // LIBNN_DEVICE_CUDA_H_