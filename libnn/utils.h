#if !defined(LIBNN_UTILS_H_) && __cplusplus
#define LIBNN_UTILS_H_

#include <cmath>

#include "objmacros.h"
#include "tensor.h"
#include "interpreter.h"

#ifdef LIBCGRAPH_GIT_REFSPEC
#include <libbase/logcat.hpp>
#else
#include "logcat.hpp"
#include "tie.hpp"
#endif

namespace nn{
namespace utils{
enum DiffS{
  UnkDiff    = -1,
  NoDiff     =  0,
  DataDiff   =  1,
  DimDiff    =  2,
  SizeDiff   =  3,
  TypeDiff   =  4,
  StructDiff =  5,
};

template<typename T>
bool aequal(T x, T y, T epsilon = std::numeric_limits<T>::epsilon()){
  if (std::abs(x) < std::abs(y))
    return std::abs(x - y) <= std::abs(x)*epsilon;
  else
    return std::abs(x - y) <= std::abs(y)*epsilon;
}

template<typename Type> 
base::Error assign(Tensor UNUSED(tensor), std::vector<Type>&& UNUSED(array)){
  return NoSupport; 
}

template<>
base::Error assign<Int32>(Tensor tensor, std::vector<Int32>&& array);
template<>
base::Error assign<Int64>(Tensor tensor, std::vector<Int64>&& array);
template<> 
base::Error assign<Real32>(Tensor tensor, std::vector<Real32>&& vector);
template<>
base::Error assign<Real64>(Tensor tensor, std::vector<Real64>&& vector);

template<typename Type>
std::vector<Type> values(Tensor UNUSED(tensor)){ throw NoSupport; }

template<> std::vector<Int32> values<Int32>(Tensor tensor);
template<> std::vector<Int64> values<Int64>(Tensor tensor);
template<> std::vector<Real32> values<Real32>(Tensor tensor);
template<> std::vector<Real64> values<Real64>(Tensor tensor);

template<typename Type> Type generate(std::vector<int> UNUSED(param)){ 
  throw NoSupport; 
}

template<> Shape generate<Shape>(std::vector<int> dims);
template<> Tensor generate<Tensor>(std::vector<int> dims);
template<> Ordinate generate<Ordinate>(std::vector<int> ords);

inline std::vector<int> values(Shape shape){
  return std::vector<int>{shape->dims, shape->dims + shape->ndims};
}

Shape clearFaulty(Interpreter interpreter, Shape shape, base::Error error);
Tensor clearFaulty(Interpreter interpreter, Tensor tensor, base::Error error);
Ordinate clearFaulty(Interpreter interpreter, Ordinate ordinate, base::Error error);
Operator clearFaulty(Interpreter interpreter, Operator op, base::Error error);

bool outdate(Shape shape);
bool outdate(Tensor tensor);
bool outdate(Ordinate ordinate);

bool outdate(Interpreter interpreter, Shape shape);
bool outdate(Interpreter interpreter, Tensor tensor);
bool outdate(Interpreter interpreter, Ordinate ordinate);

template<typename Type> Type move(Type UNUSED(dst), Type UNUSED(src)){
  throw NoSupport;
}

template<> Tensor move<Tensor>(Tensor dst, Tensor src);
template<> Shape move<Shape>(Shape dst, Shape src);
template<> Ordinate move<Ordinate>(Ordinate dst, Ordinate src);

template<typename Type> Type copy(Interpreter UNUSED(dst_inter), Type UNUSED(dst), 
                                  Interpreter UNUSED(src_inter), Type UNUSED(src)){
  throw NoSupport;
}

template<> Tensor copy<Tensor>(Interpreter d_inter, Tensor d_obj,
                               Interpreter s_inter, Tensor s_obj);
template<> Shape copy<Shape>(Interpreter d_inter, Shape d_obj,
                             Interpreter s_inter, Shape s_obj);
template<> Ordinate copy<Ordinate>(Interpreter d_inter, Ordinate d_obj,
                                   Interpreter s_inter, Ordinate s_obj);

int diff(Tensor left, Tensor right, double epsilon = 0.0);
unsigned long size(Tensor tensor);

template<typename Type=void*>
Type null(Interpreter interpreter = nullptr, base::Error error = base::Error{}){
  nNet_GetInterpreterW(interpreter)->error(rvalue(error));
  return nullptr;
}

template<typename Type=void*>
Type make_null(std::function<void()> callback,
               Interpreter interpreter = nullptr,
               base::Error error = base::Error{}){
  callback();
  return null<Type>(interpreter, error);
}

template<typename Parrent, typename Child>
Child* response(Pointer ptr){
  return dynamic_cast<Child*>(reinterpret_cast<Parrent*>(ptr));
}

template<typename Type>
Type* response(Pointer ptr){ return reinterpret_cast<Type*>(ptr);  }

template<typename Type>
Type best(std::vector<Tensor> UNUSED(inputs)){ throw NoSupport; } 

template<> NumT best<NumT>(std::vector<Tensor> inputs);

template<typename Index, typename Value>
Value& mapByIndex(const std::map<Index, Value>& map, std::size_t index){
  return *(map.begin() + index);
}

template<typename NumT>
std::vector<NumT> range(NumT begin, NumT end, 
                        std::function<NumT(NumT)> eval = nullptr){
  std::vector<NumT> result;

  for (auto i = begin; i <= end; ++i){
    if (eval)
      result.push_back(eval(i));
    else
      result.push_back(i);
  }
  return result;
}

Operator clearFaulty(Interpreter interpreter, Operator op, base::Error error);
Ordinate clearFaulty(Interpreter interpreter, Ordinate ordinate, base::Error error);
Tensor   clearFaulty(Interpreter interpreter, Tensor tensor, base::Error error);
Shape    clearFaulty(Interpreter interpreter, Shape shape, base::Error error);

std::string shape(Tensor tensor);
std::string shape(std::vector<int> dims);

std::string print(Array array, int begin, int end, int type, int first_column = 0);
std::string print(Item item, int type);
std::string print(Ordinate ordinate);
std::string print(Shape shape);
std::string print(Tensor tensor);
std::string print(NumT type);

void sequence(int begin, std::vector<std::function<int()>> tasks);

namespace wstring{
std::vector<std::string> split(CString raw, const char sep);

template<typename ...Args>
base::Tie<std::string> tie(Args&... args){
  using Convert = std::function<base::Error(std::string&, base::Auto&)>;
  using Type    = std::reference_wrapper<const std::type_info>;

  std::vector<std::pair<Type, Convert>> converts{};
  converts.push_back(std::make_pair(
    std::cref(typeid(std::size_t)),
    Convert{[](std::string& input, base::Auto& output) -> base::Error{
      output.move(std::size_t(stoi(input)));
      return NoError; 
      }
    })
  );
  converts.push_back(std::make_pair(
    std::cref(typeid(int)),
    Convert{[](std::string& input, base::Auto& output) -> base::Error{
      output.move(stoi(input));
      return NoError; 
      }
    })
  );
  converts.push_back(std::make_pair(
    std::cref(typeid(std::string)),
    Convert{[](std::string& input, base::Auto& output) -> base::Error{
      output.move(std::string{input});
      return NoError; 
      }
    })
  );
  converts.push_back(std::make_pair(
    std::cref(typeid(CString)),
    Convert{[](std::string& input, base::Auto& output) -> base::Error{
      output = CString{input.c_str()};
      return NoError; 
      }
    })
  );
  return base::Tie<std::string>::tie(rvalue(converts), args...);
}
} // namespace wstring

namespace debug{
template<typename Type>
std::string to_string(std::vector<Type>& vector){
  auto result = std::string{"[ "};
  
  if (vector.size() > 0){
    result += std::to_string(vector[0]);
    for (auto i = 1; i < cast_(i, vector.size()); ++i)
      result += ", " + std::to_string(vector[i]);
  }

  return result + " ]";
}
} // namespace debug
} // namespace utils
} // namespace nn
#endif  // LIBNN_UTILS_HPP_
