#ifndef LIBNN_TENSOR_H_
#define LIBNN_TENSOR_H_
#include "type.h"

struct SystemDef;
struct TensorDef;
struct CInterpreter;
struct OperatorDef;

#if __cplusplus
extern "C"{
#endif

typedef struct CInterpreter* Interpreter;
typedef struct SystemDef*    Device;
typedef struct OperatorDef*  Operator;

enum TensorT{
  NilT    = 0,
  ScalarT = 1,
  VectorT = 2,
  MatrixT = 3,
  TensorT = 4
};

enum NumT{
  Unknown  = 0,
  Bool_t   = 1,
  UInt_t   = 2,
  Int32_t  = 3,
  Int64_t  = 4,
  Real32_t = 5,
  Real64_t = 6,
  String_t = 7
};

struct ShapeDef{
  int  ndims, max;
  int *dims;
};

struct OrdinateDef{
  int *begins, *ends;
  int  ndims, max;
};
#if __cplusplus
}
#endif

#if __cplusplus 
extern "C"{
#endif

struct TensorDef;
struct ArrayDef{
  bool  (*set)(struct TensorDef* self, void* array);
  void* (*get)(struct TensorDef* self);
};

struct TensorDef{
  // @public:
  int  ndims, type, unit;

  // @private:
  int   *_begins, *_ends;
  long   _size, _max;
  bool   _const;
  Device _device;

  // @private:
  struct ArrayDef _array;

  Device (*device)(struct TensorDef* self, Interpreter interpreter);

  bool (*init)(struct TensorDef *self, Interpreter interpreter, int size, 
               int type, bool force);
  bool (*diff)(struct TensorDef* left, struct TensorDef* right);
  bool (*get)(struct TensorDef *self, struct OrdinateDef *ordinate,
              struct TensorDef *result);
  bool (*set)(struct TensorDef *self, struct OrdinateDef *ordinate,
              struct TensorDef *values);
  bool (*reshape)(struct TensorDef *self, struct ShapeDef *shape);
  void (*release)(struct TensorDef *self, Interpreter interpreter);
  int *dims;
  void *_content;
};
#if __cplusplus
}
#endif

#if __cplusplus
extern "C"{
#endif
typedef struct ShapeDef     *Shape;
typedef struct TensorDef    *Tensor;
typedef struct OrdinateDef  *Ordinate;


/* @NOTE: C-API to define Tensor's properties, libnn will use to them to access
 * throught to tensor and modify Tensor's properties
 */

typedef void* Item;
typedef void* Array;
typedef int   Index;

/* @NOTE: constructor and destructor of TensorShape */
Shape nNet_CreateTensorShapeS(int ndims, ...);
Shape nNet_CreateTensorShapeW(int ndims, int *dims);
Shape nNet_CreateTensorShapeC1(Interpreter interpreter, int ndims, ...);
Shape nNet_CreateTensorShapeC2(Interpreter interpreter, int ndims, int *dims);

Shape nNet_RemoveTensorShape(Shape shape);
Shape nNet_RemoveTensorShapeW(Interpreter interpreter, Shape shape);

// -- @NOTE: utilities of TensorShape 
int nNet_GetShapeSize(Shape shape);
int nNet_GetShapeNDims(Shape shape);

/* @NOTE: constructor and destructor of Ordinate */
Ordinate nNet_CreateOrdinateS(Tensor tensor, int size, ...);
Ordinate nNet_CreateOrdinateW1(Tensor tensor, int *ordinate, int size);
Ordinate nNet_CreateOrdinateW2(Tensor tensor, int *begin, int bsize,
                                              int *end, int esize);
Ordinate nNet_CreateOrdinateC1(Interpreter interpreter, Tensor tensor, 
                               int size, ...);
Ordinate nNet_CreateOrdinateC2(Interpreter interpreter, Tensor tensor, 
                               int *ordinate, int size);
Ordinate nNet_CreateOrdinateC3(Interpreter interpreter, Tensor tensor, 
                               int *begin, int bsize, int *end, int esize);

Ordinate nNet_RemoveOrdinate(Ordinate ordinate);
Ordinate nNet_RemoveOrdinateW(Interpreter interpreter, Ordinate ordinate);

Ordinate nNet_RewriteOrdinateS(Ordinate ordinate, Tensor tensor, int size, ...);
Ordinate nNet_RewriteOrdinateW1(Ordinate ordinate, Tensor tensor, int size,
                                int *dims);
Ordinate nNet_RewriteOrdinateW2(Ordinate ordinate, Tensor tensor,
                                int *begin, int bsize, int *end, int esize);

/* @NOTE: convenient api-functions to calculate Tensor's index */
Index nNet_OrdinateToIndexW(Interpreter interpreter, Tensor tensor, Ordinate ordinate);
Index nNet_OrdinateToIndex(Tensor tensor, Ordinate ordinate);
Index nNet_CalcIndex(Tensor tensor, int i);

// Ordinate nNet_IndexToOrdinate(Tensor tensor, int index, Ordinate ordinate);

/* @NOTE: C-API to communicate wit Tensor object, tensor is the basic object of
 * libnn and it will be use to calculate, store and transfer between real nodes
 */

// @NOTE: constructor and destructor of Tensor object
Tensor nNet_CreateTensorS();
Tensor nNet_CreateTensorW(Shape shape);
Tensor nNet_CreateTensorC(Interpreter interpreter, Shape shape);

Tensor nNet_RemoveTensor(Tensor tensor);
Tensor nNet_RemoveTensorW(Interpreter interpreter, Tensor tensor);

// @NOTE: tensor utilities
Tensor nNet_ClearTensor(Tensor tensor);
Tensor nNet_ClearTensorW(Interpreter interpreter, Tensor tensor);

Tensor nNet_ReshapeTensorS(Tensor tensor, Shape shape);
Tensor nNet_ReshapeTensorW(Tensor tensor, Tensor source);
Tensor nNet_ReshapeTensorC1(Interpreter interpreter, Tensor tensor, Shape shape);
Tensor nNet_ReshapeTensorC2(Interpreter interpreter, Tensor tensor, Tensor source);

// @NOTE: getter and setter of Tensor object
CString nNet_GetTensorTypeName(Tensor tensor);
CString nNet_GetTensorTypeNameW(Interpreter interpreter, Tensor tensor);

int nNet_GetTensorType(Tensor tensor);
int nNet_GetTensorTypeW(Interpreter interpreter, Tensor tensor);

int  nNet_GetTensorNumT(Tensor tensor);
long nNet_GetTensorSize(Tensor tensor);
bool nNet_GetTensorShape(Tensor tensor, Shape shape);
bool nNet_GetTensorShapeW(Interpreter interpreter, Tensor tensor, Shape shape);
bool nNet_GetSubTensor(Tensor tensor, Ordinate ordinate, Tensor result);
bool nNet_GetSubTensorW(Interpreter interpreter, Tensor tensor, Ordinate ordinate,
                        Tensor result);

Device nNet_GetDeviceOfTensor(Tensor tensor);
Device nNet_GetDeviceOfTensorW(Interpreter interpreter, Tensor tensor);

bool nNet_SetTensorNumT(Tensor tensor, int type);
bool nNet_SetTensorShape(Tensor tensor, Shape shape);
bool nNet_SetSubTensor(Tensor tensor, Ordinate ordinate, Tensor result);

bool nNet_SetTensorNumTW(Interpreter interpreter, Tensor tensor, int type);
bool nNet_SetTensorShapeW(Interpreter interpreter, Tensor tensor, Shape shape);
bool nNet_SetSubTensorW(Interpreter interpreter, Tensor tensor, 
                        Ordinate ordinate, Tensor result);

// @NOTE: create a vectorize-array
Array nNet_AllocateArray(long size, int type);

// @NOTE: check Tensor status
bool nNet_IsRefTensor(Tensor tensor);

// @NOTE: manipulate directly on to tensor memory
long nNet_GetArraySizeTC(Interpreter interpreter, Tensor tensor);
long nNet_GetArraySizeSC(Interpreter interpreter, Shape shape);
long nNet_GetArraySizeT(Tensor tensor);
long nNet_GetArraySizeS(Shape  shape);

Item nNet_GetTensorItemS(Tensor tensor, Index index);
bool nNet_SetTensorItemS(Tensor tensor, Index index, Item item);
void nNet_TryDropingItem(Tensor tensor, Item item, Index index);

Item nNet_GetTensorItemW(Tensor tensor, Ordinate ordinate);
bool nNet_SetTensorItemW(Tensor tensor, Ordinate ordinate, Item item);
bool nNet_ConvertNumT(Item src, int stype, Item dst, int dtype);
#if __cplusplus
}
#endif

#if __cplusplus
#include <algorithm>    // std::reverse
#include <vector>

#ifdef LIBCGRAPH_GIT_REFSPEC
#include <libbase/all.hpp>
#else
#include "logcat.hpp"
#endif

namespace nn{
namespace utils{
int type(const std::type_info& info);
} // namespace utils

namespace tensor{
/* @NOTE: generic tensor's utilities */
Tensor ones(std::vector<int> dims, int type, Interpreter interpreter = nullptr);
Tensor zeros(std::vector<int> dims, int type, Interpreter interpreter = nullptr);

/* @NOTE: matrix's utilities */
Tensor ones(int type, int num_rows = -1, int num_columns = -1);
Tensor zeros(int type, int num_rows = -1, int num_columns = -1);
Tensor eye(int type, int num_rows = -1, int num_columns = -1);

template<typename Output, typename Input>
Tensor tensorWithType(std::vector<Input> data, Interpreter interpreter = nullptr){
  auto shape = data.size() > 1? nNet_CreateTensorShapeC1(interpreter, 1, data.size())
                              : nullptr;
  auto result  = nNet_CreateTensorC(interpreter, shape);
  auto success = true;

  int type;

  if (data.size() == 0) return nNet_RemoveTensorW(interpreter, result);
  if (nNet_RemoveTensorShapeW(interpreter, shape))
    return nNet_RemoveTensorW(interpreter, result);

  if (!(type = utils::type(typeid(Output))))
    return nNet_RemoveTensorW(interpreter, result);

  if (!nNet_SetTensorNumTW(interpreter, result, type))
    return nNet_RemoveTensorW(interpreter, result);

  for (auto i = 0; i < cast_(i, data.size()); ++i){
    Output src = data[i];

    if (!(success = nNet_SetTensorItemS(result, i, &src)))
      break;
  }

  return (success)? result: nNet_RemoveTensorW(interpreter, result);
}

template<typename Output, typename Input>
Tensor tensorWithType(std::vector<Input> data, std::vector<int> shape,
              Interpreter interpreter = nullptr){
  std::reverse(shape.begin(), shape.end());
  if (shape.size() > 0){
    auto tshape = nNet_CreateTensorShapeC2(interpreter, shape.size(), 
                                           shape.data());
    if (tshape){
      auto size = nNet_GetShapeSize(tshape);

      if (cast_(size, data.size()) < size){
        for (auto i = data.size(); i < cast_(i, size); ++i){
          data.push_back(Input{0});
        }
      } else if (cast_(size, data.size()) > size){
          data.resize(size);
      }

      return nNet_ReshapeTensorC1(interpreter, 
                                  tensorWithType<Output, Input>(data, interpreter),
                                  tshape);
    } else
        return nullptr;
  }

  return nullptr;
}

template<typename Output, typename Input>
Tensor tensorWithType(std::vector<Input> data, int dtype,
              Interpreter interpreter = nullptr){
  return nNet_SetTensorNumTW(interpreter, 
                             tensorWithType<Output, Input>(data, interpreter),
                             dtype);
}

template<typename Output, typename Input>
Tensor tensorWithType(std::vector<Input> data, int dtype, std::vector<int> shape,
              Interpreter interpreter = nullptr){
  return nNet_SetTensorNumTW(interpreter,
                             tensorWithType<Output, Input>(rvalue(data), shape, interpreter),
                             dtype);
}

template<typename Type>
Tensor tensor(std::vector<Type>&& data, Interpreter interpreter = nullptr){
  auto shape = data.size() > 1? nNet_CreateTensorShapeC1(interpreter, 1, data.size())
                              : nullptr;
  auto result  = nNet_CreateTensorC(interpreter, shape);
  auto success = true;

  int type;

  if (data.size() == 0) return nNet_RemoveTensorW(interpreter, result);
  if (nNet_RemoveTensorShapeW(interpreter, shape))
    return nNet_RemoveTensorW(interpreter, result);

  if (!(type = utils::type(typeid(Type))))
    return nNet_RemoveTensorW(interpreter, result);

  if (!nNet_SetTensorNumTW(interpreter, result, type))
    return nNet_RemoveTensorW(interpreter, result);

  for (auto i = 0; i < cast_(i, data.size()); ++i){
    Type src = data[i];

    if (!(success = nNet_SetTensorItemS(result, i, &src)))
      break;
  }

  return (success)? result: nNet_RemoveTensorW(interpreter, result);
}

template<typename Type>
Tensor tensor(std::vector<Type>&& data, std::vector<int> shape,
              Interpreter interpreter = nullptr){
  std::reverse(shape.begin(), shape.end());
  if (shape.size() > 0){
    auto tshape = nNet_CreateTensorShapeC2(interpreter, shape.size(), 
                                           shape.data());
    if (tshape){
      auto size = nNet_GetShapeSize(tshape);

      if (cast_(size, data.size()) < size){
        for (auto i = data.size(); i < cast_(i, size); ++i){
          data.push_back(Type{0});
        }
      } else if (cast_(size, data.size()) > size){
        data.resize(size);
      }

      return nNet_ReshapeTensorC1(interpreter, 
                                  tensor<Type>(rvalue(data), interpreter),
                                  tshape);
    } else return nullptr;
  }

  return nullptr;
}

template<typename Type>
Tensor tensor(std::vector<Type>&& data, int dtype,
              Interpreter interpreter = nullptr){
  return nNet_SetTensorNumTW(interpreter, 
                             tensor<Type>(rvalue(data), interpreter),
                             dtype);
}

template<typename Type>
Tensor tensor(std::vector<Type>&& data, int dtype, std::vector<int> shape,
              Interpreter interpreter = nullptr){
  return nNet_SetTensorNumTW(interpreter,
                             tensor<Type>(rvalue(data), shape, interpreter),
                             dtype);
}

inline int dimemsion(Tensor tensor, std::size_t index){
  if (!tensor) return -1;
  if (tensor->ndims == 0) return index? -1: 0;
  if (tensor->ndims >= cast_(tensor->ndims, index) && tensor->dims)
    return tensor->dims[tensor->ndims - index - 1];
  return -1;
}

template<typename Type>
Type scalar(Tensor tensor){
  Type result;

  if (nNet_GetTensorType(tensor) != ScalarT){
    throw (NoSupport << "expect \'tensor\' be a scalar,"
                     << "but your tensor's type is" 
                     << nNet_GetTensorTypeName(tensor));
  } else {
    auto item    = nNet_GetTensorItemS(tensor, 0);
    auto success = nNet_ConvertNumT(item, nNet_GetTensorNumT(tensor),
                                    &result, utils::type(typeid(Type)));

    nNet_TryDropingItem(tensor, item, 0);
    if (!success){
      throw BadAccess.reason("nNet_ConvertNumT()");
    }
  }
  return result;
}
} // namespace tensor
} // namespace nn
#endif
#endif // LIBNN_TENSOR_H_
