from tensorflow.contrib.framework.python.ops import audio_ops as contrib_audio
import tensorflow as tf
import pandas as pd
import numpy as np
import argparse
import glob

def export_csv_with_google_format(flags, fingerprint_input, wav_data_placeholder):
	with tf.Session() as sess:
		dt = None
		labels = set()
		mapping = dict()

		for file in glob.glob(flags.input):
			labels.add(file.split('/')[-2])

		for idx, value in enumerate(labels):
			mapping[value] = idx

		for file in glob.glob(flags.input):
			with open(file, 'rb') as rd:
				mfcc = sess.run(fingerprint_input, feed_dict={wav_data_placeholder: rd.read()})
			if dt is None:
				dt = pd.DataFrame(np.concatenate(([mapping[file.split('/')[-2]]], mfcc.reshape(mfcc.size)))).T
			else:
				dt.loc[len(dt)] = np.concatenate(([mapping[file.split('/')[-2]]], mfcc.reshape(mfcc.size)))
		dt.to_csv(flags.csv_output, index=False)

def main(flags):
	wav_data_placeholder = tf.placeholder(tf.string, [], name='wav_data')
	decoded_sample_data = contrib_audio.decode_wav(wav_data_placeholder,
		desired_channels=1,
		desired_samples=flags.desired_samples)
	spectrogram = contrib_audio.audio_spectrogram(decoded_sample_data.audio,
		window_size=flags.window_size_samples,
		stride=flags.window_stride_samples,
		magnitude_squared=True)

	if flags.preprocess == 'average':
		fingerprint_input = tf.nn.pool(tf.expand_dims(spectrogram, -1),
			window_shape=[1, flags.average_window_width],
			strides=[1, flags.average_window_width],
			pooling_type='AVG',
			padding='SAME')
	elif flags.preprocess == 'mfcc':
		fingerprint_input = contrib_audio.mfcc(spectrogram, decoded_sample_data.sample_rate,
			dct_coefficient_count=flags.dct_coefficient_count)
	else:
		raise Exception('Unknown preprocess mode "%s" (should be "mfcc" or'
			' "average")' % (preprocess))
	if flags.input_type.lower() == 'google':
		export_csv_with_google_format(flags, fingerprint_input, wav_data_placeholder)
	else:
		raise NotImplementedError('type %s is underdeveloping' % flags.input_type)

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--input', type=str, required=True)
	parser.add_argument('--input_type', type=str, required=True)
	parser.add_argument('--csv_output', type=str, required=True)
	parser.add_argument('--average_window_width', type=int, required=True)
	parser.add_argument('--preprocess', type=str, default='mfcc')
	parser.add_argument('--window_stride_samples', type=int, default=160)
	parser.add_argument('--window_size_samples', type=int, default=480)
	parser.add_argument('--dct_coefficient_count', type=int, default=40)
	parser.add_argument('--desired_samples', type=int, default=16000)
	flags, _ = parser.parse_known_args()

	main(flags)