import scipy.io.wavfile as wav
import numpy as np
import matplotlib.pyplot as plt
import argparse
import shutil
import glob
import math
import re
import os

RIGHT_MONITOR_THRESHOLD = 0.70
LEFT_MONITOR_THRESHOLD  = 0.75

ALPHA = 0.05

def eval_rms(window_of_signal, ratio):
	sum_up = 0.0

	for value in window_of_signal:
		sum_up += math.pow(value/ratio, 2)

	return math.sqrt(sum_up)

def eval_peak_db(rms):
	if rms > 0:
		return 20 * math.log10(rms / 0.1)
	else:
		return 0.0

def eval_low_pass_of_peak_db(peak_db, low_pass):
	peak_power = math.pow(10.0, ALPHA *peak_db)
	return ALPHA * peak_power + (1.0 - ALPHA) * low_pass

def remove_left_silence_segments(signal, rate, window_size):
	index    = 0
	maximum  = None
	blocks   = [[]]
	low_pass = 0.0

	for i in range(0, int(round(len(signal)/window_size))):
		begin = i*window_size
		end   = begin + window_size

		rms      = eval_rms(signal[int(begin): int(end)], math.pow(2, 15))
		peak     = eval_peak_db(rms)
		low_pass = eval_low_pass_of_peak_db(peak, low_pass)

		if low_pass >= LEFT_MONITOR_THRESHOLD:
			blocks[index].extend(signal[int(begin): int(end)])
		elif len(blocks[index]) > 0:
			blocks.append([])
			index += 1

	for (i, block) in enumerate(blocks):
		if maximum is None:
			maximum = block
			index   = i
		elif len(block) > len(maximum):
			maximum = block
			index   = i
	result = []

	for i in range(index, len(blocks)):
		result.extend(blocks[i])
	return result

def remove_right_silence_segments(signal, rate, window_size):
	result   = []
	low_pass = 0.0

	for i in range(0, int(round(len(signal)/window_size))):
		begin = i*window_size
		end   = begin + window_size

		rms      = eval_rms(signal[int(begin): int(end)], math.pow(2, 15))
		peak     = eval_peak_db(rms)
		low_pass = eval_low_pass_of_peak_db(peak, low_pass)

		if low_pass >= RIGHT_MONITOR_THRESHOLD:
			result.extend(signal[int(begin): int(end)])
	return result

def remove_silence_segments(signal, rate, window_size):
	signal = remove_left_silence_segments(signal, rate, window_size)
	return remove_right_silence_segments(signal, rate, window_size)

def get_index_of_google(index_mix_with_name):
	end_of_hash = index_mix_with_name.find('_nohash_')
	end_of_name = index_mix_with_name.find('.')
	if end_of_hash >= len(index_mix_with_name):
		return index_mix_with_name
	else:
		return index_mix_with_name[0:end_of_hash] + index_mix_with_name[end_of_hash + len('_nohash_'):end_of_name]

def separate_index_and_name_of_azure(index_mix_with_name):
	for idx, c in enumerate(index_mix_with_name):
		if '9' < c or c < '0':
			continue
		else:
			return index_mix_with_name[0: idx]
	return index_mix_with_name
	

def build_output_with_google(output_pattern, path, format):
	splited = output_pattern.split('/')

	if splited[-1].split('.')[0] != '%s%s':
		return None
	elif splited[-2] != '%s':
		return None
	elif format == 'google':
		name, index_mix_with_name = path.split('/')[-2:]
		index = get_index_of_google(index_mix_with_name)
	elif format == 'azure':
		index_mix_with_name = path.split('/')[-1]
		name, index = separate_index_and_name_of_azure(index_mix_with_name)

	name = re.sub("[^a-zA-Z \']", "", name)

	standarlized = name.replace(' ', '_')
	name = name.replace('\'', ' ')
	root = '/'.join(splited[:-2])
	
	if not os.path.exists(root + '/' + name):
		os.mkdir(root + '/' + name)

	standarlized = standarlized.replace('\'', '_')
	return output_pattern % (name, standarlized, index)

def build_output_with_azure(output_pattern, path, format):
	splited = output_pattern.split('/')

	if splited[-1].split('.')[0] != '%s%s':
		return None
	elif format == 'google':
		name, index_mix_with_name = path.split('/')[-2:]
		index = get_index_of_google(index_mix_with_name)
	elif format == 'azure':
		index_mix_with_name = path.split('/')[-1]
		name, index = separate_index_and_name_of_azure(index_mix_with_name)

	name = re.sub("[^a-zA-Z \']", "", name)
	standarlized = name.replace(' ', '_')
	name = name.replace('\'', ' ')
	standarlized = standarlized.replace('\'', '_')

	with open('/'.join(splited[:-2]) + '/transcript.txt', 'a') as fd:
		ext = output_pattern.split('.')[-1]
		fd.write("%s%s.%s %s\n" % (standarlized, index, ext, name))
	return output_pattern % (standarlized, index)

def main(flags):
	inputs = flags.input

	if flags.output_type.lower() == 'google':
		output = build_output_with_google
	elif flags.output_type.lower() == 'azure':
		output = build_output_with_azure
	else:
		exit(-1)

	if flags.input.split('/')[-1].split('.')[0] == '*':
		if flags.input.split('/')[-2] == '*':
			format = 'google'
		else:
			format = 'azure'
	else:
		exit(-1)

	if flags.use_removing_silence_segments:
		window_size_by_mili = 1.0
		for filename in glob.glob(inputs):
			rate, signal = wav.read(filename)
			window_size  = window_size_by_mili*rate/1000.0
			signal = remove_silence_segments(signal, rate, window_size)

			if len(signal) > 0:
				wav.write(output(flags.output, filename, format), rate, np.asarray(signal))
	else:
		for filename in glob.glob(inputs):			
			shutil.copy(filename, output(flags.output, filename, format))


if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--use_removing_silence_segments', type=bool, default=False)
	parser.add_argument('--output_type', type=str, required=True)
   	parser.add_argument('--output', type=str, required=True)
   	parser.add_argument('--input', type=str, required=True)
	flags, _ = parser.parse_known_args()

	main(flags)