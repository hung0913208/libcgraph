unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
 	MINGW*)     machine=MinGw;;
    *)          machine="UNKNOWN:${unameOut}"
esac

if [ "$machine" == "Mac" ]; then
	SOURCES=$1
	shift

	if [ -d "$SOURCES" ]; then
		for path in $SOURCES/*.metal; do
			/usr/bin/xcrun $* $path
		done
	elif [ -f "$SOURCES" ]; then
		/usr/bin/xcrun $* $SOURCES
	else
		exit -1
	fi
fi