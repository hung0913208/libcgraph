#!/usr/bin/env bash

wget -O /usr/local/bin/gitlab-runner https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64
chmod +x /usr/local/bin/gitlab-runner

if [[ $2 == 'docker' ]]; then
	apt install docker.io
	systemctl start docker

	useradd --comment 'GitLab Runner' --create-home gitlab-runner --shell /bin/bash
	gitlab-runner install --user=gitlab-runner --working-directory=/home/gitlab-runner
	gpasswd -a gitlab-runner docker

	gitlab-runner start
	gitlab-runner register \
		--non-interactive \
		--url "https://gitlab.com/" \
		--registration-token "$1" \
		--executor "docker" \
		--docker-image "ubuntu" \
		--tag-list "docker" \
		--run-untagged \
		--locked="false"
elif [[ $2 == 'shell' ]]; then
	gitlab-runner install --user=root --working-directory=/root
	gitlab-runner start
	gitlab-runner register \
		--non-interactive \
		--url "https://gitlab.com/" \
		--registration-token "$1" \
		--tag-list "$3" \
		--executor "shell"
fi

gitlab-runner restart