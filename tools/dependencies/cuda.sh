#!/usr/bin/env bash

apt-get install pciutils
lspci | grep 'NVIDIA Corporation' &> /dev/null
if [ $? == 0 ]; then
	wget -O cuda-repo-ubuntu1604_9.2.88-1_amd64.deb https://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/cuda-repo-ubuntu1604_9.2.88-1_amd64.deb
	apt-get install -y ./cuda-repo-ubuntu1604_9.2.88-1_amd64.deb
	apt-get install -y dirmngr
	apt-key adv --keyserver keyserver.ubuntu.com --recv-keys F60F4B3D7FA2AF80
	apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1604/x86_64/7fa2af80.pub!apt-get update
	apt-get update
	apt-get install -y cuda
fi