if [ ! -f ./eigen ]; then
    hg clone https://bitbucket.org/eigen/eigen/
    mkdir -p eigen/build
    cd eigen/build
    cmake .. && make && make install
fi
cd $1