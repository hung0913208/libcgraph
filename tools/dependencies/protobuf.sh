#!/bin/bash
# Copyright 2015 The TensorFlow Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
# ==============================================================================
# Builds protobuf 3 for iOS.

unameOut="$(uname -s)"
case "${unameOut}" in
    Linux*)     machine=Linux;;
    Darwin*)    machine=Mac;;
    CYGWIN*)    machine=Cygwin;;
    MINGW*)     machine=MinGw;;
    FreeBSD*)   machine=BSD;;
    *)          machine="UNKNOWN:${unameOut}"
esac

package_pb_library() {
    pb_libs="${LIBDIR}/${1}/lib/libprotobuf.a"
    if [ -f "${LIBDIR}/libprotobuf.a" ]; then
        pb_libs="$pb_libs ${LIBDIR}/libprotobuf.a"
    fi
    lipo \
    $pb_libs \
    -create \
    -output ${LIBDIR}/libprotobuf.a

    pblite_libs="${LIBDIR}/${1}/lib/libprotobuf-lite.a"
    if [ -f "${LIBDIR}/libprotobuf-lite.a" ]; then
        pblite_libs="$pblite_libs ${LIBDIR}/libprotobuf-lite.a"
    fi
    lipo \
    $pblite_libs \
    -create \
    -output ${LIBDIR}/libprotobuf-lite.a
}

make_host_protoc() {
  if [[ ! $1 ]]; then
    echo "needs 1 argument (HOST_GENDIR)"
    exit 1
  fi
  HOST_GENDIR="$1"

  rm -rf "${HOST_GENDIR}"
  mkdir -p "${HOST_GENDIR}"

  autoreconf -i
  ./configure --disable-shared --prefix="${HOST_GENDIR}"
  make clean
  make -j"$(get_job_count)"
  if [ $? -ne 0 ]; then
    echo "make failed"
    exit 1
  fi
  make install
}

BUILD_TARGETS=("i386")
if [ "$machine" == "Mac" ] || [ "$machine" == "BSD" ]; then
    JOB_COUNT=$(sysctl hw.ncpu | awk '{print $2}')
elif [ "$machine" == "Linux" ]; then
    JOB_COUNT=$(grep processor /proc/cpuinfo | wc -l)
fi

if [ ! -d ./protobuf ]; then
    git clone https://github.com/google/protobuf.git
fi

if [ "$machine" == "Mac" ]; then
    LIBDIR="$(pwd)/protobuf-build"
    HOST_GENDIR="$(pwd)/protobuf-host"

    if [ -d "${LIBDIR}" ]; then
        rm -f "${LIBDIR}"/libprotobuf.a
        rm -f "${LIBDIR}"/libprotobuf-lite.a
    fi
fi

cd protobuf || exit -1

if [ $# == 1 ]; then
    git checkout tags/"$1"
elif [ $# -gt 1 ]; then
    git checkout tags/"$1" -b "$2"
fi

if [ "$machine" == 'Mac' ]; then
    brew install automake
    brew install autoconf
elif [ "$machine" == 'Linux' ]; then
    sudo apt install -y autoconf libtool make
fi

./autogen.sh
if [ $? -ne 0 ]
then
  echo "./autogen.sh command failed."
  exit -1
fi

if [ "$machine" == 'Linux' ] || [ "$machine" == "BSD" ]; then
    ./configure
else
    # IPHONEOS_PLATFORM=$(xcrun --sdk iphoneos --show-sdk-platform-path)
    # IPHONESIMULATOR_PLATFORM=$(xcrun --sdk iphonesimulator --show-sdk-platform-path)
    # IOS_SDK_VERSION=$(xcrun --sdk iphoneos --show-sdk-version)

    IPHONESIMULATOR_SYSROOT=$(xcrun --sdk iphonesimulator --show-sdk-path)
    IPHONEOS_SYSROOT=$(xcrun --sdk iphoneos --show-sdk-path)
    MIN_SDK_VERSION=8.0

    mkdir -p "${LIBDIR}"
    mkdir -p "${HOST_GENDIR}"

    OSX_VERSION=darwin14.0.0

    CFLAGS="-DNDEBUG -Os -pipe -fPIC -fno-exceptions"
    CXXFLAGS="${CFLAGS} -std=c++11 -stdlib=libc++"
    LDFLAGS="-stdlib=libc++"
    LIBS="-lc++ -lc++abi"

    PROTOC_PATH="${HOST_GENDIR}/bin/protoc"

    if [ ! -d "${PROTOC_PATH}" ]; then
        make_host_protoc "${HOST_GENDIR}"
    fi

    for TARGET in "${BUILD_TARGETS[@]}";do
        case "$TARGET" in
        i386)   make distclean
                ./configure \
                --host=i386-apple-${OSX_VERSION} \
                --disable-shared \
                --enable-cross-compile \
                --with-protoc="${PROTOC_PATH}" \
                --prefix=${LIBDIR}/iossim_386 \
                --exec-prefix=${LIBDIR}/iossim_386 \
                "CFLAGS=${CFLAGS} \
                -mios-simulator-version-min=${MIN_SDK_VERSION} \
                -arch i386 \
                -fembed-bitcode \
                -isysroot ${IPHONESIMULATOR_SYSROOT}" \
                "CXX=${CXX}" \
                "CXXFLAGS=${CXXFLAGS} \
                -mios-simulator-version-min=${MIN_SDK_VERSION} \
                -arch i386 \
                -fembed-bitcode \
                -isysroot \
                ${IPHONESIMULATOR_SYSROOT}" \
                LDFLAGS="-arch i386 \
                -fembed-bitcode \
                -mios-simulator-version-min=${MIN_SDK_VERSION} \
                ${LDFLAGS} \
                -L${IPHONESIMULATOR_SYSROOT}/usr/lib/ \
                -L${IPHONESIMULATOR_SYSROOT}/usr/lib/system" \
                "LIBS=${LIBS}"
        ;;

        x86_64) make distclean
                ./configure \
                --host=x86_64-apple-${OSX_VERSION} \
                --disable-shared \
                --enable-cross-compile \
                --with-protoc="${PROTOC_PATH}" \
                --prefix=${LIBDIR}/iossim_x86_64 \
                --exec-prefix=${LIBDIR}/iossim_x86_64 \
                "CFLAGS=${CFLAGS} \
                -mios-simulator-version-min=${MIN_SDK_VERSION} \
                -arch x86_64 \
                -fembed-bitcode \
                -isysroot ${IPHONESIMULATOR_SYSROOT}" \
                "CXX=${CXX}" \
                "CXXFLAGS=${CXXFLAGS} \
                -mios-simulator-version-min=${MIN_SDK_VERSION} \
                -arch x86_64 \
                -fembed-bitcode \
                -isysroot \
                ${IPHONESIMULATOR_SYSROOT}" \
                LDFLAGS="-arch x86_64 \
                -fembed-bitcode \
                -mios-simulator-version-min=${MIN_SDK_VERSION} \
                ${LDFLAGS} \
                -L${IPHONESIMULATOR_SYSROOT}/usr/lib/ \
                -L${IPHONESIMULATOR_SYSROOT}/usr/lib/system" \
                "LIBS=${LIBS}"
        ;;

        armv7)  make distclean
                ./configure \
                --host=armv7-apple-${OSX_VERSION} \
                --with-protoc="${PROTOC_PATH}" \
                --disable-shared \
                --prefix=${LIBDIR}/ios_arm7 \
                --exec-prefix=${LIBDIR}/ios_arm7 \
                "CFLAGS=${CFLAGS} \
                -miphoneos-version-min=${MIN_SDK_VERSION} \
                -arch armv7 \
                -fembed-bitcode \
                -isysroot ${IPHONEOS_SYSROOT}" \
                "CXX=${CXX}" \
                "CXXFLAGS=${CXXFLAGS} \
                -miphoneos-version-min=${MIN_SDK_VERSION} \
                -arch armv7 \
                -fembed-bitcode \
                -isysroot ${IPHONEOS_SYSROOT}" \
                LDFLAGS="-arch armv7 \
                -fembed-bitcode \
                -miphoneos-version-min=${MIN_SDK_VERSION} \
                ${LDFLAGS}" \
                "LIBS=${LIBS}"
        ;;

        armv7s) make distclean
                ./configure \
                --host=armv7s-apple-${OSX_VERSION} \
                --with-protoc="${PROTOC_PATH}" \
                --disable-shared \
                --prefix=${LIBDIR}/ios_arm7s \
                --exec-prefix=${LIBDIR}/ios_arm7s \
                "CFLAGS=${CFLAGS} \
                -miphoneos-version-min=${MIN_SDK_VERSION} \
                -arch armv7s \
                -fembed-bitcode \
                -isysroot ${IPHONEOS_SYSROOT}" \
                "CXX=${CXX}" \
                "CXXFLAGS=${CXXFLAGS} \
                -miphoneos-version-min=${MIN_SDK_VERSION} \
                -arch armv7s \
                -fembed-bitcode \
                -isysroot ${IPHONEOS_SYSROOT}" \
                LDFLAGS="-arch armv7s \
                -fembed-bitcode \
                -miphoneos-version-min=${MIN_SDK_VERSION} \
                ${LDFLAGS}" \
                "LIBS=${LIBS}"
        ;;

        arm64)  make distclean
                ./configure \
                --host=arm \
                --with-protoc="${PROTOC_PATH}" \
                --disable-shared \
                --prefix=${LIBDIR}/ios_arm64 \
                --exec-prefix=${LIBDIR}/ios_arm64 \
                "CFLAGS=${CFLAGS} \
                -miphoneos-version-min=${MIN_SDK_VERSION} \
                -arch arm64 \
                -fembed-bitcode \
                -isysroot ${IPHONEOS_SYSROOT}" \
                "CXXFLAGS=${CXXFLAGS} \
                -miphoneos-version-min=${MIN_SDK_VERSION} \
                -arch arm64 \
                -fembed-bitcode \
                -isysroot ${IPHONEOS_SYSROOT}" \
                LDFLAGS="-arch arm64 \
                -fembed-bitcode \
                -miphoneos-version-min=${MIN_SDK_VERSION} \
                ${LDFLAGS}" \
                "LIBS=${LIBS}"
        ;;
        *)
        esac

        make -j"${JOB_COUNT}"
        make install

        case "$TARGET" in
        i386)   package_pb_library "iossim_386"
                tree "${LIBDIR}/iossim_386"
        ;;

        x86_64) package_pb_library "iossim_x86_64"
                tree "${LIBDIR}/iossim_x86_64"
        ;;

        armv7)  package_pb_library "ios_arm7"
                tree "${LIBDIR}/ios_arm7"
        ;;

        armv7s) package_pb_library "ios_arm7s"
                tree "${LIBDIR}/ios_arm7s"
        ;;

        arm64)  package_pb_library "ios_arm64"
                tree "${LIBDIR}/ios_arm64"
        ;;
        *)
        esac


    done
fi

make && make install
if [ "$machine" == 'Linux' ]; then
    ldconfig
fi
cd ../ || exit -1