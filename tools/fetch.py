from progress.bar import Bar

import requests
import argparse
import tarfile
import zipfile
import gzip
import shutil
import os.path

def download_from_link_to_file(link, file, chunk_size=1024):
	if not os.path.isfile(file):
		r = requests.get(link, stream=True)

		if r.status_code == 200:
			if os.path.exists(file) is True:
				shutil.rmtree(file)

			total_size = int(r.headers.get('content-length', 0))
			bar = Bar('Downloading %s:' % file, max=total_size/chunk_size)

			with open(file, 'wb') as fd:
				for chunk in r.iter_content(chunk_size=chunk_size):
					fd.write(chunk)
					bar.next()
				bar.finish()

def parse_format_from_link(link):
	splited = link.split('/')[-1].split('.')
  
	if len(splited) >= 2:
		if splited[1] == 'csv':
			return splited[0]
		else:
			return splited[1]
	else:
		return None

def extract_compressed_dataset(output, file, tool):
	if file is not None:
		for folder in file.split('/')[:-1]:
			output = output + '/' + folder
			if os.path.exists(output) is False:
				os.mkdir(output)

		if tool == 'tar':
			tar_fd = tarfile.open(file, 'r:' + file.split('.')[-1])
			tar_fd.extractall(path=output)
			tar_fd.close()
		elif tool == 'zip':
			zip_fd = zipfile.ZipFile(file, 'r')
			zip_fd.extractall(output)
			zip_fd.close()
		elif tool == 'gz':
			output_file = '.'.join(file.split('/')[-1].split('.')[:-1])

			with gzip.open(file, 'rb') as rd:
				with open(output + '/' + output_file, 'wb') as wd:
					wd.write(rd.read())
		else:
			dataset = output + '/' + file.split('/')[-1]
			shutil.copy(file, dataset)
			print("cannot extract dataset %s, please do it manualy" % dataset)

def download_from_opensource_dataset(name):
	datasets = {
		'libri_speech': {
			'TRAIN_CLEAN_100_URL': {
				'url': 'http://www.openslr.org/resources/12/train-clean-100.tar.gz',
				'type': 'train'
			},
			'TRAIN_CLEAN_360_URL': {
				'url': 'http://www.openslr.org/resources/12/train-clean-360.tar.gz',
				'type': 'train'
			},
			'TRAIN_OTHER_500_URL': {
				'url': 'http://www.openslr.org/resources/12/train-other-500.tar.gz',
				'type': 'train'
			},
			'DEV_CLEAN_URL': {
				'url': 'http://www.openslr.org/resources/12/dev-clean.tar.gz',
				'type': None
			},
			'DEV_OTHER_URL': {
				'url': 'http://www.openslr.org/resources/12/dev-other.tar.gz',
				'type': None
			},
			'TEST_CLEAN_URL': {
				'url': 'http://www.openslr.org/resources/12/test-clean.tar.gz',
				'type': 'test'
			},
			'TEST_OTHER_URL': {
				'url': 'http://www.openslr.org/resources/12/test-other.tar.gz',
				'type': 'test'
			}
		}
	}

	if name in datasets:
		links = datasets[name]

		if isinstance(links, str):
			file = links.split('/')[-1]
			download_from_link_to_file(links, file)

			yield file, parse_format_from_link(links)
		elif isinstance(links, dict):
			for name in links:
				group = links[name]

				if isinstance(group, dict):
					path = ''

					if 'type' in group and (not group['type'] is None):
						path = group['type'] + '/'
						if os.path.exists(path) is False:
							os.mkdir(path)

					if 'url' in group:
						link = group['url']
						file = path + link.split('/')[-1]

						download_from_link_to_file(link, file)
						yield (file, parse_format_from_link(link))
				elif isinstance(group, list) or isinstance(group, tuple):
					for link in group:
						file = links.split('/')[-1]

						download_from_link_to_file(link, file)
						yield file, parse_format_from_link(link)
				elif isinstance(group, str):
					file = group.split('/')[-1]

					download_from_link_to_file(group, file)
					yield file, parse_format_from_link(group)
		elif isinstance(links, tuple) or isinstance(links, list):
			for link in links:
				file = links.split('/')[-1]

				download_from_link_to_file(link, file)
				yield file, parse_format_from_link(link)

def download_from_google_dataset(name):
	datasets = {
		'speech_commands': 'http://download.tensorflow.org/data/speech_commands_v0.02.tar.gz',
		'HIGGS': [
			'https://archive.ics.uci.edu/ml/machine-learning-databases/00280/HIGGS.csv.gz',
			'https://archive.ics.uci.edu/ml/machine-learning-databases/00280/HIGGS.csv.gz.npz'
		]
	}

	if name in datasets:
		links = datasets[name]

		if isinstance(links, str):
			file = links.split('/')[-1]
			download_from_link_to_file(links, file)

			yield (file, parse_format_from_link(links))
		elif isinstance(links, tuple) or isinstance(links, list):
			for link in links:
				file = link.split('/')[-1]
				download_from_link_to_file(link, file)

				yield (file, parse_format_from_link(link))

def download_from_kaggle(name):
	pass

def download_from_froyhub(name):
	pass

if __name__ == '__main__':
	parser = argparse.ArgumentParser()
	parser.add_argument('--output', type=str, default='.')
	parser.add_argument('--name', type=str, required=True)
	parser.add_argument('--source', type=str, required=True)
	flags, _ = parser.parse_known_args()

	if flags.source == 'google':
		for file, tool in download_from_google_dataset(flags.name):
			extract_compressed_dataset(flags.output, file, tool)
	elif flags.source == 'opensource':
		for file, tool in download_from_opensource_dataset(flags.name):
			print(file, tool)
			extract_compressed_dataset(flags.output, file, tool)
	elif flags.source == 'kaggle':
		pass
