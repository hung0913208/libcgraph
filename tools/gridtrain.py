import pandas as pd
import numpy as np
import argparse
from sklearn.metrics import confusion_matrix, classification_report, accuracy_score
from sklearn.model_selection import train_test_split, KFold, GridSearchCV

try:
  import tensorflow as tf
  import tensorflow.keras as keras
except ModuleNotFoundError:
  import keras

def create_model(input_shape, output_shape, loss=('categorical_crossentropy', 'softmax'), layers=[], **kargs):
  indexs = {}
  first = True
  use_conv2d = False
  model = keras.Sequential()

  for layer in layers:
    if not layer['op'] in indexs:
      indexs[layer['op']] = 0

    if layer['op'] == 'conv2d':
      if not loss is None and use_conv2d is True:
        model.add(keras.layers.Dropout())
      conv2d_padding, pool_padding = kargs['paddings'][indexs['conv2d']]
      activation = kargs['activations'][indexs['conv2d']]

      filter = kargs['filters'][indexs['conv2d']]
      kernel = kargs['kernels'][indexs['conv2d']]

      if first is True:
        model.add(keras.layers.Conv2D(conv2d, padding=conv2d_padding, activation=activation, input_shape=input_shape))
      else:
        model.add(keras.layers.Conv2D(filter, kernel, padding=conv2d_padding, activation=activation))

      if layer['pool'] == 'max':
        model.add(keras.layers.MaxPool2D(padding=pool_padding))
      elif layer['pool'] == 'avg':
        model.add(keras.layers.AvgPool2D(padding=pool_padding))
    elif layer['op'] == 'dense':
      units = kargs['units'][indexs['dense']]
      activation = kargs['activations'][indexs['dense']]

      if first is True:
        model.add(keras.layers.Dense(units, input_shape=input_shape, activation=activation))
      else:
        model.add(keras.layers.Dense(units, activation=activation))

    indexs[layer['op']] += 1
    use_conv2d = layer['op'] == 'conv2d'
    first = False

  if first is True:
    model.add(keras.layers.Dense(output_shape, input_shape=input_shape, activation=loss[1]))
  else:
    model.add(keras.layers.Dense(output_shape, activation=loss[1]))

  model.compile(optimizer='adam', loss=loss[0], metrics=['accuracy'])
  return model

def search_best_fit_model(x_train, y_train, x_test, y_test,
                          input_shape, output_shape, epochs=[10],
                          loss=[('categorical_crossentropy' , 'softmax')],
                          designs=[]):
  y_test_class = np.argmax(y_test, axis=1)
  best_accuracy = 0.0
  best_model = None
  step = -1

  for design in designs:
    use_conv2d = False
    use_dense  = False
    keep_build_model = True
    keep_configure = True

    for layer in design['layers']:
      if layer['op'] == 'conv2d':
        use_conv2d = True
      elif layer['op'] == 'dense':
        use_dense = True

    # load the first configure
    if 'paddings' in design and (step == 4 or step < 0) and use_conv2d:
      configure['paddings'] = design['paddings']
    if 'activations' in design and (step == 3 or step < 0) and use_conv2d:
      configure['activations'] = design['activations']
    if 'filters' in design and (step == 2 or step < 0) and use_conv2d:
      configure['filters'] = design['filters']
    if 'kernels' in design and (step == 1 or step < 0) and use_conv2d:
      configure['kernels'] = design['kernels']
    if 'units' in design and (step == 0 or step < 0) and use_dense:
      configure['units'] = design['units']

    while keep_build_model:
      mode = create_model(input_shape, output_shape, loss=loss, 
                          layers=design['layers'], **configure)

      model.fit(x_train, y_train, epochs=10, verbose=0)
      accuracy = accuracy_score(y_test_class, np.argmax(model.predict(x_test), axis=1))

      if best_model is None:
        best_model = model
        best_accuracy = accuracy
      elif accuracy > best_accuracy:
        best_model = model

      while keep_configure:
        keep_configure = False

        if step < 0:
          keep_configure = True
          step += 1
        elif 'paddings' in design and step == 4:
          if indexs[step] < len(design['paddings']):
            configure['paddings'] = design['paddings'][indexs[step]]
            indexs[step] += 1
            keep_configure = True
            step = 0
          else:
            indexs[step] = 0
            keep_build_model = False
        elif 'activations' in design and step == 3:
          if indexs[step] < len(design['filters']):
            configure['activations'] = design['activations'][indexs[step]]
            indexs[step] += 1
            keep_configure = True
            step = 0
          else:
            indexs[step] = 0
            keep_configure = True
            step += 1
        elif 'filters' in design and step == 2:
          if indexs[step] < len(design['filters']):
            configure['filters'] = design['filters'][indexs[step]]
            indexs[step] += 1
            keep_configure = True
            step = 0
          else:
            indexs[step] = 0
            keep = True
            step += 1
        elif 'kernels' in design and step == 1:
          if indexs[step] < len(design['kernels']):
            configure['kernels'] = design['kernels'][indexs[step]]
            indexs[step] += 1

            if use_dense:
              keep_configure = True
              step = 0
            else:
              keep = False
          else:
            indexs[step] = 0
            keep_configure = True
            step += 1
        elif 'units' in design and step == 0:
          if indexs[step] < len(design['units']):
            configure['units'] = design['units'][indexs[step]]
            indexs[step] += 1
          else:
            indexs[step] = 0

            if use_conv2d:
              step += 1
              keep_configure = True
            else:
              keep_build_model = False

def main(flags):
  seed = 1000
  dataset = pd.read_csv(file)

  x = dataset.drop(dataset.columns[0], axis=1)
  y = dataset.loc[:, dataset.columns[0]]

  x_train, x_test, y_train, y_test = train_test_split(x, y, train_size=0.8, random_state=seed)
  x_train, x_val, y_train, y_val = train_test_split(x_train, y_train, train_size=0.8, random_state=seed)

if __name__ == '__main__':
  parser = argparse.ArgumentParser()
  parser.add_argument('--dataset', required=True, type=str)
  parser.add_argument('--designs', required=True, type=str)
  parser.add_argument('--epoch', required=True, type=int)
  flags, _ = parser.parse_known_args()

  main(flags)
